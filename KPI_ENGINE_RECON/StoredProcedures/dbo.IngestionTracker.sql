SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

CREATE Procedure [dbo].[IngestionTracker]
As
BEGIN
					DECLARE @INFO VARCHAR(8000)
					,	@TRUE BIT = 1
					,	@FALSE BIT = 0
					,	@DB_ID INT = DB_ID()
					,	@LOGID INT = 0
					,	@PROC VARCHAR(500)=OBJECT_SCHEMA_NAME(@@PROCID)+'.'+OBJECT_NAME(@@PROCID)
					,	@PERF_START DATETIME
					,	@PERF_DURATION INT
					,	@PERF_ROW INT
					,	@RC	INT
					;

					
					

					

		EXEC KPI_ENGINE_RECON.dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, @INFO=@INFO, @LOG2ID=@LOGID OUT;
		DECLARE @PROCFULLNAME VARCHAR(500) = DB_NAME()+'.'+@PROC;
		----EXEC dbo.SP_MI_UTIL_SEND_MAIL @PROCFULLNAME, 'STARTED', 1, @ECHO=0; 
	DECLARE @MSG VARCHAR(1000)
	DECLARE @PERF_START_PROC DATETIME 
	DECLARE @PERF_RPS NUMERIC(10,2) 
	DECLARE @ROWS INT 
	DECLARE @PERF_ROWS INT 
	SET @PERF_ROWS = 0 
	SET @PERF_START = GETDATE() 
	SET @PERF_START_PROC = GETDATE() 

BEGIN TRY


Declare @year varchar(4)=Year(GetDate());
Declare @month INT=MONTH(GetDate());
Declare @monthname varchar(20)=DATENAME(month,GetDate());
Declare @rootId INT=159;

-- Add Monthly record if it does not exist
BEGIN
   IF NOT EXISTS (SELECT * FROM KPI_ENGINE_RECON.dbo.Ingestion_Tracker 
                   WHERE IngestionYear = @year
                   AND IngestionMonth = @month
                   )
   BEGIN
       insert into KPI_ENGINE_RECON.dbo.Ingestion_Tracker(IngestionYear,IngestionMonth,IngestionMonthName,ROOT_COMPANIES_ID,DataSource,Others)
       VALUES (@year, @month, @monthname,@rootId,'Humana UHN',2)

	   insert into KPI_ENGINE_RECON.dbo.Ingestion_Tracker(IngestionYear,IngestionMonth,IngestionMonthName,ROOT_COMPANIES_ID,DataSource,Others)
       VALUES (@year, @month, @monthname,@rootId,'Humana PPO',2)

	   insert into KPI_ENGINE_RECON.dbo.Ingestion_Tracker(IngestionYear,IngestionMonth,IngestionMonthName,ROOT_COMPANIES_ID,DataSource,Others)
       VALUES (@year, @month, @monthname,@rootId,'Cigna',2)

	   insert into KPI_ENGINE_RECON.dbo.Ingestion_Tracker(IngestionYear,IngestionMonth,IngestionMonthName,ROOT_COMPANIES_ID,DataSource,Others)
       VALUES (@year, @month, @monthname,@rootId,'Bright',2)

	   insert into KPI_ENGINE_RECON.dbo.Ingestion_Tracker(IngestionYear,IngestionMonth,IngestionMonthName,ROOT_COMPANIES_ID,DataSource,Others)
       VALUES (@year, @month, @monthname,@rootId,'BCBS-UHS',2)

	   insert into KPI_ENGINE_RECON.dbo.Ingestion_Tracker(IngestionYear,IngestionMonth,IngestionMonthName,ROOT_COMPANIES_ID,DataSource,Others)
       VALUES (@year, @month, @monthname,@rootId,'MSSP',2)

	   insert into KPI_ENGINE_RECON.dbo.Ingestion_Tracker(IngestionYear,IngestionMonth,IngestionMonthName,ROOT_COMPANIES_ID,DataSource,Eligibility,MedClaims,RxClaims)
       VALUES (@year, @month, @monthname,@rootId,'Physician Roster',2,2,2)

	   insert into KPI_ENGINE_RECON.dbo.Ingestion_Tracker(IngestionYear,IngestionMonth,IngestionMonthName,ROOT_COMPANIES_ID,DataSource,Eligibility,MedClaims,RxClaims)
       VALUES (@year, @month, @monthname,@rootId,'FlatFiles - Gaps',2,2,2)

	   insert into KPI_ENGINE_RECON.dbo.Ingestion_Tracker(IngestionYear,IngestionMonth,IngestionMonthName,ROOT_COMPANIES_ID,DataSource,Eligibility,MedClaims,RxClaims)
       VALUES (@year, @month, @monthname,@rootId,'FlatFiles - EMR',2,2,2)
   END
END




-- Logic to update Eligibility FLag

-- Humana logic Humana UHN
Drop table if Exists #humanauhnelig
Create Table #humanauhnelig
(
	IngestionYear INT,
	IngestionMonth INT,

)
Insert into #humanauhnelig
select distinct Year(convert(varchar,cast(FileDate as Date),23)),Month(convert(varchar,cast(FileDate as Date),23)) from KPI_ENGINE_SOURCE.UHN.HUMANA_REMBX_ARCHIVE where filename like '%SGTNUHN%'

Update i Set i.Eligibility=1 from KPI_ENGINE_RECON.dbo.Ingestion_Tracker i join #humanauhnelig h on i.IngestionYear=h.IngestionYear and i.IngestionMonth=h.IngestionMonth and i.DataSource='Humana UHN'


-- Humana logic Humana PPO
Drop table if Exists #humanappoelig
Create Table #humanappoelig
(
	IngestionYear INT,
	IngestionMonth INT,

)
Insert into #humanappoelig
select distinct Year(convert(varchar,cast(FileDate as Date),23)),Month(convert(varchar,cast(FileDate as Date),23)) from KPI_ENGINE_SOURCE.UHN.HUMANA_REMBX_ARCHIVE where filename like '%PPO%'

Update i Set i.Eligibility=1 from KPI_ENGINE_RECON.dbo.Ingestion_Tracker i join #humanappoelig h on i.IngestionYear=h.IngestionYear and i.IngestionMonth=h.IngestionMonth and i.DataSource='Humana PPO'



-- Cigna Eligibility
Drop table if Exists #cignaelig
Create Table #cignaelig
(
	IngestionYear INT,
	IngestionMonth INT,

)
Insert into #cignaelig
select distinct Year(convert(varchar,cast(FileDate as Date),23)),Month(convert(varchar,cast(FileDate as Date),23)) from KPI_ENGINE_SOURCE.UHN.CIGNA_ELIGIBILITY_FILES_ARCHIVE

Update i Set i.Eligibility=1 from KPI_ENGINE_RECON.dbo.Ingestion_Tracker i join #cignaelig h on i.IngestionYear=h.IngestionYear and i.IngestionMonth=h.IngestionMonth and i.DataSource='Cigna'


-- Bright Eligibility
Drop table if Exists #brightelig
Create Table #brightelig
(
	IngestionYear INT,
	IngestionMonth INT,

)
Insert into #brightelig
select distinct Year(convert(varchar,cast(FileDate as Date),23)),Month(convert(varchar,cast(FileDate as Date),23)) from KPI_ENGINE_SOURCE.UHN.BRIGHT_ELIGIBILITY_FILES_ARCHIVE

Update i Set i.Eligibility=1 from KPI_ENGINE_RECON.dbo.Ingestion_Tracker i join #brightelig h on i.IngestionYear=h.IngestionYear and i.IngestionMonth=h.IngestionMonth and i.DataSource='Bright'


-- BCBS-UHS Eligibility
Drop table if Exists #bcbsuhselig
Create Table #bcbsuhselig
(
	IngestionYear INT,
	IngestionMonth INT,

)
Insert into #bcbsuhselig
select distinct Year(convert(varchar,cast(FileDate as Date),23)),Month(convert(varchar,cast(FileDate as Date),23)) from KPI_ENGINE_SOURCE.UHN.BCBS_ELIGIBILITY_ARCHIVE

Update i Set i.Eligibility=1 from KPI_ENGINE_RECON.dbo.Ingestion_Tracker i join #bcbsuhselig h on i.IngestionYear=h.IngestionYear and i.IngestionMonth=h.IngestionMonth and i.DataSource='BCBS-UHS'



-- MSSP Eligibility
Drop table if Exists #msspelig
Create Table #msspelig
(
	IngestionYear INT,
	IngestionMonth INT,

)
Insert into #msspelig
select distinct Year(convert(varchar,cast(concat('20',substring(FTP_FILE_NAME,patindex('%[0-9][0-9][0-9][0-9][0-9][0-9]%',FTP_FILE_NAME),6)) as Date),23)),Month(convert(varchar,cast(concat('20',substring(FTP_FILE_NAME,patindex('%[0-9][0-9][0-9][0-9][0-9][0-9]%',FTP_FILE_NAME),6)) as Date),23)) from KPI_ENGINE_SOURCE.dbo.UHN_CCLF8_BEN_DEMOGRAPHICS_ARCHIVED

Update i Set i.Eligibility=1 from KPI_ENGINE_RECON.dbo.Ingestion_Tracker i join #msspelig h on i.IngestionYear=h.IngestionYear and i.IngestionMonth=h.IngestionMonth and i.DataSource='MSSP'



--Update MedClaims Logic

-- Humana logic Humana UHN
Drop table if Exists #humanauhnmedclaims
Create Table #humanauhnmedclaims
(
	IngestionYear INT,
	IngestionMonth INT,

)
Insert into #humanauhnmedclaims
select distinct Year(convert(varchar,cast(FileDate as Date),23)),Month(convert(varchar,cast(FileDate as Date),23)) from KPI_ENGINE_SOURCE.UHN.HUMANA_RECLMEXP_ARCHIVE where filename like '%SGTNUHN%'

Update i Set i.MedClaims=1,RxClaims=1 from KPI_ENGINE_RECON.dbo.Ingestion_Tracker i join #humanauhnmedclaims h on i.IngestionYear=h.IngestionYear and i.IngestionMonth=h.IngestionMonth and i.DataSource='Humana UHN'


-- Humana logic Humana PPO
Drop table if Exists #humanappomedclaims
Create Table #humanappomedclaims
(
	IngestionYear INT,
	IngestionMonth INT,

)
Insert into #humanappomedclaims
select distinct Year(convert(varchar,cast(FileDate as Date),23)),Month(convert(varchar,cast(FileDate as Date),23)) from KPI_ENGINE_SOURCE.UHN.HUMANA_RECLMEXP_ARCHIVE where filename like '%PPO%'

Update i Set i.MedClaims=1,RxClaims=1 from KPI_ENGINE_RECON.dbo.Ingestion_Tracker i join #humanappomedclaims h on i.IngestionYear=h.IngestionYear and i.IngestionMonth=h.IngestionMonth and i.DataSource='Humana PPO'



-- Cigna Eligibility
Drop table if Exists #cignamedclaims
Create Table #cignamedclaims
(
	IngestionYear INT,
	IngestionMonth INT,

)
Insert into #cignamedclaims
select distinct Year(convert(varchar,cast(FileDate as Date),23)),Month(convert(varchar,cast(FileDate as Date),23)) from KPI_ENGINE_SOURCE.UHN.CIGNA_MEDCLAIMS_FILES_ARCHIVE

Update i Set i.MedClaims=1 from KPI_ENGINE_RECON.dbo.Ingestion_Tracker i join #cignamedclaims h on i.IngestionYear=h.IngestionYear and i.IngestionMonth=h.IngestionMonth and i.DataSource='Cigna'


-- Bright Eligibility
Drop table if Exists #brightmedclaims
Create Table #brightmedclaims
(
	IngestionYear INT,
	IngestionMonth INT,

)
Insert into #brightmedclaims
select distinct Year(convert(varchar,cast(FileDate as Date),23)),Month(convert(varchar,cast(FileDate as Date),23)) from KPI_ENGINE_SOURCE.UHN.BRIGHT_MEDCLAIMS_FILES_ARCHIVE

Update i Set i.MedClaims=1 from KPI_ENGINE_RECON.dbo.Ingestion_Tracker i join #brightmedclaims h on i.IngestionYear=h.IngestionYear and i.IngestionMonth=h.IngestionMonth and i.DataSource='Bright'


-- BCBS-UHS Eligibility
Drop table if Exists #bcbsuhsmedclaims
Create Table #bcbsuhsmedclaims
(
	IngestionYear INT,
	IngestionMonth INT,

)
Insert into #bcbsuhsmedclaims
select distinct Year(convert(varchar,cast(FileDate as Date),23)),Month(convert(varchar,cast(FileDate as Date),23)) from KPI_ENGINE_SOURCE.UHN.BCBS_MEDCLAIMS_ARCHIVE

Update i Set i.MedClaims=1 from KPI_ENGINE_RECON.dbo.Ingestion_Tracker i join #bcbsuhsmedclaims h on i.IngestionYear=h.IngestionYear and i.IngestionMonth=h.IngestionMonth and i.DataSource='BCBS-UHS'



-- MSSP Eligibility
Drop table if Exists #msspmedclaims
Create Table #msspmedclaims
(
	IngestionYear INT,
	IngestionMonth INT,

)
Insert into #msspmedclaims
select distinct Year(convert(varchar,cast(concat('20',substring(FTP_FILE_NAME,patindex('%[0-9][0-9][0-9][0-9][0-9][0-9]%',FTP_FILE_NAME),6)) as Date),23)),Month(convert(varchar,cast(concat('20',substring(FTP_FILE_NAME,patindex('%[0-9][0-9][0-9][0-9][0-9][0-9]%',FTP_FILE_NAME),6)) as Date),23)) from KPI_ENGINE_SOURCE.dbo.UHN_CCLF1_CLAIMHEADER_ARCHIVED

Update i Set i.MedClaims=1 from KPI_ENGINE_RECON.dbo.Ingestion_Tracker i join #msspmedclaims h on i.IngestionYear=h.IngestionYear and i.IngestionMonth=h.IngestionMonth and i.DataSource='MSSP'


-- Logic to Check for RxClaims

-- Cigna RxClaims
Drop table if Exists #cignarxclaims
Create Table #cignarxclaims
(
	IngestionYear INT,
	IngestionMonth INT,

)
Insert into #cignarxclaims
select distinct Year(convert(varchar,cast(FileDate as Date),23)),Month(convert(varchar,cast(FileDate as Date),23)) from KPI_ENGINE_SOURCE.UHN.CIGNA_RxCLAIMS_FILES_ARCHIVE

Update i Set i.RxClaims=1 from KPI_ENGINE_RECON.dbo.Ingestion_Tracker i join #cignarxclaims h on i.IngestionYear=h.IngestionYear and i.IngestionMonth=h.IngestionMonth and i.DataSource='Cigna'


-- Bright Eligibility
Drop table if Exists #brightrxclaims
Create Table #brightrxclaims
(
	IngestionYear INT,
	IngestionMonth INT,

)
Insert into #brightrxclaims
select distinct Year(convert(varchar,cast(FileDate as Date),23)),Month(convert(varchar,cast(FileDate as Date),23)) from KPI_ENGINE_SOURCE.UHN.BRIGHT_RXCLAIMS_FILES_ARCHIVE

Update i Set i.RxClaims=1 from KPI_ENGINE_RECON.dbo.Ingestion_Tracker i join #brightrxclaims h on i.IngestionYear=h.IngestionYear and i.IngestionMonth=h.IngestionMonth and i.DataSource='Bright'


-- BCBS-UHS Eligibility
Drop table if Exists #bcbsuhsrxclaims
Create Table #bcbsuhsrxclaims
(
	IngestionYear INT,
	IngestionMonth INT,

)
Insert into #bcbsuhsrxclaims
select distinct Year(convert(varchar,cast(FileDate as Date),23)),Month(convert(varchar,cast(FileDate as Date),23)) from KPI_ENGINE_SOURCE.UHN.Medco_Rxclaims_Archive

Update i Set i.RxClaims=1 from KPI_ENGINE_RECON.dbo.Ingestion_Tracker i join #bcbsuhsrxclaims h on i.IngestionYear=h.IngestionYear and i.IngestionMonth=h.IngestionMonth and i.DataSource='BCBS-UHS'



-- MSSP Eligibility
Drop table if Exists #mssprxclaims
Create Table #mssprxclaims
(
	IngestionYear INT,
	IngestionMonth INT,

)
Insert into #mssprxclaims
select distinct Year(convert(varchar,cast(concat('20',substring(FTP_FILE_NAME,patindex('%[0-9][0-9][0-9][0-9][0-9][0-9]%',FTP_FILE_NAME),6)) as Date),23)),Month(convert(varchar,cast(concat('20',substring(FTP_FILE_NAME,patindex('%[0-9][0-9][0-9][0-9][0-9][0-9]%',FTP_FILE_NAME),6)) as Date),23)) from KPI_ENGINE_SOURCE.dbo.UHN_CCLF7_PART_D_ARCHIVED


Update i Set i.RxClaims=1 from KPI_ENGINE_RECON.dbo.Ingestion_Tracker i join #mssprxclaims h on i.IngestionYear=h.IngestionYear and i.IngestionMonth=h.IngestionMonth and i.DataSource='MSSP'


-- update logic for Physician roster and FlatFiles
-- Physician Roster
Drop table if Exists #physicianroster
Create Table #physicianroster
(
	IngestionYear INT,
	IngestionMonth INT,

)
Insert into #physicianroster
select distinct Year(convert(varchar,cast(FileDate as Date),23)),Month(convert(varchar,cast(FileDate as Date),23)) from KPI_ENGINE_SOURCE.UHN.PHYSICIAN_ROSTER_ARCHIVE

Update i Set i.others=1 from KPI_ENGINE_RECON.dbo.Ingestion_Tracker i join #physicianroster h on i.IngestionYear=h.IngestionYear and i.IngestionMonth=h.IngestionMonth and i.DataSource='Physician Roster'


-- FlatFiles EMR
Drop table if Exists #flatfilesemr
Create Table #flatfilesemr
(
	IngestionYear INT,
	IngestionMonth INT,

)
Insert into #flatfilesemr
select distinct Year(convert(varchar,cast(FileDate as Date),23)),Month(convert(varchar,cast(FileDate as Date),23)) from KPI_ENGINE_SOURCE.UHN.FlatFiles_EMR_ARCHIVE

Update i Set i.others=1 from KPI_ENGINE_RECON.dbo.Ingestion_Tracker i join #flatfilesemr h on i.IngestionYear=h.IngestionYear and i.IngestionMonth=h.IngestionMonth and i.DataSource='FlatFiles - EMR'


-- FlatFiles Gaps
Drop table if Exists #flatfilesgaps
Create Table #flatfilesgaps
(
	IngestionYear INT,
	IngestionMonth INT,

)
Insert into #flatfilesgaps
select distinct Year(convert(varchar,cast(FileDate as Date),23)),Month(convert(varchar,cast(FileDate as Date),23)) from KPI_ENGINE_SOURCE.UHN.FlatFiles_GAPS_ARCHIVE

Update i Set i.others=1 from KPI_ENGINE_RECON.dbo.Ingestion_Tracker i join #flatfilesgaps h on i.IngestionYear=h.IngestionYear and i.IngestionMonth=h.IngestionMonth and i.DataSource='FlatFiles - Gaps'



		SET @PERF_ROW = @@ROWCOUNT
		SET @PERF_DURATION = DATEDIFF(MINUTE,@PERF_START,GETDATE());
		EXEC KPI_ENGINE_RECON.dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, 'Enrollment Completed', @PERF_ROW, @DURATION_IN_MIN=@PERF_DURATION, @LOG2ID=@LOGID;
		EXEC KPI_ENGINE_RECON.dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, @LOG2ID=@LOGID, @END_FLAG=1;

END TRY
BEGIN CATCH
		BEGIN
			EXEC KPI_ENGINE_RECON.dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, 'FATAL ERROR', @LOG2ID=@LOGID;
			RAISERROR ('',16,0)
			RETURN 16
        END
	END CATCH
RETURN 0
END






GO
