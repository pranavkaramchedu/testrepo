SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
CREATE procedure sp_incoming_latest_file
(@statement varchar(50),@payer varchar(50))
as
BEGIN

if @statement='update_eligibility'
begin
update incoming_latest_file
set eligibility=1
where payer=@payer
end

if @statement='update_claims'
begin
update incoming_latest_file
set claims=1
where payer=@payer
end

if @statement='update_rx'
begin
update incoming_latest_file
set rx=1
where payer=@payer
end
end
GO
