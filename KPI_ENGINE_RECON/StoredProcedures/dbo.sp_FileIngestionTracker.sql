SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
create procedure sp_FileIngestionTracker
(@statement varchar(50),@payer varchar(50))
as
BEGIN

if @statement='update_eligibility'
begin
update FileIngestionTracker
set eligibility=1
where payer=@payer
end

if @statement='update_claims'
begin
update FileIngestionTracker
set claims=1
where payer=@payer
end

if @statement='update_rx'
begin
update FileIngestionTracker
set rx=1
where payer=@payer
end
end
GO
