SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
CREATE Proc [dbo].[usp_GetSourceIngestionStats]
AS

BEGIN
					DECLARE @INFO VARCHAR(8000)
					,	@TRUE BIT = 1
					,	@FALSE BIT = 0
					,	@DB_ID INT = DB_ID()
					,	@LOGID INT = 0
					,	@PROC VARCHAR(500)=OBJECT_SCHEMA_NAME(@@PROCID)+'.'+OBJECT_NAME(@@PROCID)
					,	@PERF_START DATETIME
					,	@PERF_DURATION INT
					,	@PERF_ROW INT
					,	@RC	INT
					;

					Select @PROC
					

					

		EXEC KPI_ENGINE_RECON.dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, @INFO=@INFO, @LOG2ID=@LOGID OUT;
		DECLARE @PROCFULLNAME VARCHAR(500) = DB_NAME()+'.'+@PROC;
		----EXEC dbo.SP_MI_UTIL_SEND_MAIL @PROCFULLNAME, 'STARTED', 1, @ECHO=0; 
	DECLARE @MSG VARCHAR(1000)
	DECLARE @PERF_START_PROC DATETIME 
	DECLARE @PERF_RPS NUMERIC(10,2) 
	DECLARE @ROWS INT 
	DECLARE @PERF_ROWS INT 
	SET @PERF_ROWS = 0 
	SET @PERF_START = GETDATE() 
	SET @PERF_START_PROC = GETDATE() 

BEGIN TRY




Declare @metricid INT;
Declare @expression nvarchar(max);
Declare @table varchar(100);
Declare @source varchar(100);
Declare @xx nvarchar(max);
DECLARE @SQL nVARCHAR(MAX);
Declare @desc varchar(300);

DECLARE C CURSOR FOR 
Select 
	id,
	Expression,
	Description,
	Source
from KPI_Engine_Recon.dbo.Source_Metrics_Config
Where
	DeleteFlag=0

OPEN C  
FETCH NEXT FROM C INTO @metricid,@expression,@desc,@source

WHILE @@FETCH_STATUS = 0  
BEGIN  


Set @SQL='Select @x=('+replace(@expression,';','')+' FOR JSON PATH)'
Print @SQL

exec sp_executesql @SQL, N'@x nvarchar(max) out', @xx out

Print @xx

If(@xx is not null)
Begin
 
	Insert into KPI_ENGINE_RECON.EVN.Source_Ingestion_Stats(MetricId,MetricValues)  values(cast(@metricid as varchar),@xx)

End

FETCH NEXT FROM C INTO @metricid,@expression,@desc,@source
END 

CLOSE C  
DEALLOCATE C

		SET @PERF_ROW = @@ROWCOUNT
		SET @PERF_DURATION = DATEDIFF(MINUTE,@PERF_START,GETDATE());
		EXEC KPI_ENGINE_RECON.dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, 'Enrollment Completed', @PERF_ROW, @DURATION_IN_MIN=@PERF_DURATION, @LOG2ID=@LOGID;
		EXEC KPI_ENGINE_RECON.dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, @LOG2ID=@LOGID, @END_FLAG=1;

END TRY
BEGIN CATCH
		BEGIN
			EXEC KPI_ENGINE_RECON.dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, 'FATAL ERROR', @LOG2ID=@LOGID;
			RAISERROR ('',16,0)
			RETURN 16
        END
	END CATCH
RETURN 0
END






GO
