SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON






CREATE proc [dbo].[data_profiling] @tablename varchar(100),@schemaname varchar(20)
as


Declare @col_ct INT;
Declare @colname varchar(100);
declare @col_type varchar(50);
declare @col_len varchar(20);
declare @minval varchar(50);
declare @maxval nvarchar(max);
declare @top10val nvarchar(max);
declare @rowct INT;
declare @fillct INT;
declare @nullct INT;
declare @uniquect INT;
declare @i INT=0;
declare @isnumber INT;
declare @Sql nvarchar(max);
declare @ttablename varchar(200);

Set @ttablename=concat(@schemaname,'.',@tablename);

-- Delete any existing records
SET @Sql = 'Delete from dbo.raw_data_profiling where TableName='''+@tablename+''''
EXECUTE sp_executesql @Sql
-- Print 'Delete Records'
-- Get total column count of the table
SET @Sql = 'SELECT @col_ct=count(*) from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME=''' + @tablename+ '''and TABLE_SCHEMA='''+@schemaname+''''

EXECUTE sp_executesql @Sql,N'@col_ct INT OUTPUT',
@col_ct = @col_ct OUTPUT

-- Print 'Get Column Count'
-- Run a loop to get stats for each column
while(@i<@col_ct)
Begin
	
	Set @minval='';
	Set @maxval='';


	-- Get the column name
	SET @Sql = 'SELECT @colname=COLUMN_NAME from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME=''' +@tablename+ '''and TABLE_SCHEMA='''+@schemaname+''' Order by Column_Name ASC OFFSET @ctr ROWS FETCH NEXT 1 ROWS ONLY'
	
	EXECUTE sp_executesql @Sql, N'@ctr INT,@colname varchar(100) OUTPUT',@i,@colname = @colname OUTPUT;

	-- Print 'Get Column Name'
	-- Print @colname


--	Get Column data type and length

	SET @Sql = 'SELECT @col_type=DATA_TYPE,@col_len=CHARACTER_MAXIMUM_LENGTH from information_schema.columns where TABLE_NAME=''' +@tablename+ '''and TABLE_SCHEMA='''+@schemaname+''' and COLUMN_NAME='''+@colname+''''


	EXECUTE sp_executesql @Sql, N'@col_type varchar(100) OUTPUT,@col_len varchar(20) OUTPUT',
	@col_type = @col_type OUTPUT,
	@col_len=@col_len OUTPUT
	
/*
	-- Load data in temp table
	drop table if exists colinmem;

	SET @Sql='CREATE TABLE colinmem(data '+@col_type+'('+@col_len+')) Create clustered index idx on colinmem(data)'

	EXECUTE sp_executesql @Sql;

--	Print 'colinmem created'
	-- Insert data in mem table

	SET @Sql='Insert into colinmem select '+@colname+' from '+@ttablename;
	EXECUTE sp_executesql @Sql;

	Print 'data inserted in colinmem'
*/
	-- Check if the Column is numeric

	SET @Sql = 'SELECT @isnumber=Min(ISNUMERIC('+@colname+')) from '+@ttablename+' where '+@colname+' is not null';
	EXECUTE sp_executesql @Sql, N'@isnumber varchar(100) OUTPUT',@isnumber = @isnumber OUTPUT;
	-- Print 'Check if Column is numeric'
	---- Print @colname +'->'+cast(@isnumber	as varchar)


--	SELECT @isnumber=Min(ISNUMERIC(data)) from colinmem where data is not null;
		
--- Get total Row count

	SET @Sql = 'SELECT @rowct=count(*) from '+@ttablename

	EXECUTE sp_executesql @Sql,N'@rowct INT OUTPUT',
	@rowct = @rowct OUTPUT

	
--	select @rowct=count(*) from colinmem;

	-- Print 'Get Row Count'
---- Get Unique value count
	
	SET @Sql = 'SELECT @uniquect=count(distinct '+@colname+') from '+@ttablename
	EXECUTE sp_executesql @Sql,N'@uniquect INT OUTPUT',
	@uniquect = @uniquect OUTPUT
	-- Print 'Get Unique Count'


--	select @uniquect=count(distinct data) from colinmem;

------ Get FIll rate

	SET @Sql = 'SELECT @fillct=count(*) from '+@ttablename+' where len('+@colname+')>0 and '+@colname+' is not null'
	
	EXECUTE sp_executesql @Sql,N'@fillct INT OUTPUT',
	@fillct = @fillct OUTPUT
	-- Print 'Get Fill Count'

--	select @fillct=count(*) from colinmem where len(data)>0 and data is not null

--------------- Get Null value count

	SET @Sql = 'SELECT @nullct=count(*) from '+@ttablename+' where '+@colname+' is null'

	EXECUTE sp_executesql @Sql,N'@nullct INT OUTPUT',
	@nullct = @nullct OUTPUT
	-- Print 'Get null Count'

--	select @nullct=count(*) from colinmem where data is null;

---- Get top 10 values by occurence


	SET @Sql = 'SELECT @top10val=string_agg(concat('+@colname+
	','':'',occurrence),'','') from (
	select top 10 '+@colname+',count(*) as occurrence from '+@ttablename+' group by '+@colname+' order by 2 desc
	)t1'
	
	EXECUTE sp_executesql @Sql,N'@top10val nvarchar(max) OUTPUT',
	@top10val = @top10val OUTPUT
	-- Print 'Get Top 10 Values'

/*	
	select @top10val=string_agg(concat(data,':',occurrence),',') from(
	select top 10 data,count(*) as occurrence from colinmem group by data order by 2 desc
	)t1 
*/
--- Get min and max value if number else get length of the string
	If(@isnumber=1)
	Begin
		
		SET @Sql = 'SELECT top 1 @minval=min('+@colname+') from '+@ttablename
		EXECUTE sp_executesql @Sql,N'@minval varchar(300) OUTPUT',
		@minval = @minval OUTPUT
		-- Print 'Get Min Value'
		
		--select top 1 @minval=min(data) from colinmem;
	End
	Else
	Begin
		
		SET @Sql = 'select top 1 @minval='+@colname+' from '+@ttablename+' where len('+@colname+')=(select min(len('+@colname+')) from '+@ttablename+')'
		
		EXECUTE sp_executesql @Sql,N'@minval varchar(300) OUTPUT',
		@minval = @minval OUTPUT
		-- Print 'Get Min Value 1'
		

		--select top 1 @minval=data from colinmem where len(data)=(select min(len(data)) from colinmem);

	End
------

	If(@isnumber=1)
	Begin
		
		SET @Sql = 'SELECT top 1 @maxval=max('+@colname+') from '+@ttablename
		EXECUTE sp_executesql @Sql,N'@maxval varchar(300) OUTPUT',
		@maxval = @maxval OUTPUT
		-- Print 'Get Max Value'
		

		--select top 1 @maxval=max(data) from colinmem;
	End
	Else
	Begin
		
		SET @Sql = 'select top 1 @maxval='+@colname+' from '+@ttablename+' where len('+@colname+')=(select max(len('+@colname+')) from '+@ttablename+')'
		
		EXECUTE sp_executesql @Sql,N'@maxval varchar(300) OUTPUT',
		@maxval = @maxval OUTPUT
		-- Print 'Get Max Value 1'
		

		--select top 1 @maxval=data from colinmem where len(data)=(select max(len(data)) from colinmem)
	End	

	
--
	Insert into [dbo].[raw_data_profiling](TableName,ColumnName,RowCt,FillCt,FillPercentage,UniqueValuesCt,NullCt,Minvalue,MaxValue,Top10Values) values(@tablename,@colname,@rowct,@fillct,concat(round((cast(@fillct as float)/cast(@rowct as float))*100,2),'%'),@uniquect,@nullct,@minval,@maxval,@top10val)

	Set @i=@i+1;

	
END

	--drop table if exists colinmem;


GO
