SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
CREATE PROCEDURE [dbo].[MSSP_ARCHIVE_SOURCE_DATA]
AS
BEGIN

declare @table varchar(200)
declare @sql varchar(max)

declare C_ARCH cursor
For 
select distinct T.name
from sys.tables T
left join sys.schemas S on T.schema_id=S.Schema_id
left join sys.columns C on T.object_id=C.object_id 
where (T.name like '%CCLF%' and T.name not like '%ARCHIVED') or T.name in('UHN_BOTC'/*,'UHN_QLAR_2020_MSSP','UHN_QLAR_MSSP'*/)
and S.name='dbo'


OPEN C_ARCH  
FETCH NEXT FROM C_ARCH INTO @table  

WHILE @@FETCH_STATUS = 0  
BEGIN  

SET @sql='INSERT INTO KPI_ENGINE_SOURCE.DBO.'+@table+'_ARCHIVED SELECT * FROM KPI_ENGINE_SOURCE.DBO.'+@table
exec(@sql)
---print(@sql)

SET @sql='DELETE FROM KPI_ENGINE_SOURCE.DBO.'+@table
exec(@sql)
---print(@sql)
      

FETCH NEXT FROM C_ARCH INTO @table  
END 

CLOSE C_ARCH
DEALLOCATE C_ARCH

END
GO
