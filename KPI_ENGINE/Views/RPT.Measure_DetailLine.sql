SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON











CREATE VIEW [RPT].[Measure_DetailLine]
As
SELECT distinct 
      [MEASURE_ID]
      ,[MEASURE_NAME]
      ,[MEASURE_TYPE]
      ,[Payer]
      ,[EMPI]
      ,[PayerId]
      ,[MEM_FNAME]
      ,[MEM_MNAME]
      ,[MEM_LNAME]
      ,[MEM_DOB]
      ,[MEM_GENDER]
      ,[ENROLLMENT_STATUS]
      ,[LAST_VISIT_DATE]
      ,[PRODUCT_TYPE]
      ,[NUM]
      ,[DEN]
      ,[EXCL]
      ,[REXCL]
      ,[Code]
      ,[DateofService]
      ,[CE]
      ,[Epop]
      ,[Event]
	  ,[DischargeDate] 
      ,[outlier]
      ,[Encounters]
	  ,[REPORT_ID]
      ,[ReportType]
      ,[REPORT_RUN_DATE]
	  ,Report_Quarter
      ,[ROOT_COMPANIES_ID]
	  ,PERIOD_START_DATE
	  ,PERIOD_END_DATE
	  ,DischargeFacility
	  ,LastSeenDate
	  ,EpisodeDate
  FROM [KPI_ENGINE].[RPT].[MEASURE_DETAILED_LINE]
  Where 
	REPORT_ID=(Select top 1 ReportId from RPT.Report_Details where ROOT_COMPANIES_ID=159 order by ReportEndDate Desc) and
	Measure_id not in('CDC1','CDC9','COA2','COA4')
	
	Union all

	SELECT  
      NULL as [MEASURE_ID]
      ,NULL as  [MEASURE_NAME]
      ,NULL as [MEASURE_TYPE]
      ,NULL as  [Payer]
      ,NULL as  [EMPI]
      ,NULL as  [PayerId]
      ,NULL as  [MEM_FNAME]
      ,NULL as  [MEM_MNAME]
      ,NULL as  [MEM_LNAME]
      ,cast('1900-01-01' as date) as  [MEM_DOB]
      ,NULL as  [MEM_GENDER]
      ,NULL as  [ENROLLMENT_STATUS]
      ,cast('1900-01-01' as date) as  [LAST_VISIT_DATE]
      ,NULL as  [PRODUCT_TYPE]
      ,NULL as  [NUM]
      ,NULL as  [DEN]
      ,NULL as  [EXCL]
      ,NULL as [REXCL]
      ,NULL as [Code]
      ,cast('1900-01-01' as date) as [DateofService]
      ,NULL as [CE]
      ,NULL as [Epop]
      ,NULL as [Event]
     ,cast('1900-01-01' as date) as [DischargeDate] 
      ,NULL as [outlier]
      ,NULL as [Encounters]
	  ,NULL as [REPORT_ID]
      ,NULL as [ReportType]
	  
      ,cast('1900-01-01' as date) as [REPORT_RUN_DATE]
	  ,'1900 - Q4' as Report_Quarter
      ,159 as [ROOT_COMPANIES_ID]
	  ,cast('1900-01-01' as date)
	  ,cast('1900-01-01' as date)
	  ,null
	  ,null
	  ,null
  

  

GO
