SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

CREATE view [dbo].[Utilization_Drilldown] as
select distinct
  mdl.measure_name,
  mdl.measure_id,
  mdl.[PROVIDER_ID],
  mdl.[PCP_NAME],
  mdl.[PRACTICE_NAME],
  mdl.[SPECIALTY],
  [LAST_VISIT_DATE],
  [MEM_FNAME],
  [MEM_LNAME],
  [Payer],
  [PayerId],
  [MEM_DOB],
  [MEM_GENDER],
  [ENROLLMENT_STATUS],
  [NetworkResult],
  [To_target],
  [Den],
  [Num],
  [Excl],
  [Rexcl],
  [code],
  [dateofservice],
  [dischargedate],
  mdl.[Encounters],
  scr.[REPORT_RUN_DATE],
  scr.[PERIOD_START_DATE],
  scr.[PERIOD_END_DATE],
  mdl.root_companies_id,
  case
    when scr.measure_name = '30 Day Readmission Rate – All Cause' then 'Readmission'
    when scr.measure_name = 'All cause unplanned admissions for patients with multiple chronic conditions' then 'Admission'
    when scr.measure_name = 'Ambulatory Sensitive Conditions - Acute Composite (Dehydration, Bacterial Pneumonia, or UTI)' then 'Admission'
    when scr.measure_name = 'ED Visits/1,000' then 'ED'
    when scr.measure_name = 'SNF 30 Day all cause readmission' then 'SNF Readmission'
    else '-'
  end as Types_Encounter
from
  [KPI_ENGINE].[RPT].[MEASURE_DETAILED_LINE] mdl
  join [KPI_ENGINE].[RPT].[PROVIDER_SCORECARD] scr on mdl.[REPORT_ID] = scr.[REPORT_ID]
  and mdl.MEASURE_ID = scr.MEASURE_ID
  and mdl.measure_name = scr.measure_name
  and mdl.root_companies_id = scr.root_companies_id
where
  mdl.root_companies_id = 159 and
  ENROLLMENT_STATUS='Active'
  and 1 = 1
  and mdl.REPORT_ID =(
   select top 1 ReportId from RPT.Report_details Order by ReportEndDate Desc
  )
	and MEASURE_TITLE='Utilization Management'

GO
