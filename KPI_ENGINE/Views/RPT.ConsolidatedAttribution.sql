SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON






/* CR-2021-11-24 -Shanawaz - KPISQL-537 Updated the view definition to include AdmissionDate and Discharge Date */ 










CREATE VIEW [RPT].[ConsolidatedAttribution]
As
Select 
	* 
from
(
select EMPI,PayerId,AlternateMemberId,MedicareId,MemberLastName,MemberFirstName,MemberMiddleName,MemberDOB,MEM_GENDER,EnrollmentStatus,AssignedStatus,DATA_SOURCE as Payer,'Ambulatory_PCP' AS Attribution_Type,'1' as [Rank],AmbulatoryPCPNPI as NPI,AmbulatoryPCPName as Prov_Name,AmbulatoryPCPSpecialty as Specialty,AmbulatoryPCPPractice as Practice,AmbulatoryPCPRecentVisit as RecentVisit,null as AdmissionDate,null as DischargeDate,a.ReportId,LoadDate,rd.ReportEndDate,a.ROOT_COMPANIES_ID,rd.ReportQuarter from KPI_ENGINE.RPT.PCP_ATTRIBUTION a
Join RPT.Report_Details rd on
	a.ReportId=rd.ReportId
where a.AssignedStatus='Assigned' and a.ReportId=(Select top 1 ReportId from RPT.Report_Details where ROOT_COMPANIES_ID=159   order by ReportEndDate Desc)

Union all


SELECT P.EMPI,at.PayerId,AlternateMemberId,MedicareId,MemberLastName,MemberFirstName,MemberMiddleName,MemberDOB,MEM_GENDER,EnrollmentStatus,AssignedStatus,DATA_SOURCE,a.AttributionType,a.Rank,a.[NPI],a.Provider,a.Specialty,a.Practice,a.RecentVisit,a.AdmissionDate,a.DischargeDate,at.ReportId,LoadDate,rd.ReportEndDate,at.ROOT_COMPANIES_ID,rd.ReportQuarter
FROM RPT.AttributionDocument P CROSS APPLY OPENJSON (P.attribution) WITH (   
              Specialty   varchar(200) '$.Specialty' ,  
              NPI     varchar(20)     '$.NPI',  
              Provider varchar(200) '$.Provider',  
              AttributionType varchar(100)          '$.AttributionType'  ,
			  RecentVisit date '$.RecentVisit'  ,
			  Practice varchar(100) '$.Practice',
			  AdmissionDate varchar(20) '$.AdmissionDate',
			  DischargeDate varchar(20) '$.DischargeDate',
			  Rank INT '$.Rank'

 )  AS a
 Join RPT.PCP_Attribution at on p.EMPI=at.EMPI
 Join RPT.Report_Details rd on
	at.ReportId=rd.ReportId
 Where
	at.ReportId=(Select top 1 ReportId from RPT.Report_Details where ROOT_COMPANIES_ID=159   order by ReportEndDate Desc)


Union all

select EMPI,PayerId,AlternateMemberId,MedicareId,MemberLastName,MemberFirstName,MemberMiddleName,MemberDOB,MEM_GENDER,EnrollmentStatus,AssignedStatus,DATA_SOURCE as Payer,'Hospitalist' AS Attribution_Type,'1' as [Rank],HospitalistPCPNPI as NPI,HospitalistPCPName as Prov_Name,HospitalistPCPSpecialty as Specialty,HospitalistPCPPractice as Practice,HospitalistPCPRecentVisit as RecentVisit,null,null,a.ReportId,	LoadDate,rd.ReportEndDate,a.ROOT_COMPANIES_ID,rd.ReportQuarter from KPI_ENGINE.RPT.PCP_ATTRIBUTION a
Join RPT.Report_Details rd on
	a.ReportId=rd.ReportId
where a.AssignedStatus='Assigned' and a.AssignedStatus='Assigned' and a.ReportId=(Select top 1 ReportId from RPT.Report_Details where ROOT_COMPANIES_ID=159   order by ReportEndDate Desc)
and HospitalistPCPNPI=''

Union all

select EMPI,PayerId,AlternateMemberId,MedicareId,MemberLastName,MemberFirstName,MemberMiddleName,MemberDOB,MEM_GENDER,EnrollmentStatus,AssignedStatus,DATA_SOURCE as Payer,'Ambulatory_Specialist' AS Attribution_Type,'1' as [Rank],AmbulatorySpecialistNPI_1 as NPI,AmbulatorySpecialistName_1 as Prov_Name,AmbulatorySpecialistSpecialty_1 as Specialty,AmbulatorySpecialistPractice_1 as Practice,AmbulatorySpecialistRecentVisit_1 as RecentVisit,null,null,a.ReportId,	LoadDate,rd.ReportEndDate,a.ROOT_COMPANIES_ID,rd.ReportQuarter from KPI_ENGINE.RPT.PCP_ATTRIBUTION a
Join RPT.Report_Details rd on
	a.ReportId=rd.ReportId
where a.AssignedStatus='Assigned' and a.ReportId=(Select top 1 ReportId from RPT.Report_Details where ROOT_COMPANIES_ID=159   order by ReportEndDate Desc)
and AmbulatorySpecialistNPI_1=''


Union all



select EMPI,PayerId,AlternateMemberId,MedicareId,MemberLastName,MemberFirstName,MemberMiddleName,MemberDOB,MEM_GENDER,EnrollmentStatus,AssignedStatus,DATA_SOURCE as Payer,'IP_Surg_Specialist' AS Attribution_Type,'1' as [Rank],InpatientSurgicalSpecialistNPI_1 as NPI,InpatientSurgicalSpecialistName_1 as Prov_Name,InpatientSurgicalSpecialistSpecialty_1 as Specialty,InpatientSurgicalSpecialistPractice_1 as Practice,InpatientSurgicalSpecialistRecentVisit_1 as RecentVisit,null,null,a.ReportId,	LoadDate,rd.ReportEndDate,a.ROOT_COMPANIES_ID,rd.ReportQuarter from KPI_ENGINE.RPT.PCP_ATTRIBUTION a
Join RPT.Report_Details rd on
	a.ReportId=rd.ReportId
where a.AssignedStatus='Assigned' and a.ReportId=(Select top 1 ReportId from RPT.Report_Details where ROOT_COMPANIES_ID=159   order by ReportEndDate Desc)
and InpatientSurgicalSpecialistNPI_1=''



Union all

select NULL as EMPI,NULL as PayerId,NULL as AlternateMemberId,NULL as MedicareId,NULL as MemberLastName,NULL as MemberFirstName,NULL as MemberMiddleName,NULL as MemberDOB,NULL as MEM_GENDER,NULL as EnrollmentStatus,NULL as AssignedStatus,NULL as Payer,NULL as Attribution_Type,NULL  as [Rank],NULL as NPI,NULL as Prov_Name,NULL as Specialty,NULL as Practice,NULL as RecentVisit,null,null,NULL as  ReportId,NULL as 	LoadDate,NULL as ReportEndDate,159 as ROOT_COMPANIES_ID,'2020 - Q4' as ReportQuarter

union all

Select '1','1','1','1','Dummy','Record','','1900-01-01','','Inactive','','',case when PROV_SPEC_CODE2 in('207Q00000X','207R00000X','208D00000X','208000000X') Then 'Ambulatory_PCP' when PROV_SPEC_CODE2='208M00000X' Then 'Hospitalist' when PROV_SPEC_DESC2 in('207RC0001X','207RC0000X','207RC0000X','207RG0100X','207VG0400X','207RI0011X','207RN0300X','207V00000X','207W00000X','152W00000X','207RP1001X','207RR0500X') Then 'Ambulatory_Specialist' WHen PROV_SPEC_DESC2 in('207L00000X','208G00000X','208C00000X','208600000X','207T00000X','2086S0129X') then 'IP_Surg_Specialist' end as Attribution_Type,1,PROV_NPI,PROV_NAME,PROV_SPEC_DESC2,PROV_GRP_NAME,null,null,null,(Select top 1 ReportId from RPT.Report_Details where ROOT_COMPANIES_ID=159   order by ReportEndDate Desc),null,null,159,null from PROVIDER where _PROV_UDF_02_='Y'  and  _PROV_UDF_04_='Primary Address' and PROV_NPI not in(
Select distinct AmbulatoryPCPNPI from RPT.PCP_attribution where AssignedStatus='Assigned' and ReportId=(Select top 1 ReportId from RPT.Report_Details where ROOT_COMPANIES_ID=159   order by ReportEndDate Desc)

	Union all

	Select distinct HospitalistPCPNPI from RPT.PCP_attribution where AssignedStatus='Assigned' and  ReportId=(Select top 1 ReportId from RPT.Report_Details where ROOT_COMPANIES_ID=159   order by ReportEndDate Desc)

	Union all

	Select distinct AmbulatorySpecialistNPI_1 from RPT.PCP_attribution where AssignedStatus='Assigned' and  ReportId=(Select top 1 ReportId from RPT.Report_Details where ROOT_COMPANIES_ID=159   order by ReportEndDate Desc)

	
	Union all

	Select distinct AmbulatorySpecialistNPI_2 from RPT.PCP_attribution where AssignedStatus='Assigned' and  ReportId=(Select top 1 ReportId from RPT.Report_Details where ROOT_COMPANIES_ID=159   order by ReportEndDate Desc)

	Union all

	Select distinct AmbulatorySpecialistNPI_3 from RPT.PCP_attribution where AssignedStatus='Assigned' and  ReportId=(Select top 1 ReportId from RPT.Report_Details where ROOT_COMPANIES_ID=159   order by ReportEndDate Desc)

	Union all

	Select distinct AmbulatorySpecialistNPI_4 from RPT.PCP_attribution where AssignedStatus='Assigned' and  ReportId=(Select top 1 ReportId from RPT.Report_Details where ROOT_COMPANIES_ID=159   order by ReportEndDate Desc)

	Union all  

	Select distinct AmbulatorySpecialistNPI_5 from RPT.PCP_attribution where AssignedStatus='Assigned' and  ReportId=(Select top 1 ReportId from RPT.Report_Details where ROOT_COMPANIES_ID=159   order by ReportEndDate Desc)

	Union all  

	Select distinct InpatientSurgicalSpecialistNPI_1 from RPT.PCP_attribution where AssignedStatus='Assigned' and  ReportId=(Select top 1 ReportId from RPT.Report_Details where ROOT_COMPANIES_ID=159   order by ReportEndDate Desc)

	Union all  

	Select distinct InpatientSurgicalSpecialistNPI_2 from RPT.PCP_attribution where  AssignedStatus='Assigned' and ReportId=(Select top 1 ReportId from RPT.Report_Details where ROOT_COMPANIES_ID=159   order by ReportEndDate Desc)

	Union all


	Select distinct InpatientSurgicalSpecialistNPI_3 from RPT.PCP_attribution where AssignedStatus='Assigned' and  ReportId=(Select top 1 ReportId from RPT.Report_Details where ROOT_COMPANIES_ID=159   order by ReportEndDate Desc)

	Union all  

	Select distinct InpatientSurgicalSpecialistNPI_4 from RPT.PCP_attribution where AssignedStatus='Assigned' and  ReportId=(Select top 1 ReportId from RPT.Report_Details where ROOT_COMPANIES_ID=159   order by ReportEndDate Desc)

	Union all  

	Select distinct InpatientSurgicalSpecialistNPI_5 from RPT.PCP_attribution where  AssignedStatus='Assigned' and ReportId=(Select top 1 ReportId from RPT.Report_Details where ROOT_COMPANIES_ID=159   order by ReportEndDate Desc)
)


)t1









GO
