SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
CREATE VIEW Total_Gaps AS
SELECT distinct [MEASURE_NAME],
  sum(Gaps) as Gaps
FROM KPI_ENGINE.RPT.PROVIDER_SCORECARD
where
  [root_companies_id] = [root_companies_id]
  and 1 = 1
  and REPORT_ID =(
    select
      MAX(REPORT_ID)
    from
      KPI_ENGINE.RPT.PROVIDER_SCORECARD
    where
      Report_Quarter = '2020 - Q4'
  )
  and MEASURE_TITLE = 'Utilization Management'
group by
  [MEASURE_NAME]

GO
