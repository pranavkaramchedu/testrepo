SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON






CREATE view [dbo].[HCC_Patient_Summary] as

Select
	patient_id as EMPI,
	t2.OpenHCCcount,
	t2.ClosedHCCcount,
	t2.ClosureRate,
	t2.DiagnosisRiskScore,
	t2.ChronicHCCCount,
	cast(ISNULL(cms_risk_score,0) as float) as RiskScore
From
(
	Select
		EMPI
		,count(case when status!='Closed' then 1 end) as OpenHCCcount
		,count(case when status='Closed' then 1 end) as ClosedHCCcount
		,Round((cast(count(case when status='Closed' and HCCType!='Interaction' then 1 end) as float)/ISNULL(NULLIF((count(case when status!='Closed' and HCCType!='Interaction' then 1 end)+count(case when status='Closed' and HCCType!='Interaction' then 1 end)),0),1))*100,2) as ClosureRate
		,sum(HCCWeight) as DiagnosisRiskScore
		,count(case when HCCType='Chronic' then 1 end) as ChronicHCCCount
	From
	(
		select 
		EMPi,
		hcc,
		status,
		HCCType,
		HCCWeight
		from RPT.HCC_list
		where
		root_companies_id = 159
    
  
		Group by 
			EMPi,
		hcc,
		status,
		HCCType,
		HCCWeight
	)t1
	Group by
		EMPI
)t2
Right outer Join ACG.PATIENT_SAMPLE on EMPI=patient_id


GO
