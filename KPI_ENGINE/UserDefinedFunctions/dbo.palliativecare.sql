SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON



CREATE FUNCTION [dbo].[palliativecare]
(
	@rootId varchar(10)
	,@startdate Date
	,@enddate Date
)
RETURNS TABLE AS
RETURN 
(
	select distinct 
		EMPI
		,ServiceDate
		,Code
		,DataSource
		,BILL_PROV
	from
	(

		Select 
			p.EMPI
			,p.PROC_START_DATE as ServiceDate
			,p.PROC_CODE as Code
			,p.PROC_DATA_SRC as DataSource
			,BILL_PROV
		from PROCEDURES p
		left outer join CLAIMLINE c on p.CLAIM_ID=c.CLAIM_ID and
									   ISNULL(p.SV_LINE,0)=ISNULL(c.SV_LINE,0) and
									   p.PROC_DATA_SRC=c.CL_DATA_SRC and
									   p.ROOT_COMPANIES_ID=c.ROOT_COMPANIES_ID and
									   p.EMPI=c.EMPI
		where p.ROOT_COMPANIES_ID=@rootId and 
			  ISNULL(c.POS,'')!='81' and
			  ISNULL(PROC_STATUS,'EVN')!='INT' and
			  PROC_START_DATE between @startdate and @enddate and 
			  PROC_CODE IN
			  (
				select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Palliative Care Assessment','Palliative Care Intervention','Palliative Care Encounter')
			  )

		Union all

		select 
			d.EMPI
			,d.DIAG_START_DATE as ServiceDate
			,d.DIAG_CODE as Code
			,d.DIAG_DATA_SRC as DataSource
			,BILL_PROV
		from DIAGNOSIS d
		left outer join CLAIMLINE c on d.CLAIM_ID=c.CLAIM_ID and
									   d.DIAG_DATA_SRC=c.CL_DATA_SRC and
									   d.ROOT_COMPANIES_ID=c.ROOT_COMPANIES_ID and
									   d.EMPI=c.EMPI
		where 
			d.ROOT_COMPANIES_ID=@rootId and 
			ISNULL(c.POS,'')!='81' and
			DIAG_START_DATE between @startdate and @enddate and 
			DIAG_CODE IN(select Code_New from HDS.VALUESET_TO_CODE where Value_Set_Name in('Palliative Care Assessment','Palliative Care Intervention','Palliative Care Encounter'))

		Union all

		Select
			EMPI
			,VisitDate as ServiceDate
			,visittypecode as Code
			,Visit_Data_Src as DataSource
			,null
		From Visit
		where
			ROOT_COMPANIES_ID=@rootId and
			VisitDate between @startDate and @endDate and
			visittypecode in
			(
				select code_new from HDS.VALUESET_TO_CODE where Value_Set_Name in('Palliative Care Assessment','Palliative Care Intervention','Palliative Care Encounter')
			)

		
	)t1
)

GO
