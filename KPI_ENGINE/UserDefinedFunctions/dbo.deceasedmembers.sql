SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
CREATE FUNCTION [dbo].[deceasedmembers]
/****** Change Log:
	2021-11-12 | KPISQL-523 | SG: Changed the MEM_DOD filter not to consider '1900-01-01' as the DoD
******/
(
	@rootId INT
	,@startdate Date
	,@enddate Date
)
RETURNS TABLE AS
RETURN 
(
	select distinct 
		EMPI 
	from
	(
	-- Members who were declared as dead at Discharge
		select
			EMPI
		from KPI_ENGINE.dbo.CLAIMLINE 
		where ROOT_COMPANIES_ID=@rootId and 
			  DIS_DATE!='' and 
			  COALESCE(DIS_DATE,TO_DATE) <= @enddate and 
			  ISNULL(pos,'')!='81'  and 
			  DIS_STAT='20'

		Union all
		-- Members marked as deceased
		Select
			EMPI
		From MEMBER
		where ROOT_COMPANIES_ID=@rootId and
			  _MEM_UDF_04_=1

		Union all
		-- Members marked as deceased
		Select
			EMPI
		From MEMBER
		where ROOT_COMPANIES_ID=@rootId 
			--and MEM_DOD<=@enddate
			and NULLIF(MEM_DOD,'1900-01-01')<=@enddate
--changed the filter and added NULLIF, NOT to consider '1900-01-01' as valid DoD
	)t1
)
GO
