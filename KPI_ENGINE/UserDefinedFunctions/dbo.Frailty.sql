SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON


CREATE FUNCTION [dbo].[Frailty]
(
	@rootId varchar(10)
	,@startdate Date
	,@enddate Date
)
RETURNS TABLE AS
RETURN 
(
	Select distinct
		EMPI
		,ServiceDate
		,Code
		,DataSource
		,BILL_PROV
	From
	(
		select   
			d.EMPI
			,d.DIAG_START_DATE as ServiceDate
			,DIAG_CODE as Code
			,DIAG_DATA_SRC as DataSource
			,c.BILL_PROV
		from DIAGNOSIS d
		left outer join CLAIMLINE c on d.CLAIM_ID=c.CLAIM_ID and
										   d.DIAG_DATA_SRC=c.CL_DATA_SRC and
										   d.ROOT_COMPANIES_ID=c.ROOT_COMPANIES_ID and
										   d.EMPI=c.EMPI
		where 
			d.ROOT_COMPANIES_ID=@rootId and 
			ISNULL(c.POS,'')!='81' and 
			d.DIAG_START_DATE between @startdate and @enddate	and 
			d.DIAG_CODE in
			(
				select Code_New from HDS.VALUESET_TO_CODE where Value_Set_Name in('Frailty Diagnosis','Frailty Symptom','Frailty Device','Frailty Encounter') and  Code_System in('ICD10CM','ICD9CM','SNOMED CT US Edition')
			)

		Union all

		select   
			p.EMPI
			,p.PROC_START_DATE as ServiceDate
			,p.PROC_CODE as Code
			,p.PROC_DATA_SRC as DataSource
			,c.BILL_PROV
		from PROCEDURES p
		left outer join CLAIMLINE c on p.CLAIM_ID=c.CLAIM_ID and
									   p.PROC_DATA_SRC=c.CL_DATA_SRC and
									   p.ROOT_COMPANIES_ID=c.ROOT_COMPANIES_ID and
									   ISNULL(p.SV_LINE,0)=ISNULL(c.SV_LINE,0) and
									   p.EMPI=c.EMPI
		where 
			p.ROOT_COMPANIES_ID=@rootId and 
			ISNULL(c.POS,'')!='81' and 
			ISNULL(PROC_STATUS,'EVN')!='INT' and
			p.PROC_START_DATE between @startdate and @enddate and 
			p.PROC_CODE in
			(
				select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Frailty Diagnosis','Frailty Symptom','Frailty Device','Frailty Encounter')
			)

		Union all

		Select
			EMPI
			,VisitDate as ServiceDate
			,VisitTypeCode as Code
			,Visit_Data_Src as DataSource
			,null
		From Visit
		Where
			ROOT_COMPANIES_ID=@rootId and
			VisitDate between @startdate and @enddate and 
			VisitTypeCode in
			(
				select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Frailty Diagnosis','Frailty Symptom','Frailty Device','Frailty Encounter')
			)
	)t1	
)

GO
