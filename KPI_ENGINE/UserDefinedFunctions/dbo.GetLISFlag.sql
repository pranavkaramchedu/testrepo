SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON



CREATE FUNCTION [dbo].[GetLISFlag]
(
	@rootId varchar(10)
	,@startdate Date
	,@enddate Date
)
RETURNS TABLE AS
RETURN 
(
	Select distinct
		EMPI
	From MCFIELDS
	Where
		ROOT_COMPANIES_ID=@rootId and
		(
			Low_Income_Period_End_Date between @startdate and @enddate
			or
			Low_Income_Period_End_Date is NULL
		)
		and
		RunDate is null
			   
)

GO
