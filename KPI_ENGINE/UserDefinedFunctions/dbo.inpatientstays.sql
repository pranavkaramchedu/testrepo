SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON



CREATE FUNCTION [dbo].[inpatientstays]
(
	@rootId varchar(20)
	,@startdate Date
	,@enddate Date
)
RETURNS TABLE AS
RETURN 
(
	Select distinct
		*
	from
	(
		select 
			EMPI
			,FROM_DATE
			,Coalesce(ADM_DATE,FROM_DATE) as ADM_DATE
			,Coalesce(DIS_DATE,TO_DATE) as DIS_DATE
			,CLAIM_ID
			,CL_DATA_SRC
			,SV_STAT
			,SV_LINE
			,ATT_NPI
			,BILL_PROV
			,BILL_PROV_NAME
			,REV_CODE
			,MS_DRG
		from CLAIMLINE 
		where 
			ROOT_COMPANIES_ID=@rootId and 
			ISNULL(POS,'')!='81' and 
			REV_CODE in
			(
				select code from HDS.VALUESET_TO_CODE where code_system='UBREV' and Value_Set_Name in('Inpatient Stay')
			)
	)t1
	Where 
		t1.DIS_DATE between @startdate and @enddate


)

GO
