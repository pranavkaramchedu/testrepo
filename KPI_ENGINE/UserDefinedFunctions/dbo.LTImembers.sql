SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

Create FUNCTION [dbo].[LTImembers]
(
	@rootId INT
	,@startdate Date
	,@enddate Date
)
RETURNS TABLE AS
RETURN 
(
		select distinct
			EMPI
		from KPI_ENGINE.dbo.MCFIELDS 
		where 
			ROOT_COMPANIES_ID=@rootId and 
			Rundate!='' and 
			Rundate between @startdate and @enddate and 
			LTI=1
				
	

)

GO
