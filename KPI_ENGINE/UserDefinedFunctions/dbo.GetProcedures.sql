SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON




CREATE FUNCTION [dbo].[GetProcedures]
(
	@rootId INT
	,@startdate Date
	,@enddate Date
	,@valuesetname varchar(300)
)
RETURNS TABLE AS
RETURN 
(
		select 
			p.EMPI
			,p.PROC_START_DATE
			,p.CLAIM_ID
			,p.PROC_CODE
			,PROC_DATA_SRC
			,p.SV_LINE
			,c.BILL_PROV
		from PROCEDURES p
		left outer join CLAIMLINE c on p.CLAIM_ID=c.CLAIM_ID and
									   ISNULL(p.SV_LINE,0)=ISNULL(c.SV_LINE,0) and
									   p.PROC_DATA_SRC=c.CL_DATA_SRC and
									   p.ROOT_COMPANIES_ID=c.ROOT_COMPANIES_ID and
									   p.EMPI=c.EMPI
		where 
			p.ROOT_COMPANIES_ID=@rootId and 
			ISNULL(c.POS,'')!='81' and 
			ISNULL(PROC_STATUS,'EVN')!='INT' and
			PROC_START_DATE between @startdate and @enddate	and
			PROC_CODE in
			(
				select Code from HDS.VALUESET_TO_CODE where Value_Set_Name in(SELECT value FROM STRING_SPLIT( @valuesetname, ','))
			)
	


)

GO
