SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON


CREATE FUNCTION [dbo].[observationstays]
(
	@rootId INT
	,@startdate Date
	,@enddate Date
)
RETURNS TABLE AS
RETURN 
(
	Select distinct
		*
	from
	(
		select 
			EMPI
			,FROM_DATE
			,Case
				when ADM_DATE is null Then FROM_DATE
				else ADM_DATE
			end as ADM_DATE
			,Case
				when DIS_DATE is null then TO_DATE
				else DIS_DATE
			End as DIS_DATE
			,CLAIM_ID
			,CL_DATA_SRC
			,SV_STAT
			,SV_LINE
			,REV_CODE
			,ATT_NPI
			,BILL_PROV
			,BILL_PROV_NAME
			,MS_DRG
		from CLAIMLINE 
		where 
			ROOT_COMPANIES_ID=@rootId and 
			ISNULL(POS,'')!='81' and 
			REV_CODE in
			(
				select code from HDS.VALUESET_TO_CODE where code_system='UBREV' and Value_Set_Name in('Observation Stay')
			)
	)t1
	Where 
		t1.DIS_DATE between @startdate and @enddate


)

GO
