SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
CREATE PROCEDURE [dbo].[LOAD_STAGING_TO_PROD_PROCEDURES]
AS
BEGIN

	DECLARE @INFO VARCHAR(8000)
					,	@TRUE BIT = 1
					,	@FALSE BIT = 0
					,	@DB_ID INT = DB_ID()
					,	@LOGID INT = 0
					,	@PROC VARCHAR(500)=OBJECT_SCHEMA_NAME(@@PROCID)+'.'+OBJECT_NAME(@@PROCID)
					,	@PERF_START DATETIME
					,	@PERF_DURATION INT
					,	@PERF_ROW INT
					,	@RC	INT
					;

		EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, @INFO=@INFO, @LOG2ID=@LOGID OUT;
		DECLARE @PROCFULLNAME VARCHAR(500) = DB_NAME()+'.'+@PROC;
		----EXEC dbo.SP_MI_UTIL_SEND_MAIL @PROCFULLNAME, 'STARTED', 1, @ECHO=0; 
	DECLARE @MSG VARCHAR(1000)
	DECLARE @PERF_START_PROC DATETIME 
	DECLARE @PERF_RPS NUMERIC(10,2) 
	DECLARE @ROWS INT 
	DECLARE @PERF_ROWS INT 
	SET @PERF_ROWS = 0 
	SET @PERF_START = GETDATE() 
	SET @PERF_START_PROC = GETDATE() 


BEGIN TRY

Declare @Error_Keys varchar(100);
SET @Error_Keys=NULL;

SET @Error_Keys=(Select Distinct TOP 1 PROC_DATA_SRC FROM [KPI_ENGINE].[dbo].[PROCEDURES] M
LEFT JOIN (Select Distinct PAYER  FROM [dbo].[STAGING_TO_PROD_DEDUP_KEY] WHERE PROD_TABLE='PROCEDURES') DEDUPKEY
ON PROC_DATA_SRC=DEDUPKEY.PAYER 
where PAYER is NULL)

IF @Error_Keys IS NOT NULL
RAISERROR ('CREATE THE DEDUP KEYS FOR PROCEDURES: Select Distinct TOP 1 PROC_DATA_SRC FROM [KPI_ENGINE].[dbo].[PROCEDURES] M
LEFT JOIN (Select Distinct PAYER  FROM [dbo].[STAGING_TO_PROD_DEDUP_KEY] WHERE PROD_TABLE=''PROCEDURES'') DEDUPKEY
ON PROC_DATA_SRC=DEDUPKEY.PAYER 
where PAYER is NULL',16,1);


DEclare @PROD_TABLE varchar(100)
Declare @PAYER varchar(100)
Declare @ROOT_COMPANIES_ID int
Declare @KEYS varchar(500)
DECLARE @SQL VARCHAR(MAX)
DECLARE C CURSOR LOCAL FOR 
select
    PROD_TABLE
	,PAYER
	,ROOT_COMPANIES_ID
    ,stuff((
        select ' AND ' + CASE 
	WHEN KEY_COLUMNS_TYPE='VARCHAR' THEN 'ISNULL(P.'+KEY_COLUMNS+','''')' 
	WHEN KEY_COLUMNS_TYPE='INT' THEN 'ISNULL(P.'+KEY_COLUMNS+',''0'')' 
	WHEN KEY_COLUMNS_TYPE='DATE' THEN 'ISNULL(P.'+KEY_COLUMNS+',''1900-01-01'')' 
END+'='+CASE 
	WHEN KEY_COLUMNS_TYPE='VARCHAR' THEN 'ISNULL(PP.'+KEY_COLUMNS+','''')' 
	WHEN KEY_COLUMNS_TYPE='INT' THEN 'ISNULL(PP.'+KEY_COLUMNS+',''0'')' 
	WHEN KEY_COLUMNS_TYPE='DATE' THEN 'ISNULL(PP.'+KEY_COLUMNS+',''1900-01-01'')' 
END
        from [dbo].[STAGING_TO_PROD_DEDUP_KEY] t
        where t.PROD_TABLE = t1.PROD_TABLE AND t.PAYER = t1.PAYER AND t.ROOT_COMPANIES_ID = t1.ROOT_COMPANIES_ID
        order by t.KEY_COLUMNS
        for xml path('')
    ),1,4,'') as name_csv
from [dbo].[STAGING_TO_PROD_DEDUP_KEY] t1
where t1.PROD_TABLE='PROCEDURES'
group by 
PROD_TABLE
,PAYER
,ROOT_COMPANIES_ID

OPEN C  
FETCH NEXT FROM C INTO @PROD_TABLE,@PAYER,@ROOT_COMPANIES_ID,@KEYS

WHILE @@FETCH_STATUS = 0  
BEGIN  

SET @SQL='
UPDATE [KPI_ENGINE].[dbo].[PROCEDURES]
SET
		 [ST_PROC_ID]=LTRIM(RTRIM(P.[ST_PROC_ID]))
		,[CLAIM_ID]=LTRIM(RTRIM(P.[CLAIM_ID]))
		,[SV_LINE]=LTRIM(RTRIM(P.[SV_LINE]))
		,[MEMBER_ID]=LTRIM(RTRIM(P.[MEMBER_ID]))
		,EMPI=LTRIM(RTRIM(EMPI.EMPI_ID))
		,[PROC_START_DATE]=LTRIM(RTRIM(P.[PROC_START_DATE]))
		,[PROC_END_DATE]=LTRIM(RTRIM(P.[PROC_END_DATE]))
		,[PROC_CODE]=LTRIM(RTRIM(P.[PROC_CODE]))
		,[PROC_DESC]=LTRIM(RTRIM(P.[PROC_DESC]))
		,[PROC_CODE_TYPE]=LTRIM(RTRIM(P.[PROC_CODE_TYPE]))
		,[MOD_1]=LTRIM(RTRIM(P.[MOD_1]))
		,[MOD_2]=LTRIM(RTRIM(P.[MOD_2]))
		,[MOD_3]=LTRIM(RTRIM(P.[MOD_3]))
		,[MOD_4]=LTRIM(RTRIM(P.[MOD_4]))
		,[SERVICE_UNIT]=LTRIM(RTRIM(P.[SERVICE_UNIT]))
		,[ICDPCS_CODE]=LTRIM(RTRIM(P.[ICDPCS_CODE]))
		,[PROC_DATA_SRC]=LTRIM(RTRIM(P.[PROC_DATA_SRC]))
		,[ROOT_COMPANIES_ID]=LTRIM(RTRIM(P.[ROOT_COMPANIES_ID]))
		,[LOAD_DATE_TIME]=LTRIM(RTRIM(P.[LOAD_DATE_TIME]))
		,[Filename]=LTRIM(RTRIM(P.[Filename]))
		,[FileDate]=LTRIM(RTRIM(P.[FileDate]))
		,[PROC_SEQ]=LTRIM(RTRIM(P.[PROC_SEQ]))
FROM [KPI_ENGINE_STAGING].[dbo].[STAGING_PROCEDURES] P
INNER JOIN KPI_ENGINE.dbo.Open_empi EMPI 
on P.MEMBER_ID=EMPI.Org_Patient_Extension_ID AND (P.PROC_DATA_SRC=EMPI.[Data_Source] or P.PROC_DATA_SRC in(''FlatFiles-Gaps'',''FlatFiles'')) AND P.ROOT_COMPANIES_ID=EMPI.Root_Companies_ID
LEFT JOIN KPI_ENGINE.dbo.[PROCEDURES] PP
ON '+@KEYS+
' WHERE PP.CLAIM_ID IS NOT NULL AND P.PROC_DATA_SRC='''+@PAYER+''' AND P.Root_Companies_ID='+CONVERT(VARCHAR(10),@ROOT_COMPANIES_ID)


Declare @updatecount INT;
EXECUTE (@SQL);
SET @updatecount=@@ROWCOUNT;



SET @SQL='
INSERT INTO [KPI_ENGINE].[dbo].[PROCEDURES]
(
	   [ST_PROC_ID]
      ,[CLAIM_ID]
      ,[SV_LINE]
      ,[MEMBER_ID]
	  ,EMPI
      ,[PROC_START_DATE]
      ,[PROC_END_DATE]
      ,[PROC_CODE]
      ,[PROC_DESC]
      ,[PROC_CODE_TYPE]
      ,[MOD_1]
      ,[MOD_2]
      ,[MOD_3]
      ,[MOD_4]
      ,[SERVICE_UNIT]
      ,[ICDPCS_CODE]
      ,[PROC_DATA_SRC]
      ,[ROOT_COMPANIES_ID]
      ,[LOAD_DATE_TIME]
      ,[Filename]
      ,[FileDate]
      ,[PROC_SEQ]
)
SELECT
	LTRIM(RTRIM(P.[ST_PROC_ID]))
	,LTRIM(RTRIM(P.[CLAIM_ID]))
	,LTRIM(RTRIM(P.[SV_LINE]))
	,LTRIM(RTRIM(P.[MEMBER_ID]))
	,LTRIM(RTRIM(EMPI.EMPI_ID))
	,LTRIM(RTRIM(P.[PROC_START_DATE]))
	,LTRIM(RTRIM(P.[PROC_END_DATE]))
	,LTRIM(RTRIM(P.[PROC_CODE]))
	,LTRIM(RTRIM(P.[PROC_DESC]))
	,LTRIM(RTRIM(P.[PROC_CODE_TYPE]))
	,LTRIM(RTRIM(P.[MOD_1]))
	,LTRIM(RTRIM(P.[MOD_2]))
	,LTRIM(RTRIM(P.[MOD_3]))
	,LTRIM(RTRIM(P.[MOD_4]))
	,LTRIM(RTRIM(P.[SERVICE_UNIT]))
	,LTRIM(RTRIM(P.[ICDPCS_CODE]))
	,LTRIM(RTRIM(P.[PROC_DATA_SRC]))
	,LTRIM(RTRIM(P.[ROOT_COMPANIES_ID]))
	,LTRIM(RTRIM(P.[LOAD_DATE_TIME]))
	,LTRIM(RTRIM(P.[Filename]))
	,LTRIM(RTRIM(P.[FileDate]))
	,LTRIM(RTRIM(P.[PROC_SEQ]))
FROM [KPI_ENGINE_STAGING].[dbo].[STAGING_PROCEDURES] P
INNER JOIN KPI_ENGINE.dbo.Open_empi EMPI 
on P.MEMBER_ID=EMPI.Org_Patient_Extension_ID AND (P.PROC_DATA_SRC=EMPI.[Data_Source] or P.PROC_DATA_SRC in(''FlatFiles-Gaps'',''FlatFiles'')) AND P.ROOT_COMPANIES_ID=EMPI.Root_Companies_ID
LEFT JOIN KPI_ENGINE.dbo.[PROCEDURES] PP
ON '+@KEYS+
' WHERE PP.CLAIM_ID IS NULL AND P.PROC_DATA_SRC='''+@PAYER+''' AND P.Root_Companies_ID='+CONVERT(VARCHAR(10),@ROOT_COMPANIES_ID)

Declare @insertcount INT;
EXECUTE (@SQL);
SET @insertcount=@@ROWCOUNT;



SET @SQL='
INSERT INTO KPI_ENGINE_STAGING.dbo.STAGING_PROCEDURES_ERROR
(
	   [ST_PROC_ID]
      ,[CLAIM_ID]
      ,[SV_LINE]
      ,[MEMBER_ID]
      ,[PROC_START_DATE]
      ,[PROC_END_DATE]
      ,[PROC_CODE]
      ,[PROC_DESC]
      ,[PROC_CODE_TYPE]
      ,[MOD_1]
      ,[MOD_2]
      ,[MOD_3]
      ,[MOD_4]
      ,[SERVICE_UNIT]
      ,[ICDPCS_CODE]
      ,[PROC_DATA_SRC]
      ,[ROOT_COMPANIES_ID]
      ,[LOAD_DATE_TIME]
      ,[Filename]
      ,[FileDate]
      ,[PROC_SEQ]
)
SELECT
	LTRIM(RTRIM(P.[ST_PROC_ID]))
	,LTRIM(RTRIM(P.[CLAIM_ID]))
	,LTRIM(RTRIM(P.[SV_LINE]))
	,LTRIM(RTRIM(P.[MEMBER_ID]))
	,LTRIM(RTRIM(P.[PROC_START_DATE]))
	,LTRIM(RTRIM(P.[PROC_END_DATE]))
	,LTRIM(RTRIM(P.[PROC_CODE]))
	,LTRIM(RTRIM(P.[PROC_DESC]))
	,LTRIM(RTRIM(P.[PROC_CODE_TYPE]))
	,LTRIM(RTRIM(P.[MOD_1]))
	,LTRIM(RTRIM(P.[MOD_2]))
	,LTRIM(RTRIM(P.[MOD_3]))
	,LTRIM(RTRIM(P.[MOD_4]))
	,LTRIM(RTRIM(P.[SERVICE_UNIT]))
	,LTRIM(RTRIM(P.[ICDPCS_CODE]))
	,LTRIM(RTRIM(P.[PROC_DATA_SRC]))
	,LTRIM(RTRIM(P.[ROOT_COMPANIES_ID]))
	,LTRIM(RTRIM(P.[LOAD_DATE_TIME]))
	,LTRIM(RTRIM(P.[Filename]))
	,LTRIM(RTRIM(P.[FileDate]))
	,LTRIM(RTRIM(P.[PROC_SEQ]))
FROM [KPI_ENGINE_STAGING].[dbo].[STAGING_PROCEDURES] P
LEFT JOIN KPI_ENGINE.dbo.Open_empi EMPI 
on P.MEMBER_ID=EMPI.Org_Patient_Extension_ID AND (P.PROC_DATA_SRC=EMPI.[Data_Source] or P.PROC_DATA_SRC in(''FlatFiles-Gaps'',''FlatFiles'')) AND P.ROOT_COMPANIES_ID=EMPI.Root_Companies_ID
LEFT JOIN KPI_ENGINE_STAGING.dbo.STAGING_PROCEDURES_ERROR PP
ON '+@KEYS+
' WHERE PP.CLAIM_ID IS NULL AND EMPI.EMPI_ID IS NULL AND P.PROC_DATA_SRC='''+@PAYER+''' AND P.Root_Companies_ID='+CONVERT(VARCHAR(10),@ROOT_COMPANIES_ID)

Declare @errorcount INT;
EXECUTE (@SQL);
Set @errorcount=@@ROWCOUNT


-- Shanawaz 10-21-2021 -- Adding this  for data flow trace
Insert into KPI_ENGINE_RECON.dbo.PROD_INGESTION_STATS(Dimension,Source,UpdateCount,InsertCount,ErrorCount) values(@PROD_TABLE,@PAYER,@updatecount,@insertcount,@errorcount);


FETCH NEXT FROM C INTO @PROD_TABLE,@PAYER,@ROOT_COMPANIES_ID,@KEYS
END 

CLOSE C  
DEALLOCATE C

			SET @PERF_ROW = @@ROWCOUNT
		SET @PERF_DURATION = DATEDIFF(MINUTE,@PERF_START,GETDATE());
		EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, 'Enrollment Completed', @PERF_ROW, @DURATION_IN_MIN=@PERF_DURATION, @LOG2ID=@LOGID;
		EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, @LOG2ID=@LOGID, @END_FLAG=1;


END TRY
BEGIN CATCH  
    DECLARE @ErrorMessage NVARCHAR(4000);  
    DECLARE @ErrorSeverity INT;  
    DECLARE @ErrorState INT;  

	EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, 'FATAL ERROR', @LOG2ID=@LOGID;
  
    SELECT   
        @ErrorMessage = ERROR_MESSAGE(),  
        @ErrorSeverity = ERROR_SEVERITY(),  
        @ErrorState = ERROR_STATE();  
  
    -- Use RAISERROR inside the CATCH block to return error  
    -- information about the original error that caused  
    -- execution to jump to the CATCH block.  
    RAISERROR (@ErrorMessage, -- Message text.  
               @ErrorSeverity, -- Severity.  
               @ErrorState -- State.  
               );  
END CATCH;  

END
GO
