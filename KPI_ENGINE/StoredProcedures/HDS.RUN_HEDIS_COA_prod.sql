SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
CREATE proc [HDS].[RUN_HEDIS_COA_prod] @meas_year nvarchar(4)
AS


-- Declare Variables

Declare @meas varchar(10)='159'---'COA';
Declare @ce_startdt varchar(100);

Declare @ce_startdt1 varchar(10);
Declare @ce_novdt varchar(10);
Declare @ce_enddt varchar(10);
Declare @ce_enddt1 varchar(10);
DECLARE @pt_ctr Int=0;
DECLARE @i INT=0;
declare @patientid varchar(100);
declare @plan_ct INT;
declare @gender varchar(10);
declare @age INT;
Declare @latest_insenddate varchar(10);
Declare @plan1 varchar(20);
Declare @plan2 varchar(20);
Declare @planid varchar(20);
Declare @runid INT;


-- Set Measure dates
SET @ce_startdt=concat(@meas_year,'-01-01');
SET @ce_novdt=concat(@meas_year,'-11-30');

SET @ce_enddt=concat(@meas_year,'-12-31');
SET @ce_startdt1=concat(@meas_year-1,'-01-01');
SET @ce_enddt1=concat(@meas_year-1,'-12-31');


-- Create artdataset
drop table if exists #coadataset;

CREATE TABLE #coadataset (
  [patient_id] varchar(100) NOT NULL,
  [meas] varchar(20) DEFAULT NULL,
  [payer] varchar(100) DEFAULT NULL,
  [patient_gender] varchar(45) NOT NULL,
  [age] decimal(12,4) DEFAULT NULL,
  [orec] smallint DEFAULT NULL,
  [lis] smallint DEFAULT '0',
  [rexcl] smallint DEFAULT '0',
  [rexcld] smallint DEFAULT '0',
  [CE] smallint DEFAULT '0',
  [excl] smallint DEFAULT '0',
  [num] smallint DEFAULT '0',
  [Event] smallint DEFAULT '0'
    
) ;



-- Eligible Patient List

drop table if exists #coa_memlist; 

CREATE TABLE #coa_memlist (
    memid varchar(100)
    
);


insert into #coa_memlist
SELECT DISTINCT en.MemID FROM HDS.HEDIS_MEMBER_GM gm
join hds.HEDIS_MEMBER_EN en on en.MemID=gm.MemID and en.MEASURE_ID=gm.MEASURE_ID
WHERE en.MEASURE_ID=@meas and
en.StartDate<=@ce_enddt AND FinishDate>=@ce_startdt AND 
YEAR(@ce_enddt)-YEAR(DOB) >=66 ORDER BY 1;


CREATE CLUSTERED INDEX ix_memlist_memid ON #coa_memlist ([memid]);



-- Create Temp Patient Enrollment
drop table if exists #coa_tmpsubscriber;

CREATE TABLE #coa_tmpsubscriber (
    memid varchar(100),
    dob varchar(10),
	gender varchar(1),
	payer varchar(50),
	StartDate varchar(10),
	EndDate varchar(10),
);



insert into #coa_tmpsubscriber
SELECT en.MemID,gm.DOB,gm.Gender,en.Payer,en.StartDate,en.FinishDate  FROM HDS.HEDIS_MEMBER_GM gm
join hds.HEDIS_MEMBER_EN en on en.MemID=gm.MemID and en.MEASURE_ID=gm.MEASURE_ID
WHERE en.MEASURE_ID=@meas and
en.StartDate<=@ce_enddt AND FinishDate>=@ce_startdt
AND YEAR(@ce_enddt)-YEAR(DOB) >=66 ORDER BY en.MemID,en.StartDate,en.FinishDate;

CREATE CLUSTERED INDEX ix_tmpsub_memid ON #coa_tmpsubscriber ([memid],[StartDate],[EndDate]);




-- Get Patient COUNT
SELECT @pt_ctr=COUNT(*)  FROM #coa_memlist;


-- Loop through enrollment for each patient

While @i<@pt_ctr
	BEGIN
		-- Get Patient to Loop
		SELECT  @patientid=memid FROM #coa_memlist ORDER BY memid ASC OFFSET  @i ROWS FETCH NEXT 1 ROWS ONLY ;
		
		-- ReadingGender and age
		SELECT Top 1 @gender=gender,@age=Year(@ce_enddt)-Year(dob) FROM #coa_tmpsubscriber WHERE memid=@patientid;

		
		-- Check for dual eligibility by checking if the patient is enrolled in more than one plan at the end of Enrollment
		
		SELECT Top 1 @latest_insenddate=EndDate FROM #coa_tmpsubscriber WHERE  memid=@patientid  AND StartDate<=@ce_enddt ORDER BY StartDate DESC,EndDate Desc  ;
		
		-- Check for plan count patient is enrolled in during the end of measurement year
		
		SELECT @plan_ct=COUNT(*) FROM #coa_tmpsubscriber WHERE  memid=@patientid AND @latest_insenddate BETWEEN StartDate AND EndDate;
		
		-- If only one plan, then skip the Payer reporting logic
		
		if(@plan_ct=1)
		BEGIN
			
			SELECT @planid=payer FROM #coa_tmpsubscriber WHERE  memid=@patientid AND @latest_insenddate BETWEEN StartDate AND EndDate ORDER BY payer;
		
			-- If plan is Dual , Insert MCR & MCD	
				
			If(@planid in('MMP','SN3'))
			BEGIN
				
				
				Insert INTO #coadataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'COA1',@planid,@age,@gender);
				Insert INTO #coadataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'COA2',@planid,@age,@gender);
				Insert INTO #coadataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'COA3',@planid,@age,@gender);
				Insert INTO #coadataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'COA4',@planid,@age,@gender);
				
				
				
			END
			
			Else if(@planid IN('MDE'))
			Begin
				SET @planid='MCD';
				Insert INTO #coadataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'COA1',@planid,@age,@gender);
				Insert INTO #coadataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'COA2',@planid,@age,@gender);
				Insert INTO #coadataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'COA3',@planid,@age,@gender);
				Insert INTO #coadataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'COA4',@planid,@age,@gender);
				
				SET @planid='MCR';
				Insert INTO #coadataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'COA1',@planid,@age,@gender);
				Insert INTO #coadataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'COA2',@planid,@age,@gender);
				Insert INTO #coadataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'COA3',@planid,@age,@gender);
				Insert INTO #coadataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'COA4',@planid,@age,@gender);
				
			END
			Else if(@planid IN('MD','MLI','MRB','MDE'))
			Begin
				SET @planid='MCD';
				Insert INTO #coadataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'COA1',@planid,@age,@gender);
				Insert INTO #coadataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'COA2',@planid,@age,@gender);
				Insert INTO #coadataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'COA3',@planid,@age,@gender);
				Insert INTO #coadataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'COA4',@planid,@age,@gender);
				
			END
			
			Else If(@planid IN('SN1','SN2'))
			Begin
				Insert INTO #coadataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'COA1',@planid,@age,@gender);
				Insert INTO #coadataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'COA2',@planid,@age,@gender);
				Insert INTO #coadataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'COA3',@planid,@age,@gender);
				Insert INTO #coadataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'COA4',@planid,@age,@gender);
				
				
				
			END
			Else if(@planid IN('MEP','MMO','MOS','MPO'))
			BEGIN
				
				Insert INTO #coadataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'COA1',@planid,@age,@gender);
				Insert INTO #coadataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'COA2',@planid,@age,@gender);
				Insert INTO #coadataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'COA3',@planid,@age,@gender);
				Insert INTO #coadataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'COA4',@planid,@age,@gender);
				
				
			END
			ELSE
			BEGIN
				Insert INTO #coadataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'COA1',@planid,@age,@gender);
				Insert INTO #coadataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'COA2',@planid,@age,@gender);
				Insert INTO #coadataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'COA3',@planid,@age,@gender);
				Insert INTO #coadataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'COA4',@planid,@age,@gender);
				
			END
		END
				
		-- Compare Plans if Plan count is more than one	
		if(@plan_ct>1)
		BEGIN
			
			-- Read First Plan
			SELECT  @plan1=payer FROM #coa_tmpsubscriber where memid=@patientid and @latest_insenddate between StartDate and EndDate order by 1 OFFSET  0 ROWS FETCH NEXT 1 ROWS ONLY ;
					
			-- Read second plan
			SELECT  @plan2=payer FROM #coa_tmpsubscriber where memid=@patientid and @latest_insenddate between StartDate and EndDate order by 1 OFFSET  1 ROWS FETCH NEXT 1 ROWS ONLY ;
			
			
			
			If(@plan1 IN('MCR','MP','MC','SN1','SN2','MCS') and @plan2 IN('PPO','POS','HMO','CEP'))
			BEGIN
				if(@plan1 IN('SN1','SN2'))
				BEGIN
					
					Set @planid=@plan1;
					Insert INTO #coadataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'COA1',@planid,@age,@gender);
					Insert INTO #coadataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'COA2',@planid,@age,@gender);
					Insert INTO #coadataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'COA3',@planid,@age,@gender);
					Insert INTO #coadataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'COA4',@planid,@age,@gender);
										
				END
				
				if(@plan1 in ('MCR','MP','MC','MCS'))
				Begin
					
					Set @planid=@plan1;
					Insert INTO #coadataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'COA1',@planid,@age,@gender);
					Insert INTO #coadataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'COA2',@planid,@age,@gender);
					Insert INTO #coadataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'COA3',@planid,@age,@gender);
					Insert INTO #coadataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'COA4',@planid,@age,@gender);

				
				END

			END
			Else if((@plan2 in('MCR') and @plan1 in('MDE')) or (@plan1 in('MCR') and @plan2 in('MDE')))
			Begin
					Set @planid='MCR';
					Insert INTO #coadataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'COA1',@planid,@age,@gender);
					Insert INTO #coadataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'COA2',@planid,@age,@gender);
					Insert INTO #coadataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'COA3',@planid,@age,@gender);
					Insert INTO #coadataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'COA4',@planid,@age,@gender);

					Set @planid='MCD';
					Insert INTO #coadataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'COA1',@planid,@age,@gender);
					Insert INTO #coadataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'COA2',@planid,@age,@gender);
					Insert INTO #coadataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'COA3',@planid,@age,@gender);
					Insert INTO #coadataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'COA4',@planid,@age,@gender);
					
					
			End
			Else If(@plan2 IN('MCR','MP','MC','SN1','SN2','MCS') AND @plan1 IN('PPO','POS','HMO','CEP'))
			BEGIN
			
				IF(@plan2 IN('SN1','SN2'))
				Begin
					Set @planid=@plan2;
					Insert INTO #coadataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'COA1',@planid,@age,@gender);
					Insert INTO #coadataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'COA2',@planid,@age,@gender);
					Insert INTO #coadataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'COA3',@planid,@age,@gender);
					Insert INTO #coadataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'COA4',@planid,@age,@gender);
									
				END
				
				
				IF(@plan2 IN('MCR','MP','MC','MCS'))
				BEGIN
					
					Set @planid=@plan2;
					
					Insert INTO #coadataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'COA1',@planid,@age,@gender);
					Insert INTO #coadataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'COA2',@planid,@age,@gender);
					Insert INTO #coadataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'COA3',@planid,@age,@gender);
					Insert INTO #coadataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'COA4',@planid,@age,@gender);
					
				END
			END
			
			ELSE IF(@plan1 IN('PPO','POS','HMO','CEP') AND @plan2 IN('MD','MLI','MRB'))
			BEGIN
			
				Set @planid=@plan1;
				Insert INTO #coadataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'COA1',@planid,@age,@gender);
				Insert INTO #coadataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'COA2',@planid,@age,@gender);
				Insert INTO #coadataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'COA3',@planid,@age,@gender);
				Insert INTO #coadataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'COA4',@planid,@age,@gender);
				
			END
			
			ELSE IF(@plan2 IN('PPO','POS','HMO','CEP') AND @plan1 IN('MD','MLI','MRB'))
			BEGIN
			
				Set @planid=@plan2;
				Insert INTO #coadataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'COA1',@planid,@age,@gender);
				Insert INTO #coadataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'COA2',@planid,@age,@gender);
				Insert INTO #coadataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'COA3',@planid,@age,@gender);
				Insert INTO #coadataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'COA4',@planid,@age,@gender);
				
			END
			
			ELSE IF(@plan1 IN('MDE','SN3','MMP') or @plan2 in('MDE','SN3','MMP'))
			BEGIN
				
				if(@plan1 IN('SN3','MMP'))
				Begin
					Set @planid=@plan1;
					Insert INTO #coadataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'COA1',@planid,@age,@gender);
					Insert INTO #coadataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'COA2',@planid,@age,@gender);
					Insert INTO #coadataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'COA3',@planid,@age,@gender);
					Insert INTO #coadataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'COA4',@planid,@age,@gender);
				
				END
				if(@plan2 IN('SN3','MMP'))
				Begin
					Set @planid=@plan2;
					Insert INTO #coadataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'COA1',@planid,@age,@gender);
					Insert INTO #coadataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'COA2',@planid,@age,@gender);
					Insert INTO #coadataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'COA3',@planid,@age,@gender);
					Insert INTO #coadataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'COA4',@planid,@age,@gender);
				
				END
				
							
			END
			
			ELSE
			BEGIN
			
				Set @planid=@plan1;	
				Insert INTO #coadataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'COA1',@planid,@age,@gender);
				Insert INTO #coadataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'COA2',@planid,@age,@gender);
				Insert INTO #coadataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'COA3',@planid,@age,@gender);
				Insert INTO #coadataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'COA4',@planid,@age,@gender);
				
				Set @planid=@plan2;
				Insert INTO #coadataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'COA1',@planid,@age,@gender);
				Insert INTO #coadataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'COA2',@planid,@age,@gender);
				Insert INTO #coadataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'COA3',@planid,@age,@gender);
				Insert INTO #coadataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'COA4',@planid,@age,@gender);
				
				
			END
			
		END
		
		Set @i=@i+1;
		
	END;
-- Create Indices on coadataset
CREATE INDEX [idx1] ON #coadataset ([payer]);
CREATE INDEX [patient_id_UNIQUE] ON #coadataset ([patient_id],[payer]);

-- Continuous Enrollment



	
DROP TABLE IF EXISTS #coa_contenroll;
	
	CREATE table #coa_contenroll
	(
		Memid varchar(100)
		
	);


With coverage_CTE (Memid,lastcoveragedate,Startdate,Finishdate,nextcoveragedate) as
(
select Memid,isnull(lag(Finishdate,1) over(partition by Memid order by Startdate,FinishDate desc),@ce_startdt) as lastcoveragedate,Case when StartDate<@ce_startdt then @ce_startdt else startdate end as Startdate,case when Finishdate>@ce_enddt then @ce_enddt else finishdate end as Finishdate,isnull(lead(Startdate,1) over(partition by Memid order by Startdate,FinishDate),@ce_enddt) as nextcoveragedate  from hds.hedis_member_en where measure_id=@meas and Startdate<=@ce_enddt and Finishdate>=@ce_startdt  
)
Insert into #coa_contenroll
select Memid from(
select *, case when rn=1 and startdate>@ce_startdt then 1 else 0 end as startgap,case when datediff(day,newFinishdate,nextcoveragedate)>0 then 1  else 0 end as gaps,datediff(day,Startdate,newfinishdate)+1 as coveragedays,case when @ce_enddt between Startdate and newfinishdate then 1 else 0 end as anchor from(
Select *,case when datediff(day,Finishdate,nextcoveragedate) <=1 then isnull(lead(Finishdate,1) over(partition by Memid order by Startdate,FinishDate),@ce_enddt) else finishdate end as newfinishdate,ROW_NUMBER() over(partition by memid order by Startdate,Finishdate) as rn from coverage_CTE
)t1           
)t2 
group by memid having(sum(gaps)+sum(startgap)<=1) and sum(coveragedays)>=(DATEPART(dy, @ce_enddt)-45) and sum(anchor)>0

CREATE CLUSTERED INDEX ix_cdc_contenroll ON #coa_contenroll ([memid]);

update #coadataset set CE=1 from #coadataset ds join #coa_contenroll ce on ds.patient_id=ce.MemID;
	

-- Numerator Conditions
-- Advance Care Planning




DROP TABLE IF EXISTS #coa_acplist;
	
CREATE table #coa_acplist
(
	Memid varchar(100)
);

Insert into #coa_acplist
select distinct memid from(
select Memid from hds.HEDIS_VISIT where measure_id=@meas and date_s!='' and date_s between @ce_startdt and @ce_enddt and hcfapos!='81'
and(
CPT in (select code_new from hds.VALUESET_TO_CODE where Value_Set_Name='Advance Care Planning')
or
HCPCS in (select code_new from hds.VALUESET_TO_CODE where Value_Set_Name='Advance Care Planning')
or
Diag_i_1 in (select code_new from hds.VALUESET_TO_CODE where Value_Set_Name='Advance Care Planning')
or
Diag_i_2 in (select code_new from hds.VALUESET_TO_CODE where Value_Set_Name='Advance Care Planning')
or
Diag_i_3 in (select code_new from hds.VALUESET_TO_CODE where Value_Set_Name='Advance Care Planning')
or
Diag_i_4 in (select code_new from hds.VALUESET_TO_CODE where Value_Set_Name='Advance Care Planning')
or
Diag_i_5 in (select code_new from hds.VALUESET_TO_CODE where Value_Set_Name='Advance Care Planning')
or
Diag_i_6 in (select code_new from hds.VALUESET_TO_CODE where Value_Set_Name='Advance Care Planning')
or
Diag_i_7 in (select code_new from hds.VALUESET_TO_CODE where Value_Set_Name='Advance Care Planning')
or
Diag_i_8 in (select code_new from hds.VALUESET_TO_CODE where Value_Set_Name='Advance Care Planning')
or
Diag_i_9 in (select code_new from hds.VALUESET_TO_CODE where Value_Set_Name='Advance Care Planning')
or
Diag_i_10 in (select code_new from hds.VALUESET_TO_CODE where Value_Set_Name='Advance Care Planning')
)

Union all

select Memid from hds.HEDIS_VISIT where measure_id=@meas and date_s!='' and date_s between @ce_startdt and @ce_enddt and hcfapos!='81'
and CPT2 in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name='Advance Care Planning') and CPT2Mod not in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name='CPT CAT II Modifier')

Union all

Select Memid from hds.hedis_diag where measure_id=@meas and sdate!='' and sdate between @ce_startdt and @ce_enddt and dcode in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name='Advance Care Planning')

Union all

Select Memid from hds.hedis_proc where measure_id=@meas and sdate!='' and sdate between @ce_startdt and @ce_enddt and pstatus='EVN' and pcode in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name='Advance Care Planning')
)t1

CREATE CLUSTERED INDEX idx_coa_acplist ON #coa_acplist ([memid]);

update #coadataset set num=1 from #coadataset ds join #coa_acplist ce on ds.patient_id=ce.MemID and ds.meas='COA1'


-- Medication Review



DROP TABLE IF EXISTS #coa_medicationreviewlist;
	
CREATE table #coa_medicationreviewlist
(
	Memid varchar(100),
	claimid varchar(100)
);

Insert into #coa_medicationreviewlist
select distinct memid,claimid from(
select Memid,claimid from hds.HEDIS_VISIT where measure_id=@meas and date_s!='' and date_s between @ce_startdt and @ce_enddt and hcfapos!='81'
and CPT in (select code_new from hds.VALUESET_TO_CODE where Value_Set_Name='Medication Review') and provid in(select provid from hds.hedis_provider where measure_id=@meas and (ProvPres='Y' or PHAProv='Y'))

Union all

select Memid,ClaimID from hds.HEDIS_VISIT where measure_id=@meas and date_s!='' and date_s between @ce_startdt and @ce_enddt and hcfapos!='81'
and CPT2 in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name='Medication Review') and CPT2Mod not in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name='CPT CAT II Modifier') and provid in(select provid from hds.hedis_provider where measure_id=@meas and (ProvPres='Y' or PHAProv='Y'))
)t1

CREATE CLUSTERED INDEX idx_coa_acplist ON #coa_medicationreviewlist ([memid],[claimid]);


DROP TABLE IF EXISTS #coa_medicationlist;
	
CREATE table #coa_medicationlist
(
	Memid varchar(100),
	claimid varchar(100)
);

Insert into #coa_medicationlist
select distinct memid,claimid from(
select Memid,claimid from hds.HEDIS_VISIT where measure_id=@meas and date_s!='' and date_s between @ce_startdt and @ce_enddt and hcfapos!='81'
and 
(
CPT in (select code_new from hds.VALUESET_TO_CODE where Value_Set_Name='Medication List') 
or
HCPCS in (select code_new from hds.VALUESET_TO_CODE where Value_Set_Name='Medication List') 
)
and provid in(select provid from hds.hedis_provider where measure_id=@meas and (ProvPres='Y' or PHAProv='Y'))

Union all

select Memid,ClaimID from hds.HEDIS_VISIT where measure_id=@meas and date_s!='' and date_s between @ce_startdt and @ce_enddt and hcfapos!='81'
and CPT2 in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name='Medication List') and CPT2Mod not in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name='CPT CAT II Modifier') and provid in(select provid from hds.hedis_provider where measure_id=@meas and (ProvPres='Y' or PHAProv='Y'))
)t1

CREATE CLUSTERED INDEX idx_coa_acplist ON #coa_medicationlist ([memid],[claimid]);


drop table if exists #coa_TCMSlist
CREATE table #coa_TCMSlist
(
	Memid varchar(100),
	claimid varchar(100)
);


Insert into #coa_TCMSlist
select Memid,claimid from hds.HEDIS_VISIT where measure_id=@meas and date_s!='' and date_s between @ce_startdt and @ce_enddt and hcfapos!='81'
and CPT in (select code_new from hds.VALUESET_TO_CODE where Value_Set_Name='Transitional Care Management Services') 

CREATE CLUSTERED INDEX idx_coa_TCMSlist ON #coa_TCMSlist ([memid],[claimid]);



drop table if exists #coa_acuteiplist;

CREATE table #coa_acuteiplist
(
	Memid varchar(100),
	claimid varchar(100),
	servicedate varchar(10)
);


Insert into #coa_acuteiplist
select distinct Memid,claimid,servicedate from(
select Memid,claimid,date_s as servicedate from hds.HEDIS_VISIT where measure_id=@meas and date_s!='' and date_s between @ce_startdt and @ce_enddt and hcfapos!='81'
and (CPT in (select code_new from hds.VALUESET_TO_CODE where Value_Set_Name='Acute Inpatient') or HCFAPOS in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name='Acute Inpatient POS'))
Union all
Select Memid,0 as claimid,sdate as servicedate from hds.HEDIS_VISIT_E where MEASURE_ID=@meas and sdate!='' and sdate between @ce_startdt and @ce_enddt and activity in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name='Acute Inpatient')
)t1

CREATE CLUSTERED INDEX idx_coa_acuteiplist ON #coa_acuteiplist ([memid],[claimid],servicedate);


drop table if exists #coa_mrnumlist;
CREATE table #coa_mrnumlist
(
	Memid varchar(100)
	
);

Insert into #coa_mrnumlist
select distinct t3.memid from(
select t1.memid,t1.claimid from #coa_medicationreviewlist t1
join #coa_medicationlist t2 on t2.Memid=t1.Memid and t1.claimid=t2.claimid
Union all
Select memid,claimid from #coa_TCMSlist
)t3
left outer Join #coa_acuteiplist ip on t3.Memid=ip.Memid and t3.claimid=ip.claimid
where ip.Memid is null

CREATE CLUSTERED INDEX idx_coa_mrnumlist ON #coa_mrnumlist ([memid]);

update #coadataset set num=1 from #coadataset ds join #coa_mrnumlist n2 on ds.patient_id=n2.MemID and ds.meas='COA2'


-- Functional Status Assessment





DROP TABLE IF EXISTS #coa_FSAlist;
	
CREATE table #coa_FSAlist
(
	Memid varchar(100)
	
);

Insert into #coa_FSAlist
select distinct fsalist.Memid from(

select distinct t3.MemID from(
select distinct t1.memid,t1.claimid from(
select Memid,ClaimID from hds.HEDIS_VISIT where measure_id=@meas and date_s!='' and date_s between @ce_startdt and @ce_enddt and hcfapos!='81'
and 
(
CPT in (select code_new from hds.VALUESET_TO_CODE where Value_Set_Name='Functional Status Assessment') 
or
HCPCS in (select code_new from hds.VALUESET_TO_CODE where Value_Set_Name='Functional Status Assessment') 
)
)t1
left outer Join (
select Memid,claimid,date_s as servicedate from hds.HEDIS_VISIT where measure_id=@meas and date_s!='' and date_s between @ce_startdt and @ce_enddt and hcfapos!='81'
and (CPT in (select code_new from hds.VALUESET_TO_CODE where Value_Set_Name='Acute Inpatient') or HCFAPOS in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name='Acute Inpatient POS'))
)t2 on t1.MemID=t2.MemID and t1.claimid=t2.ClaimID where t2.MemID is null
)t3           

Union all

select distinct t3.Memid from(
Select t1.Memid,t1.ClaimID from(
select Memid,claimid from hds.HEDIS_VISIT where measure_id=@meas and date_s!='' and date_s between @ce_startdt and @ce_enddt and hcfapos!='81'
and CPT2 in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name='Functional Status Assessment') and CPT2Mod not in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name='CPT CAT II Modifier')
)t1
left outer Join (
select Memid,claimid from hds.HEDIS_VISIT where measure_id=@meas and date_s!='' and date_s between @ce_startdt and @ce_enddt and hcfapos!='81'
and (CPT in (select code_new from hds.VALUESET_TO_CODE where Value_Set_Name='Acute Inpatient') or HCFAPOS in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name='Acute Inpatient POS'))
)t2 on t1.MemID=t2.MemID and t1.claimid=t2.ClaimID where t2.memid is null
)t3            


Union all
select distinct t3.Memid from(
select t1.MemID,t1.servicedate from(
Select Memid,sdate as servicedate from hds.Hedis_proc where MEASURE_ID=@meas and sdate!='' and sdate between @ce_startdt and @ce_enddt and pstatus='EVN' and pcode in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name='Functional Status Assessment')
)t1
left outer Join(
Select Memid,sdate as servicedate from hds.HEDIS_VISIT_E where MEASURE_ID=@meas and sdate!='' and sdate between @ce_startdt and @ce_enddt and activity in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name='Acute Inpatient')
)t2 on t1.MemID=t2.MemID and t1.servicedate=t2.servicedate where t2.MemID is null
)t3
)fsalist

CREATE CLUSTERED INDEX idx_coa_FSAlist ON #coa_FSAlist ([memid]);

update #coadataset set num=1 from #coadataset ds join #coa_FSAlist n3 on ds.patient_id=n3.MemID and ds.meas='COA3'



-- Pain Management


DROP TABLE IF EXISTS #coa_painmgmtlist;
	
CREATE table #coa_painmgmtlist
(
	Memid varchar(100)
	
);

Insert into #coa_painmgmtlist
select distinct pmlist.Memid from(

select distinct t3.MemID from(
select distinct t1.memid,t1.claimid from(
select Memid,ClaimID from hds.HEDIS_VISIT where measure_id=@meas and date_s!='' and date_s between @ce_startdt and @ce_enddt and hcfapos!='81'
and 
(
CPT in (select code_new from hds.VALUESET_TO_CODE where Value_Set_Name='Pain Assessment') 
or
HCPCS in (select code_new from hds.VALUESET_TO_CODE where Value_Set_Name='Pain Assessment') 
)
)t1
left outer Join (
select Memid,claimid,date_s as servicedate from hds.HEDIS_VISIT where measure_id=@meas and date_s!='' and date_s between @ce_startdt and @ce_enddt and hcfapos!=81
and (CPT in (select code_new from hds.VALUESET_TO_CODE where Value_Set_Name='Acute Inpatient') or HCFAPOS in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name='Acute Inpatient POS'))
)t2 on t1.MemID=t2.MemID and t1.claimid=t2.ClaimID where t2.MemID is null
)t3           

Union all

select distinct t3.Memid from(
Select t1.Memid,t1.ClaimID from(
select Memid,claimid from hds.HEDIS_VISIT where measure_id=@meas and date_s!='' and date_s between @ce_startdt and @ce_enddt and hcfapos!='81'
and CPT2 in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name='Pain Assessment') and CPT2Mod not in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name='CPT CAT II Modifier')
)t1
left outer Join (
select Memid,claimid from hds.HEDIS_VISIT where measure_id=@meas and date_s!='' and date_s between @ce_startdt and @ce_enddt and hcfapos!='81'
and (CPT in (select code_new from hds.VALUESET_TO_CODE where Value_Set_Name='Acute Inpatient') or HCFAPOS in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name='Acute Inpatient POS'))
)t2 on t1.MemID=t2.MemID and t1.claimid=t2.ClaimID where t2.memid is null
)t3            


Union all
select distinct t3.Memid from(
select t1.MemID,t1.servicedate from(
Select Memid,sdate as servicedate from hds.Hedis_proc where MEASURE_ID=@meas and sdate!='' and sdate between @ce_startdt and @ce_enddt and pstatus='EVN' and pcode in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name='Pain Assessment')
)t1
left outer Join(
Select Memid,sdate as servicedate from hds.HEDIS_VISIT_E where MEASURE_ID=@meas and sdate!='' and sdate between @ce_startdt and @ce_enddt and activity in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name='Acute Inpatient')
)t2 on t1.MemID=t2.MemID and t1.servicedate=t2.servicedate where t2.MemID is null
)t3
)pmlist

CREATE CLUSTERED INDEX idx_coa_painmgmtlist ON #coa_painmgmtlist ([memid]);

update #coadataset set num=1 from #coadataset ds join #coa_painmgmtlist n4 on ds.patient_id=n4.MemID and ds.meas='COA4'


-- Hospice Exclusion

	drop table if exists #coa_hospicemembers;
	CREATE table #coa_hospicemembers
	(
		Memid varchar(100)
		
	);

	

	Insert into #coa_hospicemembers
	select distinct t1.MemID from
	(
	select Memid from hds.HEDIS_OBS where Measure_id=@meas and date !='' and date between @ce_startdt and @ce_enddt and OCode IN(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name in('Hospice Encounter','Hospice Intervention'))
	Union all
	Select Memid from hds.HEDIS_VISIT_E where Measure_id=@meas and sdate!='' and sdate between @ce_startdt and @ce_enddt and activity IN(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name in('Hospice Encounter','Hospice Intervention'))

	Union all

	select Beneficiary_id as Memid from hds.Hedis_MMDF where measure_id=@meas and Run_date!='' and Run_date between @ce_startdt and @ce_enddt and Hospice='Y'

	Union all

	select MemID from hds.HEDIS_VISIT v
	where MEASURE_ID=@meas and HCFAPOS!='81' and Date_S!='' and Date_S between @ce_startdt and @ce_enddt
	and (
	Diag_I_1 in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or
	Diag_I_2 in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or
	Diag_I_3 in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or
	Diag_I_4 in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or
	Diag_I_5 in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or
	Diag_I_6 in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or
	Diag_I_7 in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or
	Diag_I_8 in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or
	Diag_I_9 in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or
	Diag_I_10 in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or
	CPT in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or
	CPT2 in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or
	HCPCS in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or 
	Proc_I_1 in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or 
	Proc_I_2 in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or 
	Proc_I_3 in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or 
	Proc_I_4 in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or 
	Proc_I_5 in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or 
	Proc_I_6 in(select code_new from hds.VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	
	or
	Rev in(select code_new from hds.VALUESET_TO_CODE where code_system='UBREV' and Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	)
	)t1
	
	CREATE CLUSTERED INDEX idx_hospicemembers ON #coa_hospicemembers ([memid]);

	update #coadataset set rexcl=1 from #coadataset ds join #coa_hospicemembers hos on hos.memid=ds.patient_id;
			


-- Insert data in Output Table



	select @runid=max(RUN_ID) from hds.HEDIS_MEASURE_OUTPUT where measure_id=@meas;

	if(@runid>=1)
	Begin
		SET @runid=@runid+1;
	End
	Else
	Begin
		SET @runid=1;
	END

---	Insert into HEDIS_MEASURE_OUTPUT(memid,Meas,payer,CE,Event,Epop,Excl,Num,Rexcl,RExclD,Age,Gender,Measure_ID,Measurement_year,RUN_ID)
	SELECT patient_id AS memid,meas,payer,CE,EVENT,CASE WHEN CE=1 AND rexcl=0 and rexcld=0 and payer in('SN1','SN2','SN3','MMP') THEN 1 ELSE 0 END AS epop,excl,num,rexcl,rexcld,cast(age as Int) AS age,patient_gender AS gender,'COA' as Measure_ID,@meas_year,@runid FROM #coadataset
	

GO
