SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

CREATE proc [dbo].[KPI_UHN_SNFReadmission_41]
AS

BEGIN
DECLARE @INFO VARCHAR(8000)
					,	@TRUE BIT = 1
					,	@FALSE BIT = 0
					,	@DB_ID INT = DB_ID()
					,	@LOGID INT = 0
					,	@PROC VARCHAR(500)=OBJECT_SCHEMA_NAME(@@PROCID)+'.'+OBJECT_NAME(@@PROCID)
					,	@PERF_START DATETIME
					,	@PERF_DURATION INT
					,	@PERF_ROW INT
					,	@RC	INT
					;

		EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, @INFO=@INFO, @LOG2ID=@LOGID OUT;
		DECLARE @PROCFULLNAME VARCHAR(500) = DB_NAME()+'.'+@PROC;
		----EXEC dbo.SP_MI_UTIL_SEND_MAIL @PROCFULLNAME, 'STARTED', 1, @ECHO=0; 
	DECLARE @MSG VARCHAR(1000)
	DECLARE @PERF_START_PROC DATETIME 
	DECLARE @PERF_RPS NUMERIC(10,2) 
	DECLARE @ROWS INT 
	DECLARE @PERF_ROWS INT 
	SET @PERF_ROWS = 0 
	SET @PERF_START = GETDATE() 
	SET @PERF_START_PROC = GETDATE() 

BEGIN TRY




-- Declare Variables
declare @rundate Date=GetDate();
declare @meas_year varchar(4)=Year(Dateadd(month,-2,@rundate))
declare @rootId INT=159
declare @attributiondate Date=DATEADD(month, DATEDIFF(month, 0, Dateadd(month,-2,@rundate)), 0)


Declare @startDate Date;
Declare @enddate date;
Declare @quarter varchar(20)
Declare @reportId INT;
Declare @measure_id varchar(10);
Declare @target INT;
Declare @domain varchar(100);
Declare @subdomain varchar(100);
Declare @measuretype varchar(100);
Declare @measurename varchar(100);
Declare @reporttype varchar(100);

Declare @ce_startdt Date;
Declare @ce_startdt1 Date;
Declare @ce_novdt Date;
Declare @ce_enddt Date;
Declare @ce_enddt1 Date;
Declare @ce_dischargeenddt Date;
Declare @snfadmissionenddate Date;
Declare @snfadmissionstartdate Date;


-- Set Measure dates
SET @ce_startdt=concat(@meas_year,'-01-01');
SET @ce_novdt=concat(@meas_year,'-11-30');
SET @ce_enddt=concat(@meas_year,'-12-31');
SET @ce_startdt1=concat(@meas_year-1,'-01-01');
SET @ce_enddt1=concat(@meas_year-1,'-12-31');
Set @ce_dischargeenddt=concat(@meas_year,'-12-01');
Set @snfadmissionstartdate=concat(@meas_year,'-01-02');
Set @snfadmissionenddate=concat(@meas_year,'-12-02');




Set @reporttype='Network'
Set @measurename='SNF 30 Day all cause readmission'
set @target=17
Set @domain='Utilization Management'
Set @subdomain='Readmission Rate'
Set @measuretype='Calculated'
Set @measure_id=41




-- Identify members who are 65+
drop table if exists #41_popset
Create table #41_popset
(
	EMPI varchar(100),
	MEMBER_ID varchar(100),
	Gender varchar(10),
	Age INT
)
Insert into #41_popset
Select * from(
select distinct
	o.EMPI_ID
	,Org_Patient_Extension_ID as MEMBER_ID
	,Gender
	,CASE 
		WHEN dateadd(year, datediff (year, Date_of_Birth,eomonth(@ce_enddt)), Date_of_Birth) > eomonth(@ce_enddt) THEN datediff(year, Date_of_Birth, eomonth(@ce_enddt)) - 1
		ELSE datediff(year, Date_of_Birth, eomonth(@ce_enddt))
	END as Age
from open_empi_master o
where 
	o.Root_Companies_ID=@rootId
	
)t1 
where age >=65


-- Identify Deaceased Members
Drop table if exists #41_deceasedmembers
Create table #41_deceasedmembers
(	
	EMPI varchar(100)
)
Insert into #41_deceasedmembers
select * from deceasedmembers(@rootId,@ce_startdt,@ce_enddt)


-- Identify SNF Stays
drop table if exists #41_SNFStays;
create table #41_SNFStays
(
	EMPI varchar(50),
	ADM_DATE Date,
	DIS_DATE Date,
	CLAIM_ID VARCHAR(100)
)
Insert into #41_SNFStays
Select
	*
From
(
	select distinct
		EMPI
		,Case
			when ADM_DATE is null then FROM_DATE
			else ADM_DATE
		End as ADM_DATE
		,Case
			When DIS_DATE is null Then TO_DATE
			else DIS_DATE
		End as DIS_DATE
		,CLAIM_ID
	from KPI_ENGINE.dbo.CLAIMLINE 
	where ROOT_COMPANIES_ID=@rootId and 
		   ISNULL(POS,'')!='81' and
		  (
			REV_CODE in
			(
				select code from KPI_ENGINE.HDS.VALUESET_TO_CODE where Code_System='UBREV' and Value_Set_Name='Skilled Nursing Stay'
			) 
			or 
			RIGHT('0000'+CAST(Trim(UB_BILL_TYPE) AS VARCHAR(4)),4) in
			(
				select code from KPI_ENGINE.HDS.VALUESET_TO_CODE where code_system='UBTOB' and Value_Set_Name='Skilled Nursing Stay'
			)
		) 
)t1




-- Identify SNF to SNF transfers
drop table if exists #41_snfdirecttransfers;
Create table #41_snfdirecttransfers
(
	EMPI varchar(100),
	ADM_DATE Date,
	DIS_DATE Date,
	AdmissionClaimId varchar(100),
	DischargeClaimId varchar(100),
	grp_claimid nvarchar(max)
)
;with grp_starts as 
(
  select 
	EMPI
	,ADM_DATE
	,DIS_DATE
	,CLAIM_ID
	,case
		when datediff(day,lag(DIS_DATE) over(partition by EMPI order by ADM_DATE, DIS_DATE),ADM_DATE) <=1 	
		or 
		(datediff(day,lag(DIS_DATE,2) over(partition by EMPI order by ADM_DATE, DIS_DATE),ADM_DATE)<=1 )
		then 0 
		else 1
	end grp_start
  from #41_SNFStays

)
, grps as 
(
  select 
	EMPI
	,ADM_DATE
	, DIS_DATE
	,CLAIM_ID
	,sum(grp_start) over(partition by EMPI order by ADM_DATE, DIS_DATE) grp
  from grp_starts
)
Insert into #41_snfdirecttransfers
select 
	EMPI
	,AdmissionDate
	,DischargeDate
	,AdmissionClaimId
	,DischargeClaimId
	,grp_claimid 
from	
(
			select 
				EMPI
				,min(ADM_DATE) as AdmissionDate
				,max(DIS_DATE) as DischargeDate
				,min(CLAIM_ID) as AdmissionClaimId
				,max(CLAIM_ID) as DischargeClaimId
				,STRING_AGG(CLAIM_ID,',') as grp_claimid
			from grps 
			group by EMPI,grp
		
)t1
--where AdmissionDate between @snfadmissionstartdate and @snfadmissionenddate 





-- Identify Inpatient Stay List
drop table if exists #41_ipstaylist;
CREATE table #41_ipstaylist
(
	EMPI varchar(100),
	ADM_DATE Date,
	DIS_DATE Date,
	CLAIM_ID varchar(100)
				
);
Insert into #41_ipstaylist
-- Inpatient Stay list

	select distinct
		c.EMPI
		,Case
			when c.ADM_DATE is null then FROM_DATE
			else c.ADM_DATE
		End as ADM_DATE
		,Case
			When c.DIS_DATE is null Then TO_DATE
			else c.DIS_DATE
		End as DIS_DATE
		,c.CLAIM_ID
	from KPI_ENGINE.dbo.CLAIMLINE c
	left outer join #41_SNFStays s on c.CLAIM_ID=s.CLAIM_ID
	where ROOT_COMPANIES_ID=@rootId and 
		  ISNULL(c.POS,'')!='81' and
		  c.REV_CODE in
		  (
			select code from KPI_ENGINE.HDS.VALUESET_TO_CODE where code_system='UBREV' and Value_Set_Name in('Inpatient Stay')
		  )	
		  and s.EMPI is null




-- Identify IP to IP transfers
drop table if exists #41_ipdirecttransfers;
Create table #41_ipdirecttransfers
(
	EMPI varchar(100),
	ADM_DATE Date,
	DIS_DATE Date,
	AdmissionClaimId varchar(100),
	DischargeClaimId varchar(100),
	grp_claimid varchar(600)
)
;with grp_starts as 
(
  select 
	EMPI
	,ADM_DATE
	,DIS_DATE
	,CLAIM_ID
	,case
		when datediff(day,lag(DIS_DATE) over(partition by EMPI order by ADM_DATE, DIS_DATE),ADM_DATE) <=1 	
		or 
		(datediff(day,lag(DIS_DATE,2) over(partition by EMPI order by ADM_DATE, DIS_DATE),ADM_DATE)<=1 )
		then 0 
		else 1
	end grp_start
  from #41_ipstaylist

)
, grps as 
(
  select 
	EMPI
	,ADM_DATE
	, DIS_DATE
	,CLAIM_ID
	,sum(grp_start) over(partition by EMPI order by ADM_DATE, DIS_DATE) grp
  from grp_starts
)
Insert into #41_ipdirecttransfers
select 
	EMPI
	,AdmissionDate
	,DischargeDate
	,AdmissionClaimId
	,DischargeClaimId
	,grp_claimid 
from	
(
			select 
				EMPI
				,min(ADM_DATE) as AdmissionDate
				,max(DIS_DATE) as DischargeDate
				,min(CLAIM_ID) as AdmissionClaimId
				,max(CLAIM_ID) as DischargeClaimId
				,STRING_AGG(CLAIM_ID,',') as grp_claimid
			from grps 
			group by EMPI,grp
		
)t1






--Identify IP to SNF Transfer
Drop table if exists #41_iptosnf
Create table #41_iptosnf
(
	EMPI varchar(100),
	Anchor_DIS_DATE Date,
	ADM_DATE Date,
	DIS_DATE Date,
	DischargeClaimId varchar(100)
)
Insert into #41_iptosnf
select distinct
	s.EMPI
	,i.DIS_DATE
	,s.ADM_DATE
	,s.DIS_DATE
	,s.DischargeClaimId
from #41_snfdirecttransfers s
Join #41_ipdirecttransfers i on s.EMPI=i.EMPI and DATEDIFF(day,i.DIS_DATE,s.ADM_DATE) between 0 and 1
where
	i.DIS_DATE between @ce_startdt and @ce_dischargeenddt





-- Numerator logic Step 1
Drop table if exists #41snftoip
Create table #41snftoip
(
	EMPI varchar(100),
	Anchor_DIS_DATE Date,
	ADM_DATE DATE,
	DIS_DATE DATE,
	ip_ADM_DATE date,
	ip_DIS_DATE date,
	DischargeClaimId varchar(100),
	ReadmissionClaimId varchar(100)
)
Insert into #41snftoip
Select 
	EMPI
	,Anchor_DIS_DATE
	,ADM_DATE
	,DIS_DATE
	,ip_ADM_DATE
	,ip_DIS_DATE
	,DischargeClaimId
	,ReadmissionClaimId
from(
Select 
	*
	,DENSE_RANK() over(partition by EMPI,DIS_DATE order by ip_ADM_DATE) as rnk2 
from (
	Select distinct 
		*
		,DENSE_RANK() over(partition by EMPI,ip_ADM_DATE order by DIS_DATE desc) as rnk 
	from(
		select 
			s.EMPI
			,s.Anchor_DIS_DATE
			,s.ADM_DATE
			,s.DIS_DATE
			,i.ADM_DATE as ip_ADM_DATE
			,i.DIS_DATE as ip_DIS_DATE
			,s.DischargeClaimId
			,i.AdmissionClaimId as ReadmissionClaimId
		from #41_iptosnf s
		join #41_ipdirecttransfers i on s.EMPI=i.EMPI and DATEDIFF(day,s.Anchor_DIS_DATE,i.ADM_DATE) between 0 and 30
		)t1
	)t2
)t3
where rnk=1 and rnk2=1



-- Identify Exclusions
-- Identify Prgnacncy Visits
drop table if exists #41_pregvstlist;
CREATE table #41_pregvstlist
(
	EMPI varchar(50),
	FROM_DATE DATE
	
)
Insert into #41_pregvstlist
select 
	t1.* 
from
(
	select distinct
		d.EMPI
		,c.FROM_DATE
	from KPI_ENGINE.dbo.DIAGNOSIS d
	join KPI_ENGINE.dbo.CLAIMLINE c on d.CLAIM_ID=c.CLAIM_ID and 
									   d.DIAG_DATA_SRC=c.CL_DATA_SRC and 
									   d.ROOT_COMPANIES_ID=c.ROOT_COMPANIES_ID
	where d.ROOT_COMPANIES_ID=@rootId and 
		  FROM_DATE between @ce_startdt and @ce_enddt and 
		 ISNULL(POS,'')!='81' and
		  DIAG_CODE in
		  (
			select REPLACE(code,'.','') from KPI_ENGINE.HDS.VALUESET_TO_CODE where Value_Set_Name='Pregnancy'
		  )
	)t1
	join KPI_ENGINE.dbo.open_empi_master gm on t1.EMPI=gm.EMPI_ID and 
											   gm.ROOT_COMPANIES_ID=@rootId and 
											   gm.Gender='F'



--Identify Perinatal Conditions
drop table if exists #41_perinatalvstlist;
CREATE table #41_perinatalvstlist
(
	EMPI varchar(100),
	FROM_DATE Date
);
Insert into #41_perinatalvstlist
select distinct
	d.EMPI
	,c.FROM_DATE 
from KPI_ENGINE.dbo.DIAGNOSIS d
join KPI_ENGINE.dbo.CLAIMLINE c on d.CLAIM_ID=c.CLAIM_ID and 
								   d.DIAG_DATA_SRC=c.CL_DATA_SRC and 
								   d.ROOT_COMPANIES_ID=c.ROOT_COMPANIES_ID
where d.ROOT_COMPANIES_ID=@rootId and 
	  FROM_DATE between @ce_startdt and @ce_enddt and 
	  ISNULL(POS,'')!='81' and
	  DIAG_CODE in
	  (
			select REPLACE(code,'.','') from KPI_ENGINE.HDS.VALUESET_TO_CODE where Value_Set_Name='Perinatal Conditions'
	  )


-- Indetifying Chemotherapy
drop table if exists #41_chemovstlist;
CREATE table #41_chemovstlist
(
	EMPI varchar(100),
	FROM_DATE Date
	
);
Insert into #41_chemovstlist
select distinct
	d.EMPI
	,c.FROM_DATE
from KPI_ENGINE.dbo.DIAGNOSIS d
join KPI_ENGINE.dbo.CLAIMLINE c on d.CLAIM_ID=c.CLAIM_ID and 
								   d.DIAG_DATA_SRC=c.CL_DATA_SRC and 
								   d.ROOT_COMPANIES_ID=c.ROOT_COMPANIES_ID
where d.ROOT_COMPANIES_ID=@rootId and 
	  FROM_DATE between @ce_startdt and @ce_enddt and 
	  ISNULL(POS,'')!='81' and
	  DIAG_CODE in
	  (
		select REPLACE(code,'.','') from KPI_ENGINE.HDS.VALUESET_TO_CODE where Value_Set_Name='Chemotherapy Encounter'
	  )


-- Rehabilitation
drop table if exists #41_rehabvstlist;
CREATE table #41_rehabvstlist
(
	EMPI varchar(100),
	FROM_DATE Date
	
);
Insert into #41_rehabvstlist
select distinct
	d.EMPI
	,c.FROM_DATE
from KPI_ENGINE.dbo.DIAGNOSIS d
join KPI_ENGINE.dbo.CLAIMLINE c on d.CLAIM_ID=c.CLAIM_ID and 
								   d.DIAG_DATA_SRC=c.CL_DATA_SRC and 
								   d.ROOT_COMPANIES_ID=c.ROOT_COMPANIES_ID
where d.ROOT_COMPANIES_ID=@rootId and 
	  FROM_DATE between @ce_startdt and @ce_enddt and 
	  ISNULL(POS,'')!='81' and
	  DIAG_CODE in
	  (
		select replace(code,'.','') from KPI_ENGINE.HDS.VALUESET_TO_CODE where Value_Set_Name='Rehabilitation'
	  )


-- Organ Transplant
drop table if exists #41_organtransplanttlist;
CREATE table #41_organtransplanttlist
(
	EMPI varchar(100),
	FROM_DATE DATE
	
);
Insert into #41_organtransplanttlist
select distinct
	d.EMPI
	,c.FROM_DATE
from KPI_ENGINE.dbo.PROCEDURES d 
join KPI_ENGINE.dbo.CLAIMLINE c on d.CLAIM_ID=c.CLAIM_ID and 
								   d.PROC_DATA_SRC=c.CL_DATA_SRC and 
								   d.ROOT_COMPANIES_ID=c.ROOT_COMPANIES_ID
where d.ROOT_COMPANIES_ID=@rootId and 
	  FROM_DATE between @ce_startdt and @ce_enddt and 
	  ISNULL(POS,'')!='81' and
	  (
		 d.PROC_CODE in
		 (
			select code from KPI_ENGINE.HDS.VALUESET_TO_CODE where Value_Set_Name in('Kidney Transplant','Organ Transplant Other Than Kidney')
		 )
		or 
		d.ICDPCS_CODE in
		(
			select code from KPI_ENGINE.HDS.VALUESET_TO_CODE where Value_Set_Name in('Kidney Transplant','Bone Marrow Transplant','Organ Transplant Other Than Kidney','Introduction of Autologous Pancreatic Cells')
		)
	  )



-- Identify Stays with Principal Acute Diagnosis
drop table if exists #41_acutediagnosis;
CREATE table #41_acutediagnosis
(
	EMPI varchar(100),
	FROM_DATE Date,
	CLAIM_ID varchar(100)
	
);
Insert into #41_acutediagnosis
select distinct
	d.EMPI
	,c.FROM_DATE
	,c.CLAIM_ID
from KPI_ENGINE.dbo.DIAGNOSIS d
join KPI_ENGINE.dbo.CLAIMLINE c on d.CLAIM_ID=c.CLAIM_ID and 
								   d.DIAG_DATA_SRC=c.CL_DATA_SRC and 
								   d.ROOT_COMPANIES_ID=c.ROOT_COMPANIES_ID
where d.ROOT_COMPANIES_ID=@rootId and 
	  FROM_DATE between @ce_startdt and @ce_enddt and 
	  ISNULL(POS,'')!='81' and
	  DIAG_CODE in
	  (
		select replace(code,'.','') from KPI_ENGINE.HDS.VALUESET_TO_CODE where Value_Set_Name='Acute Condition'
	  )
	  and
	  DIAG_SEQ_NO=1



--Potentially Planned Procedure
drop table if exists #41_plannedproctlist;
CREATE table #41_plannedproctlist
(
	EMPI varchar(100),
	FROM_DATE DATE
	
);
Insert into #41_plannedproctlist
select distinct
	d.EMPI
	,c.FROM_DATE
from KPI_ENGINE.dbo.PROCEDURES d 
join KPI_ENGINE.dbo.CLAIMLINE c on d.CLAIM_ID=c.CLAIM_ID and 
								   d.PROC_DATA_SRC=c.CL_DATA_SRC and 
								   d.ROOT_COMPANIES_ID=c.ROOT_COMPANIES_ID
left outer join #41_acutediagnosis ad on d.EMPI=ad.EMPI and
										 d.CLAIM_ID=ad.CLAIM_ID
where d.ROOT_COMPANIES_ID=@rootId and 
	  d.PROC_START_DATE between @ce_startdt and @ce_enddt and 
	  ISNULL(POS,'')!='81' and
	  d.ICDPCS_CODE in
	  (
		select code from KPI_ENGINE.HDS.VALUESET_TO_CODE where Value_Set_Name in('Potentially Planned Procedures','Potentially Planned Post-Acute Care Hospitalization')
	  )
	  and
	  ad.EMPI is NULL


--Introduction of Autologous Pancreatic Cells
drop table if exists #41_autologouspancreatic;
CREATE table #41_autologouspancreatic
(
	EMPI varchar(100),
	FROM_DATE DATE
	
);
Insert into #41_autologouspancreatic
select distinct
	d.EMPI
	,c.FROM_DATE
from KPI_ENGINE.dbo.PROCEDURES d 
join KPI_ENGINE.dbo.CLAIMLINE c on d.CLAIM_ID=c.CLAIM_ID and 
								   d.PROC_DATA_SRC=c.CL_DATA_SRC and 
								   d.ROOT_COMPANIES_ID=c.ROOT_COMPANIES_ID
where d.ROOT_COMPANIES_ID=@rootId and 
	  FROM_DATE between @ce_startdt and @ce_enddt and 
	  ISNULL(POS,'')!='81' and
	  d.ICDPCS_CODE in
	  (
		select code from KPI_ENGINE.HDS.VALUESET_TO_CODE where Value_Set_Name ='Introduction of Autologous Pancreatic Cells'
	  )


-- Identify Hospice exclusions
drop table if exists #41_hospicemembers;
CREATE table #41_hospicemembers
(
	EMPI varchar(50)
		
);
Insert into #41_hospicemembers
select distinct EMPI from hospicemembers(@rootId,@ce_startdt,@ce_enddt)



-- Removed Planned Stays from the ipstay list
drop table if exists #41_staywitexclusions
Create table #41_staywitexclusions
(
	EMPI varchar(50),
	AdmissionDate Date,
	DischargeDate Date
)
Insert into #41_staywitexclusions
select distinct 
	s.EMPI
	,s.ADM_DATE
	,s.DIS_DATE 
from #41snftoip s
left outer join #41_plannedproctlist p on s.EMPI=p.EMPI and 
									 p.FROM_DATE between s.IP_ADM_DATE and s.IP_DIS_DATE
left outer join #41_chemovstlist c on s.EMPI=c.EMPI and 
									  c.FROM_DATE between s.IP_ADM_DATE and s.IP_DIS_DATE

left outer join #41_organtransplanttlist o on s.EMPI=o.EMPI and 
											  o.FROM_DATE between s.IP_ADM_DATE and s.IP_DIS_DATE
left outer join #41_perinatalvstlist pn on s.EMPI=pn.EMPI and 
										   pn.FROM_DATE between s.IP_ADM_DATE and s.IP_DIS_DATE
left outer join #41_pregvstlist pg on s.EMPI=pg.EMPI and 
									  pg.FROM_DATE between s.IP_ADM_DATE and s.IP_DIS_DATE
left outer join #41_rehabvstlist r on s.EMPI=r.EMPI and 
									  r.FROM_DATE between s.IP_ADM_DATE and s.IP_DIS_DATE
left outer join #41_autologouspancreatic ap on s.EMPI=ap.EMPI and 
											   ap.FROM_DATE between s.IP_ADM_DATE and s.IP_DIS_DATE
where p.EMPI is NULL and 
	  c.EMPI is NULL and 
	  o.EMPI is NULL and 
	  pn.EMPI is NULL and 
	  pg.EMPI is NULL and 
	  r.EMPI is NULL and 
	  ap.EMPI is null and
	  s.ADM_DATE!=s.DIS_DATE


-- Identify Excluded stays
drop table if exists #41_excludedstays
Create table #41_excludedstays
(
	EMPI varchar(50),
	AdmissionDate Date,
	DischargeDate Date
)
Insert into #41_excludedstays
select distinct 
	s.EMPI
	,s.ADM_DATE
	,s.DIS_DATE 
from #41snftoip s
left outer join #41_plannedproctlist p on s.EMPI=p.EMPI and 
									 p.FROM_DATE between s.IP_ADM_DATE and s.IP_DIS_DATE
left outer join #41_chemovstlist c on s.EMPI=c.EMPI and 
									  c.FROM_DATE between s.IP_ADM_DATE and s.IP_DIS_DATE

left outer join #41_organtransplanttlist o on s.EMPI=o.EMPI and 
											  o.FROM_DATE between s.IP_ADM_DATE and s.IP_DIS_DATE
left outer join #41_perinatalvstlist pn on s.EMPI=pn.EMPI and 
										   pn.FROM_DATE between s.IP_ADM_DATE and s.IP_DIS_DATE
left outer join #41_pregvstlist pg on s.EMPI=pg.EMPI and 
									  pg.FROM_DATE between s.IP_ADM_DATE and s.IP_DIS_DATE
left outer join #41_rehabvstlist r on s.EMPI=r.EMPI and 
									  r.FROM_DATE between s.IP_ADM_DATE and s.IP_DIS_DATE
left outer join #41_autologouspancreatic ap on s.EMPI=ap.EMPI and 
											   ap.FROM_DATE between s.IP_ADM_DATE and s.IP_DIS_DATE
where p.EMPI is NOT NULL or 
	  c.EMPI is NOT  NULL or 
	  o.EMPI is NOT  NULL or 
	  pn.EMPI is NOT  NULL or 
	  pg.EMPI is NOT  NULL or 
	  r.EMPI is NOT  NULL or 
	  ap.EMPI is NOT  null or
	  ADM_DATE=DIS_DATE



-- Get ReportId from Report_Details

	exec GetReportDetail @rundate=@rundate,@rootId=@rootId,@startDate=@startDate output,@enddate=@enddate output,@quarter=@quarter output,@reportId=@reportId output





-- Create the output as required
Drop table if exists #41_dataset
Create Table #41_dataset
(
	EMPI varchar(100),
	Provider_Id varchar(20),
	PCP_NPI varchar(20),
	PCP_NAME varchar(200),
	Practice_Name varchar(200),
	Specialty varchar(100),
	Measure_id varchar(20),
	Measure_Name varchar(100),
	Payer varchar(50),
	PayerId varchar(100),
	MEM_FNAME varchar(100),
	MEM_MName varchar(50),
	MEM_LNAME varchar(100),
	MEM_DOB Date,
	MEM_GENDER varchar(20),
	Enrollment_Status varchar(20),
	Last_visit_date Date,
	Product_Type varchar(50),
	Num bit,
	Den bit,
	Excl bit,
	Rexcl bit,
	CE bit,
	Epop bit,
	Report_Id INT,
	ReportType varchar(20),
	Report_Quarter varchar(20),
	Period_Start_Date Date,
	Period_End_Date Date,
	Root_Companies_Id INT,
	DischargeDate Date,
	DateofService Date

)
Insert into #41_dataset
select distinct
	d.EMPI
	,a.AmbulatoryPCPNPI as Provider_Id 
	,a.AmbulatoryPCPNPI as PCP_NPI
	,a.AmbulatoryPCPName as PCP_NAME
	,a.AmbulatoryPCPPractice as Practice_Name
	,a.AmbulatoryPCPSpecialty as Specialty
	,@measure_id as Measure_id
	,@measurename as Measure_Name
	,a.DATA_SOURCE as Payer
	,a.PayerId
	,a.MemberFirstName as MEM_FNAME
	,a.MemberMiddleName as MEM_MName
	,a.MemberLastName as MEM_LNAME
	,a.MemberDOB
	,m.Gender as MEM_GENDER
	,a.EnrollmentStatus
	,a.AmbulatoryPCPRecentVisit as Last_visit_date
	,mm.PAYER_TYPE as ProductType
	,0 as Num
	,1 as Den
	,0 as Excl
	,0 as Rexcl
	,0 as CE
	,0 as Epop
	,@reportId
	,@reporttype as ReportType
	,@quarter as Report_quarter
	,@startDate as Period_start_date
	,@enddate as Period_end_date
	,@rootId
	,d.DIS_DATE as DischargeDate
	,'1900-01-01' as DateofService
from #41_iptosnf d
join RPT.PCP_ATTRIBUTION a on d.EMPI=a.EMPI and a.ReportId=@reportId
join #41_popset p on d.EMPI=p.EMPI
Join open_empi_master m on d.EMPI=m.EMPI_ID
left outer join MEMBER_MONTH mm on d.EMPI=mm.EMPI and mm.MEMBER_MONTH_START_DATE=@attributiondate
where 
	a.AssignedStatus='Assigned' 




update ds Set num=1,DateofService=n.ip_ADM_DATE from #41_dataset ds join #41snftoip n on ds.EMPI=n.EMPI and ds.DischargeDate=n.DIS_DATE
--update ds Set CE=1 from #41_dataset ds join #41_contenroll n on ds.EMPI=n.EMPI
update ds Set Rexcl=1 from #41_dataset ds join #41_hospicemembers n on ds.EMPI=n.EMPI
--update ds Set Rexcl=1 from #41_dataset ds join #41_LTImembers n on ds.EMPI=n.EMPI
update ds Set Rexcl=1 from #41_dataset ds join #41_excludedstays n on ds.EMPI=n.EMPI and ds.DateofService=n.DischargeDate
--update #41_dataset set Epop=1 where Rexcl=0 and Excl=0 and CE=1




-- Insert data into Measure Detailed Line
Delete from RPT.MEASURE_DETAILED_LINE where MEASURE_ID=@measure_id and REPORT_ID=@reportId and ROOT_COMPANIES_ID=@rootId;

Insert into RPT.MEASURE_DETAILED_LINE(Provider_Id,PCP_NPI,PCP_NAME,Practice_Name,Specialty,Measure_id,Measure_Name,Payer,PayerId,MEM_FNAME,MEM_MName,MEM_LNAME,MEM_DOB,MEM_GENDER,ENROLLMENT_STATUS,Last_visit_date,Product_Type,Num,Den,Excl,Rexcl,CE,Epop,Report_Id,ReportType,Report_Quarter,Period_Start_date,Period_end_Date,Root_Companies_id,EMPI,Measure_Type,Code,DateofService,DischargeDate)
Select Provider_Id,PCP_NPI,PCP_NAME,Practice_Name,Specialty,Measure_id,Measure_Name,Payer,PayerId,MEM_FNAME,MEM_MName,MEM_LNAME,MEM_DOB,MEM_GENDER,Enrollment_Status,Last_visit_date,Product_Type,Num,Den,Excl,Rexcl,CE,Epop,Report_Id,ReportType,Report_Quarter,Period_Start_date,Period_end_Date,Root_Companies_id,d.EMPI,@measuretype,'SNF to IP Readmission',DateofService,DischargeDate
From #41_dataset d
--where Specialty in('Hospitalists')




-- Insert data into Provider Scorecard
Delete from RPT.PROVIDER_SCORECARD  where MEASURE_ID=@measure_id and REPORT_ID=@reportId and ROOT_COMPANIES_ID=@rootId;


/*
Insert into RPT.PROVIDER_SCORECARD(Measure_id,Measure_Name,Measure_Title,MEASURE_SUBTITLE,Measure_Type,NUM_COUNT,DEN_COUNT,Excl_Count,Rexcl_Count,Gaps,Result,Target,Report_Id,ReportType,Report_Quarter,Period_Start_Date,Period_End_Date,Root_Companies_Id,To_Target)
Select
	*
	,Case
		when Result > @target then @target-Result
		Else 0
	end as To_Target
From
(
	Select 
		Measure_id
		,Measure_Name
		,Measure_Title
		,MEASURE_SUBTITLE
		,Measure_Type
		,SUM(Cast(NUM_COUNT as INT)) as NUM_COUNT
		,SUM(Cast(DEN_COUNT as INT)) as DEN_COUNT
		,SUM(Cast(Excl_Count as INT)) as Excl_Count
		,Sum(Cast(Rexcl_count as INT)) as Rexcl_Count
		,SUM(Cast(NUM_COUNT as INT)) as Gaps
		,Case 
			when SUM(Cast(DEN_excl as Float)) >0 then Round((SUM(Cast(NUM_COUNT as Float))/(SUM(Cast(DEN_excl as Float))))*100,2)
			else 0
		end as Result
		,@target as Target
		,Report_Id
		,ReportType
		,Report_Quarter
		,Period_Start_Date
		,Period_End_Date
		,Root_Companies_Id
	From
	(
		Select distinct
			EMPI
			,Measure_id
			,Measure_Name
			,@domain as Measure_Title
			,@subdomain as MEASURE_SUBTITLE
			,@measuretype as Measure_Type
			,Case
				when NUM=1 and Excl=0 and Rexcl=0 then 1
				else 0
			end as NUM_COUNT
			,DEN as DEN_COUNT
			,Case
				when DEN=1 and Excl=0 and Rexcl=0  then 1
				else 0
			end as DEN_excl
			,Excl as Excl_Count
			,Rexcl as Rexcl_count
			,Report_Id
			,ReportType
			,Report_Quarter
			,Period_Start_Date
			,Period_End_Date
			,Root_Companies_Id
			,DischargeDate
		From #41_dataset
		where 
			Enrollment_Status='Active' -- and
			--Specialty in('Hospitalists')
	)t1
	Group by Measure_id,Measure_Name,Measure_Title,MEASURE_SUBTITLE,Report_Id,ReportType,Report_Quarter,Period_Start_Date,Period_End_Date,Root_Companies_Id,Measure_Type
)t2

*/


Insert into RPT.PROVIDER_SCORECARD(Measure_id,Measure_Name,Measure_Title,MEASURE_SUBTITLE,Measure_Type,NUM_COUNT,DEN_COUNT,Excl_Count,Rexcl_Count,Gaps,Result,Target,To_Target,Report_Id,ReportType,Report_Quarter,Period_Start_Date,Period_End_Date,Root_Companies_Id)

	Select
		Measure_id,
		Measure_Name,
		Measure_Title,
		MEASURE_SUBTITLE,
		Measure_Type,
		NUM_COUNT,
		DEN_COUNT,
		Excl_Count,
		Rexcl_Count,
		Gaps,
		Result,
		Target,
		Case
			when Result > Target then Floor((DEN_COUNT*Target)/100)-NUM_COUNT
			when Result <= Target then 0
		end as To_Target,
		Report_Id,
		ReportType,
		Report_Quarter,
		Period_Start_Date,
		Period_End_Date,
		Root_Companies_Id
	From
	(
		
			Select
				Measure_id,
				Measure_Name,
				Measure_Title,
				MEASURE_SUBTITLE,
				Measure_Type,
				SUM(Cast(NUM_COUNT as INT)) as NUM_COUNT,
				SUM(Cast(Den_excl as INT)) as DEN_COUNT,
				SUM(Cast(Excl_Count as INT)) as Excl_Count,
				Sum(Cast(Rexcl_count as INT)) as Rexcl_Count,
				SUM(Cast(NUM_COUNT as INT))  as Gaps,
				Case
					when SUM(Cast(DEN_Excl as Float))>0 Then Round((SUM(cast(NUM_COUNT as Float))/ISNULL(NULLIF(SUM(Cast(DEN_Excl as Float)),0),1))*100,2)
					else 0
				end as Result,
				Target,
				Report_Id,
				ReportType,
				Report_Quarter,
				Period_Start_Date,
				Period_End_Date,
				Root_Companies_Id
			From
			(
				Select distinct
					m.EMPI
					,a.NPI as Provider_id
					,a.NPI as PCP_NPI
					,a.Prov_Name as PCP_Name
					,a.Specialty
					,a.Practice as Practice_Name
					,m.Measure_id
					,m.Measure_Name
					,l.measure_Title
					,l.Measure_SubTitle
					,'Calculated' as Measure_Type
					,Case
						when NUM=1 and excl=0 and rexcl=0 Then 1
						else 0
					end as NUM_COUNT
					,DEN as DEN_COUNT
					,Case
						When DEN=1 and Excl=0 and Rexcl=0 Then 1
						else 0
					End as Den_excl
					,Excl as Excl_Count
					,Rexcl as Rexcl_count
					,Report_Id
					,l.ReportType
					,Report_Quarter
					,Period_Start_Date
					,Period_End_Date
					,m.Root_Companies_Id
					,l.Target
					,m.Code
					,m.DateofService
					,m.DischargeDate
				From RPT.MEASURE_DETAILED_LINE m
				Join RPT.ConsolidatedAttribution_Snapshot a on
					m.EMPI=a.EMPI and
					a.Attribution_Type='Ambulatory_PCP'
				Join RFT.UHN_measuresList l on
					m.Measure_Id=l.measure_id
				where 
					Enrollment_Status='Active' and
					a.NPI!='' and
					m.MEASURE_ID=@measure_id and
					REPORT_ID=@reportId 

			)tbl
			Group By
				Measure_id,Measure_Name,Measure_Title,MEASURE_SUBTITLE,Report_Id,ReportType,Report_Quarter,Period_Start_Date,Period_End_Date,Root_Companies_Id,Measure_Type,target
		
	)tbl2		
			
			

		SET @PERF_ROW = @@ROWCOUNT
		SET @PERF_DURATION = DATEDIFF(MINUTE,@PERF_START,GETDATE());
		EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, 'Enrollment Completed', @PERF_ROW, @DURATION_IN_MIN=@PERF_DURATION, @LOG2ID=@LOGID;
		EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, @LOG2ID=@LOGID, @END_FLAG=1;

END TRY
BEGIN CATCH
		BEGIN
			EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, 'FATAL ERROR', @LOG2ID=@LOGID;
			RAISERROR ('',16,0)
			RETURN 16
        END
	END CATCH
RETURN 0
END

GO
