SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
CREATE Proc [RPT].[REFRESH_SCORECARD_DASHBOARD_SNAPSHOT]

AS

BEGIN
DECLARE @INFO VARCHAR(8000)
					,	@TRUE BIT = 1
					,	@FALSE BIT = 0
					,	@DB_ID INT = DB_ID()
					,	@LOGID INT = 0
					,	@PROC VARCHAR(500)=OBJECT_SCHEMA_NAME(@@PROCID)+'.'+OBJECT_NAME(@@PROCID)
					,	@PERF_START DATETIME
					,	@PERF_DURATION INT
					,	@PERF_ROW INT
					,	@RC	INT
					;

		EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, @INFO=@INFO, @LOG2ID=@LOGID OUT;
		DECLARE @PROCFULLNAME VARCHAR(500) = DB_NAME()+'.'+@PROC;
		----EXEC dbo.SP_MI_UTIL_SEND_MAIL @PROCFULLNAME, 'STARTED', 1, @ECHO=0; 
	DECLARE @MSG VARCHAR(1000)
	DECLARE @PERF_START_PROC DATETIME 
	DECLARE @PERF_RPS NUMERIC(10,2) 
	DECLARE @ROWS INT 
	DECLARE @PERF_ROWS INT 
	SET @PERF_ROWS = 0 
	SET @PERF_START = GETDATE() 
	SET @PERF_START_PROC = GETDATE() 

BEGIN TRY




/******************************************************************************************************************************/
-------------------------------------------Refreshing Consolidated Attribution Snapshot Table-----------------------------------
/******************************************************************************************************************************/

/* Moving this logic to UHN attribution Procedure*/

/*
------Truncating Table
Truncate table [KPI_ENGINE].[RPT].[ConsolidatedAttribution_Snapshot]



------Dropping Index Before Insertion-----

DROP INDEX [idx_ConsolidatedAttribution_CurrMnth_EMPI] ON [RPT].[ConsolidatedAttribution_Snapshot] WITH ( ONLINE = OFF )

DROP INDEX [idx_ConsolidatedAttribution_CurrMnth_NPI] ON [RPT].[ConsolidatedAttribution_Snapshot]

DROP INDEX [idx_ConsolidatedAttribution_CurrMnth_Practice] ON [RPT].[ConsolidatedAttribution_Snapshot]

DROP INDEX [idx_ConsolidatedAttribution_CurrMnth_Prov_Name] ON [RPT].[ConsolidatedAttribution_Snapshot]

DROP INDEX [idx_ConsolidatedAttribution_CurrMnth_root_companies_id] ON [RPT].[ConsolidatedAttribution_Snapshot]

DROP INDEX [idx_ConsolidatedAttribution_CurrMnth_Specialty] ON [RPT].[ConsolidatedAttribution_Snapshot]

--------------Inserting the latest report data-----------------------
INSERT INTO [KPI_ENGINE].[RPT].[ConsolidatedAttribution_Snapshot]
(
[EMPI]
      ,[PayerId]
      ,[AlternateMemberId]
      ,[MedicareId]
      ,[MemberLastName]
      ,[MemberFirstName]
      ,[MemberMiddleName]
      ,[MemberDOB]
      ,[MEM_GENDER]
      ,[EnrollmentStatus]
      ,[AssignedStatus]
      ,[Payer]
      ,[Attribution_Type]
      ,[Rank]
      ,[NPI]
      ,[Prov_Name]
      ,[Specialty]
      ,[Practice]
      ,[RecentVisit]
      ,[ReportId]
      ,[LoadDate]
      ,[ReportEndDate]
      ,[ROOT_COMPANIES_ID]
      ,[ReportQuarter]
)
SELECT [EMPI]
      ,[PayerId]
      ,[AlternateMemberId]
      ,[MedicareId]
      ,[MemberLastName]
      ,[MemberFirstName]
      ,[MemberMiddleName]
      ,[MemberDOB]
      ,[MEM_GENDER]
      ,[EnrollmentStatus]
      ,[AssignedStatus]
      ,[Payer]
      ,[Attribution_Type]
      ,[Rank]
      ,[NPI]
      ,[Prov_Name]
      ,[Specialty]
      ,[Practice]
      ,[RecentVisit]
      ,[ReportId]
      ,[LoadDate]
      ,[ReportEndDate]
      ,[ROOT_COMPANIES_ID]
      ,[ReportQuarter]
  FROM [KPI_ENGINE].[RPT].[ConsolidatedAttribution]

---------------------------------------------------------------------

------Recreating Index After Insertion-----
CREATE CLUSTERED INDEX [idx_ConsolidatedAttribution_CurrMnth_EMPI] ON [RPT].[ConsolidatedAttribution_Snapshot]
(
	[EMPI] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]

CREATE NONCLUSTERED INDEX [idx_ConsolidatedAttribution_CurrMnth_NPI] ON [RPT].[ConsolidatedAttribution_Snapshot]
(
	[NPI] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]

CREATE NONCLUSTERED INDEX [idx_ConsolidatedAttribution_CurrMnth_Practice] ON [RPT].[ConsolidatedAttribution_Snapshot]
(
	[Practice] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]

CREATE NONCLUSTERED INDEX [idx_ConsolidatedAttribution_CurrMnth_Prov_Name] ON [RPT].[ConsolidatedAttribution_Snapshot]
(
	[Prov_Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]

CREATE NONCLUSTERED INDEX [idx_ConsolidatedAttribution_CurrMnth_Specialty] ON [RPT].[ConsolidatedAttribution_Snapshot]
(
	[Specialty] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]

CREATE NONCLUSTERED INDEX [idx_ConsolidatedAttribution_CurrMnth_root_companies_id] ON [RPT].[ConsolidatedAttribution_Snapshot]
(
	[ROOT_COMPANIES_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]


*/


/******************************************************************************************************************************/
-------------------------------------------Refreshing Measure DetailLine Snapshot Table-----------------------------------------
/******************************************************************************************************************************/

------Truncating Table
Truncate table [KPI_ENGINE].[RPT].[Measure_DetailLine_Snapshot]



------Dropping Index Before Insertion-----
DROP INDEX [idx_Meas_DetailLineCurrMnth_EMPI] ON [RPT].[Measure_DetailLine_Snapshot] WITH ( ONLINE = OFF )

DROP INDEX [idx_Meas_DetailLineCurrMnth_ENROLLMENT_STATUS] ON [RPT].[Measure_DetailLine_Snapshot]

DROP INDEX [idx_Meas_DetailLineCurrMnth_MEASURE_ID] ON [RPT].[Measure_DetailLine_Snapshot]

DROP INDEX [idx_Meas_DetailLineCurrMnth_REPORT_ID] ON [RPT].[Measure_DetailLine_Snapshot]

DROP INDEX [idx_Meas_DetailLineCurrMnth_ReportType] ON [RPT].[Measure_DetailLine_Snapshot]

DROP INDEX [idx_Meas_DetailLineCurrMnth_root_companies_id] ON [RPT].[Measure_DetailLine_Snapshot]


--------------Inserting the latest report data-----------------------
Insert into [RPT].[Measure_DetailLine_Snapshot]
(
	   [MEASURE_ID]
      ,[MEASURE_NAME]
      ,[MEASURE_TYPE]
      ,[Payer]
      ,[EMPI]
      ,[PayerId]
      ,[MEM_FNAME]
      ,[MEM_MNAME]
      ,[MEM_LNAME]
      ,[MEM_DOB]
      ,[MEM_GENDER]
      ,[ENROLLMENT_STATUS]
      ,[LAST_VISIT_DATE]
      ,[PRODUCT_TYPE]
      ,[NUM]
      ,[DEN]
      ,[EXCL]
      ,[REXCL]
      ,[Code]
      ,[DateofService]
      ,[CE]
      ,[Epop]
      ,[Event]
      ,[DischargeDate]
      ,[outlier]
      ,[Encounters]
      ,[REPORT_ID]
      ,[ReportType]
      ,[REPORT_RUN_DATE]
      ,[Report_Quarter]
      ,[ROOT_COMPANIES_ID]
	  ,Period_Start_Date
	  ,Period_End_Date
	  ,DischargeFacility
	  ,LastSeenDate
	  ,EpisodeDate
)
select 
	   [MEASURE_ID]
      ,[MEASURE_NAME]
      ,[MEASURE_TYPE]
      ,[Payer]
      ,[EMPI]
      ,[PayerId]
      ,[MEM_FNAME]
      ,[MEM_MNAME]
      ,[MEM_LNAME]
      ,[MEM_DOB]
      ,[MEM_GENDER]
      ,[ENROLLMENT_STATUS]
      ,[LAST_VISIT_DATE]
      ,[PRODUCT_TYPE]
      ,[NUM]
      ,[DEN]
      ,[EXCL]
      ,[REXCL]
      ,[Code]
      ,[DateofService]
      ,[CE]
      ,[Epop]
      ,[Event]
      ,[DischargeDate]
      ,[outlier]
      ,[Encounters]
      ,[REPORT_ID]
      ,[ReportType]
      ,[REPORT_RUN_DATE]
      ,[Report_Quarter]
      ,[ROOT_COMPANIES_ID]
	  ,Period_Start_Date
	  ,Period_End_Date
	  ,DischargeFacility
	  ,LastSeenDate
	  ,EpisodeDate
from [RPT].[Measure_DetailLine]
Where Report_Quarter!='1900 - Q4'




---------------------------------------------------------------------

------Recreating Index After Insertion-----
CREATE CLUSTERED INDEX [idx_Meas_DetailLineCurrMnth_EMPI] ON [RPT].[Measure_DetailLine_Snapshot]
(
	[EMPI] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]

CREATE NONCLUSTERED INDEX [idx_Meas_DetailLineCurrMnth_ENROLLMENT_STATUS] ON [RPT].[Measure_DetailLine_Snapshot]
(
	[ENROLLMENT_STATUS] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]

CREATE NONCLUSTERED INDEX [idx_Meas_DetailLineCurrMnth_MEASURE_ID] ON [RPT].[Measure_DetailLine_Snapshot]
(
	[MEASURE_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]

CREATE NONCLUSTERED INDEX [idx_Meas_DetailLineCurrMnth_REPORT_ID] ON [RPT].[Measure_DetailLine_Snapshot]
(
	[REPORT_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]

CREATE NONCLUSTERED INDEX [idx_Meas_DetailLineCurrMnth_ReportType] ON [RPT].[Measure_DetailLine_Snapshot]
(
	[ReportType] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]

CREATE NONCLUSTERED INDEX [idx_Meas_DetailLineCurrMnth_root_companies_id] ON [RPT].[Measure_DetailLine_Snapshot]
(
	[ROOT_COMPANIES_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]




		SET @PERF_ROW = @@ROWCOUNT
		SET @PERF_DURATION = DATEDIFF(MINUTE,@PERF_START,GETDATE());
		EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, 'Enrollment Completed', @PERF_ROW, @DURATION_IN_MIN=@PERF_DURATION, @LOG2ID=@LOGID;
		EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, @LOG2ID=@LOGID, @END_FLAG=1;

END TRY
BEGIN CATCH
		BEGIN
			EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, 'FATAL ERROR', @LOG2ID=@LOGID;
			RAISERROR ('',16,0)
			RETURN 16
        END
	END CATCH
RETURN 0
END

GO
