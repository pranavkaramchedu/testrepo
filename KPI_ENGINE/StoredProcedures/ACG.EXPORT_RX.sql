SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
CREATE PROC [ACG].[EXPORT_RX] @status varchar(20)
AS

BEGIN
/*
DECLARE @INFO VARCHAR(8000)
					,	@TRUE BIT = 1
					,	@FALSE BIT = 0
					,	@DB_ID INT = DB_ID()
					,	@LOGID INT = 0
					,	@PROC VARCHAR(500)=OBJECT_SCHEMA_NAME(@@PROCID)+'.'+OBJECT_NAME(@@PROCID)
					,	@PERF_START DATETIME
					,	@PERF_DURATION INT
					,	@PERF_ROW INT
					,	@RC	INT
					;

		EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, @INFO=@INFO, @LOG2ID=@LOGID OUT;
		DECLARE @PROCFULLNAME VARCHAR(500) = DB_NAME()+'.'+@PROC;
		----EXEC dbo.SP_MI_UTIL_SEND_MAIL @PROCFULLNAME, 'STARTED', 1, @ECHO=0; 
	DECLARE @MSG VARCHAR(1000)
	DECLARE @PERF_START_PROC DATETIME 
	DECLARE @PERF_RPS NUMERIC(10,2) 
	DECLARE @ROWS INT 
	DECLARE @PERF_ROWS INT 
	SET @PERF_ROWS = 0 
	SET @PERF_START = GETDATE() 
	SET @PERF_START_PROC = GETDATE() 

BEGIN TRY

*/

Declare @rootId INT=159

Select 
patient_id
,rx_fill_date
,rx_cd
,rx_code_type
,CONVERT(int,CASE WHEN rx_days_supply like '%.%' then LEFT(rx_days_supply,(CHARINDEX('.',rx_days_supply)-1)) else rx_days_supply END) AS rx_days_supply
,CONVERT(int,CASE WHEN rx_cost like '%.%' then LEFT(rx_cost,(CHARINDEX('.',rx_cost)-1)) else rx_cost END) AS rx_cost
,CONVERT(int,CASE WHEN rx_quantity like '%.%' then LEFT(rx_quantity,(CHARINDEX('.',rx_quantity)-1)) else rx_quantity END) AS rx_quantity
,[source]
FROM
(
Select DISTINCT
s.EMPI AS patient_id
,RX.FILL_DATE AS rx_fill_date
,RX.MEDICATION_CODE AS rx_cd
,CASE WHEN RX.MEDICATION_CODE_TYPE='NDC' THEN 'N' END AS rx_code_type
,RX.SUPPLY_DAYS AS rx_days_supply
,CL.AMT_ALLOWED AS rx_cost
,RX.QUANTITY AS rx_quantity
,'CLAIMS' AS [source]
,ROW_NUMBER() over( partition by CL.EMPI,RX.FILL_DATE,RX.MEDICATION_CODE order by RX.SUPPLY_DAYS desc) as RN
FROM [KPI_ENGINE].DBO.[CLAIMLINE] CL
INNER JOIN [KPI_ENGINE].DBO.MEDICATION RX
ON CL.CLAIM_ID=RX.CLAIM_ID AND CL.FROM_DATE=RX.FILL_DATE 
AND CL.CL_DATA_SRC=RX.MED_DATA_SRC AND CL.ROOT_COMPANIES_ID=RX.ROOT_COMPANIES_ID
Join RPT.ConsolidatedAttribution_Snapshot s on convert(varchar(100),cl.EMPI)=s.EMPI and s.Attribution_Type='Ambulatory_PCP' and s.EnrollmentStatus=@status
where rx.ROOT_COMPANIES_ID=@rootId
) T
where RN=1

/*
		SET @PERF_ROW = @@ROWCOUNT
		SET @PERF_DURATION = DATEDIFF(MINUTE,@PERF_START,GETDATE());
		EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, 'Enrollment Completed', @PERF_ROW, @DURATION_IN_MIN=@PERF_DURATION, @LOG2ID=@LOGID;
		EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, @LOG2ID=@LOGID, @END_FLAG=1;

END TRY
BEGIN CATCH
		BEGIN
			EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, 'FATAL ERROR', @LOG2ID=@LOGID;
			RAISERROR ('',16,0)
			RETURN 16
        END
	END CATCH
RETURN 0
*/
END

GO
