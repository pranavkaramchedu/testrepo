SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON


CREATE PROCEDURE HDS.RUN_HEDIS_TRC  @meas_year nvarchar(4)
AS
-- Declare Variables

Declare @runid INT=0;
DECLARE @ce_startdt DATE;
DECLARE @ce_enddt DATE;
DECLARE @ce_dischenddt DATE;
DECLARE @meas VARCHAR(10);


SET @meas='TRC';
SET @ce_startdt=concat(@meas_year,'0101');
SET @ce_enddt=concat(@meas_year,'1231');
SET @ce_dischenddt=concat(@meas_year,'1201');


-- hospice Exclusion

drop table if exists #trc_hospicemembers;

CREATE table #trc_hospicemembers
(
	Memid varchar(50)
		
);

	

Insert into #trc_hospicemembers
select distinct t1.MemID from
(
	Select Memid from HDS.HEDIS_VISIT_E where Measure_id=@meas and sdate!='' and sdate between @ce_startdt and @ce_enddt and activity IN(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Hospice Encounter','Hospice Intervention'))

	Union all
	
	Select Memid from HDS.HEDIS_OBS where Measure_id=@meas and Date!='' and Date between @ce_startdt and @ce_enddt and ocode IN(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Hospice Encounter','Hospice Intervention'))


	Union all

	select Beneficiary_id as Memid from HDS.Hedis_MMDF where Measure_id=@meas and Run_date!='' and Run_date between @ce_startdt and @ce_enddt and Hospice='Y'

	Union all

	select MemID from HDS.HEDIS_VISIT v
	where Measure_id=@meas and HCFAPOS!=81 and Date_S!='' and Date_S between @ce_startdt and @ce_enddt
	and (
	CPT in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name ='Hospice Intervention')
	or
	HCPCS in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or
	Rev in(select code from HDS.VALUESET_TO_CODE where code_system='UBREV' and Value_Set_Name ='Hospice Encounter')
	)
	)t1
	
CREATE CLUSTERED INDEX idx_hospicemembers ON #trc_hospicemembers ([memid]);



-- Identify Inpatient Records
drop table if exists #trc_ipstaylist;

Create table #trc_ipstaylist
(
	Memid varchar(50),
	Date_S varchar(8),
	Date_Adm varchar(8),
	Date_Disch varchar(8),
	Claimid INT
)
Insert into #trc_ipstaylist
select v.Memid,Date_S,Date_Adm,Date_Disch,ClaimID from hds.Hedis_visit v
join hds.hedis_member_gm gm on v.memid=gm.memid and year(@ce_enddt)-year(gm.dob)>=18 and v.MEASURE_ID=gm.MEASURE_ID
--left outer join #trc_hospicemembers h on v.MemID=h.Memid
where v.measure_id=@meas and Date_Disch!='' and hCFAPOS!=81 
--and h.Memid is null 
and v.SuppData='N'
and REV in(select code from HDS.VALUESET_TO_CODE where Code_System='UBREV' and Value_Set_Name='Inpatient Stay')


-- Identify Non Acute IP
drop table if exists #trc_naipstaylist;

Create table #trc_naipstaylist
(
	Memid varchar(50),
	Date_S varchar(8),
	Date_Adm varchar(8),
	Date_Disch varchar(8),
	Claimid INT
)
Insert into #trc_naipstaylist
select v.Memid,Date_S,Date_Adm,Date_Disch,ClaimID from hds.Hedis_visit v
where v.measure_id=@meas and Date_Disch!='' and hCFAPOS!=81
and (
	REV in(select code from HDS.VALUESET_TO_CODE where Code_System='UBREV' and Value_Set_Name='Nonacute Inpatient Stay')
	or
	RIGHT('0000'+CAST(Trim(BillType) AS VARCHAR(4)),4) in(select code from HDS.VALUESET_TO_CODE where Code_System='UBTOB' and Value_Set_Name='Nonacute Inpatient Stay')
)



-- Identifying Acute Stays with Non Acute
drop table if exists #trc_acutestayexcl;
Create table #trc_acutestayexcl
(
	Memid varchar(50),
	claimid INT
)
Insert into #trc_acutestayexcl
select i.Memid,i.claimid from #trc_ipstaylist i
join #trc_naipstaylist na on i.Memid=na.Memid and i.Date_Adm>na.Date_Adm and i.Date_Disch<na.Date_Disch




-- Creating set of Inpatient stays excluding acute stays which happens during Non acute stay
drop table if exists #trc_ipstaylistwooverlap
create table #trc_ipstaylistwooverlap
(
	memid varchar(50),
	AdmissionDate varchar(8),
	DischargeDate varchar(8),
)
Insert into #trc_ipstaylistwooverlap
select i.Memid,i.Date_Adm,i.Date_Disch from #trc_ipstaylist i
left outer join #trc_acutestayexcl a on i.Memid=a.Memid and i.Claimid=a.claimid
where
a.Memid is null


-- Merging stays and readmissions and restricting discharges between 20200101 and 20201201
drop table if exists #trc_base1;
Create table #trc_base1
(
	Memid varchar(50),
	AdmissionDate varchar(8),
	DischargeDate varchar(8),
	StayNumber INT
)

;with grp_starts as (
  select Memid,AdmissionDate, DischargeDate,
  case
    when datediff(day,lag(DischargeDate) over(partition by Memid order by AdmissionDate, DischargeDate),AdmissionDate) between 0 and 30
    then 0 else 1
  end grp_start
  from #trc_ipstaylistwooverlap

)
, grps as (
  select Memid,AdmissionDate, DischargeDate,
  sum(grp_start) over(partition by Memid order by AdmissionDate, DischargeDate) grp
  from grp_starts
)
Insert into #trc_base1
select t2.* from(
select t1.*,DENSE_RANK() over(partition by Memid order by AdmissionDate,DischargeDate) as  StayNumber from(
select Memid,min(AdmissionDate) as AdmissionDate,max(DischargeDate) as DischargeDate
from grps 
group by Memid,grp
)t1 where dischargedate between @ce_startdt and @ce_dischenddt
)t2 where StayNumber<=2


-- Adding Payer Mapping
Drop table if exists #trc_payermapping;
Create table #trc_payermapping
(
	Memid varchar(50),
	Payer varchar(10),
	StayNumber INT
)	


;With mappedpayers as
(
	
	select f1.memid,f1.AdmissionDate,f1.DischargeDate,f1.StayNumber,
	case
		when en_payer is null and en1_payer is null and en2_payer is not null then en2_payer
		when en_payer is null and en2_payer is not null  then en1_payer
		when en_payer is not null then en_payer
	end as Payer
	from (
	select b.*,en.Payer as en_payer,en1.Payer as en1_payer,en2.Payer as en2_payer 
	from #trc_base1 b
	left outer join hds.hedis_member_en en on b.Memid=en.Memid and en.Measure_id=@meas and dateadd(day,30,b.DischargeDate) between en.StartDate and en.FinishDate
	left outer join HDS.HEDIS_MEMBER_EN en1 on b.memid=en1.memid and en1.StartDate<=dateadd(day,30,b.DischargeDate) and en1.FinishDate>=b.DischargeDate and en1.StartDate>dateadd(day,30,b.DischargeDate)	and en1.MEASURE_ID=@meas
	left outer join
	( 
		select * from(
		select *,row_number() over(partition by Memid,dischargedate order by daysdiff) as rn from(
		select b.*,en.Payer,datediff(day,en.FinishDate,b.DischargeDate) as daysdiff from #trc_base1 b
		left outer join HDS.HEDIS_MEMBER_EN en on b.memid=en.memid 
		and en.StartDate<=dateadd(day,30,b.DischargeDate) and en.FinishDate>=b.DischargeDate and en.FinishDate<dateadd(day,30,b.DischargeDate)
		and en.MEASURE_ID=@meas
		)t1 
		)t2
		where rn=1 and Payer is not null
	)en2 on b.Memid=en2.Memid and b.DischargeDate=en2.DischargeDate
	--where b.Memid=100002          
	)f1 
	
	
	
	
)

Insert into #trc_payermapping(Memid,Payer,StayNumber)
select t5.Memid,m.PayerMapping as Payer,t5.StayNumber from(
select distinct t4.* from(
select 	t3.Memid
	,t3.StayNumber
	,newpayer as Payer
	
from(
	select t2.*,
	-- Applying Dual enrollment for Payer field
		case 
			when (Payer in('HMO','CEP','POS','PPO') and nxtpayer in('MD','MLI','MRB')) and staynumber=nxtstaynumber then Payer
			When (Payer in('HMO','CEP','POS','PPO') and prvpayer in('MD','MLI','MRB')) and staynumber=prvstaynumber then Payer
			When (Payer in('MD','MLI','MRB') and nxtpayer in('HMO','CEP','POS','PPO')) and staynumber=nxtstaynumber then nxtpayer
			When (Payer in('MD','MLI','MRB') and prvpayer in('HMO','CEP','POS','PPO')) and staynumber=prvstaynumber then prvpayer
			when (Payer in('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP') and nxtpayer in('HMO','CEP','POS','PPO')) and staynumber=nxtstaynumber then Payer
			When (Payer in('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP') and prvpayer in('HMO','CEP','POS','PPO')) and staynumber=prvstaynumber then Payer
			When (Payer in('HMO','CEP','POS','PPO') and nxtpayer in('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP')) and staynumber=nxtstaynumber then nxtpayer
			When (Payer in('HMO','CEP','POS','PPO') and prvpayer in('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP')) and staynumber=prvstaynumber then prvpayer
			else Payer 
		end as newpayer
		from(
			select 
				m1.*
				,isnull(lead(Payer,1) over(partition by Memid order by Memid,staynumber),Payer) as nxtpayer
				,isnull(lag(Payer,1) over(partition by Memid order by Memid,staynumber),Payer) as prvpayer
				,isnull(lead(Staynumber,1) over(partition by Memid order by Memid,staynumber),Staynumber) as nxtStaynumber
				,isnull(lag(Staynumber,1) over(partition by Memid order by Memid,staynumber),Staynumber) as prvStaynumber
			from mappedpayers m1
		)t2
	)t3 
)t4 
)t5
join 
HDS.Hedis_payer_Mapping m on t5.Payer=m.payer and m.Measure_ID='TRC'



-- Create trcdataset

-- Create temp table to store BCS data
drop table if exists #trcdataset;

CREATE TABLE #trcdataset (
  [MemID] varchar(100) NOT NULL,
  [Meas] varchar(20) DEFAULT NULL,
  [Payer] varchar(100) DEFAULT NULL,
  [Gender] varchar(45) NOT NULL,
  [Age] decimal(12,4) DEFAULT NULL,
  [Rexcl] smallint DEFAULT '0',
  [Rexcld] smallint DEFAULT '0',
  [CE] smallint DEFAULT '0',
  [Excl] smallint DEFAULT '0',
  [Num] smallint DEFAULT '0',
  [Event] smallint DEFAULT '1'
  
  
) ;


Insert into #trcdataset(Memid,Meas,Payer,Gender,Age)
select t1.*,gm.Gender,Year(@ce_enddt) - Year(gm.dob) as Age from(
select Memid,concat('TRC','E',char(64+staynumber)) as meas,Payer from #trc_payermapping 
Union all
select Memid,concat('TRC','M',char(64+staynumber)) as meas,Payer from #trc_payermapping 
)t1
join HDS.HEDIS_MEMBER_GM gm on t1.Memid=gm.MemID and gm.MEASURE_ID=@meas


update #trcdataset set Rexcl=1 from #trcdataset ds join #trc_hospicemembers h on ds.MemID=h.Memid



-- Numerator Logic
-- Patient Engagement
Drop table if exists #trc_PEvstlist;
Create table #trc_PEvstlist
(
	Memid varchar(50),
	servicedate varchar(8)
)
Insert into #trc_PEvstlist
select distinct Memid,servicedate from(

select Memid,Date_S as servicedate  from HDS.HEDIS_VISIT where MEASURE_ID=@meas and Date_S!='' and Date_S between @ce_startdt and @ce_enddt and HCFAPOS!=81 and (
	CPT in(select code from HDS.VALUESET_TO_CODE where value_set_Name in('Outpatient','Telephone Visits','Transitional Care Management Services','Online Assessments'))
	or
	HCPCS in(select code from HDS.VALUESET_TO_CODE where value_set_Name in('Outpatient','Online Assessments'))
	or
	REV in(select code from HDS.VALUESET_TO_CODE where Code_System='UBREV' and value_set_Name='Outpatient')
	)

Union All

Select Memid,sdate as servicedate from HDS.HEDIS_VISIT_E where MEASURE_ID=@meas and sdate!='' and sdate between @ce_startdt and @ce_enddt and activity in(select code from HDS.VALUESET_TO_CODE where value_set_Name in('Outpatient','Telephone Visits','Transitional Care Management Services','Online Assessments'))

)t1

drop table if exists #trc_PENumList;
Create table #trc_PENumList
(
	Memid varchar(50),
	Meas varchar(10)
)
Insert into #trc_PENumList
select distinct b.Memid,concat('TRCE',char(64+b.staynumber)) as Meas from #trc_base1 b
join #trc_PEvstlist v on b.Memid=v.Memid and v.servicedate between DATEADD(day,1,b.DischargeDate) and DATEADD(day,30,b.DischargeDate)


update #trcdataset set Num=1 from #trcdataset ds join #trc_PENumList n on ds.MemID=n.Memid and ds.Meas=n.Meas

-- Medication Reconciliation

Drop table if exists #trc_medreconvstlist;
Create table #trc_medreconvstlist
(
	Memid varchar(50),
	servicedate varchar(8)
)
Insert into #trc_medreconvstlist
select distinct Memid,servicedate from(

select Memid,Date_S as servicedate  from HDS.HEDIS_VISIT where MEASURE_ID=@meas and Date_S!='' and Date_S between @ce_startdt and @ce_enddt and HCFAPOS!=81 and 	CPT in(select code from HDS.VALUESET_TO_CODE where value_set_Name in('Medication Reconciliation Encounter'))
and ProvID in(select ProvID from HDS.HEDIS_PROVIDER where MEASURE_ID=@meas and 'Y' in(RN,PHAProv,ProvPres))

Union all

select Memid,Date_S as servicedate  from HDS.HEDIS_VISIT where MEASURE_ID=@meas and Date_S!='' and Date_S between @ce_startdt and @ce_enddt and HCFAPOS!=81 and 	CPT2 in(select code from HDS.VALUESET_TO_CODE where value_set_Name in('Medication Reconciliation Intervention'))
and CPT2Mod not in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name='CPT CAT II Modifier')
and ProvID in(select ProvID from HDS.HEDIS_PROVIDER where MEASURE_ID=@meas and 'Y' in(RN,PHAProv,ProvPres))


Union All

Select Memid,sdate as servicedate from HDS.HEDIS_VISIT_E where MEASURE_ID=@meas and sdate!='' and sdate between @ce_startdt and @ce_enddt and activity in(select code from HDS.VALUESET_TO_CODE where value_set_Name in('Medication Reconciliation Encounter'))
and providerid in(select ProvID from HDS.HEDIS_PROVIDER where MEASURE_ID=@meas and 'Y' in(RN,PHAProv,ProvPres))

Union All

select z1.* from(
Select Memid,sdate as servicedate from HDS.HEDIS_PROC where MEASURE_ID=@meas and pstatus='EVN' and sdate!='' and sdate between @ce_startdt and @ce_enddt and pcode in(select code from HDS.VALUESET_TO_CODE where value_set_Name in('Medication Reconciliation Intervention'))
)z1
join HDS.HEDIS_VISIT_E v on z1.MemID=v.MemID and z1.servicedate=v.sdate and v.providerid in(select ProvID from HDS.HEDIS_PROVIDER where MEASURE_ID=@meas and 'Y' in(RN,PHAProv,ProvPres))

)t1

drop table if exists #trc_MedRecNumList;
Create table #trc_MedRecNumList
(
	Memid varchar(50),
	Meas varchar(10)
)
Insert into #trc_MedRecNumList
select distinct b.Memid,concat('TRCM',char(64+b.staynumber)) as Meas from #trc_base1 b
join #trc_medreconvstlist v on b.Memid=v.Memid and v.servicedate between b.DischargeDate and DATEADD(day,30,b.DischargeDate)


update #trcdataset set Num=1 from #trcdataset ds join #trc_MedRecNumList n on ds.MemID=n.Memid and ds.Meas=n.Meas

-- Continuous Enrollment




-- Check for Continuous Enrollment
drop table if exists #trc_contenroll;
create table #trc_contenroll
(
	Memid varchar(50),
	StayNumber INT
);


--Check for continuous enrollment
				
--CTE1 to check covergae during last 365 days
With coverage_CTE1 as
(

	select 
	b.Memid
	,dischargedate
	,staynumber
	,isnull(lag(en.Finishdate,1) over(partition by b.Memid,staynumber order by en.Startdate,en.FinishDate desc),b.DischargeDate) as lastcoveragedate
	,Case 
		when en.StartDate<b.DischargeDate then b.DischargeDate
		else en.startdate 
	end as Startdate
	,case 
		when en.Finishdate>dateadd(day,30,b.dischargedate) then convert(varchar,cast(dateadd(day,30,b.dischargedate) as date),112)
		else en.finishdate 
	end as Finishdate
	,isnull(lead(en.Startdate,1) over(partition by b.Memid,staynumber order by Startdate,FinishDate),convert(varchar,cast(dateadd(day,30,b.dischargedate) as date),112)) as nextcoveragedate
	from #trc_base1 b
	join HDS.HEDIS_MEMBER_EN en on b.memid=en.MemID and en.MEASURE_ID=@meas
	where  en.Startdate<=dateadd(day,30,b.dischargedate) and en.Finishdate>=b.DischargeDate
	--and b.Memid=101847
)
Insert into #trc_contenroll
select t3.Memid,t3.staynumber from(
select t2.Memid,t2.staynumber,sum(anchor) as anchor from(
	
		select 
			*
			, case 
				when rn=1 and startdate>DischargeDate then 1 
				else 0 
				end as startgap
			,case 
				when datediff(day,newFinishdate,nextcoveragedate)>0 then 1  
				else 0 
				end as gaps
			,datediff(day,Startdate,Finishdate)+1 as coveragedays
			,case
				when dateadd(day,30,dischargedate) between Startdate and newfinishdate then 1 
				else 0 
				end as anchor 
				from(
					Select 
						*
						,case 
							when datediff(day,Finishdate,nextcoveragedate) <=1 then isnull(lead(Finishdate,1) over(partition by Memid,staynumber order by Startdate,FinishDate),convert(varchar,cast(dateadd(day,30,dischargedate) as date),112)) 
							else finishdate 
							end as newfinishdate
						,ROW_NUMBER() over(partition by memid,staynumber order by Startdate,Finishdate) as rn 
						from coverage_CTE1
				)t1           
		)t2  
			group by memid,staynumber having(sum(gaps)+sum(startgap)=0) and sum(coveragedays)>=31
	)t3


update #trcdataset set CE=1 from #trcdataset ds join #trc_contenroll ce on ds.MemID=ce.Memid and SUBSTRING(ds.Meas,5,1)=char(64+ce.StayNumber)


	-- Generate Output
	select @runid=max(RUN_ID) from HEDIS_MEASURE_OUTPUT where measure_id=@meas;

	if(@runid>=1)
	Begin
		SET @runid=@runid+1;
	End
	Else
	Begin
		SET @runid=1;
	END
	

	Insert into HEDIS_MEASURE_OUTPUT(memid,Meas,payer,CE,Event,Epop,Excl,Num,Rexcl,RExclD,Age,Gender,Measure_ID,Measurement_year,RUN_ID)
	SELECT memid,meas,payer,CE,EVENT,CASE WHEN CE=1 and Event=1 AND rexcl=0 and rexcld=0 and payer in('MCR','MCS','MP','SN1','SN2','SN3','MMP') THEN 1 ELSE 0 END AS epop,excl,num,rexcl,rexcld,cast(age as Int) AS age,gender,@meas,@meas_year,@runid FROM #trcdataset

GO
