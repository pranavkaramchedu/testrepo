SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
CREATE Proc [REG].[RUN_REGISTRY_AdultWellness]
As

BEGIN
DECLARE @INFO VARCHAR(8000)
					,	@TRUE BIT = 1
					,	@FALSE BIT = 0
					,	@DB_ID INT = DB_ID()
					,	@LOGID INT = 0
					,	@PROC VARCHAR(500)=OBJECT_SCHEMA_NAME(@@PROCID)+'.'+OBJECT_NAME(@@PROCID)
					,	@PERF_START DATETIME
					,	@PERF_DURATION INT
					,	@PERF_ROW INT
					,	@RC	INT
					;

		EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, @INFO=@INFO, @LOG2ID=@LOGID OUT;
		DECLARE @PROCFULLNAME VARCHAR(500) = DB_NAME()+'.'+@PROC;
		----EXEC dbo.SP_MI_UTIL_SEND_MAIL @PROCFULLNAME, 'STARTED', 1, @ECHO=0; 
	DECLARE @MSG VARCHAR(1000)
	DECLARE @PERF_START_PROC DATETIME 
	DECLARE @PERF_RPS NUMERIC(10,2) 
	DECLARE @ROWS INT 
	DECLARE @PERF_ROWS INT 
	SET @PERF_ROWS = 0 
	SET @PERF_START = GETDATE() 
	SET @PERF_START_PROC = GETDATE() 

BEGIN TRY


declare @rundate Date=GetDate();
declare @meas_year varchar(4)=Year(Dateadd(month,-2,@rundate))
declare @rootId INT=159
declare @attributiondate Date=DATEADD(month, DATEDIFF(month, 0, Dateadd(month,-2,@rundate)), 0)

Declare @startDate Date;
Declare @enddate date;
Declare @quarter varchar(20)

Declare @reportId INT;


-- Get ReportId from Report_Details

	exec GetReportDetail @rundate=@rundate,@rootId=@rootId,@startDate=@startDate output,@enddate=@enddate output,@quarter=@quarter output,@reportId=@reportId output


--Insert data in Registry Out Line
		Drop table if exists #adultwellness;
		Create Table #adultwellness
		(
				RegistryName varchar(50),
				EMPI varchar(100),
				Payer varchar(50),
				PayerId varchar(50),
				MemberLastName varchar(100),
				MemberFirstName varchar(100),
				MemberMiddleName varchar(50),
				MemberDOB Date,
				MEM_GENDER varchar(20),
				EnrollmentStatus varchar(20),
				PracticeName varchar(200),
				AttributedProviderName varchar(200),
				AttributedProviderSpecialty varchar(100),
				AttributedProviderNPI varchar(20),
				ROOT_COMPANIES_ID INT,
				rnk INT
		)
		Insert into #adultwellness
		select 
			'Adult Wellness' as RegistryName
			,EMPI
			,Payer
			,PayerId
			,MemberLastName
			,MemberFirstName
			,MemberMiddleName
			,MemberDOB
			,MEM_GENDER
			,EnrollmentStatus
			,PracticeName
			,AttributedProviderName
			,AttributedProviderSpecialty
			,AttributedProviderNPI
			,@rootId as ROOT_COMPANIES_ID
			,rnk
			
		from
		(
			select *,row_number() over(partition by EMPI order by EMPI) as rnk
			from(
			select 
				m.EMPI_ID as EMPI
				,Org_Patient_Extension_ID as PayerId
				,Last_Name as MemberLastName
				,First_Name as MemberFirstName
				,Middle_Name as MemberMiddleName
				,Date_of_Birth as MemberDOB
				,Gender as MEM_GENDER
				,EnrollmentStatus
				,CASE WHEN dateadd(year, datediff (year, Date_of_Birth,eomonth(@enddate)), Date_of_Birth) > eomonth(@enddate)
						THEN datediff(year, Date_of_Birth, eomonth(@enddate)) - 1
						ELSE datediff(year, Date_of_Birth, eomonth(@enddate))
				   END as Age
				,a.DATA_SOURCE as Payer
				,a.AmbulatoryPCPPractice as PracticeName
				,a.AmbulatoryPCPName as AttributedProviderName
				,a.AmbulatoryPCPSpecialty as AttributedProviderSpecialty
				,a.AmbulatoryPCPNPI as AttributedProviderNPI
			from KPI_ENGINE.dbo.open_empi_master m
			join KPI_ENGINE.RPT.PCP_ATTRIBUTION a on 
				m.EMPI_ID=a.EMPI  and 
				a.AssignedStatus='Assigned' and 
				a.ReportId=@reportId
			--where EMPI_ID='3B68068E-C55C-4C3F-9D02-6448169BD854'
			)tmp
			where age > 21 
		)t1

		
	-- Insert or delete the report if it exists

	
		Delete from KPI_ENGINE.REG.REGISTRY_OUTPUT_LINE where ReportId=@reportId and RegistryName='Adult Wellness' and ROOT_COMPANIES_ID=@rootId;;
		

		Insert into KPI_ENGINE.REG.REGISTRY_OUTPUT_LINE(RegistryName,EMPI,Payer,PayerId,MemberLastName,MemberFirstName,MemberMiddleName,MemberDOB,MEM_GENDER,EnrollmentStatus,PracticeName,AttributedProviderName,AttributedProviderSpecialty,AttributedProviderNPI,ROOT_COMPANIES_ID,ReportId)
		select RegistryName,EMPI,Payer,PayerId,MemberLastName,MemberFirstName,MemberMiddleName,MemberDOB,MEM_GENDER,EnrollmentStatus,PracticeName,AttributedProviderName,AttributedProviderSpecialty,AttributedProviderNPI,ROOT_COMPANIES_ID,@reportId from #adultwellness


		Delete from KPI_ENGINE.REG.REGISTRY_OUTPUT where ReportId=@reportId and RegistryName='Adult Wellness' and ROOT_COMPANIES_ID=@rootId;

		Insert into KPI_ENGINE.REG.REGISTRY_OUTPUT(RegistryName,EMPI,Payer,PayerId,MemberLastName,MemberFirstName,MemberMiddleName,MemberDOB,MEM_GENDER,EnrollmentStatus,PracticeName,AttributedProviderName,AttributedProviderSpecialty,AttributedProviderNPI,ROOT_COMPANIES_ID,ReportId)
		select RegistryName,EMPI,Payer,PayerId,MemberLastName,MemberFirstName,MemberMiddleName,MemberDOB,MEM_GENDER,EnrollmentStatus,PracticeName,AttributedProviderName,AttributedProviderSpecialty,AttributedProviderNPI,ROOT_COMPANIES_ID,@reportId from #adultwellness where rnk=1

		
		SET @PERF_ROW = @@ROWCOUNT
		SET @PERF_DURATION = DATEDIFF(MINUTE,@PERF_START,GETDATE());
		EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, 'Enrollment Completed', @PERF_ROW, @DURATION_IN_MIN=@PERF_DURATION, @LOG2ID=@LOGID;
		EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, @LOG2ID=@LOGID, @END_FLAG=1;

END TRY
BEGIN CATCH
		BEGIN
			EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, 'FATAL ERROR', @LOG2ID=@LOGID;
			RAISERROR ('',16,0)
			RETURN 16
        END
	END CATCH
RETURN 0
END

GO
