SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
CREATE PROC [ACG].[EXPORT_PATIENT] @status varchar(20)


AS



BEGIN
/*
DECLARE @INFO VARCHAR(8000)
					,	@TRUE BIT = 1
					,	@FALSE BIT = 0
					,	@DB_ID INT = DB_ID()
					,	@LOGID INT = 0
					,	@PROC VARCHAR(500)=OBJECT_SCHEMA_NAME(@@PROCID)+'.'+OBJECT_NAME(@@PROCID)
					,	@PERF_START DATETIME
					,	@PERF_DURATION INT
					,	@PERF_ROW INT
					,	@RC	INT
					;

		EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, @INFO=@INFO, @LOG2ID=@LOGID OUT;
		DECLARE @PROCFULLNAME VARCHAR(500) = DB_NAME()+'.'+@PROC;
		----EXEC dbo.SP_MI_UTIL_SEND_MAIL @PROCFULLNAME, 'STARTED', 1, @ECHO=0; 
	DECLARE @MSG VARCHAR(1000)
	DECLARE @PERF_START_PROC DATETIME 
	DECLARE @PERF_RPS NUMERIC(10,2) 
	DECLARE @ROWS INT 
	DECLARE @PERF_ROWS INT 
	SET @PERF_ROWS = 0 
	SET @PERF_START = GETDATE() 
	SET @PERF_START_PROC = GETDATE() 

BEGIN TRY

*/


select 
patient_id
,Age
,sex
,line_of_business
,company
,product
,employer_group_id
,employer_group_name
,benefit_plan
,health_system
,pcp_id
,pcp_name
,pcp_group_id
,pcp_group_name
,pregnant
,delivered
,low_birthweight
,pharmacy_cost
,total_cost
,inpatient_hospitalization_count
,inpatient_hospitalization_days
,emergency_visit_count
,outpatient_visit_count
,dialysis_service
,nursing_service
,major_procedure
,cancer_treatment
,care_management_program
,date_of_birth
,psychotherapy_service
,mechanical_ventilation
,cal_ssa
,all_cause_inpatient_hospitalization_count
,unplanned_inpatient_hospitalization_count
,readmission_30_day_count
,unplanned_readmission_30_day_count
,observation_stay
,observation_day_count
from
(
Select 
M.EMPI AS patient_id
,FLOOR(DATEDIFF(DAY, MemberDOB,GETDATE()) / 365.25) AS Age
,MEM_GENDER AS sex
,CASE WHEN Payer='MSSP' then 'Medicare' else CASE WHEN  Payer in ('BCBS-UHS','BRIGHT','CIGNA','HUMANA') then 'COMM' END END AS line_of_business
,NULL AS company
,E.PAYER_TYPE AS product
,NULL AS employer_group_id
,NULL AS employer_group_name
,E.BEN_PKG_ID AS benefit_plan
,NULL AS health_system
,m.NPI AS pcp_id
,NULL AS pcp_name
,NULL AS pcp_group_id
,NULL AS pcp_group_name
,NULL AS pregnant
,NULL AS delivered
,NULL AS low_birthweight
,NULL AS pharmacy_cost
,NULL AS total_cost
,NULL AS inpatient_hospitalization_count
,NULL AS inpatient_hospitalization_days
,NULL AS emergency_visit_count
,NULL AS outpatient_visit_count
,NULL AS dialysis_service
,NULL AS nursing_service
,NULL AS major_procedure
,NULL AS cancer_treatment
,NULL AS care_management_program
,m.MemberDOB AS date_of_birth
,NULL AS psychotherapy_service
,NULL AS mechanical_ventilation
,NULL AS cal_ssa
,NULL AS all_cause_inpatient_hospitalization_count
,NULL AS unplanned_inpatient_hospitalization_count
,NULL AS readmission_30_day_count
,NULL AS unplanned_readmission_30_day_count
,NULL AS observation_stay
,NULL AS observation_day_count
,ROW_NUMBER() over(Partition by M.empi order by e.EFF_DATE desc,e.TERM_DATE Desc) as RN
from RPT.ConsolidatedAttribution_Snapshot M
inner join dbo.enrollment E
on m.EMPI=E.EMPI and m.Payer=E.EN_DATA_SRC and m.ROOT_COMPANIES_ID=E.ROOT_COMPANIES_ID
where Payer in ('BCBS-UHS','BRIGHT','CIGNA','HUMANA','MSSP') and m.EnrollmentStatus=@status and m.Attribution_Type='Ambulatory_PCP' 
--and m.EMPI in (select distinct TOP 500 EMPI from dbo.MEMBER where MEM_DATA_SRC='MSSP' order by EMPI)
--and m.empi='{0002D469-2227-4222-BDF0-5F44F8979A7A}'
) T where RN=1
order by patient_id;

/*
		SET @PERF_ROW = @@ROWCOUNT
		SET @PERF_DURATION = DATEDIFF(MINUTE,@PERF_START,GETDATE());
		EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, 'Enrollment Completed', @PERF_ROW, @DURATION_IN_MIN=@PERF_DURATION, @LOG2ID=@LOGID;
		EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, @LOG2ID=@LOGID, @END_FLAG=1;

END TRY
BEGIN CATCH
		BEGIN
			EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, 'FATAL ERROR', @LOG2ID=@LOGID;
			RAISERROR ('',16,0)
			RETURN 16
        END
	END CATCH
RETURN 0
*/
END

GO
