SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
CREATE PROC HDS.LOAD_PROC_TO_HEDIS
AS
BEGIN

INSERT INTO [KPI_ENGINE].[HDS].[HEDIS_PROC]
(
	   [MemID]
      ,[sdate]
      ,[autdate]
      ,[reldate]
      ,[pcode]
      ,[pflag]
      ,[edate]
      ,[pstatus]
      ,[attcode]
      ,[atttext]
      ,[dvice]
      ,[negation]
      ,[prcode]
      ,[rdate]
      ,[qrda]
      ,[MEASURE_ID]
      ,[LOAD_DATE]
)
SELECT EMPI AS [MemID]
      ,PROC_START_DATE AS [sdate]
      ,NULL AS [autdate]
      ,NULL AS [reldate]
      ,PROC_CODE AS [pcode]
      ,CASE WHEN PROC_CODE_TYPE in ('CPT','CPT-CAT-II','',NULL) then 'C' when PROC_CODE_TYPE='HCPCS' then 'H' when PROC_CODE_TYPE='ADA' then 'D' END AS [pflag]
      ,PROC_END_DATE AS [edate]
      ,'EVN' AS [pstatus]
      ,NULL AS [attcode]
      ,NULL AS [atttext]
      ,NULL AS [dvice]
      ,NULL AS [negation]
      ,NULL AS [prcode]
      ,NULL AS [rdate]
      ,NULL AS [qrda]
      ,'159' AS [MEASURE_ID]
      ,CONVERT(DATE,GETDATE()) AS [LOAD_DATE]
---FROM [KPI_ENGINE].[HDS].[HEDIS_PROC]
FROM KPI_ENGINE.DBO.PROCEDURES
WHERE PROC_CODE_TYPE<>''

END
GO
