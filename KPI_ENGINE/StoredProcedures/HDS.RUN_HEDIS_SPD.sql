SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON






CREATE PROCEDURE [HDS].[RUN_HEDIS_SPD]  @meas_year nvarchar(4)
AS
-- Declare Variables

DECLARE @pt_ctr INT=0;
Declare @runid INT=0;
DECLARE @i INT= 0;
DECLARE @mem_ptr VARCHAR(10);

DECLARE @gender VARCHAR(20);
DECLARE @age VARCHAR(10);
DECLARE @latest_insdate DATETIME;
DECLARE @latest_insenddate DATETIME;
DECLARE @ce_startdt varchar(8);
DECLARE @ce_enddt varchar(8);
DECLARE @ce_startdt1 varchar(8);
DECLARE @ce_enddt1 varchar(8);
declare @patientid varchar(20);
DECLARE @plan_ct INT=0;
DECLARE @planid VARCHAR(10);
DECLARE @plan1 VARCHAR(10);
DECLARE @plan2 VARCHAR(10);
DECLARE @meas VARCHAR(10);
DECLARE @j INT =0;
DECLARE @t_planid VARCHAR(10);  

SET @meas='SPD';
SET @ce_startdt=concat(@meas_year,'0101');
SET @ce_enddt=concat(@meas_year,'1231');
SET @ce_startdt1=concat(@meas_year-1,'0101');
SET @ce_enddt1=concat(@meas_year-1,'1231');





-- Create temp table to store SPD data
drop table if exists #spddataset;

CREATE TABLE #spddataset (
  [patient_id] varchar(100) NOT NULL,
  [meas] varchar(20) DEFAULT NULL,
  [payer] varchar(100) DEFAULT NULL,
  [patient_gender] varchar(45) NOT NULL,
  [age] decimal(12,4) DEFAULT NULL,
  [orec] smallint DEFAULT NULL,
  [lis] smallint DEFAULT '0',
  [rexcl] smallint DEFAULT '0',
  [rexcld] smallint DEFAULT '0',
  [CE] smallint DEFAULT '0',
  [excl] smallint DEFAULT '0',
  [num] smallint DEFAULT '0',
  [Event] smallint DEFAULT '0'
  
  
) ;

-- Eligible Patient List

drop table if exists #spd_memlist; 

CREATE TABLE #spd_memlist (
    memid varchar(100)
    
);


insert into #spd_memlist
SELECT DISTINCT en.MemID FROM HDS.HEDIS_MEMBER_GM gm
join hds.HEDIS_MEMBER_EN en on en.MemID=gm.MemID and en.MEASURE_ID=gm.MEASURE_ID
WHERE en.Measure_id=@meas and
en.StartDate<=@ce_enddt AND FinishDate>=@ce_startdt1
AND YEAR(@ce_enddt)-YEAR(DOB) BETWEEN 40 AND 75
ORDER BY 1;

CREATE CLUSTERED INDEX ix_memlist_memid ON #spd_memlist ([memid]);


-- Create Temp Patient Enrollment
drop table if exists #spd_tmpsubscriber;

CREATE TABLE #spd_tmpsubscriber (
    memid varchar(100),
    dob varchar(8),
	gender varchar(1),
	payer varchar(50),
	StartDate varchar(8),
	EndDate varchar(8),
);



insert into #spd_tmpsubscriber
SELECT en.MemID,gm.DOB,gm.Gender,en.Payer,en.StartDate,en.FinishDate  FROM HDS.HEDIS_MEMBER_GM gm
join hds.HEDIS_MEMBER_EN en on en.MemID=gm.MemID and en.MEASURE_ID=gm.MEASURE_ID
WHERE en.Measure_id=@meas and
en.StartDate<=@ce_enddt AND FinishDate>=@ce_startdt1
and YEAR(@ce_enddt)-YEAR(DOB) BETWEEN 40 AND 75 ORDER BY en.MemID,en.StartDate,en.FinishDate;

CREATE CLUSTERED INDEX ix_tmpsub_memid ON #spd_tmpsubscriber ([memid],[StartDate],[EndDate]);


-- Get Patient COUNT
SELECT @pt_ctr=COUNT(*)  FROM #spd_memlist;


While @i<@pt_ctr
	BEGIN
		-- Get Patient to Loop
		SELECT  @patientid=memid FROM #spd_memlist ORDER BY memid ASC OFFSET  @i ROWS FETCH NEXT 1 ROWS ONLY ;
		
		-- ReadingGender and age
		SELECT Top 1 @gender=gender,@age=YEAR(@ce_enddt)-YEAR(DOB) FROM #spd_tmpsubscriber WHERE memid=@patientid;
		
		-- Check for dual eligibility by checking if the patient is enrolled in more than one plan at the end of Enrollment
		
		SELECT Top 1 @latest_insenddate=EndDate FROM #spd_tmpsubscriber WHERE  memid=@patientid  AND StartDate<=@ce_enddt ORDER BY StartDate DESC,EndDate Desc  ;
		
		-- Check for plan count patient is enrolled in during the end of measurement year
		
		SELECT @plan_ct=COUNT(*) FROM #spd_tmpsubscriber WHERE  memid=@patientid AND @latest_insenddate BETWEEN StartDate AND EndDate;
		
		-- If only one plan, then skip the Payer reporting logic
		
		if(@plan_ct=1)
		BEGIN
			
			SELECT @planid=payer FROM #spd_tmpsubscriber WHERE  memid=@patientid AND @latest_insenddate BETWEEN StartDate AND EndDate ORDER BY payer;
		
			-- If plan is Dual , Insert MCR & MCD	
			
			If(@planid in('MMP','SN3','MDE'))
			BEGIN
				IF(@planid in('MMP','SN3'))
				
				SET @planid='MCD';
				Insert INTO #spddataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'SPDA',@planid,@age,@gender);
				Insert INTO #spddataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'SPDB',@planid,@age,@gender);
				
				SET @planid='MCR';
				Insert INTO #spddataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'SPDA',@planid,@age,@gender);
				Insert INTO #spddataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'SPDB',@planid,@age,@gender);
				
				
			END
			
			Else if(@planid IN('MD','MLI','MRB'))
			Begin
				SET @planid='MCD';
				Insert INTO #spddataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'SPDA',@planid,@age,@gender);
				Insert INTO #spddataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'SPDB',@planid,@age,@gender);
				
			END
			
			Else If(@planid IN('SN1','SN2'))
			Begin
				
				SET @planid='MCR';
				Insert INTO #spddataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'SPDA',@planid,@age,@gender);
				Insert INTO #spddataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'SPDB',@planid,@age,@gender);
				
				
			END
			ELSE
			BEGIN
				Insert INTO #spddataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'SPDA',@planid,@age,@gender);
				Insert INTO #spddataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'SPDB',@planid,@age,@gender);
				
			END
		END
				
		-- Compare Plans if Plan count is more than one	
		if(@plan_ct>1)
		BEGIN
			
			-- Read First Plan
			SELECT  @plan1=payer FROM #spd_tmpsubscriber where memid=@patientid and @latest_insenddate between StartDate and EndDate order by 1 OFFSET  0 ROWS FETCH NEXT 1 ROWS ONLY ;
					
			-- Read second plan
			SELECT  @plan2=payer FROM #spd_tmpsubscriber where memid=@patientid and @latest_insenddate between StartDate and EndDate order by 1 OFFSET  1 ROWS FETCH NEXT 1 ROWS ONLY ;
			
			
			
			If(@plan1 IN('MCR','MP','MC','SN1','SN2','MCS') and @plan2 IN('PPO','POS','HMO','CEP'))
			BEGIN
				if(@plan1 IN('SN1','SN2'))
				BEGIN
					
					SET @planid='MCR';
					Insert INTO #spddataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'SPDA',@planid,@age,@gender);
					Insert INTO #spddataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'SPDB',@planid,@age,@gender);
					
								
				END
				
				if(@plan1 in ('MCR','MP','MC','MCS'))
				Begin
					
					Set @planid=@plan1;
					Insert INTO #spddataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'SPDA',@planid,@age,@gender);
					Insert INTO #spddataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'SPDB',@planid,@age,@gender);
				
				END

			END
			
			Else If(@plan2 IN('MCR','MP','MC','SN1','SN2','MCS') AND @plan1 IN('PPO','POS','HMO','CEP'))
			BEGIN
			
				IF(@plan2 IN('SN1','SN2'))
				Begin
					
					Set @planid='MCR';
					Insert INTO #spddataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'SPDA',@planid,@age,@gender);
					Insert INTO #spddataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'SPDB',@planid,@age,@gender);
				
				
				END
				
				
				IF(@plan2 IN('MCR','MP','MC','MCS'))
				BEGIN
					
					Set @planid=@plan2;
					
					Insert INTO #spddataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'SPDA',@planid,@age,@gender);
					Insert INTO #spddataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'SPDB',@planid,@age,@gender);
				
				END
			END
			
			ELSE IF(@plan1 IN('PPO','POS','HMO','CEP') AND @plan2 IN('MD','MLI','MRB'))
			BEGIN
			
				Set @planid=@plan1;
				Insert INTO #spddataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'SPDA',@planid,@age,@gender);
				Insert INTO #spddataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'SPDB',@planid,@age,@gender);
					
			END
			
			
			ELSE IF(@plan2 IN('PPO','POS','HMO','CEP') AND @plan1 IN('MD','MLI','MRB'))
			BEGIN
			
				Set @planid=@plan2;
				Insert INTO #spddataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'SPDA',@planid,@age,@gender);
				Insert INTO #spddataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'SPDB',@planid,@age,@gender);
				
			END
			
			ELSE IF(@plan1 IN('MDE','SN3','MMP') or @plan2 in('MDE','SN3','MMP'))
			BEGIN
			
				
					
				Set @planid='MCR';
				Insert INTO #spddataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'SPDA',@planid,@age,@gender);
				Insert INTO #spddataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'SPDB',@planid,@age,@gender);
				
				
				Set @planid='MCD';
				Insert INTO #spddataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'SPDA',@planid,@age,@gender);
				Insert INTO #spddataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'SPDB',@planid,@age,@gender);
				
			END
			ELSE
			BEGIN
			
				Set @planid=@plan1;
				
				Insert INTO #spddataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'SPDA',@planid,@age,@gender);
				Insert INTO #spddataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'SPDB',@planid,@age,@gender);
					
				Set @planid=@plan2;
				Insert INTO #spddataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'SPDA',@planid,@age,@gender);
				Insert INTO #spddataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'SPDB',@planid,@age,@gender);
				
			END
			
		END
		
		Set @i=@i+1;
		
	END;
	

-- Create Indices on cdcdataset
CREATE INDEX [idx1] ON #spddataset ([payer]);
CREATE INDEX [patient_id_UNIQUE] ON #spddataset ([patient_id],[payer]);



-- Continuous Enrollment
		
	
DROP TABLE IF EXISTS #spd_contenroll;
	
CREATE table #spd_contenroll
(
	Memid varchar(50)
		
);


With coverage_CTE1 (Memid,lastcoveragedate,Startdate,Finishdate,nextcoveragedate) as
(

select Memid,isnull(lag(Finishdate,1) over(partition by Memid order by Startdate,FinishDate desc),convert(varchar,cast(@ce_startdt as date),112)) as lastcoveragedate,Case when StartDate<@ce_startdt then convert(varchar,cast(@ce_startdt as date),112) else startdate end as Startdate,case when Finishdate>@ce_enddt then convert(varchar,cast(@ce_enddt as date),112) else finishdate end as Finishdate,isnull(lead(Startdate,1) over(partition by Memid order by Startdate,FinishDate),convert(varchar,cast(@ce_enddt as date),112)) as nextcoveragedate  from hedis_member_en where Measure_id=@meas and Startdate<=@ce_enddt and Finishdate>=@ce_startdt  and drug='Y'
),
coverage_CTE2 (Memid,lastcoveragedate,Startdate,Finishdate,nextcoveragedate) as
(
select Memid,isnull(lag(Finishdate,1) over(partition by Memid order by Startdate,FinishDate desc),convert(varchar,cast(@ce_startdt1 as date),112)) as lastcoveragedate,Case when StartDate<@ce_startdt1 then convert(varchar,cast(@ce_startdt1 as date),112) else startdate end as Startdate,case when Finishdate>@ce_enddt1 then convert(varchar,cast(@ce_enddt1 as date),112) else finishdate end as Finishdate,isnull(lead(Startdate,1) over(partition by Memid order by Startdate,FinishDate),convert(varchar,cast(@ce_enddt1 as date),112)) as nextcoveragedate  from hedis_member_en where Measure_id=@meas and Startdate<=@ce_enddt1 and Finishdate>=@ce_startdt1
)
Insert into #spd_contenroll

select t3.Memid from(
select t2.Memid from(
select *, case when rn=1 and startdate>@ce_startdt then 1 else 0 end as startgap,case when datediff(day,newFinishdate,nextcoveragedate)>0 then 1  else 0 end as gaps,datediff(day,Startdate,Finishdate)+1 as coveragedays,case when @ce_enddt between Startdate and newfinishdate then 1 else 0 end as anchor from(
Select *,case when datediff(day,Finishdate,nextcoveragedate) <=1 then isnull(lead(Finishdate,1) over(partition by Memid order by Startdate,FinishDate),convert(varchar,cast(@ce_enddt as date),112)) else finishdate end as newfinishdate,ROW_NUMBER() over(partition by memid order by Startdate,Finishdate) as rn from coverage_CTE1
)t1           
)t2  
group by memid having(sum(gaps)+sum(startgap)<=1) and sum(coveragedays)>=(DATEPART(dy, @ce_enddt)-45) and sum(anchor)>0
)t3
Join(

select distinct memid from
(

	select z2.Memid from
	(

		select *, case when rn=1 and startdate>@ce_startdt1 then 1 else 0 end as startgap,case when datediff(day,newFinishdate,nextcoveragedate)>0 then 1  else 0 end as gaps,datediff(day,Startdate,Finishdate)+1 as coveragedays,case when @ce_enddt1 between Startdate and newfinishdate then 1 else 0 end as anchor from
		(
			Select *,case when datediff(day,Finishdate,nextcoveragedate) <=1 then isnull(lead(Finishdate,1) over(partition by Memid order by Startdate,FinishDate),convert(varchar,cast(@ce_enddt1 as date),112)) else finishdate end as newfinishdate,ROW_NUMBER() over(partition by memid order by Startdate,Finishdate) as rn from coverage_CTE2
		)z1           
	)z2 
group by memid having(sum(gaps)+sum(startgap)<=1) and sum(coveragedays)>=(DATEPART(dy, @ce_enddt1)-45)
)z3 
)z4 on z4.memid=t3.memid order by t3.memid

CREATE CLUSTERED INDEX ix_spd_contenroll ON #spd_contenroll ([memid]);


update #spddataset set CE=1 from #spddataset ds join #spd_contenroll ce on ds.patient_id=ce.MemID;


-- Start of Event List

-- Get members with Diabetes

DROP TABLE IF EXISTS #spd_diabetesmemlist_claim;
	
CREATE table #spd_diabetesmemlist_claim
(
	Memid varchar(50),
	servicedate varchar(8),
	claimid INT

			
);

Insert into #spd_diabetesmemlist_claim
select distinct Memid,date_s as servicedate,claimid from HEDIS_VISIT where Measure_id=@meas and suppdata='N' and Date_S!='' and Date_S between @ce_startdt1 and @ce_enddt and hcfapos!=81
and 
(
		Diag_I_1 in(select code from VALUESET_TO_CODE where Value_Set_Name='Diabetes')
		or
		Diag_I_2 in(select code from VALUESET_TO_CODE where Value_Set_Name='Diabetes')
		or
		Diag_I_3 in(select code from VALUESET_TO_CODE where Value_Set_Name='Diabetes')
		or
		Diag_I_4 in(select code from VALUESET_TO_CODE where Value_Set_Name='Diabetes')
		or
		Diag_I_5 in(select code from VALUESET_TO_CODE where Value_Set_Name='Diabetes')
		or
		Diag_I_6 in(select code from VALUESET_TO_CODE where Value_Set_Name='Diabetes')
		or
		Diag_I_7 in(select code from VALUESET_TO_CODE where Value_Set_Name='Diabetes')
		or
		Diag_I_8 in(select code from VALUESET_TO_CODE where Value_Set_Name='Diabetes')
		or
		Diag_I_9 in(select code from VALUESET_TO_CODE where Value_Set_Name='Diabetes')
		or
		Diag_I_10 in(select code from VALUESET_TO_CODE where Value_Set_Name='Diabetes')
	)

CREATE CLUSTERED INDEX idx_spd_Diabetesmemlist_claim ON #spd_Diabetesmemlist_claim ([memid],[servicedate],[claimid]);



DROP TABLE IF EXISTS #spd_Diabetesmemlist_clinical;
	
CREATE table #spd_Diabetesmemlist_clinical
(
	Memid varchar(50),
	servicedate varchar(8)
				
);

Insert into #spd_Diabetesmemlist_clinical
select Memid,sdate as servicedate from Hedis_diag where Measure_id=@meas and sdate!='' and sdate between @ce_startdt1 and @ce_enddt and dcode in(select code from VALUESET_TO_CODE where Value_Set_Name='Diabetes') 

CREATE CLUSTERED INDEX idx_spd_Diabetesmemlist_clinical ON #spd_Diabetesmemlist_clinical ([memid],[servicedate]);

-- Calculate Visit LIST

CREATE table #spd_clinicalDiabetesvstlist
(
	Memid varchar(50),
	servicedate varchar(8)
				
);

Insert into #spd_clinicalDiabetesvstlist
select Memid,sdate from HEDIS_VISIT_E where Measure_id=@meas and sdate!='' and sdate between @ce_startdt1 and @ce_enddt and activity in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Telephone Visits ','Online Assessments','ED')) 


CREATE CLUSTERED INDEX idx_spd_clinicalDiabetesvstlist ON #spd_clinicalDiabetesvstlist ([memid],[servicedate]);




-- Inpatient Member list
DROP TABLE IF EXISTS #spd_ipstaylist;
	
CREATE table #spd_ipstaylist
(
	Memid varchar(50),
	servicedate varchar(8),
	claimid INT
				
);


insert into #spd_ipstaylist
select Memid,Date_Disch as servicedate,claimid from HEDIS_VISIT where Measure_id=@meas and suppdata='N' and Date_Disch!='' and Date_Disch between @ce_startdt1 and @ce_enddt and hcfapos!=81 and REV in(select code from VALUESET_TO_CODE where code_system='UBREV' and Value_Set_Name='Inpatient Stay')
	
CREATE CLUSTERED INDEX idx_spd_claimDiabetesvstlist ON #spd_ipstaylist ([memid],[servicedate],[claimid]);


-- Non acute inpatient stay list


DROP TABLE IF EXISTS #spd_nonacuteipstaylist;
	
CREATE table #spd_nonacuteipstaylist
(
	Memid varchar(50),
	servicedate varchar(8),
	claimid INT
				
);


insert into #spd_nonacuteipstaylist
select Memid,Date_Disch as servicedate,claimid from HEDIS_VISIT where Measure_id=@meas and suppdata='N' and Date_Disch!='' and Date_Disch between @ce_startdt1 and @ce_enddt and hcfapos!=81
	and (REV in(select code from VALUESET_TO_CODE where code_system='UBREV' and Value_Set_Name='Nonacute Inpatient Stay') or RIGHT('0000'+CAST(Trim(BillType) AS VARCHAR(4)),4) in(select code from VALUESET_TO_CODE where code_system='UBTOB' and Value_Set_Name='Nonacute Inpatient Stay'))
	
CREATE CLUSTERED INDEX idx_spd_nonacuteipstaylist ON #spd_nonacuteipstaylist ([memid],[servicedate],[claimid]);






DROP TABLE IF EXISTS #spd_claimDiabetesvstlist;
	
CREATE table #spd_claimDiabetesvstlist
(
	Memid varchar(50),
	servicedate varchar(8),
	claimid INT
				
);

Insert into #spd_claimDiabetesvstlist
select distinct Memid,servicedate,claimid from(
select Memid,date_s as servicedate,claimid from HEDIS_VISIT where Measure_id=@meas and suppdata='N' and Date_S!='' and Date_S between @ce_startdt1 and @ce_enddt and hcfapos!=81
	and 
	(
		CPT in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Telephone Visits','Online Assessments','ED','Observation'))
		or
		HCPCS in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Online Assessments'))
		or
		REV in(select code from VALUESET_TO_CODE where code_system='UBREV' and Value_Set_Name in('Outpatient','ED'))
	)

Union all

select Memid,date_s as servicedate,claimid from HEDIS_VISIT where MEASURE_ID=@meas and suppdata='N' and Date_S!='' and Date_S between @ce_startdt1 and @ce_enddt and hcfapos!=81
and CPT in(select code from VALUESET_TO_CODE where Value_Set_Name='Nonacute Inpatient') and (CPTMod_1 not in(select code from VALUESET_TO_CODE where Value_Set_Name='Telehealth Modifier') and CPTMod_2 not in(select code from VALUESET_TO_CODE where Value_Set_Name='Telehealth Modifier') and HCFAPOS not in(select code from VALUESET_TO_CODE where Value_Set_Name='Telehealth POS'))

Union all

select t1.Memid,t1.servicedate,t1.claimid from #spd_ipstaylist t1
join #spd_nonacuteipstaylist t2 on t1.Memid=t2.Memid and t1.claimid=t2.claimid




)t1
	

CREATE CLUSTERED INDEX idx_spd_claimDiabetesvstlist ON #spd_claimDiabetesvstlist ([memid],[servicedate],[claimid]);


-- Check vst count with Diabetes
drop table if exists #spd_diabetesvstlist;
CREATE table #spd_diabetesvstlist
(
	Memid varchar(50)
				
);

Insert into #spd_diabetesvstlist
select Memid from(
/*
select t1.Memid,t1.servicedate from #spd_clinicalDiabetesvstlist t1
join #spd_diabetesmemlist_clinical t2 on t1.Memid=t2.memid and t1.servicedate=t2.servicedate
Union all
*/
select distinct t1.Memid,t1.servicedate from #spd_claimDiabetesvstlist t1
join #spd_diabetesmemlist_claim t2 on t1.Memid=t2.memid and t1.claimid=t2.claimid
)t3 group by memid having count(memid)>1

CREATE CLUSTERED INDEX idx_spd_diabetesvstlist ON #spd_diabetesvstlist ([memid]);


update #spddataset set Event=1 from #spddataset ds join #spd_diabetesvstlist re on ds.patient_id=re.MemID and ds.meas='SPDA';



-- acute inpatient encounter (Acute Inpatient Value Set) with an Diabetes diagnosis (Diabetes Value Set) without telehealth (Telehealth Modifier Value Set; Telehealth POS Value Set).

DROP TABLE IF EXISTS #spd_acuteipenceventlist;
	
CREATE table #spd_acuteipenceventlist
(
	Memid varchar(50)
				
);

Insert into #spd_acuteipenceventlist
select t1.Memid from(
select Memid,date_s as servicedate,claimid from HEDIS_VISIT where MEASURE_ID=@meas and suppdata='N' and Date_S!='' and Date_S between @ce_startdt1 and @ce_enddt and hcfapos!=81
and CPT in(select code from VALUESET_TO_CODE where Value_Set_Name='Acute Inpatient') and (CPTMod_1 not in(select code from VALUESET_TO_CODE where Value_Set_Name='Telehealth Modifier') and CPTMod_2 not in(select code from VALUESET_TO_CODE where Value_Set_Name='Telehealth Modifier') and HCFAPOS not in(select code from VALUESET_TO_CODE where Value_Set_Name='Telehealth POS'))
)t1
join #spd_diabetesmemlist_claim t2 on t1.memid=t2.memid and t1.claimid=t2.claimid



update #spddataset set Event=1 from #spddataset ds join #spd_acuteipenceventlist re on ds.patient_id=re.MemID and ds.meas='SPDA';

-- •	At least one acute inpatient discharge with a diagnosis of diabetes (Diabetes Value Set) on the discharge claim


DROP TABLE IF EXISTS #spd_acuteipstaymemlist;
CREATE table #spd_acuteipstaymemlist
(
	Memid varchar(50)
				
);

Insert into #spd_acuteipstaymemlist
select t1.memid from(
select ip.Memid,ip.servicedate,ip.claimid from(
select Memid,Date_Disch as servicedate,claimid from HEDIS_VISIT where Measure_id=@meas and suppdata='N' and Date_Disch!='' and Date_Disch between @ce_startdt1 and @ce_enddt and hcfapos!=81
	and REV in(select code from VALUESET_TO_CODE where code_system='UBREV' and Value_Set_Name='Inpatient Stay')
	)ip
left outer Join(
select Memid,Date_Disch as servicedate,claimid from HEDIS_VISIT where Measure_id=@meas and suppdata='N' and Date_Disch!='' and Date_Disch between @ce_startdt1 and @ce_enddt and hcfapos!=81
	and (REV in(select code from VALUESET_TO_CODE where code_system='UBREV' and Value_Set_Name='Nonacute Inpatient Stay') or RIGHT('0000'+CAST(Trim(BillType) AS VARCHAR(4)),4) in(select code from VALUESET_TO_CODE where code_system='UBTOB' and Value_Set_Name='Nonacute Inpatient Stay'))
)naip on naip.MemID=ip.MemID and naip.ClaimID=ip.ClaimID
	where naip.MemID is null
)t1
join #spd_diabetesmemlist_claim t2 on t1.memid=t2.memid and t1.claimid=t2.claimid



CREATE CLUSTERED INDEX idx_spd_acuteipstaymemlist ON #spd_acuteipstaymemlist ([memid]);



update #spddataset set Event=1 from #spddataset ds join #spd_acuteipstaymemlist re on ds.patient_id=re.MemID and ds.meas='SPDA';


--Members who were dispensed insulin or hypoglycemics/anti-hyperglycemics on an ambulatory basis during the measurement year or the year prior to the measurement year (Diabetes Medications List).

DROP TABLE IF EXISTS #spd_diabetesmedlist;
CREATE table #spd_diabetesmedlist
(
	Memid varchar(50)
				
);

Insert into #spd_diabetesmedlist
select Memid from hedis_pharm where measure_id=@meas and suppdata='N' and PrServdate!='' and PrServdate between @ce_startdt1 and @ce_enddt and NDC in(select code from MEDICATION_LIST_TO_CODES where Medication_List_Name='Diabetes Medications')


CREATE CLUSTERED INDEX idx_spd_diabetesmedlist ON #spd_diabetesmedlist ([memid]);


update #spddataset set Event=1 from #spddataset ds join #spd_diabetesmedlist re on ds.patient_id=re.MemID and ds.meas='SPDA';


-- End of Event List

	
-- Required Exclusion
-- MI (MI Value Set) on the discharge claim


DROP TABLE IF EXISTS #spd_MImemlist;
	
CREATE table #spd_MImemlist
(
	Memid varchar(50),
	claimid INT
		
);

Insert into #spd_MImemlist
select distinct Memid,claimid from HEDIS_VISIT where Measure_id=@meas and Date_Disch!='' and Date_Disch between @ce_startdt1 and @ce_enddt1 and hcfapos!=81
and  
(
	Diag_I_1 in(select code from VALUESET_TO_CODE where Value_Set_Name='MI')
	or
	Diag_I_2 in(select code from VALUESET_TO_CODE where Value_Set_Name='MI')
	or
	Diag_I_3 in(select code from VALUESET_TO_CODE where Value_Set_Name='MI')
	or
	Diag_I_4 in(select code from VALUESET_TO_CODE where Value_Set_Name='MI')
	or
	Diag_I_5 in(select code from VALUESET_TO_CODE where Value_Set_Name='MI')
	or
	Diag_I_6 in(select code from VALUESET_TO_CODE where Value_Set_Name='MI')
	or
	Diag_I_7 in(select code from VALUESET_TO_CODE where Value_Set_Name='MI')
	or
	Diag_I_8 in(select code from VALUESET_TO_CODE where Value_Set_Name='MI')
	or
	Diag_I_9 in(select code from VALUESET_TO_CODE where Value_Set_Name='MI')
	or
	Diag_I_10 in(select code from VALUESET_TO_CODE where Value_Set_Name='MI')
)

--select * from #spd_MImemlist where memid=130131


--Inpatient member list
DROP TABLE IF EXISTS #spd_ipmemlist;
	
CREATE table #spd_ipmemlist
(
	Memid varchar(50),
	claimid INT
		
);

Insert into #spd_ipmemlist
select distinct Memid,claimid from HEDIS_VISIT where Measure_id=@meas and Date_Disch!='' and Date_Disch between @ce_startdt1 and @ce_enddt1 and hcfapos!=81
and REV in(select code from VALUESET_TO_CODE where Code_System='UBREV' and Value_Set_Name='Inpatient Stay') 


-- 	Other revascularization


DROP TABLE IF EXISTS #spd_ORCmemlist;
	
CREATE table #spd_ORCmemlist
(
	Memid varchar(50)
			
);

Insert into #spd_ORCmemlist
select distinct Memid from(
select distinct Memid from HEDIS_VISIT where Measure_id=@meas and Date_S!='' and Date_S between @ce_startdt1 and @ce_enddt1 and hcfapos!=81
and CPT in(select code from VALUESET_TO_CODE where Value_Set_Name='Other Revascularization')

Union all

select memid from hedis_proc where measure_id=@meas and pstatus='EVN' and sdate!='' and sdate between @ce_startdt1 and @ce_enddt1 and pcode in(select code from VALUESET_TO_CODE where Value_Set_Name='Other Revascularization')

)t1


-- CABG Mem LIST

DROP TABLE IF EXISTS #spd_CABGmemlist;
	
CREATE table #spd_CABGmemlist
(
	Memid varchar(50)
			
);

Insert into #spd_CABGmemlist
select distinct Memid from(
select distinct Memid from HEDIS_VISIT where Measure_id=@meas and Date_S!='' and Date_S between @ce_startdt1 and @ce_enddt1 and hcfapos!=81
and 
(
	CPT in(select code from VALUESET_TO_CODE where Value_Set_Name='CABG')
	or
	HCPCS in(select code from VALUESET_TO_CODE where Value_Set_Name='CABG')
	or
	Proc_i_1 in(select code from VALUESET_TO_CODE where Value_Set_Name='CABG')
	or
	Proc_i_2 in(select code from VALUESET_TO_CODE where Value_Set_Name='CABG')
	or
	Proc_i_3 in(select code from VALUESET_TO_CODE where Value_Set_Name='CABG')
	or
	Proc_i_4 in(select code from VALUESET_TO_CODE where Value_Set_Name='CABG')
	or
	Proc_i_5 in(select code from VALUESET_TO_CODE where Value_Set_Name='CABG')
	or
	Proc_i_6 in(select code from VALUESET_TO_CODE where Value_Set_Name='CABG')
	
)

Union all

select memid from hedis_proc where measure_id=@meas and pstatus='EVN' and sdate!='' and sdate between @ce_startdt1 and @ce_enddt1 and pcode in(select code from VALUESET_TO_CODE where Value_Set_Name='CABG')

)t1


-- PCI List

DROP TABLE IF EXISTS #spd_PCImemlist;
	
CREATE table #spd_PCImemlist
(
	Memid varchar(50)
			
);

Insert into #spd_PCImemlist
select distinct Memid from(
select distinct Memid from HEDIS_VISIT where Measure_id=@meas and Date_S!='' and Date_S between @ce_startdt1 and @ce_enddt1 and hcfapos!=81
and 
(
	CPT in(select code from VALUESET_TO_CODE where Value_Set_Name='PCI')
	or
	HCPCS in(select code from VALUESET_TO_CODE where Value_Set_Name='PCI')
	or
	Proc_i_1 in(select code from VALUESET_TO_CODE where Value_Set_Name='PCI')
	or
	Proc_i_2 in(select code from VALUESET_TO_CODE where Value_Set_Name='PCI')
	or
	Proc_i_3 in(select code from VALUESET_TO_CODE where Value_Set_Name='PCI')
	or
	Proc_i_4 in(select code from VALUESET_TO_CODE where Value_Set_Name='PCI')
	or
	Proc_i_5 in(select code from VALUESET_TO_CODE where Value_Set_Name='PCI')
	or
	Proc_i_6 in(select code from VALUESET_TO_CODE where Value_Set_Name='PCI')
	
)

union all

select Memid from HEDIS_PROC where MEASURE_ID=@meas and pstatus='EVN' and sdate!='' and sdate between @ce_startdt1 and @ce_enddt1 and pcode in(select code from VALUESET_TO_CODE where Value_Set_Name='PCI')

)t1




-- IVD Diagnosis
DROP TABLE IF EXISTS #spd_IVDmemlist_claim;
	
CREATE table #spd_IVDmemlist_claim
(
	Memid varchar(50),
	servicedate varchar(8),
	claimid INT

			
);

Insert into #spd_IVDmemlist_claim
select distinct Memid,date_s as servicedate,claimid from HEDIS_VISIT where Measure_id=@meas and Date_S!='' and Date_S between @ce_startdt1 and @ce_enddt and hcfapos!=81
and 
(
		Diag_I_1 in(select code from VALUESET_TO_CODE where Value_Set_Name='IVD')
		or
		Diag_I_2 in(select code from VALUESET_TO_CODE where Value_Set_Name='IVD')
		or
		Diag_I_3 in(select code from VALUESET_TO_CODE where Value_Set_Name='IVD')
		or
		Diag_I_4 in(select code from VALUESET_TO_CODE where Value_Set_Name='IVD')
		or
		Diag_I_5 in(select code from VALUESET_TO_CODE where Value_Set_Name='IVD')
		or
		Diag_I_6 in(select code from VALUESET_TO_CODE where Value_Set_Name='IVD')
		or
		Diag_I_7 in(select code from VALUESET_TO_CODE where Value_Set_Name='IVD')
		or
		Diag_I_8 in(select code from VALUESET_TO_CODE where Value_Set_Name='IVD')
		or
		Diag_I_9 in(select code from VALUESET_TO_CODE where Value_Set_Name='IVD')
		or
		Diag_I_10 in(select code from VALUESET_TO_CODE where Value_Set_Name='IVD')
	)

CREATE CLUSTERED INDEX idx_spd_IVDmemlist_claim ON #spd_IVDmemlist_claim ([memid],[servicedate],[claimid]);

-- select * from #spd_IVDmemlist_claim where memid=130131

DROP TABLE IF EXISTS #spd_IVDmemlist_clinical;
	
CREATE table #spd_IVDmemlist_clinical
(
	Memid varchar(50),
	servicedate varchar(8)
				
);

Insert into #spd_IVDmemlist_clinical
select Memid,sdate as servicedate from Hedis_diag where Measure_id=@meas and sdate!='' and sdate between @ce_startdt1 and @ce_enddt and dcode in(select code from VALUESET_TO_CODE where Value_Set_Name='IVD') 

CREATE CLUSTERED INDEX idx_spd_IVDmemlist_clinical ON #spd_IVDmemlist_clinical ([memid],[servicedate]);

-- select * from #spd_IVDmemlist_clinical where memid=130131

DROP TABLE IF EXISTS #spd_clinicalIVDvstlist;
	
CREATE table #spd_clinicalIVDvstlist
(
	Memid varchar(50),
	servicedate varchar(8)
				
);

Insert into #spd_clinicalIVDvstlist
select Memid,sdate from HEDIS_VISIT_E where Measure_id=@meas and sdate!='' and sdate between @ce_startdt1 and @ce_enddt and activity in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Telephone Visits ','Online Assessments')) 


CREATE CLUSTERED INDEX idx_spd_clinicalIVDvstlist ON #spd_clinicalIVDvstlist ([memid],[servicedate]);



DROP TABLE IF EXISTS #spd_claimIVDvstlist;
	
CREATE table #spd_claimIVDvstlist
(
	Memid varchar(50),
	servicedate varchar(8),
	claimid INT
				
);

Insert into #spd_claimIVDvstlist
select distinct Memid,servicedate,claimid from(
select Memid,date_s as servicedate,claimid from HEDIS_VISIT where Measure_id=@meas and Date_S!='' and Date_S between @ce_startdt1 and @ce_enddt and hcfapos!=81
	and 
	(
		CPT in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Telephone Visits','Online Assessments'))
		or
		HCPCS in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Online Assessments'))
		or
		REV in(select code from VALUESET_TO_CODE where code_system='UBREV' and Value_Set_Name='Outpatient')
	)
Union all
-- acute inpatient encounter (Acute Inpatient Value Set) with an IVD diagnosis (IVD Value Set) without telehealth (Telehealth Modifier Value Set; Telehealth POS Value Set).
select Memid,date_s as servicedate,claimid from HEDIS_VISIT where MEASURE_ID=@meas and Date_S!='' and Date_S between @ce_startdt1 and @ce_enddt and hcfapos!=81
and CPT in(select code from VALUESET_TO_CODE where Value_Set_Name='Acute Inpatient') and (CPTMod_1 not in(select code from VALUESET_TO_CODE where Value_Set_Name='Telehealth Modifier') and CPTMod_2 not in(select code from VALUESET_TO_CODE where Value_Set_Name='Telehealth Modifier') and HCFAPOS not in(select code from VALUESET_TO_CODE where Value_Set_Name='Telehealth POS'))
)t1
	

CREATE CLUSTERED INDEX idx_spd_claimIVDvstlist ON #spd_claimIVDvstlist ([memid],[servicedate],[claimid]);

-- select * from #spd_claimIVDvstlist where memid=130131

DROP TABLE IF EXISTS #spd_acuteipIVDvstlist;
CREATE table #spd_acuteipIVDvstlist
(
	Memid varchar(50),
	servicedate varchar(8),
	claimid INT
				
);

Insert into #spd_acuteipIVDvstlist
select ip.Memid,ip.servicedate,ip.claimid from(
select Memid,Date_Disch as servicedate,claimid from HEDIS_VISIT where Measure_id=@meas and Date_Disch!='' and Date_Disch between @ce_startdt1 and @ce_enddt and hcfapos!=81
	and REV in(select code from VALUESET_TO_CODE where code_system='UBREV' and Value_Set_Name='Inpatient Stay')
	)ip
left outer Join(
select Memid,Date_Disch as servicedate,claimid from HEDIS_VISIT where Measure_id=@meas and Date_Disch!='' and Date_Disch between @ce_startdt1 and @ce_enddt and hcfapos!=81
	and (REV in(select code from VALUESET_TO_CODE where code_system='UBREV' and Value_Set_Name='Nonacute Inpatient Stay') or RIGHT('0000'+CAST(Trim(BillType) AS VARCHAR(4)),4) in(select code from VALUESET_TO_CODE where code_system='UBTOB' and Value_Set_Name='Nonacute Inpatient Stay'))
)naip on naip.MemID=ip.MemID and naip.ClaimID=ip.ClaimID
	where naip.MemID is null
	

CREATE CLUSTERED INDEX idx_spd_acuteipIVDvstlist ON #spd_acuteipIVDvstlist ([memid],[servicedate],[claimid]);


DROP TABLE IF EXISTS #spd_IVDlist;
CREATE table #spd_IVDlist
(
	Memid varchar(50)	
				
);


Insert into #spd_IVDlist
select Memid from(
select Memid,Year(servicedate) as vstyear from(
select ivd.Memid,ivd.servicedate from #spd_IVDmemlist_claim ivd 
join #spd_acuteipIVDvstlist aip on ivd.memid=aip.memid and ivd.claimid=aip.claimid
Union all
select ivd.Memid,ivd.servicedate from #spd_IVDmemlist_claim ivd 
join #spd_claimIVDvstlist vst on ivd.memid=vst.memid and ivd.claimid=vst.claimid
Union all
select ivd.Memid,ivd.servicedate from #spd_IVDmemlist_clinical ivd 
join #spd_clinicalIVDvstlist vst on ivd.memid=vst.memid and ivd.servicedate=vst.servicedate

)t1 group by Memid,Year(servicedate) 
)t2 group by memid having count(Memid)>1

CREATE CLUSTERED INDEX idx_spd_IVDlist ON #spd_IVDlist ([memid]);

-- select * from #spd_IVDlist where memid=130131

-- Required Exclusion List



DROP TABLE IF EXISTS #spd_requiredexcllist;
	
CREATE table #spd_requiredexcllist
(
	Memid varchar(50)
			
);

Insert into #spd_requiredexcllist
Select distinct Memid from(
select distinct s1.Memid from #spd_ipmemlist s1
join #spd_MImemlist s2 on s1.Memid=s2.Memid and s1.claimid=s2.claimid
Union all 
select Memid from #spd_IVDlist
Union all
select distinct Memid from #spd_ORCmemlist
Union all
select distinct Memid from #spd_CABGmemlist
Union all
select distinct Memid from #spd_PCImemlist

)t1


CREATE CLUSTERED INDEX idx_spd_requiredexcllist ON #spd_requiredexcllist ([memid]);



update #spddataset set rexcld=1 from #spddataset ds join #spd_requiredexcllist re on ds.patient_id=re.MemID;


-- Required exclusions 
-- •	Female members with a diagnosis of pregnancy (Pregnancy Value Set) during the measurement year or the year prior to the measurement year.

DROP TABLE IF EXISTS #spd_pregmemlist;
	
CREATE table #spd_pregmemlist
(
	Memid varchar(50)

			
);

Insert into #spd_pregmemlist
select distinct f1.Memid from(
select Memid from HEDIS_VISIT where Measure_id=@meas and Date_S!='' and Date_S between @ce_startdt1 and @ce_enddt and hcfapos!=81
and 
(
	Diag_i_1 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Pregnancy'))
	or
	Diag_i_2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Pregnancy'))
	or
	Diag_i_3 in(select code from VALUESET_TO_CODE where Value_Set_Name='Pregnancy')
	or
	Diag_i_4 in(select code from VALUESET_TO_CODE where Value_Set_Name='Pregnancy')
	or
	Diag_i_5 in(select code from VALUESET_TO_CODE where Value_Set_Name='Pregnancy')
	or
	Diag_i_6 in(select code from VALUESET_TO_CODE where Value_Set_Name='Pregnancy')
	or
	Diag_i_7 in(select code from VALUESET_TO_CODE where Value_Set_Name='Pregnancy')
	or
	Diag_i_8 in(select code from VALUESET_TO_CODE where Value_Set_Name='Pregnancy')
	or
	Diag_i_9 in(select code from VALUESET_TO_CODE where Value_Set_Name='Pregnancy')
	or
	Diag_i_10 in(select code from VALUESET_TO_CODE where Value_Set_Name='Pregnancy')
)
Union all

select Memid from Hedis_diag where Measure_id=@meas and sdate!='' and sdate between @ce_startdt1 and @ce_enddt and dcode in(select code from VALUESET_TO_CODE where Value_Set_Name='Pregnancy') 

Union all

select Memid from HEDIS_proc where Measure_id=@meas and pstatus='EVN' and sdate!='' and sdate between @ce_startdt1 and @ce_enddt and pcode in(select code from VALUESET_TO_CODE where Value_Set_Name in('Pregnancy')) 

Union all

select Memid from HEDIS_obs where Measure_id=@meas and date!='' and date between @ce_startdt1 and @ce_enddt and ocode in(select code from VALUESET_TO_CODE where Value_Set_Name in('Pregnancy')) 

)f1
	
	
CREATE CLUSTERED INDEX idx_spc_pregmemlist ON #spd_pregmemlist ([memid]);

update #spddataset set Rexcld=1 from #spddataset ds join #spd_pregmemlist re1 on ds.patient_id=re1.MemID and ds.patient_gender='F';

-- •	In vitro fertilization (IVF Value Set) in the measurement year or year prior to the measurement year

DROP TABLE IF EXISTS #spd_IVFmemlist;
	
CREATE table #spd_IVFmemlist
(
	Memid varchar(50)

			
);

Insert into #spd_IVFmemlist
select distinct f1.Memid from(
select Memid from HEDIS_VISIT where Measure_id=@meas and Date_S!='' and Date_S between @ce_startdt1 and @ce_enddt and hcfapos!=81
and HCPCS in(select code from VALUESET_TO_CODE where Value_Set_Name in('IVF'))
	
Union all

select Memid from Hedis_diag where Measure_id=@meas and sdate!='' and sdate between @ce_startdt1 and @ce_enddt and dcode in(select code from VALUESET_TO_CODE where Value_Set_Name='IVF') 

Union all

select Memid from HEDIS_proc where Measure_id=@meas and pstatus='EVN' and sdate!='' and sdate between @ce_startdt1 and @ce_enddt and pcode in(select code from VALUESET_TO_CODE where Value_Set_Name in('IVF'))

Union all

select Memid from HEDIS_obs where Measure_id=@meas and date!='' and date between @ce_startdt1 and @ce_enddt and ocode in(select code from VALUESET_TO_CODE where Value_Set_Name in('IVF')) 

)f1
	
	
CREATE CLUSTERED INDEX idx_#spd_IVFmemlist ON #spd_IVFmemlist ([memid]);

update #spddataset set Rexcld=1 from #spddataset ds join #spd_IVFmemlist re2 on ds.patient_id=re2.MemID;


-- •	Dispensed at least one prescription for clomiphene (Estrogen Agonists Medications List) during the measurement year or the year prior to the measurement year


DROP TABLE IF EXISTS #spd_estrogenmemlist;
	
CREATE table #spd_estrogenmemlist
(
	Memid varchar(50)

			
);

Insert into #spd_estrogenmemlist
select distinct Memid from(
select Memid from HEDIS_PHARM where Measure_id=@meas and Prservdate!='' and PrServdate between @ce_startdt1 and @ce_enddt and NDC in(select code from MEDICATION_LIST_TO_CODES where Medication_List_Name='Estrogen Agonists Medications')
Union all
select Memid from HEDIS_PHARM_C where Measure_id=@meas and Startdate!='' and Startdate between @ce_startdt1 and @ce_enddt and Rxnorm in(select code from MEDICATION_LIST_TO_CODES where Medication_List_Name='Estrogen Agonists Medications')
)f1
	
CREATE CLUSTERED INDEX idx_spc_estrogenmemlist ON #spd_estrogenmemlist ([memid]);


update #spddataset set Rexcld=1 from #spddataset ds join #spd_estrogenmemlist re3 on ds.patient_id=re3.MemID;

-- •	ESRD (ESRD Diagnosis Value Set) or dialysis (Dialysis Procedure Value Set) during the measurement year or the year prior to the measurement year

drop table if exists #spd_esrdmemlist;
	
CREATE table #spd_esrdmemlist
(
	Memid varchar(50)

			
);

Insert into #spd_esrdmemlist
select distinct f1.Memid from(
select Memid from HEDIS_VISIT where Measure_id=@meas and Date_S!='' and Date_S between @ce_startdt1 and @ce_enddt and hcfapos!=81
and 
(
	CPT in(select code from VALUESET_TO_CODE where Value_Set_Name in('Dialysis Procedure'))
	or
	HCPCS in(select code from VALUESET_TO_CODE where Value_Set_Name in('Dialysis Procedure'))
	or
	Proc_I_1 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Dialysis Procedure'))
	or
	Proc_I_2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Dialysis Procedure'))
	or
	Proc_I_3 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Dialysis Procedure'))
	or
	Proc_I_4 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Dialysis Procedure'))
	or
	Proc_I_5 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Dialysis Procedure'))
	or
	Proc_I_6 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Dialysis Procedure'))
	or
	Diag_i_1 in(select code from VALUESET_TO_CODE where Value_Set_Name in('ESRD Diagnosis'))
	or
	Diag_i_2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('ESRD Diagnosis'))
	or
	Diag_i_3 in(select code from VALUESET_TO_CODE where Value_Set_Name='ESRD Diagnosis')
	or
	Diag_i_4 in(select code from VALUESET_TO_CODE where Value_Set_Name='ESRD Diagnosis')
	or
	Diag_i_5 in(select code from VALUESET_TO_CODE where Value_Set_Name='ESRD Diagnosis')
	or
	Diag_i_6 in(select code from VALUESET_TO_CODE where Value_Set_Name='ESRD Diagnosis')
	or
	Diag_i_7 in(select code from VALUESET_TO_CODE where Value_Set_Name='ESRD Diagnosis')
	or
	Diag_i_8 in(select code from VALUESET_TO_CODE where Value_Set_Name='ESRD Diagnosis')
	or
	Diag_i_9 in(select code from VALUESET_TO_CODE where Value_Set_Name='ESRD Diagnosis')
	or
	Diag_i_10 in(select code from VALUESET_TO_CODE where Value_Set_Name='ESRD Diagnosis')
)
Union all

select Memid from Hedis_diag where Measure_id=@meas and sdate!='' and sdate between @ce_startdt1 and @ce_enddt and dcode in(select code from VALUESET_TO_CODE where Value_Set_Name='ESRD Diagnosis') 

Union all

select Memid from HEDIS_proc where Measure_id=@meas and pstatus='EVN' and sdate!='' and sdate between @ce_startdt1 and @ce_enddt and pcode in(select code from VALUESET_TO_CODE where Value_Set_Name in('ESRD Diagnosis','Dialysis Procedure')) 

)f1
	
	
CREATE CLUSTERED INDEX idx_#spd_esrdmemlist ON #spd_esrdmemlist ([memid]);

update #spddataset set Rexcld=1 from #spddataset ds join #spd_esrdmemlist re4 on ds.patient_id=re4.MemID ;

-- •	Cirrhosis (Cirrhosis Value Set) during the measurement year or the year prior to the measurement year


Drop table if exists #spd_Cirrhosismemlist;
	
CREATE table #spd_Cirrhosismemlist
(
	Memid varchar(50)

			
);

Insert into #spd_Cirrhosismemlist
select distinct f1.Memid from(
select Memid from HEDIS_VISIT where Measure_id=@meas and Date_S!='' and Date_S between @ce_startdt1 and @ce_enddt and hcfapos!=81
and 
(
	Diag_i_1 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Cirrhosis'))
	or
	Diag_i_2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Cirrhosis'))
	or
	Diag_i_3 in(select code from VALUESET_TO_CODE where Value_Set_Name='Cirrhosis')
	or
	Diag_i_4 in(select code from VALUESET_TO_CODE where Value_Set_Name='Cirrhosis')
	or
	Diag_i_5 in(select code from VALUESET_TO_CODE where Value_Set_Name='Cirrhosis')
	or
	Diag_i_6 in(select code from VALUESET_TO_CODE where Value_Set_Name='Cirrhosis')
	or
	Diag_i_7 in(select code from VALUESET_TO_CODE where Value_Set_Name='Cirrhosis')
	or
	Diag_i_8 in(select code from VALUESET_TO_CODE where Value_Set_Name='Cirrhosis')
	or
	Diag_i_9 in(select code from VALUESET_TO_CODE where Value_Set_Name='Cirrhosis')
	or
	Diag_i_10 in(select code from VALUESET_TO_CODE where Value_Set_Name='Cirrhosis')
)
Union all

select Memid from Hedis_diag where Measure_id=@meas and sdate!='' and sdate between @ce_startdt1 and @ce_enddt and dcode in(select code from VALUESET_TO_CODE where Value_Set_Name='Cirrhosis') 

)f1
	
	
CREATE CLUSTERED INDEX idx_spc_Cirrhosismemlist ON #spd_Cirrhosismemlist ([memid]);

update #spddataset set Rexcld=1 from #spddataset ds join #spd_Cirrhosismemlist re5 on ds.patient_id=re5.MemID ;

-- •	Myalgia, myositis, myopathy or rhabdomyolysis (Muscular Pain and Disease Value Set) during the measurement year

drop table if exists #spd_MPDmemlist;
	
CREATE table #spd_MPDmemlist
(
	Memid varchar(50)

			
);

Insert into #spd_MPDmemlist
select distinct f1.Memid from(
select Memid from HEDIS_VISIT where Measure_id=@meas and Date_S!='' and Date_S between @ce_startdt and @ce_enddt and hcfapos!=81
and 
(
	Diag_i_1 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Muscular Pain and Disease'))
	or
	Diag_i_2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Muscular Pain and Disease'))
	or
	Diag_i_3 in(select code from VALUESET_TO_CODE where Value_Set_Name='Muscular Pain and Disease')
	or
	Diag_i_4 in(select code from VALUESET_TO_CODE where Value_Set_Name='Muscular Pain and Disease')
	or
	Diag_i_5 in(select code from VALUESET_TO_CODE where Value_Set_Name='Muscular Pain and Disease')
	or
	Diag_i_6 in(select code from VALUESET_TO_CODE where Value_Set_Name='Muscular Pain and Disease')
	or
	Diag_i_7 in(select code from VALUESET_TO_CODE where Value_Set_Name='Muscular Pain and Disease')
	or
	Diag_i_8 in(select code from VALUESET_TO_CODE where Value_Set_Name='Muscular Pain and Disease')
	or
	Diag_i_9 in(select code from VALUESET_TO_CODE where Value_Set_Name='Muscular Pain and Disease')
	or
	Diag_i_10 in(select code from VALUESET_TO_CODE where Value_Set_Name='Muscular Pain and Disease')
)
Union all

select Memid from Hedis_diag where Measure_id=@meas and sdate!='' and sdate between @ce_startdt and @ce_enddt and dcode in(select code from VALUESET_TO_CODE where Value_Set_Name='Muscular Pain and Disease') 

)f1
	
	
CREATE CLUSTERED INDEX idx_spc_MPDMemlist ON #spd_MPDmemlist ([memid]);

update #spddataset set Rexcld=1 from #spddataset ds join #spd_MPDmemlist re6 on ds.patient_id=re6.MemID ;

-- •	Members receiving palliative care (Palliative Care Assessment Value Set; Palliative Care Encounter Value Set; Palliative Care Intervention Value Set) during the measurement year

drop table if exists #spd_palliativememlist;
	
CREATE table #spd_palliativememlist
(
	Memid varchar(50)

			
);

Insert into #spd_palliativememlist
select distinct f1.Memid from(
select Memid from HEDIS_VISIT where Measure_id=@meas and Date_S!='' and Date_S between @ce_startdt and @ce_enddt and hcfapos!=81
and 
(
	HCPCS in(select code from VALUESET_TO_CODE where Value_Set_Name in('Palliative Care Assessment','Palliative Care Encounter'))
	or 
	Diag_i_1 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Palliative Care Assessment','Palliative Care Encounter'))
	or
	Diag_i_2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Palliative Care Assessment','Palliative Care Encounter'))
	or
	Diag_i_3 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Palliative Care Assessment','Palliative Care Encounter'))
	or
	Diag_i_4 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Palliative Care Assessment','Palliative Care Encounter'))
	or
	Diag_i_5 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Palliative Care Assessment','Palliative Care Encounter'))
	or
	Diag_i_6 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Palliative Care Assessment','Palliative Care Encounter'))
	or
	Diag_i_7 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Palliative Care Assessment','Palliative Care Encounter'))
	or
	Diag_i_8 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Palliative Care Assessment','Palliative Care Encounter'))
	or
	Diag_i_9 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Palliative Care Assessment','Palliative Care Encounter'))
	or
	Diag_i_10 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Palliative Care Assessment','Palliative Care Encounter'))
)

Union all

select Memid from Hedis_obs where Measure_id=@meas and date!='' and date between @ce_startdt and @ce_enddt and ocode in(select code from VALUESET_TO_CODE where Value_Set_Name in('Palliative Care Assessment','Palliative Care Encounter','Palliative Care Intervention')) 

Union all

select Memid from HEDIS_VISIT_E where Measure_id=@meas and sdate!='' and sdate between @ce_startdt and @ce_enddt and activity in(select code from VALUESET_TO_CODE where Value_Set_Name in('Palliative Care Assessment','Palliative Care Encounter')) 



)f1
	
	
CREATE CLUSTERED INDEX idx_spc_palliativememlist ON #spd_palliativememlist ([memid]);

update #spddataset set Rexcld=1 from #spddataset ds join #spd_palliativememlist re7 on ds.patient_id=re7.MemID ;


-- Exclusion

	-- AdvancedIllness

	drop table if exists #spd_advillness;
	
	CREATE table #spd_advillness
		(
			Memid varchar(50),
			
			servicedate varchar(8),
			claimid int
		);

	

	Insert into #spd_advillness
	select MemID,Date_S,claimid from HEDIS_VISIT v
	where Measure_id=@meas and HCFAPOS!=81 and suppdata='N' and Date_S between @ce_startdt1 and @ce_enddt
	and (
	Diag_I_1 in(select code from VALUESET_TO_CODE where Value_Set_Name='Advanced Illness')
	or
	Diag_I_2 in(select code from VALUESET_TO_CODE where Value_Set_Name='Advanced Illness')
	or
	Diag_I_3 in(select code from VALUESET_TO_CODE where Value_Set_Name='Advanced Illness')
	or
	Diag_I_4 in(select code from VALUESET_TO_CODE where Value_Set_Name='Advanced Illness')
	or
	Diag_I_5 in(select code from VALUESET_TO_CODE where Value_Set_Name='Advanced Illness')
	or
	Diag_I_6 in(select code from VALUESET_TO_CODE where Value_Set_Name='Advanced Illness')
	or
	Diag_I_7 in(select code from VALUESET_TO_CODE where Value_Set_Name='Advanced Illness')
	or
	Diag_I_8 in(select code from VALUESET_TO_CODE where Value_Set_Name='Advanced Illness')
	or
	Diag_I_9 in(select code from VALUESET_TO_CODE where Value_Set_Name='Advanced Illness')
	or
	Diag_I_10 in(select code from VALUESET_TO_CODE where Value_Set_Name='Advanced Illness')
	)
	
	CREATE CLUSTERED INDEX ix_spc_advillness ON #spd_advillness ([memid],[servicedate],[claimid]);
	
-- Members with Institutinal SNP
	UPDATE #spddataset SET #spddataset.rexcl=1 FROM #spddataset ds JOIN HEDIS_MEMBER_EN s on ds.patient_id=s.MemID and s.Measure_id=@meas WHERE  ds.age>=66 AND ds.payer IN('MCR','MCS','MP','MC','SN2','SN1','SN3') AND s.StartDate<=@ce_enddt AND s.FinishDate>=@ce_startdt AND s.Payer='SN2';


	-- LTI Exclusion
	
	drop table if exists #spd_LTImembers;
	
	CREATE table #spd_LTImembers
	(
		Memid varchar(50)
				
	);

	

	Insert into #spd_LTImembers
	SELECT DISTINCT Beneficiary_ID FROM HEDIS_MMDF WHERE Measure_id=@meas AND Run_Date BETWEEN @ce_startdt AND @ce_enddt AND LTI_Flag='Y';

	CREATE CLUSTERED INDEX idx_spc_ltimembers ON #spd_LTImembers ([memid]);

	update #spddataset set rexcl=1 from #spddataset ds join #spd_LTImembers re on ds.patient_id=re.Memid where ds.age>=66 AND ds.payer IN('MCR','MCS','MP','MC','MMP','SN1','SN2','SN3');




	-- Hospice Exclusion

	drop table if exists #spd_hospicemembers;
	CREATE table #spd_hospicemembers
	(
		Memid varchar(50)
		
	);

	

	Insert into #spd_hospicemembers
	select distinct t1.MemID from
	(
	select Memid from HEDIS_OBS where Measure_id=@meas and date !='' and date between @ce_startdt and @ce_enddt and OCode IN(select code from VALUESET_TO_CODE where Value_Set_Name in('Hospice Encounter','Hospice Intervention'))
	Union all
	Select Memid from HEDIS_VISIT_E where Measure_id=@meas and sdate!='' and sdate between @ce_startdt and @ce_enddt and activity IN(select code from VALUESET_TO_CODE where Value_Set_Name in('Hospice Encounter','Hospice Intervention'))

	Union all

	select Beneficiary_id as Memid from Hedis_MMDF where Measure_id=@meas and Run_date!='' and Run_date between @ce_startdt and @ce_enddt and Hospice='Y'

	Union all

	select MemID from HEDIS_VISIT v
	where Measure_id=@meas and HCFAPOS!=81 and Date_S!='' and Date_S between @ce_startdt and @ce_enddt
	and (
	Diag_I_1 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or
	Diag_I_2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or
	Diag_I_3 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or
	Diag_I_4 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or
	Diag_I_5 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or
	Diag_I_6 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or
	Diag_I_7 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or
	Diag_I_8 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or
	Diag_I_9 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or
	Diag_I_10 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or
	CPT in(select code from VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or
	CPT2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or
	HCPCS in(select code from VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or 
	Proc_I_1 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or 
	Proc_I_2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or 
	Proc_I_3 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or 
	Proc_I_4 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or 
	Proc_I_5 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or 
	Proc_I_6 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	
	or
	Rev in(select code from VALUESET_TO_CODE where code_system='UBREV' and Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	)
	)t1
	
	CREATE CLUSTERED INDEX idx_hospicemembers ON #spd_hospicemembers ([memid]);

	update #spddataset set rexcl=1 from #spddataset ds join #spd_hospicemembers hos on hos.memid=ds.patient_id;
			
-- Frailty Members LIST
	drop table if exists #spd_frailtymembers;
	
	CREATE table #spd_frailtymembers
	(
		
		Memid varchar(50)
			
	);

	

	Insert into #spd_frailtymembers
	select MemID from HEDIS_VISIT v
	where Measure_id=@meas and HCFAPOS!=81 and suppdata='N' and Date_S!='' and Date_S between @ce_startdt and @ce_enddt
	and (
	Diag_I_1 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Frailty Diagnosis','Frailty Device','Frailty Encounter','Frailty Symptom'))
	or
	Diag_I_2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Frailty Diagnosis','Frailty Device','Frailty Encounter','Frailty Symptom'))
	or
	Diag_I_3 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Frailty Diagnosis','Frailty Device','Frailty Encounter','Frailty Symptom'))
	or
	Diag_I_4 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Frailty Diagnosis','Frailty Device','Frailty Encounter','Frailty Symptom'))
	or
	Diag_I_5 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Frailty Diagnosis','Frailty Device','Frailty Encounter','Frailty Symptom'))
	or
	Diag_I_6 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Frailty Diagnosis','Frailty Device','Frailty Encounter','Frailty Symptom'))
	or
	Diag_I_7 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Frailty Diagnosis','Frailty Device','Frailty Encounter','Frailty Symptom'))
	or
	Diag_I_8 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Frailty Diagnosis','Frailty Device','Frailty Encounter','Frailty Symptom'))
	or
	Diag_I_9 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Frailty Diagnosis','Frailty Device','Frailty Encounter','Frailty Symptom'))
	or
	Diag_I_10 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Frailty Diagnosis','Frailty Device','Frailty Encounter','Frailty Symptom'))
	or
	CPT in(select code from VALUESET_TO_CODE where Value_Set_Name in('Frailty Diagnosis','Frailty Device','Frailty Encounter','Frailty Symptom'))
	or
	HCPCS in(select code from VALUESET_TO_CODE where Value_Set_Name in('Frailty Diagnosis','Frailty Device','Frailty Encounter','Frailty Symptom'))
	);

	CREATE CLUSTERED INDEX idx_spc_frailtymembers ON #spd_frailtymembers ([memid]);
	
	
	
		
-- Required Exclusion 1

	-- Inpatient Stay List
	drop table if exists #spd_inpatientstaylist;
	CREATE table #spd_inpatientstaylist
	(
		Memid varchar(50),
		
		date_s varchar(8),
		claimid int
	);

	

	Insert into #spd_inpatientstaylist
	select distinct MemID,Date_S,claimid from HEDIS_VISIT v where v.Measure_id=@meas and v.HCFAPOS!=81 and suppdata='N' and v.date_disch!='' and v.date_disch between @ce_startdt1 and @ce_enddt and v.REV in (select code from VALUESET_TO_CODE where code_system='UBREV' and Value_Set_Name in('Inpatient Stay'));
	
	CREATE CLUSTERED INDEX ix_spc_inpatientstaylist ON #spd_inpatientstaylist ([memid],[date_s]);
	
	-- Non acute Inpatient stay list
	
	drop table if exists #spd_noncauteinpatientstaylist;
	CREATE table #spd_noncauteinpatientstaylist
		(
			Memid varchar(50),
			
			date_s varchar(8),
			claimid int
		);

	

	Insert into #spd_noncauteinpatientstaylist
	select distinct MemID,Date_S,claimid from HEDIS_VISIT v where v.Measure_id=@meas and v.HCFAPOS!=81 and suppdata='N' and v.date_disch!='' and v.date_disch between @ce_startdt1 and @ce_enddt and (v.REV in (select code from VALUESET_TO_CODE where code_system='UBREV'and Value_Set_Name in('Nonacute Inpatient Stay')) or RIGHT('0000'+CAST(Trim(v.BillType) AS VARCHAR(4)),4) in (select code from VALUESET_TO_CODE where code_system='UBTOB' and Value_Set_Name in('Nonacute Inpatient Stay')))
	
	CREATE CLUSTERED INDEX ix_spc_nonacuteinpatientstaylist ON #spd_noncauteinpatientstaylist ([memid],[date_s]);
	
	-- Outpatient and other visits
	drop table if exists #spd_visitlist;
	CREATE table #spd_visitlist
	(
		Memid varchar(50),
		
		date_s varchar(8),
		claimid int
	);

	

	Insert into #spd_visitlist
	select distinct MemID,Date_S,claimid from HEDIS_VISIT v
	where Measure_id=@meas and HCFAPOS!=81 and suppdata='N' and Date_S!='' and  Date_S between @ce_startdt1 and @ce_enddt
	and (
	Diag_I_1 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments','Nonacute Inpatient'))
	or
	Diag_I_2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments','Nonacute Inpatient'))
	or
	Diag_I_3 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments','Nonacute Inpatient'))
	or
	Diag_I_4 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments','Nonacute Inpatient'))
	or
	Diag_I_5 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments','Nonacute Inpatient'))
	or
	Diag_I_6 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments','Nonacute Inpatient'))
	or
	Diag_I_7 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments','Nonacute Inpatient'))
	or
	Diag_I_8 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments','Nonacute Inpatient'))
	or
	Diag_I_9 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments','Nonacute Inpatient'))
	or
	Diag_I_10 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments','Nonacute Inpatient'))
	or
	CPT in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments','Nonacute Inpatient'))
	or
	CPT2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments','Nonacute Inpatient'))
	or
	HCPCS in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments','Nonacute Inpatient'))
	or 
	Proc_I_1 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments','Nonacute Inpatient'))
	or 
	Proc_I_2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments','Nonacute Inpatient'))
	or 
	Proc_I_3 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments','Nonacute Inpatient'))
	or 
	Proc_I_4 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments','Nonacute Inpatient'))
	or 
	Proc_I_5 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments','Nonacute Inpatient'))
	or 
	Proc_I_6 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments','Nonacute Inpatient'))
	
	or 
	Rev in(select code from VALUESET_TO_CODE where code_system='UBREV' and  Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments','Nonacute Inpatient'))
	
	)
	
	CREATE CLUSTERED INDEX ix_spc_visitlist ON #spd_visitlist ([memid],[date_s]);
	
	-- Required exclusion table
	drop table if exists #spd_reqdexcl1;
	CREATE table #spd_reqdexcl1
	(
		Memid varchar(50)
			
	);

	

	Insert into #spd_reqdexcl1
	select distinct t3.Memid from(
	select t2.MemId from(
	select distinct t1.Memid,t1.date_s from(
	select Memid,date_s,claimid  from #spd_visitlist 
	union all
	select na.Memid,na.Date_s,na.claimid from #spd_noncauteinpatientstaylist na
	join #spd_inpatientstaylist inp on na.Memid=inp.Memid and na.claimid=inp.claimid
	)t1
	Join #spd_advillness a on a.Memid=t1.Memid and a.claimid=t1.claimid
	)t2 group by t2.Memid having count(t2.Memid)>1
	)t3 
	Join #spd_frailtymembers f on f.Memid=t3.Memid


	CREATE CLUSTERED INDEX idx_spc_reqdexcl1 ON #spd_reqdexcl1 ([memid]);
	
	update #spddataset set rexcl=1 from #spddataset ds
	join #spd_reqdexcl1 re1 on re1.Memid=ds.patient_id and ds.age BETWEEN 66 AND 80;

-- Required Exclusion 2

	-- Acute Inpatient with Advanced Illness
	drop table if exists #spd_reqdexcl2;
	CREATE table #spd_reqdexcl2
	(
		Memid varchar(50)
				
	);

	

	insert into #spd_reqdexcl2
	select distinct t2.Memid from(
	select t1.MemID from (
	select distinct MemID,Date_S,claimid from HEDIS_VISIT v
	where Measure_id=@meas and HCFAPOS!=81 and suppdata='N' and Date_S!='' and Date_S between @ce_startdt1 and @ce_enddt
	and (
	Diag_I_1 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Acute Inpatient'))
	or
	Diag_I_2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Acute Inpatient'))
	or
	Diag_I_3 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Acute Inpatient'))
	or
	Diag_I_4 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Acute Inpatient'))
	or
	Diag_I_5 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Acute Inpatient'))
	or
	Diag_I_6 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Acute Inpatient'))
	or
	Diag_I_7 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Acute Inpatient'))
	or
	Diag_I_8 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Acute Inpatient'))
	or
	Diag_I_9 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Acute Inpatient'))
	or
	Diag_I_10 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Acute Inpatient'))
	or
	CPT in(select code from VALUESET_TO_CODE where Value_Set_Name in('Acute Inpatient'))
	or
	CPT2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Acute Inpatient'))
	or
	HCPCS in(select code from VALUESET_TO_CODE where Value_Set_Name in('Acute Inpatient'))
	or 
	Proc_I_1 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Acute Inpatient'))
	or 
	Proc_I_2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Acute Inpatient'))
	or 
	Proc_I_3 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Acute Inpatient'))
	or 
	Proc_I_4 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Acute Inpatient'))
	or 
	Proc_I_5 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Acute Inpatient'))
	or 
	Proc_I_6 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Acute Inpatient'))
	
	)
	)t1
	Join #spd_advillness a on a.Memid=t1.MemID and a.claimid=t1.claimid
	)t2
	join #spd_frailtymembers f on f.Memid=t2.MemID

	CREATE CLUSTERED INDEX idx_spc_reqdexcl2 ON #spd_reqdexcl2 ([memid]);
	
	update #spddataset set rexcl=1 from #spddataset ds
	join #spd_reqdexcl2 re2 on re2.Memid=ds.patient_id and ds.age BETWEEN 66 AND 80;
	
			
	-- Required exclusion 3
	drop table if exists #spd_reqdexcl3;
	CREATE table #spd_reqdexcl3
	(
		Memid varchar(50)
			
	);

	

	insert into #spd_reqdexcl3
	select distinct t2.MemId from(
	select t1.Memid,t1.date_s,t1.claimid from(
	select inp.Memid,inp.date_s,inp.claimid from #spd_inpatientstaylist inp
	left outer join #spd_noncauteinpatientstaylist na on inp.Memid=na.Memid and inp.claimid=na.claimid
	where na.Memid is null
	)t1
	join #spd_advillness a on a.Memid=t1.Memid and a.claimid=t1.claimid
	)t2
	join #spd_frailtymembers f on f.Memid=t2.Memid

	CREATE CLUSTERED INDEX idx_spc_reqdexcl3 ON #spd_reqdexcl3 ([memid]);
	
	update #spddataset set rexcl=1 from #spddataset ds
	join #spd_reqdexcl3 re3 on re3.Memid=ds.patient_id and ds.age BETWEEN 66 AND 80;

		
-- RequiredExcl 4
	drop table if exists #spd_reqdexcl4;
	CREATE table #spd_reqdexcl4
	(
		Memid varchar(50)
				
	);

	

	insert into #spd_reqdexcl4
	select t1.Memid from(
	select Memid from HEDIS_PHARM where Measure_id=@meas  and suppdata='N' and PrServDate!='' and  PrServDate between @ce_startdt1 and @ce_enddt and NDC in(select code from MEDICATION_LIST_TO_CODES where Medication_List_Name='Dementia Medications')
	)t1
	Join #spd_frailtymembers f on f.MemId=t1.Memid;

	CREATE CLUSTERED INDEX idx_spc_reqdexcl4 ON #spd_reqdexcl4 ([memid]);
	
	update #spddataset set rexcl=1 from #spddataset ds
	join #spd_reqdexcl4 re4 on re4.Memid=ds.patient_id and ds.age BETWEEN 66 AND 80;
	

-- End of Exclusions

-- Start of Event List for SPD


	
--Numerator
	-- SPC A
	
	
DROP TABLE IF EXISTS #spd_spdamemlist;
	
CREATE table #spd_spdamemlist
(
	Memid varchar(50)

			
);

Insert into #spd_spdamemlist
select distinct Memid from(
select Memid from HEDIS_PHARM where Measure_id=@meas and Prservdate!='' and PrServdate between @ce_startdt and @ce_enddt and NDC in(select code from MEDICATION_LIST_TO_CODES where Medication_List_Name in('Atorvastatin High Intensity Medications','Amlodipine Atorvastatin High Intensity Medications','Rosuvastatin High Intensity Medications','Simvastatin High Intensity Medications','Ezetimibe Simvastatin High Intensity Medications','Atorvastatin Moderate Intensity Medications','Amlodipine Atorvastatin Moderate Intensity Medications','Rosuvastatin Moderate Intensity Medications  ','Simvastatin Moderate Intensity Medications','Ezetimibe Simvastatin Moderate Intensity Medications','Pravastatin Moderate Intensity Medications','Lovastatin Moderate Intensity Medications','Fluvastatin Moderate Intensity Medications','Pitavastatin Moderate Intensity Medications','Ezetimibe Simvastatin Low Intensity Medications','Fluvastatin Low Intensity Medications','Lovastatin Low Intensity Medications','Pravastatin Low Intensity Medications','Simvastatin Low Intensity Medications'
))
Union all
select Memid from HEDIS_PHARM_C where Measure_id=@meas and Startdate!='' and Startdate between @ce_startdt and @ce_enddt and Rxnorm in(select code from MEDICATION_LIST_TO_CODES where Medication_List_Name in('Atorvastatin High Intensity Medications','Amlodipine Atorvastatin High Intensity Medications','Rosuvastatin High Intensity Medications','Simvastatin High Intensity Medications','Ezetimibe Simvastatin High Intensity Medications','Atorvastatin Moderate Intensity Medications','Amlodipine Atorvastatin Moderate Intensity Medications','Rosuvastatin Moderate Intensity Medications  ','Simvastatin Moderate Intensity Medications','Ezetimibe Simvastatin Moderate Intensity Medications','Pravastatin Moderate Intensity Medications','Lovastatin Moderate Intensity Medications','Fluvastatin Moderate Intensity Medications','Pitavastatin Moderate Intensity Medications','Ezetimibe Simvastatin Low Intensity Medications','Fluvastatin Low Intensity Medications','Lovastatin Low Intensity Medications','Pravastatin Low Intensity Medications','Simvastatin Low Intensity Medications'
))
)f1
	
CREATE CLUSTERED INDEX idx_spc_spcamemlist ON #spd_spdamemlist ([memid]);

update #spddataset set num=1 from #spddataset ds join #spd_spdamemlist n1 on ds.patient_id=n1.MemID and ds.meas='SPDA'



-- SPC B Event


 update ds set ds.Event=1 from #spddataset ds join #spddataset s1 on ds.patient_id=s1.patient_id and ds.meas='SPDB' and s1.num=1 and s1.meas='SPDA';

 


-- SPD B Numerator


DROP TABLE IF EXISTS #spd_statinmedmemlist;
	
CREATE table #spd_statinmedmemlist
(
	Memid varchar(50),
	pdc INT
			
);

Insert into #spd_statinmedmemlist
select Memid,sum(pdc) as pdc from(
-- adjust DOS for year end after summing
select Memid,Servicedate,case when dateadd(day,ISNULL(NULLIF(calcdos,''),1)-1,servicedate) >=@ce_enddt then datediff(day,servicedate,@ce_enddt)+1 else calcdos end as pdc from(
select memid,servicedate,sum(calcdaysofsupply) as calcdos from(
-- SUM the DOS supply after adjustment
select distinct memid,servicedate,calcdaysofsupply from(
-- Adjust DOS to accomodate year end
select *,case when dateadd(day,ISNULL(NULLIF(calcdos,''),1)-1,servicedate) >=@ce_enddt then datediff(day,servicedate,@ce_enddt)+1 else calcdos end as calcdaysofsupply from(
select *,case 
-- different Medication on same date ,take one with more DOS
when servicedate=nextmeddate  and rectype=nextrectype and medication!=nextmed and daysofsupply>nextdos then daysofsupply
when servicedate=nextmeddate and rectype=nextrectype and medication!=nextmed and daysofsupply<nextdos then nextdos
when servicedate=prevmeddate  and rectype=prevrectype and medication!=prevmed and daysofsupply>prevdos then daysofsupply
when servicedate=prevmeddate and rectype=prevrectype and medication!=prevmed and daysofsupply<prevdos then prevdos
-- Ignore records which are on same date from Hedis_pharm and Hedis_pharm_C(Rxnorm vs NDC)
when servicedate=nextmeddate  and rectype='C' and nextrectype='P' and nextdos>daysofsupply  then nextdos
when servicedate=nextmeddate  and rectype='C' and nextrectype='P' and nextdos<daysofsupply  then 0
when servicedate=prevmeddate  and rectype='C' and prevrectype='P' and prevdos>daysofsupply  then prevdos
when servicedate=prevmeddate  and rectype='C' and prevrectype='P' and prevdos<daysofsupply  then 0
when servicedate=nextmeddate  and rectype='P' and nextrectype='C' and nextdos>daysofsupply  then nextdos
when servicedate=nextmeddate  and rectype='P' and nextrectype='C' and nextdos<daysofsupply  then 0
when servicedate=prevmeddate  and rectype='P' and prevrectype='C' and prevdos>daysofsupply  then prevdos
when servicedate=prevmeddate  and rectype='P' and prevrectype='C' and prevdos<daysofsupply  then 0
-- Adjust DOS to accomodate overlap with next record
when servicedate!=nextmeddate and medication!=nextmed and enddate>= ISNULL(nextmeddate,enddate) then daysofsupply-(datediff(day,ISNULL(nextmeddate,enddate),enddate)+1)
else daysofsupply
end as calcdos
from(
-- Get Previous and next record for further processing
select *,convert(varchar,cast(dateadd(day,ISNULL(NULLIF(daysofsupply,''),1)-1,servicedate) as date),112) as enddate,isnull(lead(servicedate,1) over(partition by Memid order by servicedate,daysofsupply),servicedate) as nextmeddate,isnull(lead(Medication,1) over(partition by Memid order by servicedate,daysofsupply),medication) as nextmed,isnull(lead(daysofsupply,1) over(partition by Memid order by servicedate,daysofsupply),daysofsupply) as nextdos,isnull(lead(rectype,1) over(partition by Memid order by servicedate,daysofsupply),rectype) as nextrectype,isnull(lag(servicedate,1) over(partition by Memid order by servicedate,daysofsupply),servicedate) as prevmeddate,isnull(lag(Medication,1) over(partition by Memid order by servicedate,daysofsupply),medication) as prevmed,isnull(lag(daysofsupply,1) over(partition by Memid order by servicedate,daysofsupply),daysofsupply) as prevdos,isnull(lag(rectype,1) over(partition by Memid order by servicedate,daysofsupply),rectype) as prevrectype from(
select t1.memid,t1.servicedate,cast(t1.daysofsupply as INT) as daysofsupply,t2.Medication,rectype from (
-- Read Mediction from Hedis_Pharm

select z1.Memid,z1.servicedate,z1.daysofsupply,z1.code,rectype from(
select Memid,Prservdate as servicedate,PDaysSup as daysofsupply,NDC as code,'P' as rectype from HEDIS_PHARM where Measure_id=@meas and Prservdate!='' and PrServdate between @ce_startdt and @ce_enddt

Union all
-- Read Mediction from Hedis_Pharm_C
select Memid,startdate as servicedate,qty as daysofsupply,rxnorm as code,'C' as rectype from HEDIS_PHARM_C where Measure_id=@meas and Startdate!='' and Startdate between @ce_startdt and @ce_enddt

)z1   

)t1
join(
select code,Medication_List_Name as Medication 
/*
case when medication_list_name in('Atorvastatin High Intensity Medications','Amlodipine Atorvastatin High Intensity Medications','Rosuvastatin High Intensity Medications','Simvastatin High Intensity Medications','Ezetimibe Simvastatin High Intensity Medications') then 'High Intensity Medication' else 'Moderate Intensity Medication' end as Medication*/
from MEDICATION_LIST_TO_CODES where Medication_List_Name in('Atorvastatin High Intensity Medications','Amlodipine Atorvastatin High Intensity Medications','Rosuvastatin High Intensity Medications','Simvastatin High Intensity Medications','Ezetimibe Simvastatin High Intensity Medications','Atorvastatin Moderate Intensity Medications','Amlodipine Atorvastatin Moderate Intensity Medications','Rosuvastatin Moderate Intensity Medications  ','Simvastatin Moderate Intensity Medications','Ezetimibe Simvastatin Moderate Intensity Medications','Pravastatin Moderate Intensity Medications','Lovastatin Moderate Intensity Medications','Fluvastatin Moderate Intensity Medications','Pitavastatin Moderate Intensity Medications','Ezetimibe Simvastatin Low Intensity Medications','Fluvastatin Low Intensity Medications','Lovastatin Low Intensity Medications','Pravastatin Low Intensity Medications','Simvastatin Low Intensity Medications')
)t2 on t1.code=t2.code
)t3
)t4
)t5 
)t6 


)t7 group by memid,servicedate
)t8 
)t9 group by memid order by memid


CREATE CLUSTERED INDEX idx_spc_statinmedmemlist ON #spd_statinmedmemlist ([memid],[pdc]);
-- IPSD


drop table if exists 	#spd_statinmedipsdlist;
CREATE table #spd_statinmedipsdlist
(
	Memid varchar(50),
	ipsd varchar(8),
	treatmentperiod INT

			
);

Insert into #spd_statinmedipsdlist
select z1.MemID,min(z1.servicedate) as ipsd,datediff(day,min(z1.servicedate),@ce_enddt)+1 as treatmentperiod from(
select Memid,Prservdate as servicedate,case when convert(varchar,cast(DATEADD(day,PDaysSup-1,Prservdate) as date),112) >@ce_enddt then @ce_enddt else convert(varchar,cast(DATEADD(day,PDaysSup-1,Prservdate) as date),112) end as Enddate,NDC as code  from HEDIS_PHARM where Measure_id=@meas  and Prservdate!='' and PrServdate between @ce_startdt and @ce_enddt and NDC in(select code from MEDICATION_LIST_TO_CODES where Medication_List_Name in('Atorvastatin High Intensity Medications','Amlodipine Atorvastatin High Intensity Medications','Rosuvastatin High Intensity Medications','Simvastatin High Intensity Medications','Ezetimibe Simvastatin High Intensity Medications','Atorvastatin Moderate Intensity Medications','Amlodipine Atorvastatin Moderate Intensity Medications','Rosuvastatin Moderate Intensity Medications  ','Simvastatin Moderate Intensity Medications','Ezetimibe Simvastatin Moderate Intensity Medications','Pravastatin Moderate Intensity Medications','Lovastatin Moderate Intensity Medications','Fluvastatin Moderate Intensity Medications','Pitavastatin Moderate Intensity Medications','Ezetimibe Simvastatin Low Intensity Medications','Fluvastatin Low Intensity Medications','Lovastatin Low Intensity Medications','Pravastatin Low Intensity Medications','Simvastatin Low Intensity Medications'))


Union all

select Memid,startdate as servicedate,case when Edate >@ce_enddt then @ce_enddt else Edate end as enddate,rxnorm as code from HEDIS_PHARM_C where Measure_id=@meas and Startdate!='' and Startdate between @ce_startdt and @ce_enddt and RxNorm in(select code from MEDICATION_LIST_TO_CODES where Medication_List_Name in('Atorvastatin High Intensity Medications','Amlodipine Atorvastatin High Intensity Medications','Rosuvastatin High Intensity Medications','Simvastatin High Intensity Medications','Ezetimibe Simvastatin High Intensity Medications','Atorvastatin Moderate Intensity Medications','Amlodipine Atorvastatin Moderate Intensity Medications','Rosuvastatin Moderate Intensity Medications  ','Simvastatin Moderate Intensity Medications','Ezetimibe Simvastatin Moderate Intensity Medications','Pravastatin Moderate Intensity Medications','Lovastatin Moderate Intensity Medications','Fluvastatin Moderate Intensity Medications','Pitavastatin Moderate Intensity Medications','Ezetimibe Simvastatin Low Intensity Medications','Fluvastatin Low Intensity Medications','Lovastatin Low Intensity Medications','Pravastatin Low Intensity Medications','Simvastatin Low Intensity Medications'))

)z1 group by z1.MemID

CREATE CLUSTERED INDEX idx_spc_statinmedipsdlist ON #spd_statinmedipsdlist ([memid],[ipsd]);

CREATE table #spd_rate2numlist
(
	Memid varchar(50),
	
);

Insert into #spd_rate2numlist
select distinct f1.Memid from(
select s1.memid,s1.treatmentperiod,s2.pdc,
Round(((cast(pdc as float)/cast(treatmentperiod as float))*100),0) as adherence
,cast(pdc as float)/cast(treatmentperiod as float)*100 as raw
from #spd_statinmedipsdlist s1
join #spd_statinmedmemlist s2 on s1.Memid=s2.Memid
 
)f1 where f1.adherence>=80 

CREATE CLUSTERED INDEX idx_spc_rate2numlist ON #spd_rate2numlist ([memid]);

update #spddataset set num=1 from #spddataset ds join #spd_rate2numlist n1 on ds.patient_id=n1.MemID and ds.meas='SPDB'


-- Opptional Exclusion

-- Patients with Induced Diabetes
	drop table if exists #spd_induceddiabetesmemlist;
	CREATE table #spd_induceddiabetesmemlist
	(
		Memid varchar(50)
				
	);

	

	Insert into #spd_induceddiabetesmemlist
	select Memid from(
	select MemId from HEDIS_DIAG where MEASURE_ID=@meas and sdate between @ce_startdt1 and @ce_enddt and sdate!=''  and dcode IN(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetes Exclusions')) 
	Union all
	select MemID from HEDIS_LAB where MEASURE_ID=@meas and date_s between @ce_startdt1 and @ce_enddt and date_s!='' and (LOINC in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetes Exclusions')) or CPT_Code IN(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetes Exclusions')))
	union all
	select MemId from HEDIS_OBS where MEASURE_ID=@meas and date between @ce_startdt1 and @ce_enddt and date!='' and OCode IN(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetes Exclusions'))
	Union all
	select MemID from HEDIS_PROC where MEASURE_ID=@meas and sdate between @ce_startdt1 and @ce_enddt and sdate!='' and pstatus='EVN' and  pcode IN(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetes Exclusions'))
	Union all
	Select Memid from HEDIS_VISIT_E where MEASURE_ID=@meas and sdate between @ce_startdt1 and @ce_enddt and activity IN(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetes Exclusions'))

	Union all
	select MemID from HEDIS_VISIT v
	where MEASURE_ID=@meas and Date_S between @ce_startdt1 and @ce_enddt and Date_S!='' and hcfapos!=81
	and (
	Diag_I_1 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetes Exclusions'))
	or
	Diag_I_2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetes Exclusions'))
	or
	Diag_I_3 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetes Exclusions'))
	or
	Diag_I_4 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetes Exclusions'))
	or
	Diag_I_5 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetes Exclusions'))
	or
	Diag_I_6 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetes Exclusions'))
	or
	Diag_I_7 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetes Exclusions'))
	or
	Diag_I_8 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetes Exclusions'))
	or
	Diag_I_9 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetes Exclusions'))
	or
	Diag_I_10 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetes Exclusions'))
	or
	CPT in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetes Exclusions'))
	or
	CPT2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetes Exclusions'))
	or
	HCPCS in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetes Exclusions'))
	or 
	Proc_I_1 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetes Exclusions'))
	or 
	Proc_I_2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetes Exclusions'))
	or 
	Proc_I_3 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetes Exclusions'))
	or 
	Proc_I_4 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetes Exclusions'))
	or 
	Proc_I_5 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetes Exclusions'))
	or 
	Proc_I_6 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetes Exclusions'))
		
	or
	Rev in(select code from VALUESET_TO_CODE where code_system='UBREV' and  Value_Set_Name in('Diabetes Exclusions'))
	)
	)t1
	
	CREATE CLUSTERED INDEX idx_cdc_induceddiabetesmemlist ON #spd_induceddiabetesmemlist ([memid]);
	
	-- Memebers with no diabetes
	drop table if exists #spd_nondiabeticmemlist;
	CREATE table #spd_nondiabeticmemlist
	(
		Memid varchar(50)
				
	);

	

	Insert into #spd_nondiabeticmemlist
	select Memid from #spd_memlist where memid not in(
	select MemId from HEDIS_DIAG where MEASURE_ID=@meas and sdate between @ce_startdt1 and @ce_enddt and sdate!=''  and dcode IN(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetes')) 
	Union all
	select MemID from HEDIS_LAB where MEASURE_ID=@meas and date_s between @ce_startdt1 and @ce_enddt and date_s!='' and (LOINC in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetes')) or CPT_Code IN(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetes')))
	union all
	select MemId from HEDIS_OBS where MEASURE_ID=@meas and date between @ce_startdt1 and @ce_enddt and date!='' and OCode IN(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetes'))
	Union all
	select MemID from HEDIS_PROC where MEASURE_ID=@meas and sdate between @ce_startdt1 and @ce_enddt and sdate!='' and pstatus='EVN' and  pcode IN(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetes'))
	Union all
	Select Memid from HEDIS_VISIT_E where MEASURE_ID=@meas and sdate between @ce_startdt1 and @ce_enddt and activity IN(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetes'))

	Union all
	select MemID from HEDIS_VISIT v
	where MEASURE_ID=@meas and Date_S between @ce_startdt1 and @ce_enddt and Date_S!='' and hcfapos!=81
	and (
	Diag_I_1 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetes'))
	or
	Diag_I_2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetes'))
	or
	Diag_I_3 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetes'))
	or
	Diag_I_4 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetes'))
	or
	Diag_I_5 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetes'))
	or
	Diag_I_6 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetes'))
	or
	Diag_I_7 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetes'))
	or
	Diag_I_8 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetes'))
	or
	Diag_I_9 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetes'))
	or
	Diag_I_10 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetes'))
	or
	CPT in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetes'))
	or
	CPT2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetes'))
	or
	HCPCS in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetes'))
	or 
	Proc_I_1 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetes'))
	or 
	Proc_I_2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetes'))
	or 
	Proc_I_3 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetes'))
	or 
	Proc_I_4 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetes'))
	or 
	Proc_I_5 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetes'))
	or 
	Proc_I_6 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diabetes'))
	
	or
	Rev in(select code from VALUESET_TO_CODE where code_system='UBREV' and  Value_Set_Name in('Diabetes'))
	)
)
	
	CREATE CLUSTERED INDEX idxcdc_nondiabeticmemlist ON #spd_nondiabeticmemlist ([memid]);
	
-- Optional Exclusion
	drop table if exists #spd_optexcl;
	CREATE table #spd_optexcl
	(
		Memid varchar(50)
				
	);

	

	Insert into #spd_optexcl
	select t1.memid from #spd_nondiabeticmemlist t1
	join #spd_induceddiabetesmemlist t2 on t1.memid=t2.memid
	
	
	CREATE CLUSTERED INDEX idx_cdc_optexcl ON #spd_optexcl ([memid]);
	
	update #spddataset set excl=1 from #spddataset ds
	join #spd_optexcl oe on oe.Memid=ds.patient_id;

	
---
	

	-- Generate Output
	select @runid=max(RUN_ID) from HEDIS_MEASURE_OUTPUT where Measure_id=@meas;

	if(@runid>=1)
	Begin
		SET @runid=@runid+1;
	End
	Else
	Begin
		SET @runid=1;
	END
	

	Insert into HEDIS_MEASURE_OUTPUT(memid,Meas,payer,CE,Event,Epop,Excl,Num,Rexcl,RExclD,Age,Gender,Measure_ID,Measurement_year,RUN_ID)
	SELECT patient_id AS memid,meas,payer,CE,EVENT,CASE WHEN CE=1 and Event=1 AND rexcl=0 and rexcld=0 and payer not in('MMO','MOS','MPO','MEP','MC') THEN 1 ELSE 0 END AS epop,excl,num,rexcl,rexcld,cast(age as Int) AS age,patient_gender AS gender,@meas,@meas_year,@runid FROM #spddataset

GO
