SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
CREATE Proc [HDS].[Score_Comparison]  @Measure_ID varchar(100)='CHL'
AS

select 'MEAS_OUTPUT' as MEAS_OUTPUT,Trim(O.Memid) as Memid,O.Meas,O.Payer,O.CE,O.Event,O.Epop,O.Excl,O.Num,O.Rexcl,O.RExcld,O.Age,O.Gender,
'SCORE' as SCORE,S.Memid,S.Meas,S.Payer,S.CE,S.Event,S.Epop,S.Excl,S.Num,S.Rexcl,S.RExcld,S.Age,S.Gender 
from HDS.HEDIS_MEASURE_OUTPUT O
left join hds.HEDIS_SCORE S
on O.MemID=S.MemID and O.MEASURE_ID=LEFT(S.MEASURE_ID,3) and O.Payer=S.Payer
where O.MEASURE_ID=@Measure_ID and O.run_id=(select max(run_id) from HDS.HEDIS_MEASURE_OUTPUT where measure_id=@Measure_ID)
AND (O.CE<>S.CE or O.Event<>S.Event or O.Epop<>S.Epop or O.Excl<>S.Excl or O.Num<>S.Num or O.Rexcl<>O.Rexcl or O.RExcld<>S.RExcld)
order by Trim(O.Memid)




select Trim(Memid) as MemId,Meas,Payer,CE,Event,Epop,Excl,Num,Rexcl,RExcld,Age,Gender 
from HDS.HEDIS_MEASURE_OUTPUT where MEASURE_ID='WCV' and run_id=(select max(run_id) from HDS.HEDIS_MEASURE_OUTPUT where measure_id='WCV')
GO
