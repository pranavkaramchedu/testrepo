SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
CREATE PROCEDURE DBO.LOAD_DIAGNOSIS_TO_HEDIS
AS
BEGIN
INSERT INTO  [KPI_ENGINE].[HDS].[HEDIS_DIAG]
(
   	   [MemID]
      ,[sdate]
      ,[dcode]
      ,[dFlag]
      ,[edate]
      ,[ordin]
      ,[tsccode]
      ,[tsctext]
      ,[relcode]
      ,[reltext]
      ,[attcode]
      ,[atttext]
      ,[Sympflag]
      ,[qrda]
      ,[MEASURE_ID]
      ,[LOAD_DATE]
)
SELECT EMPI AS [MemID]
      ,DIAG_START_DATE AS [sdate]
      ,DIAG_CODE AS [dcode]
      ,CASE WHEN DIAG_CODE_TYPE='ICD-10' THEN 'X' ELSE CASE WHEN DIAG_CODE_TYPE='ICD-9' THEN '9'  END END AS [dFlag]
      ,DIAG_END_DATE AS [edate]
      ,NULL AS [ordin]
      ,NULL AS [tsccode]
      ,NULL AS [tsctext]
      ,NULL AS [relcode]
      ,NULL AS [reltext]
      ,NULL AS [attcode]
      ,NULL AS [atttext]
      ,'D' AS [Sympflag]
      ,NULL AS [qrda]
      ,'159' AS [MEASURE_ID]
      ,CONVERT(DATE,GETDATE()) AS [LOAD_DATE]
 --- FROM [KPI_ENGINE].[HDS].[HEDIS_DIAG]
 FROM KPI_ENGINE.dbo.Diagnosis

 END


GO
