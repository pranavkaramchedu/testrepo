SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

CREATE proc [dbo].[KPI_UHN_MedicalReconciliation_45]
AS

BEGIN
DECLARE @INFO VARCHAR(8000)
					,	@TRUE BIT = 1
					,	@FALSE BIT = 0
					,	@DB_ID INT = DB_ID()
					,	@LOGID INT = 0
					,	@PROC VARCHAR(500)=OBJECT_SCHEMA_NAME(@@PROCID)+'.'+OBJECT_NAME(@@PROCID)
					,	@PERF_START DATETIME
					,	@PERF_DURATION INT
					,	@PERF_ROW INT
					,	@RC	INT
					;

		EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, @INFO=@INFO, @LOG2ID=@LOGID OUT;
		DECLARE @PROCFULLNAME VARCHAR(500) = DB_NAME()+'.'+@PROC;
		----EXEC dbo.SP_MI_UTIL_SEND_MAIL @PROCFULLNAME, 'STARTED', 1, @ECHO=0; 
	DECLARE @MSG VARCHAR(1000)
	DECLARE @PERF_START_PROC DATETIME 
	DECLARE @PERF_RPS NUMERIC(10,2) 
	DECLARE @ROWS INT 
	DECLARE @PERF_ROWS INT 
	SET @PERF_ROWS = 0 
	SET @PERF_START = GETDATE() 
	SET @PERF_START_PROC = GETDATE() 

BEGIN TRY



-- Declare Variables
declare @rundate Date=GetDate();
declare @meas_year varchar(4)=Year(Dateadd(month,-2,@rundate))
declare @rootId INT=159
declare @attributiondate Date=DATEADD(month, DATEDIFF(month, 0, Dateadd(month,-2,@rundate)), 0)


Declare @startDate Date;
Declare @enddate date;
Declare @quarter varchar(20)
Declare @reportId INT;
Declare @measure_id varchar(10);
Declare @target INT;
Declare @domain varchar(100);
Declare @subdomain varchar(100);
Declare @measuretype varchar(100);
Declare @measurename varchar(100);
Declare @reporttype varchar(100);

Declare @ce_startdt Date;
Declare @ce_startdt1 Date;
Declare @ce_novdt Date;
Declare @ce_enddt Date;
Declare @ce_enddt1 Date;
Declare @ce_dischargeenddt Date;

-- Set Measure dates
SET @ce_startdt=concat(@meas_year,'-01-01');
SET @ce_novdt=concat(@meas_year,'-11-30');
SET @ce_enddt=concat(@meas_year,'-12-31');
SET @ce_startdt1=concat(@meas_year-1,'-01-01');
SET @ce_enddt1=concat(@meas_year-1,'-12-31');
SET @ce_dischargeenddt=concat(@meas_year,'-12-01');


Set @reporttype='Network'
Set @measurename='Medication Reconciliation'
--Set @startDate=DATEADD(yy, DATEDIFF(yy, 0,Dateadd(month,-2,GETDATE())), 0) 
--Set @enddate=eomonth(Dateadd(month,-2,GetDate()))
--Set @quarter=Concat(Year(@enddate),' - Q',DATEPART(q, @enddate))
set @target=84
Set @domain='Utilization Management'
Set @subdomain='Medication Management'
Set @measuretype='Calculated'
Set @measure_id='45'


-- Identify Deaceased Members
Drop table if exists #45_deceasedmembers
Create table #45_deceasedmembers
(
	EMPI varchar(100)
)
Insert into #45_deceasedmembers
select * from deceasedmembers(@rootId,@ce_startdt,@ce_enddt)


-- Identify Hospice exclusions
drop table if exists #45_hospicemembers;
CREATE table #45_hospicemembers
(
	EMPI varchar(50)
		
);
Insert into #45_hospicemembers
select distinct EMPI from hospicemembers(@rootId,@ce_startdt,@ce_enddt)


-- Identify SNF Stays
drop table if exists #45_SNFStays;
create table #45_SNFStays
(
	EMPI varchar(50),
	ADM_DATE Date,
	DIS_DATE Date,
	CLAIM_ID VARCHAR(100)
)
Insert into #45_SNFStays
select distinct
	EMPI
	,Case
		when ADM_DATE is null then FROM_DATE
		else ADM_DATE
	End as ADM_DATE
	,Case
		When DIS_DATE is null Then TO_DATE
		else DIS_DATE
	End as DIS_DATE
	,CLAIM_ID 
from KPI_ENGINE.dbo.CLAIMLINE 
where ROOT_COMPANIES_ID=@rootId and 
	  
	  DIS_DATE between @ce_startdt and @ce_enddt  and 
	  ISNULL(POS,'')!='81'	and	
	  (
		REV_CODE in
		(
			select code from KPI_ENGINE.HDS.VALUESET_TO_CODE where Code_System='UBREV' and Value_Set_Name='Skilled Nursing Stay'
		) 
		or 
		RIGHT('0000'+CAST(Trim(UB_BILL_TYPE) AS VARCHAR(4)),4) in
		(
			select code from KPI_ENGINE.HDS.VALUESET_TO_CODE where code_system='UBTOB' and Value_Set_Name='Skilled Nursing Stay'
		)
	) 



-- Identify Inpatient Stay List
drop table if exists #45_ipstaylist;
CREATE table #45_ipstaylist
(
	EMPI varchar(100),
	ADM_DATE Date,
	DIS_DATE Date,
	CLAIM_ID varchar(100)
				
);
Insert into #45_ipstaylist
-- Inpatient Stay list
select distinct
	c.EMPI
	,Case
		when ADM_DATE is null then FROM_DATE
		else ADM_DATE
	End as ADM_DATE
	,Case
		When DIS_DATE is null Then TO_DATE
		else DIS_DATE
	End as DIS_DATE
	,CLAIM_ID
from KPI_ENGINE.dbo.CLAIMLINE c 
where ROOT_COMPANIES_ID=@rootId and 
	  
	  c.DIS_DATE between @ce_startdt and @ce_enddt  and 
	  ISNULL(c.POS,'')!='81' and
	  c.REV_CODE in
	  (
		select code from KPI_ENGINE.HDS.VALUESET_TO_CODE where code_system='UBREV' and Value_Set_Name in('Inpatient Stay')
	  )	



-- Identify Observation Stay List
drop table if exists #45_observationtaylist;
CREATE table #45_observationtaylist
(
	EMPI varchar(100),
	ADM_DATE Date,
	DIS_DATE Date,
	CLAIM_ID varchar(100)
				
);
Insert into #45_observationtaylist
-- Inpatient Stay list
select distinct
	c.EMPI
	,Case
		when ADM_DATE is null then FROM_DATE
		else ADM_DATE
	End as ADM_DATE
	,Case
		When DIS_DATE is null Then TO_DATE
		else DIS_DATE
	End as DIS_DATE
	,CLAIM_ID
from KPI_ENGINE.dbo.CLAIMLINE c 
where ROOT_COMPANIES_ID=@rootId and 
	  
	  c.DIS_DATE between @ce_startdt and @ce_enddt  and 
	  ISNULL(c.POS,'')!='81' and
	  c.REV_CODE in
	  (
		select code from KPI_ENGINE.HDS.VALUESET_TO_CODE where code_system='UBREV' and Value_Set_Name in('Observation Stay')
	  )	

-- Identify Non Acute Stays
drop table if exists #45_nonacutestaylist;
CREATE table #45_nonacutestaylist
(
	EMPI varchar(100),
	ADM_DATE Date,
	DIS_DATE Date,
	CLAIM_ID varchar(100)
				
);
Insert into #45_nonacutestaylist
-- Inpatient Stay list
select distinct
	c.EMPI
	,c.ADM_DATE
	,c.DIS_DATE
	,CLAIM_ID
from KPI_ENGINE.dbo.CLAIMLINE c 
where ROOT_COMPANIES_ID=@rootId and 
	 
	  c.DIS_DATE between @ce_startdt and @ce_enddt  and 
	  ISNULL(c.POS,'')!='81' and
	  (
		  RIGHT('0000'+CAST(Trim(c.REV_CODE) AS VARCHAR(4)),4) in
		  (
			select code from KPI_ENGINE.HDS.VALUESET_TO_CODE where code_system='UBREV' and Value_Set_Name in('Nonacute Inpatient Stay')
		  )
		  or
		  RIGHT('0000'+CAST(Trim(c.UB_Bill_Type) AS VARCHAR(4)),4) in
		  (
			select code from KPI_ENGINE.HDS.VALUESET_TO_CODE where code_system='UBTOB' and Value_Set_Name in('Nonacute Inpatient Stay')
		  )
	   )


-- Identify Acute Stays
drop table if exists #45_acutestaylist;
CREATE table #45_acutestaylist
(
	EMPI varchar(100),
	ADM_DATE Date,
	DIS_DATE Date,
	CLAIM_ID varchar(100)
				
);
Insert into #45_acutestaylist
select distinct
	t1.EMPI
	,t1.ADM_DATE
	,t1.DIS_DATE
	,t1.CLAIM_ID
from #45_ipstaylist t1
left outer Join #45_nonacutestaylist n on t1.EMPI=n.EMPI and
										  t1.CLAIM_ID=n.CLAIM_ID
Where
	n.EMPI is Null



--Identify direct Transfers
drop table if exists #45_directtransfers;
Create table #45_directtransfers
(
	EMPI varchar(100),
	ADM_DATE Date,
	DIS_DATE Date,
	AdmissionClaimId varchar(100),
	DischargeClaimId varchar(100),
	grp_claimid varchar(600)
)
;with grp_starts as 
(
  select 
	EMPI
	,ADM_DATE
	,DIS_DATE
	,CLAIM_ID
	,case
		when datediff(day,lag(DIS_DATE) over(partition by EMPI order by ADM_DATE, DIS_DATE),ADM_DATE) <=30 	
		or 
		(datediff(day,lag(DIS_DATE,2) over(partition by EMPI order by ADM_DATE, DIS_DATE),ADM_DATE)<=30 )
		then 0 
		else 1
	end grp_start
  from 
  (
	select distinct * from
	(
		select * from #45_ipstaylist
		union all
		select * from #45_SNFStays
	)z1
  )z2

)
, grps as 
(
  select 
	EMPI
	,ADM_DATE
	, DIS_DATE
	,CLAIM_ID
	,sum(grp_start) over(partition by EMPI order by ADM_DATE, DIS_DATE) grp
  from grp_starts
)
Insert into #45_directtransfers
select 
	t1.EMPI
	,AdmissionDate
	,DischargeDate
	,AdmissionClaimId
	,DischargeClaimId
	,grp_claimid 
from	
(
			select 
				EMPI
				,min(ADM_DATE) as AdmissionDate
				,max(DIS_DATE) as DischargeDate
				,min(CLAIM_ID) as AdmissionClaimId
				,max(CLAIM_ID) as DischargeClaimId
				,STRING_AGG(CLAIM_ID,',') as grp_claimid
			from grps 
			group by EMPI,grp
		
)t1
left outer Join #45_deceasedmembers m on 
	t1.EMPI=m.EMPI
where 
	DischargeDate<=@ce_dischargeenddt and
	m.EMPI is null


	
	Drop table if exists #45_dischargefacility;
	Select
		*
		into #45_dischargefacility
	From
	(
		select 
			t.EMPI,
			t.dis_date,
			c.BILL_PROV,
			coalesce(n.Provider_Organization_Name,BILL_PROV_NAME) as BillingProvider,
			c.FROM_DATE,
			Row_number() over(Partition By t.EMPI,t.DIS_DATE order by Provider_Organization_Name desc,c.FROM_DATE desc) as rn
		from #45_directtransfers t
		Join CLAIMLINE c on
			c.EMPI=t.EMPI and
			c.CLAIM_ID=t.DischargeClaimId
		Left Outer Join RFT_NPI n on
			c.BILL_PROV=n.NPI and
			n.Entity_Type_Code=2
	)t1
	Where
		rn=1
	
		

	
		


-- Identify Medical Reconciliation EVent
Drop table if exists #45_medicalrecon;
Create table #45_medicalrecon
(
	EMPI varchar(100),
	FROM_DATE Date,
	Code varchar(20)

)
Insert into #45_medicalrecon
Select
	EMPI
	,PROC_START_DATE
	,PROC_CODE
From GetProceduresWithOutMods(@rootId,@ce_startdt,@ce_enddt,'Medication Reconciliation Intervention,Medication Reconciliation Encounter','CPT CAT II Modifier')
	

-- Identify Numerator Set
Drop Table if exists #45_numeratorset;
Create Table #45_numeratorset
(
	EMPI varchar(100),
	ADM_DATE Date,
	DIS_DATE Date,
	Code varchar(20),
	DateOfService Date
)
Insert into #45_numeratorset
Select distinct 
	EMPI
	,ADM_DATE
	,DIS_DATE
	,code
	,FROM_DATE 
from
(
	Select 
		EMPI
		,ADM_DATE
		,DIS_DATE
		,code
		,FROM_DATE
		,ROW_NUMBER() over (partition by EMPI,DIS_DATE order by FROM_DATE desc) as rn 
	from
	(
		select 
			*
			,DENSE_RANK() over(partition by EMPI,DIS_DATE order by FROM_DATE asc) as rnk2 
		from
		(
			select 
				*
				,DENSE_RANK() over(partition by EMPI,FROM_DATE,code order by DIS_DATE desc) as rnk1 
			from
			(
				select distinct 
					d.EMPI
					,d.ADM_DATE
					,d.DIS_DATE
					,m.FROM_DATE
					,code 
				from #45_directtransfers d
				join #45_medicalrecon m on 
					d.EMPI=m.EMPI and 
					datediff(day,d.DIS_DATE,m.FROM_DATE) between 0 and 30
			)t1

		)t2
		where 
			rnk1=1
	)t3
	where 
		rnk2=1
)t4
where 
	rn=1




--Check for continuous enrollment

drop table if exists #45_contenroll;
create table #45_contenroll
(
	EMPI varchar(100),
	DIS_DATE Date
);
With coverage_CTE1(EMPI,dischargedate,lastcoveragedate,Startdate,Finishdate,nextcoveragedate) as
(

	select distinct
		EMPI
		,DIS_DATE
		,isnull(lag(TERM_DATE,1) over(partition by EMPI,DIS_DATE order by EFF_DATE,TERM_DATE desc),convert(varchar,cast(dateadd(day,30,DIS_DATE) as date),112)) as lastcoveragedate
		,Case 
			when EFF_DATE<DIS_DATE then DIS_DATE
			else EFF_DATE 
		end as Startdate
		,case 
			when TERM_DATE>convert(varchar,cast(dateadd(day,30,DIS_DATE) as date),112) then convert(varchar,cast(dateadd(day,30,DIS_DATE) as date),112) 
			else TERM_DATE 
		end as Finishdate
		,Case
			When lead(EFF_DATE,1) over(partition by EMPI,DIS_DATE order by EFF_DATE,TERM_DATE) is not null and
				 lead(EFF_DATE,1) over(partition by EMPI,DIS_DATE order by EFF_DATE,TERM_DATE) > DIS_DATE 
				 then lead(EFF_DATE,1) over(partition by EMPI,DIS_DATE order by EFF_DATE,TERM_DATE)
			Else DIS_DATE
		End as nextcoveragedate
	From
	(
		Select distinct
			s.EMPI
			,s.DIS_DATE
			,en.EFF_DATE
			,en.TERM_DATE
		from #45_directtransfers s
		join ENROLLMENT en on s.EMPI=en.EMPI and 
							  en.ROOT_COMPANIES_ID=@rootId
		where  EFF_DATE<=convert(varchar,cast(dateadd(day,30,DIS_DATE) as date),112)  and 
			   TERM_DATE>=DIS_DATE
		--	   and s.EMPI='0123069B-6E28-4176-A890-FD62318BE03B'
	)zz1					        
)

Insert into #45_contenroll
select 
	t3.EMPI
	,t3.dischargedate 
from
(
	select 
		t2.EMPI
		,t2.dischargedate
		,sum(anchor) as anchor 
	from(
	
		select 
			*
			, case 
				when rn=1 and startdate>dischargedate then 1 
				else 0 
			end as startgap
			,case 
				when datediff(day,newFinishdate,nextcoveragedate)>0 then 1  
				else 0 
			end as gaps
			,datediff(day,Startdate,Finishdate)+1 as coveragedays
			,case
				when dischargedate between Startdate and newfinishdate then 1 
				else 0 
			end as anchor 
			from
			(
					Select 
						*
						,case 
							when datediff(day,Finishdate,nextcoveragedate) <=1 then isnull(lead(Finishdate,1) over(partition by EMPI,dischargedate order by Startdate,FinishDate),dischargedate) 
							else finishdate 
						end as newfinishdate
						,ROW_NUMBER() over(partition by EMPI,dischargedate order by Startdate,Finishdate) as rn 
					from coverage_CTE1
			)t1           
		)t2  
		group by EMPI,dischargedate 
		having (sum(gaps)+sum(startgap)<=1) and 
			   sum(coveragedays)=31
	)t3





-- Get ReportId from Report_Details

exec GetReportDetail @rundate=@rundate,@rootId=@rootId,@startDate=@startDate output,@enddate=@enddate output,@quarter=@quarter output,@reportId=@reportId output

-- Create the output as required
Drop table if exists #45_dataset
Create Table #45_dataset
(
	EMPI varchar(100),
	Provider_Id varchar(20),
	PCP_NPI varchar(20),
	PCP_NAME varchar(200),
	Practice_Name varchar(200),
	Specialty varchar(100),
	Measure_id varchar(20),
	Measure_Name varchar(100),
	Payer varchar(50),
	PayerId varchar(100),
	MEM_FNAME varchar(100),
	MEM_MName varchar(50),
	MEM_LNAME varchar(100),
	MEM_DOB Date,
	MEM_GENDER varchar(20),
	Enrollment_Status varchar(20),
	Last_visit_date Date,
	Product_Type varchar(50),
	Num bit,
	Den bit,
	Excl bit,
	Rexcl bit,
	CE bit,
	Epop bit,
	Report_Id INT,
	ReportType varchar(20),
	Report_Quarter varchar(20),
	Period_Start_Date Date,
	Period_End_Date Date,
	Root_Companies_Id INT,
	Code varchar(20),
	DateofService Date,
	DischargeDate Date,
	BillingProvider varchar(200),
	LastSeenDate Date

)
Insert into #45_dataset
select distinct
	d.EMPI
	,a.AmbulatoryPCPNPI as Provider_Id 
	,a.AmbulatoryPCPNPI as PCP_NPI
	,a.AmbulatoryPCPName as PCP_NAME
	,a.AmbulatoryPCPPractice as Practice_Name
	,a.AmbulatoryPCPSpecialty as Specialty
	,@measure_id as Measure_id
	,@measurename as Measure_Name
	,a.DATA_SOURCE as Payer
	,a.PayerId
	,a.MemberFirstName as MEM_FNAME
	,a.MemberMiddleName as MEM_MName
	,a.MemberLastName as MEM_LNAME
	,a.MemberDOB
	,m.Gender as MEM_GENDER
	,a.EnrollmentStatus
	,a.AmbulatoryPCPRecentVisit as Last_visit_date
	,mm.PAYER_TYPE as ProductType
	,Case
		when n.EMPI is not null Then 1
		else 0
	end as Num
	,1 as Den
	,0 as Excl
	,0 as Rexcl
	,Case
		when ce.EMPI is not null Then 1
		else 0
	end as CE
	,0 as Epop
	,@reportId
	,@reporttype as ReportType
	,@quarter as Report_quarter
	,@startDate as Period_start_date
	,@enddate as Period_end_date
	,@rootId
	,n.Code
	,n.DateOfService as DateofService
	,d.DIS_DATE
	,df.BillingProvider
	,df.FROM_DATE as LastSeenDate
from #45_directtransfers d
join RPT.PCP_ATTRIBUTION a on d.EMPI=a.EMPI and a.ReportId=@reportId
Join open_empi_master m on d.EMPI=m.EMPI_ID
left outer join MEMBER_MONTH mm on d.EMPI=mm.EMPI and mm.MEMBER_MONTH_START_DATE=@attributiondate
left outer join #45_numeratorset n on d.EMPI=n.EMPI and d.DIS_DATE=n.DIS_DATE
left outer join #45_contenroll ce on d.EMPI=ce.EMPI and d.DIS_DATE=ce.DIS_DATE
Left outer Join #45_dischargefacility df on d.EMPI=df.EMPI and d.DIS_DATE=df.DIS_DATE
where a.AssignedStatus='Assigned'


Update ds Set Rexcl=1 from #45_dataset ds join #45_hospicemembers h on ds.EMPI=h.EMPI

	


-- Insert data into Measure Detailed Line

	Delete from RPT.MEASURE_DETAILED_LINE where MEASURE_ID=@measure_id and REPORT_ID=@reportId and ROOT_COMPANIES_ID=@rootId;


Insert into RPT.MEASURE_DETAILED_LINE(Provider_Id,PCP_NPI,PCP_NAME,Practice_Name,Specialty,Measure_id,Measure_Name,Payer,PayerId,MEM_FNAME,MEM_MName,MEM_LNAME,MEM_DOB,MEM_GENDER,ENROLLMENT_STATUS,Last_visit_date,Product_Type,Num,Den,Excl,Rexcl,CE,Epop,Report_Id,ReportType,Report_Quarter,Period_Start_date,Period_end_Date,Root_Companies_id,EMPI,Measure_Type,Code,DateofService,DischargeDate,DischargeFacility,LastSeenDate)
Select Provider_Id,PCP_NPI,PCP_NAME,Practice_Name,Specialty,Measure_id,Measure_Name,Payer,PayerId,MEM_FNAME,MEM_MName,MEM_LNAME,MEM_DOB,MEM_GENDER,Enrollment_Status,Last_visit_date,Product_Type,Num,Den,Excl,Rexcl,CE,Epop,Report_Id,ReportType,Report_Quarter,Period_Start_date,Period_end_Date,Root_Companies_id,d.EMPI,@measuretype,code,DateofService,DischargeDate,BillingProvider,LastSeenDate
From #45_dataset d




-- Insert data into Provider Scorecard
Delete from RPT.PROVIDER_SCORECARD  where MEASURE_ID=@measure_id and REPORT_ID=@reportId and ROOT_COMPANIES_ID=@rootId;

Insert into RPT.PROVIDER_SCORECARD(Measure_id,Measure_Name,Measure_Title,MEASURE_SUBTITLE,Measure_Type,NUM_COUNT,DEN_COUNT,Excl_Count,Rexcl_Count,Gaps,Result,Target,To_Target,Report_Id,ReportType,Report_Quarter,Period_Start_Date,Period_End_Date,Root_Companies_Id)
Select 
	Measure_id
	,Measure_Name
	,Measure_Title
	,MEASURE_SUBTITLE
	,Measure_Type
	,SUM(Cast(NUM_COUNT as INT)) as NUM_COUNT
	,SUM(Cast(DEN_COUNT as INT)) as DEN_COUNT
	,SUM(Cast(Excl_Count as INT)) as Excl_Count
	,Sum(Cast(Rexcl_count as INT)) as Rexcl_Count
	,sum(Cast(DEN_Excl as INT)) - SUM(Cast(NUM_COUNT as INT)) as Gaps
	,Case
		when SUM(Cast(DEN_Excl as Float))>0 Then Round((SUM(cast(NUM_COUNT as Float))/ISNULL(NULLIF(SUM(Cast(DEN_Excl as Float)),0),1))*100,2)
		else 0
	end as Result
	,Target as Target
	,Case	
		when ((SUM(Cast(DEN_Excl as INT)))*(cast(Target*0.01 as float))) - SUM(Cast(NUM_COUNT as INT))>0 Then CEILING(((SUM(Cast(DEN_Excl as INT)))*(cast(Target*0.01 as float))) - SUM(Cast(NUM_COUNT as INT)))
		Else 0
	end as To_Target
	,Report_Id
	,ReportType
	,Report_Quarter
	,Period_Start_Date
	,Period_End_Date
	,Root_Companies_Id
From
(
	Select distinct
		m.EMPI
		,a.NPI as Provider_id
		,a.NPI as PCP_NPI
		,a.Prov_Name as PCP_Name
		,a.Specialty
		,a.Practice as Practice_Name
		,m.Measure_id
		,l.Measure_Name
		,l.measure_Title
		,l.Measure_SubTitle
		,'Calculated' as Measure_Type
		,Case
			when NUM=1 and excl=0 and rexcl=0 Then 1
			else 0
		end as NUM_COUNT
		,DEN as DEN_COUNT
		,Case
			When DEN=1 and Excl=0 and Rexcl=0 Then 1
			else 0
		End as Den_excl
		,Excl as Excl_Count
		,Rexcl as Rexcl_count
		,m.DischargeDate
		,m.code
		,m.DateofService
		,Report_Id
		,l.ReportType
		,Report_Quarter
		,Period_Start_Date
		,Period_End_Date
		,m.Root_Companies_Id
		,l.Target
	From RPT.MEASURE_DETAILED_LINE m
	Join RPT.ConsolidatedAttribution_Snapshot a on
		m.EMPI=a.EMPI and
		a.Attribution_Type='Ambulatory_PCP'
	Join RFT.UHN_measuresList l on
		m.Measure_Id=l.measure_id
	where Enrollment_Status='Active' and
		  a.NPI!='' and
		  m.MEASURE_ID=@measure_id and
		  REPORT_ID=@reportId 

)t1
Group by Measure_id,Measure_Name,Measure_Title,MEASURE_SUBTITLE,Report_Id,ReportType,Report_Quarter,Period_Start_Date,Period_End_Date,Root_Companies_Id,Measure_Type,Target





		SET @PERF_ROW = @@ROWCOUNT
		SET @PERF_DURATION = DATEDIFF(MINUTE,@PERF_START,GETDATE());
		EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, 'Enrollment Completed', @PERF_ROW, @DURATION_IN_MIN=@PERF_DURATION, @LOG2ID=@LOGID;
		EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, @LOG2ID=@LOGID, @END_FLAG=1;

END TRY
BEGIN CATCH
		BEGIN
			EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, 'FATAL ERROR', @LOG2ID=@LOGID;
			RAISERROR ('',16,0)
			RETURN 16
        END
	END CATCH
RETURN 0
END

GO
