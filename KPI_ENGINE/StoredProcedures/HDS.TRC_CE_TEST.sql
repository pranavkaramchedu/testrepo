SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
Create proc hds.TRC_CE_TEST
AS


drop table if exists #TRC_memlist; 

CREATE TABLE #TRC_memlist (
    memid varchar(100)
    
);


insert into #TRC_memlist
SELECT DISTINCT en.MemID FROM HDS.HEDIS_MEMBER_GM gm
join hds.HEDIS_MEMBER_EN en on en.MemID=gm.MemID and en.MEASURE_ID=gm.MEASURE_ID
WHERE en.MEASURE_ID='TRC_SAMPLE' and
CONVERT(DATE,en.StartDate)<='2020-12-31' AND CONVERT(DATE,FinishDate)>='2020-01-01'
AND YEAR('2020-12-31')-YEAR(DOB) >=18 ORDER BY 1;


	-- Inpatient Stay List
	drop table if exists #TRC_inpatientstaylist;
	CREATE table #trc_inpatientstaylist
	(
		Memid varchar(50),
		
		date_s varchar(8),
		date_disch varchar(8),
		claimid int
	);

Insert into #TRC_inpatientstaylist
select distinct MemID,Date_S,date_disch,claimid from HDS.HEDIS_VISIT v where v.Measure_id='TRC_SAMPLE' and v.HCFAPOS!=81 and suppdata='N' and v.date_disch!='' and CONVERT(DATE,v.date_disch) between '2020-01-01' and '2020-12-01' and v.REV in (select code from HDS.VALUESET_TO_CODE where code_system='UBREV' and Value_Set_Name in('Inpatient Stay'));
	
CREATE CLUSTERED INDEX ix_cdc_inpatientstaylist ON #TRC_inpatientstaylist ([memid],[date_s]);

----Readmission---------


drop table if exists #Readmission;
	CREATE table #Readmission
	(
		Memid varchar(50)
	);
Insert into #Readmission(Memid)
select distinct MemID from HDS.HEDIS_VISIT v where v.Measure_id='TRC_SAMPLE' and v.HCFAPOS!=81 and suppdata='N' and v.date_disch!='' and CONVERT(DATE,v.date_disch) >'2020-12-01' and v.REV in (select code from HDS.VALUESET_TO_CODE where code_system='UBREV' and Value_Set_Name in('Inpatient Stay'));


drop table if exists #TRC_inpatientstaylist_Final;
	CREATE table #TRC_inpatientstaylist_Final
	(
		Memid varchar(50)
	);
Insert into #TRC_inpatientstaylist_Final(Memid)
select distinct IP.Memid from #TRC_inpatientstaylist Ip
left join #Readmission R on IP.Memid=R.Memid
where  R.Memid is NULL ----AND ip.Memid='148480'





	drop table if exists #Denomerator;
	CREATE table #Denomerator
	(
		Memid varchar(50)
	);

Insert into #Denomerator
Select distinct M.memid from #TRC_memlist M
inner join (select distinct MemId from #TRC_inpatientstaylist_Final) IP on M.memid=Ip.Memid
----where M.memid='146792'
CREATE CLUSTERED INDEX ix_TRC_Denomeratort ON #Denomerator ([memid]);


-- Create Temp Patient Enrollment
drop table if exists #TRC_tmpsubscriber;

CREATE TABLE #TRC_tmpsubscriber (
    memid varchar(100),
    dob varchar(8),
	gender varchar(1),
	payer varchar(50),
	StartDate varchar(8),
	EndDate varchar(8),
	RN int
);
insert into #TRC_tmpsubscriber
SELECT en.MemID,gm.DOB,gm.Gender,en.Payer,en.StartDate,en.FinishDate, ROW_NUMBER() OVER(partition by en.MemID order by en.StartDate,en.FinishDate) as RN
FROM HDS.HEDIS_MEMBER_GM gm
join hds.HEDIS_MEMBER_EN en on en.MemID=gm.MemID and en.MEASURE_ID=gm.MEASURE_ID
Inner join #Denomerator D on gm.MemID=D.Memid
WHERE gm.MEASURE_ID='TRC_SAMPLE' and
CONVERT(DATE,en.StartDate)<='2020-12-31' AND CONVERT(DATE,FinishDate)>='2020-01-01'
AND YEAR('2020-12-31')-YEAR(DOB) >=18 ORDER BY en.MemID,CONVERT(DATE,en.StartDate),CONVERT(DATE,FinishDate);

CREATE CLUSTERED INDEX ix_tmpsub_memid ON #TRC_tmpsubscriber ([memid],[StartDate],[EndDate]);




-- Inpatient Stay List
	drop table if exists #TRC_DISCHARGE_MEMBERS;
	CREATE table #TRC_DISCHARGE_MEMBERS
	(
		Memid varchar(50),
		date_s varchar(8),
		date_disch varchar(8),
		claimid int
	);

Insert into #TRC_DISCHARGE_MEMBERS
select distinct v.MemID,Date_S,date_disch,claimid from HDS.HEDIS_VISIT v 
Inner join #TRC_tmpsubscriber S on v.MemID=S.memid and convert(date,v.Date_Disch) between convert(date, S.StartDate) and convert(date,S.EndDate)
where v.Measure_id='TRC_SAMPLE' and v.HCFAPOS!=81 and suppdata='N' and v.date_disch!='' and CONVERT(DATE,v.date_disch) between '2020-01-01' and '2020-12-01' and v.REV in (select code from HDS.VALUESET_TO_CODE where code_system='UBREV' and Value_Set_Name in('Inpatient Stay'))
AND v.MEMID IN (Select MemID from #Denomerator)




drop table if exists #CE;
	CREATE table #CE
	(
		Memid varchar(50),
		claimid varchar(50)
	);
Insert into #CE(Memid,claimid)
select distinct E.memid,C.claimid from #TRC_tmpsubscriber E
inner join #TRC_DISCHARGE_MEMBERS C
on E.memid=C.Memid and CONVERT(DATE,E.EndDate)>=DATEADD(d,30,CONVERT(DATE,C.date_disch)) AND
(CONVERT(DATE,C.date_disch) BETWEEN CONVERT(DATE,E.StartDate) and CONVERT(DATE,E.EndDate))
---where E.memid='102880'
Union
select distinct E.memid,C.claimid from #TRC_tmpsubscriber E
left join #TRC_tmpsubscriber E1 on E.Memid=E1.Memid and E1.RN=E.RN+1
inner join #TRC_DISCHARGE_MEMBERS C---where memid='102855'
on E.memid=C.Memid AND ((CONVERT(DATE,C.date_disch) BETWEEN CONVERT(DATE,E.StartDate) and CONVERT(DATE,E.EndDate)) 
AND CONVERT(DATE,E1.EndDate)>=DATEADD(d,30,CONVERT(DATE,C.date_disch))
AND DATEDIFF(d,CONVERT(DATE,E.EndDate),CONVERT(DATE,E1.StartDate)) in (0,1))



drop table if exists #Ignore_CE;
	CREATE table #Ignore_CE
	(
		Memid varchar(50)
	);
Insert into #Ignore_CE(Memid)
select distinct D.MemId 
from #TRC_DISCHARGE_MEMBERS D
left join #CE CE on D.Memid=CE.Memid and D.claimid=CE.claimid
where CE.Memid is null 



drop table if exists #CE_FINAL;
	CREATE table #CE_FINAL
	(
		Memid varchar(50),
	);
Insert into #CE_FINAL(Memid)
select distinct CE.Memid from #CE CE 
left join #Ignore_CE D
on CE.Memid=D.Memid 
where D.Memid is NULL
/*
union
select distinct E.memid from #TRC_tmpsubscriber E
Inner join (select MemID,sdate,edate from HDS.HEDIS_VISIT_E where MEASURE_ID='TRC_SAMPLE' and sdate!='' and convert(date,sdate) between '2020-01-01' AND '2020-12-31') V on E.memid=V.MemID and convert(date,V.sdate) Between convert(date,E.StartDate) and convert(date,E.EndDate) and  convert(date,E.EndDate) >=DATEADD(d,30,CONVERT(DATE,V.edate))
*/



---select * into #TRCdataset from hds.TRCdataset
update #TRCdataset set CE=0 
update #TRCdataset set CE=1 from #TRCdataset ds join #CE_FINAL CE on CE.memid=ds.patient_id;



select * from #TRCdataset E
left join hds.HEDIS_score S 
on E.patient_id=S.MemID 
where measure_id='TRC_SAMPLE' and E.CE<>S.CE AND S.CE=1







select top 100 * from #TRC_tmpsubscriber E where memid='102854'
select top 100 * from #TRC_DISCHARGE_MEMBERS C where memid='102854'
select top 100 * from #CE C where memid='102854' 
select top 100 * from #CE_FINAL C where memid='102854'
select * from #TRCdataset where patient_id='102854' 
select top 100 * from hds.HEDIS_SCORE E where memid='102957' and MEASURE_ID='TRC_SAMPLE'




select top 100 * from #TRC_tmpsubscriber E where memid='102892'
select top 100 * from #TRC_DISCHARGE_MEMBERS C where memid='102892'
select top 100 * from hds.HEDIS_VISIT_E C where memid='102892'






GO
