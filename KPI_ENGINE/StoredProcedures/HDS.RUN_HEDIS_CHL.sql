SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
/****** Object:  StoredProcedure [HDS].[CHL]    Script Date: 15-12-2020 08:33:46 ******/
--SET ANSI_NULLS ON
--GO
--SET QUOTED_IDENTIFIER ON
--GO

CREATE proc [HDS].[RUN_HEDIS_CHL]
AS

Drop table if exists #CHLdataset;

CREATE TABLE #CHLdataset (
  [patient_id] varchar(100) NOT NULL,
  [meas] varchar(20) DEFAULT NULL,
  [payer] varchar(100) DEFAULT NULL,
  [patient_gender] varchar(45) NOT NULL,
  [age] decimal(12,4) DEFAULT NULL,
  [orec] smallint DEFAULT NULL,
  [lis] smallint DEFAULT '0',
  [rexcl] smallint DEFAULT '0',
  [rexcld] smallint DEFAULT '0',
  [CE] smallint DEFAULT '0',
  [excl] smallint DEFAULT '0',
  [num] smallint DEFAULT '0',
  [Event] smallint DEFAULT 0,
) ;

-- Eligible Patient List
CREATE INDEX [idx1] ON #CHLdataset ([payer]);
CREATE INDEX [patient_id_UNIQUE] ON #CHLdataset ([patient_id],[payer]);

drop table if exists #CHL_memlist; 

CREATE TABLE #CHL_memlist (
    memid varchar(100)    
);

insert into #CHL_memlist
SELECT DISTINCT en.MemID FROM HDS.HEDIS_MEMBER_GM gm
join hds.HEDIS_MEMBER_EN en on en.MemID=gm.MemID and en.MEASURE_ID=gm.MEASURE_ID
WHERE en.MEASURE_ID='CHL_TEST' and Gender='F' 
and convert(date,en.StartDate)<='2020-12-31' AND convert(date,FinishDate)>='2020-01-01'
AND YEAR('2020-12-31')-YEAR(DOB) BETWEEN 16 AND 24

CREATE CLUSTERED INDEX ix_memlist_memid ON #CHL_memlist ([memid]);

---select count(memid) from #CHL_memlist
---select count(distinct memid) from hds.HEDIS_score where measure_id='CHL_TEST'


/*
select * from #CHL_memlist A
left join
(select distinct memid as Memid from hds.HEDIS_score where measure_id='CHL_TEST') B
on A.memid=B.MemID 
where B.Memid is null
------------------------------------------------------------------------
SELECT 2020-year(dob) age,* FROM HDS.HEDIS_MEMBER_GM gm
join hds.HEDIS_MEMBER_EN en on en.MemID=gm.MemID and en.MEASURE_ID=gm.MEASURE_ID
WHERE en.MEASURE_ID='CHL_TEST' and gm.MemID in 
(select A.memid from #CHL_memlist A
left join
(select distinct memid as Memid from hds.HEDIS_score where measure_id='CHL_TEST') B
on A.memid=B.MemID 
where B.Memid is null
)
*/

-- Create Temp Patient Enrollment
drop table if exists #CHL_tmpsubscriber;

CREATE TABLE #CHL_tmpsubscriber (
    memid varchar(100),
    dob varchar(8),
	gender varchar(1),
	payer varchar(50),
	StartDate varchar(8),
	EndDate varchar(8),
);

CREATE CLUSTERED INDEX ix_tmpsub_memid ON #CHL_tmpsubscriber ([memid],[StartDate],[EndDate]);

insert into #CHL_tmpsubscriber
SELECT en.MemID,gm.DOB,gm.Gender,en.Payer,en.StartDate,en.FinishDate  FROM HDS.HEDIS_MEMBER_GM gm
join hds.HEDIS_MEMBER_EN en on en.MemID=gm.MemID and en.MEASURE_ID=gm.MEASURE_ID
WHERE en.MEASURE_ID='CHL_TEST' and gm.Gender='F' and
convert(date,en.StartDate)<='2020-12-31' AND convert(date,FinishDate)>='2020-01-01'
AND YEAR('2020-12-31')-YEAR(DOB) BETWEEN 16 AND 24


Declare @pt_ctr INT;
Declare @i INT = 0;
Declare @patientid varchar(100);
Declare @gender varchar(1);
Declare @age int;
Declare @latest_insenddate varchar(10);
Declare @plan_ct INT=0;
Declare @planid varchar(20);
Declare @meas varchar(10)='CHL';
Declare @plan1 varchar(20);
Declare @plan2 varchar(20);

-- Get Patient COUNT
SELECT @pt_ctr=COUNT(*)  FROM #CHL_memlist;

PRINT 'pt_ctr:' + Cast(@pt_ctr as nvarchar);

-- While LOOP

-- Loop through enrollment for each patient

While @i<@pt_ctr
	BEGIN
		-- Get Patient to Loop
		SELECT  @patientid=memid FROM #CHL_memlist ORDER BY memid ASC OFFSET  @i ROWS FETCH NEXT 1 ROWS ONLY ;
		
		-- ReadingGender and age
		SELECT Top 1 @gender=gender,@age=YEAR('2020-12-31')-YEAR(DOB) FROM #CHL_tmpsubscriber WHERE memid=@patientid;
		
		-- Check for dual eligibility by checking if the patient is enrolled in more than one plan at the end of Enrollment
		
		SELECT Top 1 @latest_insenddate=convert(date,EndDate) FROM #CHL_tmpsubscriber WHERE  memid=@patientid  AND convert(date,StartDate)<='2020-12-31' ORDER BY convert(date,StartDate) DESC,convert(date,EndDate) Desc  ;
		-----SELECT Top 1 convert(date,EndDate) FROM #CHL_tmpsubscriber WHERE  memid='100001'  AND convert(date,StartDate)<='2020-12-31' ORDER BY convert(date,StartDate) DESC,convert(date,EndDate) Desc  ;
		-- Check for plan count patient is enrolled in during the end of measurement year
		
		SELECT @plan_ct=COUNT(*) FROM #chl_tmpsubscriber WHERE  memid=@patientid AND convert(date,@latest_insenddate) BETWEEN convert(date,StartDate) AND convert(date,EndDate);
		-----SELECT COUNT(*) FROM #chl_tmpsubscriber WHERE  memid='100001' AND convert(date,'2021-12-31') BETWEEN convert(date,StartDate) AND convert(date,EndDate);
		-- If only one plan, then skip the Payer reporting logic
		
		if(@plan_ct=1)
		BEGIN
			
			SELECT @planid=payer FROM #chl_tmpsubscriber WHERE  memid=@patientid AND convert(date,@latest_insenddate) BETWEEN convert(date,StartDate) AND convert(date,EndDate) ORDER BY payer;
		
			-- If plan is Dual , Insert MCR & MCD	
			
			If(@planid in('MMP','SN3','MDE'))
			BEGIN
				IF(@planid in('MMP','SN3'))
				Begin
					------Insert INTO #CHLdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);   ----Commented
				Print 'Q'
				END
				
				SET @planid='MCD';
				Insert INTO #CHLdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
				--Print 'W'
				SET @planid='MCR';
				------Insert INTO #CHLdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);     ------Commented
				Print 'E'
			END
			
			Else if(@planid IN('MD','MLI','MRB'))
			Begin
				SET @planid='MCD';
				Insert INTO #CHLdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
				Print 'R'
			END
			
			Else If(@planid IN('SN1','SN2'))
			Begin
				---Insert INTO #CHLdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);----------Commented
				Print 'T'
				SET @planid='MCR';
				Insert INTO #CHLdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
				Print 'Y'
			END
			ELSE
			BEGIN
				Insert INTO #CHLdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
				Print 'Z'
			END
		END
				
		-- Compare Plans if Plan count is more than one	
		if(@plan_ct>1)
		BEGIN
			
			-- Read First Plan
			SELECT  @plan1=payer FROM #CHL_tmpsubscriber where memid=@patientid and convert(date,@latest_insenddate) between convert(date,StartDate) AND convert(date,EndDate) order by 1 OFFSET  0 ROWS FETCH NEXT 1 ROWS ONLY ;
					
			-- Read second plan
			SELECT  @plan2=payer FROM #chl_tmpsubscriber where memid=@patientid and convert(date,@latest_insenddate) between convert(date,StartDate) AND convert(date,EndDate) order by 1 OFFSET  1 ROWS FETCH NEXT 1 ROWS ONLY ;
			
			
			
			If(@plan1 IN('MCR','MP','MC','SN1','SN2','MCS') and @plan2 IN('PPO','POS','HMO','CEP'))
			BEGIN
				if(@plan1 IN('SN1','SN2'))
				BEGIN
					SET @planid=@plan1;
					Insert INTO #CHLdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
					Print 'A'
					SET @planid='MCR';
					Insert INTO #CHLdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
					Print 'B'
				END
				
				if(@plan1 in ('MCR','MP','MC','MCS'))
				Begin
					
					Set @planid=@plan1;
					Insert INTO #CHLdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
				
				END

			END
			
			Else If(@plan2 IN('MCR','MP','MC','SN1','SN2','MCS') AND @plan1 IN('PPO','POS','HMO','CEP'))
			BEGIN
			
				IF(@plan2 IN('SN1','SN2'))
				Begin
					
					Set @planid=@plan2;
					Insert INTO #CHLdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
					Print 'C'
					Set @planid='MCR';
					Insert INTO #CHLdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
					Print 'D'
				END
				
				
				IF(@plan2 IN('MCR','MP','MC','MCS'))
				BEGIN
					
					Set @planid=@plan2;
					
					Insert INTO #CHLdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
					Print 'E'
				END
			END
			
			ELSE IF(@plan1 IN('PPO','POS','HMO','CEP') AND @plan2 IN('MD','MLI','MRB'))
			BEGIN
			
				Set @planid=@plan1;
				Insert INTO #CHLdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
				PRINT 'F'	
			END
			
			ELSE IF(@plan2 IN('PPO','POS','HMO','CEP') AND @plan1 IN('MD','MLI','MRB'))
			BEGIN
			
				Set @planid=@plan2;
				Insert INTO #CHLdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
				PRINT 'G'	
			END
			
			ELSE IF(@plan1 IN('MDE','SN3','MMP') or @plan2 in('MDE','SN3','MMP'))
			BEGIN
			
				if(@plan1 IN('SN3','MMP'))
				Begin
					Set @planid=@plan1;
					Insert INTO #CHLdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
				PRINT 'H'
				End
				
				if(@plan2 IN('SN3','MMP'))
				Begin
					Set @planid=@plan2
					----Insert INTO #CHLdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);  Commented and gm.MemID='138201'
				END
				
					
				Set @planid='MCR';
				---Insert INTO #CHLdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);  -----Commented
				PRINT 'I'
				Set @planid='MCD';
				Insert INTO #CHLdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
				PRINT 'J'
			END
			
			ELSE
			BEGIN
			
				Set @planid=@plan1;
				
				Insert INTO #CHLdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
				Print '1'
				Set @planid=@plan2;
				Insert INTO #CHLdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
				Print '2'
			END
			
		END
		
		Set @i=@i+1;		
END;	

-- Continuous Enrollment
				
	DROP TABLE IF EXISTS #chl_contenroll;
	
	CREATE table #chl_contenroll
	(
		Memid varchar(50),
		CE INT
	);

	CREATE CLUSTERED INDEX ix_chl_contenroll ON #chl_contenroll ([memid]);

	
	Insert into #chl_contenroll
	SELECT MemID,SUM(ceflag) AS CE FROM
	(
	
		-- CE Check for 1st year
		SELECT MemID,SUM(coverage) AS coverage, 1 AS ceflag FROM
		(
			SELECT MemID,Payer,start_date,end_date,DATEDIFF(day,start_date,end_date)+1 AS coverage FROM
			(

				SELECT s.MemID,s.Payer,s.FinishDate,
				CASE
				WHEN Convert(date,StartDate)<'2020-01-01' THEN '2020-01-01'
				ELSE Convert(date,StartDate)
				END AS start_date,
				CASE
				WHEN Convert(date,FinishDate)>'2020-12-31' THEN '2020-12-31'
				ELSE Convert(date,FinishDate)
				END AS end_date
				FROM HDS.HEDIS_MEMBER_EN s
				JOIN 
				(
					SELECT MemID,SUM(diff) as diff FROM
					(
						SELECT MemID,nextstartdate,FinishDate,
						CASE
						WHEN DATEDIFF(day,FinishDate,nextstartdate)<=1 THEN 0
						ELSE 1
						END AS diff
						  FROM
						  (
							SELECT MemID,StartDate,FinishDate,
							CASE
							WHEN nextstartdate IS NULL THEN FinishDate
							ELSE nextstartdate
							END AS nextstartdate FROM
							(
								SELECT s.MemID,Convert(date,s.StartDate) as StartDate,Convert(date,s.FinishDate) as FinishDate,
								(
									SELECT Top 1 s1.StartDate
									FROM HDS.HEDIS_MEMBER_EN s1
									WHERE s1.MEASURE_ID=s.MEASURE_ID AND s1.MemID=s.MemID AND s.FinishDate<s1.StartDate AND Convert(date,s1.StartDate)<='2020-12-31' AND Convert(date,s1.FinishDate)>='2020-01-01'
									ORDER BY s1.MemID,s1.StartDate,s1.FinishDate
								) AS nextstartdate

								 FROM HDS.HEDIS_MEMBER_EN s
								WHERE MEASURE_ID='CHL_TEST' AND Convert(date,s.StartDate)<='2020-12-31' AND Convert(date,s.FinishDate)>='2020-01-01'
							)t
						)t1 
					)t2 GROUP BY MemID HAVING SUM(diff)<=1
				)
				s1 ON s.MemID=s1.MemID
				WHERE s.MEASURE_ID='CHL_TEST'
				-- SQLINES DEMO *** 100001
				AND Convert(date,s.StartDate)<='2020-12-31' AND Convert(date,s.FinishDate)>='2020-01-01'
	
			)t2
		)t3 GROUP BY MemID HAVING sum(coverage)>=(DATEPART(dy, '2020-12-31')-45)

		Union All

		-- Anchor Date
	 
		SELECT MemID,SUM(DATEDIFF(day,start_date,end_date)) AS coverage, 1 AS ceflag FROM
			(
			SELECT MemID,FinishDate,
			CASE
				WHEN Convert(date,StartDate)<'2020-01-01' THEN '2020-01-01'
				ELSE Convert(date,StartDate)
			END AS start_date,
			CASE
				WHEN Convert(date,FinishDate)>'2020-12-31' THEN '2020-12-31'
				ELSE Convert(date,FinishDate)
			END AS end_date
			FROM HDS.HEDIS_MEMBER_EN 
			WHERE MEASURE_ID='CHL_TEST'
			-- SQLINES DEMO *** 0000          
			AND '2020-12-31' BETWEEN Convert(date,StartDate) AND Convert(date,FinishDate)
		
		)t4  GROUP BY MemID
 
	)cont_enroll GROUP BY MemID  HAVING SUM(ceflag)=2 order by MemID;
	
	update #chldataset set CE=1 from #chldataset ds join #chl_contenroll ce on ds.patient_id=ce.MemID;


-----Event Method 1:Claim/Encounter Data
	exec dbo.sp_kpi_droptable '#Event'
	CREATE table #Event
	(
		Memid varchar(50)
			
	);
	
	insert into #Event
	select distinct MemID from HDS.HEDIS_VISIT v
	where MEASURE_ID='CHL_TEST' and convert(date,Date_S) between '2020-01-01' and '2020-12-31' and Date_S!='' and HCFAPOS<>'81' 
	and SuppData='N' 
	and (
	Diag_I_1 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in ('Pregnancy','Sexual Activity'))
	or
	Diag_I_2 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in ('Pregnancy','Sexual Activity'))
	or
	Diag_I_3 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in ('Pregnancy','Sexual Activity'))
	or
	Diag_I_4 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in ('Pregnancy','Sexual Activity'))
	or
	Diag_I_5 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in ('Pregnancy','Sexual Activity'))
	or
	Diag_I_6 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in ('Pregnancy','Sexual Activity'))
	or
	Diag_I_7 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in ('Pregnancy','Sexual Activity'))
	or
	Diag_I_8 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in ('Pregnancy','Sexual Activity'))
	or
	Diag_I_9 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in ('Pregnancy','Sexual Activity'))
	or
	Diag_I_10 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in ('Pregnancy','Sexual Activity'))
	)
Union
	select distinct MemID from HDS.HEDIS_VISIT v
	where MEASURE_ID='CHL_TEST' and convert(date,Date_S) between '2020-01-01' and '2020-12-31' and Date_S!=''
	and SuppData='N' 
 AND( 
	Diag_I_1 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in ('Sexual Activity'))
	OR
	Diag_I_2 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in ('Sexual Activity'))
	OR
	Diag_I_3 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in ('Sexual Activity'))
	OR
	Diag_I_4 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in ('Sexual Activity'))
	OR
	Diag_I_5 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in ('Sexual Activity'))
	OR
	Diag_I_6 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in ('Sexual Activity'))
	OR
	Diag_I_7 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in ('Sexual Activity'))
	OR
	Diag_I_8 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in ('Sexual Activity'))
	OR
	Diag_I_9 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in ('Sexual Activity'))
	OR
	Diag_I_10 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in ('Sexual Activity'))
	OR
	HCPCS in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in ('Sexual Activity'))
	or 
	CPT in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in ('Sexual Activity','Pregnancy Tests'))
	or
	CPT2 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in ('Sexual Activity','Pregnancy Tests'))
	)
----Union
----select distinct Memid from HDS.HEDIS_OBS OBS where Measure_id='CHL_TEST' and Convert(date,OBS.date) between '2020-01-01' and '2020-12-31' and date !='' and OCode IN(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in ('Pregnancy','Sexual Activity','Pregnancy Tests'))
---union 
---select distinct memid from HDS.HEDIS_DIAG where MEASURE_ID='CHL_TEST' and convert(date,sdate) between '2020-01-01' and '2020-12-31' and sdate!='' and dcode in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Pregnancy','Sexual Activity'))
--union 
--select distinct memid from HDS.HEDIS_PROC where MEASURE_ID='CHL_TEST' and pstatus='EVN' and convert(date,sdate) <='2020-12-31' and sdate!='' and pcode in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in ('Pregnancy','Sexual Activity','Pregnancy Tests'))
---Union 
---select distinct Memid from hds.HEDIS_LAB L where Date_S!='' and convert(date,Date_S) between '2020-01-01' and '2020-12-31' and MEASURE_ID='CHL_TEST' AND CPT_Code in (select code from HDS.VALUESET_TO_CODE where Value_Set_Name in ('Pregnancy Tests'))
-----Event Method 2:Pharmacy Data
union
select distinct Memid from HDS.HEDIS_PHARM where Measure_id='CHL_TEST'  and suppdata='N' and PrServDate!='' and  convert(date,PrServDate) between '2020-01-01' and '2020-12-31' and NDC in(select code from HDS.MEDICATION_LIST_TO_CODES where Medication_List_Name='Contraceptive Medications')
--union 
--Select distinct Memid from hds.HEDIS_PHARM_C where MEASURE_ID = 'CHL_TEST' and convert(date,DDate) between  '2020-01-01' and '2020-12-31' and RXnorm in(select code from HDS.MEDICATION_LIST_TO_CODES where Medication_List_Name='Contraceptive Medications')

CREATE CLUSTERED INDEX idx_EVENT ON #Event ([memid]);

update #chldataset set [Event]=1 from #chldataset ds join #Event E on ds.patient_id=E.MemID;


--select * from #chldataset E
--left join hds.HEDIS_score S 
--on E.patient_id=S.MemID 
--where measure_id='CHL_TEST' and E.Event<>S.Event 



-- Hospice Exclusion

	drop table if exists #bcs_hospicemembers;
	CREATE table #bcs_hospicemembers
	(
		Memid varchar(50)
		
	);

	

	
	drop table if exists #hospicemembers;
	CREATE table #hospicemembers
	(
		Memid varchar(50)
		
	);

	Insert into #hospicemembers
	select distinct t1.MemID from
	(
	select Memid from HDS.HEDIS_OBS where Measure_id='CHL_TEST' and date !='' and convert(date,[date]) between '2020-01-01' and '2020-12-31' and OCode IN(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Hospice Encounter','Hospice Intervention'))
	Union all
	select MemID from HDS.HEDIS_VISIT v
	where MEASURE_ID='CHL_TEST' and HCFAPOS!=81 and Date_S!='' and Convert(date,Date_S) between '2020-01-01' and '2020-12-31'
	and (
	Diag_I_1 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or
	Diag_I_2 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or
	Diag_I_3 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or
	Diag_I_4 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or
	Diag_I_5 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or
	Diag_I_6 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or
	Diag_I_7 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or
	Diag_I_8 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or
	Diag_I_9 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or
	Diag_I_10 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or
	CPT in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or
	CPT2 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or
	HCPCS in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or 
	Proc_I_1 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or 
	Proc_I_2 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or 
	Proc_I_3 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or 
	Proc_I_4 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or 
	Proc_I_5 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or 
	Proc_I_6 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or
	Rev in(select code from HDS.VALUESET_TO_CODE where code_system='UBREV' and Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	)
	)t1
	
	CREATE CLUSTERED INDEX idx_hospicemembers ON #hospicemembers ([memid]);
	update #CHLdataset set rexcl=1 from #CHLdataset ds join #hospicemembers hos on hos.memid=ds.patient_id;


--select * from #chldataset E
--left join hds.HEDIS_score S 
--on E.patient_id=S.MemID 
--where measure_id='CHL_TEST' and E.rexcl<>S.rexcl





drop table if exists #num;
	CREATE table #num
	(
		Memid varchar(50)
		
	);

	Insert into #num
	select distinct Memid from(
	select distinct memid from HDS.HEDIS_PROC where MEASURE_ID='CHL_TEST' and pstatus='EVN' and convert(date,sdate) between '2020-01-01' and '2020-12-31' and sdate!='' and pcode in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Chlamydia Tests'))
	Union all
	select distinct Memid from hds.HEDIS_LAB L where Date_S!='' and convert(date,Date_S) between '2020-01-01' and '2020-12-31' and MEASURE_ID='CHL_TEST' AND (CPT_Code in (select code from HDS.VALUESET_TO_CODE where Value_Set_Name in ('Chlamydia Tests')) OR LOINC in  (select code from HDS.VALUESET_TO_CODE where Value_Set_Name in ('Chlamydia Tests')))
	Union all
	select distinct MemID from HDS.HEDIS_VISIT v
	where MEASURE_ID='CHL_TEST' and Date_S!='' and convert(date,Date_S) between '2020-01-01' AND '2020-12-31'
	and (
	CPT in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in ('Chlamydia Tests'))
	or
	HCPCS in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in ('Chlamydia Tests'))
	or 
	Proc_I_1 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in ('Chlamydia Tests'))
	or 
	Proc_I_2 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in ('Chlamydia Tests'))
	or 
	Proc_I_3 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in ('Chlamydia Tests'))
	or 
	Proc_I_4 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in ('Chlamydia Tests'))
	or 
	Proc_I_5 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in ('Chlamydia Tests'))
	or 
	Proc_I_6 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in ('Chlamydia Tests'))
	)

)t1
Union 
select distinct MemID from HDS.HEDIS_OBS v where Measure_ID='CHL_TEST' AND ocode in (select code from HDS.VALUESET_TO_CODE where Value_Set_Name in ('Chlamydia Tests')) and convert(date,V.date) between '2020-01-01' and '2020-12-31'


CREATE CLUSTERED INDEX idx_num ON #num ([memid]);
	
update #CHLdataset set num=1 from #CHLdataset ds join #num num on num.Memid=ds.patient_id ;
	



--select E.Patient_Id,E.NUM,S.NUM from #chldataset E
--left join hds.HEDIS_score S 
--on E.patient_id=S.MemID 
--where measure_id='CHL_TEST' and E.NUM<>S.NUM


exec dbo.sp_kpi_droptable '#Pregnancy_Tests_Members'
CREATE table #Pregnancy_Tests_Members
	(
		Memid varchar(50)
			
	);
Insert into #Pregnancy_Tests_Members
select distinct MemID from HDS.HEDIS_VISIT v
	where MEASURE_ID='CHL_TEST' and convert(date,Date_S) between '2020-01-01' and '2020-12-31' and Date_S!='' and SuppData='N'
	and (
	CPT in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in  ('Pregnancy Tests'))
	)




-----COMPLUSORY2
exec dbo.sp_kpi_droptable '#Pregnancy_Tests_Exclusion'
CREATE table #Pregnancy_Tests_Exclusion
	(
		Memid varchar(50),
		Date_S  date

			
	);
Insert into #Pregnancy_Tests_Exclusion(Memid,Date_S)
select distinct MemID,convert(date,Date_S) as Date_S from HDS.HEDIS_VISIT v
	where MEASURE_ID='CHL_TEST' and convert(date,Date_S) between '2020-01-01' and '2020-12-31' and Date_S!=''
	and (
	CPT in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in  ('Pregnancy Test Exclusion'))
	) 
Union
select distinct Memid ,Convert(date,OBS.date) as DATE_S from HDS.HEDIS_OBS OBS where Measure_id='CHL_TEST' and Convert(date,OBS.date) between '2020-01-01' and '2020-12-31' and date !='' and OCode IN(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in ('Pregnancy Test Exclusion')) 
Union 
select distinct Memid,convert(date,Date_S) as DATE_S from hds.HEDIS_LAB L where Date_S!='' and convert(date,Date_S) between '2020-01-01' and '2020-12-31' and MEASURE_ID='CHL_TEST' AND (CPT_Code in (select code from HDS.VALUESET_TO_CODE where Value_Set_Name in ('Pregnancy Test Exclusion')) OR LOINC in (select code from HDS.VALUESET_TO_CODE where Value_Set_Name in ('Pregnancy Test Exclusion')))





-----BIT 1.2
exec dbo.sp_kpi_droptable '#Medication_XRAY'
CREATE table #Medication_XRAY
	(
		Memid varchar(50),
		DATE_S varchar(100)
			
	);
Insert into #Medication_XRAY
select distinct Memid,PrServDate AS DATE_S from HDS.HEDIS_PHARM where Measure_id='CHL_TEST' and PrServDate!='' and  convert(date,PrServDate) between '2020-01-01' and DATEADD(d,6,'2020-12-31') and NDC in(select code from HDS.MEDICATION_LIST_TO_CODES where Medication_List_Name='Retinoid Medications')
UNION
select MemID,convert(date,DDate) AS DATE_S from hds.HEDIS_pharm_c where measure_id='CHL_TEST' and convert(date,DDate) BETWEEN '2020-01-01' AND DATEADD(d,6,'2020-12-31') AND RxNorm in (select code from HDS.MEDICATION_LIST_TO_CODES where Medication_List_Name='Retinoid Medications')
Union
select distinct MemID,convert(date,Date_S) as Date_S from HDS.HEDIS_VISIT v
	where MEASURE_ID='CHL_TEST' and convert(date,Date_S) between '2020-01-01' and DATEADD(d,6,'2020-12-31') and Date_S!='' AND HCFAPOS<>'81'
	and (
	CPT in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in  ('Diagnostic Radiology'))
	) 
Union
select distinct Memid ,Convert(date,OBS.date) as DATE_S from HDS.HEDIS_OBS OBS where Measure_id='CHL_TEST' and Convert(date,OBS.date) between '2020-01-01' and DATEADD(d,6,'2020-12-31') and date !='' and OCode IN(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in ('Diagnostic Radiology'))
Union 
select distinct Memid,convert(date,Date_S) as DATE_S from hds.HEDIS_LAB L where Date_S!='' and convert(date,Date_S) between '2020-01-01' and DATEADD(d,6,'2020-12-31') and MEASURE_ID='CHL_TEST' AND CPT_Code in (select code from HDS.VALUESET_TO_CODE where Value_Set_Name in ('Diagnostic Radiology')) 
UNION
select distinct MemID, convert(date,sdate) as DATE_S from hds.hedis_proc where measure_id='CHL_TEST' and convert(date,sdate) between '2020-01-01' and DATEADD(d,6,'2020-12-31') and pstatus='EVN' and pcode in (select code from HDS.VALUESET_TO_CODE where Value_Set_Name in  ('Diagnostic Radiology'))




exec dbo.sp_kpi_droptable '#Exclusion_Window'
CREATE table #Exclusion_Window
	(
		Memid varchar(50),
			
	);
insert into #Exclusion_Window
Select Distinct A.Memid from #Pregnancy_Tests_Exclusion A
Inner join #Medication_XRAY B on A.Memid=B.Memid and  (convert(date,B.Date_S) BETWEEN convert(date,A.Date_S) AND dateadd(d,6,convert(date,A.Date_S)))




------EXcl_Preg_Test for optional Exclusion
	exec dbo.sp_kpi_droptable '#Preg_sexualActivity_Rx_Contraceptive'
	CREATE table #Preg_sexualActivity_Rx_Contraceptive
	(
		Memid varchar(50)
			
	);
	
	insert into #Preg_sexualActivity_Rx_Contraceptive
	select distinct MemID from HDS.HEDIS_VISIT v
	where MEASURE_ID='CHL_TEST' and convert(date,Date_S) between '2020-01-01' and '2020-12-31' and Date_S!='' AND SuppData='N'
	and (
	Diag_I_1 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in ('Pregnancy','Sexual Activity'))
	or
	Diag_I_2 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in ('Pregnancy','Sexual Activity'))
	or
	Diag_I_3 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in ('Pregnancy','Sexual Activity'))
	or
	Diag_I_4 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in ('Pregnancy','Sexual Activity'))
	or
	Diag_I_5 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in ('Pregnancy','Sexual Activity'))
	or
	Diag_I_6 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in ('Pregnancy','Sexual Activity'))
	or
	Diag_I_7 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in ('Pregnancy','Sexual Activity'))
	or
	Diag_I_8 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in ('Pregnancy','Sexual Activity'))
	or
	Diag_I_9 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in ('Pregnancy','Sexual Activity'))
	or
	Diag_I_10 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in ('Pregnancy','Sexual Activity'))
	)
Union
	select distinct MemID from HDS.HEDIS_VISIT v
	where MEASURE_ID='CHL_TEST' and convert(date,Date_S) between '2020-01-01' and '2020-12-31' and Date_S!='' AND SuppData='N'
 AND( 
	Diag_I_1 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in ('Sexual Activity'))
	OR
	Diag_I_2 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in ('Sexual Activity'))
	OR
	Diag_I_3 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in ('Sexual Activity'))
	OR
	Diag_I_4 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in ('Sexual Activity'))
	OR
	Diag_I_5 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in ('Sexual Activity'))
	OR
	Diag_I_6 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in ('Sexual Activity'))
	OR
	Diag_I_7 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in ('Sexual Activity'))
	OR
	Diag_I_8 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in ('Sexual Activity'))
	OR
	Diag_I_9 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in ('Sexual Activity'))
	OR
	Diag_I_10 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in ('Sexual Activity'))
	OR
	HCPCS in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in ('Sexual Activity'))
	or 
	CPT in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in ('Sexual Activity'))
	or
	CPT2 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in ('Sexual Activity'))
	)
union
select distinct Memid from HDS.HEDIS_PHARM where Measure_id='CHL_TEST'  AND SuppData='N' and PrServDate!='' and  convert(date,PrServDate) between '2020-01-01' and '2020-12-31' and NDC in(select code from HDS.MEDICATION_LIST_TO_CODES where Medication_List_Name='Contraceptive Medications')




-----CREATE CLUSTERED INDEX idx_Excl_Preg_Test ON #Excl_Preg_Test ([memid]);


------EXcl_Preg_Test for optional Exclusion
	exec dbo.sp_kpi_droptable '#EXCL'
	CREATE table #EXCL
	(
		Memid varchar(50)
			
	);

	
insert into #EXCL
select  A.Memid from 
(select Distinct A.Memid,1 as Excl from #Pregnancy_Tests_Members A
left join #Preg_sexualActivity_Rx_Contraceptive B on A.Memid=B.Memid
inner join #Exclusion_Window C on A.Memid=C.Memid 
where B.Memid is null
) A



update #CHLdataset set EXCL=1 from #CHLdataset ds join #EXCL E on E.memid=ds.patient_id;



--select E.PATIENT_ID,E.EXCL,S.MemID,S.Excl from #CHLdataset E
--left join hds.HEDIS_score S 
--on E.patient_id=S.MemID 
--where measure_id='CHL_TEST' and E.excl<>S.excl


Declare @runid INT;
Declare @meas_year nvarchar(4)='2020'
select @runid=max(RUN_ID) from HDS.HEDIS_MEASURE_OUTPUT where measure_id='CHL';

if(@runid>=1)
Begin
SET @runid=@runid+1;
End
Else
Begin
		SET @runid=1;
END
	

Insert into HDS.HEDIS_MEASURE_OUTPUT(memid,Meas,payer,CE,Event,Epop,Excl,Num,Rexcl,RExclD,Age,Gender,Measure_ID,Measurement_year,RUN_ID)
SELECT patient_id AS memid,meas,payer,CE, [EVENT],CASE WHEN CE='1'  AND EVENT='1' AND rexcl='0' and rexcld='0' AND PAYER not in ('MCS','MC','MCR','MP') THEN 1 ELSE 0 END AS epop,excl,num,rexcl,rexcld,cast(age as Int) AS age,patient_gender AS gender,'CHL' as Measure_ID,@meas_year,@runid FROM #CHLdataset

---delete from hds.HEDIS_MEASURE_OUTPUT where measure_id='CHL'

---AND PAYER not in ('MCS','MC','MCR','MP') 


---select Trim(Memid) as MemId,Meas,Payer,CE,Event,Epop,Excl,Num,Rexcl,RExcld,Age,Gender 
---from HDS.HEDIS_MEASURE_OUTPUT where MEASURE_ID='CHL' and run_id=(select max(run_id) from HDS.HEDIS_MEASURE_OUTPUT where measure_id='CHL')
GO
