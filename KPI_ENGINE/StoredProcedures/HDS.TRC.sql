SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
---USE KPI_ENGINE
CREATE PROC HDS.TRC
AS


Drop table if exists #TRCdataset;

CREATE TABLE #TRCdataset (
  [patient_id] varchar(100) NOT NULL,
  [meas] varchar(20) DEFAULT NULL,
  [payer] varchar(100) DEFAULT NULL,
  [patient_gender] varchar(45) NOT NULL,
  [age] decimal(12,4) DEFAULT NULL,
  [orec] smallint DEFAULT NULL,
  [lis] smallint DEFAULT '0',
  [rexcl] smallint DEFAULT '0',
  [rexcld] smallint DEFAULT '0',
  [CE] smallint DEFAULT '0',
  [excl] smallint DEFAULT '0',
  [num] smallint DEFAULT '0',
  [Event] smallint DEFAULT 0,
) ;

-- Eligible Patient List
----CREATE INDEX [idx1] ON #TRCdataset ([payer]);
----CREATE INDEX [patient_id_UNIQUE] ON #TRCdataset ([patient_id],[payer]);

drop table if exists #Discharge; 

CREATE TABLE #Discharge (
    memid varchar(100)    ,
	Date_ADM varchar(10)  ,
	Date_Disch varchar(10)  
);
Insert into #Discharge(memid,Date_ADM,Date_Disch)
select Distinct Memid,Date_ADM,Date_Disch from hds.HEDIS_VISIT where Measure_ID='TRC_SAMPLE' ---AND SuppData='N' 
AND HCFAPOS<>'81' AND Convert(date,Date_Disch) Between '2020-01-01' AND '2020-12-01' AND Convert(date,Date_Disch) is not null
----AND REV in (select code from hds.VALUESET_TO_CODE where Value_Set_Name='Inpatient Stay')
--AND Memid in ('140430','95004')

drop table if exists #Discharge_Count; 
CREATE TABLE #Discharge_Count (
    memid varchar(100)    ,
	CNT bigInt 
);
Insert into #Discharge_Count(memid,CNT)
select distinct memid,Count(*) from #Discharge group by memid
Having count(*)>1 


------Redimission Within 30 days.

drop table if exists #Readmission; 
CREATE TABLE #Readmission (
    memid varchar(100)    ,
	Date_S varchar(10)
);
Insert into #Readmission(memid)
select Distinct V.memid from hds.HEDIS_VISIT V
Inner Join #Discharge D On V.MemID=D.memid and Convert(date,Date_S) Between Convert(date,D.Date_Disch) and dateAdd(d,30,Convert(date,D.Date_Disch))
where Measure_ID='TRC_SAMPLE' ---AND SuppData='N' 
AND HCFAPOS<>'81' AND Convert(date,V.Date_S) Between '2020-01-01' AND '2020-12-31' AND ISNULL(V.Date_Disch,'') =''----AND Convert(date,V.Date_Disch) Between '2020-01-01' AND '2020-12-01'
----AND V.Memid in ('140430','95004')
----select top 100 * from hds.HEDIS_VISIT where measure_id='TRC_SAMPLE' AND memid in  ('148480','95405') ----ddstatcode



drop table if exists #TRC_memlist; 

CREATE TABLE #TRC_memlist (
    memid varchar(100)    
);

insert into #TRC_memlist
SELECT DISTINCT en.MemID FROM HDS.HEDIS_MEMBER_GM gm
Inner JOIN hds.HEDIS_MEMBER_EN en on en.MemID=gm.MemID and en.MEASURE_ID=gm.MEASURE_ID
Inner join (select Distinct MemId from #Discharge_Count) CNT on gm.MemID=CNT.memid
WHERE en.MEASURE_ID='TRC_SAMPLE' ----and Gender='F' 
AND DATEDIFF(year,convert(date,DOB),'2020-12-31')>=18 
Union
SELECT DISTINCT en.MemID FROM HDS.HEDIS_MEMBER_GM gm
Inner JOIN hds.HEDIS_MEMBER_EN en on en.MemID=gm.MemID and en.MEASURE_ID=gm.MEASURE_ID
Inner join (select Distinct MemId from #Readmission) CNT on gm.MemID=CNT.MemID
WHERE en.MEASURE_ID='TRC_SAMPLE' ----and Gender='F' 
---and convert(date,en.StartDate)<='2020-12-31' AND convert(date,FinishDate)>='2020-01-01'
AND DATEDIFF(year,convert(date,DOB),'2020-12-31')>=18

CREATE CLUSTERED INDEX ix_memlist_memid ON #TRC_memlist ([memid]);


select * from #TRC_memlist M
left Join (Select distinct MemId from hds.HEDIS_SCORE where MEASURE_ID='TRC_SAMPLE') S
on M.memid=S.MemID
where S.MemID is null
order by Convert(Int,M.memid)


select * from #TRC_memlist M
RIGHT Join (Select distinct MemId from hds.HEDIS_SCORE where MEASURE_ID='TRC_SAMPLE') S
on M.memid=S.MemID
where M.MemID is null ---and M.memid='127042'
order by Convert(Int,M.memid)




-- Create Temp Patient Enrollment
drop table if exists #TRC_tmpsubscriber;

CREATE TABLE #TRC_tmpsubscriber (
    memid varchar(100),
    dob varchar(8),
	gender varchar(1),
	payer varchar(50),
	StartDate varchar(8),
	EndDate varchar(8),
);



insert into #TRC_tmpsubscriber
SELECT en.MemID,gm.DOB,gm.Gender,en.Payer,en.StartDate,en.FinishDate  FROM HDS.HEDIS_MEMBER_GM gm
join hds.HEDIS_MEMBER_EN en on en.MemID=gm.MemID and en.MEASURE_ID=gm.MEASURE_ID
WHERE en.MEASURE_ID='TRC_SAMPLE' and
en.StartDate<='2020-12-31' AND FinishDate>='2020-01-01'
AND YEAR('2020-12-31')-YEAR(DOB) >=18 ORDER BY en.MemID,en.StartDate,en.FinishDate;

CREATE CLUSTERED INDEX ix_tmpsub_memid ON #TRC_tmpsubscriber ([memid],[StartDate],[EndDate]);

Declare @pt_ctr INT;
Declare @i INT = 0;
Declare @patientid varchar(100);
Declare @gender varchar(1);
Declare @age int;
Declare @latest_insenddate varchar(10);
Declare @plan_ct INT=0;
Declare @planid varchar(20);
Declare @meas varchar(10)='CHL';
Declare @plan1 varchar(20);
Declare @plan2 varchar(20);

-- Loop through enrollment for each patient
SELECT @pt_ctr=COUNT(*)  FROM #TRC_memlist;

PRINT 'pt_ctr:' + Cast(@pt_ctr as nvarchar);


While @i<@pt_ctr
	BEGIN
		-- Get Patient to Loop
		SELECT  @patientid=memid FROM #TRC_memlist ORDER BY memid ASC OFFSET  @i ROWS FETCH NEXT 1 ROWS ONLY ;
		
		-- ReadingGender and age
		SELECT Top 1 @gender=gender,@age=Year('2020-12-31')-Year(dob) FROM #TRC_tmpsubscriber WHERE memid=@patientid;

		
		-- Check for dual eligibility by checking if the patient is enrolled in more than one plan at the end of Enrollment
		
		SELECT Top 1 @latest_insenddate=EndDate FROM #TRC_tmpsubscriber WHERE  memid=@patientid  AND StartDate<='2020-12-31' ORDER BY StartDate DESC,EndDate Desc  ;
		
		-- Check for plan count patient is enrolled in during the end of measurement year
		
		SELECT @plan_ct=COUNT(*) FROM #TRC_tmpsubscriber WHERE  memid=@patientid AND @latest_insenddate BETWEEN StartDate AND EndDate;
		
		-- If only one plan, then skip the Payer reporting logic
		
		if(@plan_ct=1)
		BEGIN
			
			SELECT @planid=payer FROM #TRC_tmpsubscriber WHERE  memid=@patientid AND @latest_insenddate BETWEEN StartDate AND EndDate ORDER BY payer;
		
			-- If plan is Dual , Insert MCR & MCD	
				
			If(@planid in('MMP','SN3','MDE'))
			BEGIN
				
				SET @planid='MCR';
				Insert INTO #TRCdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
				
				
			END
			
			Else if(@planid IN('MD','MLI','MRB'))
			Begin
				SET @planid='MCD';
				Insert INTO #TRCdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
			END
			
			Else If(@planid IN('SN1','SN2'))
			Begin
				SET @planid='MCR';
				Insert INTO #TRCdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
			END
			Else if(@planid IN('MEP','MMO','MOS','MPO'))
			BEGIN
				
				Insert INTO #TRCdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
				
			END
			ELSE
			BEGIN
				if(@planid IN('CEP','HMO','POS','PPO'))
				BEGIN
					Insert INTO #TRCdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
				END
				ELSE
				BEGIN
					Insert INTO #TRCdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
				End
			END
		END
				
		-- Compare Plans if Plan count is more than one	
		if(@plan_ct>1)
		BEGIN
			
			-- Read First Plan
			SELECT  @plan1=payer FROM #TRC_tmpsubscriber where memid=@patientid and @latest_insenddate between StartDate and EndDate order by 1 OFFSET  0 ROWS FETCH NEXT 1 ROWS ONLY ;
					
			-- Read second plan
			SELECT  @plan2=payer FROM #TRC_tmpsubscriber where memid=@patientid and @latest_insenddate between StartDate and EndDate order by 1 OFFSET  1 ROWS FETCH NEXT 1 ROWS ONLY ;
			
			
			
			If(@plan1 IN('MCR','MP','MC','SN1','SN2','MCS') and @plan2 IN('PPO','POS','HMO','CEP'))
			BEGIN
				if(@plan1 IN('SN1','SN2'))
				BEGIN
					SET @planid='MCR';
					Insert INTO #TRCdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
				END
				
				if(@plan1 in ('MCR','MP','MC','MCS'))
				Begin
					
					Set @planid=@plan1;
					Insert INTO #TRCdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
				END

			END
			
			Else If(@plan2 IN('MCR','MP','MC','SN1','SN2','MCS') AND @plan1 IN('PPO','POS','HMO','CEP'))
			BEGIN
			
				IF(@plan2 IN('SN1','SN2'))
				Begin
					Set @planid='MCR';
					Insert INTO #TRCdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
					
				END
				
				
				IF(@plan2 IN('MCR','MP','MC','MCS'))
				BEGIN
					
					Set @planid=@plan2;
					
					Insert INTO #TRCdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
				END
			END
			
			ELSE IF(@plan1 IN('PPO','POS','HMO','CEP') AND @plan2 IN('MD','MLI','MRB'))
			BEGIN
			
				Set @planid=@plan1;
				Insert INTO #TRCdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);	
			END
			
			ELSE IF(@plan2 IN('PPO','POS','HMO','CEP') AND @plan1 IN('MD','MLI','MRB'))
			BEGIN
			
				Set @planid=@plan2;
				Insert INTO #TRCdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
			END
			
			ELSE IF(@plan1 IN('MDE','SN3','MMP') or @plan2 in('MDE','SN3','MMP'))
			BEGIN
			
				Set @planid='MCR';
				Insert INTO #TRCdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
				
			END
			
			ELSE
			BEGIN
			
				Set @planid=@plan1;	
				Insert INTO #TRCdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
				Set @planid=@plan2;
				Insert INTO #TRCdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);				
				
			END
			
		END
		
		Set @i=@i+1;
		
	END;
-- Create Indices on cdcdataset
CREATE INDEX [idx1] ON #TRCdataset ([payer]);
CREATE INDEX [patient_id_UNIQUE] ON #TRCdataset ([patient_id],[payer]);

-- Continuous Enrollment



select count(distinct patient_id) from #TRCdataset
select count(distinct MemId) from hds.HEDIS_SCORE where measure_id='TRC_SAMPLE'

select distinct Payer from hds.HEDIS_SCORE where measure_id='TRC_SAMPLE'


select * from hds.HEDIS_SCORE where measure_id='TRC_SAMPLE' order by convert(int,MemID)



select Distinct ''''+LTRIM(RTRIM(Patient_Id))+''''+',' from #TRCdataset A
Left join (select distinct MemId from hds.HEDIS_SCORE where measure_id='TRC_SAMPLE') S
On A.patient_id=S.MemID 
where S.MemID is null

select * from hds.hedis_score where MEASURE_ID='TRC_SAMPLE' and memid='137002'
select * from hds.HEDIS_MEMBER_EN where memid='137002' and measure_id='TRC_SAMPLE'
select * from hds.HEDIS_MEMBER_GM where memid='137002' and measure_id='TRC_SAMPLE'
Select *  from hds.HEDIS_VISIT where MEMID in ('137002') and measure_id='TRC_SAMPLE'
Select *  from hds.HEDIS_VISIT_E where MEMID in ('137002') and measure_id='TRC_SAMPLE'
select * from hds.VALUESET_TO_CODE where code in ('0114')

----Select *  from hds.HEDIS_MMDF where beneficiary_id in ('100001','100002','100005') and measure_id='TRC_SAMPLE'

select * from hds.HEDIS_MEMBER_EN where memid in ('100001','100002') and measure_id='TRC_SAMPLE'
select * from hds.HEDIS_MEMBER_GM where memid in ('100001','100002') and measure_id='TRC_SAMPLE'


Select *  from hds.HEDIS_VISIT where MEMID in ('100001','100002') and measure_id='TRC_SAMPLE'

Select *  from hds.HEDIS_VISIT where measure_id='TRC_SAMPLE'
AND MEMID in
(
select Distinct Patient_Id from #TRCdataset A
Left join (select distinct MemId from hds.HEDIS_SCORE where measure_id='TRC_SAMPLE') S
On A.patient_id=S.MemID 
where S.MemID is null
)



select distinct Code_System,code from hds.VALUESET_TO_CODE where Value_Set_Name='Inpatient Stay'
GO
