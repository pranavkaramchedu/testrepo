SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

CREATE PROCEDURE [HDS].[RUN_HEDIS_COL] @meas_year nvarchar(4)
AS
-- Declare Variables

Declare @pt_ctr INT;
Declare @runid INT;
Declare @i INT = 0;
Declare @plan_ct INT=0;
Declare @meas varchar(10)='COL';
Declare @patientid varchar(100);
Declare @gender varchar(1);
Declare @age int;
Declare @latest_insenddate varchar(8);
Declare @planid varchar(20);
Declare @plan1 varchar(20);
Declare @plan2 varchar(20);
DECLARE @ce_startdt1 varchar(8);
DECLARE @ce_enddt1 varchar(8);
DECLARE @ce_startdt varchar(8);
DECLARE @ce_enddt varchar(8);
DECLARE @ce_dt5 varchar(8);
DECLARE @ce_dt10 varchar(8);
DECLARE @ce_dt3 varchar(8);


SET @ce_startdt=concat(@meas_year,'0101');;
	SET @ce_enddt1=concat(@meas_year-1,'1231');
	SET @ce_startdt1=concat(@meas_year-1,'0101');

	SET @ce_enddt=concat(@meas_year,'1231');
	
	SET @ce_dt5=concat(@meas_year-4,'0101');
	SET @ce_dt10=concat(@meas_year-9,'0101');
	SET @ce_dt3=concat(@meas_year-2,'0101');
	

-- Create coldataset
drop table if exists #coldataset;

CREATE TABLE #coldataset (
  [patient_id] varchar(100) NOT NULL,
  [meas] varchar(20) DEFAULT NULL,
  [payer] varchar(100) DEFAULT NULL,
  [patient_gender] varchar(45) NOT NULL,
  [age] decimal(12,4) DEFAULT NULL,
  [orec] smallint DEFAULT NULL,
  [lis] smallint DEFAULT '0',
  [rexcl] smallint DEFAULT '0',
  [rexcld] smallint DEFAULT '0',
  [CE] smallint DEFAULT '0',
  [excl] smallint DEFAULT '0',
  [num] smallint DEFAULT '0'
) ;

CREATE INDEX [idx1] ON #coldataset ([payer]);
CREATE INDEX [patient_id_UNIQUE] ON #coldataset ([patient_id],[payer]);


-- Eligible Patient List

drop table if exists #col_memlist; 

CREATE TABLE #col_memlist (
    memid varchar(100)
    
);

CREATE CLUSTERED INDEX ix_memlist_memid ON #col_memlist ([memid]);

insert into #col_memlist
SELECT DISTINCT en.MemID FROM HDS.HEDIS_MEMBER_GM gm
join hds.HEDIS_MEMBER_EN en on en.MemID=gm.MemID and en.MEASURE_ID=gm.MEASURE_ID
WHERE en.MEASURE_ID='COL' and
en.StartDate<=@ce_enddt AND FinishDate>=@ce_startdt1
AND YEAR(@ce_enddt)-YEAR(DOB) BETWEEN 51 AND 75 ORDER BY 1;




-- Create Temp Patient Enrollment
drop table if exists #col_tmpsubscriber;

CREATE TABLE #col_tmpsubscriber (
    memid varchar(100),
    dob varchar(8),
	gender varchar(1),
	payer varchar(50),
	StartDate varchar(8),
	EndDate varchar(8),
);

CREATE CLUSTERED INDEX ix_tmpsub_memid ON #col_tmpsubscriber ([memid],[StartDate],[EndDate]);

insert into #col_tmpsubscriber
SELECT en.MemID,gm.DOB,gm.Gender,en.Payer,en.StartDate,en.FinishDate  FROM HDS.HEDIS_MEMBER_GM gm
join hds.HEDIS_MEMBER_EN en on en.MemID=gm.MemID and en.MEASURE_ID=gm.MEASURE_ID
WHERE en.MEASURE_ID='COL' and
en.StartDate<=@ce_enddt AND FinishDate>=@ce_startdt1
AND YEAR(@ce_enddt)-YEAR(DOB) BETWEEN 51 AND 75 ORDER BY en.MemID,en.StartDate,en.FinishDate;



-- Get Patient COUNT
SELECT @pt_ctr=COUNT(*)  FROM #col_memlist;

PRINT 'pt_ctr:' + Cast(@pt_ctr as nvarchar);

-- While LOOP

While @i<@pt_ctr
	BEGIN
		SELECT  @patientid=memid FROM #col_memlist ORDER BY memid ASC OFFSET  @i ROWS FETCH NEXT 1 ROWS ONLY ;
		
		-- ReadingGender and age
		SELECT Top 1 @gender=gender,@age=YEAR(@ce_enddt)-YEAR(DOB) FROM #col_tmpsubscriber WHERE memid=@patientid;
		
		-- Check for dual eligibility by checking if the patient is enrolled in more than one plan at the end of Enrollment
		SELECT Top 1 @latest_insenddate=EndDate FROM #col_tmpsubscriber WHERE  memid=@patientid  AND StartDate<=@ce_enddt ORDER BY EndDate DESC  ;
		
		-- Check for plan count patient is enrolled in during the end of measurement year
		SELECT @plan_ct=COUNT(*) FROM #col_tmpsubscriber WHERE  memid=@patientid AND @latest_insenddate BETWEEN StartDate AND EndDate;
		
		-- If only one plan, then skip the Payer reporting logic
		IF @plan_ct=1
	BEGIN
		SELECT @planid=payer FROM #col_tmpsubscriber WHERE  memid=@patientid AND @latest_insenddate BETWEEN StartDate AND EndDate ORDER BY payer;
				
		-- If plan is Dual , -- Insert MCR & MCD	
				
		If @planid in('MMP','SN3','MDE')
		Begin
				
			if(@planid in('MMP','SN3'))
			Begin
				Insert INTO #coldataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
			End
						
			SET @planid='MCR';
			Insert INTO #coldataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
		End				
		Else If @planid in('MD','MLI','MRB')
		Begin	
			SET @planid='MCD';
			Insert INTO #coldataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
		End				
		Else If @planid in('SN1','SN2')
		Begin
			Insert INTO #coldataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
			SET @planid='MCR';
			Insert INTO #coldataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
		End				
		ELSE
		Begin			
			Insert INTO #coldataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
		End	
				
		
	END;

			
			
IF(@plan_ct>1) 
Begin
	-- Read First Plan
	SELECT  @plan1=payer FROM #col_tmpsubscriber where memid=@patientid and @latest_insenddate between StartDate and EndDate order by 1 OFFSET  0 ROWS FETCH NEXT 1 ROWS ONLY ;
			
		-- Read second plan
		SELECT  @plan2=payer FROM #col_tmpsubscriber where memid=@patientid and @latest_insenddate between StartDate and EndDate order by 1 OFFSET  1 ROWS FETCH NEXT 1 ROWS ONLY ;
				
            
            
	IF(@plan1 IN('MCR','MP','MC','SN1','SN2','MCS') and @plan2 in('PPO','POS','HMO','CEP'))
    Begin
		IF @plan1 in('SN1','SN2')
        Begin
			SET @planid=@plan1;
			INSERT INTO #coldataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
			SET @planid='MCR';
			INSERT INTO #coldataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
		END
                
        if @plan1 in('MCR','MP','MC','MCS')
        Begin
			SET @planid=@plan1;
			INSERT INTO #coldataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
		End

	End
		        
	ELSE IF(@plan2 IN('MCR','MP','MC','SN1','SN2','MCS') and @plan1 in('PPO','POS','HMO','CEP'))
	Begin
		IF(@plan2 IN('SN1','SN2'))
        Begin
			SET @planid=@plan2;
			INSERT INTO #coldataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
			
			SET @planid='MCR';
			INSERT INTO #coldataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
		END
                
        IF(@plan2 IN('MCR','MP','MC','MCS'))
        Begin
			SET @planid=@plan2;
			INSERT INTO #coldataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
		End

	End
					
	ELSE IF(@plan1 IN('PPO','POS','HMO','CEP') AND @plan2 IN('MD','MLI','MRB'))
    Begin
		SET @planid=@plan1;
        INSERT INTO #coldataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
	End
                
	ELSE IF(@plan2 IN('PPO','POS','HMO','CEP') AND @plan1 IN('MD','MLI','MRB'))
    Begin
		SET @planid=@plan2;
        INSERT INTO #coldataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);

	End
                
	ELSE IF(@plan1 IN('MDE','SN3','MMP') OR @plan2 IN('MDE','SN3','MMP'))
    Begin
		
		IF(@plan1 IN('SN3','MMP'))
		BEGIN
			SET @planid=@plan1
			INSERT INTO #coldataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
		END
        
		IF(@plan2 IN('SN3','MMP'))
		BEGIN
			SET @planid=@plan2
			INSERT INTO #coldataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
		END
        
		SET @planid='MCR';
        INSERT INTO #coldataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
                
       
	End
			
    ELSE
	Begin			
         SET @planid=@plan1;
         INSERT INTO #coldataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
                
         SET @planid=@plan2;
         INSERT INTO #coldataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
	End
		 
						 
End	
	SET @i=@i+1;
	END;
	
	-- Continuous Enrollment
	
	
	DROP TABLE IF EXISTS #contenroll;
	
	CREATE table #contenroll
	(
		Memid varchar(50),
		CE INT
	);

	CREATE CLUSTERED INDEX ix_contenroll_memid ON #contenroll ([memid]);

	
	Insert into #contenroll
	SELECT MemID,SUM(ceflag) AS CE FROM
	(
		-- CE Check for 1st year
		SELECT MemID,SUM(coverage) AS coverage, 1 AS ceflag FROM
		(
			SELECT MemID,Payer,start_date,end_date,DATEDIFF(day,start_date,end_date)+1 AS coverage FROM
			(

				SELECT s.MemID,s.Payer,s.FinishDate,
				CASE
				WHEN StartDate<@ce_startdt1 THEN @ce_startdt1
				ELSE StartDate
				END AS start_date,
				CASE
				WHEN FinishDate>@ce_enddt1 THEN @ce_enddt1
				ELSE FinishDate
				END AS end_date
				FROM HEDIS_MEMBER_EN s
				JOIN 
				(
					SELECT MemID,SUM(diff) as diff FROM
					(
						SELECT MemID,nextstartdate,FinishDate,
						CASE
						WHEN DATEDIFF(day,FinishDate,nextstartdate)<=1 THEN 0
						ELSE 1
						END AS diff
						  FROM
						  (
							SELECT MemID,StartDate,FinishDate,
							CASE
							WHEN nextstartdate IS NULL THEN FinishDate
							ELSE nextstartdate
							END AS nextstartdate FROM
							(
								SELECT s.MemID,s.StartDate,s.FinishDate,
								(
									SELECT Top 1 s1.StartDate
									FROM HEDIS_MEMBER_EN s1
									WHERE s1.MEASURE_ID=s.MEASURE_ID AND s1.MemID=s.MemID AND s.FinishDate<s1.StartDate AND s1.StartDate<=@ce_enddt1 AND s1.FinishDate>=@ce_startdt1
									ORDER BY s1.MemID,s1.StartDate,s1.FinishDate
								) AS nextstartdate

								 FROM HEDIS_MEMBER_EN s
								WHERE MEASURE_ID='COL' AND s.StartDate<=@ce_enddt1 AND s.FinishDate>=@ce_startdt1
							)t
						)t1 
					)t2 GROUP BY MemID HAVING SUM(diff)<=1
				)
				s1 ON s.MemID=s1.MemID
				WHERE s.MEASURE_ID='COL'
				-- SQLINES DEMO *** 100001
				AND s.StartDate<=@ce_enddt1 AND s.FinishDate>=@ce_startdt1 
	
			)t2
		)t3 GROUP BY MemID HAVING sum(coverage)>=(DATEPART(dy, @ce_enddt1)-45)
 
	UNION ALL
		-- CE check for 2nd year
		-- SQLINES LICENSE FOR EVALUATION USE ONLY
		SELECT MemID,SUM(coverage) AS coverage, 1 AS ceflag FROM
		(
			SELECT MemID,Payer,start_date,end_date,DATEDIFF(day,start_date,end_date)+1 AS coverage FROM
			(

				SELECT s.MemID,s.Payer,s.FinishDate,
				CASE
					WHEN StartDate<@ce_startdt THEN @ce_startdt
					ELSE StartDate
				END AS start_date,
				CASE
					WHEN FinishDate>@ce_enddt THEN @ce_enddt
					ELSE FinishDate
				END AS end_date
				FROM HEDIS_MEMBER_EN s
				JOIN 
				(
					SELECT MemID,SUM(diff) as diff FROM
					(
						SELECT MemID,nextstartdate,FinishDate,
						CASE
							WHEN DATEDIFF(day,FinishDate,nextstartdate)<=1 THEN 0
							ELSE 1
						END AS diff
						FROM
						(
							SELECT MemID,StartDate,FinishDate,
							CASE
								WHEN nextstartdate IS NULL THEN	FinishDate
								ELSE nextstartdate
							END AS nextstartdate FROM
							(
								SELECT s.MemID,s.StartDate,s.	FinishDate,
								(
									SELECT Top 1 s1.StartDate
									FROM HEDIS_MEMBER_EN s1
									WHERE s1.MEASURE_ID=s.MEASURE_ID AND s1.MemID=s.MemID AND s.FinishDate<s1.StartDate AND s1.StartDate<=@ce_enddt AND s1.FinishDate>=@ce_startdt
									ORDER BY s1.MemID,s1.StartDate,	s1.FinishDate
								) AS nextstartdate

								FROM HEDIS_MEMBER_EN s
								WHERE MEASURE_ID='COL' AND s.StartDate<=@ce_enddt AND s.	FinishDate>=@ce_startdt
							)t
						)t1 
					)t2 GROUP BY MemID HAVING SUM(diff)<=1
				)
				s1 ON s.MemID=s1.MemID
				WHERE s.MEASURE_ID='COL'
				-- SQLINES DEMO *** 100001
				AND s.StartDate<=@ce_enddt AND s.FinishDate>=@ce_startdt
		
			)t2
		)t3 GROUP BY MemID HAVING sum(coverage)>=(DATEPART(dy, @ce_enddt)-45)
 
		UNION ALL

		-- Anchor Date
	 
		SELECT MemID,SUM(DATEDIFF(day,start_date,end_date)) AS coverage, 1 AS ceflag FROM
			(
			SELECT MemID,FinishDate,
			CASE
				WHEN StartDate<@ce_startdt THEN @ce_startdt
				ELSE StartDate
			END AS start_date,
			CASE
				WHEN FinishDate>@ce_enddt THEN @ce_enddt
				ELSE FinishDate
			END AS end_date
			FROM HEDIS_MEMBER_EN 
			WHERE MEASURE_ID='COL'
			-- SQLINES DEMO *** 0000          
			AND @ce_enddt BETWEEN StartDate AND FinishDate
		
		)t4  GROUP BY MemID 
	)cont_enroll GROUP BY MemID  HAVING SUM(ceflag)=3 order by MemID;

	Print 'Cont Enroll'



update #coldataset set CE=1 from #coldataset ds join #contenroll ce on ds.patient_id=ce.MemID;
	



-- Advanced Illness
drop table if exists #col_advillness;
CREATE table #col_advillness
(
	Memid varchar(50),
	servicedate varchar(8),
	claimid INT
);

	CREATE CLUSTERED INDEX ix_contadvillness ON #col_advillness ([memid],[servicedate],[claimid]);

	Insert into #col_advillness
	select MemID,Date_S,claimid from HEDIS_VISIT v
	where MEASURE_ID='COL' and HCFAPOS!=81 and suppdata='N' and Date_S between @ce_startdt1 and @ce_enddt and Date_S!=''
	and (
	Diag_I_1 in(select code from VALUESET_TO_CODE where Value_Set_Name='Advanced Illness')
	or
	Diag_I_2 in(select code from VALUESET_TO_CODE where Value_Set_Name='Advanced Illness')
	or
	Diag_I_3 in(select code from VALUESET_TO_CODE where Value_Set_Name='Advanced Illness')
	or
	Diag_I_4 in(select code from VALUESET_TO_CODE where Value_Set_Name='Advanced Illness')
	or
	Diag_I_5 in(select code from VALUESET_TO_CODE where Value_Set_Name='Advanced Illness')
	or
	Diag_I_6 in(select code from VALUESET_TO_CODE where Value_Set_Name='Advanced Illness')
	or
	Diag_I_7 in(select code from VALUESET_TO_CODE where Value_Set_Name='Advanced Illness')
	or
	Diag_I_8 in(select code from VALUESET_TO_CODE where Value_Set_Name='Advanced Illness')
	or
	Diag_I_9 in(select code from VALUESET_TO_CODE where Value_Set_Name='Advanced Illness')
	or
	Diag_I_10 in(select code from VALUESET_TO_CODE where Value_Set_Name='Advanced Illness')
	or
	CPT in(select code from VALUESET_TO_CODE where Value_Set_Name='Advanced Illness')
	or
	CPT2 in(select code from VALUESET_TO_CODE where Value_Set_Name='Advanced Illness')
	or
	HCPCS in(select code from VALUESET_TO_CODE where Value_Set_Name='Advanced Illness')
	or 
	Proc_I_1 in(select code from VALUESET_TO_CODE where Value_Set_Name='Advanced Illness')
	or 
	Proc_I_2 in(select code from VALUESET_TO_CODE where Value_Set_Name='Advanced Illness')
	or 
	Proc_I_3 in(select code from VALUESET_TO_CODE where Value_Set_Name='Advanced Illness')
	or 
	Proc_I_4 in(select code from VALUESET_TO_CODE where Value_Set_Name='Advanced Illness')
	or 
	Proc_I_5 in(select code from VALUESET_TO_CODE where Value_Set_Name='Advanced Illness')
	)
	


-- Required Exclusion
drop table if exists #col_reqdexcl;
CREATE table #col_reqdexcl
(
	Memid varchar(50)
			
);

	CREATE CLUSTERED INDEX idx_col_reqdexcl ON #col_reqdexcl ([memid]);


insert into #col_reqdexcl
select distinct memid from(
select memid from HEDIS_OBS where MEASURE_ID='COL' and date between @ce_startdt and @ce_enddt and date!='' and OCode in(select code from VALUESET_TO_CODE where Value_Set_Name in('Palliative Care Assessment','Palliative Care Intervention','Palliative Care Encounter'))
Union all
select memid from HEDIS_VISIT_E where MEASURE_ID='COL' and sdate between @ce_startdt and @ce_enddt and sdate!='' and activity in(select code from VALUESET_TO_CODE where Value_Set_Name in('Palliative Care Assessment','Palliative Care Intervention','Palliative Care Encounter'))
Union all
select MemID from HEDIS_VISIT v
	where MEASURE_ID='COL' and HCFAPOS!=81 and Date_S between @ce_startdt and @ce_enddt and Date_S!=''
	and (
	Diag_I_1 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Palliative Care Assessment','Palliative Care Intervention','Palliative Care Encounter'))
	or
	Diag_I_2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Palliative Care Assessment','Palliative Care Intervention','Palliative Care Encounter'))
	or
	Diag_I_3 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Palliative Care Assessment','Palliative Care Intervention','Palliative Care Encounter'))
	or
	Diag_I_4 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Palliative Care Assessment','Palliative Care Intervention','Palliative Care Encounter'))
	or
	Diag_I_5 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Palliative Care Assessment','Palliative Care Intervention','Palliative Care Encounter'))
	or
	Diag_I_6 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Palliative Care Assessment','Palliative Care Intervention','Palliative Care Encounter'))
	or
	Diag_I_7 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Palliative Care Assessment','Palliative Care Intervention','Palliative Care Encounter'))
	or
	Diag_I_8 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Palliative Care Assessment','Palliative Care Intervention','Palliative Care Encounter'))
	or
	Diag_I_9 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Palliative Care Assessment','Palliative Care Intervention','Palliative Care Encounter'))
	or
	Diag_I_10 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Palliative Care Assessment','Palliative Care Intervention','Palliative Care Encounter'))
	or
	CPT in(select code from VALUESET_TO_CODE where Value_Set_Name in('Palliative Care Assessment','Palliative Care Intervention','Palliative Care Encounter'))
	or
	CPT2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Palliative Care Assessment','Palliative Care Intervention','Palliative Care Encounter'))
	or
	HCPCS in(select code from VALUESET_TO_CODE where Value_Set_Name in('Palliative Care Assessment','Palliative Care Intervention','Palliative Care Encounter'))
	or 
	Proc_I_1 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Palliative Care Assessment','Palliative Care Intervention','Palliative Care Encounter'))
	or 
	Proc_I_2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Palliative Care Assessment','Palliative Care Intervention','Palliative Care Encounter'))
	or 
	Proc_I_3 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Palliative Care Assessment','Palliative Care Intervention','Palliative Care Encounter'))
	or 
	Proc_I_4 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Palliative Care Assessment','Palliative Care Intervention','Palliative Care Encounter'))
	or 
	Proc_I_5 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Palliative Care Assessment','Palliative Care Intervention','Palliative Care Encounter'))
	)
	)t1
	
	update #coldataset set rexcld=1 from #coldataset ds join #col_reqdexcl re on ds.patient_id=re.memid;

	

-- Members with Institutinal SNP

	UPDATE #coldataset SET #coldataset.rexcl=1 FROM #coldataset ds JOIN HEDIS_MEMBER_EN s on ds.patient_id=s.MemID and s.MEASURE_ID='COL' WHERE  ds.age>=66 AND ds.payer IN('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP') AND s.StartDate<=@ce_enddt AND s.FinishDate>=@ce_startdt AND s.Payer='SN2';

-- LTI Exclusion
	drop table if exists #col_LTImembers;
	CREATE table #col_LTImembers
			(
				Memid varchar(50)
				
			);

		CREATE CLUSTERED INDEX idx_col_ltimembers ON #col_LTImembers ([memid]);

	Insert into #col_LTImembers
	SELECT DISTINCT Beneficiary_ID FROM HEDIS_MMDF WHERE Measure_ID='COL' AND Run_Date BETWEEN @ce_startdt AND @ce_enddt AND LTI_Flag='Y';



	update #coldataset set rexcl=1 from #coldataset ds join #col_LTImembers re on ds.patient_id=re.Memid where ds.age>=66 AND ds.payer IN('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP');;

	
	
-- Hospice Exclusion
drop table if exists #col_hospicemembers;
CREATE table #col_hospicemembers
		(
			Memid varchar(50)
			
		);

	CREATE CLUSTERED INDEX idx_hospicemembers ON #col_hospicemembers ([memid]);

Insert into #col_hospicemembers
select distinct t1.MemID from
(
select Memid from HEDIS_OBS where Measure_id='COL' and date between @ce_startdt and @ce_enddt and OCode IN(select code from VALUESET_TO_CODE where Value_Set_Name in('Hospice Encounter','Hospice Intervention'))
Union all
Select Memid from HEDIS_VISIT_E where Measure_id='COL' and sdate between @ce_startdt and @ce_enddt and activity IN(select code from VALUESET_TO_CODE where Value_Set_Name in('Hospice Encounter','Hospice Intervention'))

Union all

select Beneficiary_id as Memid from Hedis_MMDF where measure_id='COL' and Run_date between @ce_startdt and @ce_enddt and Hospice='Y'

Union all

select MemID from HEDIS_VISIT v
	where MEASURE_ID='COL' and HCFAPOS!=81 and Date_S between @ce_startdt and @ce_enddt
	and (
	Diag_I_1 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or
	Diag_I_2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or
	Diag_I_3 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or
	Diag_I_4 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or
	Diag_I_5 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or
	Diag_I_6 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or
	Diag_I_7 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or
	Diag_I_8 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or
	Diag_I_9 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or
	Diag_I_10 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or
	CPT in(select code from VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or
	CPT2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or
	HCPCS in(select code from VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or 
	Proc_I_1 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or 
	Proc_I_2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or 
	Proc_I_3 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or 
	Proc_I_4 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or 
	Proc_I_5 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or
	Rev in(select code from VALUESET_TO_CODE where Code_System='UBREV' and Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	)
	)t1
	
	update #coldataset set rexcl=1 from #coldataset ds join #col_hospicemembers hos on hos.memid=ds.patient_id;
	
	-- Frailty Members LIST
	drop table if exists #col_frailtymembers;
	CREATE table #col_frailtymembers
		(
		
		Memid varchar(50)
			
		);

	CREATE CLUSTERED INDEX idx_col_frailtymembers ON #col_frailtymembers ([memid]);

Insert into #col_frailtymembers
select MemID from HEDIS_VISIT v
	where MEASURE_ID='COL' and HCFAPOS!=81 and suppdata='N' and Date_S between @ce_startdt and @ce_enddt
	and (
	Diag_I_1 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Frailty Diagnosis','Frailty Device','Frailty Encounter','Frailty Symptom'))
	or
	Diag_I_2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Frailty Diagnosis','Frailty Device','Frailty Encounter','Frailty Symptom'))
	or
	Diag_I_3 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Frailty Diagnosis','Frailty Device','Frailty Encounter','Frailty Symptom'))
	or
	Diag_I_4 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Frailty Diagnosis','Frailty Device','Frailty Encounter','Frailty Symptom'))
	or
	Diag_I_5 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Frailty Diagnosis','Frailty Device','Frailty Encounter','Frailty Symptom'))
	or
	Diag_I_6 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Frailty Diagnosis','Frailty Device','Frailty Encounter','Frailty Symptom'))
	or
	Diag_I_7 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Frailty Diagnosis','Frailty Device','Frailty Encounter','Frailty Symptom'))
	or
	Diag_I_8 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Frailty Diagnosis','Frailty Device','Frailty Encounter','Frailty Symptom'))
	or
	Diag_I_9 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Frailty Diagnosis','Frailty Device','Frailty Encounter','Frailty Symptom'))
	or
	Diag_I_10 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Frailty Diagnosis','Frailty Device','Frailty Encounter','Frailty Symptom'))
	or
	CPT in(select code from VALUESET_TO_CODE where Value_Set_Name in('Frailty Diagnosis','Frailty Device','Frailty Encounter','Frailty Symptom'))
	or
	CPT2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Frailty Diagnosis','Frailty Device','Frailty Encounter','Frailty Symptom'))
	or
	HCPCS in(select code from VALUESET_TO_CODE where Value_Set_Name in('Frailty Diagnosis','Frailty Device','Frailty Encounter','Frailty Symptom'))
	or 
	Proc_I_1 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Frailty Diagnosis','Frailty Device','Frailty Encounter','Frailty Symptom'))
	or 
	Proc_I_2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Frailty Diagnosis','Frailty Device','Frailty Encounter','Frailty Symptom'))
	or 
	Proc_I_3 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Frailty Diagnosis','Frailty Device','Frailty Encounter','Frailty Symptom'))
	or 
	Proc_I_4 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Frailty Diagnosis','Frailty Device','Frailty Encounter','Frailty Symptom'))
	or 
	Proc_I_5 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Frailty Diagnosis','Frailty Device','Frailty Encounter','Frailty Symptom'))
	or
	Rev in(select code from VALUESET_TO_CODE where Code_System='UBREV' and Value_Set_Name in('Frailty Diagnosis','Frailty Device','Frailty Encounter','Frailty Symptom'))
	);
	
	


	
-- Required Exclusion 1

	-- Inpatient Stay List
	drop table if exists #col_inpatientstaylist;
	CREATE table #col_inpatientstaylist
	(
		Memid varchar(50),
		date_s varchar(8),
		claimid INT
	);

	CREATE CLUSTERED INDEX ix_col_inpatientstaylist ON #col_inpatientstaylist ([memid],[date_s],[claimid]);

	Insert into #col_inpatientstaylist
	select distinct MemID,Date_S,claimid from HEDIS_VISIT v where v.Measure_id='COL' and suppdata='N' and v.HCFAPOS!=81 and v.date_disch between @ce_startdt1 and @ce_enddt and v.REV in (select code from VALUESET_TO_CODE where  Value_Set_Name in('Inpatient Stay') and code_system='UBREV');

	-- Non acute Inpatient stay list
	Drop table if exists #col_noncauteinpatientstaylist;
	CREATE table #col_noncauteinpatientstaylist
	(
		Memid varchar(50),
		date_s varchar(8),
		claimid INT
	);

	CREATE CLUSTERED INDEX ix_col_nonacuteinpatientstaylist ON #col_noncauteinpatientstaylist ([memid],[date_s],[claimid]);

	Insert into #col_noncauteinpatientstaylist
	select distinct MemID,Date_S,claimid from HEDIS_VISIT v where v.Measure_id='COL' and suppdata='N' and v.HCFAPOS!=81 and v.date_disch between @ce_startdt1 and @ce_enddt and (v.REV in (select code from VALUESET_TO_CODE where Code_System='UBREV' and Value_Set_Name in('Nonacute Inpatient Stay')) or RIGHT('0000'+CAST(Trim(v.BillType) AS VARCHAR(4)),4) in (select code from VALUESET_TO_CODE where Code_System='UBTOB' and Value_Set_Name in('Nonacute Inpatient Stay')))
	
	-- Outpatient and other visits
	drop table if exists #col_visitlist;
	CREATE table #col_visitlist
	(
		Memid varchar(50),
		date_s varchar(8),
		claimid INT
	);

	CREATE CLUSTERED INDEX ix_col_visitlist ON #col_visitlist ([memid],[date_s],[claimid]);

	Insert into #col_visitlist
	select distinct MemID,Date_S,claimid from HEDIS_VISIT v
	where MEASURE_ID='COL' and HCFAPOS!=81 and suppdata='N'  and Date_S between @ce_startdt1 and @ce_enddt
	and (
	Diag_I_1 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments','Nonacute Inpatient'))
	or
	Diag_I_2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments','Nonacute Inpatient'))
	or
	Diag_I_3 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments','Nonacute Inpatient'))
	or
	Diag_I_4 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments','Nonacute Inpatient'))
	or
	Diag_I_5 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments','Nonacute Inpatient'))
	or
	Diag_I_6 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments','Nonacute Inpatient'))
	or
	Diag_I_7 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments','Nonacute Inpatient'))
	or
	Diag_I_8 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments','Nonacute Inpatient'))
	or
	Diag_I_9 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments','Nonacute Inpatient'))
	or
	Diag_I_10 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments','Nonacute Inpatient'))
	or
	CPT in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments','Nonacute Inpatient'))
	or
	CPT2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments','Nonacute Inpatient'))
	or
	HCPCS in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments','Nonacute Inpatient'))
	or 
	Proc_I_1 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments','Nonacute Inpatient'))
	or 
	Proc_I_2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments','Nonacute Inpatient'))
	or 
	Proc_I_3 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments','Nonacute Inpatient'))
	or 
	Proc_I_4 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments','Nonacute Inpatient'))
	or 
	Proc_I_5 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments','Nonacute Inpatient'))
	or 
	Rev in(select code from VALUESET_TO_CODE where Code_System='UBREV' and Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments','Nonacute Inpatient'))
	
	)

	-- Required exclusion table
	drop table if exists #col_reqdexcl1;
	CREATE table #col_reqdexcl1
	(
		Memid varchar(50)
			
	);

	CREATE CLUSTERED INDEX idx_col_reqdexcl1 ON #col_reqdexcl1 ([memid]);

	Insert into #col_reqdexcl1
	select distinct t3.Memid from(
	select t2.MemId from(
	select distinct t1.Memid,t1.date_s from(
	select Memid,date_s,claimid  from #col_visitlist 
	union all
	select na.Memid,na.Date_s,na.claimid from #col_noncauteinpatientstaylist na
	join #col_inpatientstaylist inp on na.Memid=inp.Memid and na.claimid=inp.claimid 
	)t1
	Join #col_advillness a on a.Memid=t1.Memid and a.claimid=t1.claimid 
	)t2 group by t2.Memid having count(t2.Memid)>1
	)t3 
	Join #col_frailtymembers f on f.Memid=t3.Memid

	update #coldataset set rexcl=1 from #coldataset ds
	join #col_reqdexcl1 re1 on re1.Memid=ds.patient_id and ds.age>=66;
	
-- Required Exclusion 2

	-- Acute Inpatient with Advanced Illness
	drop table if exists #col_reqdexcl2;
	CREATE table #col_reqdexcl2
	(
		Memid varchar(50)
				
	);

	CREATE CLUSTERED INDEX idx_col_reqdexcl2 ON #col_reqdexcl2 ([memid]);

	insert into #col_reqdexcl2
	select distinct t2.Memid from(
	select t1.MemID,t1.claimid from (
	select distinct MemID,Date_S,claimid from HEDIS_VISIT v
	where MEASURE_ID='COL' and HCFAPOS!=81 and suppdata='N' and  Date_S between @ce_startdt1 and @ce_enddt
	and (
	Diag_I_1 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Acute Inpatient'))
	or
	Diag_I_2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Acute Inpatient'))
	or
	Diag_I_3 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Acute Inpatient'))
	or
	Diag_I_4 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Acute Inpatient'))
	or
	Diag_I_5 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Acute Inpatient'))
	or
	Diag_I_6 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Acute Inpatient'))
	or
	Diag_I_7 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Acute Inpatient'))
	or
	Diag_I_8 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Acute Inpatient'))
	or
	Diag_I_9 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Acute Inpatient'))
	or
	Diag_I_10 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Acute Inpatient'))
	or
	CPT in(select code from VALUESET_TO_CODE where Value_Set_Name in('Acute Inpatient'))
	or
	CPT2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Acute Inpatient'))
	or
	HCPCS in(select code from VALUESET_TO_CODE where Value_Set_Name in('Acute Inpatient'))
	or 
	Proc_I_1 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Acute Inpatient'))
	or 
	Proc_I_2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Acute Inpatient'))
	or 
	Proc_I_3 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Acute Inpatient'))
	or 
	Proc_I_4 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Acute Inpatient'))
	or 
	Proc_I_5 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Acute Inpatient'))
	)
	)t1
	Join #col_advillness a on a.Memid=t1.MemID and a.claimid=t1.claimid
	)t2
	join #col_frailtymembers f on f.Memid=t2.MemID

	update #coldataset set rexcl=1 from #coldataset ds
	join #col_reqdexcl2 re2 on re2.Memid=ds.patient_id and ds.age>=66;
	
-- Required exclusion 3
	drop table if exists #col_reqdexcl3;
	CREATE table #col_reqdexcl3
	(
		Memid varchar(50)
			
	);

	CREATE CLUSTERED INDEX idx_col_reqdexcl3 ON #col_reqdexcl3 ([memid]);

	insert into #col_reqdexcl3
	select distinct t2.MemId from(
	select t1.Memid,t1.date_s,t1.claimid from(
	select inp.Memid,inp.date_s,inp.claimid from #col_inpatientstaylist inp
	left outer join #col_noncauteinpatientstaylist na on inp.Memid=na.Memid and inp.claimid=na.claimid
	where na.Memid is null
	)t1
	join #col_advillness a on a.Memid=t1.Memid and a.claimid=t1.claimid
	)t2
	join #col_frailtymembers f on f.Memid=t2.Memid

	update #coldataset set rexcl=1 from #coldataset ds
	join #col_reqdexcl3 re3 on re3.Memid=ds.patient_id and ds.age>=66;


-- RequiredExcl 4
	drop table if exists #col_reqdexcl4;
	CREATE table #col_reqdexcl4
			(
				Memid varchar(50)
				
			);

		CREATE CLUSTERED INDEX idx_col_reqdexcl4 ON #col_reqdexcl4 ([memid]);

	insert into #col_reqdexcl4
	select t1.Memid from(
	select Memid from HEDIS_PHARM where Measure_id='COL' and suppdata='N' and PrServDate between @ce_startdt1 and @ce_enddt and NDC in(select code from MEDICATION_LIST_TO_CODES where Medication_List_Name='Dementia Medications')
	)t1
	Join #col_frailtymembers f on f.MemId=t1.Memid;

	update #coldataset set rexcl=1 from #coldataset ds
	join #col_reqdexcl4 re4 on re4.Memid=ds.patient_id and ds.age>=66;


-- Numerator

drop table if exists #col_numerator;
CREATE table #col_numerator
	(
		Memid varchar(50)
			
	);

	CREATE CLUSTERED INDEX idx_col_numerator ON #col_numerator ([memid]);

Insert into #col_numerator
select distinct memid from (
select distinct MemID from HEDIS_VISIT v
	where MEASURE_ID='COL'  and Date_S between @ce_startdt and @ce_enddt and Date_S!=''
	and (
	Diag_I_1 in(select code from VALUESET_TO_CODE where Value_Set_Name in('FOBT Lab Test','FOBT Test Result or Finding'))
	or
	Diag_I_2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('FOBT Lab Test','FOBT Test Result or Finding'))
	or
	Diag_I_3 in(select code from VALUESET_TO_CODE where Value_Set_Name in('FOBT Lab Test','FOBT Test Result or Finding'))
	or
	Diag_I_4 in(select code from VALUESET_TO_CODE where Value_Set_Name in('FOBT Lab Test','FOBT Test Result or Finding'))
	or
	Diag_I_5 in(select code from VALUESET_TO_CODE where Value_Set_Name in('FOBT Lab Test','FOBT Test Result or Finding'))
	or
	Diag_I_6 in(select code from VALUESET_TO_CODE where Value_Set_Name in('FOBT Lab Test','FOBT Test Result or Finding'))
	or
	Diag_I_7 in(select code from VALUESET_TO_CODE where Value_Set_Name in('FOBT Lab Test','FOBT Test Result or Finding'))
	or
	Diag_I_8 in(select code from VALUESET_TO_CODE where Value_Set_Name in('FOBT Lab Test','FOBT Test Result or Finding'))
	or
	Diag_I_9 in(select code from VALUESET_TO_CODE where Value_Set_Name in('FOBT Lab Test','FOBT Test Result or Finding'))
	or
	Diag_I_10 in(select code from VALUESET_TO_CODE where Value_Set_Name in('FOBT Lab Test','FOBT Test Result or Finding'))
	or
	CPT in(select code from VALUESET_TO_CODE where Value_Set_Name in('FOBT Lab Test','FOBT Test Result or Finding'))
	or
	CPT2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('FOBT Lab Test','FOBT Test Result or Finding'))
	or
	HCPCS in(select code from VALUESET_TO_CODE where Value_Set_Name in('FOBT Lab Test','FOBT Test Result or Finding'))
	or 
	Proc_I_1 in(select code from VALUESET_TO_CODE where Value_Set_Name in('FOBT Lab Test','FOBT Test Result or Finding'))
	or 
	Proc_I_2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('FOBT Lab Test','FOBT Test Result or Finding'))
	or 
	Proc_I_3 in(select code from VALUESET_TO_CODE where Value_Set_Name in('FOBT Lab Test','FOBT Test Result or Finding'))
	or 
	Proc_I_4 in(select code from VALUESET_TO_CODE where Value_Set_Name in('FOBT Lab Test','FOBT Test Result or Finding'))
	or 
	Proc_I_5 in(select code from VALUESET_TO_CODE where Value_Set_Name in('FOBT Lab Test','FOBT Test Result or Finding'))
	)

	Union all

	select MemID from HEDIS_LAB where Measure_id='COL' and date_s between @ce_startdt and @ce_enddt and date_s!='' and (LOINC in(select code from VALUESET_TO_CODE where Value_Set_Name in('FOBT Lab Test','FOBT Test Result or Finding')) or CPT_Code IN(select code from VALUESET_TO_CODE where Value_Set_Name in('FOBT Lab Test','FOBT Test Result or Finding')))

	Union all

	select MemId from HEDIS_OBS where Measure_id='COL' and date between @ce_startdt and @ce_enddt and date!='' and OCode IN(select code from VALUESET_TO_CODE where Value_Set_Name in('FOBT Lab Test','FOBT Test Result or Finding')) 

	Union all

	select MemId from HEDIS_DIAG where Measure_id='COL' and sdate between @ce_dt5 and @ce_enddt and sdate!='' and  dcode IN(select code from VALUESET_TO_CODE where Value_Set_Name in('Flexible Sigmoidoscopy','History of Flexible Sigmoidoscopy')) 

	Union all

select MemID from HEDIS_PROC where Measure_id='COL' and pstatus='EVN' and sdate between @ce_dt5 and @ce_enddt and sdate!='' and  pcode IN(select code from VALUESET_TO_CODE where Value_Set_Name in('Flexible Sigmoidoscopy','History of Flexible Sigmoidoscopy')) 

Union all

select distinct MemID from HEDIS_VISIT v
	where MEASURE_ID='COL'  and Date_S between @ce_dt5 and @ce_enddt and Date_S!='' and HCFAPOS!=81
	and (
	Diag_I_1 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Flexible Sigmoidoscopy','History of Flexible Sigmoidoscopy'))
	or
	Diag_I_2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Flexible Sigmoidoscopy','History of Flexible Sigmoidoscopy'))
	or
	Diag_I_3 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Flexible Sigmoidoscopy','History of Flexible Sigmoidoscopy'))
	or
	Diag_I_4 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Flexible Sigmoidoscopy','History of Flexible Sigmoidoscopy'))
	or
	Diag_I_5 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Flexible Sigmoidoscopy','History of Flexible Sigmoidoscopy'))
	or
	Diag_I_6 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Flexible Sigmoidoscopy','History of Flexible Sigmoidoscopy'))
	or
	Diag_I_7 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Flexible Sigmoidoscopy','History of Flexible Sigmoidoscopy'))
	or
	Diag_I_8 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Flexible Sigmoidoscopy','History of Flexible Sigmoidoscopy'))
	or
	Diag_I_9 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Flexible Sigmoidoscopy','History of Flexible Sigmoidoscopy'))
	or
	Diag_I_10 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Flexible Sigmoidoscopy','History of Flexible Sigmoidoscopy'))
	or
	CPT in(select code from VALUESET_TO_CODE where Value_Set_Name in('Flexible Sigmoidoscopy','History of Flexible Sigmoidoscopy'))
	or
	CPT2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Flexible Sigmoidoscopy','History of Flexible Sigmoidoscopy'))
	or
	HCPCS in(select code from VALUESET_TO_CODE where Value_Set_Name in('Flexible Sigmoidoscopy','History of Flexible Sigmoidoscopy'))
	or 
	Proc_I_1 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Flexible Sigmoidoscopy','History of Flexible Sigmoidoscopy'))
	or 
	Proc_I_2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Flexible Sigmoidoscopy','History of Flexible Sigmoidoscopy'))
	or 
	Proc_I_3 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Flexible Sigmoidoscopy','History of Flexible Sigmoidoscopy'))
	or 
	Proc_I_4 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Flexible Sigmoidoscopy','History of Flexible Sigmoidoscopy'))
	or 
	Proc_I_5 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Flexible Sigmoidoscopy','History of Flexible Sigmoidoscopy'))
	)

Union all
-- Colonoscopy

select MemId from HEDIS_DIAG where Measure_id='COL' and sdate between @ce_dt10 and @ce_enddt and sdate!='' and  dcode IN(select code from VALUESET_TO_CODE where Value_Set_Name in('Colonoscopy','History of Colonoscopy')) 

Union All

select MemId from HEDIS_PROC where Measure_id='COL' and pstatus='EVN' and sdate between @ce_dt10 and @ce_enddt and sdate!='' and  pcode IN(select code from VALUESET_TO_CODE where Value_Set_Name in('Colonoscopy','History of Colonoscopy')) 

Union all

select distinct MemID from HEDIS_VISIT v
	where MEASURE_ID='COL'  and Date_S between @ce_dt10 and @ce_enddt and Date_S!='' and HCFAPOS!=81
	and (
	Diag_I_1 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Colonoscopy','History of Colonoscopy'))
	or
	Diag_I_2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Colonoscopy','History of Colonoscopy'))
	or
	Diag_I_3 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Colonoscopy','History of Colonoscopy'))
	or
	Diag_I_4 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Colonoscopy','History of Colonoscopy'))
	or
	Diag_I_5 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Colonoscopy','History of Colonoscopy'))
	or
	Diag_I_6 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Colonoscopy','History of Colonoscopy'))
	or
	Diag_I_7 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Colonoscopy','History of Colonoscopy'))
	or
	Diag_I_8 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Colonoscopy','History of Colonoscopy'))
	or
	Diag_I_9 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Colonoscopy','History of Colonoscopy'))
	or
	Diag_I_10 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Colonoscopy','History of Colonoscopy'))
	or
	CPT in(select code from VALUESET_TO_CODE where Value_Set_Name in('Colonoscopy','History of Colonoscopy'))
	or
	CPT2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Colonoscopy','History of Colonoscopy'))
	or
	HCPCS in(select code from VALUESET_TO_CODE where Value_Set_Name in('Colonoscopy','History of Colonoscopy'))
	or 
	Proc_I_1 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Colonoscopy','History of Colonoscopy'))
	or 
	Proc_I_2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Colonoscopy','History of Colonoscopy'))
	or 
	Proc_I_3 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Colonoscopy','History of Colonoscopy'))
	or 
	Proc_I_4 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Colonoscopy','History of Colonoscopy'))
	or 
	Proc_I_5 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Colonoscopy','History of Colonoscopy'))
	)

Union all
-- CT Colonograpgy

select MemId from HEDIS_PROC where Measure_id='COL' and pstatus='EVN' and sdate between @ce_dt5 and @ce_enddt and sdate!='' and  pcode IN(select code from VALUESET_TO_CODE where Value_Set_Name in('CT Colonography'))

Union all

select distinct MemID from HEDIS_VISIT v
	where MEASURE_ID='COL'  and Date_S between @ce_dt5 and @ce_enddt and Date_S!=''
	and (
	Diag_I_1 in(select code from VALUESET_TO_CODE where Value_Set_Name in('CT Colonography'))
	or
	Diag_I_2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('CT Colonography'))
	or
	Diag_I_3 in(select code from VALUESET_TO_CODE where Value_Set_Name in('CT Colonography'))
	or
	Diag_I_4 in(select code from VALUESET_TO_CODE where Value_Set_Name in('CT Colonography'))
	or
	Diag_I_5 in(select code from VALUESET_TO_CODE where Value_Set_Name in('CT Colonography'))
	or
	Diag_I_6 in(select code from VALUESET_TO_CODE where Value_Set_Name in('CT Colonography'))
	or
	Diag_I_7 in(select code from VALUESET_TO_CODE where Value_Set_Name in('CT Colonography'))
	or
	Diag_I_8 in(select code from VALUESET_TO_CODE where Value_Set_Name in('CT Colonography'))
	or
	Diag_I_9 in(select code from VALUESET_TO_CODE where Value_Set_Name in('CT Colonography'))
	or
	Diag_I_10 in(select code from VALUESET_TO_CODE where Value_Set_Name in('CT Colonography'))
	or
	CPT in(select code from VALUESET_TO_CODE where Value_Set_Name in('CT Colonography'))
	or
	CPT2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('CT Colonography'))
	or
	HCPCS in(select code from VALUESET_TO_CODE where Value_Set_Name in('CT Colonography'))
	or 
	Proc_I_1 in(select code from VALUESET_TO_CODE where Value_Set_Name in('CT Colonography'))
	or 
	Proc_I_2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('CT Colonography'))
	or 
	Proc_I_3 in(select code from VALUESET_TO_CODE where Value_Set_Name in('CT Colonography'))
	or 
	Proc_I_4 in(select code from VALUESET_TO_CODE where Value_Set_Name in('CT Colonography'))
	or 
	Proc_I_5 in(select code from VALUESET_TO_CODE where Value_Set_Name in('CT Colonography'))
	)

Union all
-- FIT

select MemID from HEDIS_LAB where Measure_id='COL' and date_s between @ce_dt3 and @ce_enddt and date_s!='' and  (CPT_Code IN(select code from VALUESET_TO_CODE where Value_Set_Name in('FIT DNA Lab Test','FIT DNA Test Result or Finding')) or LOINC in (select code from VALUESET_TO_CODE where Value_Set_Name in('FIT DNA Lab Test','FIT DNA Test Result or Finding')))

Union all

select Memid from HEDIS_OBS where Measure_id='COL' and date between @ce_dt3 and @ce_enddt and date!=''  and OCode IN(select code from VALUESET_TO_CODE where Value_Set_Name in('FIT DNA Lab Test','FIT DNA Test Result or Finding'))

Union all

Select Memid from HEDIS_VISIT_E where Measure_id='COL' and sdate between @ce_dt3 and @ce_enddt and sdate!='' and activity IN(select code from VALUESET_TO_CODE where Value_Set_Name in('FIT DNA Lab Test','FIT DNA Test Result or Finding'))

Union all

select distinct MemID from HEDIS_VISIT v
	where MEASURE_ID='COL'  and Date_S between @ce_dt3 and @ce_enddt and Date_S!=''
	and (
	Diag_I_1 in(select code from VALUESET_TO_CODE where Value_Set_Name in('FIT DNA Lab Test','FIT DNA Test Result or Finding'))
	or
	Diag_I_2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('FIT DNA Lab Test','FIT DNA Test Result or Finding'))
	or
	Diag_I_3 in(select code from VALUESET_TO_CODE where Value_Set_Name in('FIT DNA Lab Test','FIT DNA Test Result or Finding'))
	or
	Diag_I_4 in(select code from VALUESET_TO_CODE where Value_Set_Name in('FIT DNA Lab Test','FIT DNA Test Result or Finding'))
	or
	Diag_I_5 in(select code from VALUESET_TO_CODE where Value_Set_Name in('FIT DNA Lab Test','FIT DNA Test Result or Finding'))
	or
	Diag_I_6 in(select code from VALUESET_TO_CODE where Value_Set_Name in('FIT DNA Lab Test','FIT DNA Test Result or Finding'))
	or
	Diag_I_7 in(select code from VALUESET_TO_CODE where Value_Set_Name in('FIT DNA Lab Test','FIT DNA Test Result or Finding'))
	or
	Diag_I_8 in(select code from VALUESET_TO_CODE where Value_Set_Name in('FIT DNA Lab Test','FIT DNA Test Result or Finding'))
	or
	Diag_I_9 in(select code from VALUESET_TO_CODE where Value_Set_Name in('FIT DNA Lab Test','FIT DNA Test Result or Finding'))
	or
	Diag_I_10 in(select code from VALUESET_TO_CODE where Value_Set_Name in('FIT DNA Lab Test','FIT DNA Test Result or Finding'))
	or
	CPT in(select code from VALUESET_TO_CODE where Value_Set_Name in('FIT DNA Lab Test','FIT DNA Test Result or Finding'))
	or
	CPT2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('FIT DNA Lab Test','FIT DNA Test Result or Finding'))
	or
	HCPCS in(select code from VALUESET_TO_CODE where Value_Set_Name in('FIT DNA Lab Test','FIT DNA Test Result or Finding'))
	or 
	Proc_I_1 in(select code from VALUESET_TO_CODE where Value_Set_Name in('FIT DNA Lab Test','FIT DNA Test Result or Finding'))
	or 
	Proc_I_2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('FIT DNA Lab Test','FIT DNA Test Result or Finding'))
	or 
	Proc_I_3 in(select code from VALUESET_TO_CODE where Value_Set_Name in('FIT DNA Lab Test','FIT DNA Test Result or Finding'))
	or 
	Proc_I_4 in(select code from VALUESET_TO_CODE where Value_Set_Name in('FIT DNA Lab Test','FIT DNA Test Result or Finding'))
	or 
	Proc_I_5 in(select code from VALUESET_TO_CODE where Value_Set_Name in('FIT DNA Lab Test','FIT DNA Test Result or Finding'))
	)


)t1


update #coldataset set num=1 from #coldataset ds
	join #col_numerator num on num.Memid=ds.patient_id;


-- Optional Exclusion
drop table if exists #col_optexcl;
CREATE table #col_optexcl
	(
		Memid varchar(50)
			
	);

	CREATE CLUSTERED INDEX idx_col_optexcl ON #col_optexcl ([memid]);

Insert into #col_optexcl
select distinct Memid from (
select MemId from HEDIS_DIAG where Measure_id='COL'  and sdate!='' and sdate<=@ce_enddt
and  dcode IN(select code from VALUESET_TO_CODE where Value_Set_Name in('Colorectal Cancer','History of Total Colectomy','Total Colectomy'))

Union all

select MemId from HEDIS_PROC where Measure_id='COL' and sdate!='' and sdate<=@ce_enddt and pstatus='EVN'  and  pcode IN(select code from VALUESET_TO_CODE where Value_Set_Name in('Colorectal Cancer','History of Total Colectomy','Total Colectomy'))

union all

select distinct MemID from HEDIS_VISIT v
	where MEASURE_ID='COL'  and hcfapos!=81 and Date_S!='' and Date_S<=@ce_enddt
	and (
	Diag_I_1 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Colorectal Cancer','History of Total Colectomy','Total Colectomy'))
	or
	Diag_I_2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Colorectal Cancer','History of Total Colectomy','Total Colectomy'))
	or
	Diag_I_3 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Colorectal Cancer','History of Total Colectomy','Total Colectomy'))
	or
	Diag_I_4 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Colorectal Cancer','History of Total Colectomy','Total Colectomy'))
	or
	Diag_I_5 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Colorectal Cancer','History of Total Colectomy','Total Colectomy'))
	or
	Diag_I_6 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Colorectal Cancer','History of Total Colectomy','Total Colectomy'))
	or
	Diag_I_7 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Colorectal Cancer','History of Total Colectomy','Total Colectomy'))
	or
	Diag_I_8 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Colorectal Cancer','History of Total Colectomy','Total Colectomy'))
	or
	Diag_I_9 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Colorectal Cancer','History of Total Colectomy','Total Colectomy'))
	or
	Diag_I_10 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Colorectal Cancer','History of Total Colectomy','Total Colectomy'))
	or
	CPT in(select code from VALUESET_TO_CODE where Value_Set_Name in('Colorectal Cancer','History of Total Colectomy','Total Colectomy'))
	or
	CPT2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Colorectal Cancer','History of Total Colectomy','Total Colectomy'))
	or
	HCPCS in(select code from VALUESET_TO_CODE where Value_Set_Name in('Colorectal Cancer','History of Total Colectomy','Total Colectomy'))
	or 
	Proc_I_1 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Colorectal Cancer','History of Total Colectomy','Total Colectomy'))
	or 
	Proc_I_2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Colorectal Cancer','History of Total Colectomy','Total Colectomy'))
	or 
	Proc_I_3 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Colorectal Cancer','History of Total Colectomy','Total Colectomy'))
	or 
	Proc_I_4 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Colorectal Cancer','History of Total Colectomy','Total Colectomy'))
	or 
	Proc_I_5 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Colorectal Cancer','History of Total Colectomy','Total Colectomy'))
	)

)t1

update #coldataset set excl=1 from #coldataset ds
	join #col_optexcl oexcl on oexcl.Memid=ds.patient_id;



--SES Startification

update #coldataset set lis=1 from #coldataset ds join HEDIS_MMDF m on m.beneficiary_id=ds.patient_id and m.Measure_id='COL'
where m.run_date between @ce_startdt1 and @ce_enddt and m.LIS_Premium_Subsidy>0 and ds.payer IN('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP');;

update #coldataset set lis=1 from #coldataset ds join HEDIS_LISHIST l on l.beneficiary_id=ds.patient_id and l.Measure_id='COL'
where ds.payer IN('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP') and ((Low_Income_Period_End_Date>=@ce_startdt1 and Low_Income_Period_Start_Date<=@ce_enddt) or Low_Income_Period_End_Date='');


-- Item 48
drop table if exists #col_orec;
CREATE table #col_orec
	(
		Memid varchar(50),
		orec varchar(2)
			
	);

	CREATE CLUSTERED INDEX idx_col_orec ON #col_orec ([memid],[orec]);

Insert into #col_orec
select m1.Beneficiary_ID,m1.Original_Reason_for_Entitlement_Code_OREC as orec from HEDIS_MMDF m1 where m1.Measure_id='COL'  and m1.Run_Date=(select max(m2.Run_Date) from HEDIS_MMDF m2 where m2.MEASURE_ID='COL'  and m1.Beneficiary_ID=m2.Beneficiary_ID and m2.Run_Date between @ce_startdt1 and @ce_enddt and m2.Original_Reason_for_Entitlement_Code_OREC!='') order by m1.Beneficiary_ID


update #coldataset set orec=o.orec from #coldataset ds
join #col_orec o on o.Memid=ds.patient_id
where ds.payer IN('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP');

update #coldataset set meas='COLNON' WHERE orec=0 AND lis=0 AND ce=1 AND rexcl=0 and rexcld=0 AND payer IN ('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP');


UPDATE #coldataset SET meas='COLLISDE' WHERE orec=0 AND lis=1 AND ce=1 AND rexcl=0 and rexcld=0 AND payer IN ('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP');

UPDATE #coldataset SET meas='COLDIS' WHERE orec IN(1,3) AND lis=0 AND ce=1 AND rexcl=0 and rexcld=0 AND payer IN ('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP');

UPDATE #coldataset SET meas='COLCMB' WHERE orec IN(1,3) AND lis=1 AND ce=1 AND rexcl=0 and rexcld=0 AND payer IN ('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP');


UPDATE #coldataset SET meas='COLOT' WHERE orec IN(2,9) AND ce=1 AND rexcl=0 and rexcld=0 AND payer IN ('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP');

	
	select @runid=max(RUN_ID) from HEDIS_MEASURE_OUTPUT where measure_id='COL';

	if(@runid>=1)
	Begin
		SET @runid=@runid+1;
	End
	Else
	Begin
		SET @runid=1;
	END
	

	Insert into HEDIS_MEASURE_OUTPUT(memid,Meas,payer,CE,Event,Epop,Excl,Num,Rexcl,RExclD,Age,Gender,Measure_ID,Measurement_year,RUN_ID)
	SELECT patient_id AS memid,meas,payer,CE,0 as EVENT,CASE WHEN CE=1  AND rexcl=0 and rexcld=0 and payer!='MCD' THEN 1 ELSE 0 END AS epop,excl,num,rexcl,rexcld,cast(age as Int) AS age,patient_gender AS gender,'COL' as Measure_ID,@meas_year,@runid FROM #coldataset


GO
