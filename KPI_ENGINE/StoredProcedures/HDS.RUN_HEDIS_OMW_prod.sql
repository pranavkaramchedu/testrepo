SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
CREATE PROCEDURE [HDS].[RUN_HEDIS_OMW_prod]  @meas_year nvarchar(4)
AS
-- Declare Variables

Declare @runid INT=0;
DECLARE @gender VARCHAR(20);
DECLARE @age VARCHAR(10);
DECLARE @ce_startdt varchar(10);
DECLARE @ce_Intakestartdt varchar(10);
DECLARE @ce_enddt varchar(10);
DECLARE @ce_Intakeenddt varchar(10);
DECLARE @ce_startdt1 varchar(10);
DECLARE @ce_enddt1 varchar(10);
DECLARE @ce_startdt2 varchar(10);
DECLARE @ce_enddt2 varchar(10);
DECLARE @meas VARCHAR(10);


SET @meas='159';--'OMW';
SET @ce_startdt=concat(@meas_year,'-01-01');
SET @ce_Intakestartdt=concat(@meas_year-1,'-07-01');
SET @ce_enddt=concat(@meas_year,'-12-31');
SET @ce_Intakeenddt=concat(@meas_year,'-06-30');
SET @ce_startdt1=concat(@meas_year-1,'-01-01');
SET @ce_enddt1=concat(@meas_year-1,'-12-31');


-- Create Population Set
drop table if exists #omw_popset;
Create table #omw_popset
(
	Memid varchar(100),
	Gender varchar(10),
	Age varchar(10)
)
Insert into #omw_popset
select Memid,Gender,Year(@ce_enddt)-Year(dob) as Age from HDS.HEDIS_MEMBER_GM where Measure_id=@meas
and Year(@ce_enddt)-Year(dob) between 67 and 85 and Gender='F'

-- Create Fracture Set


Drop table if exists #omw_fractureset;
Create table #omw_fractureset
(
	Memid varchar(100),
	Date_S varchar(10),
	Date_Adm varchar(10),
	Date_Disch varchar(10),
	ClaimId varchar(100)
)
;With CTE_FractureDiagnosisSet as
(

	select Memid,Date_S,Date_Adm,Date_Disch,ClaimId from HDS.HEDIS_VISIT where Measure_id=@meas and Date_S!='' and suppdata='N' and HCFAPOS!='81'
	and 
		(
			CPT in(select code_new from HDS.Valueset_to_code where value_set_Name in('Fractures'))
			or
			HCPCS in(select code_new from HDS.Valueset_to_code where value_set_Name in('Fractures'))
			or
			Diag_i_1 in(select code_new from HDS.Valueset_to_code where value_set_Name in('Fractures'))
			or
			Diag_i_2 in(select code_new from HDS.Valueset_to_code where value_set_Name in('Fractures'))
			or
			Diag_i_3 in(select code_new from HDS.Valueset_to_code where value_set_Name in('Fractures'))
			or
			Diag_i_4 in(select code_new from HDS.Valueset_to_code where value_set_Name in('Fractures'))
			or
			Diag_i_5 in(select code_new from HDS.Valueset_to_code where value_set_Name in('Fractures'))
			or
			Diag_i_6 in(select code_new from HDS.Valueset_to_code where value_set_Name in('Fractures'))
			or
			Diag_i_7 in(select code_new from HDS.Valueset_to_code where value_set_Name in('Fractures'))
			or
			Diag_i_8 in(select code_new from HDS.Valueset_to_code where value_set_Name in('Fractures'))
			or
			Diag_i_9 in(select code_new from HDS.Valueset_to_code where value_set_Name in('Fractures'))
			or
			Diag_i_10 in(select code_new from HDS.Valueset_to_code where value_set_Name in('Fractures'))
	
		)
)
Insert into #omw_fractureset
Select t1.* from CTE_FractureDiagnosisSet t1
join #omw_popset p on t1.MemId=p.Memid


-- Create Inpatient Set


Drop table if exists #omw_InpatientStaySet;
Create table #omw_InpatientStaySet
(
	Memid varchar(100),
	Date_S varchar(10),
	Date_Adm varchar(10),
	Date_Disch varchar(10),
	ClaimId varchar(100)
)
Insert into #omw_InpatientStaySet
select v.Memid,Date_S,Date_Adm,Date_Disch,ClaimId from HDS.HEDIS_VISIT v
join #omw_popset p on v.Memid=p.Memid
where Measure_id=@meas and Date_Disch!='' and suppdata='N' and HCFAPOS!='81' and REV in(select code_new from HDS.Valueset_to_code where code_system='UBREV' and value_set_Name in('Inpatient Stay'))


-- Create Ambulatory Visit Set


Drop table if exists #omw_Ambulatoryvstset;
Create table #omw_Ambulatoryvstset
(
	Memid varchar(100),
	Date_S varchar(10),
	Date_Adm varchar(10),
	Date_Disch varchar(10),
	ClaimId varchar(100)
)
;With CTE_Ambulatory as
(
	select Memid,Date_S,Date_Adm,Date_Disch,ClaimId from HDS.HEDIS_VISIT where Measure_id=@meas and Date_S!='' and suppdata='N' and HCFAPOS not in('81','02') and 
	(
		CPT in(select code_new from HDS.Valueset_to_code where value_set_Name in('Observation','Outpatient','ED'))
		or
		HCPCS in(select code_new from HDS.Valueset_to_code where value_set_Name in('Outpatient'))
		or
		REV in(select code_new from HDS.Valueset_to_code where code_system='UBREV' and value_set_Name in('Outpatient','ED'))
	)
	and 
	(
		CPTMod_1 not in(select code_new from HDS.Valueset_to_code where value_set_Name='Telehealth Modifier')
		and
		CPTMod_2 not in(select code_new from HDS.Valueset_to_code where value_set_Name='Telehealth Modifier')
	)
)
Insert into #omw_Ambulatoryvstset
Select t1.* from CTE_Ambulatory t1
join #omw_fractureset f on t1.Memid=f.Memid and t1.ClaimId=f.ClaimId


-- Exclude Ambulatory Visits which Leads to Inpatient  Stay


Drop table if exists #omw_AmbulatoryvstWExcl;
Create table #omw_AmbulatoryvstWExcl
(
	Memid varchar(100),
	Date_S varchar(10),
	Date_Adm varchar(10),
	Date_Disch varchar(10),
	ClaimId varchar(100)
)
Insert into #omw_AmbulatoryvstWExcl
Select t1.* from #omw_Ambulatoryvstset t1
left outer join #omw_InpatientStaySet i on t1.Memid=i.Memid and (datediff(day,t1.Date_S,i.Date_Adm) between 0 and 1 or t1.Date_S between i.Date_Adm and i.Date_Disch or t1.ClaimID=i.ClaimId)
where i.Memid is null 



-- Identify Direct Transfers


drop table if exists #omw_directtransfers;
Create table #omw_directtransfers
(
	Memid varchar(100),
	AdmissionDate varchar(10),
	DischargeDate varchar(10),
	AdmissionClaimId varchar(100),
	DischargeClaimId varchar(100),
	grp_claimid varchar(100)
)

;with grp_starts as (
  select Memid,Date_Adm, Date_Disch,ClaimId,
  case
    when datediff(day,lag(Date_Disch) over(partition by Memid order by Date_Adm, Date_Disch),Date_Adm) <=1
	or (datediff(day,lag(Date_Disch,2) over(partition by Memid order by Date_Adm, Date_Disch),Date_Adm)<=1 )
    then 0 else 1
  end grp_start
  from #omw_InpatientStaySet
  --where memid=161600

)
, grps as (
  select Memid,Date_Adm, Date_Disch,ClaimId
  ,sum(grp_start) over(partition by Memid order by Date_Adm, Date_Disch) grp
  from grp_starts
)
Insert into #omw_directtransfers
select t3.Memid,case when Date_S is not null then Date_S else t3.AdmissionDate end as AdmissionDate,t3.DischargeDate,AdmissionClaimid,DischargeClaimId,grp_claimid from(
Select t2.*,v.Date_S from(
select t1.* from(
select Memid,min(Date_Adm) as AdmissionDate,max(Date_Disch) as DischargeDate,min(ClaimId) as AdmissionClaimId,max(ClaimId) as DischargeClaimId,STRING_AGG(ClaimId,',') as grp_claimid
from grps 
group by Memid,grp
)t1
	join #omw_fractureset f on t1.Memid=f.Memid and (',' + RTRIM(t1.grp_claimid) + ',') LIKE '%,' + cast(f.ClaimId as varchar) + ',%'
)t2
	left outer Join #omw_Ambulatoryvstset v on t2.Memid=v.Memid and (DATEDIFF(day,v.Date_S,t2.AdmissionDate) between 0 and 1 or (',' + RTRIM(t2.grp_claimid) + ',') LIKE '%,' + cast(v.ClaimId as varchar) + ',%')
)t3


-- Compiling Episode Dates for Members from Ambulatory and Inpatient Stays


Drop Table if Exists #omw_episodedates;
Create table #omw_episodedates
(
	Memid varchar(100),
	StartDate varchar(10),
	EpisodeDate varchar(10),
	IESD varchar(8)
)

;with CTE_Episodes as (
	select Memid,Date_S as StartDate
	,Date_S as EpisodeDate -- Before change
	--,case when Date_Disch!='' then Date_Disch else Date_S end as EpisodeDate 
	from #omw_AmbulatoryvstWExcl
	Union All
	Select Memid,AdmissionDate as StartDate,DischargeDate as EpisodeDate from #omw_directtransfers
)
,CTE_IESD as
(
	select Memid,min(EpisodeDate) as IESD from CTE_Episodes where EpisodeDate between @ce_Intakestartdt and @ce_Intakeenddt group by Memid
)
Insert into #omw_episodedates
select t1.*,t2.IESD from CTE_Episodes t1
join CTE_IESD t2 on t1.Memid=t2.Memid
where t1.EpisodeDate between @ce_Intakestartdt and @ce_Intakeenddt


--Negative Diagnosis History

Drop table if exists #omw_NegDiagHistVstListwoexcl;
Create table #omw_NegDiagHistVstListwoexcl
(
	Memid varchar(100),
	Date_S varchar(10),
	Date_Adm varchar(10),
	Date_Disch varchar(10),
	ClaimId varchar(100)
)

;With CTE_AmbulatoryExcl as
(
	select Memid,Date_S,Date_Adm,Date_Disch,ClaimId from HDS.HEDIS_VISIT where Measure_id=@meas and Date_S!='' and suppdata='N' and HCFAPOS!='81' and 
	(
		CPT in(select code_new from HDS.Valueset_to_code where value_set_Name in('Observation','Outpatient','ED','Telephone Visits','Online Assessments'))
		or
		HCPCS in(select code_new from HDS.Valueset_to_code where value_set_Name in('Outpatient','Online Assessments'))
		or
		REV in(select code_new from HDS.Valueset_to_code where code_system='UBREV' and value_set_Name in('Outpatient','ED'))
	)
	
)
Insert into #omw_NegDiagHistVstListwoexcl
Select t1.* from CTE_AmbulatoryExcl t1
join #omw_fractureset f on t1.MemID=f.Memid and t1.ClaimID=f.ClaimId

-- Negative Disgnosis History Visit List

Drop table if exists #omw_NegDiagHistVstList;
Create table #omw_NegDiagHistVstList
(
	Memid varchar(100),
	Date_S varchar(10),
	Date_Adm varchar(10),
	Date_Disch varchar(10),
	ClaimId varchar(100)
)
Insert into #omw_NegDiagHistVstList
Select t1.* from #omw_NegDiagHistVstListwoexcl t1
left outer join #omw_InpatientStaySet i on t1.Memid=i.Memid and datediff(day,t1.Date_S,i.Date_Adm) between 0 and 1
where i.Memid is null


-- Exclude Records with Positive Diagnosis History


Drop table if exists #omw_EpisodeDatesWNMH;
Create table #omw_EpisodeDatesWNMH
(
	Memid varchar(100),
	StartDate varchar(10),
	EpisodeDate varchar(10),
	IESD varchar(10)
)
Insert into #omw_EpisodeDatesWNMH
select distinct e.* from #omw_episodedates e
left outer join #omw_NegDiagHistVstList v on e.Memid=v.Memid and v.Date_S between DATEADD(day,-60,e.StartDate) and DateAdd(day,-1,e.StartDate) and e.EpisodeDate!=v.Date_Disch
left outer join #omw_directtransfers s on e.Memid=s.Memid and s.DischargeDate between DATEADD(day,-60,e.StartDate) and DateAdd(day,-1,e.StartDate)
where v.Memid is null and s.Memid is null


-- Adding Payer Mapping


Drop table if exists #omw_payermapping;
Create table #omw_payermapping
(
	Memid varchar(100),
	Payer varchar(10),
	
)	


;With mappedpayers as
(
	
	select f1.memid,Payer
	from (
	select b.*,case when en.Payer is null then en1.Payer else en.Payer end as Payer
	from #omw_episodedatesWNMH b
	left outer join hds.hedis_member_en en on b.Memid=en.Memid and en.Measure_id=@meas and @ce_enddt between en.StartDate and en.FinishDate
	left outer join
	( 
		select * from(
		select *,row_number() over(partition by Memid,EpisodeDate order by Finishdate desc) as rn from(
		select b.*,en.Payer,en.Finishdate from #omw_episodedatesWNMH b
		join HDS.HEDIS_MEMBER_EN en on b.memid=en.memid 
		and en.StartDate<=dateadd(day,180,b.EpisodeDate) and en.FinishDate>=dateadd(Month,-12,b.EpisodeDate)
		and en.Measure_id=@meas
		
		)t1 
		)t2
		where rn=1 
	)en1 on b.Memid=en1.Memid 
	          
	)f1 
--	where memid=123138          
	
	
	
)

Insert into #omw_payermapping(Memid,Payer)
select distinct t4.Memid,m.PayerMapping as Payer from(
select 	
	t3.Memid
	,newpayer as Payer
from(
	select t2.*,
	-- Applying Dual enrollment for Payer field
		case 
			when (Payer in('HMO','CEP','POS','PPO') and nxtpayer in('MD','MLI','MRB')) then Payer
			When (Payer in('HMO','CEP','POS','PPO') and prvpayer in('MD','MLI','MRB')) then Payer
			When (Payer in('MD','MLI','MRB') and nxtpayer in('HMO','CEP','POS','PPO')) then nxtpayer
			When (Payer in('MD','MLI','MRB') and prvpayer in('HMO','CEP','POS','PPO')) then prvpayer
			when (Payer in('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP') and nxtpayer in('HMO','CEP','POS','PPO')) then Payer
			When (Payer in('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP') and prvpayer in('HMO','CEP','POS','PPO'))  then Payer
			When (Payer in('HMO','CEP','POS','PPO') and nxtpayer in('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP')) then nxtpayer
			When (Payer in('HMO','CEP','POS','PPO') and prvpayer in('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP')) then prvpayer
			else Payer 
		end as newpayer
		from(
			select 
				m1.*
				,isnull(lead(Payer,1) over(partition by Memid order by Memid),Payer) as nxtpayer
				,isnull(lag(Payer,1) over(partition by Memid order by Memid),Payer) as prvpayer
				
			from mappedpayers m1
		)t2
	)t3 
)t4 
join 
HDS.Hedis_payer_Mapping m on t4.Payer=m.payer and m.Measure_ID=@meas


-- Create OMW Dataset


drop table if exists #omwdataset;

CREATE TABLE #omwdataset (
  [MemID] varchar(100) NOT NULL,
  [Meas] varchar(20) DEFAULT NULL,
  [Payer] varchar(100) DEFAULT NULL,
  [Gender] varchar(45) NOT NULL,
  [Age] INT,
  [Rexcl] smallint DEFAULT '0',
  [Rexcld] smallint DEFAULT '0',
  [CE] smallint DEFAULT '0',
  [Excl] smallint DEFAULT '0',
  [Num] smallint DEFAULT '0',
  [Event] smallint DEFAULT '1'
  
  
) ;


Insert into #omwdataset(Memid,Meas,Payer,Age,Gender)
select m.Memid,'OMW' as Meas,m.Payer,p.Age,p.Gender from #omw_payermapping m
join #omw_popset p on m.Memid=p.Memid



---- Continuous Enrollment



DROP TABLE IF EXISTS #omw_contenroll;
	
CREATE table #omw_contenroll
(
	Memid varchar(100),
	EpisodeDate varchar(10)
		
);
	

With coverage_CTE(Memid,lastcoveragedate,Startdate,Finishdate,nextcoveragedate,EpisodeDate) as
(
	
	select 
		ed.Memid
		,isnull(lag(en.Finishdate,1) over(partition by ed.Memid order by en.Startdate,en.FinishDate desc),convert(varchar,cast(dateadd(Month,-12,ed.EpisodeDate) as date),112)) as lastcoveragedate
		,Case 
			when en.StartDate<dateadd(MONTH,-12,ed.EpisodeDate) then convert(varchar,cast(dateadd(MONTH,-12,ed.EpisodeDate) as date),112)
			else en.StartDate 
		end as Startdate
		,case 
			when en.Finishdate>dateadd(day,180,ed.EpisodeDate) then convert(varchar,cast(dateadd(day,180,ed.EpisodeDate) as date),112)
			else finishdate 
		end as Finishdate
		,isnull(lead(en.Startdate,1) over(partition by ed.Memid order by en.Startdate,en.FinishDate),convert(varchar,cast(dateadd(day,180,ed.EpisodeDate) as date),112)) as nextcoveragedate
		,ed.EpisodeDate 
		 
		from #omw_EpisodeDatesWNMH ed
		join HDS.HEDIS_MEMBER_EN en on ed.MemId=en.MemID and en.Measure_id=@meas and en.drug='Y' and en.Startdate<=dateadd(day,180,ed.EpisodeDate) and en.Finishdate>=dateadd(MONTH,-12,ed.EpisodeDate)
	--where ed.memid=100454
)
Insert into #omw_contenroll
select Memid,EpisodeDate from(
	select 
		*
		,case 
			when rn=1 and startdate>dateadd(MONTH,-12,EpisodeDate) then 1 
			else 0 
		end as startgap
		,case 
			when datediff(day,newFinishdate,nextcoveragedate)>0 then 1  
			else 0 
		end as gaps
		,datediff(day,Startdate,finishdate) as coveragedays
		,case 
			when EpisodeDate between Startdate and newfinishdate then 1 
			else 0 
		end as anchor 
		from(
			
			Select 
				*
				,case 
					when datediff(day,Finishdate,nextcoveragedate) <=1 then isnull(lead(Finishdate,1) over(partition by Memid order by Startdate,FinishDate),convert(varchar,cast(dateadd(day,180,EpisodeDate) as date),112)) 
					else finishdate 
				end as newfinishdate
				,ROW_NUMBER() over(partition by memid order by Startdate,Finishdate) as rn 
			from coverage_CTE
		)t1                        
	)t2 
group by memid,EpisodeDate having(sum(gaps)+sum(startgap)<=1) and sum(coveragedays)>=(datediff(day,dateadd(Month,-12,EpisodeDate),dateadd(day,180,EpisodeDate))-45) and sum(anchor)>0


update #omwdataset set CE=1 from #omwdataset ds join #omw_contenroll ce on ds.MemID=ce.Memid



-- Required Exclusions

-- Bone Mineral Density Tests


Drop table if Exists #omw_BMDTSet
Create Table #omw_BMDTSet
(
	Memid Varchar(100),
	EpisodeDate varchar(10)
)
;With CTE_BMDT as
(
	Select distinct Memid,servicedate from(
	select Memid,Date_S as servicedate from HDS.HEDIS_VISIT where Measure_id=@meas and Date_S!='' and HCFAPOS!='81' and
	(
		CPT in(select code_new from HDS.Valueset_to_code where value_set_Name='Bone Mineral Density Tests')
		or
		Proc_I_1 in(select code_new from HDS.Valueset_to_code where value_set_Name='Bone Mineral Density Tests')
		or
		Proc_I_2 in(select code_new from HDS.Valueset_to_code where value_set_Name='Bone Mineral Density Tests')
		or
		Proc_I_3 in(select code_new from HDS.Valueset_to_code where value_set_Name='Bone Mineral Density Tests')
		or
		Proc_I_4 in(select code_new from HDS.Valueset_to_code where value_set_Name='Bone Mineral Density Tests')
		or
		Proc_I_5 in(select code_new from HDS.Valueset_to_code where value_set_Name='Bone Mineral Density Tests')
		or
		Proc_I_6 in(select code_new from HDS.Valueset_to_code where value_set_Name='Bone Mineral Density Tests')
	)
	--and memid=100339
	Union all

	select Memid,date as servicedate from HDS.Hedis_Obs where Measure_id=@meas and OCode in(select code_new from HDS.Valueset_to_code where value_set_Name='Bone Mineral Density Tests')


	union All

	select Memid,sdate as servicedate from HDS.HEDIS_PROC where Measure_id=@meas and pstatus='EVN' and pcode in(select code_new from HDS.Valueset_to_code where value_set_Name='Bone Mineral Density Tests')
	)t1

)
Insert into #omw_BMDTSet
select b.MemID,e.EpisodeDate from #omw_EpisodeDatesWNMH e
join CTE_BMDT b on e.Memid=b.Memid and b.servicedate between DATEADD(day,-730,e.StartDate) and DATEADD(day,-1,e.StartDate)




--•	Members who had a claim/encounter for osteoporosis therapy (Osteoporosis Medication Therapy Value Set) during the 365 days (12 months) prior to the Episode Date.

Drop table if exists #omw_OsteoporosisSet
Create Table #omw_OsteoporosisSet
(
	Memid varchar(100),
	EpisodeDate varchar(10)
)

;With CTE_Osteo as
(
	select Memid,Date_S as servicedate,Date_S as enddate from HDS.HEDIS_VISIT where Measure_id=@meas and Date_S!=''  and HCFAPOS!=81 and HCPCS in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name='Osteoporosis Medication Therapy')
	--and memid=143020
	Union all
	select Memid,PrServDate as servicedate,convert(varchar,cast(dateadd(day,cast(PDaysSup as INT),PrServDate) as date),112) as Enddate from HDS.HEDIS_PHARM where Measure_id=@meas and NDC in(select code from HDS.MEDICATION_LIST_TO_CODES where Medication_List_Name='Osteoporosis Medications') 
	Union All
	Select Memid,StartDate as servicedate,EDate as Enddate from HDS.HEDIS_PHARM_C where Measure_id=@meas and Rxnorm in(select code from HDS.MEDICATION_LIST_TO_CODES where Medication_List_Name='Osteoporosis Medications') 
	--and memid=143020

)
Insert into #omw_OsteoporosisSet
select 
	distinct b.MemID,e.episodedate 
	from #omw_EpisodeDatesWNMH e
join CTE_Osteo b on e.Memid=b.Memid and (b.servicedate between DATEADD(day,-366,e.StartDate) and DATEADD(day,-1,e.StartDate) or b.enddate between DATEADD(day,-366,e.StartDate) and DATEADD(day,-1,e.StartDate)) 
--where e.memid=100025         

--


drop table if exists #omw_reqdexcl
create table #omw_reqdexcl
(
	Memid varchar(100)
)
Insert into #omw_reqdexcl
select distinct f2.Memid from(
	select f1.* from #omw_EpisodeDatesWNMH f1
	left outer join
	(

		select e.Memid,e.EpisodeDate,e.ct from(
		select *,count(memid) over(partition by Memid) as ct from #omw_EpisodeDatesWNMH
		)e
		join 
		(
			Select *,count(Memid) over(partition by Memid) as ct,DENSE_RANK() over(partition by Memid order by episodedate) as rn from(
			select distinct Memid,EpisodeDate from(
			select * from #omw_BMDTSet
			Union all
			Select * from #omw_OsteoporosisSet
			)d1
			)t1
		)t2 on e.Memid=t2.Memid and e.EpisodeDate=t2.EpisodeDate  and e.ct>1
	
	)t3 on f1.Memid=t3.Memid and f1.EpisodeDate=t3.EpisodeDate
	where t3.Memid is null
)f2
join
(
	select distinct Memid,EpisodeDate from(
			select * from #omw_BMDTSet
			Union all
			Select * from #omw_OsteoporosisSet
			)d2
)t4 on f2.Memid=t4.Memid and f2.EpisodeDate=t4.EpisodeDate


-- Palliative Care


Drop table if exists #omw_palliativecare;
Create Table #omw_palliativecare
(
	Memid varchar(100)
)
Insert into #omw_palliativecare
Select distinct Memid from(
select Memid from HDS.Hedis_Obs where Measure_id=@meas and date!='' and date between @ce_Intakestartdt and @ce_enddt and OCode in(select code_new from HDS.Valueset_to_code where value_set_Name in('Palliative Care Assessment','Palliative Care Encounter','Palliative Care Intervention'))

	Union all

select Memid from HDS.Hedis_Visit_e where Measure_id=@meas and sdate!='' and sdate between @ce_Intakestartdt and @ce_enddt and activity in(select code_new from HDS.Valueset_to_code where value_set_Name in('Palliative Care Encounter','Palliative Care Intervention'))

Union All

select MemID from HDS.HEDIS_VISIT v
	where Measure_id=@meas and HCFAPOS!='81' and Date_S between @ce_Intakestartdt and @ce_enddt and Date_S!=''
	and (
	Diag_I_1 in(select code_new from HDS.VALUESET_TO_CODE where Value_Set_Name in('Palliative Care Assessment','Palliative Care Intervention','Palliative Care Encounter'))
	or
	Diag_I_2 in(select code_new from HDS.VALUESET_TO_CODE where Value_Set_Name in('Palliative Care Assessment','Palliative Care Intervention','Palliative Care Encounter'))
	or
	Diag_I_3 in(select code_new from HDS.VALUESET_TO_CODE where Value_Set_Name in('Palliative Care Assessment','Palliative Care Intervention','Palliative Care Encounter'))
	or
	Diag_I_4 in(select code_new from HDS.VALUESET_TO_CODE where Value_Set_Name in('Palliative Care Assessment','Palliative Care Intervention','Palliative Care Encounter'))
	or
	Diag_I_5 in(select code_new from HDS.VALUESET_TO_CODE where Value_Set_Name in('Palliative Care Assessment','Palliative Care Intervention','Palliative Care Encounter'))
	or
	Diag_I_6 in(select code_new from HDS.VALUESET_TO_CODE where Value_Set_Name in('Palliative Care Assessment','Palliative Care Intervention','Palliative Care Encounter'))
	or
	Diag_I_7 in(select code_new from HDS.VALUESET_TO_CODE where Value_Set_Name in('Palliative Care Assessment','Palliative Care Intervention','Palliative Care Encounter'))
	or
	Diag_I_8 in(select code_new from HDS.VALUESET_TO_CODE where Value_Set_Name in('Palliative Care Assessment','Palliative Care Intervention','Palliative Care Encounter'))
	or
	Diag_I_9 in(select code_new from HDS.VALUESET_TO_CODE where Value_Set_Name in('Palliative Care Assessment','Palliative Care Intervention','Palliative Care Encounter'))
	or
	Diag_I_10 in(select code_new from HDS.VALUESET_TO_CODE where Value_Set_Name in('Palliative Care Assessment','Palliative Care Intervention','Palliative Care Encounter'))
	or
	CPT in(select code_new from HDS.VALUESET_TO_CODE where Value_Set_Name in('Palliative Care Assessment','Palliative Care Intervention','Palliative Care Encounter'))
	or
	CPT2 in(select code_new from HDS.VALUESET_TO_CODE where Value_Set_Name in('Palliative Care Assessment','Palliative Care Intervention','Palliative Care Encounter'))
	or
	HCPCS in(select code_new from HDS.VALUESET_TO_CODE where Value_Set_Name in('Palliative Care Assessment','Palliative Care Intervention','Palliative Care Encounter'))
	or 
	Proc_I_1 in(select code_new from HDS.VALUESET_TO_CODE where Value_Set_Name in('Palliative Care Assessment','Palliative Care Intervention','Palliative Care Encounter'))
	or 
	Proc_I_2 in(select code_new from HDS.VALUESET_TO_CODE where Value_Set_Name in('Palliative Care Assessment','Palliative Care Intervention','Palliative Care Encounter'))
	or 
	Proc_I_3 in(select code_new from HDS.VALUESET_TO_CODE where Value_Set_Name in('Palliative Care Assessment','Palliative Care Intervention','Palliative Care Encounter'))
	or 
	Proc_I_4 in(select code_new from HDS.VALUESET_TO_CODE where Value_Set_Name in('Palliative Care Assessment','Palliative Care Intervention','Palliative Care Encounter'))
	or 
	Proc_I_5 in(select code_new from HDS.VALUESET_TO_CODE where Value_Set_Name in('Palliative Care Assessment','Palliative Care Intervention','Palliative Care Encounter'))
	)
)t1
update #omwdataset set Rexcld=1 from #omwdataset ds join #omw_palliativecare t1 on ds.Memid=t1.Memid
update #omwdataset set Rexcld=1 from #omwdataset ds join #omw_reqdexcl t1 on ds.Memid=t1.Memid


--Rexcl Logic


	-- AdvancedIllness

	drop table if exists #omw_advillness;
	
	CREATE table #omw_advillness
	(
		Memid varchar(100),
		servicedate varchar(10),
		claimid varchar(100)
	);
	Insert into #omw_advillness
	select MemID,Date_S,claimid from HDS.HEDIS_VISIT v
	where Measure_id=@meas and HCFAPOS!='81' and suppdata='N' and Date_S between @ce_startdt1 and @ce_enddt
	and 
	(
		Diag_I_1 in(select code_new from HDS.VALUESET_TO_CODE where Value_Set_Name='Advanced Illness')
		or
		Diag_I_2 in(select code_new from HDS.VALUESET_TO_CODE where Value_Set_Name='Advanced Illness')
		or
		Diag_I_3 in(select code_new from HDS.VALUESET_TO_CODE where Value_Set_Name='Advanced Illness')
		or
		Diag_I_4 in(select code_new from HDS.VALUESET_TO_CODE where Value_Set_Name='Advanced Illness')
		or
		Diag_I_5 in(select code_new from HDS.VALUESET_TO_CODE where Value_Set_Name='Advanced Illness')
		or
		Diag_I_6 in(select code_new from HDS.VALUESET_TO_CODE where Value_Set_Name='Advanced Illness')
		or
		Diag_I_7 in(select code_new from HDS.VALUESET_TO_CODE where Value_Set_Name='Advanced Illness')
		or
		Diag_I_8 in(select code_new from HDS.VALUESET_TO_CODE where Value_Set_Name='Advanced Illness')
		or
		Diag_I_9 in(select code_new from HDS.VALUESET_TO_CODE where Value_Set_Name='Advanced Illness')
		or
		Diag_I_10 in(select code_new from HDS.VALUESET_TO_CODE where Value_Set_Name='Advanced Illness')
	)
	
	-- Members with Institutinal SNP

	UPDATE ds SET ds.rexcl=1 FROM #omwdataset ds JOIN HDS.HEDIS_MEMBER_EN s on ds.MemID=s.MemID and s.Measure_id=@meas WHERE  ds.age>=67 AND ds.payer IN('MCR','MCS','MP','MC','SN2','SN1','SN3') AND s.StartDate<=@ce_enddt AND s.FinishDate>=@ce_Intakestartdt AND s.Payer='SN2';


	-- LTI Exclusion

	drop table if exists #omw_LTImembers;
	
	CREATE table #omw_LTImembers
	(
		Memid varchar(100)
				
	);
	Insert into #omw_LTImembers
	SELECT DISTINCT Beneficiary_ID FROM HDS.HEDIS_MMDF WHERE Measure_id=@meas AND Run_Date BETWEEN @ce_Intakestartdt AND @ce_enddt AND LTI_Flag='Y';

	
	update ds set rexcl=1 from #omwdataset ds join #omw_LTImembers re on ds.Memid=re.Memid where ds.age>=67 AND ds.payer IN('MCR','MCS','MP','MC','MMP','SN1','SN2','SN3');

	   
	-- Hospice Exclusion

	drop table if exists #omw_hospicemembers;
	CREATE table #omw_hospicemembers
	(
		Memid varchar(100)
		
	);
	Insert into #omw_hospicemembers
	select distinct t1.MemID from
	(
		select Memid from HDS.HEDIS_OBS where Measure_id=@meas and date !='' and date between @ce_startdt and @ce_enddt and OCode IN(select code_new from HDS.VALUESET_TO_CODE where Value_Set_Name in('Hospice Encounter','Hospice Intervention'))
		
		Union all
		
		Select Memid from HDS.HEDIS_VISIT_E where Measure_id=@meas and sdate!='' and sdate between @ce_startdt and @ce_enddt and activity IN(select code_new from HDS.VALUESET_TO_CODE where Value_Set_Name in('Hospice Encounter','Hospice Intervention'))

		Union all

		select Beneficiary_id as Memid from HDS.Hedis_MMDF where Measure_id=@meas and Run_date!='' and Run_date between @ce_startdt and @ce_enddt and Hospice='Y'

		Union all

		select MemID from HDS.HEDIS_VISIT v
		where Measure_id=@meas and HCFAPOS!='81' and Date_S!='' and Date_S between @ce_startdt and @ce_enddt
		and (
			Diag_I_1 in(select code_new from HDS.VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
			or
			Diag_I_2 in(select code_new from HDS.VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
			or
			Diag_I_3 in(select code_new from HDS.VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
			or
			Diag_I_4 in(select code_new from HDS.VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
			or
			Diag_I_5 in(select code_new from HDS.VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
			or
			Diag_I_6 in(select code_new from HDS.VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
			or
			Diag_I_7 in(select code_new from HDS.VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
			or
			Diag_I_8 in(select code_new from HDS.VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
			or
			Diag_I_9 in(select code_new from HDS.VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
			or
			Diag_I_10 in(select code_new from HDS.VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
			or
			CPT in(select code_new from HDS.VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
			or
			CPT2 in(select code_new from HDS.VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
			or
			HCPCS in(select code_new from HDS.VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
			or 
			Proc_I_1 in(select code_new from HDS.VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
			or 
			Proc_I_2 in(select code_new from HDS.VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
			or 
			Proc_I_3 in(select code_new from HDS.VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
			or 
			Proc_I_4 in(select code_new from HDS.VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
			or 
			Proc_I_5 in(select code_new from HDS.VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
			or 
			Proc_I_6 in(select code_new from HDS.VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	
			or
			Rev in(select code_new from HDS.VALUESET_TO_CODE where code_system='UBREV' and Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
			)
		)t1
	
	update ds set ds.rexcl=1 from #omwdataset ds join #omw_hospicemembers hos on hos.memid=ds.MemID;
			
	-- Frailty Members LIST



	
	drop table if exists #omw_frailtymembers;
	
	CREATE table #omw_frailtymembers
	(
		
		Memid varchar(100)
			
	);
	Insert into #omw_frailtymembers
	select MemID from HDS.HEDIS_VISIT v
	where Measure_id=@meas and HCFAPOS!='81' and suppdata='N' and Date_S!='' and Date_S between @ce_Intakestartdt and @ce_enddt
	and (
		Diag_I_1 in(select code_new from HDS.VALUESET_TO_CODE where Value_Set_Name in('Frailty Diagnosis','Frailty Device','Frailty Encounter','Frailty Symptom'))
		or
		Diag_I_2 in(select code_new from HDS.VALUESET_TO_CODE where Value_Set_Name in('Frailty Diagnosis','Frailty Device','Frailty Encounter','Frailty Symptom'))
		or
		Diag_I_3 in(select code_new from HDS.VALUESET_TO_CODE where Value_Set_Name in('Frailty Diagnosis','Frailty Device','Frailty Encounter','Frailty Symptom'))
		or
		Diag_I_4 in(select code_new from HDS.VALUESET_TO_CODE where Value_Set_Name in('Frailty Diagnosis','Frailty Device','Frailty Encounter','Frailty Symptom'))
		or
		Diag_I_5 in(select code_new from HDS.VALUESET_TO_CODE where Value_Set_Name in('Frailty Diagnosis','Frailty Device','Frailty Encounter','Frailty Symptom'))
		or
		Diag_I_6 in(select code_new from HDS.VALUESET_TO_CODE where Value_Set_Name in('Frailty Diagnosis','Frailty Device','Frailty Encounter','Frailty Symptom'))
		or
		Diag_I_7 in(select code_new from HDS.VALUESET_TO_CODE where Value_Set_Name in('Frailty Diagnosis','Frailty Device','Frailty Encounter','Frailty Symptom'))
		or
		Diag_I_8 in(select code_new from HDS.VALUESET_TO_CODE where Value_Set_Name in('Frailty Diagnosis','Frailty Device','Frailty Encounter','Frailty Symptom'))
		or
		Diag_I_9 in(select code_new from HDS.VALUESET_TO_CODE where Value_Set_Name in('Frailty Diagnosis','Frailty Device','Frailty Encounter','Frailty Symptom'))
		or
		Diag_I_10 in(select code_new from HDS.VALUESET_TO_CODE where Value_Set_Name in('Frailty Diagnosis','Frailty Device','Frailty Encounter','Frailty Symptom'))
		or
		CPT in(select code_new from HDS.VALUESET_TO_CODE where Value_Set_Name in('Frailty Diagnosis','Frailty Device','Frailty Encounter','Frailty Symptom'))
		or
		HCPCS in(select code_new from HDS.VALUESET_TO_CODE where Value_Set_Name in('Frailty Diagnosis','Frailty Device','Frailty Encounter','Frailty Symptom'))
	);

	update ds Set ds.Rexcl=1 from #omwdataset ds join #omw_frailtymembers f on ds.MemID=f.Memid and ds.age>=81;

	-- Required Exclusion 1

	-- Inpatient Stay List

	drop table if exists #omw_inpatientstaylist;
	CREATE table #omw_inpatientstaylist
	(
		Memid varchar(100),
		date_s varchar(10),
		claimid varchar(100)
	);
	Insert into #omw_inpatientstaylist
	select distinct MemID,Date_S,claimid from HDS.HEDIS_VISIT v where v.Measure_id=@meas and v.HCFAPOS!='81' and suppdata='N' and v.date_disch!='' and v.date_disch between @ce_startdt1 and @ce_enddt and v.REV in (select code_new from HDS.VALUESET_TO_CODE where code_system='UBREV' and Value_Set_Name in('Inpatient Stay'));
	
	-- Non acute Inpatient stay list


	drop table if exists #omw_noncauteinpatientstaylist;
	CREATE table #omw_noncauteinpatientstaylist
	(
		Memid varchar(100),
		date_s varchar(10),
		claimid varchar(100)
	);
	Insert into #omw_noncauteinpatientstaylist
	select distinct MemID,Date_S,claimid from HDS.HEDIS_VISIT v where v.Measure_id=@meas and v.HCFAPOS!='81' and suppdata='N' and v.date_disch!='' and v.date_disch between @ce_startdt1 and @ce_enddt and (v.REV in (select code_new from HDS.VALUESET_TO_CODE where code_system='UBREV'and Value_Set_Name in('Nonacute Inpatient Stay')) or RIGHT('0000'+CAST(Trim(v.BillType) AS VARCHAR(4)),4) in (select code_new from HDS.VALUESET_TO_CODE where code_system='UBTOB' and Value_Set_Name in('Nonacute Inpatient Stay')))
	
	
	-- Outpatient and other visits

	drop table if exists #omw_visitlist;
	CREATE table #omw_visitlist
	(
		Memid varchar(100),
		date_s varchar(10),
		claimid varchar(100)
	);
	Insert into #omw_visitlist
	select distinct MemID,Date_S,claimid from HDS.HEDIS_VISIT v
	where Measure_id=@meas and HCFAPOS!='81' and suppdata='N' and Date_S!='' and  Date_S between @ce_startdt1 and @ce_enddt
	and (
		Diag_I_1 in(select code_new from HDS.VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments','Nonacute Inpatient'))
		or
		Diag_I_2 in(select code_new from HDS.VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments','Nonacute Inpatient'))
		or
		Diag_I_3 in(select code_new from HDS.VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments','Nonacute Inpatient'))
		or
		Diag_I_4 in(select code_new from HDS.VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments','Nonacute Inpatient'))
		or
		Diag_I_5 in(select code_new from HDS.VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments','Nonacute Inpatient'))
		or
		Diag_I_6 in(select code_new from HDS.VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments','Nonacute Inpatient'))
		or
		Diag_I_7 in(select code_new from HDS.VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments','Nonacute Inpatient'))
		or
		Diag_I_8 in(select code_new from HDS.VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments','Nonacute Inpatient'))
		or
		Diag_I_9 in(select code_new from HDS.VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments','Nonacute Inpatient'))
		or
		Diag_I_10 in(select code_new from HDS.VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments','Nonacute Inpatient'))
		or
		CPT in(select code_new from HDS.VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments','Nonacute Inpatient'))
		or
		CPT2 in(select code_new from HDS.VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments','Nonacute Inpatient'))
		or
		HCPCS in(select code_new from HDS.VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments','Nonacute Inpatient'))
		or 
		Proc_I_1 in(select code_new from HDS.VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments','Nonacute Inpatient'))
		or 
		Proc_I_2 in(select code_new from HDS.VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments','Nonacute Inpatient'))
		or 
		Proc_I_3 in(select code_new from HDS.VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments','Nonacute Inpatient'))
		or 
		Proc_I_4 in(select code_new from HDS.VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments','Nonacute Inpatient'))
		or 
		Proc_I_5 in(select code_new from HDS.VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments','Nonacute Inpatient'))
		or 
		Proc_I_6 in(select code_new from HDS.VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments','Nonacute Inpatient'))
	
		or 
		Rev in(select code_new from HDS.VALUESET_TO_CODE where code_system='UBREV' and  Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments','Nonacute Inpatient'))
	
	)
	
	
	-- Required exclusion table

	
	drop table if exists #omw_reqdexcl1;
	CREATE table #omw_reqdexcl1
	(
		Memid varchar(100)
			
	);
	Insert into #omw_reqdexcl1
	select distinct t3.Memid from(
	select t2.MemId from(
	select distinct t1.Memid,t1.date_s from(
	select Memid,date_s,claimid  from #omw_visitlist 
	union all
	select na.Memid,na.Date_s,na.claimid from #omw_noncauteinpatientstaylist na
	join #omw_inpatientstaylist inp on na.Memid=inp.Memid and na.claimid=inp.claimid
	)t1
	Join #omw_advillness a on a.Memid=t1.Memid and a.claimid=t1.claimid
	)t2 group by t2.Memid having count(t2.Memid)>1
	)t3 
	Join #omw_frailtymembers f on f.Memid=t3.Memid
	
	
	update ds set rexcl=1 from #omwdataset ds
	join #omw_reqdexcl1 re1 on re1.Memid=ds.MemID and ds.age BETWEEN 67 AND 80;

	-- Required Exclusion 2

	-- Acute Inpatient with Advanced Illness

	drop table if exists #omw_reqdexcl2;
	CREATE table #omw_reqdexcl2
	(
		Memid varchar(100)
				
	);
	insert into #omw_reqdexcl2
	select distinct t2.Memid from(
	select t1.MemID from (
	select distinct MemID,Date_S,claimid from HDS.HEDIS_VISIT v
	where Measure_id=@meas and HCFAPOS!='81' and suppdata='N' and Date_S!='' and Date_S between @ce_startdt1 and @ce_enddt
	and (
		Diag_I_1 in(select code_new from HDS.VALUESET_TO_CODE where Value_Set_Name in('Acute Inpatient'))
		or
		Diag_I_2 in(select code_new from HDS.VALUESET_TO_CODE where Value_Set_Name in('Acute Inpatient'))
		or
		Diag_I_3 in(select code_new from HDS.VALUESET_TO_CODE where Value_Set_Name in('Acute Inpatient'))
		or
		Diag_I_4 in(select code_new from HDS.VALUESET_TO_CODE where Value_Set_Name in('Acute Inpatient'))
		or
		Diag_I_5 in(select code_new from HDS.VALUESET_TO_CODE where Value_Set_Name in('Acute Inpatient'))
		or
		Diag_I_6 in(select code_new from HDS.VALUESET_TO_CODE where Value_Set_Name in('Acute Inpatient'))
		or
		Diag_I_7 in(select code_new from HDS.VALUESET_TO_CODE where Value_Set_Name in('Acute Inpatient'))
		or
		Diag_I_8 in(select code_new from HDS.VALUESET_TO_CODE where Value_Set_Name in('Acute Inpatient'))
		or
		Diag_I_9 in(select code_new from HDS.VALUESET_TO_CODE where Value_Set_Name in('Acute Inpatient'))
		or
		Diag_I_10 in(select code_new from HDS.VALUESET_TO_CODE where Value_Set_Name in('Acute Inpatient'))
		or
		CPT in(select code_new from HDS.VALUESET_TO_CODE where Value_Set_Name in('Acute Inpatient'))
		or
		CPT2 in(select code_new from HDS.VALUESET_TO_CODE where Value_Set_Name in('Acute Inpatient'))
		or
		HCPCS in(select code_new from HDS.VALUESET_TO_CODE where Value_Set_Name in('Acute Inpatient'))
		or 
		Proc_I_1 in(select code_new from HDS.VALUESET_TO_CODE where Value_Set_Name in('Acute Inpatient'))
		or 
		Proc_I_2 in(select code_new from HDS.VALUESET_TO_CODE where Value_Set_Name in('Acute Inpatient'))
		or 
		Proc_I_3 in(select code_new from HDS.VALUESET_TO_CODE where Value_Set_Name in('Acute Inpatient'))
		or 
		Proc_I_4 in(select code_new from HDS.VALUESET_TO_CODE where Value_Set_Name in('Acute Inpatient'))
		or 
		Proc_I_5 in(select code_new from HDS.VALUESET_TO_CODE where Value_Set_Name in('Acute Inpatient'))
		or 
		Proc_I_6 in(select code_new from HDS.VALUESET_TO_CODE where Value_Set_Name in('Acute Inpatient'))
	
	)
	)t1
	Join #omw_advillness a on a.Memid=t1.MemID and a.claimid=t1.claimid
	)t2
	join #omw_frailtymembers f on f.Memid=t2.MemID

	
	update ds set ds.rexcl=1 from #omwdataset ds
	join #omw_reqdexcl2 re2 on re2.Memid=ds.MemID and ds.age BETWEEN 67 AND 80;
	
			
	-- Required exclusion 3

	drop table if exists #omw_reqdexcl3;
	CREATE table #omw_reqdexcl3
	(
		Memid varchar(100)
			
	);
	insert into #omw_reqdexcl3
	select distinct t2.MemId from(
	select t1.Memid,t1.date_s,t1.claimid from(
	select inp.Memid,inp.date_s,inp.claimid from #omw_inpatientstaylist inp
	left outer join #omw_noncauteinpatientstaylist na on inp.Memid=na.Memid and inp.claimid=na.claimid
	where na.Memid is null
	)t1
	join #omw_advillness a on a.Memid=t1.Memid and a.claimid=t1.claimid
	)t2
	join #omw_frailtymembers f on f.Memid=t2.Memid

	
	update ds set ds.rexcl=1 from #omwdataset ds
	join #omw_reqdexcl3 re3 on re3.Memid=ds.MemId and ds.age BETWEEN 67 AND 80;

		
	-- RequiredExcl 4


	drop table if exists #omw_reqdexcl4;
	CREATE table #omw_reqdexcl4
	(
		Memid varchar(100)
				
	);
	insert into #omw_reqdexcl4
	select t1.Memid from(
	select Memid from HDS.HEDIS_PHARM where Measure_id=@meas  and suppdata='N' and PrServDate!='' and  PrServDate between @ce_startdt1 and @ce_enddt and NDC in(select code from HDS.MEDICATION_LIST_TO_CODES where Medication_List_Name='Dementia Medications')
	)t1
	Join #omw_frailtymembers f on f.MemId=t1.Memid;

	update ds set ds.rexcl=1 from #omwdataset ds
	join #omw_reqdexcl4 re4 on re4.Memid=ds.MemID and ds.age BETWEEN 66 AND 80;
	

-- Numerator Logic
	-- Identifying IESD

	drop table if Exists #omw_IESD
	Create Table #omw_IESD
	(	
		Memid varchar(100),
		IESD varchar(8)
	)
	;with CTE_exclusions as
	(
		select distinct Memid,EpisodeDate from(
					
			select * from #omw_BMDTSet
					
			Union all

			Select * from #omw_OsteoporosisSet

			Union all

			select e.Memid,e.EpisodeDate from #omw_EpisodeDatesWNMH  e
			left outer join  #omw_contenroll ce on e.Memid=ce.Memid and e.EPisodedate=ce.episodedate
			where ce.Memid is null
						
			Union all

			select e.Memid,e.EpisodeDate from #omw_EpisodeDatesWNMH  e
			join  #omw_palliativecare p on e.Memid=p.Memid 
						
		)d1
					
	),CTE_Inclusion as
	(
	select e.Memid,e.EpisodeDate
		from(
			select *,count(memid) over(partition by Memid) as ct from #omw_EpisodeDatesWNMH
		)e
		left outer join CTE_exclusions excl on e.Memid=excl.Memid and e.EpisodeDate=excl.EpisodeDate 
		where excl.Memid is null
	)
	Insert into #omw_IESD
	select Memid,min(Episodedate) as IESD from(
	select * from CTE_exclusions where memid not in(select memid from CTE_Inclusion)
	Union all
	select * from CTE_Inclusion
	)t1 group by Memid
	--having Memid=158800




	-- Bone Density

	-- Bone Mineral Density Tests
	Drop table if Exists #omw_NUM_BMDTSet
	Create Table #omw_NUM_BMDTSet
	(
		Memid Varchar(100),
		EpisodeDate varchar(10)
	)
	;With CTE_BMDT as
	(
		Select distinct Memid,servicedate from(
		select Memid,Date_S as servicedate from HDS.HEDIS_VISIT where Measure_id=@meas and Date_S!='' and HCFAPOS!='81' and
		(
			CPT in(select code_new from HDS.Valueset_to_code where value_set_Name='Bone Mineral Density Tests')
			or
			Proc_I_1 in(select code_new from HDS.Valueset_to_code where value_set_Name='Bone Mineral Density Tests')
			or
			Proc_I_2 in(select code_new from HDS.Valueset_to_code where value_set_Name='Bone Mineral Density Tests')
			or
			Proc_I_3 in(select code_new from HDS.Valueset_to_code where value_set_Name='Bone Mineral Density Tests')
			or
			Proc_I_4 in(select code_new from HDS.Valueset_to_code where value_set_Name='Bone Mineral Density Tests')
			or
			Proc_I_5 in(select code_new from HDS.Valueset_to_code where value_set_Name='Bone Mineral Density Tests')
			or
			Proc_I_6 in(select code_new from HDS.Valueset_to_code where value_set_Name='Bone Mineral Density Tests')
		)
		--and memid=143020
		Union all

		select Memid,date as servicedate from HDS.Hedis_Obs where Measure_id=@meas and OCode in(select code_new from HDS.Valueset_to_code where value_set_Name='Bone Mineral Density Tests')


		union All

		select Memid,sdate as servicedate from HDS.HEDIS_PROC where Measure_id=@meas and pstatus='EVN' and pcode in(select code_new from HDS.Valueset_to_code where value_set_Name='Bone Mineral Density Tests')
		)t1
	--	where memid=143020
	)
	Insert into #omw_NUM_BMDTSet
	select ie.MemID,ie.IESD from #omw_IESD ie
	join #omw_EpisodeDatesWNMH e on ie.Memid=e.Memid and ie.IESD=e.EpisodeDate
	join CTE_BMDT b on ie.Memid=b.Memid and b.servicedate between e.StartDate and DATEADD(day,180,ie.IESD)
	--where ie.Memid=143020

	
	Update ds Set Num=1 from #omwdataset ds join #omw_NUM_BMDTSet n on ds.MemID=n.Memid

	-- Osteoporosis

	
	Drop table if exists #omw_NUM_OsteoporosisSet
	Create Table #omw_NUM_OsteoporosisSet
	(
		Memid varchar(100),
		IESD varchar(8)
	)

	;With CTE_Osteo as
	(
		select distinct Memid,Servicedate,Enddate from(
		select Memid,Date_S as servicedate,Date_S as enddate from HDS.HEDIS_VISIT where Measure_id=@meas and Date_S!=''  and HCFAPOS!='81' and HCPCS in(select code_new from HDS.VALUESET_TO_CODE where Value_Set_Name='Osteoporosis Medication Therapy')
		--and memid=143020
		Union all
		select Memid,PrServDate as servicedate,convert(varchar,cast(dateadd(day,cast(PDaysSup as INT),PrServDate) as date),112) as Enddate from HDS.HEDIS_PHARM where Measure_id=@meas and NDC in(select code from HDS.MEDICATION_LIST_TO_CODES where Medication_List_Name='Osteoporosis Medications') 
		Union All
		Select Memid,StartDate as servicedate,EDate as Enddate from HDS.HEDIS_PHARM_C where Measure_id=@meas and Rxnorm in(select code from HDS.MEDICATION_LIST_TO_CODES where Medication_List_Name='Osteoporosis Medications') 
		--and memid=143020
		)t1
		--where memid=158800
	)
	Insert into #omw_NUM_OsteoporosisSet
	select b.MemID,ie.IESD from #omw_IESD ie
	join #omw_EpisodeDatesWNMH e on ie.Memid=e.Memid and ie.IESD=e.EpisodeDate
	join CTE_Osteo b on e.Memid=b.Memid and b.servicedate between e.StartDate and DATEADD(day,180,ie.IESD)
	--where ie.Memid=143020

	Update ds Set Num=1 from #omwdataset ds join #omw_NUM_OsteoporosisSet n on ds.MemID=n.Memid

	
	
	-- Long Osteporosis

	Drop table if exists #omw_NUM_LongOsteoporosisSet
	Create Table #omw_NUM_LongOsteoporosisSet
	(
		Memid varchar(100),
		IESD varchar(8)
	)

	;With CTE_Osteo as
	(
		select Memid,Date_S as servicedate,Date_S as enddate from HDS.HEDIS_VISIT where Measure_id=@meas and Date_S!=''  and HCFAPOS!='81' and HCPCS in(select code_new from HDS.VALUESET_TO_CODE where Value_Set_Name='Long-Acting Osteoporosis Medications')
	)
	Insert into #omw_NUM_LongOsteoporosisSet
	select ie.* from #omw_EpisodeDatesWNMH e
	join #omw_IESD ie on e.Memid=ie.Memid and e.EpisodeDate=ie.IESD
	join CTE_Osteo b on e.Memid=b.Memid and b.servicedate between e.StartDate and e.EpisodeDate

	Update ds Set Num=1 from #omwdataset ds join #omw_NUM_LongOsteoporosisSet n on ds.MemID=n.Memid

-- Generate Output

	select @runid=max(RUN_ID) from HDS.HEDIS_MEASURE_OUTPUT where measure_id=@meas;

	if(@runid>=1)
	Begin
		SET @runid=@runid+1;
	End
	Else
	Begin
		SET @runid=1;
	END
	

	--Insert into HDS.HEDIS_MEASURE_OUTPUT(memid,Meas,payer,CE,Event,Epop,Excl,Num,Rexcl,RExclD,Age,Gender,Measure_ID,Measurement_year,RUN_ID)
	SELECT memid,meas,payer,CE,EVENT,CASE WHEN Event=1 and CE=1  AND rexcl=0 and rexcld=0 and payer in('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP') THEN 1 ELSE 0 END AS epop,excl,num,rexcl,rexcld,cast(age as Int) AS age,Gender AS gender,@meas,@meas_year,@runid FROM #omwdataset


GO
