SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

CREATE proc [HDS].[RUN_HEDIS_AMM] @meas_year nvarchar(4)
AS


-- Declare Variables

Declare @meas varchar(10)='AMM';
Declare @ce_startdt varchar(8);
Declare @ce_medstartdt varchar(8);
Declare @ce_startdt1 varchar(8);
Declare @ce_enddt varchar(8);
Declare @ce_medenddt varchar(8);
Declare @ce_enddt1 varchar(8);
Declare @ipsd varchar(8);
DECLARE @pt_ctr Int=0;
DECLARE @i INT=0;
declare @patientid varchar(50);
declare @plan_ct INT;
declare @gender varchar(10);
declare @age INT;
Declare @latest_insenddate varchar(8);
Declare @plan1 varchar(20);
Declare @plan2 varchar(20);
Declare @planid varchar(20);
Declare @runid INT;




-- Set Measure dates
SET @ce_medstartdt=concat(@meas_year-1,'0501');
SET @ce_startdt=concat(@meas_year,'0101');
SET @ce_medenddt=concat(@meas_year,'0430');
SET @ce_enddt=concat(@meas_year,'1231');
SET @ce_startdt1=concat(@meas_year-1,'0101');
SET @ce_enddt1=concat(@meas_year-1,'1231');


-- Create cdcdataset
drop table if exists #ammdataset;

CREATE TABLE #ammdataset (
  [patient_id] varchar(100) NOT NULL,
  [meas] varchar(20) DEFAULT NULL,
  [payer] varchar(100) DEFAULT NULL,
  [patient_gender] varchar(45) NOT NULL,
  [age] decimal(12,4) DEFAULT NULL,
  [orec] smallint DEFAULT NULL,
  [lis] smallint DEFAULT '0',
  [rexcl] smallint DEFAULT '0',
  [rexcld] smallint DEFAULT '1',
  [CE] smallint DEFAULT '0',
  [excl] smallint DEFAULT '0',
  [num] smallint DEFAULT '0',
  [Event] smallint DEFAULT '1'
   
) ;


-- Members with anti-depressants

drop table if exists #amm_memwithdepressionmedlist; 

CREATE TABLE #amm_memwithdepressionmedlist (
    memid varchar(100),
	servicedate varchar(8)
    
);

Insert into #amm_memwithdepressionmedlist
select distinct memid,servicedate from(

select memid,prservdate as servicedate from hedis_pharm where measure_id=@meas and suppdata='N' and prservdate!='' and PrServDate between @ce_startdt1 and @ce_enddt and  NDC in (select code from MEDICATION_LIST_TO_CODES where Medication_List_Name='Antidepressant Medications') 

)t1


CREATE CLUSTERED INDEX idx_#amm_memwithdepressionmedlist ON #amm_memwithdepressionmedlist ([memid],[servicedate]);

-- identify IPSD
drop table if exists #amm_memipsd; 

CREATE TABLE #amm_memipsd (
    memid varchar(100),
	ipsd varchar(8)
    
);

Insert into #amm_memipsd
select Memid,min(servicedate) as ipsd from #amm_memwithdepressionmedlist where servicedate between @ce_medstartdt and @ce_medenddt group by memid

CREATE CLUSTERED INDEX idx_amm_memipsd ON #amm_memipsd ([memid],[ipsd]);


-- Eligible Patient List

drop table if exists #amm_memlist; 

CREATE TABLE #amm_memlist (
    memid varchar(100)
    
);


insert into #amm_memlist
select distinct t2.memid from (
select *,datediff(day,lastmeddate,servicedate) as medgap from (
select *,isnull(LAG(servicedate,1) over(partition by memid order by servicedate),servicedate) as lastmeddate from #amm_memwithdepressionmedlist 
)t1 where t1.servicedate between @ce_medstartdt and @ce_medenddt and (datediff(day,lastmeddate,servicedate)>105 or datediff(day,lastmeddate,servicedate)=0) 
)t2
Join 
HEDIS_MEMBER_GM g on t2.memid=g.MemID and g.MEASURE_ID=@meas and DATEDIFF(day,g.dob,@ce_medenddt)/365.25>=18  
order by 1

CREATE CLUSTERED INDEX ix_memlist_memid ON #amm_memlist ([memid]);



-- Create Temp Patient Enrollment
drop table if exists #amm_tmpsubscriber;

CREATE TABLE #amm_tmpsubscriber (
    memid varchar(100),
    dob varchar(8),
	gender varchar(1),
	payer varchar(50),
	StartDate varchar(8),
	EndDate varchar(8),
);



insert into #amm_tmpsubscriber
select en.MemID,gm.DOB,gm.Gender,en.Payer,en.StartDate,en.FinishDate as Enddate from (
select *,datediff(day,lastmeddate,servicedate) as medgap from (
select *,isnull(LAG(servicedate,1) over(partition by memid order by servicedate),servicedate) as lastmeddate from #amm_memwithdepressionmedlist 
)t1 where t1.servicedate between @ce_medstartdt and @ce_medenddt and (datediff(day,lastmeddate,servicedate)>105 or datediff(day,lastmeddate,servicedate)=0) 
)t2
Join 
HEDIS_MEMBER_GM gm on t2.memid=gm.MemID and gm.MEASURE_ID=@meas and DATEDIFF(day,gm.dob,@ce_medenddt)/365.25>=18 
Join HEDIS_MEMBER_EN en on t2.memid=en.MemID and en.MEASURE_ID=@meas and en.StartDate<=(select dateadd(day,231,t2.ipsd) from #amm_memipsd t2 where en.memid=t2.memid) and en.FinishDate>=(select dateadd(day,-105,t2.ipsd) from #amm_memipsd t2 where en.memid=t2.memid)
order by 1

CREATE CLUSTERED INDEX ix_tmpsub_memid ON #amm_tmpsubscriber ([memid],[StartDate],[EndDate]);

-- Get Patient COUNT
SELECT @pt_ctr=COUNT(*)  FROM #amm_memlist;


-- Loop through enrollment for each patient

While @i<@pt_ctr
	BEGIN
		-- Get Patient to Loop
		SELECT  @patientid=memid FROM #amm_memlist ORDER BY memid ASC OFFSET  @i ROWS FETCH NEXT 1 ROWS ONLY ;
		
		-- ReadingGender and age
		SELECT Top 1 @gender=gender,@age=DATEDIFF(day,dob,@ce_medenddt)/365.25 FROM #amm_tmpsubscriber WHERE memid=@patientid;
		
		-- Check for dual eligibility by checking if the patient is enrolled in more than one plan at the end of Enrollment
		
		SELECT Top 1 @latest_insenddate=t1.Enddate FROM #amm_tmpsubscriber t1 WHERE  t1.memid=@patientid  AND t1.StartDate<=(select dateadd(day,231,t2.ipsd) from #amm_memipsd t2 where t1.memid=t2.memid) ORDER BY StartDate DESC,EndDate Desc  ;
		
		-- Check for plan count patient is enrolled in during the end of measurement year
		
		SELECT @plan_ct=COUNT(*) FROM #amm_tmpsubscriber WHERE  memid=@patientid AND @latest_insenddate BETWEEN StartDate AND EndDate;
		
		-- If only one plan, then skip the Payer reporting logic
		
		if(@plan_ct=1)
		BEGIN
			
			SELECT @planid=payer FROM #amm_tmpsubscriber WHERE  memid=@patientid AND @latest_insenddate BETWEEN StartDate AND EndDate ORDER BY payer;
		
			-- If plan is Dual , Insert MCR & MCD	
				
			If(@planid in('MMP','SN3','MDE'))
			BEGIN
				
				IF(@planid in('MMP','SN3'))
				Begin
					Insert INTO #ammdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'amm2',@planid,@age,@gender);
					Insert INTO #ammdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'amm3',@planid,@age,@gender);
					
				END
				
				SET @planid='MCD';
				Insert INTO #ammdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'amm2',@planid,@age,@gender);
				Insert INTO #ammdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'amm3',@planid,@age,@gender);
						
				SET @planid='MCR';
				Insert INTO #ammdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'amm2',@planid,@age,@gender);
				Insert INTO #ammdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'amm3',@planid,@age,@gender);
				
				
			END
			
			Else if(@planid IN('MD','MLI','MRB'))
			Begin
				SET @planid='MCD';
				
				Insert INTO #ammdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'amm2',@planid,@age,@gender);
				Insert INTO #ammdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'amm3',@planid,@age,@gender);
				
			END
			
			Else If(@planid IN('SN1','SN2'))
			Begin
				
				Insert INTO #ammdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'amm2',@planid,@age,@gender);
				Insert INTO #ammdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'amm3',@planid,@age,@gender);
				
				SET @planid='MCR';
				Insert INTO #ammdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'amm2',@planid,@age,@gender);
				Insert INTO #ammdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'amm3',@planid,@age,@gender);
				
			END
			Else if(@planid IN('MEP','MMO','MOS','MPO'))
			BEGIN
				
				Insert INTO #ammdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'amm2',@planid,@age,@gender);
				Insert INTO #ammdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'amm3',@planid,@age,@gender);
				
			END
			ELSE
			BEGIN
			
				Insert INTO #ammdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'amm2',@planid,@age,@gender);
				Insert INTO #ammdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'amm3',@planid,@age,@gender);
				
			END
		END
				
		-- Compare Plans if Plan count is more than one	
		if(@plan_ct>1)
		BEGIN
			
			-- Read First Plan
			SELECT  @plan1=payer FROM #amm_tmpsubscriber where memid=@patientid and @latest_insenddate between StartDate and EndDate order by 1 OFFSET  0 ROWS FETCH NEXT 1 ROWS ONLY ;
					
			-- Read second plan
			SELECT  @plan2=payer FROM #amm_tmpsubscriber where memid=@patientid and @latest_insenddate between StartDate and EndDate order by 1 OFFSET  1 ROWS FETCH NEXT 1 ROWS ONLY ;
			
			
			
			If(@plan1 IN('MCR','MP','MC','SN1','SN2','MCS') and @plan2 IN('PPO','POS','HMO','CEP'))
			BEGIN
				if(@plan1 IN('SN1','SN2'))
				BEGIN
					SET @planid=@plan1;
					Insert INTO #ammdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'amm2',@planid,@age,@gender);
					Insert INTO #ammdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'amm3',@planid,@age,@gender);
				
					SET @planid='MCR';
					Insert INTO #ammdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'amm2',@planid,@age,@gender);
					Insert INTO #ammdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'amm3',@planid,@age,@gender);
				
					
				END
				
				if(@plan1 in ('MCR','MP','MC','MCS'))
				Begin
					
					Set @planid=@plan1;
					Insert INTO #ammdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'amm2',@planid,@age,@gender);
					Insert INTO #ammdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'amm3',@planid,@age,@gender);
				
				END

			END
			
			Else If(@plan2 IN('MCR','MP','MC','SN1','SN2','MCS') AND @plan1 IN('PPO','POS','HMO','CEP'))
			BEGIN
			
				IF(@plan2 IN('SN1','SN2'))
				Begin
				
					SET @planid=@plan2;
					Insert INTO #ammdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'amm2',@planid,@age,@gender);
					Insert INTO #ammdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'amm3',@planid,@age,@gender);
				
					SET @planid='MCR';
					Insert INTO #ammdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'amm2',@planid,@age,@gender);
					Insert INTO #ammdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'amm3',@planid,@age,@gender);
				
				END
				
				
				IF(@plan2 IN('MCR','MP','MC','MCS'))
				BEGIN
					
					Set @planid=@plan2;
					
					Insert INTO #ammdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'amm2',@planid,@age,@gender);
					Insert INTO #ammdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'amm3',@planid,@age,@gender);
				
					
				END
			END
			
			ELSE IF(@plan1 IN('PPO','POS','HMO','CEP') AND @plan2 IN('MD','MLI','MRB'))
			BEGIN
			
				Set @planid=@plan1;
				Insert INTO #ammdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'amm2',@planid,@age,@gender);
				Insert INTO #ammdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'amm3',@planid,@age,@gender);
				
					
			END
			
			ELSE IF(@plan2 IN('PPO','POS','HMO','CEP') AND @plan1 IN('MD','MLI','MRB'))
			BEGIN
			
				Set @planid=@plan2;
				Insert INTO #ammdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'amm2',@planid,@age,@gender);
				Insert INTO #ammdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'amm3',@planid,@age,@gender);
				
			END
			
			ELSE IF(@plan1 IN('MDE','SN3','MMP') or @plan2 in('MDE','SN3','MMP'))
			BEGIN
			
				
				if(@plan1 IN('SN3','MMP'))
				Begin
					Set @planid=@plan1;
					Insert INTO #ammdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'amm2',@planid,@age,@gender);
					Insert INTO #ammdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'amm3',@planid,@age,@gender);
				
				End
				
				if(@plan2 IN('SN3','MMP'))
				Begin
					Set @planid=@plan2
					Insert INTO #ammdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'amm2',@planid,@age,@gender);
					Insert INTO #ammdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'amm3',@planid,@age,@gender);
				
				END
				
					
				Set @planid='MCR';
				Insert INTO #ammdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'amm2',@planid,@age,@gender);
				Insert INTO #ammdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'amm3',@planid,@age,@gender);
				
				Set @planid='MCD';
				Insert INTO #ammdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'amm2',@planid,@age,@gender);
				Insert INTO #ammdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'amm3',@planid,@age,@gender);
				
			END
			ELSE
			BEGIN
			
				Set @planid=@plan1;	
				Insert INTO #ammdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'amm2',@planid,@age,@gender);
				Insert INTO #ammdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'amm3',@planid,@age,@gender);
				
				Set @planid=@plan2;
				Insert INTO #ammdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'amm2',@planid,@age,@gender);
				Insert INTO #ammdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,'amm3',@planid,@age,@gender);
				
				
			END
			
		END
		
		Set @i=@i+1;
		
	END;
-- Create Indices on ammdataset
CREATE INDEX [idx1] ON #ammdataset ([payer]);
CREATE INDEX [patient_id_UNIQUE] ON #ammdataset ([patient_id],[payer]);



---- Continuous Enrollment
	
	
DROP TABLE IF EXISTS #amm_contenroll;
	
	CREATE table #amm_contenroll
	(
		Memid varchar(50)
		
	);
	

With coverage_CTE (Memid,lastcoveragedate,Startdate,Finishdate,nextcoveragedate,ipsd) as
(
select e.Memid,isnull(lag(e.Finishdate,1) over(partition by e.Memid order by e.Startdate,e.FinishDate desc),(select convert(varchar,cast(dateadd(day,-105,t1.ipsd) as date),112) from #amm_memipsd t1 where t1.memid=e.MemID)) as lastcoveragedate,Case when e.StartDate<(select dateadd(day,-105,t1.ipsd) from #amm_memipsd t1 where t1.memid=e.MemID) then (select convert(varchar,cast(dateadd(day,-105,t1.ipsd) as date),112) from #amm_memipsd t1 where t1.memid=e.MemID) else startdate end as Startdate,case when Finishdate>(select dateadd(day,231,t1.ipsd) from #amm_memipsd t1 where t1.memid=e.MemID) then (select convert(varchar,cast(dateadd(day,231,t1.ipsd) as date),112) from #amm_memipsd t1 where t1.memid=e.MemID) else finishdate end as Finishdate,isnull(lead(Startdate,1) over(partition by Memid order by Startdate,FinishDate),(select convert(varchar,cast(dateadd(day,231,t1.ipsd) as date),112) from #amm_memipsd t1 where t1.memid=e.MemID)) as nextcoveragedate,(select t1.ipsd from #amm_memipsd t1 where t1.memid=e.MemID) as ipsd from hedis_member_en e where e.measure_id=@meas and e.drug='Y' and e.Startdate<=(select dateadd(day,231,t1.ipsd) from #amm_memipsd t1 where t1.memid=e.MemID) and e.Finishdate>=(select dateadd(day,-105,t1.ipsd) from #amm_memipsd t1 where t1.memid=e.MemID) 
)
Insert into #amm_contenroll
select Memid from(
select *, case when rn=1 and startdate>dateadd(day,-105,ipsd) then 1 else 0 end as startgap,case when datediff(day,newFinishdate,nextcoveragedate)>0 then 1  else 0 end as gaps,datediff(day,Startdate,finishdate)+1 as coveragedays,case when ipsd between Startdate and newfinishdate then 1 else 0 end as anchor from(
Select *,case when datediff(day,Finishdate,nextcoveragedate) <=1 then isnull(lead(Finishdate,1) over(partition by Memid order by Startdate,FinishDate),dateadd(day,231,ipsd)) else finishdate end as newfinishdate,ROW_NUMBER() over(partition by memid order by Startdate,Finishdate) as rn from coverage_CTE
)t1                        
)t2 
group by memid having(sum(gaps)+sum(startgap)<=1) and sum(coveragedays)>=292 and sum(anchor)>0


CREATE CLUSTERED INDEX ix_amm_contenroll ON #amm_contenroll ([memid]);
	
update #ammdataset set CE=1 from #ammdataset ds join #amm_contenroll ce on ds.patient_id=ce.MemID;

-- Numerator 
-- Effective Acute Phase Treatment AMM2

DROP TABLE IF EXISTS #amm_numamm2;
	
	CREATE table #amm_numamm2
	(
		Memid varchar(50)
		
	);
	

Insert into #amm_numamm2
select distinct Memid from(
select *,datediff(day,Enddate,nextdate) as gap from(
select *,lead(Prservdate,1) over(partition by Memid order by Prservdate,Enddate) as nextdate from(

select Memid,PrServDate,PDaysSup,convert(varchar,cast(dateadd(day,cast(PDaysSup as INT),PrServDate) as date),112) as Enddate from HEDIS_PHARM t1 where MEASURE_ID=@meas  and ndc in(select code from MEDICATION_LIST_TO_CODES where Medication_List_Name='Antidepressant Medications') and   Prservdate between (select t2.ipsd from #amm_memipsd t2 where t1.MemID=t2.memid) and (select convert(varchar,cast(dateadd(day,115,t2.ipsd) as date),112) from #amm_memipsd t2 where t1.MemID=t2.memid)
Union all

select Memid,Startdate,qty,Edate from hedis_pharm_c t1 where measure_id=@meas and Rxnorm in(select code from MEDICATION_LIST_TO_CODES where Medication_List_Name='Antidepressant Medications') and StartDate between (select t2.ipsd from #amm_memipsd t2 where t1.MemID=t2.memid) and (select convert(varchar,cast(dateadd(day,115,t2.ipsd) as date),112) from #amm_memipsd t2 where t1.MemID=t2.memid) 
          
)f1 
)f2
)f3  group by Memid having sum(gap)<=31 and sum(cast(PDaysSup as Int))>=84


CREATE CLUSTERED INDEX ix_amm_contenroll ON #amm_numamm2 ([memid]);
	
update #ammdataset set num=1 from #ammdataset ds join #amm_numamm2 n2 on ds.patient_id=n2.MemID and ds.meas='AMM2';
	

--  Effective Continuation Phase Treatment 


DROP TABLE IF EXISTS #amm_numamm3;
	
	CREATE table #amm_numamm3
	(
		Memid varchar(50)
		
	);
Insert into #amm_numamm3
select distinct Memid from(
select *,datediff(day,Enddate,nextdate) as gap from(
select *,lead(Prservdate,1) over(partition by Memid order by Prservdate,Enddate) as nextdate from(

select Memid,PrServDate,PDaysSup,convert(varchar,cast(dateadd(day,cast(PDaysSup as INT),PrServDate) as date),112) as Enddate from HEDIS_PHARM t1 where MEASURE_ID=@meas  and ndc in(select code from MEDICATION_LIST_TO_CODES where Medication_List_Name='Antidepressant Medications') and   Prservdate between (select t2.ipsd from #amm_memipsd t2 where t1.MemID=t2.memid) and (select convert(varchar,cast(dateadd(day,232,t2.ipsd) as date),112) from #amm_memipsd t2 where t1.MemID=t2.memid)

Union all

select Memid,Startdate,qty,Edate from hedis_pharm_c t1 where measure_id=@meas and Rxnorm in(select code from MEDICATION_LIST_TO_CODES where Medication_List_Name='Antidepressant Medications') and StartDate between (select t2.ipsd from #amm_memipsd t2 where t1.MemID=t2.memid) and (select convert(varchar,cast(dateadd(day,232,t2.ipsd) as date),112) from #amm_memipsd t2 where t1.MemID=t2.memid) 
         
)f1 
)f2 

)f3  group by Memid having sum(gap)<=52 and sum(cast(PDaysSup as Int))>=180


CREATE CLUSTERED INDEX ix_amm_contenroll ON #amm_numamm3 ([memid]);
	
update #ammdataset set num=1 from #ammdataset ds join #amm_numamm3 n3 on ds.patient_id=n3.MemID and ds.meas='AMM3';

-- Identify Patients with Major depression within 60 days

drop table if exists #amm_majordepressionclaim;
CREATE TABLE #amm_majordepressionclaim (
    memid varchar(100),
	servicedate varchar(8),
	claimid INT
	
);

Insert into #amm_majordepressionclaim
select MemID,Date_S as servicedate,claimid from HEDIS_VISIT v
where MEASURE_ID=@meas  and  Date_S!='' and hcfapos!=81  
and (
	Diag_I_1 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Major Depression'))
	or
	Diag_I_2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Major Depression'))
	or
	Diag_I_3 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Major Depression'))
	or
	Diag_I_4 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Major Depression'))
	or
	Diag_I_5 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Major Depression'))
	or
	Diag_I_6 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Major Depression'))
	or
	Diag_I_7 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Major Depression'))
	or
	Diag_I_8 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Major Depression'))
	or
	Diag_I_9 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Major Depression'))
	or
	Diag_I_10 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Major Depression'))
	)


CREATE CLUSTERED INDEX idx_amm_majordepressionclaim ON #amm_majordepressionclaim ([memid],[servicedate],[claimid]);



drop table if exists #amm_majordepressionvst;
CREATE TABLE #amm_majordepressionvst (
    memid varchar(100),
	servicedate varchar(8)
	
	
);

Insert into #amm_majordepressionvst
select Memid,sdate as servicedate from hedis_diag where measure_id=@meas and sdate!='' and dcode in(select code from VALUESET_TO_CODE where Value_Set_Name in('Major Depression'))

CREATE CLUSTERED INDEX idx_amm_majordepressionvst ON #amm_majordepressionvst ([memid],[servicedate]);


-- Create Member list who has visits within 60 of IPSD
drop table if exists #amm_rexcldmemlist;
CREATE TABLE #amm_rexcldmemlist (
    memid varchar(100),
	
);


Insert into #amm_rexcldmemlist
select distinct Memid from(

-- Inpatient visit with Major depression


select distinct f1.Memid from(
select Memid,Date_S as servicedate,claimid from hedis_visit t1  where measure_id=@meas  and date_s!='' and hcfapos!=81  and rev in(select code from valueset_to_code where value_set_name='Inpatient Stay') and (Date_Adm between (select dateadd(day,-60,t2.ipsd) from #amm_memipsd t2 where t1.memid=t2.memid ) and (select dateadd(day,60,t2.ipsd) from #amm_memipsd t2 where t1.memid=t2.memid) or Date_Disch between (select dateadd(day,-60,t2.ipsd) from #amm_memipsd t2 where t1.memid=t2.memid ) and (select dateadd(day,60,t2.ipsd) from #amm_memipsd t2 where t1.memid=t2.memid))
)f1
Join #amm_majordepressionclaim c on c.memid=f1.memid and c.claimid=f1.claimid

--Acute Inpatient with Major depression
Union all

select distinct f1.Memid from(
select Memid,Date_S as servicedate,ClaimID from hedis_visit t1 where measure_id=@meas  and date_s!='' and hcfapos!=81  and CPT in(select code from valueset_to_code where value_set_name='Acute Inpatient') and date_s between (select dateadd(day,-60,t2.ipsd) from #amm_memipsd t2 where t1.memid=t2.memid ) and (select dateadd(day,60,t2.ipsd) from #amm_memipsd t2 where t1.memid=t2.memid)
)f1
Join #amm_majordepressionclaim c on c.memid=f1.memid and c.claimid=f1.claimid

Union all

select distinct f1.memid from(
select Memid,sdate as servicedate from HEDIS_VISIT_E t1 where measure_id=@meas and sdate!='' and activity in(select code from valueset_to_code where value_set_name='Acute Inpatient') and sdate between (select dateadd(day,-60,t2.ipsd) from #amm_memipsd t2 where t1.memid=t2.memid ) and (select dateadd(day,60,t2.ipsd) from #amm_memipsd t2 where t1.memid=t2.memid)
)f1
join #amm_majordepressionvst v on v.memid=f1.memid and v.servicedate=f1.servicedate

Union All

-- Nonacute Inpatient with Major depression

select distinct f1.Memid from(
select Memid,Date_S as servicedate,ClaimID from hedis_visit t1 where measure_id=@meas  and date_s!='' and hcfapos!=81  and CPT in(select code from valueset_to_code where value_set_name='Nonacute Inpatient') and date_s between (select dateadd(day,-60,t2.ipsd) from #amm_memipsd t2 where t1.memid=t2.memid ) and (select dateadd(day,60,t2.ipsd) from #amm_memipsd t2 where t1.memid=t2.memid)
)f1
Join #amm_majordepressionclaim c on c.memid=f1.memid and c.claimid=f1.claimid

Union all

select distinct f1.memid from(
select Memid,sdate as servicedate from HEDIS_VISIT_E t1 where measure_id=@meas and sdate!='' and activity in(select code from valueset_to_code where value_set_name='nonacute Inpatient') and sdate between (select dateadd(day,-60,t2.ipsd) from #amm_memipsd t2 where t1.memid=t2.memid ) and (select dateadd(day,60,t2.ipsd) from #amm_memipsd t2 where t1.memid=t2.memid)
)f1
join #amm_majordepressionvst v on v.memid=f1.memid and v.servicedate=f1.servicedate

-- Unspecified visit with Outpatient POS and Major depression

Union all

select distinct f1.memid from(
select distinct Memid,Date_S as servicedate,ClaimID from hedis_visit t1 where measure_id=@meas  and date_s!=''  and 
CPT in(select code from valueset_to_code where value_set_name='Visit Setting Unspecified') and hcfapos in(select code from valueset_to_code where value_set_name='Outpatient POS') and date_s between (select dateadd(day,-60,t2.ipsd) from #amm_memipsd t2 where t1.memid=t2.memid ) and (select dateadd(day,60,t2.ipsd) from #amm_memipsd t2 where t1.memid=t2.memid)
)f1
join #amm_majordepressionclaim c on c.memid=f1.MemID and c.claimid=f1.ClaimID


-- BH Outpatient Value Set with Major Depression Value Set

Union all

select distinct f1.memid from(
select memid,date_s as servicedate,ClaimID from hedis_visit t1 where measure_id=@meas and date_s!='' and hcfapos!=81  and
(
CPT in(select code from valueset_to_code where value_set_name='BH Outpatient')
or
HCPCS in(select code from valueset_to_code where value_set_name='BH Outpatient')
or
REV in(select code from valueset_to_code where value_set_name='BH Outpatient')
) and date_s between (select dateadd(day,-60,t2.ipsd) from #amm_memipsd t2 where t1.memid=t2.memid ) and (select dateadd(day,60,t2.ipsd) from #amm_memipsd t2 where t1.memid=t2.memid)
)f1
join #amm_majordepressionclaim c on c.memid=f1.MemID and c.claimid=f1.ClaimID

Union all


select distinct f1.memid from(
select memid,sdate as servicedate from Hedis_proc t1 where measure_id=@meas and sdate!='' and pstatus='EVN' and pcode in(select code from valueset_to_code where value_set_name='BH Outpatient') and sdate between (select dateadd(day,-60,t2.ipsd) from #amm_memipsd t2 where t1.memid=t2.memid ) and (select dateadd(day,60,t2.ipsd) from #amm_memipsd t2 where t1.memid=t2.memid)

Union all

Select memid,sdate from hedis_visit_e t1 where measure_id=@meas and sdate!='' and activity in(select code from valueset_to_code where value_set_name='BH Outpatient') and sdate between (select dateadd(day,-60,t2.ipsd) from #amm_memipsd t2 where t1.memid=t2.memid ) and (select dateadd(day,60,t2.ipsd) from #amm_memipsd t2 where t1.memid=t2.memid)
)f1
join #amm_majordepressionvst v on v.memid=f1.MemID and v.servicedate=f1.servicedate




-- Visit Setting Unspecified Value Set with Partial Hospitalization POS Value Set with Major Depression Value Set

Union all

select distinct f1.memid from(
select distinct Memid,Date_S as servicedate,ClaimID from hedis_visit t1 where measure_id=@meas  and date_s!=''  and 
CPT in(select code from valueset_to_code where value_set_name='Visit Setting Unspecified') and hcfapos in(select code from valueset_to_code where value_set_name='Partial Hospitalization POS') and date_s between (select dateadd(day,-60,t2.ipsd) from #amm_memipsd t2 where t1.memid=t2.memid ) and (select dateadd(day,60,t2.ipsd) from #amm_memipsd t2 where t1.memid=t2.memid)
)f1
join #amm_majordepressionclaim c on c.memid=f1.MemID and c.claimid=f1.ClaimID


--Visit Setting Unspecified Value Set with Community Mental Health Center POS Value Set with Major Depression Value Set

Union all

select distinct f1.memid from(
select distinct Memid,Date_S as servicedate,ClaimID from hedis_visit t1 where measure_id=@meas  and date_s!=''  and 
CPT in(select code from valueset_to_code where value_set_name='Visit Setting Unspecified') and hcfapos in(select code from valueset_to_code where value_set_name='Community Mental Health Center POS') and date_s between (select dateadd(day,-60,t2.ipsd) from #amm_memipsd t2 where t1.memid=t2.memid ) and (select dateadd(day,60,t2.ipsd) from #amm_memipsd t2 where t1.memid=t2.memid)
)f1
join #amm_majordepressionclaim c on c.memid=f1.MemID and c.claimid=f1.ClaimID

-- Partial Hospitalization or Intensive Outpatient Value Set with Major Depression Value Set.

Union all

select distinct f1.memid from(
select memid,date_s as servicedate,ClaimID from hedis_visit t1 where measure_id=@meas and date_s!='' and hcfapos!=81  and
(
HCPCS in(select code from valueset_to_code where value_set_name='Partial Hospitalization or Intensive Outpatient')
or
REV in(select code from valueset_to_code where value_set_name='Partial Hospitalization or Intensive Outpatient')
) and date_s between (select dateadd(day,-60,t2.ipsd) from #amm_memipsd t2 where t1.memid=t2.memid ) and (select dateadd(day,60,t2.ipsd) from #amm_memipsd t2 where t1.memid=t2.memid)
)f1
join #amm_majordepressionclaim c on c.memid=f1.MemID and c.claimid=f1.ClaimID

Union all

select distinct f1.memid from(
select memid,sdate as servicedate from Hedis_proc t1 where measure_id=@meas and sdate!='' and pstatus='EVN' and pcode in(select code from valueset_to_code where value_set_name='Partial Hospitalization or Intensive Outpatient') and sdate between (select dateadd(day,-60,t2.ipsd) from #amm_memipsd t2 where t1.memid=t2.memid ) and (select dateadd(day,60,t2.ipsd) from #amm_memipsd t2 where t1.memid=t2.memid)

Union all

Select memid,sdate from hedis_visit_e t1 where measure_id=@meas and sdate!='' and activity in(select code from valueset_to_code where value_set_name='Partial Hospitalization or Intensive Outpatient') and sdate between (select dateadd(day,-60,t2.ipsd) from #amm_memipsd t2 where t1.memid=t2.memid ) and (select dateadd(day,60,t2.ipsd) from #amm_memipsd t2 where t1.memid=t2.memid)
)f1
join #amm_majordepressionvst v on v.memid=f1.MemID and v.servicedate=f1.servicedate

Union all

-- Electroconvulsive Therapy Value Set with Major Depression Value Set


select distinct f1.Memid from(
select memid,claimid from hedis_visit t1 where measure_id=@meas and date_s!='' and hcfapos!=81  and
(
CPT in(select code from valueset_to_code where value_set_name='Electroconvulsive Therapy')
or
Proc_I_1 in(select code from valueset_to_code where value_set_name='Electroconvulsive Therapy')
or
Proc_I_2 in(select code from valueset_to_code where value_set_name='Electroconvulsive Therapy')
or
Proc_I_3 in(select code from valueset_to_code where value_set_name='Electroconvulsive Therapy')
or
Proc_I_4 in(select code from valueset_to_code where value_set_name='Electroconvulsive Therapy')
or
Proc_I_5 in(select code from valueset_to_code where value_set_name='Electroconvulsive Therapy')
or
Proc_I_6 in(select code from valueset_to_code where value_set_name='Electroconvulsive Therapy')
) and date_s between (select dateadd(day,-60,t2.ipsd) from #amm_memipsd t2 where t1.memid=t2.memid ) and (select dateadd(day,60,t2.ipsd) from #amm_memipsd t2 where t1.memid=t2.memid)
)f1
join #amm_majordepressionclaim c on c.memid=f1.MemID and c.claimid=f1.ClaimID


Union all

select distinct f1.memid from(
select memid,sdate as servicedate from Hedis_proc t1 where measure_id=@meas and sdate!='' and pstatus='EVN' and pcode in(select code from valueset_to_code where value_set_name='Electroconvulsive Therapy') and sdate between (select dateadd(day,-60,t2.ipsd) from #amm_memipsd t2 where t1.memid=t2.memid ) and (select dateadd(day,60,t2.ipsd) from #amm_memipsd t2 where t1.memid=t2.memid)

Union all

Select memid,sdate from hedis_visit_e t1 where measure_id=@meas and sdate!='' and activity in(select code from valueset_to_code where value_set_name='Electroconvulsive Therapy') and sdate between (select dateadd(day,-60,t2.ipsd) from #amm_memipsd t2 where t1.memid=t2.memid ) and (select dateadd(day,60,t2.ipsd) from #amm_memipsd t2 where t1.memid=t2.memid)
)f1
join #amm_majordepressionvst v on v.memid=f1.MemID and v.servicedate=f1.servicedate


-- Transcranial Magnetic Stimulation Value Set with Major Depression Value Set.

Union all

select distinct f1.Memid from(
select memid,claimid from hedis_visit t1 where measure_id=@meas and date_s!='' and hcfapos!=81  and

CPT in(select code from valueset_to_code where value_set_name='Transcranial Magnetic Stimulation')
 and date_s between (select dateadd(day,-60,t2.ipsd) from #amm_memipsd t2 where t1.memid=t2.memid ) and (select dateadd(day,60,t2.ipsd) from #amm_memipsd t2 where t1.memid=t2.memid)
)f1
join #amm_majordepressionclaim c on c.memid=f1.MemID and c.claimid=f1.ClaimID


Union all

select distinct f1.memid from(
select memid,sdate as servicedate from Hedis_proc t1 where measure_id=@meas and sdate!='' and pstatus='EVN' and pcode in(select code from valueset_to_code where value_set_name='Transcranial Magnetic Stimulation') and sdate between (select dateadd(day,-60,t2.ipsd) from #amm_memipsd t2 where t1.memid=t2.memid ) and (select dateadd(day,60,t2.ipsd) from #amm_memipsd t2 where t1.memid=t2.memid)

Union all

Select memid,sdate from hedis_visit_e t1 where measure_id=@meas and sdate!='' and activity in(select code from valueset_to_code where value_set_name='Transcranial Magnetic Stimulation') and sdate between (select dateadd(day,-60,t2.ipsd) from #amm_memipsd t2 where t1.memid=t2.memid ) and (select dateadd(day,60,t2.ipsd) from #amm_memipsd t2 where t1.memid=t2.memid)
)f1
join #amm_majordepressionvst v on v.memid=f1.MemID and v.servicedate=f1.servicedate

-- Visit Setting Unspecified Value Set with Telehealth POS Value Set with Major Depression Value Set.

Union All

select distinct f1.memid from(
select distinct Memid,Date_S as servicedate,ClaimID from hedis_visit t1 where measure_id=@meas  and date_s!=''  and 
CPT in(select code from valueset_to_code where value_set_name='Visit Setting Unspecified') and hcfapos in(select code from valueset_to_code where value_set_name='Telehealth POS') and date_s between (select dateadd(day,-60,t2.ipsd) from #amm_memipsd t2 where t1.memid=t2.memid ) and (select dateadd(day,60,t2.ipsd) from #amm_memipsd t2 where t1.memid=t2.memid)
)f1
join #amm_majordepressionclaim c on c.memid=f1.MemID and c.claimid=f1.ClaimID


-- observation visit (Observation Value Set) with any diagnosis of major depression (Major Depression Value Set).

Union All

select distinct f1.Memid from(
select memid,claimid from hedis_visit t1 where measure_id=@meas and date_s!='' and hcfapos!=81  and

CPT in(select code from valueset_to_code where value_set_name='Observation')
 and date_s between (select dateadd(day,-60,t2.ipsd) from #amm_memipsd t2 where t1.memid=t2.memid ) and (select dateadd(day,60,t2.ipsd) from #amm_memipsd t2 where t1.memid=t2.memid)
)f1
join #amm_majordepressionclaim c on c.memid=f1.MemID and c.claimid=f1.ClaimID


Union all

select distinct f1.memid from(
select memid,sdate as servicedate from Hedis_proc t1 where measure_id=@meas and sdate!='' and pstatus='EVN' and pcode in(select code from valueset_to_code where value_set_name='Observation') and sdate between (select dateadd(day,-60,t2.ipsd) from #amm_memipsd t2 where t1.memid=t2.memid ) and (select dateadd(day,60,t2.ipsd) from #amm_memipsd t2 where t1.memid=t2.memid)

Union all

Select memid,sdate from hedis_visit_e t1 where measure_id=@meas and sdate!='' and activity in(select code from valueset_to_code where value_set_name='Observation') and sdate between (select dateadd(day,-60,t2.ipsd) from #amm_memipsd t2 where t1.memid=t2.memid ) and (select dateadd(day,60,t2.ipsd) from #amm_memipsd t2 where t1.memid=t2.memid)
)f1
join #amm_majordepressionvst v on v.memid=f1.MemID and v.servicedate=f1.servicedate

--  ED visit (ED Value Set) with any diagnosis of major depression (Major Depression Value Set).

Union All


select distinct f1.Memid from(
select memid,claimid from hedis_visit t1 where measure_id=@meas and date_s!='' and hcfapos!=81  and
(
CPT in(select code from valueset_to_code where value_set_name='ED')
or
REV in(select code from valueset_to_code where value_set_name='ED')
)
 and date_s between (select dateadd(day,-60,t2.ipsd) from #amm_memipsd t2 where t1.memid=t2.memid ) and (select dateadd(day,60,t2.ipsd) from #amm_memipsd t2 where t1.memid=t2.memid)
)f1
join #amm_majordepressionclaim c on c.memid=f1.MemID and c.claimid=f1.ClaimID


Union all

select distinct f1.memid from(
select memid,sdate as servicedate from Hedis_proc t1 where measure_id=@meas and sdate!='' and pstatus='EVN' and pcode in(select code from valueset_to_code where value_set_name='ED') and sdate between (select dateadd(day,-60,t2.ipsd) from #amm_memipsd t2 where t1.memid=t2.memid ) and (select dateadd(day,60,t2.ipsd) from #amm_memipsd t2 where t1.memid=t2.memid)

Union all

Select memid,sdate from hedis_visit_e t1 where measure_id=@meas and sdate!='' and activity in(select code from valueset_to_code where value_set_name='ED') and sdate between (select dateadd(day,-60,t2.ipsd) from #amm_memipsd t2 where t1.memid=t2.memid ) and (select dateadd(day,60,t2.ipsd) from #amm_memipsd t2 where t1.memid=t2.memid)
)f1
join #amm_majordepressionvst v on v.memid=f1.MemID and v.servicedate=f1.servicedate


-- ED visit with any diagnosis of major depression: Visit Setting Unspecified Value Set with ED POS Value Set with Major Depression Value Set.

Union ALL


select distinct f1.memid from(
select distinct Memid,Date_S as servicedate,ClaimID from hedis_visit t1 where measure_id=@meas  and date_s!=''  and 
CPT in(select code from valueset_to_code where value_set_name='Visit Setting Unspecified') and hcfapos in(select code from valueset_to_code where value_set_name='ED POS') and date_s between (select dateadd(day,-60,t2.ipsd) from #amm_memipsd t2 where t1.memid=t2.memid ) and (select dateadd(day,60,t2.ipsd) from #amm_memipsd t2 where t1.memid=t2.memid)
)f1
join #amm_majordepressionclaim c on c.memid=f1.MemID and c.claimid=f1.ClaimID


-- telephone visit (Telephone Visits Value Set) with any diagnosis of major depression (Major Depression Value Set).

Union ALL

select distinct f1.Memid from(
select memid,claimid from hedis_visit t1 where measure_id=@meas and date_s!='' and hcfapos!=81  and
(
CPT in(select code from valueset_to_code where value_set_name='Telephone Visits')
)
 and date_s between (select dateadd(day,-60,t2.ipsd) from #amm_memipsd t2 where t1.memid=t2.memid ) and (select dateadd(day,60,t2.ipsd) from #amm_memipsd t2 where t1.memid=t2.memid)
)f1
join #amm_majordepressionclaim c on c.memid=f1.MemID and c.claimid=f1.ClaimID


Union all

select distinct f1.memid from(
Select memid,sdate as servicedate from hedis_visit_e t1 where measure_id=@meas and sdate!='' and activity in(select code from valueset_to_code where value_set_name='Telephone Visits') and sdate between (select dateadd(day,-60,t2.ipsd) from #amm_memipsd t2 where t1.memid=t2.memid ) and (select dateadd(day,60,t2.ipsd) from #amm_memipsd t2 where t1.memid=t2.memid)
)f1
join #amm_majordepressionvst v on v.memid=f1.MemID and v.servicedate=f1.servicedate

-- e-visit or virtual check-in (Online Assessments Value Set) with any diagnosis of major depression (Major Depression Value Set).

Union ALL


select distinct f1.Memid from(
select memid,claimid from hedis_visit t1 where measure_id=@meas and date_s!='' and hcfapos!=81  and
(
CPT in(select code from valueset_to_code where value_set_name='Online Assessments')
or
HCPCS in(select code from valueset_to_code where value_set_name='Online Assessments')
)
 and date_s between (select dateadd(day,-60,t2.ipsd) from #amm_memipsd t2 where t1.memid=t2.memid ) and (select dateadd(day,60,t2.ipsd) from #amm_memipsd t2 where t1.memid=t2.memid)
)f1
join #amm_majordepressionclaim c on c.memid=f1.MemID and c.claimid=f1.ClaimID


Union all

select distinct f1.memid from(

Select memid,sdate as servicedate from hedis_visit_e t1 where measure_id=@meas and sdate!='' and activity in(select code from valueset_to_code where value_set_name='Online Assessments') and sdate between (select dateadd(day,-60,t2.ipsd) from #amm_memipsd t2 where t1.memid=t2.memid ) and (select dateadd(day,60,t2.ipsd) from #amm_memipsd t2 where t1.memid=t2.memid)
)f1
join #amm_majordepressionvst v on v.memid=f1.MemID and v.servicedate=f1.servicedate
)t1

CREATE CLUSTERED INDEX idx_amm_rexcldmemlist ON #amm_rexcldmemlist ([memid]);

update #ammdataset set rexcld=0 from #ammdataset ds join #amm_rexcldmemlist re on ds.patient_id=re.MemID;


---- Hospice Exclusion

	drop table if exists #amm_hospicemembers;
	CREATE table #amm_hospicemembers
	(
		Memid varchar(50)
		
	);

	

	Insert into #amm_hospicemembers
	select distinct t1.MemID from
	(
	select Memid from HEDIS_OBS where Measure_id=@meas and date !='' and date between @ce_startdt and @ce_enddt and OCode IN(select code from VALUESET_TO_CODE where Value_Set_Name in('Hospice Encounter','Hospice Intervention'))
	Union all
	Select Memid from HEDIS_VISIT_E where Measure_id=@meas and sdate!='' and sdate between @ce_startdt and @ce_enddt and activity IN(select code from VALUESET_TO_CODE where Value_Set_Name in('Hospice Encounter','Hospice Intervention'))

	Union all

	select Beneficiary_id as Memid from Hedis_MMDF where measure_id=@meas and Run_date!='' and Run_date between @ce_startdt and @ce_enddt and Hospice='Y'

	Union all

	select MemID from HEDIS_VISIT v
	where MEASURE_ID=@meas and HCFAPOS!=81 and Date_S!='' and Date_S between @ce_startdt and @ce_enddt
	and (
	Diag_I_1 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or
	Diag_I_2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or
	Diag_I_3 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or
	Diag_I_4 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or
	Diag_I_5 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or
	Diag_I_6 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or
	Diag_I_7 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or
	Diag_I_8 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or
	Diag_I_9 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or
	Diag_I_10 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or
	CPT in(select code from VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or
	CPT2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or
	HCPCS in(select code from VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or 
	Proc_I_1 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or 
	Proc_I_2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or 
	Proc_I_3 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or 
	Proc_I_4 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or 
	Proc_I_5 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or 
	Proc_I_6 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	
	or
	Rev in(select code from VALUESET_TO_CODE where code_system='UBREV' and Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	)
	)t1
	
	CREATE CLUSTERED INDEX idx_hospicemembers ON #amm_hospicemembers ([memid]);
	update #ammdataset set rexcl=1 from #ammdataset ds join #amm_hospicemembers hos on hos.memid=ds.patient_id;
	
-- Insert data in Output Table
	select @runid=max(RUN_ID) from HEDIS_MEASURE_OUTPUT where measure_id=@meas;

	if(@runid>=1)
	Begin
		SET @runid=@runid+1;
	End
	Else
	Begin
		SET @runid=1;
	END

	Insert into HEDIS_MEASURE_OUTPUT(memid,Meas,payer,CE,Event,Epop,Excl,Num,Rexcl,RExclD,Age,Gender,Measure_ID,Measurement_year,RUN_ID)
	SELECT patient_id AS memid,meas,payer,CE,EVENT,CASE WHEN CE=1 AND EVENT=1 AND rexcl=0 and rexcld=0 THEN 1 ELSE 0 END AS epop,excl,num,rexcl,rexcld,cast(age as Int) AS age,patient_gender AS gender,@meas as Measure_ID,@meas_year,@runid FROM #ammdataset
	

GO
