SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON


CREATE proc [dbo].[KPI_UHN_ACEIandARBAdherence_48]
AS

BEGIN
DECLARE @INFO VARCHAR(8000)
					,	@TRUE BIT = 1
					,	@FALSE BIT = 0
					,	@DB_ID INT = DB_ID()
					,	@LOGID INT = 0
					,	@PROC VARCHAR(500)=OBJECT_SCHEMA_NAME(@@PROCID)+'.'+OBJECT_NAME(@@PROCID)
					,	@PERF_START DATETIME
					,	@PERF_DURATION INT
					,	@PERF_ROW INT
					,	@RC	INT
					;

		EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, @INFO=@INFO, @LOG2ID=@LOGID OUT;
		DECLARE @PROCFULLNAME VARCHAR(500) = DB_NAME()+'.'+@PROC;
		----EXEC dbo.SP_MI_UTIL_SEND_MAIL @PROCFULLNAME, 'STARTED', 1, @ECHO=0; 
	DECLARE @MSG VARCHAR(1000)
	DECLARE @PERF_START_PROC DATETIME 
	DECLARE @PERF_RPS NUMERIC(10,2) 
	DECLARE @ROWS INT 
	DECLARE @PERF_ROWS INT 
	SET @PERF_ROWS = 0 
	SET @PERF_START = GETDATE() 
	SET @PERF_START_PROC = GETDATE() 

BEGIN TRY


-- Declare Variables
Declare @rundate date=GetDate();
declare @meas_year varchar(4)=Year(Dateadd(month,-2,@rundate))
declare @rootId INT=159
declare @attributiondate Date=DATEADD(month, DATEDIFF(month, 0, Dateadd(month,-2,@rundate)), 0)


Declare @startDate Date;
Declare @enddate date;
Declare @quarter varchar(20)
Declare @reportId INT;
Declare @measure_id varchar(10);
Declare @result numeric;
Declare @gap numeric;
Declare @numerator float;
Declare @denominator float;
Declare @target INT;
Declare @domain varchar(100);
Declare @subdomain varchar(100);
Declare @measuretype varchar(100);
Declare @measurename varchar(100);
Declare @reporttype varchar(100);

Declare @ce_startdt Date;
Declare @ce_startdt1 Date;
Declare @ce_novdt Date;
Declare @ce_enddt Date;
Declare @ce_enddt1 Date;

-- Set Measure dates
SET @ce_startdt=concat(@meas_year,'-01-01');
SET @ce_novdt=concat(@meas_year,'-11-30');
SET @ce_enddt=concat(@meas_year,'-12-31');
SET @ce_startdt1=concat(@meas_year-1,'-01-01');
SET @ce_enddt1=concat(@meas_year-1,'-12-31');


Set @reporttype='Physician'
Set @measurename='Medication Adherence for Hypertension (ACEi or ARB)'
--Set @startDate=DATEADD(yy, DATEDIFF(yy, 0,Dateadd(month,-2,GETDATE())), 0) 
--Set @enddate=eomonth(Dateadd(month,-2,GetDate()))
--Set @quarter=Concat(Year(@enddate),' - Q',DATEPART(q, @enddate))
set @target=89
Set @domain='Clinical Quality / Chronic Condition Management'
Set @subdomain='Medication Management'
Set @measuretype='UHN'
Set @measure_id='48'


-- Get ReportId from Report_Details

	
	exec GetReportDetail @rundate=@rundate,@rootId=@rootId,@startDate=@startDate output,@enddate=@enddate output,@quarter=@quarter output,@reportId=@reportId output


Drop table if exists #48_deceasedmembers
Create table #48_deceasedmembers
(
	EMPI varchar(100)
)
Insert into #48_deceasedmembers
select EMPI from deceasedmembers(@rootId,@ce_startdt,@enddate)



-- Identify Members with Hypertension
Drop table if exists #48_Hypertension;
Create table #48_Hypertension
(
	EMPI varchar(100)
)
Insert into #48_Hypertension
Select distinct
	EMPI
From
(
	Select
		EMPI
	From GetDiagnosis(@rootId,'1900-01-01',@enddate,'Hypertension')

	Union all

	Select
		EMPI
	From GetDiagnosis(@rootId,'1900-01-01',@enddate,'Essential Hypertension')
)t1


	  

-- Identify Members with ACE and ARB Medication
Drop table if exists #48_ACEandARBMeds;
Create table #48_ACEandARBMeds
(
	EMPI varchar(100),
	FILL_DATE Date,
	SUPPLY_DAYS INT,
	Code varchar(20)
)
Insert into #48_ACEandARBMeds
Select distinct
	m.EMPI
	,m.FILL_DATE
	,Cast(cast(m.SUPPLY_DAYS as numeric) as INT) as SupplyDays
	,m.MEDICATION_CODE
FROM Medication m
join #48_Hypertension h on m.EMPI=h.EMPI
left outer join #48_deceasedmembers d on m.EMPI=d.EMPI
where Root_companies_id=@rootId and
	  FILL_DATE between @ce_startdt and @enddate and
	  Medication_code in
	  (
		Select code from HDS.MEDICATION_LIST_TO_CODES where Medication_list_name='ACE Inhibitor and ARB Medications'
	  )
	  and
	  d.EMPI is null and
	  m.MED_DATA_SRC not in('Flatfiles','Flatfiles-Gaps')









-- Identify Inpatient and Skilled Nursing Stays
Drop Table if exists #48_stays
Create Table #48_stays
(
	EMPI varchar(100),
	ADM_DATE Date,
	DIS_DATE Date	
)
Insert into #48_stays
Select 
	* 
from
(
	Select 
		EMPI
		,coalesce(ADM_DATE,FROM_DATE) as ADM_DATE
		,coalesce(DIS_DATE,TO_DATE) as DIS_DATE
	From ClaimLine
	where Root_Companies_id=@rootId and
		  ISNULL(POS,0)!='81' and
		  (
			  RIGHT('0000'+CAST(Trim(REV_CODE) AS VARCHAR(4)),4) in
			  (
				select code from HDS.ValueSet_to_code where code_system='UBREV' and Value_Set_Name in('Inpatient Stay','Skilled Nursing Stay')
			  )
			  or
			  RIGHT('0000'+CAST(Trim(UB_BILL_TYPE) AS VARCHAR(4)),4) in
			  (
				select code from HDS.ValueSet_to_code where code_system='UBTOB' and Value_Set_Name ='Skilled Nursing Stay'
			  )
		  )
)t1
Where DIS_DATE between @ce_startdt and @enddate
	  
	

-- Exclude Medications which were prescribed during stay
Drop table if exists #48_ACEandARBMeds_withexcl
Create table #48_ACEandARBMeds_withexcl
(
	EMPI varchar(100),
	FILL_DATE Date,
	SUPPLY_DAYS INT,
	Code varchar(20)

)
Insert into #48_ACEandARBMeds_withexcl
Select 
	m.EMPI,
	m.FILL_DATE,
	m.SUPPLY_DAYS,
	m.Code 
from #48_ACEandARBMeds m
left outer join #48_stays s on m.EMPI=s.EMPI and m.FILL_DATE between s.ADM_DATE and s.DIS_DATE
where s.EMPI is null






DROP TABLE IF EXISTS #48_medmemlist;
CREATE table #48_medmemlist
(
	EMPI varchar(100),
	pdc INT			
);
Insert into #48_medmemlist
select 
	EMPI
	,sum(pdc) as pdc 
from
(
	-- adjust DOS for year end after summing
	select
		EMPI
		,Servicedate
		,case 
			when dateadd(day,ISNULL(NULLIF(calcdos,''),1)-1,servicedate) >=@enddate then datediff(day,servicedate,@enddate)+1 
			else calcdos 
		end as pdc 
	from
	(
		select 
			EMPI
			,servicedate
			,sum(calcdaysofsupply) as calcdos 
		from
		(
			-- SUM the DOS supply after adjustment
			select distinct 
				EMPI
				,servicedate
				,calcdaysofsupply 
			from
			(
				-- Adjust DOS to accomodate year end
				select 
					*
					,case 
						when dateadd(day,ISNULL(NULLIF(calcdos,''),1)-1,servicedate) >=@enddate then datediff(day,servicedate,@enddate)+1 
						else calcdos 
					end as calcdaysofsupply 
				from
				(
					select 
						*
						,case 
						-- different Medication on same date ,take one with more DOS
						when servicedate=nextmeddate and daysofsupply>nextdos then daysofsupply
						when servicedate=nextmeddate and daysofsupply<nextdos then nextdos
						when servicedate=prevmeddate and daysofsupply>prevdos then daysofsupply
						when servicedate=prevmeddate and daysofsupply<prevdos then prevdos
						-- Adjust DOS to accomodate overlap with next record
						when servicedate!=nextmeddate and enddate>= ISNULL(nextmeddate,enddate) then daysofsupply-(datediff(day,ISNULL(nextmeddate,enddate),enddate)+1)
						else daysofsupply
						end as calcdos
					from
					(
					-- Get Previous and next record for further processing
						select 
							*
							,convert(varchar,cast(dateadd(day,ISNULL(NULLIF(daysofsupply,''),1)-1,servicedate) as date),23) as enddate
							,isnull(lead(servicedate,1) over(partition by EMPI order by servicedate,daysofsupply),servicedate) as nextmeddate
							,isnull(lead(daysofsupply,1) over(partition by EMPI order by servicedate,daysofsupply),daysofsupply) as nextdos
							,isnull(lag(servicedate,1) over(partition by EMPI order by servicedate,daysofsupply),servicedate) as prevmeddate
							,isnull(lag(daysofsupply,1) over(partition by EMPI order by servicedate,daysofsupply),daysofsupply) as prevdos
						
						from
						(
							Select
								EMPI,
								servicedate,
								daysofsupply,
								Code
							From
							(
								select
									EMPI
									,FILL_DATE as servicedate
									,sum(SUPPLY_DAYS) as daysofsupply
									,Code
								from #48_ACEandARBMeds_withexcl 
								Group By EMPI,FILL_DATE,Code
							)t1
						
						)t3
					)t4
				)t5 
			)t6 
		)t7 
		group by 
			EMPI
			,servicedate
	)t8 
)t9
group by 
	EMPI 
order by EMPI



drop table if exists #48_medipsdlist;
CREATE table #48_medipsdlist
(
	EMPI varchar(100),
	ipsd Date,
	treatmentperiod INT			
);
Insert into #48_medipsdlist
select 
	z1.EMPI
	,min(z1.servicedate) as ipsd
	,datediff(day,min(z1.servicedate),@enddate)+1 as treatmentperiod 
from
(
	select 
		EMPI
		,FILL_DATE as servicedate
		,case 
			when convert(varchar,cast(DATEADD(day,SUPPLY_DAYS-1,FILL_DATE) as date),112) >@enddate then @enddate 
			else convert(varchar,cast(DATEADD(day,SUPPLY_DAYS-1,FILL_DATE) as date),112) 
		end as Enddate
		,MEDICATION_CODE as code  
	from MEDICATION 
	where 
		ROOT_COMPANIES_ID=@rootId  and 
		FILL_DATE between @ce_startdt and @enddate and 
		MEDICATION_CODE in
		(
			Select code from HDS.MEDICATION_LIST_TO_CODES where Medication_list_name='ACE Inhibitor and ARB Medications'
		)
		
)z1 
group by 
	z1.EMPI




-- Calculate Medication Adherence
Drop table if exists #48_numeratorset
Create table #48_numeratorset
(
	EMPI varchar(100)
	
)
Insert into #48_numeratorset
select distinct 
	f1.EMPI 
from
(
	select 
		s1.EMPI
		,s1.treatmentperiod
		,s2.pdc
		,Round(((cast(pdc as float)/cast(treatmentperiod as float))*100),0) as adherence
		,cast(pdc as float)/cast(treatmentperiod as float)*100 as raw
	from #48_medipsdlist s1
	join #48_medmemlist s2 on 
		s1.EMPI=s2.EMPI
	
)f1 where f1.adherence>=80 



-- Identify Exclusions
-- hospice  
drop table if exists #48_exclusions;
CREATE table #48_exclusions
(
	EMPI varchar(100)
		
)
Insert into #48_exclusions
select EMPI from hospicemembers(@rootId,@ce_startdt,@enddate)








-- Create Dataset
-- Create the output as required
Drop table if exists #48_dataset
Create Table #48_dataset
(
	EMPI varchar(100),
	Provider_Id varchar(20),
	PCP_NPI varchar(20),
	PCP_NAME varchar(200),
	Practice_Name varchar(200),
	Specialty varchar(100),
	Measure_id varchar(20),
	Measure_Name varchar(100),
	Payer varchar(50),
	PayerId varchar(100),
	MEM_FNAME varchar(100),
	MEM_MName varchar(50),
	MEM_LNAME varchar(100),
	MEM_DOB Date,
	MEM_GENDER varchar(20),
	Enrollment_Status varchar(20),
	Last_visit_date Date,
	Product_Type varchar(50),
	Num bit,
	Den bit,
	Excl bit,
	Rexcl bit,
	Report_Id INT,
	ReportType varchar(20),
	Report_Quarter varchar(20),
	Period_Start_Date Date,
	Period_End_Date Date,
	Root_Companies_Id INT

)
Insert into #48_dataset
select distinct
	d.EMPI
	,a.AmbulatoryPCPNPI as Provider_Id 
	,a.AmbulatoryPCPNPI as PCP_NPI
	,a.AmbulatoryPCPName as PCP_NAME
	,a.AmbulatoryPCPPractice as Practice_Name
	,a.AmbulatoryPCPSpecialty as Specialty
	,@measure_id as Measure_id
	,@measurename as Measure_Name
	,a.DATA_SOURCE as Payer
	,a.PayerId
	,a.MemberFirstName as MEM_FNAME
	,a.MemberMiddleName as MEM_MName
	,a.MemberLastName as MEM_LNAME
	,a.MemberDOB
	,m.Gender as MEM_GENDER
	,a.EnrollmentStatus
	,a.AmbulatoryPCPRecentVisit as Last_visit_date
	,mm.PAYER_TYPE as ProductType
	,0 as Num
	,1 as Den
	,0 as Excl
	,0 as Rexcl
	,@reportId
	,@reporttype
	,@quarter 
	,@startDate 
	,@enddate 
	,@rootId
from #48_ACEandARBMeds_withexcl d
join RPT.PCP_ATTRIBUTION a on d.EMPI=a.EMPI and a.ReportId=@reportId
Join open_empi_master m on d.EMPI=m.EMPI_ID
left outer join MEMBER_MONTH mm on d.EMPI=mm.EMPI and mm.MEMBER_MONTH_START_DATE=@attributiondate
where a.AssignedStatus='Assigned'


update ds Set num=1 from #48_dataset ds join #48_numeratorset n on ds.EMPI=n.EMPI
update ds Set Rexcl=1 from #48_dataset ds join #48_exclusions n on ds.EMPI=n.EMPI



-- Insert data into Measure Detailed Line
Delete from RPT.MEASURE_DETAILED_LINE where MEASURE_ID=@measure_id and Report_id=@reportId and ROOT_COMPANIES_ID=@rootId;

Insert into RPT.MEASURE_DETAILED_LINE(Provider_Id,PCP_NPI,PCP_NAME,Practice_Name,Specialty,Measure_id,Measure_Name,Payer,PayerId,MEM_FNAME,MEM_MName,MEM_LNAME,MEM_DOB,MEM_GENDER,ENROLLMENT_STATUS,Last_visit_date,Product_Type,Num,Den,Excl,Rexcl,Report_Id,ReportType,Report_Quarter,Period_Start_date,Period_end_Date,Root_Companies_id,EMPI,Measure_Type)
Select Provider_Id,PCP_NPI,PCP_NAME,Practice_Name,Specialty,Measure_id,Measure_Name,Payer,PayerId,MEM_FNAME,MEM_MName,MEM_LNAME,MEM_DOB,MEM_GENDER,Enrollment_Status,Last_visit_date,Product_Type,Num,Den,Excl,Rexcl,Report_Id,ReportType,Report_Quarter,Period_Start_date,Period_end_Date,Root_Companies_id,d.EMPI,@measuretype
From #48_dataset d




-- Insert data into Provider Scorecard
Delete from RPT.PROVIDER_SCORECARD where MEASURE_ID=@measure_id and Report_id=@reportId and ROOT_COMPANIES_ID=@rootId

Insert into RPT.PROVIDER_SCORECARD(Provider_Id,PCP_NPI,PCP_NAME,Specialty,Practice_Name,Measure_id,Measure_Name,Measure_Title,MEASURE_SUBTITLE,Measure_Type,NUM_COUNT,DEN_COUNT,Excl_Count,Rexcl_Count,Gaps,Result,Target,To_Target,Report_Id,ReportType,Report_Quarter,Period_Start_Date,Period_End_Date,Root_Companies_Id)
Select 
	Provider_Id
	,PCP_NPI
	,PCP_NAME
	,Specialty
	,Practice_Name
	,Measure_id
	,Measure_Name
	,Measure_Title
	,MEASURE_SUBTITLE
	,Measure_Type
	,SUM(Cast(NUM_COUNT as INT)) as NUM_COUNT
	,SUM(Cast(DEN_COUNT as INT)) as DEN_COUNT
	,SUM(Cast(Excl_Count as INT)) as Excl_Count
	,Sum(Cast(Rexcl_count as INT)) as Rexcl_Count
	,sum(Cast(DEN_Excl as INT)) - SUM(Cast(NUM_COUNT as INT)) as Gaps
	,Case
		when SUM(Cast(DEN_Excl as Float))>0 Then Round((SUM(cast(NUM_COUNT as Float))/ISNULL(SUM(Cast(DEN_Excl as Float)),1))*100,2)
		else 0
	end as Result
	,@target as Target
	,Case	
		when ((SUM(Cast(DEN_Excl as INT)))*(cast(@target*0.01 as float))) - SUM(Cast(NUM_COUNT as INT))>0 Then CEILING(((SUM(Cast(DEN_Excl as INT)))*(cast(@target*0.01 as float))) - SUM(Cast(NUM_COUNT as INT)))
		Else 0
	end as To_Target
	,Report_Id
	,ReportType
	,Report_Quarter
	,Period_Start_Date
	,Period_End_Date
	,Root_Companies_Id
From
(
	Select distinct
		m.EMPI
		,a.NPI as Provider_id
		,a.NPI as PCP_NPI
		,a.Prov_Name as PCP_Name
		,a.Specialty
		,a.Practice as Practice_Name
		,m.Measure_id
		,m.Measure_Name
		,l.measure_Title
		,l.Measure_SubTitle
		,'Calculated' as Measure_Type
		,Case
			when NUM=1 and excl=0 and rexcl=0 Then 1
			else 0
		end as NUM_COUNT
		,DEN as DEN_COUNT
		,Case
			When DEN=1 and Excl=0 and Rexcl=0 Then 1
			else 0
		End as Den_excl
		,Excl as Excl_Count
		,Rexcl as Rexcl_count
		,Report_Id
		,l.ReportType
		,Report_Quarter
		,Period_Start_Date
		,Period_End_Date
		,m.Root_Companies_Id
		,l.Target
	From RPT.MEASURE_DETAILED_LINE m
	Join RPT.ConsolidatedAttribution_Snapshot a on
		m.EMPI=a.EMPI
	Join RFT.UHN_measuresList l on
		m.Measure_Id=l.measure_id
	Join RFT.UHN_MeasureSpecialtiesMapping s on
		a.Specialty=s.Specialty and
		m.MEASURE_ID=s.Measure_id
	where Enrollment_Status='Active' and
		  a.NPI!='' and
		  m.MEASURE_ID=@measure_id and
		  REPORT_ID=@reportId 
)t1
Group by Provider_Id,PCP_NPI,PCP_NAME,Specialty,Practice_Name,Measure_id,Measure_Name,Measure_Title,MEASURE_SUBTITLE,Report_Id,ReportType,Report_Quarter,Period_Start_Date,Period_End_Date,Root_Companies_Id,Measure_Type,Target

SET @PERF_ROW = @@ROWCOUNT
		SET @PERF_DURATION = DATEDIFF(MINUTE,@PERF_START,GETDATE());
		EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, 'Enrollment Completed', @PERF_ROW, @DURATION_IN_MIN=@PERF_DURATION, @LOG2ID=@LOGID;
		EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, @LOG2ID=@LOGID, @END_FLAG=1;

END TRY
BEGIN CATCH
		BEGIN
			EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, 'FATAL ERROR', @LOG2ID=@LOGID;
			RAISERROR ('',16,0)
			RETURN 16
        END
	END CATCH
RETURN 0
END

GO
