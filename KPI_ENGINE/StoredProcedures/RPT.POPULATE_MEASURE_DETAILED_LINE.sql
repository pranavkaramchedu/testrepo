SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
CREATE PROCEDURE RPT.POPULATE_MEASURE_DETAILED_LINE

AS
BEGIN

INSERT INTO [KPI_ENGINE].[RPT].[MEASURE_DETAILED_LINE_TEST]
(
	   [PROVIDER_ID]
      ,[PCP_NPI]
      ,[PCP_NAME]
      ,[PRACTICE_NAME]
      ,[SPECIALTY]
      ,[MEASURE_ID]
      ,[MEASURE_NAME]
      ,[MEASURE_TYPE]
      ,[PAYERID]
      ,[MEMBER_ID]
      ,[MEM_FNAME]
      ,[MEM_MNAME]
      ,[MEM_LNAME]
      ,[MEM_DOB]
      ,[MEM_GENDER]
      ,[ENROLLMENT_STATUS]
      ,[LAST_VISIT_DATE]
      ,[PRODUCT_TYPE]
      ,[NUM]
      ,[DEN]
      ,[EXCL]
      ,[REXCL]
      ,[REPORT_ID]
      ,[ReportType]
      ,[REPORT_RUN_DATE]
      ,[Report_Quarter]
      ,[PERIOD_START_DATE]
      ,[PERIOD_END_DATE]
      ,[ROOT_COMPANIES_ID]
)
SELECT 
	   [PROVIDER_ID]
      ,[PCP_NPI]
      ,[PCP_NAME]
      ,[PRACTICE_NAME]
      ,[SPECIALTY]
      ,[MEASURE_ID]
      ,[MEASURE_NAME]
      ,'HEDIS' AS [MEASURE_TYPE]
      ,[PAYERID]
      ,[MEMBER_ID]
      ,[MEM_FNAME]
      ,[MEM_MNAME]
      ,[MEM_LNAME]
      ,[MEM_DOB]
      ,[MEM_GENDER]
      ,[ENROLLMENT_STATUS]
      ,[LAST_VISIT_DATE]
      ,[PRODUCT_TYPE]
      ,[NUM]
      ,[DEN]
      ,[EXCL]
      ,[REXCL]
      ,1 AS [REPORT_ID]
      ,'Physician' AS [ReportType]
      ,CONVERT(DATE,GETDATE()) AS [REPORT_RUN_DATE]
      ,CAST(DATEPART(YEAR, GETDATE()) AS NVARCHAR(10)) + ' - Q' + CAST(DATEPART(QUARTER, GETDATE()) AS NVARCHAR(10)) AS [Report_Quarter]
      ,'2021-01-01' AS [PERIOD_START_DATE]
      ,'2020-03-01' AS [PERIOD_END_DATE]
      ,[ROOT_COMPANIES_ID]
  FROM [KPI_ENGINE].[RPT].[MEASURE_DETAILED_LINE]


END
GO
