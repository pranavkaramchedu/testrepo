SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON






CREATE proc [dbo].[KPI_HEDIS_CCS] 
AS


BEGIN
DECLARE @INFO VARCHAR(8000)
					,	@TRUE BIT = 1
					,	@FALSE BIT = 0
					,	@DB_ID INT = DB_ID()
					,	@LOGID INT = 0
					,	@PROC VARCHAR(500)=OBJECT_SCHEMA_NAME(@@PROCID)+'.'+OBJECT_NAME(@@PROCID)
					,	@PERF_START DATETIME
					,	@PERF_DURATION INT
					,	@PERF_ROW INT
					,	@RC	INT
					;

		EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, @INFO=@INFO, @LOG2ID=@LOGID OUT;
		DECLARE @PROCFULLNAME VARCHAR(500) = DB_NAME()+'.'+@PROC;
		----EXEC dbo.SP_MI_UTIL_SEND_MAIL @PROCFULLNAME, 'STARTED', 1, @ECHO=0; 
	DECLARE @MSG VARCHAR(1000)
	DECLARE @PERF_START_PROC DATETIME 
	DECLARE @PERF_RPS NUMERIC(10,2) 
	DECLARE @ROWS INT 
	DECLARE @PERF_ROWS INT 
	SET @PERF_ROWS = 0 
	SET @PERF_START = GETDATE() 
	SET @PERF_START_PROC = GETDATE() 

BEGIN TRY


-- Declare Variables
declare @rundate Date=GetDate()
declare @meas_year varchar(4)=Year(Dateadd(month,-2,@rundate))
declare @rootId varchar(10)='159'


Declare @meas varchar(10)='CCS';
Declare @ce_startdt date;
Declare @ce_startdt1 Date;
Declare @ce_startdt2 Date;
Declare @ce_startdt4 Date;
Declare @ce_middt Date;
Declare @ce_enddt Date;
Declare @ce_enddt1 Date;
Declare @ce_enddt2 Date;
Declare @ce_enddt4 Date;
Declare @runid INT;

Declare @startDate Date;
Declare @enddate date;
Declare @quarter varchar(20);
Declare @measure_id varchar(10);
Declare @target INT;
Declare @domain varchar(100);
Declare @subdomain varchar(100);
Declare @measuretype varchar(100);
Declare @measurename varchar(100);
Declare @reporttype varchar(100);
Declare @reportId INT;




-- Set Measure dates
SET @ce_startdt=concat(@meas_year,'-01-01');
SET @ce_middt=concat(@meas_year,'-06-30');
SET @ce_enddt=concat(@meas_year,'-12-31');
SET @ce_enddt1=concat(@meas_year-1,'-12-31');
SET @ce_enddt2=concat(@meas_year-2,'-12-31');
SET @ce_enddt4=concat(@meas_year-4,'-12-31');
SET @ce_startdt1=concat(@meas_year-1,'-01-01');
SET @ce_startdt2=concat(@meas_year-2,'-01-01');
SET @ce_startdt4=concat(@meas_year-4,'-01-01');


/*
Set @reporttype='Physician'
Set @measurename='Hypertension: BP Control <140/90 (18-85); GAP Open/Closed by last Documented B/P for CY'
--Set @startDate=DATEADD(yy, DATEDIFF(yy, 0,Dateadd(month,-2,GETDATE())), 0) 
--Set @enddate=eomonth(Dateadd(month,-2,GetDate()))
--Set @quarter=Concat(Year(@enddate),' - Q',DATEPART(q, @enddate))
set @target=82
Set @domain='Clinical Quality / Chronic Condition Management'
Set @subdomain='Hypertension'
Set @measuretype='UHN'
Set @measure_id='12'
*/

-- Create Eligible Patient LIST

drop table if exists #ccs_memlist; 
CREATE TABLE #ccs_memlist (
    EMPI varchar(100)
    
)
insert into #ccs_memlist
SELECT DISTINCT 
	en.EMPI 
FROM open_empi_master gm
join Enrollment en on 
	en.EMPI=gm.EMPI_ID and 
	en.ROOT_COMPANIES_ID=gm.ROOT_COMPANIES_ID
WHERE 
	en.ROOT_COMPANIES_ID=@rootId and
	en.EFF_DATE<=@ce_enddt AND 
	TERM_DATE>=@ce_startdt2 AND 
	YEAR(@ce_enddt)-YEAR(Date_of_Birth) BETWEEN 24 AND 64 and
	gm.Gender='F'
	ORDER BY 1;


-- Create Temp Table for enrollment

drop table if exists #ccs_tmpsubscriber;
CREATE TABLE #ccs_tmpsubscriber (
    EMPI varchar(100),
    dob Date,
	gender varchar(1),
	Age INT,
	payer varchar(50),
	StartDate Date,
	EndDate Date
);
insert into #ccs_tmpsubscriber
SELECT distinct
	en.EMPI
	,gm.Date_of_Birth
	,gm.Gender
	,YEAR(@ce_enddt)-YEAR(Date_of_Birth) as age
	,en.PAYER_TYPE
	,en.EFF_DATE
	,en.TERM_DATE  
FROM Open_EMPI_MASTER gm
join ENROLLMENT en on en.EMPI=gm.EMPI_ID and 
					  en.ROOT_COMPANIES_ID=gm.ROOT_COMPANIES_ID
WHERE 
	en.ROOT_COMPANIES_ID=@rootId and
	en.EFF_DATE<=@ce_enddt AND 
	TERM_DATE>=@ce_startdt2 AND 
	YEAR(@ce_enddt)-YEAR(Date_of_Birth) BETWEEN 24 AND 64 and
	gm.Gender='F'
	ORDER BY 
		en.EMPI
		,en.EFF_DATE
		,en.TERM_DATE;

		


-- Clear #ccsdataset

drop table if exists #ccsdataset;

CREATE TABLE #ccsdataset (
  EMPI varchar(100) NOT NULL,
  [meas] varchar(20) DEFAULT NULL,
  [payer] varchar(100) DEFAULT NULL,
  [gender] varchar(45) NOT NULL,
  [age] INT,
  [orec] smallint DEFAULT NULL,
  [lis] smallint DEFAULT '0',
  [rexcl] smallint DEFAULT '0',
  [rexcld] smallint DEFAULT '0',
  [event] smallint DEFAULT '0',
  [CE] smallint DEFAULT '0',
  [excl] smallint DEFAULT '0',
  [num] smallint DEFAULT '0'
) ;
Insert into #ccsdataset(EMPI,meas,Payer,Age,gender)
Select distinct
	EMPI
	,'CCS'
	,pm.PayerMapping
	,age
	,gender
From
(
	Select distinct
		EMPI
		,gender
		,age
		,case 
				when (Payer in('HMO','CEP','POS','PPO','EPO') and nxtpayer in('MD','MLI','MRB'))  then TRIM(Payer)
				When (Payer in('HMO','CEP','POS','PPO','EPO') and prvpayer in('MD','MLI','MRB'))  then TRIM(Payer)
				When (Payer in('MD','MLI','MRB','MDE') and nxtpayer in('HMO','CEP','POS','PPO','EPO'))  then TRIM(nxtpayer)
				When (Payer in('MD','MLI','MRB','MDE') and prvpayer in('HMO','CEP','POS','PPO','EPO'))  then TRIM(prvpayer)
				when (Payer in('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP','MR') and nxtpayer in('HMO','CEP','POS','PPO','EPO'))  then TRIM(Payer)
				When (Payer in('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP','MR') and prvpayer in('HMO','CEP','POS','PPO','EPO'))  then TRIM(Payer)
				When (Payer in('HMO','CEP','POS','PPO','EPO') and nxtpayer in('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP','MR'))  then TRIM(nxtpayer)
				When (Payer in('HMO','CEP','POS','PPO','EPO') and prvpayer in('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP','MR'))  then TRIM(prvpayer)
				When (Payer in('MDE','MRB','MD','MLI') and prvpayer in('MCR','MCS','MP','MC','SN1','SN2','SN3','MR'))  then TRIM(payer)
				When (Payer in('MCR','MCS','MP','MC','SN1','SN2','SN3','MR') and nxtpayer in('MDE','MRB','MD','MLI'))  then TRIM(nxtpayer)
				When (Payer in('MCR','MCS','MP','MC','SN1','SN2','SN3','MR') and prvpayer in('MDE','MRB','MD','MLI'))  then TRIM(prvpayer)
				When (Payer in('MDE','MRB','MD','MLI') and nxtpayer in('MCR','MCS','MP','MC','SN1','SN2','SN3','MR'))  then TRIM(payer)
				else Payer 
				
			end as newpayer
	From
	(
		select
			*
			,isnull(lead(Payer) over(partition by EMPI order by Payer),Payer) as nxtpayer
			,isnull(lag(Payer) over(partition by EMPI order by Payer),Payer) as prvpayer
		From 
		(
			select
				EMPI
				,payer
				,gender
				,age
			From
			(
				select 
					EMPI
					,StartDate
					,EndDate
					,TRIM(payer) as payer
					,gender
					,age
					,RANK() over(partition by EMPI order by StartDate desc,EndDate Desc) as rn 
				from #ccs_tmpsubscriber 
				where  
					StartDate<=@ce_enddt
					--and EMPI=100346
			)t1
			where 
				rn=1
		)t2
		
	)t3
)t4
Join HDS.HEDIS_PAYER_MAPPING pm on 
	t4.newpayer=pm.payer and 
	pm.Measure_id='CCS'
Order by 1



-- Continuous Enrollment
				
	DROP TABLE IF EXISTS #ccs_contenroll1;
	CREATE table #ccs_contenroll1
	(
		EMPI varchar(100),
		
	);
	Insert into #ccs_contenroll1
	select 
		EMPI
	from GetContinuousEnrolledEMPI(@rootId,@ce_startdt,@ce_enddt,45,0)

	DROP TABLE IF EXISTS #ccs_contenroll2;
	CREATE table #ccs_contenroll2
	(
		EMPI varchar(100),
		
	);
	Insert into #ccs_contenroll2
	select 
		EMPI
	from GetContinuousEnrolledEMPI(@rootId,@ce_startdt1,@ce_enddt1,45,1)

	DROP TABLE IF EXISTS #ccs_contenroll3;
	CREATE table #ccs_contenroll3
	(
		EMPI varchar(100),
		
	);
	Insert into #ccs_contenroll3
	select 
		EMPI
	from GetContinuousEnrolledEMPI(@rootId,@ce_startdt2,@ce_enddt2,45,1)




	update #ccsdataset set CE=0;

	update #ccsdataset 
		set CE=1 
	from #ccsdataset ds 
	join #ccs_contenroll1 ce1 on 
		ds.EMPI=ce1.EMPI
	Where
		Payer='MCD'
	
	update #ccsdataset 
		set CE=1 
	from #ccsdataset ds 
	join #ccs_contenroll1 ce1 on 
		ds.EMPI=ce1.EMPI
	join #ccs_contenroll2 ce2 on 
		ds.EMPI=ce2.EMPI
	join #ccs_contenroll3 ce3 on 
		ds.EMPI=ce3.EMPI
	Where
		payer!='MCD'



	-- Required Exclusion

	drop table if exists #ccs_reqdexcl
	CREATE table #ccs_reqdexcl
	(
		EMPI varchar(100)
			
	)
	insert into #ccs_reqdexcl
	Select
		EMPI
	From palliativecare(@rootId,@ce_startdt,@ce_enddt)
	
	update #ccsdataset set rexcld=1 from #ccsdataset ds join #ccs_reqdexcl re on ds.EMPI=re.EMPI;


-- Hospice Exclusion

	drop table if exists #ccs_hospicemembers;
	CREATE table #ccs_hospicemembers
	(
		EMPI varchar(100)
		
	)
	Insert into #ccs_hospicemembers
	Select
		EMPI
	From hospicemembers(@rootId,@ce_startdt,@ce_enddt)
	
	update #ccsdataset set rexcl=1 from #ccsdataset ds join #ccs_hospicemembers hos on hos.EMPI=ds.EMPI;
			
				
-- Identify Numerator Set
Drop Table if exists #ccs_numset1
Create Table #ccs_numset1
(
	EMPI varchar(100)
)
Insert into #ccs_numset1
Select distinct
	EMPI
From
(

	Select
		EMPI
	From DIAGNOSIS
	where
		ROOT_COMPANIES_ID=@rootId and
		DIAG_START_DATE between @ce_startdt2 and @ce_enddt and
		DIAG_CODE in
		(
			select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Cervical Cytology Lab Test','Cervical Cytology Result or Finding')
		)
	

	Union all

	Select
		EMPI
	From PROCEDURES
	where
		ROOT_COMPANIES_ID=@rootId and
		PROC_START_DATE between @ce_startdt2 and @ce_enddt and
		ISNULL(PROC_STATUS,'EVN')!='INT' and
		PROC_CODE in
		(
			select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Cervical Cytology Lab Test','Cervical Cytology Result or Finding')
		)
		

	Union all

	Select
		EMPI
	From Lab
	where
		ROOT_COMPANIES_ID=@rootId and
		ResultDate between @ce_startdt2 and @ce_enddt and
		ResultCode in
		(
			select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Cervical Cytology Lab Test','Cervical Cytology Result or Finding')
		)
		

)t1	


Update ds 
	set ds.Num=1
From #ccsdataset ds
join #ccs_numset1 n1 on
	ds.EMPI=n1.EMPI
Where
	ds.age between 24 and 64



-- •	Women 30–64 years of age as of December 31 of the measurement year who had cervical high-risk human papillomavirus (hrHPV) testing (High Risk HPV Lab Test Value Set, High Risk HPV Test Result or Finding Value Set) during the measurement year or the four years prior to the measurement year and who were 30 years or older on the date of the test. 

Drop Table if exists #ccs_HPVlist
Create Table #ccs_HPVlist
(
	EMPI varchar(100),
	ServiceDate Date
)
Insert into #ccs_HPVlist
Select distinct
	EMPI
	,ServiceDate
From
(

	Select
		EMPI
		,DIAG_START_DATE as ServiceDate
	From DIAGNOSIS
	where
		ROOT_COMPANIES_ID=@rootId and
		DIAG_START_DATE between @ce_startdt4 and @ce_enddt and
		DIAG_CODE in
		(
			select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('High Risk HPV Lab Test','High Risk HPV Test Result or Finding')
		)

	Union all

	Select
		EMPI
		,PROC_START_DATE as ServiceDate
	From PROCEDURES
	where
		ROOT_COMPANIES_ID=@rootId and
		PROC_START_DATE between @ce_startdt4 and @ce_enddt and
		ISNULL(PROC_STATUS,'EVN')!='INT' and
		PROC_CODE in
		(
			select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('High Risk HPV Lab Test','High Risk HPV Test Result or Finding')
		)
	

	Union all

	Select
		EMPI
		,ResultDate as ServiceDate
	From Lab
	where
		ROOT_COMPANIES_ID=@rootId and
		ResultDate between @ce_startdt4 and @ce_enddt and
		ResultCode in
		(
			select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('High Risk HPV Lab Test','High Risk HPV Test Result or Finding')
		)

)t1	


Drop table if exists #ccs_numset2
Create Table #ccs_numset2
(
	EMPI varchar(100)
)
Insert into #ccs_numset2
Select
	EMPI
From
(
	Select 
		h.EMPI
		,CASE 
			WHEN dateadd(year, datediff (year, dob,ServiceDate), dob) > ServiceDate THEN datediff(year, dob, ServiceDate) - 1
			ELSE datediff(year, dob, ServiceDate)
		END as Age
	from #ccs_HPVlist h
	join #ccs_tmpsubscriber t on h.EMPI=t.EMPI 
	--where h.empi=100015
)t1
Where
	age between 30 and 64


	Update ds set ds.Num=1 from #ccsdataset ds join #ccs_numset2 n2 on ds.EMPI=n2.EMPI


-- Optional Exclusion 1 and 3
	drop table if exists #ccs_optexcl;
	CREATE table #ccs_optexcl
	(
		EMPI varchar(100)
				
	)
	insert into #ccs_optexcl
	select distinct 
		EMPI 
	from
	(
	-- Optional Exclusion Kidney 

		Select 
			EMPI
		From GetDiagnosis(@rootId,'1900-01-01',@ce_enddt,'Absence of Cervix Diagnosis')
		
		Union all

		Select 
			EMPI
		From GetDiagnosis(@rootId,'1900-01-01',@ce_enddt,'Hysterectomy With No Residual Cervix')
		
		
		Union All

		Select 
			EMPI
		From GetProcedures(@rootId,'1900-01-01',@ce_enddt,'Hysterectomy With No Residual Cervix')
		--Where EMPi=100560

		Union all

		Select
			EMPI
		From GETICDPCS(@rootId,'1900-01-01',@ce_enddt,'Hysterectomy With No Residual Cervix')
		--where EMPI='100560'
		

		
	)t1

	
	update #ccsdataset set excl=0;
	update ds set excl=1 from #ccsdataset ds
	join #ccs_optexcl oe on oe.EMPI=ds.EMPI ;


	

-- Get ReportId from Report_Details

		exec GetReportDetail @rundate=@rundate,@rootId=@rootId,@startDate=@startDate output,@enddate=@enddate output,@quarter=@quarter output,@reportId=@reportId output


	Delete from HDS.HEDIS_MEASURE_OUTPUT  where MEASURE_ID=@meas and RUN_ID=@reportId and ROOT_COMPANIES_ID=@rootId
	Insert into HDS.HEDIS_MEASURE_OUTPUT(memid,Meas,payer,CE,Event,Epop,Excl,Num,Rexcl,RExclD,Age,Gender,Measure_ID,Measurement_year,RUN_ID,ROOT_COMPANIES_ID)
	SELECT 
		EMPI
		,meas
		,payer
		,CE
		,EVENT
		,CASE 
			WHEN CE=1 AND rexcl=0 and rexcld=0 and Payer in('CEP','HMO','MCD','MEP','MMO','MOS','MPO','POS','PPO') THEN 1 
			ELSE 0 
		END AS epop
		,excl
		,num
		,rexcl
		,rexcld
		,age
		,gender
		,'CCS'
		,@meas_year
		,@reportId
		,@rootId 
	FROM #ccsdataset


	/*

	-- Insert data into Measure Detailed Line
	Delete from RPT.MEASURE_DETAILED_LINE where MEASURE_ID=@measure_id and REPORT_ID=@reportId and ROOT_COMPANIES_ID=@rootId;
	Insert into RPT.MEASURE_DETAILED_LINE(Provider_Id,PCP_NPI,PCP_NAME,Practice_Name,Specialty,Measure_id,Measure_Name,Payer,PayerId,MEM_FNAME,MEM_MName,MEM_LNAME,MEM_DOB,MEM_GENDER,ENROLLMENT_STATUS,Last_visit_date,Product_Type,Num,Den,Excl,Rexcl,CE,Event,Epop,Report_Id,ReportType,Report_Quarter,Period_Start_date,Period_end_Date,Root_Companies_id,EMPI,MEASURE_TYPE,Code,DateofService)
	Select 
		a.AmbulatoryPCPNPI as Provider_Id 
		,a.AmbulatoryPCPNPI
		,a.AmbulatoryPCPName as PCP_NAME
		,a.AmbulatoryPCPPractice as Practice_Name
		,a.AmbulatoryPCPSpecialty as Specialty
		,@measure_id as Measure_id
		,@measurename as Measure_Name
		,a.DATA_SOURCE as Payer
		,a.PayerId
		,a.MemberFirstName as MEM_FNAME
		,a.MemberMiddleName as MEM_MName
		,a.MemberLastName as MEM_LNAME
		,a.MemberDOB
		,a.MEM_GENDER
		,a.EnrollmentStatus
		,a.AmbulatoryPCPRecentVisit as Last_visit_date
		,d.Payer
		,Num
		,1 as Den
		,Excl
		,Rexcl
		,CE
		,Event
		,CASE 
			WHEN CE=1  AND rexcl=0 and rexcld=0 and Event=1 THEN 1 
			ELSE 0 
		END AS epop
		,@reportId
		,@reporttype
		,@quarter
		,@startDate
		,@enddate
		,@rootId
		,d.EMPI as EMPI
		,@measuretype
		,nd.Code
		,nd.ServiceDate
	From #cbpdataset d
	join KPI_ENGINE.RPT.PCP_ATTRIBUTION a on d.EMPI=a.EMPI and a.Reportid=@reportId
	Left outer join #numlist nd on d.EMPI=nd.EMPI
	Where Event=1
--	where a.AmbulatoryPCPSpecialty in('Family Medicine','General Practice','Gynecology','Internal Medicine','Obstetrics & Gynecology')



	-- Insert data into Provider Scorecard
	Delete from RPT.PROVIDER_SCORECARD  where MEASURE_ID=@measure_id and REPORT_ID=@reportId and ROOT_COMPANIES_ID=@rootId;
Insert into RPT.PROVIDER_SCORECARD(Provider_Id,PCP_NPI,PCP_NAME,Specialty,Practice_Name,Measure_id,Measure_Name,Measure_Title,MEASURE_SUBTITLE,Measure_Type,NUM_COUNT,DEN_COUNT,Excl_Count,Rexcl_Count,Gaps,Result,Target,To_Target,Report_Id,ReportType,Report_Quarter,Period_Start_Date,Period_End_Date,Root_Companies_Id)
Select 
	Provider_Id
	,PCP_NPI
	,PCP_NAME
	,Specialty
	,Practice_Name
	,Measure_id
	,Measure_Name
	,Measure_Title
	,MEASURE_SUBTITLE
	,Measure_Type
	,SUM(Cast(NUM_COUNT as INT)) as NUM_COUNT
	,SUM(Cast(DEN_COUNT as INT)) as DEN_COUNT
	,SUM(Cast(Excl_Count as INT)) as Excl_Count
	,Sum(Cast(Rexcl_count as INT)) as Rexcl_Count
	,sum(Cast(DEN_Excl as INT)) - SUM(Cast(NUM_COUNT as INT)) as Gaps
	,Case
		when SUM(Cast(DEN_Excl as Float))>0 Then Round((SUM(cast(NUM_COUNT as Float))/SUM(Cast(DEN_Excl as Float)))*100,2)
		else 0
	end as Result
	,@target as Target
	,Case
		when ((SUM(Cast(DEN_Excl as INT)))*(cast(@target*0.01 as float))) - SUM(Cast(NUM_COUNT as INT))>0 Then ROUND(((SUM(Cast(DEN_Excl as INT)))*(cast(@target*0.01 as float))) - SUM(Cast(NUM_COUNT as INT)),0)
		Else 0
	end as To_Target
	,Report_Id
	,ReportType
	,Report_Quarter
	,Period_Start_Date
	,Period_End_Date
	,Root_Companies_Id
From
(
	Select distinct
		EMPI
		,Provider_Id
		,PCP_NPI
		,PCP_NAME
		,Specialty
		,Practice_Name
		,Measure_id
		,Measure_Name
		,@domain as Measure_Title
		,@subdomain as MEASURE_SUBTITLE
		,@measuretype as Measure_Type
		,Case
			when NUM=1 and excl=0 and rexcl=0 and Event=1 Then 1
			else 0
		end as NUM_COUNT
		,DEN as DEN_COUNT
		,Case
			When DEN=1 and Excl=0 and Rexcl=0 and Event=1 Then 1
			else 0
		End as Den_excl
		,Excl as Excl_Count
		,Rexcl as Rexcl_count
		,Report_Id
		,ReportType
		,Report_Quarter
		,Period_Start_Date
		,Period_End_Date
		,Root_Companies_Id
	From RPT.MEASURE_DETAILED_LINE
	where Enrollment_Status='Active' and
		  MEASURE_ID=@measure_id and
		  REPORT_ID=@reportId
)t1
Group by Provider_Id,PCP_NPI,PCP_NAME,Specialty,Practice_Name,Measure_id,Measure_Name,Measure_Title,MEASURE_SUBTITLE,Report_Id,ReportType,Report_Quarter,Period_Start_Date,Period_End_Date,Root_Companies_Id,Measure_Type



	
GO


*/


	SET @PERF_ROW = @@ROWCOUNT
		SET @PERF_DURATION = DATEDIFF(MINUTE,@PERF_START,GETDATE());
		EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, 'Enrollment Completed', @PERF_ROW, @DURATION_IN_MIN=@PERF_DURATION, @LOG2ID=@LOGID;
		EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, @LOG2ID=@LOGID, @END_FLAG=1;

END TRY
BEGIN CATCH
		BEGIN
			EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, 'FATAL ERROR', @LOG2ID=@LOGID;
			RAISERROR ('',16,0)
			RETURN 16
        END
	END CATCH
RETURN 0
END

GO
