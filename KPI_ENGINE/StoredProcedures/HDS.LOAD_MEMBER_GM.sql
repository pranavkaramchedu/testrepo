SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
CREATE PROC HDS.LOAD_MEMBER_GM
AS
---TRUNCATE TABLE [HDS].[HEDIS_MEMBER_GM]
SELECT MEMBER_ID AS [MemID]
      ,MEM_GENDER AS [Gender]
      ,MEM_DOB AS [DOB]
      ,MEM_LNAME AS [LName]
      ,MEM_FNAME AS [FName]
      ,MEM_MNAME AS [MMidName]
      ,NULL AS [SubID]
      ,MEM_ADDR1 AS [Add1]
      ,MEM_ADDR2 AS [Add2]
      ,MEM_CITY AS [City]
      ,MEM_STATE AS [State]
      ,MEM_ZIP AS [MZip]
      ,MEM_PHONE AS [MPhone]
      ,NULL AS [PFirstName]
      ,NULL AS [PMidName]
      ,NULL AS [PLastName]
      ,MEM_RACE AS [Race]
      ,CASE WHEN MEM_ETHNICITY=1 THEN '11' WHEN MEM_ETHNICITY=2 THEN '12' WHEN MEM_ETHNICITY=3 THEN '19' END AS [Ethn]
      ,'25' AS [RaceDS]
      ,'95' AS [EthnDS]
      ,CASE WHEN MEM_LANGUAGE=1 THEN '31' WHEN MEM_LANGUAGE in (2,3,4,8) THEN '32' WHEN MEM_LANGUAGE=9 THEN '39' END AS [SpokenLang]
      ,'41' AS [SpokenLangSource]
      ,'59' AS [WrittenLang]
      ,'69' AS [WrittenLangSource]
      ,'79' AS [OtherLang]
      ,'89' AS [OtherSource]
---  FROM [KPI_ENGINE].[HDS].[HEDIS_MEMBER_GM]
FROM KPI_ENGINE.dbo.MEMBER M
GO
