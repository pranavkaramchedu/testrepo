SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON




CREATE proc [HDS].[KPI_HEDIS_ART]
AS

BEGIN
DECLARE @INFO VARCHAR(8000)
					,	@TRUE BIT = 1
					,	@FALSE BIT = 0
					,	@DB_ID INT = DB_ID()
					,	@LOGID INT = 0
					,	@PROC VARCHAR(500)=OBJECT_SCHEMA_NAME(@@PROCID)+'.'+OBJECT_NAME(@@PROCID)
					,	@PERF_START DATETIME
					,	@PERF_DURATION INT
					,	@PERF_ROW INT
					,	@RC	INT
					;

		EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, @INFO=@INFO, @LOG2ID=@LOGID OUT;
		DECLARE @PROCFULLNAME VARCHAR(500) = DB_NAME()+'.'+@PROC;
		----EXEC dbo.SP_MI_UTIL_SEND_MAIL @PROCFULLNAME, 'STARTED', 1, @ECHO=0; 
	DECLARE @MSG VARCHAR(1000)
	DECLARE @PERF_START_PROC DATETIME 
	DECLARE @PERF_RPS NUMERIC(10,2) 
	DECLARE @ROWS INT 
	DECLARE @PERF_ROWS INT 
	SET @PERF_ROWS = 0 
	SET @PERF_START = GETDATE() 
	SET @PERF_START_PROC = GETDATE() 

BEGIN TRY


-- Declare Variables
declare @rundate Date=GetDate()
declare @meas_year varchar(4)=Year(Dateadd(month,-2,@rundate))
declare @rootId INT=159

Declare @meas varchar(10)='ART';
Declare @ce_startdt Date;
Declare @ce_startdt1 Date;
Declare @ce_novdt Date;
Declare @ce_enddt Date;
Declare @ce_enddt1 Date;
Declare @runid INT;

Declare @startDate Date;
Declare @enddate date;
Declare @quarter varchar(20);
Declare @measure_id varchar(10);
Declare @target INT;
Declare @domain varchar(100);
Declare @subdomain varchar(100);
Declare @measuretype varchar(100);
Declare @measurename varchar(100);
Declare @reporttype varchar(100);
Declare @reportId INT;



-- Set Measure dates
SET @ce_startdt=concat(@meas_year,'-01-01');
SET @ce_novdt=concat(@meas_year,'-11-30');
SET @ce_enddt=concat(@meas_year,'-12-31');
SET @ce_startdt1=concat(@meas_year-1,'-01-01');
SET @ce_enddt1=concat(@meas_year-1,'-12-31');

Set @reporttype='Physician'
Set @measurename='Disease-Modifying Anti-Rheumatic Drug Therapy for Rheumatoid Arthritis'
Set @startDate=DATEADD(yy, DATEDIFF(yy, 0,Dateadd(month,-2,GETDATE())), 0) 
Set @enddate=eomonth(Dateadd(month,-2,GetDate()))
Set @quarter=Concat(Year(@enddate),' - Q',DATEPART(q, @enddate))
set @target=84
Set @domain='Clinical Quality / Chronic Condition Management'
Set @subdomain='Rheumatoid Arthritis'
Set @measuretype='UHN'
Set @measure_id='16'




-- Eligible Patient List
drop table if exists #art_memlist; 
CREATE TABLE #art_memlist 
(
    EMPI varchar(100)
);
insert into #art_memlist
SELECT DISTINCT 
	en.EMPI 
FROM open_empi_master gm
join Enrollment en on 
	en.EMPI=gm.EMPI_ID and 
	en.ROOT_COMPANIES_ID=gm.ROOT_COMPANIES_ID
WHERE 
	en.ROOT_COMPANIES_ID=@rootId and
	en.EFF_DATE<=@ce_enddt AND 
	TERM_DATE>=@ce_startdt AND 
	YEAR(@ce_enddt)-YEAR(Date_of_Birth) >=18




-- Create Temp Patient Enrollment
drop table if exists #art_tmpsubscriber;
CREATE TABLE #art_tmpsubscriber 
(
    EMPI varchar(100),
    dob Date,
	gender varchar(1),
	payer varchar(50),
	StartDate Date,
	EndDate Date
);
insert into #art_tmpsubscriber
SELECT distinct
	en.EMPI
	,gm.Date_of_Birth
	,gm.Gender
	,en.PAYER_TYPE
	,en.EFF_DATE
	,en.TERM_DATE 
FROM open_empi_master gm
join ENROLLMENT en on 
	en.EMPI=gm.EMPI_ID and 
	en.ROOT_COMPANIES_ID=gm.ROOT_COMPANIES_ID
WHERE 
	en.ROOT_COMPANIES_ID=@rootId and
	en.EFF_DATE<=@ce_enddt AND 
	TERM_DATE>=@ce_startdt AND 
	YEAR(@ce_enddt)-YEAR(Date_of_Birth) >=18
	ORDER BY 
		en.EMPI
		,en.EFF_DATE
		,en.TERM_DATE;



-- Create artdataset
drop table if exists #artdataset;
CREATE TABLE #artdataset (
  EMPI varchar(100) NOT NULL,
  [meas] varchar(20) DEFAULT NULL,
  [payer] varchar(100) DEFAULT NULL,
  gender varchar(45) NOT NULL,
  [age] INT DEFAULT NULL,
  [orec] smallint DEFAULT NULL,
  [lis] smallint DEFAULT '0',
  [rexcl] smallint DEFAULT '0',
  [rexcld] smallint DEFAULT '0',
  [CE] smallint DEFAULT '0',
  [excl] smallint DEFAULT '0',
  [num] smallint DEFAULT '0',
  [Event] smallint DEFAULT '0'
 
) ;
Insert INTO #artdataset(EMPI,meas,payer,age,gender)
Select distinct
	EMPI
	,'ART'
	,payer
	,YEAR(@ce_enddt)-YEAR(dob)
	,Gender
From #art_tmpsubscriber



/*
-- Get Patient COUNT
SELECT @pt_ctr=COUNT(*)  FROM #art_memlist;


-- Loop through enrollment for each patient

While @i<@pt_ctr
	BEGIN
		-- Get Patient to Loop
		SELECT  @patientid=memid FROM #art_memlist ORDER BY memid ASC OFFSET  @i ROWS FETCH NEXT 1 ROWS ONLY ;
		
		-- ReadingGender and age
		SELECT Top 1 @gender=gender,@age=Year(@ce_enddt)-Year(dob) FROM #art_tmpsubscriber WHERE memid=@patientid;

		
		-- Check for dual eligibility by checking if the patient is enrolled in more than one plan at the end of Enrollment
		
		SELECT Top 1 @latest_insenddate=EndDate FROM #art_tmpsubscriber WHERE  memid=@patientid  AND StartDate<=@ce_enddt ORDER BY StartDate DESC,EndDate Desc  ;
		
		-- Check for plan count patient is enrolled in during the end of measurement year
		
		SELECT @plan_ct=COUNT(*) FROM #art_tmpsubscriber WHERE  memid=@patientid AND @latest_insenddate BETWEEN StartDate AND EndDate;
		
		-- If only one plan, then skip the Payer reporting logic
		
		if(@plan_ct=1)
		BEGIN
			
			SELECT @planid=payer FROM #art_tmpsubscriber WHERE  memid=@patientid AND @latest_insenddate BETWEEN StartDate AND EndDate ORDER BY payer;
		
			-- If plan is Dual , Insert MCR & MCD	
				
			If(@planid in('MMP','SN3','MDE'))
			BEGIN
				
				SET @planid='MCR';
				Insert INTO #artdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
				
				
			END
			
			Else if(@planid IN('MD','MLI','MRB'))
			Begin
				SET @planid='MCD';
				Insert INTO #artdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
			END
			
			Else If(@planid IN('SN1','SN2'))
			Begin
				SET @planid='MCR';
				Insert INTO #artdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
			END
			Else if(@planid IN('MEP','MMO','MOS','MPO'))
			BEGIN
				
				Insert INTO #artdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
				
			END
			ELSE
			BEGIN
				if(@planid IN('CEP','HMO','POS','PPO'))
				BEGIN
					Insert INTO #artdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
				END
				ELSE
				BEGIN
					Insert INTO #artdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
				End
			END
		END
				
		-- Compare Plans if Plan count is more than one	
		if(@plan_ct>1)
		BEGIN
			
			-- Read First Plan
			SELECT  @plan1=payer FROM #art_tmpsubscriber where memid=@patientid and @latest_insenddate between StartDate and EndDate order by 1 OFFSET  0 ROWS FETCH NEXT 1 ROWS ONLY ;
					
			-- Read second plan
			SELECT  @plan2=payer FROM #art_tmpsubscriber where memid=@patientid and @latest_insenddate between StartDate and EndDate order by 1 OFFSET  1 ROWS FETCH NEXT 1 ROWS ONLY ;
			
			
			
			If(@plan1 IN('MCR','MP','MC','SN1','SN2','MCS') and @plan2 IN('PPO','POS','HMO','CEP'))
			BEGIN
				if(@plan1 IN('SN1','SN2'))
				BEGIN
					SET @planid='MCR';
					Insert INTO #artdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
				END
				
				if(@plan1 in ('MCR','MP','MC','MCS'))
				Begin
					
					Set @planid=@plan1;
					Insert INTO #artdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
				END

			END
			
			Else If(@plan2 IN('MCR','MP','MC','SN1','SN2','MCS') AND @plan1 IN('PPO','POS','HMO','CEP'))
			BEGIN
			
				IF(@plan2 IN('SN1','SN2'))
				Begin
					Set @planid='MCR';
					Insert INTO #artdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
					
				END
				
				
				IF(@plan2 IN('MCR','MP','MC','MCS'))
				BEGIN
					
					Set @planid=@plan2;
					
					Insert INTO #artdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
				END
			END
			
			ELSE IF(@plan1 IN('PPO','POS','HMO','CEP') AND @plan2 IN('MD','MLI','MRB'))
			BEGIN
			
				Set @planid=@plan1;
				Insert INTO #artdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);	
			END
			
			ELSE IF(@plan2 IN('PPO','POS','HMO','CEP') AND @plan1 IN('MD','MLI','MRB'))
			BEGIN
			
				Set @planid=@plan2;
				Insert INTO #artdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
			END
			
			ELSE IF(@plan1 IN('MDE','SN3','MMP') or @plan2 in('MDE','SN3','MMP'))
			BEGIN
			
				Set @planid='MCR';
				Insert INTO #artdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
				
			END
			
			ELSE
			BEGIN
			
				Set @planid=@plan1;	
				Insert INTO #artdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
				Set @planid=@plan2;
				Insert INTO #artdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);				
				
			END
			
		END
		
		Set @i=@i+1;
		
	END;
-- Create Indices on cdcdataset
CREATE INDEX [idx1] ON #artdataset ([payer]);
CREATE INDEX [patient_id_UNIQUE] ON #artdataset ([patient_id],[payer]);
*/
-- Continuous Enrollment
	
	
DROP TABLE IF EXISTS #art_contenroll;
CREATE table #art_contenroll
(
	EMPI varchar(100)		
);
With coverage_CTE (EMPI,lastcoveragedate,Startdate,Finishdate,nextcoveragedate) as
(
	select distinct
		EMPI
		,isnull(lag(TERM_DATE,1) over(partition by EMPI order by EFF_DATE,TERM_DATE desc),@ce_startdt) as lastcoveragedate
		,Case when EFF_DATE<@ce_startdt then @ce_startdt 
			else EFF_DATE 
		end as Startdate
		,case 
			when TERM_DATE>@ce_enddt then @ce_enddt 
			else TERM_DATE 
		end as Finishdate
		,isnull(lead(EFF_DATE,1) over(partition by EMPI order by EFF_DATE,TERM_DATE),@ce_enddt) as nextcoveragedate  
	from 
	(
		Select distinct
			EMPI
			,EFF_DATE
			,TERM_DATE
		FROM Enrollment 
		where 
			ROOT_COMPANIES_ID=@rootId and 
			EFF_DATE<=@ce_enddt and 
			TERM_DATE>=@ce_startdt 
			--and drug='Y'   
	)t1
)
Insert into #art_contenroll
select 
	EMPI 
from
(
	select 
		*
		,case 
			when rn=1 and startdate>@ce_startdt then 1 
			else 0 
		end as startgap
		,case 
			when datediff(day,newFinishdate,nextcoveragedate)>0 then 1  
			else 0 
		end as gaps
		,datediff(day,Startdate,newfinishdate)+1 as coveragedays
		,case 
			when @ce_enddt between Startdate and newfinishdate then 1 
			else 0 
		end as anchor 
	from
	(
		Select 
			*
			,case 
				when datediff(day,Finishdate,nextcoveragedate) <=1 then isnull(lead(Finishdate,1) over(partition by EMPI order by Startdate,FinishDate),@ce_enddt)
				else finishdate 
			end as newfinishdate
			,ROW_NUMBER() over(partition by EMPI order by Startdate,Finishdate) as rn 
		from coverage_CTE
	)t1           
)t2 
group by 
	EMPI 
having
	(sum(gaps)+sum(startgap)<=1) and
	sum(coveragedays)>=(DATEPART(dy, @ce_enddt)-45) and 
	sum(anchor)>0


update #artdataset set CE=1 from #artdataset ds join #art_contenroll ce on ds.EMPI=ce.EMPI;
	

-- Exclusions
	-- AdvancedIllness

	drop table if exists #art_advillness;
	CREATE table #art_advillness
	(
		EMPI varchar(100),
		servicedate Date,
		CLAIM_ID varchar(100)
	);
	Insert into #art_advillness
	Select
		EMPI
		,DIAG_START_DATE
		,CLAIM_ID
	From advancedillness(@rootId,@ce_startdt1,@ce_enddt)


	
	-- Members with Institutinal SNP

	UPDATE #artdataset SET #artdataset.rexcl=1 FROM #artdataset ds JOIN Enrollment s on ds.EMPI=s.EMPI and s.ROOT_COMPANIES_ID=@rootId WHERE  ds.age>=66 AND ds.payer IN('MCR','MCS','MP','MC','SN2','SN1','SN3','MC','MR') AND s.EFF_DATE<=@ce_enddt AND s.TERM_DATE>=@ce_startdt AND s.Payer_Type='SN2';


	-- LTI Exclusion
	
	drop table if exists #art_LTImembers;
	
	CREATE table #art_LTImembers
	(
		EMPI varchar(100)				
	)
	Insert into #art_LTImembers
	Select
		EMPI
	From LTImembers(@rootId,@ce_startdt,@ce_enddt)

	update #artdataset set rexcl=1 from #artdataset ds join #art_LTImembers re on ds.EMPI=re.EMPI where ds.age>=66 AND ds.payer IN('MCR','MCS','MP','MC','MMP','SN1','SN2','SN3','MC','MR');

	

	-- Hospice Exclusion

	drop table if exists #art_hospicemembers;
	CREATE table #art_hospicemembers
	(
		EMPI varchar(100)		
	);
	Insert into #art_hospicemembers
	Select
		EMPI
	From hospicemembers(@rootId,@ce_startdt,@ce_enddt)

	update #artdataset set rexcl=1 from #artdataset ds join #art_hospicemembers hos on hos.EMPI=ds.EMPI;
			
				
	-- Frailty Members LIST
	drop table if exists #art_frailtymembers;
	
	CREATE table #art_frailtymembers
	(
		EMPI varchar(100)		
	);
	Insert into #art_frailtymembers
	Select
		EMPI
	From Frailty(@rootId,@ce_startdt,@ce_enddt)
		
-- Required Exclusion 1

	-- Inpatient Stay List
	drop table if exists #art_inpatientstaylist;
	CREATE table #art_inpatientstaylist
	(
		EMPI varchar(100),
		FROM_DATE Date,
		CLAIM_ID varchar(100)
	)
	Insert into #art_inpatientstaylist
		Select
		EMPI
		,FROM_DATE
		,CLAIM_ID
	From inpatientstays(@rootId,@ce_startdt1,@ce_enddt)

	
	-- Non acute Inpatient stay list
	
	drop table if exists #art_noncauteinpatientstaylist;
	CREATE table #art_noncauteinpatientstaylist
	(
		EMPI varchar(100),
		FROM_DATE Date,
		CLAIM_ID varchar(100)
	)
	Insert into #art_noncauteinpatientstaylist
	Select 
		EMPI
		,FROM_DATE,
		CLAIM_ID
	From nonacutestays(@rootId,@ce_startdt1,@ce_enddt)

	
	-- Outpatient and other visits
	drop table if exists #art_visitlist;
	CREATE table #art_visitlist
	(
		EMPI varchar(100),
		FROM_DATE Date,
		CLAIM_ID varchar(100)
	)
	Insert into #art_visitlist
	Select distinct
		EMPI
		,FROM_DATE
		,CLAIM_ID
	From
	(

		select 
			p.EMPI
			,FROM_DATE
			,ADM_DATE
			,DIS_DATE
			,p.CLAIM_ID 
		from Procedures p
		Join CLAIMLINE c on
			p.CLAIM_ID=c.CLAIM_ID and
			ISNULL(p.SV_LINE,0)=ISNULL(c.SV_LINE,0) and
			p.PROC_DATA_SRC=c.CL_DATA_SRC and
			p.ROOT_COMPANIES_ID=c.ROOT_COMPANIES_ID and
			p.EMPI=c.EMPI
		where 
			p.ROOT_COMPANIES_ID=@rootId and 
			ISNULL(PROC_STATUS,'EVN')!='INT' and
			ISNULL(c.POS,'0')!='81' and 
			Proc_Code in
			(
				select code_new from HDS.Valueset_to_code where value_set_Name in('Observation','Outpatient','ED','Telephone Visits','Online Assessments')
			)
	
			Union All

			Select
				EMPI
				,FROM_DATE
				,ADM_DATE
				,DIS_DATE
				,CLAIM_ID
			From CLAIMLINE 
			Where
				ROOT_COMPANIES_ID=@rootId and
				ISNULL(POS,'0') not in('81') and 
				REV_CODE in
				(
					select code from HDS.Valueset_to_code where code_system='UBREV' and value_set_Name in('Outpatient','ED')
				)
		)t1

	
	-- Required exclusion table
	drop table if exists #art_reqdexcl1;
	CREATE table #art_reqdexcl1
	(
		EMPI varchar(100)			
	)
	Insert into #art_reqdexcl1
	select distinct 
		t3.EMPI 
	from
	(
		select 
			t2.EMPI 
		from
		(
			select distinct 
				t1.EMPI
				,t1.FROM_DATE 
			from
			(
				select
					EMPI
					,FROM_DATE
					,CLAIM_ID  
				from #art_visitlist 
				
				union all
				
				select 
					na.EMPI
					,na.FROM_DATE
					,na.CLAIM_ID 
				from #art_noncauteinpatientstaylist na
				join #art_inpatientstaylist inp on 
					na.EMPI=inp.EMPI and 
					na.CLAIM_ID=inp.CLAIM_ID
			)t1
			Join #art_advillness a on 
				a.EMPI=t1.EMPI and 
				a.CLAIM_ID=t1.CLAIM_ID
		)t2 
		group by 
			t2.EMPI 
		having 
			count(t2.EMPI)>1
	)t3 
	Join #art_frailtymembers f on
		f.EMPI=t3.EMPI

	update #artdataset set rexcl=1 from #artdataset ds
	join #art_reqdexcl1 re1 on re1.EMPI=ds.EMPI and ds.age Between 66 AND 80;

-- Required Exclusion 2

	-- Acute Inpatient with Advanced Illness
	drop table if exists #art_reqdexcl2;
	CREATE table #art_reqdexcl2
	(
		EMPI varchar(100)				
	)
	insert into #art_reqdexcl2
	select distinct 
		t2.EMPI 
	from
	(
		select 
			t1.EMPI 
		from 
		(
			Select distinct
				EMPI
				,PROC_START_DATE
				,CLAIM_ID
			From GetProcedures(@rootId,@ce_startdt1,@ce_enddt,'Acute Inpatient')
		)t1
		Join #art_advillness a on 
			a.EMPI=t1.EMPI and 
			a.CLAIM_ID=t1.CLAIM_ID
	)t2
	join #art_frailtymembers f on 
		f.EMPI=t2.EMPI

		
	update #artdataset set rexcl=1 from #artdataset ds
	join #art_reqdexcl2 re2 on re2.EMPI=ds.EMPI and ds.age Between 66 AND 80;
	
			
	-- Required exclusion 3
	drop table if exists #art_reqdexcl3;
	CREATE table #art_reqdexcl3
	(
		EMPI varchar(100)			
	)
	insert into #art_reqdexcl3
	select distinct 
		t2.EMPI 
	from
	(
		select 
			t1.EMPI
			,t1.FROM_DATE
			,t1.CLAIM_ID 
		from
		(
			select 
				inp.EMPI
				,inp.FROM_DATE
				,inp.CLAIM_ID 
			from #art_inpatientstaylist inp
			left outer join #art_noncauteinpatientstaylist na on 
				inp.EMPI=na.EMPI and
				inp.CLAIM_ID=na.CLAIM_ID
			where 
				na.EMPI is null
		)t1
		join #art_advillness a on 
			a.EMPI=t1.EMPI and 
			a.CLAIM_ID=t1.CLAIM_ID
	)t2
	join #art_frailtymembers f on
		f.EMPI=t2.EMPI


	update #artdataset set rexcl=1 from #artdataset ds
	join #art_reqdexcl3 re3 on re3.EMPI=ds.EMPI and ds.age Between 66 AND 80;

		
-- RequiredExcl 4
	drop table if exists #art_reqdexcl4;
	CREATE table #art_reqdexcl4
	(
		EMPI varchar(100)				
	)
	insert into #art_reqdexcl4
	select 
		t1.EMPI 
	from
	(
		Select
			EMPI
		From MEDICATION
		where
			ROOT_COMPANIES_ID=@rootId and
			FILL_DATE between @ce_startdt1 and @ce_enddt and
			MEDICATION_CODE in
			(
				select code from HDS.MEDICATION_LIST_TO_CODES where Medication_List_Name='Dementia Medications'
			)
	)t1
	Join #art_frailtymembers f on 
		f.EMPI=t1.EMPI;

	
	
	update #artdataset set rexcl=1 from #artdataset ds
	join #art_reqdexcl4 re4 on re4.EMPI=ds.EMPI and ds.age Between 66 AND 80;
	
	-- reqdexcl 5 fraity with 81 years and above
	update #artdataset set rexcl=1 from #artdataset ds
	join #art_frailtymembers f on f.EMPI=ds.EMPI and ds.age>=81;

	
-- Event LIST
	drop table if exists #art_vstlist;
	CREATE table #art_vstlist
	(
		EMPI varchar(100),
		serviceDate Date,
		CLAIM_ID varchar(100)			
	)
	insert into #art_vstlist
	Select distinct 
		*
	From
	(

		Select
			p.EMPI
			,PROC_START_DATE as ServiceDate
			,p.CLAIM_ID
		FROM Procedures p
		left outer Join CLAIMLINE c on
			p.CLAIM_ID=c.CLAIM_ID and
			ISNULL(p.SV_LINE,0)=ISNULL(c.SV_LINE,0) and
			p.PROC_DATA_SRC=c.CL_DATA_SRC and
			p.ROOT_COMPANIES_ID=c.ROOT_COMPANIES_ID and
			p.EMPI=c.EMPI
		Where
			p.ROOT_COMPANIES_ID=@rootId and
			ISNULL(c.POS,'0')!='81' and
			p.PROC_START_DATE between @ce_startdt and @ce_enddt and
			p.PROC_CODE in
			(
				select code from HDS.valueset_to_code where value_set_name in('Outpatient','Telephone Visits','Online Assessments')
			)

		Union all

		Select
			EMPI
			,FROM_DATE as ServiceDate
			,CLAIM_ID
		From CLAIMLINE
		Where
			ROOT_COMPANIES_ID=@rootId and
			ISNULL(POS,'0')!='81' and
			FROM_DATE between @ce_startdt and @ce_enddt and
			REV_CODE in
			(
				select code from HDS.valueset_to_code where code_system='UBREV' and value_set_name in('Outpatient')
			)

		Union all

		Select
			t1.*
		From
		(
			Select
				EMPI
				,FROM_DATE as servicedate
				,CLAIM_ID
			From inpatientstays(@rootId,@ce_startdt,@ce_enddt)
		)t1
		Join
		(
			Select
				EMPI
				,FROM_DATE
				,CLAIM_ID
			From nonacutestays(@rootId,@ce_startdt,@ce_enddt)
		)t2 on
			t1.EMPI=t2.EMPI and
			t1.CLAIM_ID=t2.CLAIM_ID
	)j1



drop table if exists #art_rheumatoidlist;
CREATE table #art_rheumatoidlist
(
	EMPI varchar(100),
	servicedate Date,
	CLAIM_ID varchar(100)
);
Insert into #art_rheumatoidlist
Select distinct
	EMPI
	,DIAG_START_DATE
	,CLAIM_ID
From GetDiagnosis(@rootId,@ce_startdt,@ce_novdt,'Rheumatoid Arthritis')



drop table if exists #art_eventlist;
CREATE table #art_eventlist
(
	EMPI varchar(100)	
)
insert into #art_eventlist
select 
	EMPI 
from
(
	select distinct 
		r.EMPI
		,r.servicedate 
	from #art_rheumatoidlist r
	join #art_vstlist v on 
		v.EMPI=r.EMPI and 
		v.CLAIM_ID=r.CLAIM_ID

)t1 
group by 
	EMPI 
having 
	count(EMPI)>1
	

update #artdataset set Event=1 from #artdataset ds
	join #art_eventlist e on e.EMPI=ds.EMPI;
	
	
-- Numerator
drop table if exists #art_numlist;
CREATE table #art_numlist
(
	EMPI varchar(100),
	ServiceDate Date,
	Code varchar(20)
);
insert into #art_numlist
Select distinct 
	EMPI 
	,ServiceDate
	,Code
from
(
	Select
		EMPI
		,PROC_START_DATE as ServiceDate
		,PROC_CODE as Code
	From GetProcedures(@rootId,@ce_startdt,@ce_enddt,'DMARD')

	Union all

	select  
		EMPI
		,FILL_DATE as ServiceDate
		,MEDICATION_CODE as Code
	from MEDICATION 
	where 
		ROOT_COMPANIES_ID=@rootId and 
		FILL_DATE between @ce_startdt and @ce_enddt and 
		MEDICATION_CODE in
		(
			select code from HDS.Medication_list_to_codes where Medication_list_name='DMARD Medications'
		)


)t1


update #artdataset set num=1 from #artdataset ds
	join #art_numlist n on n.EMPI=ds.EMPI;


-- Create Numerator Details
Drop table if exists #art_numdetails
Create Table #art_numdetails
(
	EMPI varchar(100)
	,ServiceDate Date
	,Code varchar(20)
	,Rownumber INT
)
Insert into #art_numdetails
Select
	*
From
(
	Select
		*
		,ROW_NUMBER() over(partition by EMPI order by ServiceDate Desc) as rn
	From #art_numlist
)
t1
Where
	rn=1



	
-- Optional Exclusions
	
-- HIV Exclusions

drop table if exists #art_optexcl1;
CREATE table #art_optexcl1
(
	EMPI varchar(100)	
);

Insert into #art_optexcl1
select distinct 
	d.EMPI 
from Diagnosis d
Left outer join CLAIMLINE c on
	d.CLAIM_ID=c.CLAIM_ID and
	d.DIAG_DATA_SRC=c.CL_DATA_SRC and
	d.ROOT_COMPANIES_ID=c.ROOT_COMPANIES_ID
Where
	d.ROOT_COMPANIES_ID=@rootId and
	ISNULL(c.POS,'0')!='81' and
	d.DIAG_START_DATE<=@ce_enddt and
	DIAG_CODE in
	(
		select Code_New from hds.valueset_to_code where value_set_name in('HIV','HIV Type 2')
	)
	
update #artdataset set excl=1 from #artdataset ds
	join #art_optexcl1 oe1 on oe1.EMPI=ds.EMPI;
	


-- Pregnancy

drop table if exists #art_optexcl2;
CREATE table #art_optexcl2
(
	EMPI varchar(100)	
);
Insert into #art_optexcl2
Select distinct
	EMPI
From GetDiagnosis(@rootId,@ce_startdt,@ce_enddt,'Pregnancy')

update #artdataset set excl=1 from #artdataset ds
	join #art_optexcl2 oe2 on oe2.EMPI=ds.EMPI and ds.gender='F';


	-- Insert data in Output Table
	-- Get ReportId from Report_Details

	exec GetReportDetail @rundate=@rundate,@rootId=@rootId,@startDate=@startDate output,@enddate=@enddate output,@quarter=@quarter output,@reportId=@reportId output

	Delete from HDS.HEDIS_MEASURE_OUTPUT where Measure_id='ART' and ROOT_COMPANIES_ID=@rootId and RUN_ID=@reportId

	Insert into HDS.HEDIS_MEASURE_OUTPUT(memid,Meas,payer,CE,Event,Epop,Excl,Num,Rexcl,RExclD,Age,Gender,Measure_ID,Measurement_year,RUN_ID,ROOT_COMPANIES_ID)
	SELECT EMPI,meas,payer,CE,EVENT,CASE WHEN CE=1 AND EVENT=1 AND rexcl=0 and rexcld=0 and payer in('MCR','MC','MCS','MP','MC','MR') THEN 1 ELSE 0 END AS epop,excl,num,rexcl,rexcld,age,gender AS gender,@meas as Measure_ID,@meas_year,@reportId,@rootId FROM #artdataset
	


	-- Insert data into Measure Detailed Line
	Delete from RPT.MEASURE_DETAILED_LINE where Measure_id=@measure_id and ROOT_COMPANIES_ID=@rootId and REPORT_ID=@reportId;
	Insert into RPT.MEASURE_DETAILED_LINE(Provider_Id,PCP_NPI,PCP_NAME,Practice_Name,Specialty,Measure_id,Measure_Name,Payer,PayerId,MEM_FNAME,MEM_MName,MEM_LNAME,MEM_DOB,MEM_GENDER,ENROLLMENT_STATUS,Last_visit_date,Product_Type,Num,Den,Excl,Rexcl,CE,Event,Epop,Report_Id,ReportType,Report_Quarter,Period_Start_date,Period_end_Date,Root_Companies_id,EMPI,MEASURE_TYPE,Code,DateofService)
	Select 
		a.AmbulatoryPCPNPI as Provider_Id 
		,a.AmbulatoryPCPNPI
		,a.AmbulatoryPCPName as PCP_NAME
		,a.AmbulatoryPCPPractice as Practice_Name
		,a.AmbulatoryPCPSpecialty as Specialty
		,@measure_id as Measure_id
		,@measurename as Measure_Name
		,a.DATA_SOURCE as Payer
		,a.PayerId
		,a.MemberFirstName as MEM_FNAME
		,a.MemberMiddleName as MEM_MName
		,a.MemberLastName as MEM_LNAME
		,a.MemberDOB
		,a.MEM_GENDER
		,a.EnrollmentStatus
		,a.AmbulatoryPCPRecentVisit as Last_visit_date
		,d.Payer
		,Num
		,1 as Den
		,Excl
		,Rexcl
		,CE
		,Event
		,CASE 
			WHEN CE=1  AND rexcl=0 and rexcld=0 and Event=1 THEN 1 
			ELSE 0 
		END AS epop
		,@reportId
		,@reporttype
		,@quarter
		,@startDate
		,@enddate
		,@rootId
		,d.EMPI as EMPI
		,@measuretype
		,nd.Code
		,nd.ServiceDate
	From #artdataset d
	join KPI_ENGINE.RPT.PCP_ATTRIBUTION a on d.EMPI=a.EMPI and a.Reportid=@reportId
	Left outer join #art_numdetails nd on d.EMPI=nd.EMPI
	where event=1
--	where a.AmbulatoryPCPSpecialty in('Family Medicine','General Practice','Gynecology','Internal Medicine','Obstetrics & Gynecology')



	-- Insert data into Provider Scorecard
	Delete from RPT.PROVIDER_SCORECARD where Measure_id=@measure_id and ROOT_COMPANIES_ID=@rootId and REPORT_ID=@reportId;
Insert into RPT.PROVIDER_SCORECARD(Provider_Id,PCP_NPI,PCP_NAME,Specialty,Practice_Name,Measure_id,Measure_Name,Measure_Title,MEASURE_SUBTITLE,Measure_Type,NUM_COUNT,DEN_COUNT,Excl_Count,Rexcl_Count,Gaps,Result,Target,To_Target,Report_Id,ReportType,Report_Quarter,Period_Start_Date,Period_End_Date,Root_Companies_Id)
Select 
	Provider_Id
	,PCP_NPI
	,PCP_NAME
	,Specialty
	,Practice_Name
	,Measure_id
	,Measure_Name
	,Measure_Title
	,MEASURE_SUBTITLE
	,Measure_Type
	,SUM(Cast(NUM_COUNT as INT)) as NUM_COUNT
	,SUM(Cast(DEN_COUNT as INT)) as DEN_COUNT
	,SUM(Cast(Excl_Count as INT)) as Excl_Count
	,Sum(Cast(Rexcl_count as INT)) as Rexcl_Count
	,sum(Cast(DEN_Excl as INT)) - SUM(Cast(NUM_COUNT as INT)) as Gaps
	,Case
		when SUM(Cast(DEN_Excl as Float))>0 Then Round((SUM(cast(NUM_COUNT as Float))/ISNULL(NULLIF(SUM(Cast(DEN_Excl as Float)),0),1))*100,2)
		else 0
	end as Result
	,Target as Target
	,Case
		when ((SUM(Cast(DEN_Excl as INT)))*(cast(Target*0.01 as float))) - SUM(Cast(NUM_COUNT as INT))>0 Then CEILING(((SUM(Cast(DEN_Excl as INT)))*(cast(Target*0.01 as float))) - SUM(Cast(NUM_COUNT as INT)))
		Else 0
	end as To_Target
	,Report_Id
	,ReportType
	,Report_Quarter
	,Period_Start_Date
	,Period_End_Date
	,Root_Companies_Id
From
(
	Select distinct
		m.EMPI
		,a.NPI as Provider_id
		,a.NPI as PCP_NPI
		,a.Prov_Name as PCP_Name
		,a.Specialty
		,a.Practice as Practice_Name
		,m.Measure_id
		,m.Measure_Name
		,l.measure_Title
		,l.Measure_SubTitle
		,'Calculated' as Measure_Type
		,Case
			when NUM=1 and excl=0 and rexcl=0 Then 1
			else 0
		end as NUM_COUNT
		,DEN as DEN_COUNT
		,Case
			When DEN=1 and Excl=0 and Rexcl=0 Then 1
			else 0
		End as Den_excl
		,Excl as Excl_Count
		,Rexcl as Rexcl_count
		,Report_Id
		,l.ReportType
		,Report_Quarter
		,Period_Start_Date
		,Period_End_Date
		,m.Root_Companies_Id
		,l.Target
	From RPT.MEASURE_DETAILED_LINE m
	Join RPT.ConsolidatedAttribution_Snapshot a on
		m.EMPI=a.EMPI
	Join RFT.UHN_measuresList l on
		m.Measure_Id=l.measure_id
	Join RFT.UHN_MeasureSpecialtiesMapping s on
		a.Specialty=s.Specialty and
		m.MEASURE_ID=s.Measure_id
	where Enrollment_Status='Active' and
		  a.NPI!='' and
		  m.MEASURE_ID=@measure_id and
		  REPORT_ID=@reportId 
)t1
Group by Provider_Id,PCP_NPI,PCP_NAME,Specialty,Practice_Name,Measure_id,Measure_Name,Measure_Title,MEASURE_SUBTITLE,Report_Id,ReportType,Report_Quarter,Period_Start_Date,Period_End_Date,Root_Companies_Id,Measure_Type,Target





		SET @PERF_ROW = @@ROWCOUNT
		SET @PERF_DURATION = DATEDIFF(MINUTE,@PERF_START,GETDATE());
		EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, 'Enrollment Completed', @PERF_ROW, @DURATION_IN_MIN=@PERF_DURATION, @LOG2ID=@LOGID;
		EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, @LOG2ID=@LOGID, @END_FLAG=1;

END TRY
BEGIN CATCH
		BEGIN
			EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, 'FATAL ERROR', @LOG2ID=@LOGID;
			RAISERROR ('',16,0)
			RETURN 16
        END
	END CATCH
RETURN 0
END

GO
