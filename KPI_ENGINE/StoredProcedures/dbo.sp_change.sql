SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
CREATE procedure [dbo].[sp_change](

@Dimension varchar(max)='_PROV_UDF_05_',    
@Business_Name varchar(max)='Active',
@FormatType varchar(max)='int'
)
as 
begin
declare @File_type varchar(max)
set @File_type=  case 
when @Dimension like '%ENR%UDF%' then 'dbo.staging_enrollment'
when @Dimension like '%Claim%UDF%' then 'DBO.staging_Claimline'
when @Dimension like '%MEM%UDF%' then 'dbo.staging_Member'
when @Dimension like '%PROV%UDF%' then 'dbo.staging_Provider'
end
select @File_type

declare @update_1 varchar(max)
declare @update_2 varchar(max)

set @update_1='update dbo.kpi_user_field set FormatType='''+@FormatType+''',Business_Name='''+@Business_Name+''' where Dimension='''+@Dimension+''' and File_Type='''+@File_Type+''''
set @update_2='alter table dbo.Staging_'+@File_Type+' alter column '+@Dimension+ ' '+@FormatType

print @update_1
print @update_2
--exec (@update_1)
--exec (@update_2)

end
GO
