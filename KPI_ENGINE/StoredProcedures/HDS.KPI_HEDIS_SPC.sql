SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON





CREATE PROCEDURE [HDS].[KPI_HEDIS_SPC] 
AS

BEGIN
DECLARE @INFO VARCHAR(8000)
					,	@TRUE BIT = 1
					,	@FALSE BIT = 0
					,	@DB_ID INT = DB_ID()
					,	@LOGID INT = 0
					,	@PROC VARCHAR(500)=OBJECT_SCHEMA_NAME(@@PROCID)+'.'+OBJECT_NAME(@@PROCID)
					,	@PERF_START DATETIME
					,	@PERF_DURATION INT
					,	@PERF_ROW INT
					,	@RC	INT
					;

		EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, @INFO=@INFO, @LOG2ID=@LOGID OUT;
		DECLARE @PROCFULLNAME VARCHAR(500) = DB_NAME()+'.'+@PROC;
		----EXEC dbo.SP_MI_UTIL_SEND_MAIL @PROCFULLNAME, 'STARTED', 1, @ECHO=0; 
	DECLARE @MSG VARCHAR(1000)
	DECLARE @PERF_START_PROC DATETIME 
	DECLARE @PERF_RPS NUMERIC(10,2) 
	DECLARE @ROWS INT 
	DECLARE @PERF_ROWS INT 
	SET @PERF_ROWS = 0 
	SET @PERF_START = GETDATE() 
	SET @PERF_START_PROC = GETDATE() 

BEGIN TRY


-- Declare Variables
Declare @rundate date=GetDate();
declare @meas_year varchar(4)=Year(Dateadd(month,-2,@rundate))
declare @rootId varchar(10)='159'


DECLARE @ce_startdt Date
DECLARE @ce_enddt Date;
DECLARE @ce_startdt1 Date;
DECLARE @ce_enddt1 Date;
DECLARE @meas VARCHAR(10);

Declare @startDate Date;
Declare @enddate date;
Declare @quarter varchar(20);
Declare @measure_id varchar(10);
Declare @target INT;
Declare @domain varchar(100);
Declare @subdomain varchar(100);
Declare @measuretype varchar(100);
Declare @measurename varchar(100);
Declare @reporttype varchar(100);
Declare @reportId INT;

SET @meas='SPC';
SET @ce_startdt=concat(@meas_year,'-01-01');
SET @ce_enddt=concat(@meas_year,'-12-31');
SET @ce_startdt1=concat(@meas_year-1,'-01-01');
SET @ce_enddt1=concat(@meas_year-1,'-12-31');


Set @reporttype='Physician'
--Set @measurename='Hypertension: BP Control <140/90 (18-85); GAP Open/Closed by last Documented B/P for CY'
--Set @startDate=DATEADD(yy, DATEDIFF(yy, 0,Dateadd(month,-2,GETDATE())), 0) 
--Set @enddate=eomonth(Dateadd(month,-2,GetDate()))
--Set @quarter=Concat(Year(@enddate),' - Q',DATEPART(q, @enddate))
-- set @target=82
Set @domain='Clinical Quality / Chronic Condition Management'
--Set @subdomain='Hypertension'
Set @measuretype='UHN'
--Set @measure_id='12'


-- Get ReportId from Report_Details
	exec GetReportDetail @rundate=@rundate,@rootId=@rootId,@startDate=@startDate output,@enddate=@enddate output,@quarter=@quarter output,@reportId=@reportId output

-- Eligible Patient List

drop table if exists #spc_memlist; 
CREATE TABLE #spc_memlist (
    EMPI varchar(100)
    
)
insert into #spc_memlist
SELECT DISTINCT 
	en.EMPI FROM 
open_empi_master gm
join Enrollment en on 
	en.EMPI=gm.EMPI_ID and 
	en.ROOT_COMPANIES_ID=gm.ROOT_COMPANIES_ID
WHERE 
	en.ROOT_COMPANIES_ID=@rootId and
	en.EFF_DATE<=@enddate AND
	TERM_DATE>=@ce_startdt1 and 
	(
		(
			gm.Gender='F' AND 
			YEAR(@enddate)-YEAR(Date_of_Birth) BETWEEN 40 AND 75
		) 
		or 
		(
			gm.Gender='M' AND 
			YEAR(@enddate)-YEAR(Date_of_Birth) BETWEEN 21 AND 75
		)
	)
ORDER BY 1;



-- Create Temp Patient Enrollment
drop table if exists #spc_tmpsubscriber;
CREATE TABLE #spc_tmpsubscriber (
    EMPI varchar(100),
    dob Date,
	age INT,
	gender varchar(1),
	payer varchar(50),
	StartDate Date,
	EndDate Date
);
insert into #spc_tmpsubscriber
SELECT distinct
	en.EMPI
	,gm.Date_of_Birth
	,YEAR(@enddate)-YEAR(Date_of_Birth) as age
	,gm.Gender
	,en.PAYER_TYPE
	,en.EFF_DATE
	,en.TERM_DATE 
FROM open_empi_master gm
join ENROLLMENT en on 
	en.EMPI=gm.EMPI_ID and 
	en.ROOT_COMPANIES_ID=gm.ROOT_COMPANIES_ID
WHERE 
	en.ROOT_COMPANIES_ID=@rootId and
	en.EFF_DATE<=@enddate AND 
	TERM_DATE>=@ce_startdt1 and 
	(
		(
			gm.Gender='F' AND 
			YEAR(@enddate)-YEAR(Date_of_Birth) BETWEEN 40 AND 75
		) 
		or
		(
			gm.Gender='M' AND 
			YEAR(@enddate)-YEAR(Date_of_Birth) BETWEEN 21 AND 75
		)
	)
ORDER BY 
	en.EMPI
	,en.EFF_DATE
	,en.TERM_DATE;



-- Create temp table to store SPC data
drop table if exists #spcdataset;
CREATE TABLE #spcdataset (
  EMPI varchar(100) NOT NULL,
  [meas] varchar(20) DEFAULT NULL,
  [payer] varchar(100) DEFAULT NULL,
  Gender varchar(45) NOT NULL,
  [age] INT,
  [orec] smallint DEFAULT NULL,
  [lis] smallint DEFAULT '0',
  [rexcl] smallint DEFAULT '0',
  [rexcld] smallint DEFAULT '0',
  [CE] smallint DEFAULT '0',
  [excl] smallint DEFAULT '0',
  [num] smallint DEFAULT '0',
  [Event] smallint DEFAULT '0'
  
  
)
Insert into #spcdataset(EMPI,meas,payer,Gender,age)
Select distinct
	EMPI
	,'SPCA'
	,pm.PayerMapping
	,gender
	,age
From
(
	Select distinct
		EMPI
		,gender
		,age
		,case 
				when (Payer in('HMO','CEP','POS','PPO') and nxtpayer in('MD','MLI','MRB'))  then TRIM(Payer)
				When (Payer in('HMO','CEP','POS','PPO') and prvpayer in('MD','MLI','MRB'))  then TRIM(Payer)
				When (Payer in('MD','MLI','MRB') and nxtpayer in('HMO','CEP','POS','PPO'))  then TRIM(nxtpayer)
				When (Payer in('MD','MLI','MRB') and prvpayer in('HMO','CEP','POS','PPO'))  then TRIM(prvpayer)
				when (Payer in('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP') and nxtpayer in('HMO','CEP','POS','PPO'))  then TRIM(Payer)
				When (Payer in('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP') and prvpayer in('HMO','CEP','POS','PPO'))  then TRIM(Payer)
				When (Payer in('HMO','CEP','POS','PPO') and nxtpayer in('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP'))  then TRIM(nxtpayer)
				When (Payer in('HMO','CEP','POS','PPO') and prvpayer in('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP'))  then TRIM(prvpayer)
				else Payer 
			end as newpayer
	From
	(
		select
			*
			,isnull(lead(Payer) over(partition by EMPI order by Payer),Payer) as nxtpayer
			,isnull(lag(Payer) over(partition by EMPI order by Payer),Payer) as prvpayer
		From 
		(
			select
				EMPI
				,payer
				,gender
				,age
			From
			(
				select 
					EMPI
					,StartDate
					,EndDate
					,TRIM(payer) as payer
					,gender
					,age
					,RANK() over(partition by EMPI order by StartDate desc,EndDate Desc) as rn 
				from #spc_tmpsubscriber 
				where  
					StartDate<=@enddate
				--	and EMPI=95066
			)t1
			where 
				rn=1
		)t2
		
	)t3
)t4
Join HDS.HEDIS_PAYER_MAPPING pm on 
	t4.newpayer=pm.payer and 
	pm.Measure_id='SPC'
Order by 1



Insert into #spcdataset(EMPI,meas,payer,Gender,age)
Select
	EMPI
	,'SPCB'
	,Payer
	,Gender
	,Age
From #spcdataset


-- Continuous Enrollment
		
	
DROP TABLE IF EXISTS #spc_contenroll1;
CREATE table #spc_contenroll1
(
	EMPI varchar(100)		
)
Insert into #spc_contenroll1
Select
	EMPI
From GetContinuousEnrolledEMPIwithPharmacy(@rootId,@ce_startdt,@enddate,45,0)


DROP TABLE IF EXISTS #spc_contenroll2;
CREATE table #spc_contenroll2
(
	EMPI varchar(100)		
)
Insert into #spc_contenroll2
Select
	EMPI
From GetContinuousEnrolledEMPI(@rootId,@ce_startdt1,@ce_enddt1,45,1)

update #spcdataset 
	set CE=1 
from #spcdataset ds 
join #spc_contenroll1 ce1 on 
	ds.EMPI=ce1.EMPI
Join #spc_contenroll2 ce2 on
	ds.EMPI=ce2.EMPI


	
-- Event List
-- MI (MI Value Set) on the discharge claim


DROP TABLE IF EXISTS #spc_MImemlist;
CREATE table #spc_MImemlist
(
	EMPI varchar(100),
	CLAIM_ID varchar(100)
		
);
Insert into #spc_MImemlist
Select distinct	
	EMPI
	,CLAIM_ID
From GetDiagnosis(@rootId,@ce_startdt1,@ce_enddt1,'MI')
Where
	DIAG_DATA_SRC not in(Select DATA_SOURCE from DATA_SOURCE where Supplemental=1)


DROP TABLE IF EXISTS #spc_ipstaylist;
CREATE table #spc_ipstaylist
(
	EMPI varchar(100),
	DIS_DATE Date,
	CLAIM_ID varchar(100),
	CL_DATA_SRC varchar(50)
		
);
Insert into #spc_ipstaylist
Select distinct
	EMPI
	,DIS_DATE
	,CLAIM_ID
	,CL_DATA_SRC
From inpatientstays(@rootId,@ce_startdt1,@enddate)
Where
	CL_DATA_SRC not in(Select data_source from data_source where supplemental=1)


-- Non acute inpatient stay list
DROP TABLE IF EXISTS #spc_nonacuteipstaylist;
CREATE table #spc_nonacuteipstaylist
(
	EMPI varchar(100),
	servicedate Date,
	CLAIM_ID varchar(100),
	CL_DATA_SRC varchar(50)
);
insert into #spc_nonacuteipstaylist
Select distinct
	EMPI
	,DIS_DATE
	,CLAIM_ID
	,CL_DATA_SRC
From nonacutestays(@rootId,@ce_startdt1,@enddate)




-- 	Other revascularization
DROP TABLE IF EXISTS #spc_ORCmemlist;
CREATE table #spc_ORCmemlist
(
	EMPI varchar(100)
			
);
Insert into #spc_ORCmemlist
select distinct
	EMPI
from GetProcedures(@rootId,@ce_startdt1,@ce_enddt1,'Other Revascularization')
Where
	PROC_DATA_SRC not in(Select DATA_SOURCE from DATA_SOURCE where Supplemental=1)




-- CABG Mem LIST
DROP TABLE IF EXISTS #spc_CABGmemlist;
CREATE table #spc_CABGmemlist
(
	EMPI varchar(100)
);
Insert into #spc_CABGmemlist
select distinct 
	EMPI 
from
(
	Select
		EMPI
	From GetProcedures(@rootId,@ce_startdt1,@ce_enddt1,'CABG')
	Where
		PROC_DATA_SRC not in(Select DATA_SOURCE from DATA_SOURCE where Supplemental=1)


	Union all

	Select
		EMPI
	From GetICDPCS(@rootId,@ce_startdt1,@ce_enddt1,'CABG')
	Where
		PROC_DATA_SRC not in(Select DATA_SOURCE from DATA_SOURCE where Supplemental=1)



)t1



-- PCI List

DROP TABLE IF EXISTS #spc_PCImemlist;
CREATE table #spc_PCImemlist
(
	EMPI varchar(100)
			
);
Insert into #spc_PCImemlist
select distinct 
	EMPI 
from
(
	Select
		EMPI
	From GetProcedures(@rootId,@ce_startdt1,@ce_enddt1,'PCI')
	Where
		PROC_DATA_SRC not in(Select DATA_SOURCE from DATA_SOURCE where Supplemental=1)


	Union all

	Select
		EMPI
	From GetICDPCS(@rootId,@ce_startdt1,@ce_enddt1,'PCI')
	Where
		PROC_DATA_SRC not in(Select DATA_SOURCE from DATA_SOURCE where Supplemental=1)


)t1


-- IVD Diagnosis
DROP TABLE IF EXISTS #spc_IVDmemlist;
CREATE table #spc_IVDmemlist
(
	EMPI varchar(100),
	servicedate Date,
	CLAIM_ID varchar(100),
	DATA_SRC varchar(50)
		
);
Insert into #spc_IVDmemlist
Select distinct
	EMPI
	,DIAG_START_DATE
	,CLAIM_ID
	,DIAG_DATA_SRC
From GetDiagnosis(@rootId,@ce_startdt1,@enddate,'IVD')
Where
	DIAG_DATA_SRC not in(Select DATA_SOURCE from DATA_SOURCE where Supplemental=1)



-- Identify Telehealth POS Claims
Drop table if exists #spc_telehealthvisits;
Create Table #spc_telehealthvisits
(
	EMPI varchar(100),
	CLAIM_ID varchar(100),
	CL_DATA_SRC varchar(50)
)
Insert into #spc_telehealthvisits
Select distinct
	EMPI
	,CLAIM_ID
	,CL_DATA_SRC
From CLAIMLINE
Where 
	ROOT_COMPANIES_ID=@rootId and
	FROM_DATE between @ce_startdt1 and @enddate and
	ISNULL(POS,'0') in
	(
		Select code from HDS.ValueSet_TO_CODE where Value_SET_NAMe='Telehealth POS'
	)

	


DROP TABLE IF EXISTS #spc_IVDvstlist;
CREATE table #spc_IVDvstlist
(
	EMPI varchar(100),
	servicedate Date,
	CLAIM_ID varchar(100),
	DATA_SRC varchar(50)
				
);
Insert into #spc_IVDvstlist
Select distinct
	EMPI
	,ServiceDate
	,CLAIM_ID
	,DATA_SRC
From
(
	Select
		EMPI
		,PROC_START_DATE as servicedate
		,CLAIM_ID
		,PROC_DATA_SRC as DATA_SRC
	From GetProcedures(@rootId,@ce_startdt1,@enddate,'Outpatient')
	Where
		PROC_DATA_SRC not in(Select DATA_SOURCE from DATA_SOURCE where Supplemental=1)

	Union all

	Select
		EMPI
		,PROC_START_DATE as servicedate
		,CLAIM_ID
		,PROC_DATA_SRC as DATA_SRC
	From GetProcedures(@rootId,@ce_startdt1,@enddate,'Telephone Visits')
	Where
		PROC_DATA_SRC not in(Select DATA_SOURCE from DATA_SOURCE where Supplemental=1)

	Union all

	Select
		EMPI
		,PROC_START_DATE as servicedate
		,CLAIM_ID
		,PROC_DATA_SRC as DATA_SRC
	From GetProcedures(@rootId,@ce_startdt1,@enddate,'Online Assessments')
	Where
		PROC_DATA_SRC not in(Select DATA_SOURCE from DATA_SOURCE where Supplemental=1)

	Union all

	Select
		ai.EMPI
		,PROC_START_DATE as servicedate
		,ai.CLAIM_ID
		,PROC_DATA_SRC as DATA_SRC
	From GetProceduresWithOutMods(@rootId,@ce_startdt1,@enddate,'Acute Inpatient','Telehealth Modifier') ai
	Left outer join #spc_telehealthvisits tv on
		ai.EMPI=tv.EMPI and
		ai.CLAIM_ID=tv.CLAIM_ID and
		ai.PROC_DATA_SRC=tv.CL_DATA_SRC
	Where
		tv.EMPI is null and
		ai.PROC_DATA_SRC not in(Select DATA_SOURCE from DATA_SOURCE where Supplemental=1)

	Union all

	Select
		ip.*
	From #spc_ipstaylist ip
	Left outer join #spc_nonacuteipstaylist na on
		ip.EMPI=na.EMPI and
		ip.CLAIM_ID=na.CLAIM_ID and
		ip.CL_DATA_SRC=na.CL_DATA_SRC
	Where
		na.EMPI is null
		


	Union all

	Select
		EMPI
		,FROM_DATE as servicedate
		,CLAIM_ID
		,CL_DATA_SRC as DATA_SRC
	From CLAIMLINE
	where
		ROOT_COMPANIES_ID=@rootId and
		CL_DATA_SRC not in(Select DATA_SOURCE from DATA_SOURCE where Supplemental=1) and
		ISNULL(POS,'0')!='81' and
		FROM_DATE between @ce_startdt1 and @enddate and
		REV_CODE in
		(
			Select code from HDS.ValueSet_to_code where code_system='UBREV' and Value_Set_Name='Outpatient'
		)

)t1






DROP TABLE IF EXISTS #spc_IVDEventlist;
CREATE table #spc_IVDEventlist
(
	EMPI varchar(100)	
				
);
Insert into #spc_IVDEventlist
select 
	EMPI 
from
(
	select 
		EMPI
		,Year(servicedate) as vstyear 
	from
	(
		select 
			ivd.EMPI
			,ivd.servicedate 
		from #spc_IVDmemlist ivd 
		join #spc_IVDvstlist aip on 
			ivd.EMPI=aip.EMPI and 
			ivd.CLAIM_ID=aip.CLAIM_ID
		
		
	)t1 
	group by 
		EMPI
		,Year(servicedate) 
)t2 
group by 
	EMPI 
having 
	count(EMPI)>1



--

DROP TABLE IF EXISTS #spc_eventlist;
CREATE table #spc_eventlist
(
	EMPI varchar(100)
			
);
Insert into #spc_eventlist
Select distinct 
	EMPI 
from
(
	select distinct 
		s1.EMPI 
	from #spc_ipstaylist s1
	join #spc_MImemlist s2 on 
		s1.EMPI=s2.EMPI and 
		s1.CLAIM_ID=s2.CLAIM_ID
	
	Union all 
	
	select 
		EMPI 
	from #spc_IVDEventlist
	
	Union all
	
	select distinct
		EMPI 
	from #spc_ORCmemlist
	
	Union all
	
	select distinct
		EMPI 
	from #spc_CABGmemlist
	
	Union all

	select distinct 
		EMPI 
	from #spc_PCImemlist
)t1


update #spcdataset set 
	Event=1 
from #spcdataset ds 
join #spc_eventlist ev on 
	ds.EMPI=ev.EMPI and 
	ds.meas='SPCA';

	


-- Required exclusions 
-- •	Female members with a diagnosis of pregnancy (Pregnancy Value Set) during the measurement year or the year prior to the measurement year.

DROP TABLE IF EXISTS #spc_pregmemlist;
Select
	*
into #spc_pregmemlist
From
(
	Select 
		EMPI,
		DIAG_CODE,
		DIAG_START_DATE,
		BILL_PROV,
		coalesce(NULLIF(concat(Provider_Last_Name,' ',Provider_First_Name),' '),Provider_organization_Name) as BillingProvider,
		Row_number() over(Partition By EMPI Order by DIAG_START_DATE desc,Provider_Last_Name desc,Provider_organization_name desc) as rn
	From GetDiagnosis(@rootId,@ce_startdt1,@ce_enddt1,'Pregnancy')
	Left outer Join RFT_NPI n on BILL_PROV=NPI
	
)t1
Where
	rn=1
	
	

-- •	In vitro fertilization (IVF Value Set) in the measurement year or year prior to the measurement year

DROP TABLE IF EXISTS #spc_IVFmemlist;
Select
	*
into #spc_IVFmemlist
From
(
Select 
	t1.*,
	coalesce(NULLIF(concat(Provider_Last_Name,' ',Provider_First_Name),' '),Provider_organization_Name) as BillingProvider,
	Row_number() over(Partition By EMPI Order by Proc_Start_date desc,Provider_Last_Name desc,Provider_organization_name desc) as rn
From
(

Select 
	EMPI,
	Proc_code,
	Proc_Start_date,
	Bill_Prov
From GetProcedures(@rootId,@ce_startdt1,@ce_enddt,'IVF')

Union all

Select 
	EMPI,
	DIAG_CODE,
	DiAG_START_DATE,
	BILL_PROV
From GetDiagnosis(@rootId,@ce_startdt1,@ce_enddt,'IVF')
)t1
left outer Join RFT_NPI on t1.BILL_PROV=NPI
)t2
where rn=1



-- •	Dispensed at least one prescription for clomiphene (Estrogen Agonists Medications List) during the measurement year or the year prior to the measurement year


DROP TABLE IF EXISTS #spc_estrogenmemlist;
Select
	*
into #spc_estrogenmemlist
From
(
Select 
	m.EMPI,
	MEDICATION_CODE,
	FILL_DATE,
	BILL_PROV,
	coalesce(NULLIF(concat(Provider_Last_Name,' ',Provider_First_Name),' '),Provider_organization_Name) as BillingProvider,
	Row_number() over(Partition By m.EMPI Order by FILL_DATE desc,Provider_Last_Name desc,Provider_organization_name desc) as rn
From MEDICATION m
Left outer Join Claimline c on
	m.CLAIM_ID=c.CLAIM_ID and
	m.EMPI=c.EMPI and
	m.MED_DATA_SRC=c.CL_DATA_SRC and
	m.ROOT_COMPANIES_ID=c.ROOT_COMPANIES_ID
Left outer Join RFT_NPI n on
	c.BILL_PROV=n.NPI
Where
	m.ROOT_COMPANIES_ID=@rootId and
	FILL_DATE between @ce_startdt1 and @ce_enddt and
	MEDICATION_CODE in
	(
		select code from HDS.MEDICATION_LIST_TO_CODES where Medication_List_Name='Estrogen Agonists Medications'
	)
)t1
Where
	rn=1
	

	
-- •	ESRD (ESRD Diagnosis Value Set) or dialysis (Dialysis Procedure Value Set) during the measurement year or the year prior to the measurement year

drop table if exists #spc_esrdmemlist;
Select
	*
into #spc_esrdmemlist
From
(
Select 
	t1.*,
	coalesce(NULLIF(concat(Provider_Last_Name,' ',Provider_First_Name),' '),Provider_organization_Name) as BillingProvider,
	Row_number() over(Partition By EMPI Order by Proc_Start_date desc,Provider_Last_Name desc,Provider_organization_name desc) as rn
From
(
	Select
		EMPI,
		Proc_code,
		Proc_Start_date,
		Bill_Prov
	From GetProcedures(@rootId,@ce_startdt1,@ce_enddt,'Dialysis Procedure')

	Union All

	Select
		EMPI,
		ICDPCS_CODE,
		Proc_Start_date,
		Bill_Prov
	From GetICDPCS(@rootId,@ce_startdt1,@ce_enddt,'Dialysis Procedure')

	Union All

	Select
		EMPI,
		DIAG_CODE,
		diag_Start_date,
		Bill_Prov
	From GetDiagnosis(@rootId,@ce_startdt1,@ce_enddt,'ESRD Diagnosis')
)t1
left outer Join RFT_NPI on t1.BILL_PROV=NPI
)t2
where rn=1




-- •	Cirrhosis (Cirrhosis Value Set) during the measurement year or the year prior to the measurement year

Drop table if exists #spc_Cirrhosismemlist;
Select
	*
into #spc_Cirrhosismemlist
From
(
Select distinct
	EMPI,
	DIAG_CODE,
	diag_Start_date,
	Bill_Prov,
	coalesce(NULLIF(concat(Provider_Last_Name,' ',Provider_First_Name),' '),Provider_organization_Name) as BillingProvider,
	Row_number() over(Partition By EMPI Order by diag_Start_date desc,Provider_Last_Name desc,Provider_organization_name desc) as rn
From GetDiagnosis(@rootId,@ce_startdt1,@ce_enddt,'Cirrhosis')
left outer Join RFT_NPI on BILL_PROV=NPI
)t2
where rn=1

	
	

-- •	Myalgia, myositis, myopathy or rhabdomyolysis (Muscular Pain and Disease Value Set) during the measurement year

drop table if exists #spc_MPDmemlist;
Select
	*
into #spc_MPDmemlist
From
(
Select distinct
	EMPI,
	DIAG_CODE,
	diag_Start_date,
	Bill_Prov,
	coalesce(NULLIF(concat(Provider_Last_Name,' ',Provider_First_Name),' '),Provider_organization_Name) as BillingProvider,
	Row_number() over(Partition By EMPI Order by diag_Start_date desc,Provider_Last_Name desc,Provider_organization_name desc) as rn
From GetDiagnosis(@rootId,@ce_startdt,@ce_enddt,'Muscular Pain and Disease')
left outer Join RFT_NPI on BILL_PROV=NPI
)t2
where rn=1





-- •	Members receiving palliative care (Palliative Care Assessment Value Set; Palliative Care Encounter Value Set; Palliative Care Intervention Value Set) during the measurement year

drop table if exists #spc_palliativememlist;
Select
	EMPI,
	Code,
	ServiceDate,
	BILL_PROV,
	BillingProvider,
	rn
into #spc_palliativememlist
From
(
Select 
	t1.*,
	coalesce(NULLIF(concat(Provider_Last_Name,' ',Provider_First_Name),' '),Provider_organization_Name) as BillingProvider,
	Row_number() over(Partition By EMPI Order by ServiceDate desc,Provider_Last_Name desc,Provider_organization_name desc) as rn
From palliativecare(@rootId,@ce_startdt,@ce_enddt) t1
left outer Join RFT_NPI on BILL_PROV=NPI
)t1
where
	rn=1

update #spcdataset set Rexcld=1 from #spcdataset ds join #spc_palliativememlist re7 on ds.EMPI=re7.EMPI ;
update #spcdataset set Rexcld=1 from #spcdataset ds join #spc_pregmemlist re1 on ds.EMPI=re1.EMPI and ds.Gender='F';
update #spcdataset set Rexcld=1 from #spcdataset ds join #spc_IVFmemlist re2 on ds.EMPI=re2.EMPI;
update #spcdataset set Rexcld=1 from #spcdataset ds join #spc_estrogenmemlist re3 on ds.EMPI=re3.EMPI;
update #spcdataset set Rexcld=1 from #spcdataset ds join #spc_esrdmemlist re4 on ds.EMPI=re4.EMPI ;
update #spcdataset set Rexcld=1 from #spcdataset ds join #spc_Cirrhosismemlist re5 on ds.EMPI=re5.EMPI ;
update #spcdataset set Rexcld=1 from #spcdataset ds join #spc_MPDmemlist re6 on ds.EMPI=re6.EMPI ;





-- Exclusion

	-- AdvancedIllness

	
	drop table if exists #spc_advillness;
	select
		EMPI
		,DIAG_CODE
		,DIAG_START_DATE
		,CLAIM_ID 
		,BILL_PROV
		,coalesce(NULLIF(concat(Provider_Last_Name,' ',Provider_First_Name),' '),Provider_organization_Name) as BillingProvider
		,row_number() over(Partition by EMPI,CLAIM_ID order by DIAG_START_DATE desc,Provider_Last_Name desc,Provider_organization_name desc) as rn
		into #spc_advillness
	from advancedillness(@rootId,@ce_startdt1,@ce_enddt)
	left outer Join RFT_NPI on BILL_PROV=NPI
	Where
		DIAG_DATA_SRC not in(select DATA_SOURCE from DATA_SOURCE where supplemental=1)
	

	
	-- LTI Exclusion
	
	drop table if exists #spc_LTImembers;
	CREATE table #spc_LTImembers
	(
		EMPI varchar(100)				
	);
	Insert into #spc_LTImembers
	Select distinct
		EMPI
	From LTImembers(@rootId,@ce_startdt,@ce_enddt)
	
	

	

	-- Hospice Exclusion
	drop table if exists #spc_hospicemembers;
	Select
		*
	into #spc_hospicemembers
	From
	(
	Select 
		EMPI,
		REV_CODE,
		FROM_DATE,
		BILL_PROV,
		coalesce(NULLIF(concat(Provider_Last_Name,' ',Provider_First_Name),' '),Provider_organization_Name) as BillingProvider,
		row_number() over(Partition by EMPI order by FROM_DATE desc,Provider_Last_Name desc,Provider_organization_name desc) as rn
	From hospicemembers(@rootId,@ce_startdt,@ce_enddt)
	Left outer Join RFT_NPI on BILL_PROV=NPI
	)t1
	Where
		rn=1
			
-- Frailty Members LIST
	drop table if exists #spc_frailtymembers;
	Select
		*
	into #spc_frailtymembers
	From
	(
	Select 
		EMPI,
		Code,
		ServiceDate,
		BILL_PROV,
		coalesce(NULLIF(concat(Provider_Last_Name,' ',Provider_First_Name),' '),Provider_organization_Name) as BillingProvider,
		row_number() over(Partition by EMPI order by ServiceDate desc,Provider_Last_Name desc,Provider_organization_name desc) as rn
	From Frailty(@rootId,@ce_startdt,@ce_enddt)
	Left outer Join RFT_NPI on BILL_PROV=NPI
	Where
		DataSource not in(select DATA_SOURCE from DATA_SOURCE where supplemental=1)
	)t1
	Where
		rn=1
	
		
-- Required Exclusion 1

	-- Inpatient Stay List
	drop table if exists #spc_inpatientstaylist;
	CREATE table #spc_inpatientstaylist
	(
		EMPI varchar(100),
		FROM_DATE Date,
		CLAIM_ID varchar(100)
	);
	Insert into #spc_inpatientstaylist
	select distinct 
		EMPI
		,FROM_DATE
		,CLAIM_ID 
	from Inpatientstays(@rootId,@ce_startdt1,@ce_enddt)
	Where
		CL_DATA_SRC not in(select DATA_SOURCE from DATA_SOURCE where supplemental=1)
	
	-- Non acute Inpatient stay list
	
	drop table if exists #spc_noncauteinpatientstaylist;
	CREATE table #spc_noncauteinpatientstaylist
	(
		EMPI varchar(100),
		FROM_DATE Date,
		CLAIM_ID varchar(100)
	);
	Insert into #spc_noncauteinpatientstaylist
	select distinct 
		EMPI
		,FROM_DATE
		,CLAIM_ID 
	from nonacutestays(@rootId,@ce_startdt1,@ce_enddt)
	Where
		CL_DATA_SRC not in(select DATA_SOURCE from DATA_SOURCE where supplemental=1)


	
	-- Outpatient and other visits
	drop table if exists #spc_visitlist;
	CREATE table #spc_visitlist
	(
		EMPI varchar(100),
		FROM_DATE Date,
		CLAIM_ID varchar(100)
	);
	Insert into #spc_visitlist
	Select distinct
		*
	From
	(
		
		Select
			EMPI
			,PROC_START_DATE
			,CLAIM_ID
		From GetProcedures(@rootId,@ce_startdt1,@ce_enddt,'Outpatient')

		Union all

		Select
			EMPI
			,PROC_START_DATE
			,CLAIM_ID
		From GetProcedures(@rootId,@ce_startdt1,@ce_enddt,'Observation')

		Union all

		Select
			EMPI
			,PROC_START_DATE
			,CLAIM_ID
		From GetProcedures(@rootId,@ce_startdt1,@ce_enddt,'ED')

		Union all

		Select
			EMPI
			,PROC_START_DATE
			,CLAIM_ID
		From GetProcedures(@rootId,@ce_startdt1,@ce_enddt,'Telephone Visits')

		Union all

		Select
			EMPI
			,PROC_START_DATE
			,CLAIM_ID
		From GetProcedures(@rootId,@ce_startdt1,@ce_enddt,'Online Assessments')

		Union all

		Select
			EMPI
			,PROC_START_DATE
			,CLAIM_ID
		From GetProcedures(@rootId,@ce_startdt1,@ce_enddt,'Nonacute Inpatient')

		Union all

		select 
			EMPI
			,FROM_DATE
			,CLAIM_ID 
		from CLAIMLINE 
		where 
			ROOT_COMPANIES_ID=@rootId and 
			ISNULL(POS,'')!='81' and 
			CL_DATA_SRC not in(select DATA_SOURCE from DATA_SOURCE where supplemental=1) and
			FROM_DATE between @ce_startdt1 and @ce_enddt and
			REV_CODE in
			(
				select code from HDS.VALUESET_TO_CODE where code_system='UBREV' and  Value_Set_Name in('Outpatient','ED')
			)
	)t1


	

	-- Required exclusion table
	drop table if exists #spc_reqdexcl1;
	select  
		t3.EMPI ,
		f.Code,
		f.ServiceDate,
		f.BILL_PROV,
		f.BillingProvider
	into #spc_reqdexcl1
	from
	(
		select 
			t2.EMPI 
		from
		(
			select distinct 
				a.EMPI,
				a.DIAG_CODE,
				a.DIAG_START_DATE,
				a.BILL_PROV,
				a.BillingProvider
			from
			(
				select 
					EMPI
					,FROM_DATE
					,CLAIM_ID  
				from #spc_visitlist 
				
				union all
				
				select 
					na.EMPI
					,na.FROM_DATE
					,na.CLAIM_ID 
				from #spc_noncauteinpatientstaylist na
				join #spc_inpatientstaylist inp on 
					na.EMPI=inp.EMPI and 
					na.CLAIM_ID=inp.CLAIM_ID
			)t1
			Join #spc_advillness a on
				a.EMPI=t1.EMPI and 
				a.CLAIM_ID=t1.CLAIM_ID
		)t2 
		group by 
			t2.EMPI 
		having 
			count(t2.EMPI)>1
	)t3 
	Join #spc_frailtymembers f on 
		f.EMPI=t3.EMPI

	
-- Required Exclusion 2


	-- Acute Inpatient with Advanced Illness
	drop table if exists #spc_reqdexcl2;
	select distinct 
		t2.EMPI,
		f.Code,
		f.ServiceDate,
		f.BILL_PROV,
		f.BillingProvider
	into #spc_reqdexcl2
	from
	(
		select 
			t1.EMPI 
		from
		(
			Select distinct
				EMPI
				,PROC_START_DATE
				,CLAIM_ID
			From GetProcedures(@rootId,@ce_startdt1,@ce_enddt,'Acute Inpatient')
			Where
				PROC_DATA_SRC not in(select DATA_SOURCE from DATA_SOURCE where Supplemental=1)
	
		)t1
		Join #spc_advillness a on 
			a.EMPI=t1.EMPI and 
			a.CLAIM_ID=t1.CLAIM_ID
	)t2
	join #spc_frailtymembers f on 
		f.EMPI=t2.EMPI

	
	
			
	-- Required exclusion 3
	drop table if exists #spc_reqdexcl3;
	select distinct 
		t2.EMPI,
		f.Code,
		f.ServiceDate,
		f.BILL_PROV,
		f.BillingProvider
	into #spc_reqdexcl3
	from
	(
		select 
			t1.EMPI
			,t1.FROM_DATE
			,t1.CLAIM_ID 
		from
		(
			select 
				inp.EMPI
				,inp.FROM_DATE
				,inp.CLAIM_ID 
			from #spc_inpatientstaylist inp
			left outer join #spc_noncauteinpatientstaylist na on 
				inp.EMPI=na.EMPI and 
				inp.CLAIM_ID=na.CLAIM_ID
			where 
				na.EMPI is null
		)t1
		join #spc_advillness a on 
			a.EMPI=t1.EMPI and
			a.CLAIM_ID=t1.CLAIM_ID
	)t2
	join #spc_frailtymembers f on
		f.EMPI=t2.EMPI

		
	
		
-- RequiredExcl 4
	drop table if exists #spc_reqdexcl4;
	select distinct
		t1.EMPI,
		f.Code,
		f.ServiceDate,
		f.BILL_PROV,
		f.BillingProvider
	into #spc_reqdexcl4
	from
	(
		select distinct
			EMPI
		from MEDICATION 
		where
			ROOT_COMPANIES_ID=159  and 
			MED_DATA_SRC in(select DATA_SOURCE from DATA_SOURCE where supplemental=0) and 
			FILL_DATE between '2019-01-01' and '2020-12-31' and
			MEDICATION_CODE in
			(
				select code from HDS.MEDICATION_LIST_TO_CODES where Medication_List_Name='Dementia Medications'
			)
	)t1
	Join #spc_frailtymembers f on
		f.EMPI=t1.EMPI;

	
	update #spcdataset set 
		rexcl=1 
	from #spcdataset ds
	join #spc_reqdexcl4 re4 on 
		re4.EMPI=ds.EMPI and 
		ds.age BETWEEN 66 AND 80;

	-- Members with Institutinal SNP
	UPDATE #spcdataset SET 
		#spcdataset.rexcl=1 
	FROM #spcdataset ds 
	JOIN Enrollment s on 
		ds.EMPI=s.EMPI and 
		s.ROOT_COMPANIES_ID=@rootId 
	WHERE  
		ds.age>=66 AND 
		ds.payer IN('MCR','MCS','MP','MC','SN2','SN1','SN3','MC','MR') AND 
		s.EFF_DATE<=@enddate AND 
		s.TERM_DATE>=@ce_startdt AND 
		s.Payer_Type='SN2';

	update #spcdataset set 
		rexcl=1 
	from #spcdataset ds 
	join #spc_LTImembers re on 
		ds.EMPI=re.EMPI 
	where 
		ds.age>=66 AND 
		ds.payer IN('MCR','MCS','MP','MC','MMP','SN1','SN2','SN3','MC','MR');

	update #spcdataset set 
		rexcl=1 
	from #spcdataset ds 
	join #spc_hospicemembers hos on 
		hos.EMPI=ds.EMPI;

	update #spcdataset set 
		rexcl=1 
	from #spcdataset ds
	join #spc_reqdexcl1 re1 on 
		re1.EMPI=ds.EMPI and 
		ds.age BETWEEN 66 AND 80;

	update #spcdataset set 
		rexcl=1 
	from #spcdataset ds
	join #spc_reqdexcl2 re2 on 
		re2.EMPI=ds.EMPI and 
		ds.age BETWEEN 66 AND 80;

	update #spcdataset set 
		rexcl=1 
	from #spcdataset ds
	join #spc_reqdexcl3 re3 on 
		re3.EMPI=ds.EMPI and 
		ds.age BETWEEN 66 AND 80;

	
--Numerator
	-- SPC A
	
	
DROP TABLE IF EXISTS #spc_spcamemlist;
CREATE table #spc_spcamemlist
(
	EMPI varchar(100),
	ServiceDate Date,
	Code varchar(20)			
);
Insert into #spc_spcamemlist
select distinct
	EMPI
	,FILL_DATE
	,MEDICATION_CODE
from MEDICATION 
where 
	ROOT_COMPANIES_ID=@rootId and 
	FILL_DATE between @ce_startdt and @enddate and 
	MEDICATION_CODE in
	(
		select code from HDS.MEDICATION_LIST_TO_CODES where Medication_List_Name in('Atorvastatin High Intensity Medications','Amlodipine Atorvastatin High Intensity Medications','Rosuvastatin High Intensity Medications','Simvastatin High Intensity Medications','Ezetimibe Simvastatin High Intensity Medications','Atorvastatin Moderate Intensity Medications','Amlodipine Atorvastatin Moderate Intensity Medications','Rosuvastatin Moderate Intensity Medications  ','Simvastatin Moderate Intensity Medications','Ezetimibe Simvastatin Moderate Intensity Medications','Pravastatin Moderate Intensity Medications','Lovastatin Moderate Intensity Medications','Fluvastatin Moderate Intensity Medications','Pitavastatin Moderate Intensity Medications')
	)

	
update #spcdataset set num=1 from #spcdataset ds join #spc_spcamemlist n1 on ds.EMPI=n1.EMPI and ds.meas='SPCA'





-- Create Numerator details for SPCA
drop table if exists #spc_numdetail;
Create table #spc_numdetail
(
	EMPI varchar(100),
	ServiceDate Date,
	Code varchar(20),
	Rownumber INT
)
Insert into #spc_numdetail
Select
	*
From
(
	Select 
		*
		,ROW_NUMBER() over(partition by EMPI order by ServiceDate Desc) as rn
	From #spc_spcamemlist
)t1
Where
	rn=1



-- SPC B Event

 update ds set ds.Event=1 from #spcdataset ds join #spcdataset s1 on ds.EMPI=s1.EMPI and ds.meas='SPCB' and s1.num=1 and s1.meas='SPCA';

 


 
 -- SPC B Numerator

-- SP


DROP TABLE IF EXISTS #spc_statinmedmemlist;
CREATE table #spc_statinmedmemlist
(
	EMPI varchar(100),
	pdc INT			
);
Insert into #spc_statinmedmemlist
select 
	EMPI
	,sum(pdc) as pdc 
from
(
	-- adjust DOS for year end after summing
	select
		EMPI
		,Servicedate
		,case 
			when dateadd(day,ISNULL(NULLIF(calcdos,''),1)-1,servicedate) >=@enddate then datediff(day,servicedate,@enddate)+1 
			else calcdos 
		end as pdc 
	from
	(
		select 
			EMPI
			,servicedate
			,sum(calcdaysofsupply) as calcdos 
		from
		(
			-- SUM the DOS supply after adjustment
			select distinct 
				EMPI
				,servicedate
				,calcdaysofsupply 
			from
			(
				-- Adjust DOS to accomodate year end
				select 
					*
					,case 
						when dateadd(day,ISNULL(NULLIF(calcdos,''),1)-1,servicedate) >=@enddate then datediff(day,servicedate,@enddate)+1 
						else calcdos 
					end as calcdaysofsupply 
				from
				(
					select 
						*
						,case 
						-- different Medication on same date ,take one with more DOS
						when servicedate=nextmeddate  and medication!=nextmed and daysofsupply>nextdos then daysofsupply
						when servicedate=nextmeddate  and medication!=nextmed and daysofsupply<nextdos then nextdos
						when servicedate=nextmeddate  and medication=nextmed and MEDICATION_CODE_TYPE!=nextmedtype and daysofsupply<nextdos then nextdos
						when servicedate=nextmeddate  and medication=nextmed and MEDICATION_CODE_TYPE!=nextmedtype and daysofsupply>nextdos then daysofsupply
						when servicedate=prevmeddate   and medication!=prevmed and daysofsupply>prevdos then daysofsupply
						when servicedate=prevmeddate  and medication!=prevmed and daysofsupply<prevdos then prevdos
						when servicedate=prevmeddate  and medication=prevmed and MEDICATION_CODE_TYPE!=prevmedtype and daysofsupply<prevdos then prevdos
						when servicedate=prevmeddate  and medication=prevmed and MEDICATION_CODE_TYPE!=prevmedtype and daysofsupply>prevdos then daysofsupply
						-- Adjust DOS to accomodate overlap with next record
						when servicedate!=nextmeddate and medication!=nextmed and enddate>= ISNULL(nextmeddate,enddate) then daysofsupply-(datediff(day,ISNULL(nextmeddate,enddate),enddate)+1)
						else daysofsupply
					end as calcdos
				from
				(
					-- Get Previous and next record for further processing
					select 
						*
						,convert(varchar,cast(dateadd(day,ISNULL(NULLIF(daysofsupply,''),1)-1,servicedate) as date),112) as enddate
						,isnull(lead(servicedate,1) over(partition by EMPI order by servicedate,daysofsupply),servicedate) as nextmeddate
						,isnull(lead(Medication,1) over(partition by EMPI order by servicedate,daysofsupply),medication) as nextmed
						,isnull(lead(MEDICATION_CODE_TYPE,1) over(partition by EMPI order by servicedate,daysofsupply),MEDICATION_CODE_TYPE) as nextmedtype
						,isnull(lead(daysofsupply,1) over(partition by EMPI order by servicedate,daysofsupply),daysofsupply) as nextdos
						,isnull(lag(servicedate,1) over(partition by EMPI order by servicedate,daysofsupply),servicedate) as prevmeddate
						,isnull(lag(Medication,1) over(partition by EMPI order by servicedate,daysofsupply),medication) as prevmed
						,isnull(lag(daysofsupply,1) over(partition by EMPI order by servicedate,daysofsupply),daysofsupply) as prevdos
						,isnull(lag(MEDICATION_CODE_TYPE,1) over(partition by EMPI order by servicedate,daysofsupply),MEDICATION_CODE_TYPE) as prevmedtype
						
						
					from
					(
						Select
							*
						From
						(
							select 
								t1.EMPI
								,t1.servicedate
								,cast(t1.daysofsupply as INT) as daysofsupply
								,t2.Medication
								,MEDICATION_CODE_TYPE
							from
							(
								-- Read Mediction from Hedis_Pharm
								select
									EMPI
									,FILL_DATE as servicedate
									,SUM(Cast(SUPPLY_DAYS as numeric)) as daysofsupply
									,MEDICATION_CODE as code
									,ISNULL(MEDICATION_CODE_TYPE,'') as MEDICATION_CODE_TYPE
									
								from MEDICATION 
								where 
									ROOT_COMPANIES_ID=@rootId and 
									FILL_DATE between @ce_startdt and @enddate
								Group by
									EMPI,FILL_DATE,MEDICATION_CODE,MEDICATION_CODE_TYPE
									

						   
							)t1
							join
							(
								select 
									code
									,Medication_List_Name as Medication 
								from HDS.MEDICATION_LIST_TO_CODES 
								where 
									Medication_List_Name in
									('Atorvastatin High Intensity Medications','Amlodipine Atorvastatin High Intensity Medications','Rosuvastatin High Intensity Medications','Simvastatin High Intensity Medications','Ezetimibe Simvastatin High Intensity Medications','Atorvastatin Moderate Intensity Medications','Amlodipine Atorvastatin Moderate Intensity Medications','Rosuvastatin Moderate Intensity Medications  ','Simvastatin Moderate Intensity Medications','Ezetimibe Simvastatin Moderate Intensity Medications','Pravastatin Moderate Intensity Medications','Lovastatin Moderate Intensity Medications','Fluvastatin Moderate Intensity Medications','Pitavastatin Moderate Intensity Medications','Ezetimibe Simvastatin Low Intensity Medications','Fluvastatin Low Intensity Medications','Lovastatin Low Intensity Medications','Pravastatin Low Intensity Medications','Simvastatin Low Intensity Medications')
							)t2 on 
								t1.code=t2.code
						)t3
					)t4
				)t5
			)t6
		)t7 
	)t8 
	group by 
		EMPI
		,servicedate
)t8 
)t9
group by 
	EMPI 
order by EMPI


-- IPSD
drop table if exists 	#spc_statinmedipsdlist;
CREATE table #spc_statinmedipsdlist
(
	EMPI varchar(100),
	ipsd Date,
	treatmentperiod INT		
);
Insert into #spc_statinmedipsdlist
select 
	z1.EMPI
	,min(z1.servicedate) as ipsd
	,datediff(day,min(z1.servicedate),@enddate)+1 as treatmentperiod 
from
(
	select 
		EMPI
		,FILL_DATE as servicedate
		,case 
			when convert(varchar,cast(DATEADD(day,SUPPLY_DAYS-1,FILL_DATE) as date),23) >@enddate then @enddate 
			else convert(varchar,cast(DATEADD(day,SUPPLY_DAYS-1,FILL_DATE) as date),23) 
		end as Enddate
		,MEDICATION_CODE as code  
	from MEDICATION 
	where 
		ROOT_COMPANIES_ID=@rootId  and 
		FILL_DATE between @ce_startdt and @enddate and 
		MEDICATION_CODE in
		(
			select code from HDS.MEDICATION_LIST_TO_CODES where Medication_List_Name in('Atorvastatin High Intensity Medications','Amlodipine Atorvastatin High Intensity Medications','Rosuvastatin High Intensity Medications','Simvastatin High Intensity Medications','Ezetimibe Simvastatin High Intensity Medications','Atorvastatin Moderate Intensity Medications','Amlodipine Atorvastatin Moderate Intensity Medications','Rosuvastatin Moderate Intensity Medications  ','Simvastatin Moderate Intensity Medications','Ezetimibe Simvastatin Moderate Intensity Medications','Pravastatin Moderate Intensity Medications','Lovastatin Moderate Intensity Medications','Fluvastatin Moderate Intensity Medications','Pitavastatin Moderate Intensity Medications')
		)
	)z1 
	group by 
		z1.EMPI


Drop Table if exists #spc_rate2numlist;
CREATE table #spc_rate2numlist
(
	EMPI varchar(100)	
);
Insert into #spc_rate2numlist
select distinct 
	f1.EMPI 
from
(
	select 
		s1.EMPI
		,s1.treatmentperiod
		,s2.pdc
		,Round(((cast(pdc as float)/cast(treatmentperiod as float))*100),0) as adherence
		,cast(pdc as float)/cast(treatmentperiod as float)*100 as raw
	from #spc_statinmedipsdlist s1
	join #spc_statinmedmemlist s2 on 
		s1.EMPI=s2.EMPI
 
)f1 where f1.adherence>=80 



update #spcdataset set num=1 from #spcdataset ds join #spc_rate2numlist n1 on ds.EMPI=n1.EMPI and ds.meas='SPCB'


	
---
	

	

	


	Delete from HDS.HEDIS_MEASURE_OUTPUT  where MEASURE_ID=@meas and RUN_ID=@reportId and ROOT_COMPANIES_ID=@rootId
	
	Insert into HDS.HEDIS_MEASURE_OUTPUT(memid,Meas,payer,CE,Event,Epop,Excl,Num,Rexcl,RExclD,Age,Gender,Measure_ID,Measurement_year,RUN_ID,ROOT_COMPANIES_ID)
	SELECT EMPI,meas,payer,CE,EVENT,CASE WHEN CE=1 and Event=1 AND rexcl=0 and rexcld=0 and payer not in('MMO','MOS','MPO','MEP','MC','MR','EPO') THEN 1 ELSE 0 END AS epop,excl,num,rexcl,rexcld,age,gender,@meas,@meas_year,@reportId,@rootId FROM #spcdataset


	

	-- Insert data into Measure Detailed Line
	Delete from RPT.MEASURE_DETAILED_LINE where MEASURE_ID in('3','47') and  REPORT_ID=@reportId and ROOT_COMPANIES_ID=@rootId;
	
	Insert into RPT.MEASURE_DETAILED_LINE(Provider_Id,PCP_NPI,PCP_NAME,Practice_Name,Specialty,Measure_id,Measure_Name,Payer,PayerId,MEM_FNAME,MEM_MName,MEM_LNAME,MEM_DOB,MEM_GENDER,ENROLLMENT_STATUS,Last_visit_date,Product_Type,Num,Den,Excl,Rexcl,CE,Event,Epop,Report_Id,ReportType,Report_Quarter,Period_Start_date,Period_end_Date,Root_Companies_id,EMPI,MEASURE_TYPE,Code,DateofService)
	Select 
		a.AmbulatoryPCPNPI as Provider_Id 
		,a.AmbulatoryPCPNPI
		,a.AmbulatoryPCPName as PCP_NAME
		,a.AmbulatoryPCPPractice as Practice_Name
		,a.AmbulatoryPCPSpecialty as Specialty
		,Case
			When meas='SPCA' Then '3'
			When meas='SPCB' Then '47'
		End	as Measure_id
		,Case
			When meas='SPCA' Then 'Patients that are 21-75 years of age with a dx of Cardiovascular disease  that are currently taking a statin'
			When meas='SPCB' Then 'Medication Adherence for Cholesterol (Statins)'
		End	as Measure_Name
		,a.DATA_SOURCE as Payer
		,a.PayerId
		,a.MemberFirstName as MEM_FNAME
		,a.MemberMiddleName as MEM_MName
		,a.MemberLastName as MEM_LNAME
		,a.MemberDOB
		,a.MEM_GENDER
		,a.EnrollmentStatus
		,a.AmbulatoryPCPRecentVisit as Last_visit_date
		,d.Payer
		,Num
		,1 as Den
		,Excl
		,case
			when Rexcl=1 or rexcld=1 then 1
			else 0
		end as Rexcl
		,CE
		,Event
		,CASE 
			WHEN rexcl=0 and rexcld=0 and Event=1 THEN 1 
			ELSE 0 
		END AS epop
		,@reportId
		,@reporttype
		,@quarter
		,@startDate
		,@enddate
		,@rootId
		,d.EMPI as EMPI
		,@measuretype
		,nd.Code
		,nd.ServiceDate
	From #spcdataset d
	join KPI_ENGINE.RPT.PCP_ATTRIBUTION a on d.EMPI=a.EMPI and a.Reportid=@reportId
	Left outer join #spc_numdetail nd on 
		d.EMPI=nd.EMPI and
		d.meas='SPCA'
	where d.Event=1
--	where a.AmbulatoryPCPSpecialty in('Family Medicine','General Practice','Gynecology','Internal Medicine','Obstetrics & Gynecology')



	-- Insert data into Provider Scorecard
	Delete from RPT.PROVIDER_SCORECARD  where MEASURE_ID in('3','47') and REPORT_ID=@reportId and ROOT_COMPANIES_ID=@rootId;
	
Insert into RPT.PROVIDER_SCORECARD(Provider_Id,PCP_NPI,PCP_NAME,Specialty,Practice_Name,Measure_id,Measure_Name,Measure_Title,MEASURE_SUBTITLE,Measure_Type,NUM_COUNT,DEN_COUNT,Excl_Count,Rexcl_Count,Gaps,Result,Target,To_Target,Report_Id,ReportType,Report_Quarter,Period_Start_Date,Period_End_Date,Root_Companies_Id)
Select 
	Provider_Id
	,PCP_NPI
	,PCP_NAME
	,Specialty
	,Practice_Name
	,Measure_id
	,Measure_Name
	,Measure_Title
	,MEASURE_SUBTITLE
	,Measure_Type
	,SUM(Cast(NUM_COUNT as INT)) as NUM_COUNT
	,SUM(Cast(DEN_COUNT as INT)) as DEN_COUNT
	,SUM(Cast(Excl_Count as INT)) as Excl_Count
	,Sum(Cast(Rexcl_count as INT)) as Rexcl_Count
	,sum(Cast(DEN_Excl as INT)) - SUM(Cast(NUM_COUNT as INT)) as Gaps
	,Case
		when SUM(Cast(DEN_Excl as Float))>0 Then Round((SUM(cast(NUM_COUNT as Float))/ISNULL(NULLIF(SUM(Cast(DEN_Excl as Float)),0),1))*100,2)
		else 0
	end as Result
	,Target
	,Case
		when ((SUM(Cast(DEN_Excl as INT)))*(cast(Target*0.01 as float))) - SUM(Cast(NUM_COUNT as INT))>0 Then CEILING(((SUM(Cast(DEN_Excl as INT)))*(cast(Target*0.01 as float))) - SUM(Cast(NUM_COUNT as INT)))
		Else 0
	end as To_Target
	,Report_Id
	,ReportType
	,Report_Quarter
	,Period_Start_Date
	,Period_End_Date
	,Root_Companies_Id
From
(
	Select distinct
		m.EMPI
		,a.NPI as Provider_id
		,a.NPI as PCP_NPI
		,a.Prov_Name as PCP_Name
		,a.Specialty
		,a.Practice as Practice_Name
		,m.Measure_id
		,m.Measure_Name
		,l.measure_Title
		,l.Measure_SubTitle
		,'Calculated' as Measure_Type
		,Case
			when NUM=1 and excl=0 and rexcl=0 Then 1
			else 0
		end as NUM_COUNT
		,DEN as DEN_COUNT
		,Case
			When DEN=1 and Excl=0 and Rexcl=0 Then 1
			else 0
		End as Den_excl
		,Excl as Excl_Count
		,Rexcl as Rexcl_count
		,Report_Id
		,l.ReportType
		,Report_Quarter
		,Period_Start_Date
		,Period_End_Date
		,m.Root_Companies_Id
		,l.Target
	From RPT.MEASURE_DETAILED_LINE m
	Join RPT.ConsolidatedAttribution_Snapshot a on
		m.EMPI=a.EMPI
	Join RFT.UHN_measuresList l on
		m.Measure_Id=l.measure_id
	Join RFT.UHN_MeasureSpecialtiesMapping s on
		a.Specialty=s.Specialty and
		m.MEASURE_ID=s.Measure_id
	where Enrollment_Status='Active' and
		  a.NPI!='' and
		  m.MEASURE_ID in('3','47') and
		  REPORT_ID=@reportId 
)t1
Group by Provider_Id,PCP_NPI,PCP_NAME,Specialty,Practice_Name,Measure_id,Measure_Name,Measure_Title,MEASURE_SUBTITLE,Report_Id,ReportType,Report_Quarter,Period_Start_Date,Period_End_Date,Root_Companies_Id,Measure_Type,Target


Delete from KPI_Engine_MART.RPT.ExclusionReport where root_companies_id=@rootId and measure_id='3' and ReportId in(select ReportId from RPT.Report_Details where ReportStartDate=@ce_Startdt)
Insert into KPI_Engine_MART.RPT.ExclusionReport(Measure_id,EMPI,ExclusionCode,ExclusionDate,BillingProviderNPI,BillingProvider,ExclusionReason,ROOT_COMPANIES_ID,ReportId)

Select '3',EMPI,DIAG_CODE,DIAG_START_DATE,BILL_PROV,BIllingProvider,ExclusionReason,@rootId,@reportId from(
Select EMPI,DIAG_CODE,DIAG_START_DATE,BILL_PROV,BIllingProvider,rn,'Pregnancy' as ExclusionReason from #spc_pregmemlist

Union All 

Select EMPI,PROC_CODE,PROC_START_DATE,BILL_PROV,BIllingProvider,rn,'IVF' as ExclusionReason from #spc_IVFmemlist

Union all

Select EMPI,Medication_Code,FILL_DATE,BILL_PROV,BIllingProvider,rn,'Estrogen Agonists' as ExclusionReason from #spc_estrogenmemlist

Union all

Select EMPI,PROC_CODE,PROC_START_DATE,BILL_PROV,BIllingProvider,rn,'ESRD or Dialysis' as ExclusionReason from #spc_esrdmemlist

Union all

Select EMPI,DIAG_CODE,DIAG_START_DATE,BILL_PROV,BIllingProvider,rn,'Cirrhosis' as ExclusionReason from #spc_Cirrhosismemlist

Union all

Select EMPI,DIAG_CODE,DIAG_START_DATE,BILL_PROV,BIllingProvider,rn,'Muscular Pain and Disease' as ExclusionReason from #spc_MPDmemlist

Union all

Select EMPI,CODE,ServiceDate,BILL_PROV,BIllingProvider,rn,'Palliative Care' as ExclusionReason from #spc_palliativememlist

Union all

select EMPI,null,null,null,null,null,'Long Term Institution' as ExclusionReason from #spc_LTImembers

Union all

Select EMPI,case when REV_CODE is null Then 'Hospice Qualifier' else REV_CODE end as REV_CODE,FROM_DATE,BILL_PROV,BillingProvider,rn,'Hospice' as ExclusionReason from #spc_hospicemembers

Union all

Select EMPI,CODE,ServiceDate,BILL_PROV,BIllingProvider,null,'Advanced Illness and Frailty' as ExclusionReason from #spc_reqdexcl1

Union all

Select EMPI,CODE,ServiceDate,BILL_PROV,BIllingProvider,null,'Advanced Illness and Frailty' from #spc_reqdexcl2

Union all

Select EMPI,CODE,ServiceDate,BILL_PROV,BIllingProvider,null,'Advanced Illness and Frailty' from #spc_reqdexcl3

Union all

Select EMPI,CODE,ServiceDate,BILL_PROV,BIllingProvider,null,'Advanced Illness and Frailty' from #spc_reqdexcl4
)t1



/*Shanawaz -09-27-2021 Adding SPC logic to identify members with open gaps who has low statin or no statin*/

Drop table if exists #spc_lowdose;
Select
	EMPI,
	FILL_DATE,
	MEDICATION_CODE,
	PresribingProvider
	into #spc_lowdose
From
(
	select 
		EMPI
		,FILL_DATE
		,MEDICATION_CODE
		,Presc_NPI
		,coalesce(NULLIF(concat(Provider_Last_Name,' ',Provider_First_Name),' '),PRESC_NAME) as PresribingProvider
		,row_number() over(Partition by EMPI order by FILL_DATE desc) as rn
	from MEDICATION
	Left outer join RFT_NPI n on
		Presc_NPI=NPI
	where 
		ROOT_COMPANIES_ID=@rootId and 
		FILL_DATE between @ce_startdt and @ce_enddt and 
		MEDICATION_CODE in
		(
			select code from HDS.MEDICATION_LIST_TO_CODES where Medication_List_Name in('Ezetimibe Simvastatin Low Intensity Medications','Fluvastatin Low Intensity Medications','Lovastatin Low Intensity Medications','Pravastatin Low Intensity Medications','Simvastatin Low Intensity Medications')
		)
)t1
Where
	rn=1



	Delete from KPI_ENGINE_MART.RPT.Quality_Gap_Details where ROOT_COMPANIES_ID=@rootId and Measureid='3'
	Insert into KPI_ENGINE_MART.RPT.QUALITY_GAP_DETAILS(EMPI,ROOT_COMPANIES_ID,MeasureId,Notagap,ImpactDate,Gap_Reason,AddDetails1,AddValue1,AddDetails2,AddValue2,AddDetails3,AddValue3,ReportId)
	Select distinct
		 d.EMPI,
		 @rootId,
		 '3',
		 0,
		 @ce_enddt,
		 Case 
			when ld.EMPI is not null Then 'Low Dose Statin'
			else 'No Statin'
		end as GapReason,
		Case when FILL_DATE is not null Then 'Fill Date' end as AddDetails1,
		FILL_DATE as AddValue1,
		Case when NULLIF(PresribingProvider,'') is not null then 'Prescribing Provider' end as Adddetails2,
		PresribingProvider as addvalue2,
		Case when MEDICATION_CODE is not null then 'Medication Code' end as Adddetails3,
		MEDICATION_CODE as addvalue3,
		@reportId
	from #spcdataset d
	Left outer Join #spc_lowdose ld on d.EMPI=ld.EMPI
	where 
		meas='SPCA' 
		and event=1
		and Num=0
		and Rexcl=0 and Excl=0 and Rexcld=0


		SET @PERF_ROW = @@ROWCOUNT
		SET @PERF_DURATION = DATEDIFF(MINUTE,@PERF_START,GETDATE());
		EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, 'Enrollment Completed', @PERF_ROW, @DURATION_IN_MIN=@PERF_DURATION, @LOG2ID=@LOGID;
		EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, @LOG2ID=@LOGID, @END_FLAG=1;

END TRY
BEGIN CATCH
		BEGIN
			EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, 'FATAL ERROR', @LOG2ID=@LOGID;
			RAISERROR ('',16,0)
			RETURN 16
        END
	END CATCH
RETURN 0
END

GO
