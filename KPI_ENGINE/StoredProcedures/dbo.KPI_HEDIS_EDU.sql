SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON








CREATE PROCEDURE [dbo].[KPI_HEDIS_EDU] 
AS


BEGIN
DECLARE @INFO VARCHAR(8000)
					,	@TRUE BIT = 1
					,	@FALSE BIT = 0
					,	@DB_ID INT = DB_ID()
					,	@LOGID INT = 0
					,	@PROC VARCHAR(500)=OBJECT_SCHEMA_NAME(@@PROCID)+'.'+OBJECT_NAME(@@PROCID)
					,	@PERF_START DATETIME
					,	@PERF_DURATION INT
					,	@PERF_ROW INT
					,	@RC	INT
					;

		EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, @INFO=@INFO, @LOG2ID=@LOGID OUT;
		DECLARE @PROCFULLNAME VARCHAR(500) = DB_NAME()+'.'+@PROC;
		----EXEC dbo.SP_MI_UTIL_SEND_MAIL @PROCFULLNAME, 'STARTED', 1, @ECHO=0; 
	DECLARE @MSG VARCHAR(1000)
	DECLARE @PERF_START_PROC DATETIME 
	DECLARE @PERF_RPS NUMERIC(10,2) 
	DECLARE @ROWS INT 
	DECLARE @PERF_ROWS INT 
	SET @PERF_ROWS = 0 
	SET @PERF_START = GETDATE() 
	SET @PERF_START_PROC = GETDATE() 

BEGIN TRY



-- Declare Variables


declare @rundate Date=GetDate();
declare @meas_year varchar(4)=Year(Dateadd(month,-2,@rundate))
declare @rootId varchar(10)='159'



Declare @popcount INT=0;
DECLARE @ce_startdt Date
DECLARE @ce_startdt1 Date
DECLARE @ce_enddt Date
DECLARE @ce_enddt1 Date
DECLARE @meas VARCHAR(10);

Declare @startDate Date;
Declare @enddate date;
Declare @quarter varchar(20);
Declare @measure_id varchar(10);
Declare @target INT;
Declare @domain varchar(100);
Declare @subdomain varchar(100);
Declare @measuretype varchar(100);
Declare @measurename varchar(100);
Declare @reporttype varchar(100);
Declare @reportId INT;
 

SET @meas='EDU';
SET @ce_startdt=concat(@meas_year,'-01-01');
SET @ce_enddt=concat(@meas_year,'-12-31');
SET @ce_startdt1=concat(@meas_year-1,'-01-01');
SET @ce_enddt1=concat(@meas_year-1,'-12-31');


Set @reporttype='Network'
Set @measurename='ED Visits/1,000'
--Set @startDate=DATEADD(yy, DATEDIFF(yy, 0,Dateadd(month,-2,GETDATE())), 0) 
--Set @enddate=eomonth(Dateadd(month,-2,GetDate()))
--Set @quarter=Concat(Year(@enddate),' - Q',DATEPART(q, @enddate))
set @target=310
Set @domain='Utilization Management'
Set @subdomain='ED Utilization'
Set @measuretype='Calculated'
Set @measure_id='36'





-- Continuous enrollment
	
-- Continuous enrollment
	
DROP TABLE IF EXISTS #edu_contenroll1;
CREATE table #edu_contenroll1
(
	EMPI varchar(100)
		
)
Insert into #edu_contenroll1
Select 
	EMPI
From GetContinuousEnrolledEMPI(@rootId,@ce_startdt,@ce_enddt,45,0)


DROP TABLE IF EXISTS #edu_contenroll2;
CREATE table #edu_contenroll2
(
	EMPI varchar(100)
		
)
Insert into #edu_contenroll2
Select 
	EMPI
From GetContinuousEnrolledEMPI(@rootId,@ce_startdt1,@ce_enddt1,45,1)



-- hospice Exclusion

drop table if exists #edu_hospicemembers;
CREATE table #edu_hospicemembers
(
	EMPI varchar(100)
		
);
Insert into #edu_hospicemembers
Select
	EMPI
From hospicemembers(@rootId,@ce_startdt,@ce_enddt)
	


-- Creating Denominator Set

drop table if exists #edu_memlist; 
CREATE TABLE #edu_memlist (
    EMPI varchar(100)
    
);
Insert into #edu_memlist
select distinct 
	en.EMPI 
from Enrollment en
join open_empi_master gm on 
	en.EMPI=gm.EMPI_ID and 
	en.ROOT_COMPANIES_ID=gm.ROOT_COMPANIES_ID
join #edu_contenroll1 ce1 on 
	en.EMPI=ce1.EMPI
join #edu_contenroll2 ce2 on 
	en.EMPI=ce2.EMPI
left outer join #edu_hospicemembers hsx on 
	en.EMPI=hsx.EMPI
where 
	en.ROOT_COMPANIES_ID=@rootId and 
	Year(@ce_enddt) - Year(gm.Date_of_Birth)>=18 and 
	hsx.EMPI is null and 
	PAYER_TYPE in('MCR','MCS','MP','CEP','HMO','PPO','POS','SN1','SN2','SN3','MMP','MR','EPO','MC') and
	@ce_enddt between EFF_DATE and TERM_DATE



	
-- Create Temp Patient Enrollment
drop table if exists #edu_tmpsubscriber;
CREATE TABLE #edu_tmpsubscriber (
    EMPI varchar(100),
    dob Date,
	age INT,
	gender varchar(1),
	payer varchar(50),
	StartDate Date,
	EndDate Date
);
insert into #edu_tmpsubscriber
select 
	en.EMPI
	,gm.Date_of_Birth
	,Year(@ce_enddt) - Year(gm.Date_of_Birth) as age
	,gm.Gender
	,en.PAYER_TYPE
	,en.EFF_DATE
	,en.TERM_DATE 
from Enrollment en
join open_empi_master gm on 
	en.EMPI=gm.EMPI_ID and 
	en.ROOT_COMPANIES_ID=gm.ROOT_COMPANIES_ID
join #edu_contenroll1 ce1 on 
	en.EMPI=ce1.EMPI
join #edu_contenroll2 ce2 on 
	en.EMPI=ce2.EMPI
left outer join #edu_hospicemembers hsx on 
	en.EMPI=hsx.EMPI
where 
	en.ROOT_COMPANIES_ID=@rootId and 
	Year(@ce_enddt) - Year(gm.Date_of_Birth)>=18 and 
	hsx.EMPI is null and 
	PAYER_TYPE in('MCR','MCS','MP','CEP','HMO','PPO','POS','SN1','SN2','SN3','MMP','MR','EPO','MC') and
	@ce_enddt between EFF_DATE and TERM_DATE
ORDER BY 
	en.EMPI
	,en.EFF_DATE
	,en.TERM_DATE;




-- Create temp table to store EDU data
drop table if exists #edudataset;
CREATE TABLE #edudataset (
  [EMPI] varchar(100) NOT NULL,
  [meas] varchar(20) DEFAULT NULL,
  [payer] varchar(100) DEFAULT NULL,
  [gender] varchar(45) NOT NULL,
  [age] INT,
  [encounters] smallint DEFAULT '0',
  [Epop] smallint DEFAULT '1',
  [Outlier] smallint DEFAULT '0',
  [PPVComorbidWt] float DEFAULT '0',
  [PPVAgeGenWt] float DEFAULT '0',
  [PUCVComorbidWt] float DEFAULT '1',
  [PUCVAgeGenWt] float DEFAULT '0',
    
) ;
Insert INTO #edudataset(EMPI,meas,payer,age,gender)
Select distinct
	EMPI
	,'EDU'
	,pm.PayerMapping
	,age
	,gender
From
(
	Select distinct
		EMPI
		,gender
		,age
		,case 
				when (Payer in('HMO','CEP','POS','PPO') and nxtpayer in('MD','MLI','MRB'))  then TRIM(Payer)
				When (Payer in('HMO','CEP','POS','PPO') and prvpayer in('MD','MLI','MRB'))  then TRIM(Payer)
				When (Payer in('MD','MLI','MRB') and nxtpayer in('HMO','CEP','POS','PPO'))  then TRIM(nxtpayer)
				When (Payer in('MD','MLI','MRB') and prvpayer in('HMO','CEP','POS','PPO'))  then TRIM(prvpayer)
				when (Payer in('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP') and nxtpayer in('HMO','CEP','POS','PPO'))  then TRIM(Payer)
				When (Payer in('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP') and prvpayer in('HMO','CEP','POS','PPO'))  then TRIM(Payer)
				When (Payer in('HMO','CEP','POS','PPO') and nxtpayer in('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP'))  then TRIM(nxtpayer)
				When (Payer in('HMO','CEP','POS','PPO') and prvpayer in('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP'))  then TRIM(prvpayer)
				else Payer 
			end as newpayer
	From
	(
		select
			*
			,isnull(lead(Payer) over(partition by EMPI order by Payer),Payer) as nxtpayer
			,isnull(lag(Payer) over(partition by EMPI order by Payer),Payer) as prvpayer
		From 
		(
			select
				EMPI
				,payer
				,gender
				,age
			From
			(
				select 
					EMPI
					,StartDate
					,EndDate
					,TRIM(payer) as payer
					,gender
					,age
					,RANK() over(partition by EMPI order by StartDate desc,EndDate Desc) as rn 
				from #edu_tmpsubscriber 
				where  
					StartDate<=@ce_enddt
				--	and EMPI=95066
			)t1
			where 
				rn=1
		)t2
		
	)t3
)t4
Join HDS.HEDIS_PAYER_MAPPING pm on 
	t4.newpayer=pm.payer and 
	pm.Measure_id='EDU'
Order by 1




-- Get ED Visits list
Drop table if exists #edu_edvisits;
Create table #edu_edvisits
(
	EMPI varchar(100),
	FROM_DATE Date,
	Code varchar(20),
	Claim_id varchar(100)
)
Insert into #edu_edvisits
select distinct 
	EMPI
	,FROM_DATE
	,Code
	,CLAIM_ID 
from
(
	
	Select
		c.EMPI
		,p.PROC_START_DATE as FROM_DATE
		,p.PROC_CODE as Code
		,c.CLAIM_ID
	From PROCEDURES p
	Join CLAIMLINE c on 
		p.CLAIM_ID=c.CLAIM_ID and
		ISNULL(p.SV_LINE,0)=ISNULL(c.SV_LINE,0) and
		p.PROC_DATA_SRC=c.CL_DATA_SRC and
		p.ROOT_COMPANIES_ID=c.ROOT_COMPANIES_ID and
		p.EMPI=c.EMPI
	Where
		c.ROOT_COMPANIES_ID=@rootId and
		p.PROC_DATA_SRC not in(Select DATA_SOURCE from DATA_SOURCE where Supplemental=1) and
		ISNULL(c.POS,0)!='81' and
		ISNULL(PROC_STATUS,'EVN')!='INT' and
		c.FROM_DATE between @ce_startdt and @ce_enddt and
		p.PROC_CODE in
		(
			select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('ED')
		)
		and
		ISNULL(c.SV_STAT,'P') in('P','A','O','')
	--	and p.EMPi=95013

	Union all
	

	Select
		c.EMPI
		,p.PROC_START_DATE as FROM_DATE
		,c.POS as Code
		,c.CLAIM_ID
	From PROCEDURES p
	Join CLAIMLINE c on 
		p.CLAIM_ID=c.CLAIM_ID and
		p.PROC_DATA_SRC not in(Select DATA_SOURCE from DATA_SOURCE where Supplemental=1) and
		ISNULL(p.SV_LINE,0)=ISNULL(c.SV_LINE,0) and
		p.PROC_DATA_SRC=c.CL_DATA_SRC and
		p.ROOT_COMPANIES_ID=c.ROOT_COMPANIES_ID and
		p.EMPI=C.EMPI
	Where
		c.ROOT_COMPANIES_ID=@rootId and
		ISNULL(PROC_STATUS,'EVN')!='INT' and
		ISNULL(c.POS,'0') in
		(
			select code from HDS.VALUESET_TO_CODE where value_set_name='ED POS'
		)
		and
		c.FROM_DATE between @ce_startdt and @ce_enddt and
		p.PROC_CODE in
		(
			select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('ED Procedure Code')
		)
		and
		ISNULL(c.SV_STAT,'P') in('P','A','O','')

	Union all

	Select
		c.EMPI
		,c.FROM_DATE
		,REV_CODE as Code
		,c.CLAIM_ID
	From CLAIMLINE c	
	Where
		c.ROOT_COMPANIES_ID=@rootId and
		c.CL_DATA_SRC not in(Select DATA_SOURCE from DATA_SOURCE where Supplemental=1) and
		ISNULL(c.POS,'0')!='81' and
		c.FROM_DATE between @ce_startdt and @ce_enddt and
		c.REV_CODE in
		(
			select code from HDS.VALUESET_TO_CODE where code_system='UBREV' and Value_Set_Name='ED'
		)
		and
		ISNULL(c.SV_STAT,'P') in('P','A','O','')
		--and EMPI=95014
	
)t1





-- Observation Stay and Inpatient Stay
Drop table if exists #edu_stayexclusionlist;
Create table #edu_stayexclusionlist
(
	EMPI varchar(100),
	FROM_DATE Date,
	ADM_DATE Date,
	DIS_DATE Date,
	CLAIM_ID varchar(100)
)
insert into #edu_stayexclusionlist
Select distinct
	EMPI
	,FROM_DATE
	,ADM_DATE
	,DIS_DATE
	,CLAIM_ID
From
(
	Select distinct
		EMPI
		,FROM_DATE
		,ADM_DATE
		,DIS_DATE
		,CLAIM_ID
	From inpatientstays(@rootId,'1900-01-01',@ce_enddt)
	Where
		CL_DATA_SRC not in(Select DATA_SOURCE from DATA_SOURCE where Supplemental=1) 
/* commenting out this logic to only include inpatient stays
	Union All

	Select distinct
		EMPI
		,FROM_DATE
		,ADM_DATE
		,DIS_DATE
		,CLAIM_ID
	From observationstays(@rootId,@ce_startdt,@ce_enddt)
	Where
		CL_DATA_SRC not in(Select DATA_SOURCE from DATA_SOURCE where Supplemental=1) 
*/
)t1



--Exclusion Set - Mental and Behavioral Disorders,	Psychiatry ,	Electroconvulsive therapy
Drop table if exists #edu_encounterexclusionlist;
Create table #edu_encounterexclusionlist
(
	EMPI varchar(100),
	FROM_DATE Date,
	ADM_DATE Date,
	DIS_DATE Date,
	CLAIM_ID varchar(100)
)
Insert into #edu_encounterexclusionlist
Select distinct
	EMPI
	,FROM_DATE
	,ADM_DATE
	,DIS_DATE
	,CLAIM_ID
From
(

	Select
		d.EMPI
		,d.DIAG_START_DATE as FROM_DATE
		,Coalesce(c.ADM_DATE,c.FROM_DATE) as ADM_DATE
		,Coalesce(c.DIS_DATE,c.TO_DATE) as DIS_DATE
		,d.CLAIM_ID
	From diagnosis d
	Join CLAIMLINE c on
		d.CLAIM_ID=c.CLAIM_ID and
		d.DIAG_DATA_SRC=c.CL_DATA_SRC and
		d.ROOT_COMPANIES_ID=c.ROOT_COMPANIES_ID and
		d.EMPI=c.EMPI
	Where
		d.ROOT_COMPANIES_ID=@rootId and
		d.DIAG_DATA_SRC not in(Select DATA_SOURCE from DATA_SOURCE where Supplemental=1) and
		ISNULL(c.POS,'0')!='81' and
		d.DIAG_START_DATE between @ce_startdt and @ce_enddt and
		DIAG_SEQ_NO=1 and
		DIAG_CODE in
		(
			select code_new from HDS.VALUESET_TO_CODE where Value_Set_Name='Mental and Behavioral Disorders'
		)


	Union all

	Select
		p.EMPI
		,p.PROC_START_DATE as FROM_DATE
		,Coalesce(c.ADM_DATE,c.FROM_DATE) as ADM_DATE
		,Coalesce(c.DIS_DATE,c.TO_DATE) as DIS_DATE
		,p.CLAIM_ID
	From PROCEDURES p
	Join CLAIMLINE c on
		p.CLAIM_ID=c.CLAIM_ID and
		ISNULL(p.SV_LINE,0)=ISNULL(c.SV_LINE,0) and
		p.PROC_DATA_SRC=c.CL_DATA_SRC and
		p.ROOT_COMPANIES_ID=c.ROOT_COMPANIES_ID and
		p.EMPI=c.EMPI
	Where
		p.ROOT_COMPANIES_ID=@rootId and
		p.PROC_DATA_SRC not in(Select DATA_SOURCE from DATA_SOURCE where Supplemental=1) and
		ISNULL(c.POS,'0')!='81' and
		p.PROC_START_DATE between @ce_startdt and @ce_enddt and
		(
			PROC_CODE in
			(
				select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Psychiatry','Electroconvulsive Therapy')
			)
			or
			ICDPCS_CODE in
			(
				select code_new from HDS.VALUESET_TO_CODE where Value_Set_Name in('Electroconvulsive Therapy')
			)
		)
)t1




-- Ed Visit Count by member
drop table if exists #edu_EDvstcount;
Create table #edu_EDvstcount
(
	EMPI varchar(100),
	EDcount INT
)
Insert into #edu_EDvstcount
select 
	EMPI
	,count(*) as EdCount 
from
(
	select distinct 
		EMPI
		,FROM_DATE 
	from
	(
		select 
			ed.*
		from #edu_edvisits ed -- where memid=96248
		left outer join #edu_encounterexclusionlist enx on 
			ed.EMPI=enx.EMPI and 
			ed.CLAIM_ID=enx.CLAIM_ID
		left outer join #edu_stayexclusionlist stx on 
			ed.EMPI=stx.EMPI and 
			(
				ed.CLAIM_ID=stx.CLAIM_ID 
				or 
				ed.FROM_DATE between dateadd(day,-1,stx.ADM_DATE) and stx.DIS_DATE
			)

		where 
			enx.EMPI is null and 
			stx.EMPI is null 
			--and ed.EMPI=96465
	)t1 
)t2 
group by 
	EMPI 


	

		-- Create numerator details
		Drop table if exists #edu_numdetails
		Create Table #edu_numdetails
		(
			EMPI varchar(100),
			FROM_DATE Date,
			Code varchar(20)
		)
		Insert into #edu_numdetails
		Select
			EMPI
			,FROM_DATE
			,Code
		From
		(
			select 
				ed.* 
				,ROW_NUMBER() over(partition by ed.EMPI order by ed.FROM_DATE desc) as rn
			from #edu_edvisits ed -- where memid=96248
			left outer join #edu_encounterexclusionlist enx on 
				ed.EMPI=enx.EMPI and 
				ed.CLAIM_ID=enx.CLAIM_ID
			left outer join #edu_stayexclusionlist stx on 
				ed.EMPI=stx.EMPI and 
				(
					ed.CLAIM_ID=stx.CLAIM_ID 
					or 
					ed.FROM_DATE between dateadd(day,-1,stx.ADM_DATE) and stx.DIS_DATE
				)

			where 
				enx.EMPI is null and 
				stx.EMPI is null --and ed.memid=125240
		)t2
		Where
			rn=1




-- Calculate encounter and outlier fields for output
drop table if exists #edu_encounterlist;
Create table #edu_encounterlist
(
	EMPI varchar(100),
	payer varchar(50),
	encounters INT,
	outliers INT
)

Insert into #edu_encounterlist
select 
	ds.EMPI
	,ds.payer
	,case 
		when 
			ds.payer in('MCR','MP','MCS','MC','MR') and 
			ds.age between 18 and 64 and 
			ISNULL(ed.EDcount,0)>5 
		then 
			0
		when 
			ds.payer in('MCR','MP','MCS','MC','MR') and 
			ds.age>=65 and 
			ISNULL(ed.EDcount,0)>3 
		then 
			0
		when 
			ds.payer in('CEP','HMO','POS','PPO','EPO') and 
			ds.age>=18 and 
			ISNULL(ed.EDcount,0)>3 
		then 
			0
		else 
			ISNULL(ed.edcount,0)
	end as encounters
	,case 
		when 
			ds.payer in('MCR','MP','MCS','MC','MR') and
			ds.age between 18 and 64 and 
			ISNULL(ed.EDcount,0)>5 
		then 
			1
		when 
			ds.payer in('MCR','MP','MCS','MC','MR') and 
			ds.age>=65 and
			ISNULL(ed.EDcount,0)>3 
		then 
			1
		when 
			ds.payer in('CEP','HMO','POS','PPO','EPO') and
			ds.age>=18 and
			ISNULL(ed.EDcount,0)>3
		then
			1
		else
			0
	end as outliers
from #edudataset ds
left outer join #edu_EDvstcount ed on 
	ds.EMPI=ed.EMPI
--where ds.memid=100428


update ds set ds.encounters=t1.encounters,ds.outlier=t1.outliers from #edudataset ds join #edu_encounterlist t1 on t1.EMPI=ds.EMPI and t1.payer=ds.payer;





-- Identifying all diagnosis


--Identify visits for risk scoring
Drop table if exists #edu_diagvisits;
Create Table #edu_diagvisits
(
	EMPI varchar(100),
	ServiceDate Date,
	CLAIM_ID varchar(100),
	DATA_SRC varchar(50)
)
Insert into #edu_diagvisits
Select distinct
	EMPI
	,ServiceDate
	,CLAIM_ID
	,DATA_SRC
From
(
	Select
		EMPI
		,PROC_START_DATE as ServiceDate
		,CLAIM_ID
		,PROC_DATA_SRC as DATA_SRC
	From GetProcedures(@rootId,@ce_startdt1,@ce_enddt1,'Outpatient')

	Union All 

	Select
		EMPI
		,PROC_START_DATE as ServiceDate
		,CLAIM_ID
		,PROC_DATA_SRC as DATA_SRC
	From GetProcedures(@rootId,@ce_startdt1,@ce_enddt1,'Telephone Visits')

	Union all 

	Select
		EMPI
		,PROC_START_DATE as ServiceDate
		,CLAIM_ID
		,PROC_DATA_SRC as DATA_SRC
	From GetProcedures(@rootId,@ce_startdt1,@ce_enddt1,'Observation')

	Union all

	Select
		EMPI
		,PROC_START_DATE as ServiceDate
		,CLAIM_ID
		,PROC_DATA_SRC as DATA_SRC
	From GetProcedures(@rootId,@ce_startdt1,@ce_enddt1,'ED')

	Union all

	Select
		EMPI
		,PROC_START_DATE as ServiceDate
		,CLAIM_ID
		,PROC_DATA_SRC as DATA_SRC
	From GetProcedures(@rootId,@ce_startdt1,@ce_enddt1,'Nonacute Inpatient')

	Union all

	Select
		EMPI
		,PROC_START_DATE as ServiceDate
		,CLAIM_ID
		,PROC_DATA_SRC as DATA_SRC
	From GetProcedures(@rootId,@ce_startdt1,@ce_enddt1,'Acute Inpatient')

	Union All

	Select
		EMPI
		,FROM_DATE as ServiceDate
		,CLAIM_ID
		,CL_DATA_SRC as DATA_SRC
	From CLAIMLINE
	Where 
		ROOT_COMPANIES_ID=@rootId and
		ISNULL(POS,'0')!='81' and
		FROM_DATE between @ce_startdt1 and @ce_enddt1 and
		REV_CODE in
		(
			select code from HDS.VALUESET_TO_CODE where code_system='UBREV' and Value_Set_Name in('Outpatient','ED')
		)

	Union All

	Select
		p.EMPI
		,coalesce(DIS_DATE,TO_DATE) as ServiceDate
		,p.CLAIM_ID
		,PROC_DATA_SRC as DATA_SRC
	From Procedures p
	Join Claimline c on
		p.EMPI=c.EMPI and
		p.CLAIM_ID=c.CLAIM_ID and
		ISNULL(p.SV_LINE,0)=ISNULL(C.SV_LINE,0) and
		p.ROOT_COMPANIES_ID=c.ROOT_COMPANIES_ID and
		c.CL_DATA_SRC=p.PROC_DATA_SRC
	Where
		p.ROOT_COMPANIES_ID=@rootId and
		ISNULL(p.PROC_STATUS,'EVN')!='INT' and
		ISNULL(POS,'0')!='81' and
		coalesce(c.DIS_DATE,TO_DATE) between @ce_startdt1 and @ce_enddt1 and
		PROC_CODE in
		(
			select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Nonacute Inpatient','Acute Inpatient')
		)

	Union all


	Select
		EMPI
		,coalesce(DIS_DATE,TO_DATE) as ServiceDate
		,CLAIM_ID
		,CL_DATA_SRC as DATA_SRC
	From CLAIMLINE
	Where 
		ROOT_COMPANIES_ID=@rootId and
		ISNULL(POS,'0')!='81' and
		coalesce(DIS_DATE,TO_DATE) between @ce_startdt1 and @ce_enddt1 and
		REV_CODE in
		(
			select code from HDS.VALUESET_TO_CODE where code_system='UBREV' and Value_Set_Name in('Inpatient Stay')
		)



)t1



drop table if exists #edu_diagnosislist;
Create table #edu_diagnosislist
(
	EMPI varchar(100),
	diagnosis varchar(50),
	DiagnosisDate Date,
	CLAIM_ID varchar(100),
	DIAG_SEQ_NO INT,
	DIAG_DATA_SRC varchar(50)

)
Insert into #edu_diagnosislist
Select distinct
	EMPI
	,DIAG_CODE
	,DIAG_START_DATE
	,CLAIM_ID
	,DIAG_SEQ_NO
	,DIAG_DATA_SRC
From Diagnosis
Where
	ROOT_COMPANIES_ID=@rootId and
	DIAG_DATA_SRC not in(Select Data_Source from Data_Source where supplemental=1) and
	DIAG_START_DATE between @ce_startdt1 and @ce_enddt1


drop table if exists #edu_diagnosis;
Create table #edu_diagnosis
(
	EMPI varchar(100),
	diagnosis varchar(50),
	DiagnosisDate Date,
	CLAIM_ID varchar(100),
	DIAG_SEQ_NO INT

)
Insert into #edu_diagnosis
Select
	d.EMPI
	,d.diagnosis
	,v.Servicedate
	,d.CLAIM_ID
	,d.DIAG_SEQ_NO
From #edu_diagnosislist d
Join #edu_diagvisits v on
	d.EMPI=v.EMPI and
	d.CLAIM_ID=v.CLAIM_ID and
	d.DIAG_DATA_SRC=v.DATA_SRC



-- HCC RANK
drop table if exists #edu_hccrank;
Create table #edu_hccrank
(
	EMPI varchar(100),
	payer varchar(10),
	genagegroup varchar(20),
	reportingindicator varchar(20),
	productline  varchar(20),
	hcc  varchar(10)
	
)
Insert into #edu_hccrank
select 
	t2.EMPI
	,t2.payer
	,case 
		when age between 18 and 44 and gender='F' then 'F_18-44'
		when age between 45 and 54 and gender='F' then 'F_45-54'
		when age between 55 and 64 and gender='F' then 'F_55-64'
		when age between 65 and 74 and gender='F' then 'F_65-74'
		when age between 75 and 84 and gender='F' then 'F_75-84'
		when age >=85 and gender='F' then 'F_85'
		when age between 18 and 44 and gender='M' then 'M_18-44'
		when age between 45 and 54 and gender='M' then 'M_45-54'
		when age between 55 and 64 and gender='M' then 'M_55-64'
		when age between 65 and 74 and gender='M' then 'M_65-74'
		when age between 75 and 84 and gender='M' then 'M_75-84'
		when age >=85 and gender='M' then 'M_85'
	end as genagegroup
	,case 
		when payer in('MCR','MP','MCS','MC','MR') and age between 18 and 64 then 'Standard - 18-64'
		when payer in('MCR','MP','MCS','MC','MR') and age>=65 then 'Standard - 65+'
		else 'Standard - 18+'
	end as reportingindicator
	,case
		When payer in('CEP','HMO','POS','PPO','EPO') then 'Commercial'
		else 'Medicare'
	end as productline
	,hcc
from
(
	select distinct 
		EMPI
		,HCC
		,cast(age as INT) as age
		,gender
		,payer 
	from 
	(
		select 
			d.EMPI
			,cc.Comorbid_CC
			,hcc.RankingGroup
			,isnull(hcc.Rank,1) as Rank
			,ISNULL(hcc.HCC,concat('H',cc.Comorbid_CC)) as HCC
			,Rank() over(partition by d.EMPI,RankingGroup order by Rank) as calcrank
			,ds.age
			,ds.gender
			,ds.payer 
		from #edu_diagnosis d
		left outer join HDS.HEDIS_TABLE_CC cc on 
			d.diagnosis=cc.diagnosiscode and 
			cc.cc_type='Shared'
		left outer join HDS.HEDIS_HCC_RANK hcc on 
			cc.Comorbid_CC=hcc.CC and 
			hcc.hcc_type='Shared'
		join #edudataset ds on 
			ds.EMPI=d.EMPI and 
			ds.outlier=0
		where 
			Comorbid_CC is not null
	)t1 
	where 
		calcrank=1
)t2


-- HCC RANK COMB
drop table if exists #edu_hccrankcmb;
Create table #edu_hccrankcmb
(
	EMPI varchar(100),
	payer varchar(10),
	genagegroup varchar(20),
	reportingindicator varchar(20),
	productline  varchar(20),
	hcc  varchar(10),
	hcccmb  varchar(10)
	
)
Insert into #edu_hccrankcmb
select 
	h.*
	,cmb.hcccomb 
from #edu_hccrank h 
left outer join HDS.HEDIS_HCC_COMB cmb on 
	cmb.ComorbidHCC1 in
	(
		select 
			HCC 
		from #edu_hccrank t1 
		where 
			t1.EMPI=h.EMPI
	) 
	and 
	cmb.ComorbidHCC2 in
	(
		select 
			HCC 
		from #edu_hccrank t1 
		where 
			t1.EMPI=h.EMPI
	)
	and 
	cmb.hcc_type='Shared'


-- PVC Model
drop table if exists #edu_ppvagegenwt;
Create table #edu_ppvagegenwt
(
	EMPI varchar(100),
	payer varchar(10),
	PPVAgeGenWt float
	
)
Insert into #edu_ppvagegenwt
select distinct 
	t1.EMPI
	,t1.payer
	,r1.Weight as PPVAgeGenWt 
from
(
	select 
		EMPI
		,payer
		,case 
			when age between 18 and 44 and gender='F' then 'F_18-44'
			when age between 45 and 54 and gender='F' then 'F_45-54'
			when age between 55 and 64 and gender='F' then 'F_55-64'
			when age between 65 and 74 and gender='F' then 'F_65-74'
			when age between 75 and 84 and gender='F' then 'F_75-84'
			when age >=85 and gender='F' then 'F_85'
			when age between 18 and 44 and gender='M' then 'M_18-44'
			when age between 45 and 54 and gender='M' then 'M_45-54'
			when age between 55 and 64 and gender='M' then 'M_55-64'
			when age between 65 and 74 and gender='M' then 'M_65-74'
			when age between 75 and 84 and gender='M' then 'M_75-84'
			when age >=85 and gender='M' then 'M_85'
		end as genagegroup
		,case 
			when payer in('MCR','MP','MCS','MR','MC') and age between 18 and 64 then 'Standard - 18-64'
			when payer in('MCR','MP','MCS','MR','MC') and age>=65 then 'Standard - 65+'
			else 'Standard - 18+'
		end as reportingindicator
		,case
			When payer in('CEP','HMO','POS','PPO','EPO') then 'Commercial'
			else 'Medicare'
		end as productline
	from #edudataset 
	where 
		outlier=0
 )t1
 join HDS.HEDIS_RISK_ADJUSTMENT r1 on 
	r1.VariableName=t1.genagegroup and 
	t1.productline=r1.ProductLine and 
	r1.variabletype='DEMO' and 
	r1.Model='PPV' and
	r1.Measure_id='EDU'




drop table if exists #edu_ppvcomorbidwt;
Create table #edu_ppvcomorbidwt
(
	EMPI varchar(100),
	payer varchar(10),
	PPVComorbidWt float
	
)
Insert into #edu_ppvcomorbidwt
select
	EMPI
	,payer
	,sum(weight) as PPVComorbidWt 
from
(
	select distinct 
		h.EMPI
		,h.payer
		,h.genagegroup
		,h.reportingindicator
		,h.hcc as hcc
		,r1.Weight 
	from #edu_hccrankcmb h
	join HDS.HEDIS_RISK_ADJUSTMENT r1 on 
		r1.VariableName=h.hcc and 
		h.productline=r1.ProductLine and 
		r1.variabletype='HCC' and 
		r1.Model='PPV' and
		r1.ReportingIndicator=h.reportingindicator and 
		r1.Measure_ID='EDU'

	Union all

	select distinct 
		h.EMPI
		,h.payer
		,h.genagegroup
		,h.reportingindicator
		,h.hcccmb as hcc
		,r1.Weight 
	from #edu_hccrankcmb h
	join HDS.HEDIS_RISK_ADJUSTMENT r1 on 
		r1.VariableName=h.hcccmb and 
		h.productline=r1.ProductLine and 
		r1.variabletype='HCC' and 
		r1.Model='PPV' and 
		r1.ReportingIndicator=h.reportingindicator and 
		r1.Measure_ID='EDU'

)t1 
group by 
	EMPI
	,payer


update #edudataset set PPVAgeGenWt=p.PPVAgeGenWt from #edudataset ds join #edu_ppvagegenwt p on p.EMPI=ds.EMPI and p.payer=ds.payer;
update #edudataset set PPVComorbidWt=p.PPVComorbidWt from #edudataset ds join #edu_ppvcomorbidwt p on p.EMPI=ds.EMPI and p.payer=ds.payer;


-- PUCV Model

drop table if exists #edu_pucvagegenwt;
Create table #edu_pucvagegenwt
(
	EMPI varchar(50),
	payer varchar(10),
	PUCVAgeGenWt float
	
)
Insert into #edu_pucvagegenwt
select distinct 
	t1.EMPI
	,t1.payer
	,r1.Weight as PUCVAgeGenWt 
from
(
	select 
		EMPI
		,payer
		,case 
			when age between 18 and 44 and gender='F' then 'F_18-44'
			when age between 45 and 54 and gender='F' then 'F_45-54'
			when age between 55 and 64 and gender='F' then 'F_55-64'
			when age between 65 and 74 and gender='F' then 'F_65-74'
			when age between 75 and 84 and gender='F' then 'F_75-84'
			when age >=85 and gender='F' then 'F_85'
			when age between 18 and 44 and gender='M' then 'M_18-44'
			when age between 45 and 54 and gender='M' then 'M_45-54'
			when age between 55 and 64 and gender='M' then 'M_55-64'
			when age between 65 and 74 and gender='M' then 'M_65-74'
			when age between 75 and 84 and gender='M' then 'M_75-84'
			when age >=85 and gender='M' then 'M_85'
		end as genagegroup
		,case 
			when payer in('MCR','MP','MCS','MC','MR') and age between 18 and 64 then 'Standard - 18-64'
			when payer in('MCR','MP','MCS','MC','MR') and age>=65 then 'Standard - 65+'
			else 'Standard - 18+'
		end as reportingindicator
		,case
			When payer in('CEP','HMO','POS','PPO','EPO') then 'Commercial'
			else 'Medicare'
		end as productline
	from #edudataset 
	where 
		outlier=0
 )t1
 join HDS.HEDIS_RISK_ADJUSTMENT r1 on 
	r1.VariableName=t1.genagegroup and 
	t1.productline=r1.ProductLine and 
	r1.variabletype='DEMO' and
	r1.Model='PUCV'  and 
	r1.Measure_ID='EDU'



drop table if exists #edu_pucvcomorbidwt;
Create table #edu_pucvcomorbidwt
(
	EMPI varchar(100),
	payer varchar(10),
	PUCVComorbidWt float
	
)
Insert into #edu_pucvcomorbidwt
select 
	EMPI
	,payer
	,case 
		when decimalct>10 then round(PUCVomorbidWt,10,1) 
		else PUCVomorbidWt 
	end as PUCVomorbidWt 
from
(
	select 
		EMPI
		,payer
		,PUCVomorbidWt
		,CASE Charindex('.',PUCVomorbidWt) 
			WHEN 0 THEN 0  
			ELSE Len (Cast(Cast(Reverse(CONVERT(VARCHAR(50),PUCVomorbidWt, 128)) AS FLOAT) AS BIGINT)) 
		END as decimalct 
	from
	(
		select 
			EMPI
			,payer
			,EXP(SUM(LOG(weight))) as PUCVomorbidWt 
		from
		(
			select distinct 
				h.EMPI
				,h.payer
				,h.genagegroup
				,h.reportingindicator
				,h.hcc as hcc
				,r1.Weight as weight 
			from #edu_hccrankcmb h
			join HDS.HEDIS_RISK_ADJUSTMENT r1 on 
				r1.VariableName=h.hcc and 
				h.productline=r1.ProductLine and 
				r1.variabletype='HCC' and 
				r1.Model='PUCV' and 
				r1.ReportingIndicator=h.reportingindicator  and 
				r1.Measure_ID='EDU'
--and memid=97968
			Union all

			select distinct 
				h.EMPI
				,h.payer
				,h.genagegroup
				,h.reportingindicator
				,h.hcccmb as hcc
				,r1.Weight 
			from #edu_hccrankcmb h
			join HDS.HEDIS_RISK_ADJUSTMENT r1 on 
				r1.VariableName=h.hcccmb and
				h.productline=r1.ProductLine and 
				r1.variabletype='HCC' and
				r1.Model='PUCV' and 
				r1.ReportingIndicator=h.reportingindicator  and 
				r1.Measure_ID='EDU'
		)t1 
		group by 
			EMPI
			,payer
	)t2
)t3



update #edudataset set PUCVAgeGenWt=p.PUCVAgeGenWt from #edudataset ds join #edu_pucvagegenwt p on p.EMPI=ds.EMPI and p.payer=ds.payer;
update #edudataset set PUCVComorbidWt=p.PUCVComorbidWt from #edudataset ds join #edu_pucvcomorbidwt p on p.EMPI=ds.EMPI and p.payer=ds.payer;
update #edudataset set PUCVComorbidWt=0 where Outlier=1;



-- Generate Output
-- Get ReportId from Report_Details

	exec GetReportDetail @rundate=@rundate,@rootId=@rootId,@startDate=@startDate output,@enddate=@enddate output,@quarter=@quarter output,@reportId=@reportId output



Delete from HDS.HEDIS_MEASURE_OUTPUT_RISK_UTL  where MEASURE_ID=@meas and RUN_ID=@reportId and ROOT_COMPANIES_ID=@rootId

Insert into HDS.HEDIS_MEASURE_OUTPUT_RISK_UTL(Memid,Meas,payer,Epop,Encounters,Outlier,PPVComorbidWt,PPVAgeGenWt,PUCVComorbidWt,PUCVAgeGenWt,Age,Gender,Measure_ID,Measurement_year,RUN_ID,ROOT_COMPANIES_ID)
	SELECT EMPI,'EDU' as meas,payer,Epop,Encounters,Outlier,PPVComorbidWt,PPVAgeGenWt,PUCVComorbidWt,PUCVAgeGenWt,cast(age as Int) AS age,gender,@meas,@meas_year,@reportId,@rootId FROM #edudataset

	



	Delete from RPT.MEASURE_DETAILED_LINE where MEASURE_ID=@measure_id and REPORT_ID=@reportId and ROOT_COMPANIES_ID=@rootId;

	

Insert into RPT.MEASURE_DETAILED_LINE(Provider_Id,PCP_NPI,PCP_NAME,Practice_Name,Specialty,Measure_id,Measure_Name,Payer,PayerId,MEM_FNAME,MEM_MName,MEM_LNAME,MEM_DOB,MEM_GENDER,ENROLLMENT_STATUS,Last_visit_date,Product_Type,Encounters,outlier,Epop,Report_Id,ReportType,Report_Quarter,Period_Start_date,Period_end_Date,Root_Companies_id,EMPI,Measure_Type,DateofService,Code)
Select 
	
	a.AmbulatoryPCPNPI as Provider_Id 
	,a.AmbulatoryPCPNPI as PCP_NPI
	,a.AmbulatoryPCPName as PCP_NAME
	,a.AmbulatoryPCPPractice as Practice_Name
	,a.AmbulatoryPCPSpecialty as Specialty
	,@measure_id as Measure_id
	,@measurename as Measure_Name
	,a.DATA_SOURCE as Payer
	,a.PayerId
	,a.MemberFirstName as MEM_FNAME
	,a.MemberMiddleName as MEM_MName
	,a.MemberLastName as MEM_LNAME
	,a.MemberDOB
	,a.MEM_GENDER
	,a.EnrollmentStatus
	,a.AmbulatoryPCPRecentVisit as Last_visit_date
	,'' as ProductType
	,d.EDcount
	,0 as Outlier
	,1 as Epop
	,@reportId
	,@reporttype
	,@quarter
	,@startDate
	,@enddate
	,@rootId
	,d.EMPI
	,@measuretype
	,nd.FROM_DATE
	,nd.Code
From #edu_EDvstcount d
join RPT.PCP_ATTRIBUTION a on 
	d.EMPI=a.EMPI and a.ReportId=@reportId
Left outer join #edu_numdetails nd on
	d.EMPI=nd.EMPI

	
	

-- Calculate total population count in a period

/*
select @popcount=count(*) from(
select EMPI from MEMBER_MONTH where MEMBER_MONTH_START_DATE between @startDate and @enddate group by  EMPI
)t1
*/


select @popcount=(select
                    sum(MM_UNITS)
                  from
                    MEMBER_MONTH mm
                    join RPT.ConsolidatedAttribution_Snapshot on RPT.ConsolidatedAttribution_Snapshot.EMPI = convert(varchar(100), mm.EMPI)
                    and Attribution_Type = 'Ambulatory_PCP'
                  Where
                    MEMBER_MONTH_START_DATE between DATEADD(yy, DATEDIFF(yy, 0, GETDATE()), 0)
                    and EOMONTH(Dateadd(mm, -1, Getdate()))
                    and 1 = 1
                    and "RPT"."ConsolidatedAttribution_Snapshot"."EnrollmentStatus" = 'Active')


		Delete from RPT.PROVIDER_SCORECARD  where MEASURE_ID=@measure_id and REPORT_ID=@reportId and ROOT_COMPANIES_ID=@rootId;


	Insert into RPT.PROVIDER_SCORECARD(Measure_id,Measure_Name,Measure_Title,MEASURE_SUBTITLE,Measure_Type,Encounters,Gaps,Result,Target,Report_Id,ReportType,Report_Quarter,Period_Start_Date,Period_End_Date,Root_Companies_Id,To_Target)
	Select
		*
		,Case
			when Result > @target then FLOOR(cast((Encounters*Target) as decimal)/Result - Encounters)
			Else 0
		end as To_Target
	From
	(
		Select 
			Measure_id
			,Measure_Name
			,Measure_Title
			,MEASURE_SUBTITLE
			,Measure_Type
			,Sum(Encounters) as Encounters
			,Sum(Encounters) as Gaps
			,(Cast(Sum(Encounters) as float)/@popcount)*1000*12 as Result
			,@target as Target
			,Report_Id
			,ReportType
			,Report_Quarter
			,Period_Start_Date
			,Period_End_Date
			,Root_Companies_Id
		From
		(
			Select distinct
				EMPI
				,Measure_id
				,Measure_Name
				,@domain as Measure_Title
				,@subdomain as MEASURE_SUBTITLE
				,@measuretype as Measure_Type
				,Encounters
				,outlier
				,Report_Id
				,ReportType
				,Report_Quarter
				,Period_Start_Date
				,Period_End_Date
				,Root_Companies_Id
			From RPT.MEASURE_DETAILED_LINE
			where 
				Enrollment_Status='Active'  and
				MEASURE_ID=@measure_id and
				REPORT_ID=@reportId
		)t1
		Group by Measure_id,Measure_Name,Measure_Title,MEASURE_SUBTITLE,Report_Id,ReportType,Report_Quarter,Period_Start_Date,Period_End_Date,Root_Companies_Id,Measure_Type
	)t2


SET @PERF_ROW = @@ROWCOUNT
		SET @PERF_DURATION = DATEDIFF(MINUTE,@PERF_START,GETDATE());
		EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, 'Enrollment Completed', @PERF_ROW, @DURATION_IN_MIN=@PERF_DURATION, @LOG2ID=@LOGID;
		EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, @LOG2ID=@LOGID, @END_FLAG=1;

END TRY
BEGIN CATCH
		BEGIN
			EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, 'FATAL ERROR', @LOG2ID=@LOGID;
			RAISERROR ('',16,0)
			RETURN 16
        END
	END CATCH
RETURN 0
END

GO
