SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
CREATE PROCEDURE [dbo].[LOAD_STAGING_TO_PROD_LAB]

AS
BEGIN
	
	DECLARE @INFO VARCHAR(8000)
					,	@TRUE BIT = 1
					,	@FALSE BIT = 0
					,	@DB_ID INT = DB_ID()
					,	@LOGID INT = 0
					,	@PROC VARCHAR(500)=OBJECT_SCHEMA_NAME(@@PROCID)+'.'+OBJECT_NAME(@@PROCID)
					,	@PERF_START DATETIME
					,	@PERF_DURATION INT
					,	@PERF_ROW INT
					,	@RC	INT
					;

		EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, @INFO=@INFO, @LOG2ID=@LOGID OUT;
		DECLARE @PROCFULLNAME VARCHAR(500) = DB_NAME()+'.'+@PROC;
		----EXEC dbo.SP_MI_UTIL_SEND_MAIL @PROCFULLNAME, 'STARTED', 1, @ECHO=0; 
	DECLARE @MSG VARCHAR(1000)
	DECLARE @PERF_START_PROC DATETIME 
	DECLARE @PERF_RPS NUMERIC(10,2) 
	DECLARE @ROWS INT 
	DECLARE @PERF_ROWS INT 
	SET @PERF_ROWS = 0 
	SET @PERF_START = GETDATE() 
	SET @PERF_START_PROC = GETDATE() 

BEGIN TRY

Declare @Error_Keys varchar(100);
SET @Error_Keys=NULL;

SET @Error_Keys=(Select Distinct TOP 1 LAB_DATA_SRC FROM [KPI_ENGINE_STAGING].[dbo].[STAGING_LAB] M
LEFT JOIN (Select Distinct PAYER  FROM [dbo].[STAGING_TO_PROD_DEDUP_KEY] WHERE PROD_TABLE='LAB') DEDUPKEY
ON LAB_DATA_SRC=DEDUPKEY.PAYER 
where PAYER is NULL)

IF @Error_Keys IS NOT NULL
RAISERROR ('CREATE THE DEDUP KEYS FOR LAB: Select Distinct TOP 1 LAB_DATA_SRC FROM [KPI_ENGINE_STAGING].[dbo].[STAGING_LAB] M
LEFT JOIN (Select Distinct PAYER  FROM [dbo].[STAGING_TO_PROD_DEDUP_KEY] WHERE PROD_TABLE=''LAB'') DEDUPKEY
ON MEM_DATA_SRC=DEDUPKEY.PAYER 
where PAYER is NULL',16,1);



DEclare @PROD_TABLE varchar(100)
Declare @PAYER varchar(100)
Declare @ROOT_COMPANIES_ID int
Declare @KEYS varchar(500)
DECLARE @SQL VARCHAR(MAX)
DECLARE C CURSOR LOCAL FOR 
select
    PROD_TABLE
	,PAYER
	,ROOT_COMPANIES_ID
    ,stuff((
        select ' AND ' + CASE 
	WHEN KEY_COLUMNS_TYPE='VARCHAR' THEN 'ISNULL(L.'+KEY_COLUMNS+','''')' 
	WHEN KEY_COLUMNS_TYPE='INT' THEN 'ISNULL(L.'+KEY_COLUMNS+',''0'')' 
	WHEN KEY_COLUMNS_TYPE='DATE' THEN 'ISNULL(L.'+KEY_COLUMNS+',''1900-01-01'')' 
END+'='+CASE 
	WHEN KEY_COLUMNS_TYPE='VARCHAR' THEN 'ISNULL(PL.'+KEY_COLUMNS+','''')' 
	WHEN KEY_COLUMNS_TYPE='INT' THEN 'ISNULL(PL.'+KEY_COLUMNS+',''0'')' 
	WHEN KEY_COLUMNS_TYPE='DATE' THEN 'ISNULL(PL.'+KEY_COLUMNS+',''1900-01-01'')' 
END
        from [dbo].[STAGING_TO_PROD_DEDUP_KEY] t
        where t.PROD_TABLE = t1.PROD_TABLE AND t.PAYER = t1.PAYER AND t.ROOT_COMPANIES_ID = t1.ROOT_COMPANIES_ID
        order by t.KEY_COLUMNS
        for xml path('')
    ),1,4,'') as name_csv
from KPI_ENGINE.[dbo].[STAGING_TO_PROD_DEDUP_KEY] t1
where t1.PROD_TABLE='LAB'
group by 
PROD_TABLE
,PAYER
,ROOT_COMPANIES_ID

OPEN C  
FETCH NEXT FROM C INTO @PROD_TABLE,@PAYER,@ROOT_COMPANIES_ID,@KEYS

WHILE @@FETCH_STATUS = 0  
BEGIN  


SET @SQL='
UPDATE KPI_ENGINE.DBO.LAB
SET
       [ST_LAB_ID]= LTRIM(RTRIM(L.[ST_LAB_ID]))
      ,[MEMBER_ID]=LTRIM(RTRIM(L.[MEMBER_ID]))
      ,[TestCode]=LTRIM(RTRIM(L.[TestCode]))
      ,[TestName]=LTRIM(RTRIM(L.[TestName]))
      ,[OrderDate]=LTRIM(RTRIM(L.[OrderDate]))
      ,[CollectionDateTime]=LTRIM(RTRIM(L.[CollectionDateTime]))
      ,[ResultDate]=LTRIM(RTRIM(L.[ResultDate]))
      ,[ResultCode]=LTRIM(RTRIM(L.[ResultCode]))
      ,[ResultName]=LTRIM(RTRIM(L.[ResultName]))
      ,[Value]=LTRIM(RTRIM(L.[Value]))
      ,[Unit]=LTRIM(RTRIM(L.[Unit]))
      ,[ReferenceRange]=LTRIM(RTRIM(L.[ReferenceRange]))
      ,[Criticality]=LTRIM(RTRIM(L.[Criticality]))
      ,[OrderingProviderNPI]=LTRIM(RTRIM(L.[OrderingProviderNPI]))
      ,[ResultId]=LTRIM(RTRIM(L.[ResultId]))
      ,[PerformingLab]=LTRIM(RTRIM(L.[PerformingLab]))
      ,[Specimen]=LTRIM(RTRIM(L.[Specimen]))
      ,[Comments]=LTRIM(RTRIM(L.[Comments]))
      ,[LAB_DATA_SRC]=LTRIM(RTRIM(L.[LAB_DATA_SRC]))
      ,[ROOT_COMPANIES_ID]=LTRIM(RTRIM(L.[ROOT_COMPANIES_ID]))
      ,[FileDate]=LTRIM(RTRIM(L.[FileDate]))
      ,[FileName]=LTRIM(RTRIM(L.[FileName]))
      ,[LoadDateTime]=LTRIM(RTRIM(L.[LoadDateTime]))
FROM [KPI_ENGINE_STAGING].[dbo].[STAGING_LAB] L
INNER JOIN KPI_ENGINE.dbo.Open_empi EMPI 
on L.MEMBER_ID=EMPI.Org_Patient_Extension_ID AND (L.LAB_DATA_SRC=EMPI.[Data_Source] or L.LAB_DATA_SRC in(''FlatFiles-Gaps'',''FlatFiles'')) AND L.ROOT_COMPANIES_ID=EMPI.Root_Companies_ID
LEFT JOIN KPI_ENGINE.dbo.LAB PL
ON '+@KEYS+
' WHERE PL.MEMBER_ID IS NOT NULL AND L.LAB_DATA_SRC='''+@PAYER+''' AND L.Root_Companies_ID='+CONVERT(VARCHAR(10),@ROOT_COMPANIES_ID)


Declare @updatecount INT;
EXECUTE (@SQL);
SET @updatecount=@@ROWCOUNT;




SET @SQL='
INSERT INTO KPI_ENGINE.DBO.LAB
(
	   [ST_LAB_ID]
      ,[MEMBER_ID]
	  ,EMPI
      ,[TestCode]
      ,[TestName]
      ,[OrderDate]
      ,[CollectionDateTime]
      ,[ResultDate]
      ,[ResultCode]
      ,[ResultName]
      ,[Value]
      ,[Unit]
      ,[ReferenceRange]
      ,[Criticality]
      ,[OrderingProviderNPI]
      ,[ResultId]
      ,[PerformingLab]
      ,[Specimen]
      ,[Comments]
      ,[LAB_DATA_SRC]
      ,[ROOT_COMPANIES_ID]
      ,[FileDate]
      ,[FileName]
      ,[LoadDateTime]
)
SELECT
		 LTRIM(RTRIM(L.[ST_LAB_ID]))
		,LTRIM(RTRIM(L.[MEMBER_ID]))
		,LTRIM(RTRIM(EMPI.[EMPI_ID]))
		,LTRIM(RTRIM(L.[TestCode]))
		,LTRIM(RTRIM(L.[TestName]))
		,LTRIM(RTRIM(L.[OrderDate]))
		,LTRIM(RTRIM(L.[CollectionDateTime]))
		,LTRIM(RTRIM(L.[ResultDate]))
		,LTRIM(RTRIM(L.[ResultCode]))
		,LTRIM(RTRIM(L.[ResultName]))
		,LTRIM(RTRIM(L.[Value]))
		,LTRIM(RTRIM(L.[Unit]))
		,LTRIM(RTRIM(L.[ReferenceRange]))
		,LTRIM(RTRIM(L.[Criticality]))
		,LTRIM(RTRIM(L.[OrderingProviderNPI]))
		,LTRIM(RTRIM(L.[ResultId]))
		,LTRIM(RTRIM(L.[PerformingLab]))
		,LTRIM(RTRIM(L.[Specimen]))
		,LTRIM(RTRIM(L.[Comments]))
		,LTRIM(RTRIM(L.[LAB_DATA_SRC]))
		,LTRIM(RTRIM(L.[ROOT_COMPANIES_ID]))
		,LTRIM(RTRIM(L.[FileDate]))
		,LTRIM(RTRIM(L.[FileName]))
		,LTRIM(RTRIM(L.[LoadDateTime]))
FROM [KPI_ENGINE_STAGING].[dbo].[STAGING_LAB] L
INNER JOIN KPI_ENGINE.dbo.Open_empi EMPI 
on L.MEMBER_ID=EMPI.Org_Patient_Extension_ID AND (L.LAB_DATA_SRC=EMPI.[Data_Source] or L.LAB_DATA_SRC in(''FlatFiles-Gaps'',''FlatFiles'')) AND  L.ROOT_COMPANIES_ID=EMPI.Root_Companies_ID
LEFT JOIN KPI_ENGINE.dbo.LAB PL
ON '+@KEYS+
' WHERE PL.MEMBER_ID IS NULL AND L.LAB_DATA_SRC='''+@PAYER+''' AND L.Root_Companies_ID='+CONVERT(VARCHAR(10),@ROOT_COMPANIES_ID)

Declare @insertcount INT;
EXECUTE (@SQL);
SET @insertcount=@@ROWCOUNT;



SET @SQL='
INSERT INTO KPI_ENGINE_STAGING.DBO.STAGING_LAB_ERROR
(
	   [ST_LAB_ID]
      ,[MEMBER_ID]
      ,[TestCode]
      ,[TestName]
      ,[OrderDate]
      ,[CollectionDateTime]
      ,[ResultDate]
      ,[ResultCode]
      ,[ResultName]
      ,[Value]
      ,[Unit]
      ,[ReferenceRange]
      ,[Criticality]
      ,[OrderingProviderNPI]
      ,[ResultId]
      ,[PerformingLab]
      ,[Specimen]
      ,[Comments]
      ,[LAB_DATA_SRC]
      ,[ROOT_COMPANIES_ID]
      ,[FileDate]
      ,[FileName]
      ,[LoadDateTime]
)
SELECT
		 LTRIM(RTRIM(L.[ST_LAB_ID]))
		,LTRIM(RTRIM(L.[MEMBER_ID]))
		,LTRIM(RTRIM(L.[TestCode]))
		,LTRIM(RTRIM(L.[TestName]))
		,LTRIM(RTRIM(L.[OrderDate]))
		,LTRIM(RTRIM(L.[CollectionDateTime]))
		,LTRIM(RTRIM(L.[ResultDate]))
		,LTRIM(RTRIM(L.[ResultCode]))
		,LTRIM(RTRIM(L.[ResultName]))
		,LTRIM(RTRIM(L.[Value]))
		,LTRIM(RTRIM(L.[Unit]))
		,LTRIM(RTRIM(L.[ReferenceRange]))
		,LTRIM(RTRIM(L.[Criticality]))
		,LTRIM(RTRIM(L.[OrderingProviderNPI]))
		,LTRIM(RTRIM(L.[ResultId]))
		,LTRIM(RTRIM(L.[PerformingLab]))
		,LTRIM(RTRIM(L.[Specimen]))
		,LTRIM(RTRIM(L.[Comments]))
		,LTRIM(RTRIM(L.[LAB_DATA_SRC]))
		,LTRIM(RTRIM(L.[ROOT_COMPANIES_ID]))
		,LTRIM(RTRIM(L.[FileDate]))
		,LTRIM(RTRIM(L.[FileName]))
		,LTRIM(RTRIM(L.[LoadDateTime]))
FROM [KPI_ENGINE_STAGING].[dbo].[STAGING_LAB] L
LEFT JOIN KPI_ENGINE.dbo.Open_empi EMPI 
on L.MEMBER_ID=EMPI.Org_Patient_Extension_ID AND (L.LAB_DATA_SRC=EMPI.[Data_Source] or L.LAB_DATA_SRC in(''FlatFiles-Gaps'',''FlatFiles'')) AND  L.ROOT_COMPANIES_ID=EMPI.Root_Companies_ID
LEFT JOIN KPI_ENGINE_STAGING.DBO.STAGING_LAB_ERROR PL
ON '+@KEYS+
' WHERE PL.MEMBER_ID IS NULL AND EMPI.EMPI_ID IS NULL AND L.LAB_DATA_SRC='''+@PAYER+''' AND L.Root_Companies_ID='+CONVERT(VARCHAR(10),@ROOT_COMPANIES_ID)


Declare @errorcount INT;
EXECUTE (@SQL);
Set @errorcount=@@ROWCOUNT


-- Shanawaz 10-21-2021 -- Adding this  for data flow trace
Insert into KPI_ENGINE_RECON.dbo.PROD_INGESTION_STATS(Dimension,Source,UpdateCount,InsertCount,ErrorCount) values(@PROD_TABLE,@PAYER,@updatecount,@insertcount,@errorcount);


FETCH NEXT FROM C INTO @PROD_TABLE,@PAYER,@ROOT_COMPANIES_ID,@KEYS
END 

CLOSE C  
DEALLOCATE C
	
		SET @PERF_ROW = @@ROWCOUNT
		SET @PERF_DURATION = DATEDIFF(MINUTE,@PERF_START,GETDATE());
		EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, 'Enrollment Completed', @PERF_ROW, @DURATION_IN_MIN=@PERF_DURATION, @LOG2ID=@LOGID;
		EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, @LOG2ID=@LOGID, @END_FLAG=1;

END TRY
BEGIN CATCH  
    DECLARE @ErrorMessage NVARCHAR(4000);  
    DECLARE @ErrorSeverity INT;  
    DECLARE @ErrorState INT;  

	EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, 'FATAL ERROR', @LOG2ID=@LOGID;
  
    SELECT   
        @ErrorMessage = ERROR_MESSAGE(),  
        @ErrorSeverity = ERROR_SEVERITY(),  
        @ErrorState = ERROR_STATE();  
  
    -- Use RAISERROR inside the CATCH block to return error  
    -- information about the original error that caused  
    -- execution to jump to the CATCH block.  
    RAISERROR (@ErrorMessage, -- Message text.  
               @ErrorSeverity, -- Severity.  
               @ErrorState -- State.  
               );  
END CATCH;  

END
GO
