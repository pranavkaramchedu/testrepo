SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON


CREATE PROCEDURE [HDS].[KPI_HEDIS_COL] 
AS

BEGIN
DECLARE @INFO VARCHAR(8000)
					,	@TRUE BIT = 1
					,	@FALSE BIT = 0
					,	@DB_ID INT = DB_ID()
					,	@LOGID INT = 0
					,	@PROC VARCHAR(500)=OBJECT_SCHEMA_NAME(@@PROCID)+'.'+OBJECT_NAME(@@PROCID)
					,	@PERF_START DATETIME
					,	@PERF_DURATION INT
					,	@PERF_ROW INT
					,	@RC	INT
					;

		EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, @INFO=@INFO, @LOG2ID=@LOGID OUT;
		DECLARE @PROCFULLNAME VARCHAR(500) = DB_NAME()+'.'+@PROC;
		----EXEC dbo.SP_MI_UTIL_SEND_MAIL @PROCFULLNAME, 'STARTED', 1, @ECHO=0; 
	DECLARE @MSG VARCHAR(1000)
	DECLARE @PERF_START_PROC DATETIME 
	DECLARE @PERF_RPS NUMERIC(10,2) 
	DECLARE @ROWS INT 
	DECLARE @PERF_ROWS INT 
	SET @PERF_ROWS = 0 
	SET @PERF_START = GETDATE() 
	SET @PERF_START_PROC = GETDATE() 

BEGIN TRY


-- Declare Variables
declare @rundate Date=GetDate()
declare @meas_year varchar(4)=Year(Dateadd(month,-2,@rundate))
declare @rootId varchar(10)='159'


Declare @runid INT=0;
DECLARE @ce_startdt DATE;
DECLARE @ce_enddt DATE;
DECLARE @ce_startdt1 DATE;
DECLARE @ce_enddt1 DATE;
DECLARE @ce_startdt4 DATE;
DECLARE @ce_startdt9 DATE;
DECLARE @ce_startdt2 DATE;
DECLARE @meas VARCHAR(10);

	
Declare @startDate Date;
Declare @enddate date;
Declare @quarter varchar(20);
Declare @measure_id varchar(10);
Declare @target INT;
Declare @domain varchar(100);
Declare @subdomain varchar(100);
Declare @measuretype varchar(100);
Declare @measurename varchar(100);
Declare @reporttype varchar(100);
Declare @reportId INT;



SET @meas='COL';
SET @ce_startdt=concat(@meas_year,'-01-01');
SET @ce_enddt=concat(@meas_year,'-12-31');
SET @ce_startdt1=concat(@meas_year-1,'-01-01');
SET @ce_enddt1=concat(@meas_year-1,'-12-31');
SET @ce_startdt4=concat(@meas_year-4,'-01-01');
SET @ce_startdt9=concat(@meas_year-9,'-01-01');
SET @ce_startdt2=concat(@meas_year-2,'-01-01');

Set @reporttype='Physician'
Set @measurename='Colorectal Cancer Screening'
--Set @startDate=DATEADD(yy, DATEDIFF(yy, 0,Dateadd(month,-2,GETDATE())), 0) 
--Set @enddate=eomonth(Dateadd(month,-2,GetDate()))
--Set @quarter=Concat(Year(@enddate),' - Q',DATEPART(q, @enddate))
--Set @rootId=@rootId
set @target=84
Set @domain='Clinical Quality / Wellness and Prevention'
Set @subdomain='Adult Wellness and Prevention'
Set @measuretype='UHN'
Set @measure_id='23'

/*** To Test
select memid, epop from KPI_HEDIS.dbo.HEDIS_SCORE where ROOT_COMPANIES_ID='COL-S' 
except
select memid, epop  from HDS.HEDIS_MEASURE_OUTPUT where ROOT_COMPANIES_ID='COL-S'

select MemID,Meas,Payer,CE,Event,Excl,Num,RExcl,RExclD,Age,Gender from KPI_HEDIS.dbo.HEDIS_SCORE where ROOT_COMPANIES_ID='COL-S' 
except
select EMPI,Meas,Payer,CE,Event,Excl,Num,RExcl,RExclD,Age,Gender from #COLdataset

SELECT * FROM HDS.HEDIS_MEASURE_OUTPUT where ROOT_COMPANIES_ID='COL-S' AND memid=128337
SELECT * FROM KPI_HEDIS.dbo.HEDIS_SCORE WHERE MEMID=128337 AND ROOT_COMPANIES_ID='COL-S' 
select * from #COLdataset where empi=108285
*/


-- Eligible Patient List
drop table if exists #COL_memlist; 
CREATE TABLE #COL_memlist 
(
    EMPI varchar(100)
    
)
insert into #COL_memlist
SELECT DISTINCT 
	en.EMPI 
FROM member gm
join ENROLLMENT en on en.EMPI=gm.EMPI and 
					  en.ROOT_COMPANIES_ID=gm.ROOT_COMPANIES_ID
WHERE 
	en.ROOT_COMPANIES_ID=@rootId and
	en.EFF_DATE<=@ce_enddt AND 
	TERM_DATE>=@ce_startdt1  AND 
	YEAR(@ce_enddt)-YEAR(MEM_DOB) BETWEEN 51 AND 75 
ORDER BY 1;



-- Create Temp Patient Enrollment
drop table if exists #COL_tmpsubscriber;
CREATE TABLE #COL_tmpsubscriber (
    EMPI varchar(100),
    dob date,
	gender varchar(1),
	age INT,
	payer varchar(50),
	StartDate date,
	EndDate date,
)
insert into #COL_tmpsubscriber
SELECT distinct
	en.EMPI
	,gm.MEM_DOB
	,gm.MEM_GENDER
	,YEAR(@ce_enddt)-YEAR(MEM_DOB) as age
	,en.PAYER_TYPE
	,en.EFF_DATE
	,en.TERM_DATE  
FROM MEMBER gm
join ENROLLMENT en on en.EMPI=gm.EMPI and 
					  en.ROOT_COMPANIES_ID=gm.ROOT_COMPANIES_ID
WHERE 
	en.ROOT_COMPANIES_ID=@rootId and
	en.EFF_DATE<=@ce_enddt AND 
	TERM_DATE>=@ce_startdt1 AND 
	YEAR(@ce_enddt)-YEAR(MEM_DOB) BETWEEN 51 AND 75
	ORDER BY en.EMPI,en.EFF_DATE,en.TERM_DATE;



drop table if exists #COLdataset;
CREATE TABLE #COLdataset (
  EMPI varchar(100) NOT NULL,
  [meas] varchar(20) DEFAULT NULL,
  [payer] varchar(100) DEFAULT NULL,
  Gender varchar(45) NOT NULL,
  [age] INT,
  [orec] smallint DEFAULT NULL,
  [lis] smallint DEFAULT '0',
  [rexcl] smallint DEFAULT '0',
  [rexcld] smallint DEFAULT '0',
  [CE] smallint DEFAULT '0',
  [excl] smallint DEFAULT '0',
  [num] smallint DEFAULT '0',
  [Event] smallint DEFAULT '0'

) ;

insert into #COLdataset(EMPI,meas,payer,Gender,age)
Select distinct
	EMPI
	,'COL'
	,pm.PayerMapping
	,gender
	,age
From
(
	Select distinct
		EMPI
		,gender
		,age
		,case 
				when (Payer in('HMO','CEP','POS','PPO') and nxtpayer in('MD','MLI','MRB'))  then TRIM(Payer)
				When (Payer in('HMO','CEP','POS','PPO') and prvpayer in('MD','MLI','MRB'))  then TRIM(Payer)
				When (Payer in('MD','MLI','MRB') and nxtpayer in('HMO','CEP','POS','PPO'))  then TRIM(nxtpayer)
				When (Payer in('MD','MLI','MRB') and prvpayer in('HMO','CEP','POS','PPO'))  then TRIM(prvpayer)
				when (Payer in('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP') and nxtpayer in('HMO','CEP','POS','PPO'))  then TRIM(Payer)
				When (Payer in('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP') and prvpayer in('HMO','CEP','POS','PPO'))  then TRIM(Payer)
				When (Payer in('HMO','CEP','POS','PPO') and nxtpayer in('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP'))  then TRIM(nxtpayer)
				When (Payer in('HMO','CEP','POS','PPO') and prvpayer in('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP'))  then TRIM(prvpayer)
				else Payer 
			end as newpayer
	From
	(
		select
			*
			,isnull(lead(Payer) over(partition by EMPI order by Payer),Payer) as nxtpayer
			,isnull(lag(Payer) over(partition by EMPI order by Payer),Payer) as prvpayer
		From 
		(
			select
				EMPI
				,payer
				,gender
				,age
			From
			(
				select 
					EMPI
					,StartDate
					,EndDate
					,TRIM(payer) as payer
					,gender
					,age
					,RANK() over(partition by EMPI order by StartDate desc,EndDate Desc) as rn 
				from #COL_tmpsubscriber 
				where  
					StartDate<=@ce_enddt
				--	and EMPI=139080
			)t1
			where 
				rn=1
		)t2
		
	)t3
)t4
Join HDS.HEDIS_PAYER_MAPPING pm on 
	t4.newpayer=pm.payer and 
	pm.Measure_id='COL'
Order by 1




-- Continuous Enrollment
Drop table if exists #COL_contenroll
Create table #COL_contenroll
(
	EMPI varchar(100)
)
Insert into #COL_contenroll
select 
	t1.EMPI
from GetContinuousEnrolledEMPI(@rootId,@ce_startdt,@ce_enddt,45,0) t1
join
(
	select * from GetContinuousEnrolledEMPI(@rootId,@ce_startdt1,@ce_enddt1,45,1)
)t2 on t1.EMPI=t2.EMPI





	update #COLdataset set CE=1 from #COLdataset ds join #COL_contenroll ce on ds.EMPI=ce.EMPI;


	-- AdvancedIllness

	drop table if exists #COL_advillness;
	CREATE table #COL_advillness
	(
		EMPI varchar(100),
		servicedate date,
		claimid varchar(100)
	);
	Insert into #COL_advillness
	select
		EMPI
		,DIAG_START_DATE
		,CLAIM_ID 
	from advancedillness(@rootId,@ce_startdt1,@ce_enddt)
	Where
		DIAG_DATA_SRC not in(select DATA_SOURCE from DATA_SOURCE where supplemental=1)

	
	
	
	-- Required Exclusion

	drop table if exists #COL_reqdexcl
	CREATE table #COL_reqdexcl
	(
		EMPI varchar(100)
			
	)
	insert into #COL_reqdexcl
	Select distinct
		EMPI
	From palliativecare(@rootId,@ce_startdt,@ce_enddt)
		
	
	
	update #COLdataset set rexcld=1 from #COLdataset ds join #COL_reqdexcl re on ds.EMPI=re.EMPI;

	
	
	
	-- Members with Institutinal SNP
	UPDATE #COLdataset SET #COLdataset.rexcl=1 FROM #COLdataset ds JOIN ENROLLMENT s on ds.EMPI=s.EMPI and s.ROOT_COMPANIES_ID=@rootId WHERE  ds.age>=66 AND ds.payer IN('MCR','MCS','MP','MC','SN2','SN1','SN3','MR') AND s.EFF_DATE<=@ce_enddt AND s.TERM_DATE>=@ce_startdt AND s.PAYER_TYPE='SN2';


	-- LTI Exclusion
	drop table if exists #COL_LTImembers;
	CREATE table #COL_LTImembers
	(
		EMPI varchar(100)
	)
	Insert into #COL_LTImembers
	SELECT DISTINCT EMPI FROM MCFIELDS WHERE ROOT_COMPANIES_ID=@rootId AND RunDate BETWEEN @ce_startdt AND @ce_enddt AND LTI=1;

	
	update #COLdataset set rexcl=1 from #COLdataset ds join #COL_LTImembers re on ds.EMPI=re.EMPI where ds.age>=66 AND ds.payer IN('MCR','MCS','MP','MC','MMP','SN1','SN2','SN3','MR');


	 -- Hospice Exclusion

	drop table if exists #COL_hospicemembers;
	CREATE table #COL_hospicemembers
	(
		EMPI varchar(100)
		
	);
	Insert into #COL_hospicemembers
	select distinct EMPI from hospicemembers(@rootId,@ce_startdt,@ce_enddt)

	update #COLdataset set rexcl=1 from #COLdataset ds join #COL_hospicemembers hos on hos.EMPI=ds.EMPI;
	
	
-- Frailty Members LIST
	drop table if exists #COL_frailtymembers;
	CREATE table #COL_frailtymembers
	(
		
		EMPI varchar(100)
			
	);
	Insert into #COL_frailtymembers
	Select distinct
		EMPI
	From Frailty(@rootId,@ce_startdt,@ce_enddt)
	Where
		DataSource not in(select DATA_SOURCE from DATA_SOURCE where supplemental=1)

	
	
		
-- Required Exclusion 1

	-- Inpatient Stay List
	drop table if exists #COL_inpatientstaylist;
	CREATE table #COL_inpatientstaylist
	(
		EMPI varchar(100),
		date_s Date,
		claimid varchar(100)
	)
	Insert into #COL_inpatientstaylist
	select distinct 
		EMPI
		,FROM_DATE
		,CLAIM_ID 
	from Inpatientstays(@rootId,@ce_startdt1,@ce_enddt)
	Where
		CL_DATA_SRC not in(select DATA_SOURCE from DATA_SOURCE where supplemental=1)
	
	
	-- Non acute Inpatient stay list
	
	drop table if exists #COL_noncauteinpatientstaylist;
	CREATE table #COL_noncauteinpatientstaylist
	(
		EMPI varchar(100),
		date_s DATE,
		claimid varchar(100)
	);
	Insert into #COL_noncauteinpatientstaylist
	select distinct 
		EMPI
		,FROM_DATE
		,CLAIM_ID 
	from nonacutestays(@rootId,@ce_startdt1,@ce_enddt)
	Where
		CL_DATA_SRC not in(select DATA_SOURCE from DATA_SOURCE where supplemental=1)
	
	
	-- Outpatient and other visits
	drop table if exists #COL_visitlist;
	CREATE table #COL_visitlist
	(
		EMPI varchar(100),
		date_s date,
		claimid varchar(100)
	)
	Insert into #COL_visitlist
	Select distinct
		*
	From
	(
		
		Select
			EMPI
			,PROC_START_DATE
			,CLAIM_ID
		From GetProcedures(@rootId,@ce_startdt1,@ce_enddt,'Outpatient')

		Union all

		Select
			EMPI
			,PROC_START_DATE
			,CLAIM_ID
		From GetProcedures(@rootId,@ce_startdt1,@ce_enddt,'Observation')

		Union all

		Select
			EMPI
			,PROC_START_DATE
			,CLAIM_ID
		From GetProcedures(@rootId,@ce_startdt1,@ce_enddt,'ED')

		Union all

		Select
			EMPI
			,PROC_START_DATE
			,CLAIM_ID
		From GetProcedures(@rootId,@ce_startdt1,@ce_enddt,'Telephone Visits')

		Union all

		Select
			EMPI
			,PROC_START_DATE
			,CLAIM_ID
		From GetProcedures(@rootId,@ce_startdt1,@ce_enddt,'Online Assessments')

		Union all

		Select
			EMPI
			,PROC_START_DATE
			,CLAIM_ID
		From GetProcedures(@rootId,@ce_startdt1,@ce_enddt,'Nonacute Inpatient')

		Union all

		select 
			EMPI
			,FROM_DATE
			,CLAIM_ID 
		from CLAIMLINE 
		where 
			ROOT_COMPANIES_ID=@rootId and 
			ISNULL(POS,'')!='81' and 
			CL_DATA_SRC not in(select DATA_SOURCE from DATA_SOURCE where supplemental=1) and
			FROM_DATE between @ce_startdt1 and @ce_enddt and
			REV_CODE in
			(
				select code from HDS.VALUESET_TO_CODE where code_system='UBREV' and  Value_Set_Name in('Outpatient','ED')
			)
	)t1

	
	-- Required exclusion table
	drop table if exists #COL_reqdexcl1;
	CREATE table #COL_reqdexcl1
	(
		EMPI varchar(100)
			
	)
	Insert into #COL_reqdexcl1
	select distinct 
		t3.EMPI 
	from
	(
		select 
			t2.EMPI 
		from
		(
			select distinct 
				t1.EMPI
				,t1.date_s 
			from
			(
					select 
						EMPI
						,date_s
						,claimid  
					from #COL_visitlist 
					
					
					union all
					
					select 
						na.EMPI
						,na.Date_s
						,na.claimid 
					from #COL_noncauteinpatientstaylist na
					join #COL_inpatientstaylist inp on na.EMPI=inp.EMPI and 
													   na.claimid=inp.claimid
					
			)t1
			Join #COL_advillness a on a.EMPI=t1.EMPI and 
									  a.claimid=t1.claimid
		)t2 
		group by t2.EMPI 
		having count(t2.EMPI)>1
	)t3 
	Join #COL_frailtymembers f on f.EMPI=t3.EMPI
	
	
	update #COLdataset set rexcl=1 from #COLdataset ds
	join #COL_reqdexcl1 re1 on re1.EMPI=ds.EMPI and ds.age BETWEEN 66 AND 80;


-- Required Exclusion 2

	-- Acute Inpatient with Advanced Illness
	drop table if exists #COL_reqdexcl2;
	CREATE table #COL_reqdexcl2
	(
		EMPI varchar(100)
				
	)
	insert into #COL_reqdexcl2
	select distinct 
		t2.EMPI 
	from
	(
		select 
			t1.EMPI 
		from 
		(
			Select distinct
				EMPI
				,PROC_START_DATE
				,CLAIM_ID
			From GetProcedures(@rootId,@ce_startdt1,@ce_enddt,'Acute Inpatient')
		
		)t1
		Join #COL_advillness a on a.EMPI=t1.EMPI and a.claimid=t1.CLAIM_ID
	)t2
	join #COL_frailtymembers f on f.EMPI=t2.EMPI

	
	update #COLdataset set rexcl=1 from #COLdataset ds
	join #COL_reqdexcl2 re2 on re2.EMPI=ds.EMPI and ds.age BETWEEN 66 AND 80;
	
			
	-- Required exclusion 3
	drop table if exists #COL_reqdexcl3;
	CREATE table #COL_reqdexcl3
	(
		EMPI varchar(100)
			
	)
	insert into #COL_reqdexcl3
	select distinct 
		t2.EMPI 
	from
	(
		select 
			t1.EMPI
			,t1.date_s
			,t1.claimid 
		from
		(
			select 
				inp.EMPI
				,inp.date_s
				,inp.claimid 
			from #COL_inpatientstaylist inp
			left outer join #COL_noncauteinpatientstaylist na on inp.EMPI=na.EMPI and 
																 inp.claimid=na.claimid
			where na.EMPI is null
		)t1
		join #COL_advillness a on a.EMPI=t1.EMPI and a.claimid=t1.claimid
	)t2
	join #COL_frailtymembers f on f.EMPI=t2.EMPI

		
	update #COLdataset set rexcl=1 from #COLdataset ds
	join #COL_reqdexcl3 re3 on re3.EMPI=ds.EMPI and ds.age BETWEEN 66 AND 80;

		
-- RequiredExcl 4
	drop table if exists #COL_reqdexcl4;
	CREATE table #COL_reqdexcl4
	(
		EMPI varchar(100)
				
	)
	insert into #COL_reqdexcl4
	select 
		t1.EMPI 
	from
	(
		select 
			EMPI 
		from MEDICATION 
		where 
			ROOT_COMPANIES_ID=@rootId  and 
			MED_DATA_SRC in(select DATA_SOURCE from DATA_SOURCE where supplemental=0) and 
			FILL_DATE between @ce_startdt1 and @ce_enddt and 
			MEDICATION_CODE in
			(
				select code from HDS.MEDICATION_LIST_TO_CODES where Medication_List_Name='Dementia Medications'
			)
			

	)t1
	Join #COL_frailtymembers f on f.EMPI=t1.EMPI;

		
	update #COLdataset set rexcl=1 from #COLdataset ds
	join #COL_reqdexcl4 re4 on re4.EMPI=ds.EMPI and ds.age BETWEEN 66 AND 80;

	
	

-- Numerator******************

	drop table if exists #COL_numlist;
	CREATE table #COL_numlist
	(
		EMPI varchar(100)
		
	)
	Insert into #COL_numlist
	select distinct 
		EMPI 
	from
	(
			Select
		EMPI
		,PROC_START_DATE as ServiceDate
		,PROC_CODE as Code
	From PROCEDURES
	where 
	ROOT_COMPANIES_ID=@rootId and 
	ISNULL(PROC_STATUS,'EVN')!='INT' and 
	PROC_START_DATE between @ce_startdt and @ce_enddt and
	PROC_CODE in 
	(
	select code from HDS.VALUESET_TO_CODE where Value_Set_Name='FOBT Lab Test'
	)

	Union All
	Select
		EMPI
		,DIAG_START_DATE as ServiceDate
		,DIAG_CODE as Code
	From DIAGNOSIS
	where 
	ROOT_COMPANIES_ID=@rootId and 
	DIAG_START_DATE between @ce_startdt and @ce_enddt and
	DIAG_CODE in 
	(
	select code from HDS.VALUESET_TO_CODE where Value_Set_Name IN('FOBT Lab Test','FOBT Test Result or Finding')
	)

	Union All
	Select
		EMPI
		,ResultDate as ServiceDate
		,Coalesce(TestCode,ResultCode) as Code
	FROM LAB
	Where
		ROOT_COMPANIES_ID=@rootId and
		ResultDate between @ce_startdt and @ce_enddt and
		(
			ResultCode in
			(
				select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('FOBT Lab Test','FOBT Test Result or Finding')
			)
			or
			TestCode in
			(
				select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('FOBT Lab Test','FOBT Test Result or Finding')
			)
		)
		
	
	Union all

	Select
		EMPI
		,PROC_START_DATE as ServiceDate
		,PROC_CODE as Code
	From GetProcedures(@rootId,@ce_startdt4,@ce_enddt,'Flexible Sigmoidoscopy')

	Union all

	Select
		EMPI
		,PROC_START_DATE as ServiceDate
		,ICDPCS_CODE as Code
	From GetICDPCS(@rootId,@ce_startdt4,@ce_enddt,'Flexible Sigmoidoscopy')

	Union all

	Select
		EMPI
		,PROC_START_DATE as ServiceDate
		,PROC_CODE as Code
	From GetProcedures(@rootId,@ce_startdt4,@ce_enddt,'History of Flexible Sigmoidoscopy')
	Union all
	select
			EMPI
		,DIAG_START_DATE as ServiceDate
		,DIAG_CODE as Code
		From GetDiagnosis(@rootId,@ce_startdt4,@ce_enddt,'History of Flexible Sigmoidoscopy')
		
	Union all

	-- Colonoscopy

	Select
		EMPI
		,PROC_START_DATE as ServiceDate
		,PROC_CODE as Code
	From GetProcedures(@rootId,@ce_startdt9,@ce_enddt,'Colonoscopy')

	Union All

	Select
		EMPI
		,PROC_START_DATE as ServiceDate
		,ICDPCS_CODE as Code
	From GetICDPCS(@rootId,@ce_startdt9,@ce_enddt,'Colonoscopy')
	Union all
	select
			EMPI
		,DIAG_START_DATE as ServiceDate
		,DIAG_CODE as Code
		From GetDiagnosis(@rootId,@ce_startdt9,@ce_enddt,'History of Colonoscopy')
		
	Union All

	Select
		EMPI
		,PROC_START_DATE as ServiceDate
		,PROC_CODE as Code
	From GetProcedures(@rootId,@ce_startdt9,@ce_enddt,'History of Colonoscopy')

	Union All
	
	-- CT Colonograpgy

			Select
		EMPI
		,PROC_START_DATE as ServiceDate
		,PROC_CODE as Code
	From PROCEDURES
	where 
	ROOT_COMPANIES_ID=@rootId and 
	ISNULL(PROC_STATUS,'EVN')!='INT' and 
	PROC_START_DATE between @ce_startdt4 and @ce_enddt and
	PROC_CODE in 
	(
	select code from HDS.VALUESET_TO_CODE where Value_Set_Name='CT Colonography'
	)
	Union All

	Select
		EMPI
		,ResultDate as ServiceDate
		,Coalesce(TestCode,ResultCode) as Code
	FROM LAB
	Where
		ROOT_COMPANIES_ID=@rootId and
		ResultDate between @ce_startdt4 and @ce_enddt and
		(
			ResultCode in
			(
				select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('CT Colonography')
			)
			or
			TestCode in
			(
				select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('CT Colonography')
			)
		)
	
	   Union all

	   -- FIT

	     			Select
		EMPI
		,PROC_START_DATE as ServiceDate
		,PROC_CODE as Code
	From PROCEDURES
	where 
	ROOT_COMPANIES_ID=@rootId and 
	ISNULL(PROC_STATUS,'EVN')!='INT' and 
	PROC_START_DATE between @ce_startdt2 and @ce_enddt and
	PROC_CODE in 
	(
	select code from HDS.VALUESET_TO_CODE where Value_Set_Name='FIT DNA Lab Test'
	)
	   Union all
	      select
			EMPI
		,DIAG_START_DATE as ServiceDate
		,DIAG_CODE as Code
		From GetDiagnosis(@rootId,@ce_startdt2,@ce_enddt,'FIT DNA Test Result or Finding')
		
	Union All
	
	   	Select
			EMPI
			,ResultDate as ServiceDate
			,Coalesce(TestCode,ResultCode) as Code
		FROM LAB
		Where
			ROOT_COMPANIES_ID=@rootId and
			ResultDate between @ce_startdt2 and @ce_enddt and
			(
				ResultCode in
				(
					select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('FIT DNA Lab Test','FIT DNA Test Result or Finding')
				)
				or
				TestCode in
				(
					select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('FIT DNA Lab Test','FIT DNA Test Result or Finding')
				)
			)

	)t1

	update #COLdataset set num=1 from #COLdataset ds
	join #COL_numlist num on ds.EMPI=num.EMPI ;
	


	-- Numerator Details*********

	drop table if exists #COL_numdetails;
	CREATE table #COL_numdetails
	(
		EMPI varchar(100),
		Code varchar(20),
		ServiceDate Date
			
	)
	Insert into #COL_numdetails
	select
		EMPI
		,Code
		,ServiceDate
	From
	(
		select 
			EMPI 
			,Code
			,ServiceDate
			,row_number() over(partition by EMPI order by ServiceDate Desc) as rn
		from
		(

				Select
		EMPI
		,PROC_START_DATE as ServiceDate
		,PROC_CODE as Code
	From PROCEDURES
	where 
	ROOT_COMPANIES_ID=@rootId and 
	ISNULL(PROC_STATUS,'EVN')!='INT' and 
	PROC_START_DATE between @ce_startdt and @ce_enddt and
	PROC_CODE in 
	(
	select code from HDS.VALUESET_TO_CODE where Value_Set_Name='FOBT Lab Test'
	)

	Union All
		Select
		EMPI
		,DIAG_START_DATE as ServiceDate
		,DIAG_CODE as Code
	From DIAGNOSIS
	where 
	ROOT_COMPANIES_ID=@rootId and 
	DIAG_START_DATE between @ce_startdt and @ce_enddt and
	DIAG_CODE in 
	(
	select code from HDS.VALUESET_TO_CODE where Value_Set_Name IN('FOBT Lab Test','FOBT Test Result or Finding')
	)

	Union All
	Select
		EMPI
		,ResultDate as ServiceDate
		,Coalesce(TestCode,ResultCode) as Code
	FROM LAB
	Where
		ROOT_COMPANIES_ID=@rootId and
		ResultDate between @ce_startdt and @ce_enddt and
		(
			ResultCode in
			(
				select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('FOBT Lab Test','FOBT Test Result or Finding')
			)
			or
			TestCode in
			(
				select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('FOBT Lab Test','FOBT Test Result or Finding')
			)
		)
		
	
	Union all

	Select
		EMPI
		,PROC_START_DATE as ServiceDate
		,PROC_CODE as Code
	From GetProcedures(@rootId,@ce_startdt4,@ce_enddt,'Flexible Sigmoidoscopy')

	Union all

	Select
		EMPI
		,PROC_START_DATE as ServiceDate
		,ICDPCS_CODE as Code
	From GetICDPCS(@rootId,@ce_startdt4,@ce_enddt,'Flexible Sigmoidoscopy')

	Union all

	Select
		EMPI
		,PROC_START_DATE as ServiceDate
		,PROC_CODE as Code
	From GetProcedures(@rootId,@ce_startdt4,@ce_enddt,'History of Flexible Sigmoidoscopy')
	Union all
	select
			EMPI
		,DIAG_START_DATE as ServiceDate
		,DIAG_CODE as Code
		From GetDiagnosis(@rootId,@ce_startdt4,@ce_enddt,'History of Flexible Sigmoidoscopy')
		
	Union all

	-- Colonoscopy

	Select
		EMPI
		,PROC_START_DATE as ServiceDate
		,PROC_CODE as Code
	From GetProcedures(@rootId,@ce_startdt9,@ce_enddt,'Colonoscopy')

	Union All

	Select
		EMPI
		,PROC_START_DATE as ServiceDate
		,ICDPCS_CODE as Code
	From GetICDPCS(@rootId,@ce_startdt9,@ce_enddt,'Colonoscopy')

	Union All

	Select
		EMPI
		,PROC_START_DATE as ServiceDate
		,PROC_CODE as Code
	From GetProcedures(@rootId,@ce_startdt9,@ce_enddt,'History of Colonoscopy')
	Union all
	select
			EMPI
		,DIAG_START_DATE as ServiceDate
		,DIAG_CODE as Code
		From GetDiagnosis(@rootId,@ce_startdt9,@ce_enddt,'History of Colonoscopy')
		
	Union All
	
	-- CT Colonograpgy

			Select
		EMPI
		,PROC_START_DATE as ServiceDate
		,PROC_CODE as Code
	From PROCEDURES
	where 
	ROOT_COMPANIES_ID=@rootId and 
	ISNULL(PROC_STATUS,'EVN')!='INT' and 
	PROC_START_DATE between @ce_startdt4 and @ce_enddt and
	PROC_CODE in 
	(
	select code from HDS.VALUESET_TO_CODE where Value_Set_Name='CT Colonography'
	)
	Union All

	Select
		EMPI
		,ResultDate as ServiceDate
		,Coalesce(TestCode,ResultCode) as Code
	FROM LAB
	Where
		ROOT_COMPANIES_ID=@rootId and
		ResultDate between @ce_startdt4 and @ce_enddt and
		(
			ResultCode in
			(
				select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('CT Colonography')
			)
			or
			TestCode in
			(
				select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('CT Colonography')
			)
		)
	
	   Union all

	   -- FIT

	   			Select
		EMPI
		,PROC_START_DATE as ServiceDate
		,PROC_CODE as Code
	From PROCEDURES
	where 
	ROOT_COMPANIES_ID=@rootId and 
	ISNULL(PROC_STATUS,'EVN')!='INT' and 
	PROC_START_DATE between @ce_startdt2 and @ce_enddt and
	PROC_CODE in 
	(
	select code from HDS.VALUESET_TO_CODE where Value_Set_Name='FIT DNA Lab Test'
	)
	   Union all
	   select
			EMPI
		,DIAG_START_DATE as ServiceDate
		,DIAG_CODE as Code
		From GetDiagnosis(@rootId,@ce_startdt2,@ce_enddt,'FIT DNA Test Result or Finding')
		
	Union All
	
	   	Select
			EMPI
			,ResultDate as ServiceDate
			,Coalesce(TestCode,ResultCode) as Code
		FROM LAB
		Where
			ROOT_COMPANIES_ID=@rootId and
			ResultDate between @ce_startdt2 and @ce_enddt and
			(
				ResultCode in
				(
					select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('FIT DNA Lab Test','FIT DNA Test Result or Finding')
				)
				or
				TestCode in
				(
					select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('FIT DNA Lab Test','FIT DNA Test Result or Finding')
				)
			)

	
		)t1
	)t2
	Where rn=1

		
	
	
-- Optional Exclussion

	-- Colorectal Cancer Value Set
	drop table if exists #COL_ColorectalCancer;
	CREATE table #COL_ColorectalCancer
	(
		EMPI varchar(100)
		
	)
	insert into #COL_ColorectalCancer
	Select distinct 
		EMPI 
	from
	(
		
		Select
			EMPI
		From GetProcedures(@rootId,'1900-01-01',@ce_enddt,'Colorectal Cancer')


		Union all
		Select
			EMPI
		From GetDiagnosis(@rootId,'1900-01-01',@ce_enddt,'Colorectal Cancer')


		Union all
		
		Select
			EMPI
		From GetProcedures(@rootId,'1900-01-01',@ce_enddt,'Total Colectomy')

		Union all
		Select
			EMPI
		From GetICDPCS(@rootId,'1900-01-01',@ce_enddt,'Total Colectomy')

		Union all
		select
			EMPI
		From GetDiagnosis(@rootId,'1900-01-01',@ce_enddt,'Total Colectomy')
		Union all
	
		select
			EMPI
		From GetDiagnosis(@rootId,'1900-01-01',@ce_enddt,'History of Total Colectomy')
		

)t1



update #COLdataset set Excl=1 from #COLdataset ds join #COL_ColorectalCancer cc on ds.empi= cc.empi



	


--SES Startification

update #COLdataset set lis=1 from #COLdataset ds join MCFIELDS m on m.EMPI=ds.EMPI and m.ROOT_COMPANIES_ID=@rootId
where m.RunDate between @ce_startdt1 and @ce_enddt and m.Premium_LIS_Amount>0 and ds.payer IN('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP','MR');

update #COLdataset set lis=0
update #COLdataset 
	set lis=1 
from #COLdataset ds 
join MCFIELDS l on 
	l.EMPI=ds.EMPI and 
	l.ROOT_COMPANIES_ID=@rootId
where 
	ds.payer IN('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP','MR') and 
	(
		Low_Income_Period_Start_Date<=@ce_enddt and Low_Income_Period_End_Date>=@ce_startdt1
		or
		Low_Income_Period_Start_Date>=@ce_startdt1 and Low_Income_Period_End_Date is NULL
	)

-- Item 48
drop table if exists #COL_orec;
CREATE table #COL_orec
(
	EMPI varchar(100),
	orec varchar(2)
			
)
Insert into #COL_orec
Select
	EMPI
	,OREC
From
(
	select 
		EMPI
		,OREC
		,row_number() over (Partition by EMPI order by RunDate Desc,PayDate Desc) as rn
	From MCFIELDS
	Where
		ROOT_COMPANIES_ID=@rootId and
		RunDate between @ce_startdt1 and @ce_enddt and
		OREC is not null
)t1
Where
	rn=1


update #COLdataset set orec=o.orec from #COLdataset ds
join #COL_orec o on o.EMPI=ds.EMPI
where ds.payer IN('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP','MR');

update #COLdataset set meas='COLNON' WHERE orec=0 AND lis=0 AND ce=1 AND rexcl=0 and rexcld=0 AND payer IN ('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP','MR');


UPDATE #COLdataset SET meas='COLLISDE' WHERE orec=0 AND lis=1 AND ce=1 AND rexcl=0 and rexcld=0 AND payer IN ('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP','MR');

UPDATE #COLdataset SET meas='COLDIS' WHERE orec IN(1,3) AND lis=0 AND ce=1 AND rexcl=0 and rexcld=0 AND payer IN ('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP','MR');

UPDATE #COLdataset SET meas='COLCMB' WHERE orec IN(1,3) AND lis=1 AND ce=1 AND rexcl=0 and rexcld=0 AND payer IN ('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP','MR');


UPDATE #COLdataset SET meas='COLOT' WHERE orec IN(2,9) AND ce=1 AND rexcl=0 and rexcld=0 AND payer IN ('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP','MR');



-- Get ReportId from Report_Details

	exec GetReportDetail @rundate=@rundate,@rootId=@rootId,@startDate=@startDate output,@enddate=@enddate output,@quarter=@quarter output,@reportId=@reportId output

	
	Delete from HDS.HEDIS_MEASURE_OUTPUT  where MEASURE_ID=@meas and RUN_ID=@reportId and ROOT_COMPANIES_ID=@rootId

	Insert into HDS.HEDIS_MEASURE_OUTPUT(memid,Meas,payer,CE,Event,Epop,Excl,Num,Rexcl,RExclD,Age,Gender,Measure_ID,Measurement_year,RUN_ID,ROOT_COMPANIES_ID)
	SELECT EMPI,meas,payer,CE,0 as EVENT,CASE WHEN CE=1  AND rexcl=0 and rexcld=0 and payer!='MCD' THEN 1 ELSE 0 END AS epop,excl,num,rexcl,rexcld,cast(age as Int) AS age,gender,@meas,@meas_year,@reportId,@rootId FROM #COLdataset 


	-- Insert data into Measure Detailed Line
	Delete from RPT.MEASURE_DETAILED_LINE where MEASURE_ID=@measure_id and REPORT_ID=@reportId and ROOT_COMPANIES_ID=@rootId;

	Insert into RPT.MEASURE_DETAILED_LINE(Provider_Id,PCP_NPI,PCP_NAME,Practice_Name,Specialty,Measure_id,Measure_Name,Payer,PayerId,MEM_FNAME,MEM_MName,MEM_LNAME,MEM_DOB,MEM_GENDER,ENROLLMENT_STATUS,Last_visit_date,Product_Type,Num,Den,Excl,Rexcl,CE,Event,Epop,Report_Id,ReportType,Report_Quarter,Period_Start_date,Period_end_Date,Root_Companies_id,EMPI,MEASURE_TYPE,Code,DateofService)
	Select 
		a.AmbulatoryPCPNPI as Provider_Id 
		,a.AmbulatoryPCPNPI
		,a.AmbulatoryPCPName as PCP_NAME
		,a.AmbulatoryPCPPractice as Practice_Name
		,a.AmbulatoryPCPSpecialty as Specialty
		,@measure_id
		,@measurename
		,a.DATA_SOURCE as Payer
		,a.PayerId
		,a.MemberFirstName as MEM_FNAME
		,a.MemberMiddleName as MEM_MName
		,a.MemberLastName as MEM_LNAME
		,a.MemberDOB
		,a.MEM_GENDER
		,a.EnrollmentStatus
		,a.AmbulatoryPCPRecentVisit as Last_visit_date
		,d.Payer
		,Num
		,1 as Den
		,Excl
		,case
			when Rexcl=1 or rexcld=1 then 1
			else 0
		end as Rexcl
		,CE
		,1 as Event
		,1 as epop
		,@reportId
		,@reporttype
		,@quarter
		,@startDate
		,@enddate
		,@rootId
		,d.EMPI as EMPI
		,@measuretype
		,nd.Code
		,nd.ServiceDate
	From #coldataset d
	join KPI_ENGINE.RPT.PCP_ATTRIBUTION a on d.EMPI=a.EMPI and a.Reportid=@reportId
	Left outer join #col_numdetails nd on d.EMPI=nd.EMPI
	--	where a.AmbulatoryPCPSpecialty in('Family Medicine','General Practice','Gynecology','Internal Medicine','Obstetrics & Gynecology')



	-- Insert data into Provider Scorecard
	Delete from RPT.PROVIDER_SCORECARD  where MEASURE_ID=@measure_id and REPORT_ID=@reportId and ROOT_COMPANIES_ID=@rootId;

Insert into RPT.PROVIDER_SCORECARD(Provider_Id,PCP_NPI,PCP_NAME,Specialty,Practice_Name,Measure_id,Measure_Name,Measure_Title,MEASURE_SUBTITLE,Measure_Type,NUM_COUNT,DEN_COUNT,Excl_Count,Rexcl_Count,Gaps,Result,Target,To_Target,Report_Id,ReportType,Report_Quarter,Period_Start_Date,Period_End_Date,Root_Companies_Id)
Select 
	Provider_Id
	,PCP_NPI
	,PCP_NAME
	,Specialty
	,Practice_Name
	,Measure_id
	,Measure_Name
	,Measure_Title
	,MEASURE_SUBTITLE
	,Measure_Type
	,SUM(Cast(NUM_COUNT as INT)) as NUM_COUNT
	,SUM(Cast(DEN_COUNT as INT)) as DEN_COUNT
	,SUM(Cast(Excl_Count as INT)) as Excl_Count
	,Sum(Cast(Rexcl_count as INT)) as Rexcl_Count
	,sum(Cast(DEN_Excl as INT)) - SUM(Cast(NUM_COUNT as INT)) as Gaps
	,Case
		when SUM(Cast(DEN_Excl as Float))>0 Then Round((SUM(cast(NUM_COUNT as Float))/ISNULL(NULLIF(SUM(Cast(DEN_Excl as Float)),0),1))*100,2)
		else 0
	end as Result
	,Target
	,Case
		when ((SUM(Cast(DEN_Excl as INT)))*(cast(Target*0.01 as float))) - SUM(Cast(NUM_COUNT as INT))>0 Then CEILING(((SUM(Cast(DEN_Excl as INT)))*(cast(Target*0.01 as float))) - SUM(Cast(NUM_COUNT as INT)))
		Else 0
	end as To_Target
	,Report_Id
	,ReportType
	,Report_Quarter
	,Period_Start_Date
	,Period_End_Date
	,Root_Companies_Id
From
(
	Select distinct
		m.EMPI
		,a.NPI as Provider_id
		,a.NPI as PCP_NPI
		,a.Prov_Name as PCP_Name
		,a.Specialty
		,a.Practice as Practice_Name
		,m.Measure_id
		,m.Measure_Name
		,l.measure_Title
		,l.Measure_SubTitle
		,'Calculated' as Measure_Type
		,Case
			when NUM=1 and excl=0 and rexcl=0 Then 1
			else 0
		end as NUM_COUNT
		,DEN as DEN_COUNT
		,Case
			When DEN=1 and Excl=0 and Rexcl=0 Then 1
			else 0
		End as Den_excl
		,Excl as Excl_Count
		,Rexcl as Rexcl_count
		,Report_Id
		,l.ReportType
		,Report_Quarter
		,Period_Start_Date
		,Period_End_Date
		,m.Root_Companies_Id
		,l.Target
	From RPT.MEASURE_DETAILED_LINE m
	Join RPT.ConsolidatedAttribution_Snapshot a on
		m.EMPI=a.EMPI
	Join RFT.UHN_measuresList l on
		m.Measure_Id=l.measure_id
	Join RFT.UHN_MeasureSpecialtiesMapping s on
		a.Specialty=s.Specialty and
		m.MEASURE_ID=s.Measure_id
	where Enrollment_Status='Active' and
		  a.NPI!='' and
		  m.MEASURE_ID=@measure_id and
		  REPORT_ID=@reportId 
)t1
Group by Provider_Id,PCP_NPI,PCP_NAME,Specialty,Practice_Name,Measure_id,Measure_Name,Measure_Title,MEASURE_SUBTITLE,Report_Id,ReportType,Report_Quarter,Period_Start_Date,Period_End_Date,Root_Companies_Id,Measure_Type,Target



-- Adding logic to add supporting Gap Report

Drop Table if exists #col_supportingreport;
select
	EMPI
	,Code
	,ServiceDate
	,TestType
	,rn
	into #col_supportingreport
From
(
	select 
		EMPI 
		,Code
		,ServiceDate
		,TestType
		,row_number() over(partition by EMPI order by ServiceDate Desc) as rn
	from
	(

		Select
			EMPI
			,PROC_START_DATE as ServiceDate
			,PROC_CODE as Code,
			'FOBT' TestType
		From PROCEDURES
		where 
			ROOT_COMPANIES_ID=@rootId and 
			ISNULL(PROC_STATUS,'EVN')!='INT' and 
			PROC_START_DATE between @ce_startdt and @ce_enddt and
			PROC_CODE in 
			(
				select code from HDS.VALUESET_TO_CODE where Value_Set_Name='FOBT Lab Test'
			)

		Union All

		Select
			EMPI
			,DIAG_START_DATE as ServiceDate
			,DIAG_CODE as Code,
			'FOBT' TestType
		From DIAGNOSIS
		where 
			ROOT_COMPANIES_ID=@rootId and 
			DIAG_START_DATE between @ce_startdt and @ce_enddt and
			DIAG_CODE in 
			(
			select code from HDS.VALUESET_TO_CODE where Value_Set_Name IN('FOBT Lab Test','FOBT Test Result or Finding')
			)

		Union All

		Select
			EMPI
			,ResultDate as ServiceDate
			,Coalesce(TestCode,ResultCode) as Code,
			'FOBT' TestType
		FROM LAB
		Where
			ROOT_COMPANIES_ID=159 and
			ResultDate between @ce_startdt and @ce_enddt and
			(
				ResultCode in
				(
					select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('FOBT Lab Test','FOBT Test Result or Finding')
				)
				or
				TestCode in
				(
					select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('FOBT Lab Test','FOBT Test Result or Finding')
				)
			)
		
	
		Union all

		Select
			EMPI
			,PROC_START_DATE as ServiceDate
			,PROC_CODE as Code
			,'Sigmoidoscopy' as TestType
		From GetProcedures(@rootId,@ce_startdt4,@ce_enddt,'Flexible Sigmoidoscopy')

		Union all

		Select
			EMPI
			,PROC_START_DATE as ServiceDate
			,ICDPCS_CODE as Code
			,'Sigmoidoscopy' as TestType
		From GetICDPCS(@rootId,@ce_startdt4,@ce_enddt,'Flexible Sigmoidoscopy')

		Union all

		Select
			EMPI
			,PROC_START_DATE as ServiceDate
			,PROC_CODE as Code
			,'Sigmoidoscopy' as TestType
		From GetProcedures(@rootId,@ce_startdt4,@ce_enddt,'History of Flexible Sigmoidoscopy')
		
		Union all
		
		select
			EMPI
			,DIAG_START_DATE as ServiceDate
			,DIAG_CODE as Code
			,'Sigmoidoscopy' as TestType
		From GetDiagnosis(@rootId,@ce_startdt4,@ce_enddt,'History of Flexible Sigmoidoscopy')
		
		Union all

		-- Colonoscopy

		Select
			EMPI
			,PROC_START_DATE as ServiceDate
			,PROC_CODE as Code
			,'Colonoscopy' as TestType
		From GetProcedures(@rootId,@ce_startdt9,@ce_enddt,'Colonoscopy')

		Union All

		Select
			EMPI
			,PROC_START_DATE as ServiceDate
			,ICDPCS_CODE as Code
			,'Colonoscopy' as TestType
		From GetICDPCS(@rootId,@ce_startdt9,@ce_enddt,'Colonoscopy')

		Union All

		Select
			EMPI
			,PROC_START_DATE as ServiceDate
			,PROC_CODE as Code
			,'Colonoscopy' as TestType
		From GetProcedures(@rootId,@ce_startdt9,@ce_enddt,'History of Colonoscopy')
		
		Union all
	
		select
			EMPI
			,DIAG_START_DATE as ServiceDate
			,DIAG_CODE as Code
			,'Colonoscopy' as TestType
		From GetDiagnosis(@rootId,@ce_startdt9,@ce_enddt,'History of Colonoscopy')
		
		Union All
	
		-- CT Colonograpgy

		Select
			EMPI
			,PROC_START_DATE as ServiceDate
			,PROC_CODE as Code
			,'Colonography' as TestType
		From PROCEDURES
		where 
			ROOT_COMPANIES_ID=@rootId and 
			ISNULL(PROC_STATUS,'EVN')!='INT' and 
			PROC_START_DATE between @ce_startdt4 and @ce_enddt and
			PROC_CODE in 
			(
				select code from HDS.VALUESET_TO_CODE where Value_Set_Name='CT Colonography'
			)
	
		Union All

		Select
			EMPI
			,ResultDate as ServiceDate
			,Coalesce(TestCode,ResultCode) as Code
			,'Colonography' as TestType
		FROM LAB
		Where
			ROOT_COMPANIES_ID=@rootId and
			ResultDate between @ce_startdt4 and @ce_enddt and
			(
				ResultCode in
				(
					select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('CT Colonography')
				)
				or
				TestCode in
				(
					select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('CT Colonography')
				)
			)
	
		Union all

	   -- FIT

	   	Select
			EMPI
			,PROC_START_DATE as ServiceDate
			,PROC_CODE as Code
			,'FIT' as testType
		From PROCEDURES
		where 
		ROOT_COMPANIES_ID=@rootId and 
		ISNULL(PROC_STATUS,'EVN')!='INT' and 
		PROC_START_DATE between @ce_startdt2 and @ce_enddt and
		PROC_CODE in 
		(
		select code from HDS.VALUESET_TO_CODE where Value_Set_Name='FIT DNA Lab Test'
		)
	   
		Union all

		select
			EMPI
			,DIAG_START_DATE as ServiceDate
			,DIAG_CODE as Code
			,'FIT' as testType
		From GetDiagnosis(@rootId,@ce_startdt2,@ce_enddt,'FIT DNA Test Result or Finding')
		
		Union All
	
	   	Select
			EMPI
			,ResultDate as ServiceDate
			,Coalesce(TestCode,ResultCode) as Code
			,'FIT' as testType
		FROM LAB
		Where
			ROOT_COMPANIES_ID=@rootId and
			ResultDate between @ce_startdt2 and @ce_enddt and
			(
				ResultCode in
				(
					select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('FIT DNA Lab Test','FIT DNA Test Result or Finding')
				)
				or
				TestCode in
				(
					select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('FIT DNA Lab Test','FIT DNA Test Result or Finding')
				)
			)

	
		)t1
	)t2


	
	Delete from KPI_ENGINE_MART.RPT.GapSupportingReport where ROOT_COMPANIES_ID=@rootId and Measure_id=@measure_id
	Insert into KPI_ENGINE_MART.RPT.GapSupportingReport(EMPI,LatestSatisfierCode,LatestSatisfierTest,LatestSatisfierTestDate,longestSatisfiercode,longestSatisfierTest,longestSatisfierTestdate,ROOT_COMPANIES_ID,ReportId,Measure_id)
	Select
		c.EMPI,
		c.Code as LatestSatisfierCode,
		c.TestType as LatestSatisfierTest,
		c.ServiceDate as LatestSatisfierTestDate,
		t.longestSatisfiedcode,
		t.longestSatisfiedTest,
		t.longestSatisfiedTestdate,
		@rootId,
		@reportId,
		@measure_id
	From #col_supportingreport c
	Join
	(
		select 
			EMPI,
			Code as longestSatisfiedcode,
			ServiceDate as longestSatisfiedTestdate,
			TestType as longestSatisfiedTest
		from #col_supportingreport c1
		WHere
			rn=(select max(rn) from #col_supportingreport c2 where c1.empi=c2.EMPI)
	)t on c.EMPI=t.EMPI 
	where
		rn=1

	


		SET @PERF_ROW = @@ROWCOUNT
		SET @PERF_DURATION = DATEDIFF(MINUTE,@PERF_START,GETDATE());
		EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, 'Enrollment Completed', @PERF_ROW, @DURATION_IN_MIN=@PERF_DURATION, @LOG2ID=@LOGID;
		EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, @LOG2ID=@LOGID, @END_FLAG=1;

END TRY
BEGIN CATCH
		BEGIN
			EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, 'FATAL ERROR', @LOG2ID=@LOGID;
			RAISERROR ('',16,0)
			RETURN 16
        END
	END CATCH
RETURN 0
END
		
GO
