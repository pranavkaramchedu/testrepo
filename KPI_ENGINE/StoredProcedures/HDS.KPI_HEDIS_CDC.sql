SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON


CREATE proc [HDS].[KPI_HEDIS_CDC]
AS

/****** Change Log: 2021-11-03 Changed by Sameer - ticket #KPISQL-502 ******/

BEGIN
DECLARE @INFO VARCHAR(8000)
					,	@TRUE BIT = 1
					,	@FALSE BIT = 0
					,	@DB_ID INT = DB_ID()
					,	@LOGID INT = 0
					,	@PROC VARCHAR(500)=OBJECT_SCHEMA_NAME(@@PROCID)+'.'+OBJECT_NAME(@@PROCID)
					,	@PERF_START DATETIME
					,	@PERF_DURATION INT
					,	@PERF_ROW INT
					,	@RC	INT
					;

		EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, @INFO=@INFO, @LOG2ID=@LOGID OUT;
		DECLARE @PROCFULLNAME VARCHAR(500) = DB_NAME()+'.'+@PROC;
		----EXEC dbo.SP_MI_UTIL_SEND_MAIL @PROCFULLNAME, 'STARTED', 1, @ECHO=0; 
	DECLARE @MSG VARCHAR(1000)
	DECLARE @PERF_START_PROC DATETIME 
	DECLARE @PERF_RPS NUMERIC(10,2) 
	DECLARE @ROWS INT 
	DECLARE @PERF_ROWS INT 
	SET @PERF_ROWS = 0 
	SET @PERF_START = GETDATE() 
	SET @PERF_START_PROC = GETDATE() 

BEGIN TRY

-- Declare Variables
Declare @rundate date=GetDate()

declare @meas_year varchar(4)=Year(Dateadd(month,-2,@rundate))
declare @rootId varchar(10)='159'


Declare @meas varchar(10)='CDC';
Declare @ce_startdt Date;
Declare @ce_rsltstartdt Date;
Declare @ce_rsltenddt Date;
Declare @ce_startdt1 Date;
Declare @ce_middt Date;
Declare @ce_enddt Date;
Declare @ce_enddt1 Date;

Declare @startDate Date;
Declare @enddate date;
Declare @quarter varchar(20);
Declare @measure_id INT;
Declare @target INT;
Declare @domain varchar(100);
Declare @subdomain varchar(100);
Declare @measuretype varchar(100);
Declare @measurename varchar(100);
Declare @reporttype varchar(100);
Declare @reportId INT;



-- Set Measure dates
SET @ce_startdt=concat(@meas_year,'-01-01');
SET @ce_rsltstartdt=concat(@meas_year-1,'-12-25');
SET @ce_rsltenddt=concat(@meas_year+1,'-01-07');
SET @ce_middt=concat(@meas_year,'-06-30');
SET @ce_enddt=concat(@meas_year,'-12-31');
SET @ce_startdt1=concat(@meas_year-1,'-01-01');
SET @ce_enddt1=concat(@meas_year-1,'-12-31');


Set @reporttype='Physician'
--Set @measurename='Breast Cancer Screening'
--Set @startDate=DATEADD(yy, DATEDIFF(yy, 0,Dateadd(month,-2,@rundate)), 0) 
--Set @enddate=eomonth(Dateadd(month,-2,@rundate))
--Set @quarter=Concat(Year(@enddate),' - Q',DATEPART(q, @enddate))
-- set @target=85
Set @domain='Clinical Quality / Chronic Condition Management'
Set @subdomain='Diabetes'
Set @measuretype='UHN'
-- Set @measure_id='22'





-- Eligible Patient List
drop table if exists #cdc_memlist; 
CREATE TABLE #cdc_memlist (
    EMPI varchar(100)
   
)
insert into #cdc_memlist
SELECT DISTINCT 
	en.EMPI 
FROM open_empi_master gm
join ENROLLMENT en on en.EMPI=gm.EMPI_ID and 
	  			      en.ROOT_COMPANIES_ID=gm.ROOT_COMPANIES_ID
WHERE 
	en.ROOT_COMPANIES_ID=@rootId and
	en.EFF_DATE<=@ce_enddt AND TERM_DATE>=@ce_startdt	AND 
	YEAR(@ce_enddt)-YEAR(Date_of_Birth) BETWEEN 18 AND 75 
ORDER BY 
	en.EMPI;


-- Create Temp Patient Enrollment
drop table if exists #cdc_tmpsubscriber;
CREATE TABLE #cdc_tmpsubscriber (
    EMPI varchar(100),
    dob date,
	age int,
	gender varchar(1),
	payer varchar(50),
	StartDate date,
	EndDate date,
)
insert into #cdc_tmpsubscriber
SELECT distinct 
	en.EMPI
	,gm.Date_of_Birth
	,Year(@ce_enddt)-Year(Date_of_Birth) as age
	,gm.Gender
	,en.PAYER_TYPE
	,en.EFF_DATE
	,en.TERM_DATE  
FROM Open_empi_master gm
join ENROLLMENT en on en.EMPI=gm.EMPI_ID and 
					  en.ROOT_COMPANIES_ID=gm.ROOT_COMPANIES_ID
WHERE 
	en.ROOT_COMPANIES_ID=@rootId and
	en.EFF_DATE<=@ce_enddt AND TERM_DATE>=@ce_startdt AND 
	YEAR(@ce_enddt)-YEAR(Date_of_Birth) BETWEEN 18 AND 75 
ORDER BY 
	en.EMPI
	,en.EFF_DATE
	,en.TERM_DATE;






-- Create cdcdataset
drop table if exists #cdcdataset;
CREATE TABLE #cdcdataset (
  EMPI varchar(100) NOT NULL,
  [meas] varchar(20) DEFAULT NULL,
  [payer] varchar(100) DEFAULT NULL,
  gender varchar(45) NOT NULL,
  [age] INT,
  [orec] smallint DEFAULT NULL,
  [lis] smallint DEFAULT '0',
  [rexcl] smallint DEFAULT '0',
  [rexcld] smallint DEFAULT '0',
  [CE] smallint DEFAULT '0',
  [excl] smallint DEFAULT '0',
  [num] smallint DEFAULT '0',
  [Event] smallint DEFAULT '0'
 
) 
Insert into #cdcdataset(EMPI,meas,payer,age,gender)
Select distinct
	EMPI
	,'CDC4'
	,pm.PayerMapping
	,age
	,gender
From
(
	Select distinct
		EMPI
		,gender
		,age
		,case 
				when (Payer in('HMO','CEP','POS','PPO') and nxtpayer in('MD','MLI','MRB'))  then TRIM(Payer)
				When (Payer in('HMO','CEP','POS','PPO') and prvpayer in('MD','MLI','MRB'))  then TRIM(Payer)
				When (Payer in('MD','MLI','MRB') and nxtpayer in('HMO','CEP','POS','PPO'))  then TRIM(nxtpayer)
				When (Payer in('MD','MLI','MRB') and prvpayer in('HMO','CEP','POS','PPO'))  then TRIM(prvpayer)
				when (Payer in('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP') and nxtpayer in('HMO','CEP','POS','PPO'))  then TRIM(Payer)
				When (Payer in('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP') and prvpayer in('HMO','CEP','POS','PPO'))  then TRIM(Payer)
				When (Payer in('HMO','CEP','POS','PPO') and nxtpayer in('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP'))  then TRIM(nxtpayer)
				When (Payer in('HMO','CEP','POS','PPO') and prvpayer in('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP'))  then TRIM(prvpayer)
				else Payer 
			end as newpayer
	From
	(
		select
			*
			,isnull(lead(Payer) over(partition by EMPI order by Payer),Payer) as nxtpayer
			,isnull(lag(Payer) over(partition by EMPI order by Payer),Payer) as prvpayer
		From 
		(
			select
				EMPI
				,payer
				,gender
				,age
			From
			(
				select 
					EMPI
					,StartDate
					,EndDate
					,TRIM(payer) as payer
					,gender
					,age
					,RANK() over(partition by EMPI order by StartDate desc,EndDate Desc) as rn 
				from #cdc_tmpsubscriber 
				where  
					StartDate<=@ce_enddt
					--and EMPI=100076
			)t1
			where 
				rn=1
		)t2
		
	)t3
)t4
Join HDS.HEDIS_PAYER_MAPPING pm on 
	t4.newpayer=pm.payer and 
	pm.Measure_id='CDC'
Order by 1



Insert into #cdcdataset(EMPI,meas,payer,gender,age)
select distinct
	EMPI
	,'CDC1' as meas
	,payer
	,gender
	,age
From #cdcdataset
where
	Payer not in('MEP','MMO','MOS','MPO')


Insert into #cdcdataset(EMPI,meas,payer,gender,age)
select distinct
	EMPI
	,'CDC2' as meas
	,payer
	,gender
	,age
From #cdcdataset
where
	Payer not in('MEP','MMO','MOS','MPO')

	

Insert into #cdcdataset(EMPI,meas,payer,gender,age)
select distinct
	EMPI
	,'CDC7' as meas
	,payer
	,gender
	,age
From #cdcdataset
where
	Payer in('MCR','MCS','MC','MP','MR')


Insert into #cdcdataset(EMPI,meas,payer,gender,age)
select distinct
	EMPI
	,'CDC9' as meas
	,payer
	,gender
	,age
From #cdcdataset
where
	Payer not in('MEP','MMO','MOS','MPO')


Insert into #cdcdataset(EMPI,meas,payer,gender,age)
select distinct
	EMPI
	,'CDC10' as meas
	,payer
	,gender
	,age
From #cdcdataset


	

-- Continuous Enrollment
				
	DROP TABLE IF EXISTS #cdc_contenroll;
	CREATE table #cdc_contenroll
	(
		EMPI varchar(100),
	)
	Insert into #cdc_contenroll
	Select
		EMPI
	From GetContinuousEnrolledEMPI(@rootId,@ce_startdt,@ce_enddt,45,0)
	
	
	update #cdcdataset set CE=1 from #cdcdataset ds join #cdc_contenroll ce on ds.EMPi=ce.EMPI;
	
	
	
	-- AdvancedIllness

	drop table if exists #cdc_advillness;
	CREATE table #cdc_advillness
	(
		EMPI varchar(100),
		servicedate Date,
		claimid varchar(100)
	)
	Insert into #cdc_advillness
	select
		EMPI
		,DIAG_START_DATE
		,CLAIM_ID 
	from advancedillness(@rootId,@ce_startdt1,@ce_enddt)
	Where
		DIAG_DATA_SRC not in(select DATA_SOURCE from DATA_SOURCE where supplemental=1)

	
	
	-- Required Exclusion

	drop table if exists #cdc_reqdexcl
	CREATE table #cdc_reqdexcl
	(
		EMPI varchar(100)
	)
	insert into #cdc_reqdexcl
	select distinct
		EMPI
	From palliativecare(@rootId,@ce_startdt,@ce_enddt)
	
	
	update #cdcdataset set rexcld=1 from #cdcdataset ds join #cdc_reqdexcl re on ds.EMPI=re.EMPI;

	-- select * from #cdc_reqdexcl where EMPI='1CAE2F97-BA81-41DA-A5D3-8A32C009C31A'

	
	-- Members with Institutinal SNP

	UPDATE ds SET 
		ds.rexcl=1 
	FROM #cdcdataset ds 
	JOIN ENROLLMENT s on 
		ds.EMPI=s.EMPI and 
		s.ROOT_COMPANIES_ID=@rootId 
	WHERE  ds.age>=66 AND 
	ds.payer IN('MCR','MCS','MP','MC','SN2','SN1','SN3','MR') AND 
	s.EFF_DATE<=@ce_enddt AND s.TERM_DATE>=@ce_startdt AND 
	s.PAYER_TYPE='SN2';


	-- LTI Exclusion
	
	drop table if exists #cdc_LTImembers;
	CREATE table #cdc_LTImembers
	(
		EMPI varchar(100)
				
	)
	Insert into #cdc_LTImembers
	Select distinct
		EMPI
	From LTImembers(@rootId,@ce_startdt,@ce_enddt)

	
	update #cdcdataset set rexcl=1 from #cdcdataset ds join #cdc_LTImembers re on ds.EMPI=re.EMPI where ds.age>=66 AND ds.payer IN('MCR','MCS','MP','MC','MMP','SN1','SN2','SN3','MR');

	

	-- Hospice Exclusion
	drop table if exists #cdc_hospicemembers;
	CREATE table #cdc_hospicemembers
	(
		EMPI varchar(50)
	)
	Insert into #cdc_hospicemembers
	select distinct
		EMPI
	From hospicemembers(@rootId,@ce_startdt,@ce_enddt)
	
	
	update #cdcdataset set rexcl=1 from #cdcdataset ds join #cdc_hospicemembers hos on hos.EMPI=ds.EMPI;
			
				
	-- Frailty Members LIST
	drop table if exists #cdc_frailtymembers;
	CREATE table #cdc_frailtymembers
	(
		EMPI varchar(100)
	)
	Insert into #cdc_frailtymembers
	Select distinct
		EMPI
	From Frailty(@rootId,@ce_startdt,@ce_enddt)
	Where
		DataSource not in(select DATA_SOURCE from DATA_SOURCE where supplemental=1)

	
	
-- Required Exclusion 1

	-- Inpatient Stay List
	drop table if exists #cdc_inpatientstaylist;
	CREATE table #cdc_inpatientstaylist
	(
		EMPI varchar(100),
		ServiceDate Date,
		claimid varchar(100),
		DATA_SRC varchar(50)

	)
	Insert into #cdc_inpatientstaylist
		select distinct 
		EMPI
		,FROM_DATE
		,CLAIM_ID 
		,CL_DATA_SRC
	from Inpatientstays(@rootId,@ce_startdt1,@ce_enddt)
	Where
		CL_DATA_SRC not in(select DATA_SOURCE from DATA_SOURCE where supplemental=1)

	
	
	-- Non acute Inpatient stay list
	drop table if exists #cdc_noncauteinpatientstaylist;
	CREATE table #cdc_noncauteinpatientstaylist
	(
		EMPI varchar(100),
		ServiceDate Date,
		claimid varchar(100),
		DATA_SRC varchar(50)
	)
	Insert into #cdc_noncauteinpatientstaylist
	select distinct 
		EMPI
		,FROM_DATE
		,CLAIM_ID 
		,CL_DATA_SRC
	from nonacutestays(@rootId,@ce_startdt1,@ce_enddt)
	Where
		CL_DATA_SRC not in(select DATA_SOURCE from DATA_SOURCE where supplemental=1)


	
	
	-- Outpatient and other visits
	drop table if exists #cdc_visitlist;
	CREATE table #cdc_visitlist
	(
		EMPI varchar(100),
		date_s date,
		claimid varchar(100),
		DATA_SRC varchar(50)
	)
	Insert into #cdc_visitlist
	Select distinct
		*
	From
	(
		
		Select
			EMPI
			,PROC_START_DATE
			,CLAIM_ID
			,PROC_DATA_SRC
		From GetProcedures(@rootId,@ce_startdt1,@ce_enddt,'Outpatient')

		Union all

		Select
			EMPI
			,PROC_START_DATE
			,CLAIM_ID
			,PROC_DATA_SRC
		From GetProcedures(@rootId,@ce_startdt1,@ce_enddt,'Observation')

		Union all

		Select
			EMPI
			,PROC_START_DATE
			,CLAIM_ID
			,PROC_DATA_SRC
		From GetProcedures(@rootId,@ce_startdt1,@ce_enddt,'ED')

		Union all

		Select
			EMPI
			,PROC_START_DATE
			,CLAIM_ID
			,PROC_DATA_SRC
		From GetProcedures(@rootId,@ce_startdt1,@ce_enddt,'Telephone Visits')

		Union all

		Select
			EMPI
			,PROC_START_DATE
			,CLAIM_ID
			,PROC_DATA_SRC
		From GetProcedures(@rootId,@ce_startdt1,@ce_enddt,'Online Assessments')

		Union all

		Select
			EMPI
			,PROC_START_DATE
			,CLAIM_ID
			,PROC_DATA_SRC
		From GetProcedures(@rootId,@ce_startdt1,@ce_enddt,'Nonacute Inpatient')

		Union all

		select 
			EMPI
			,FROM_DATE
			,CLAIM_ID 
			,CL_DATA_SRC
		from CLAIMLINE 
		where 
			ROOT_COMPANIES_ID=@rootId and 
			ISNULL(POS,'')!='81' and 
			CL_DATA_SRC not in(select DATA_SOURCE from DATA_SOURCE where supplemental=1) and
			FROM_DATE between @ce_startdt1 and @ce_enddt and
			REV_CODE in
			(
				select code from HDS.VALUESET_TO_CODE where code_system='UBREV' and  Value_Set_Name in('Outpatient','ED')
			)
	)t1

	
	
	-- Required exclusion table
	drop table if exists #cdc_reqdexcl1;
	CREATE table #cdc_reqdexcl1
	(
		EMPI varchar(100)
	)
	Insert into #cdc_reqdexcl1
	select distinct 
		t3.EMPI 
	from
	(
		select 
			t2.EMPI 
		from
		(
			select distinct 
				t1.EMPI
				,t1.date_s 
			from
			(
				select 
					EMPI
					,date_s
					,claimid  
				from #cdc_visitlist 
			
				union all
			
				select 
					na.EMPI
					,na.ServiceDate
					,na.claimid 
				from #cdc_noncauteinpatientstaylist na
				join #cdc_inpatientstaylist inp on 
					na.EMPI=inp.EMPI and 
					na.claimid=inp.claimid and
					na.DATA_SRC=inp.DATA_SRC
			)t1
			Join #cdc_advillness a on a.EMPI=t1.EMPI and 
									  a.claimid=t1.claimid
		)t2 
		group by 
			t2.EMPI 
		having 
			count(t2.EMPI)>1
	)t3 
	Join #cdc_frailtymembers f on f.EMPI=t3.EMPI


	update #cdcdataset set rexcl=1 from #cdcdataset ds
	join #cdc_reqdexcl1 re1 on re1.EMPI=ds.EMPI and ds.age BETWEEN 66 AND 80;

-- Required Exclusion 2

	-- Acute Inpatient with Advanced Illness
	drop table if exists #cdc_reqdexcl2;
	CREATE table #cdc_reqdexcl2
	(
		EMPI varchar(100)
				
	)
	insert into #cdc_reqdexcl2
	select distinct 
		t2.EMPI 
	from
	(
		select 
			t1.EMPI 
		from 
		(
			Select distinct
				EMPI
				,PROC_START_DATE
				,CLAIM_ID
			From GetProcedures(@rootId,@ce_startdt1,@ce_enddt,'Acute Inpatient')
			Where
				PROC_DATA_SRC not in(select DATA_SOURCE from DATA_SOURCE where Supplemental=1)
		)t1
		Join #cdc_advillness a on a.EMPI=t1.EMPI and 
								  a.claimid=t1.CLAIM_ID
	)t2
	join #cdc_frailtymembers f on f.EMPI=t2.EMPI

	
	
	update #cdcdataset set rexcl=1 from #cdcdataset ds
	join #cdc_reqdexcl2 re2 on re2.EMPI=ds.EMPI and ds.age BETWEEN 66 AND 80;
	
			
	-- Required exclusion 3
	drop table if exists #cdc_reqdexcl3;
	CREATE table #cdc_reqdexcl3
	(
		EMPI varchar(100)
	)
	insert into #cdc_reqdexcl3
	select distinct 
		t2.EMPI 
	from
	(
		select 
			t1.EMPI
			,t1.ServiceDate
			,t1.claimid 
		from
		(
			select 
				inp.EMPI
				,inp.ServiceDate
				,inp.claimid 
			from #cdc_inpatientstaylist inp
			left outer join #cdc_noncauteinpatientstaylist na on inp.EMPI=na.EMPI and 
																 inp.claimid=na.claimid
			where 
				na.EMPI is null
		)t1
		join #cdc_advillness a on a.EMPI=t1.EMPI and 
								  a.claimid=t1.claimid
	)t2
	join #cdc_frailtymembers f on f.EMPI=t2.EMPI

	
	
	update #cdcdataset set rexcl=1 from #cdcdataset ds
	join #cdc_reqdexcl3 re3 on re3.EMPI=ds.EMPI and ds.age BETWEEN 66 AND 80;

		
-- RequiredExcl 4
	drop table if exists #cdc_reqdexcl4;
	CREATE table #cdc_reqdexcl4
	(
		EMPI varchar(100)
	)
	insert into #cdc_reqdexcl4
	select 
		t1.EMPI 
	from
	(
		select distinct
			EMPI
		from MEDICATION 
		where
			ROOT_COMPANIES_ID=@rootId  and 
			MED_DATA_SRC in(select DATA_SOURCE from DATA_SOURCE where supplemental=0) and 
			FILL_DATE between @ce_startdt1 and @ce_enddt and
			MEDICATION_CODE in
			(
				select code from HDS.MEDICATION_LIST_TO_CODES where Medication_List_Name='Dementia Medications'
			)
	)t1
	Join #cdc_frailtymembers f on f.EMPI=t1.EMPI;

	
	
	update #cdcdataset set rexcl=1 from #cdcdataset ds
	join #cdc_reqdexcl4 re4 on re4.EMPI=ds.EMPI and ds.age BETWEEN 66 AND 80;
	
	
-- Start of Optional Exclusion
-- Patients with Indueced Diabetes
	drop table if exists #cdc_induceddiabetesmemlist;
	CREATE table #cdc_induceddiabetesmemlist
	(
		EMPI varchar(100)
				
	)
	Insert into #cdc_induceddiabetesmemlist
	Select
		EMPI
	From GetDiagnosis(@rootId,@ce_startdt1,@ce_enddt,'Diabetes Exclusions')
	

	
	-- Memebers with no diabetes
	drop table if exists #cdc_nondiabeticmemlist;
	CREATE table #cdc_nondiabeticmemlist
	(
		EMPI varchar(100)
				
	)
	Insert into #cdc_nondiabeticmemlist
	select distinct
		EMPI 
	from #cdc_memlist 
	where 
		EMPI not in
		(
			Select
				EMPI
			From GetDiagnosis(@rootId,@ce_startdt1,@ce_enddt,'Diabetes')
	
		)
			
		

	
		
-- Optional Exclusion
	drop table if exists #cdc_optexcl;
	CREATE table #cdc_optexcl
	(
		EMPI varchar(100)
				
	)
	Insert into #cdc_optexcl
	select 
		t1.EMPI 
	from #cdc_nondiabeticmemlist t1
	join #cdc_induceddiabetesmemlist t2 on t1.EMPI=t2.EMPI
	
	
	
	
	update #cdcdataset set excl=1 from #cdcdataset ds
	join #cdc_optexcl oe on oe.EMPI=ds.EMPI;

-- End of optional exclusion	
	
	
		
	-- Event

	-- Create list of patients with Diabetes
	
	drop table if exists #cdc_diabeticmemlist;
	CREATE table #cdc_diabeticmemlist
	(
		EMPI varchar(100),
		servicedate Date,
		CLAIM_ID varchar(100),
		DATA_SRC varchar(50)
				
	)
	Insert into #cdc_diabeticmemlist
	Select
		EMPI
		,DIAG_START_DATE
		,CLAIM_ID
		,DIAG_DATA_SRC
	From GetDiagnosis(@rootId,@ce_startdt1,@ce_enddt,'Diabetes')
	Where
		DIAG_DATA_SRC not in(Select DATA_SOURCE from DATA_SOURCE where Supplemental=1)

-- select * from #cdc_diabeticmemlist where EMPI=150794
	
	
	-- Acute Inpatient list
	drop table if exists #cdc_acuteipmemlist;
	CREATE table #cdc_acuteipmemlist
	(
		EMPI varchar(100),
		servicedate Date,
		CLAIM_ID varchar(100),
		DATA_SRC varchar(50)
	)
	Insert into #cdc_acuteipmemlist
	Select
		EMPI
		,PROC_START_DATE
		,CLAIM_ID
		,PROC_DATA_SRC
	From [GetProceduresWithOutMods](@rootId,@ce_startdt1,@ce_enddt,'Acute Inpatient','Telehealth Modifier')
	Where
		PROC_DATA_SRC not in(Select DATA_SOURCE from DATA_SOURCE where Supplemental=1)

	
	--Identify visits with Telehelath POS
	Drop Table if exists #cdc_telePOSvisits
	Create Table #cdc_telePOSvisits
	(
		EMPI varchar(100),
		CLAIM_ID varchar(100),
		DATA_SRC varchar(50)
	)
	Insert into #cdc_telePOSvisits
	Select distinct
		EMPI
		,CLAIM_ID
		,CL_DATA_SRC
	From Claimline
	where
		ROOT_COMPANIES_ID=@rootId and
		FROM_DATE between @ce_startdt1 and @ce_enddt and
		ISNULL(POS,'0') in
		(
			Select code from HDS.valueset_to_code where Value_Set_name='Telehealth POS'
		)
	


	--	Acute Inpatient visits with diabetes diagnosis and without telehealth POS and modifiers
	drop table if exists #cdc_eventlist1;				
	CREATE table #cdc_eventlist1
	(
		EMPI varchar(100),
	)		
	Insert into #cdc_eventlist1
	select 
		d.EMPI 
	from #cdc_diabeticmemlist d
	join #cdc_acuteipmemlist acip on 
		d.EMPI=acip.EMPI and
		d.CLAIM_ID=acip.CLAIM_ID and
		d.DATA_SRC=acip.DATA_SRC
	left outer join #cdc_telePOSvisits tv on
		acip.EMPI=tv.EMPI and
		acip.CLAIM_ID=tv.CLAIM_ID and
		acip.DATA_SRC=tv.DATA_SRC
	Where
		tv.EMPI is null

	
	
	
	
	
	-- Event List 2
	-- Acute inpatient discharge with diabetes
	
	drop table if exists #cdc_eventlist2;				
	CREATE table #cdc_eventlist2
	(
		EMPI varchar(100)
	)	
	Insert into #cdc_eventlist2
	select 
		t1.EMPI 
	from
	(
		select 
			inp.EMPI
			,inp.ServiceDate
			,inp.claimid 
		from #cdc_inpatientstaylist inp
		left outer join #cdc_noncauteinpatientstaylist na on 
			inp.EMPI=na.EMPI and 
			inp.claimid=na.claimid and
			inp.DATA_SRC=na.DATA_SRC
		where 
			na.EMPI is null
	)t1
	join #cdc_diabeticmemlist d on d.EMPI=t1.EMPI and 
								   d.CLAIM_ID=t1.claimid
	
	
	
	
	
	
	-- EventList 3
	-- Outpatient,ED,Observation,Telephone,Online,Non acute encounters and discharges
	
	drop table if exists #cdc_eventvisitlist;
	CREATE table #cdc_eventvisitlist
	(
		EMPI varchar(100),
		date_s Date,
		claimid varchar(100),
		DATA_SRC varchar(50)
	)
	Insert into #cdc_eventvisitlist
		Select Distinct
		EMPI
		,ServiceDate
		,CLAIM_ID
		,PROC_DATA_SRC
	From
	(
		select  
			p.EMPI
			,PROC_START_DATE as ServiceDate
			,p.CLAIM_ID 
			,p.PROC_DATA_SRC
		from Procedures p
		left outer join CLAIMLINE c on p.CLAIM_ID=c.CLAIM_ID and
									   ISNULL(p.SV_LINE,0)=ISNULL(c.SV_LINE,0) and
									   p.PROC_DATA_SRC=c.CL_DATA_SRC and
									   p.ROOT_COMPANIES_ID=c.ROOT_COMPANIES_ID and
									   p.EMPI=c.EMPI
		where 
			p.ROOT_COMPANIES_ID=@rootId and
			PROC_DATA_SRC not in(Select data_source from Data_source where supplemental=1) and
			ISNULL(PROC_STATUS,'EVN')!='INT' and
			ISNULL(c.POS,'0')!='81' and 
			PROC_START_DATE between @ce_startdt1 and @ce_enddt and
			PROC_CODE in
			(
				select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments')
			)

		Union all

		select  
			EMPI
			,FROM_DATE as ServiceDate
			,CLAIM_ID 
			,CL_DATA_SRC
		from CLAIMLINE
		where 
			ROOT_COMPANIES_ID=@rootId and 
			ISNULL(POS,'0')!='81' and 
			CL_DATA_SRC not in(Select data_source from Data_source where supplemental=1) and
			FROM_DATE between @ce_startdt1 and @ce_enddt and
			REV_CODE in
			(
				select code from HDS.VALUESET_TO_CODE where code_system='UBREV' and Value_Set_Name in('Outpatient','ED')
			)

		Union All

		Select distinct
			t1.EMPI
			,PROC_START_DATE
			,t1.CLAIM_ID
			,PROC_DATA_SRC
		From [GetProceduresWithOutMods](@rootId,@ce_startdt1,@ce_enddt,'Nonacute Inpatient','Telehealth Modifier') t1
		Left outer join #cdc_telePOSvisits tv on
			t1.EMPI=tv.EMPI and
			t1.CLAIM_ID=tv.CLAIM_ID and
			t1.PROC_DATA_SRC=tv.DATA_SRC
		Where
			PROC_DATA_SRC not in(Select data_source from Data_source where supplemental=1) and
			tv.EMPI is null
			--and t1.EMPI=153594
		
	)t1

	
	-- Combining visits to check for count
	drop table if exists #cdc_eventlist3;
	CREATE table #cdc_eventlist3
	(
		EMPI varchar(100)
	)
	Insert into #cdc_eventlist3
	select distinct 
		t3.EMPI 
	from
	(
		select 
			t2.EMPI 
		from
		(
			select distinct 
				t1.EMPI
				,t1.date_s 
			from
			(
				select 
					EMPI
					,date_s
					,claimid  
					,Data_SRC
				from #cdc_eventvisitlist
			--	where  EMPI=195004

				union all
	
				select 
					na.EMPI
					,na.ServiceDate
					,na.claimid 
					,na.DATA_SRC
				from #cdc_noncauteinpatientstaylist na
				join #cdc_inpatientstaylist inp on 
					na.EMPI=inp.EMPI and 
					na.claimid=inp.claimid and
					na.DATA_SRC=inp.DATA_SRC
					--and na.EMPi=195004

			
			)t1
			Join #cdc_diabeticmemlist d on 
				d.EMPI=t1.EMPI and 
				d.CLAIM_ID=t1.claimid and
				d.DATA_SRC=t1.DATA_SRC
				
		)t2 
		group by 
			t2.EMPI 
		having 
			count(t2.EMPI)>1
	)t3

	
	
	
	-- Event List 4
	-- Members with insulin or hypoglycemics/ antihyperglycemics on an ambulatory basis

	drop table if exists #cdc_eventlist4;
	CREATE table #cdc_eventlist4
	(
		EMPI varchar(100)
	)
	insert into #cdc_eventlist4
	select distinct 
		EMPI 
	from MEDICATION 
	where 
		ROOT_COMPANIES_ID=@rootId  and 
		MED_DATA_SRC not in(Select data_source from Data_source where supplemental=1) and
		FILL_DATE between @ce_startdt1 and @ce_enddt and 
		MEDICATION_CODE in
		(
			select code from HDS.MEDICATION_LIST_TO_CODES where Medication_List_Name='Diabetes Medications'
		)
	
	
	
	update #cdcdataset set event=1 from #cdcdataset ds	join #cdc_eventlist4 el4 on el4.EMPI=ds.EMPI ;
	update #cdcdataset set event=1 from #cdcdataset ds join #cdc_eventlist3 el3 on ds.EMPI=el3.EMPI;
	update #cdcdataset set event=1 from #cdcdataset ds join #cdc_eventlist2 el2 on ds.EMPI=el2.EMPI;
	update #cdcdataset set event=1 from #cdcdataset ds join #cdc_eventlist1 el1 on ds.EMPI=el1.EMPI;
	
	

	
	-- Numerator 

	-- HbA1c Testing
	drop table if exists #cdc_hba1c;
	CREATE table #cdc_hba1c
	(
		EMPI varchar(100),
		servicedate Date,
		Code varchar(20),
		Bill_Prov varchar(20),
		BILL_PROV_TIN varchar(20),
				
	)
	insert into #cdc_hba1c
	select distinct 
		EMPI
		,servicedate
		,Code
		,BILL_Prov
		,BILL_PROV_TIN
	from
	(
		select 
			p.EMPI
			,PROC_START_DATE as servicedate 
			,p.PROC_CODE as Code
			,c.BILL_PROV
			,c.BILL_PROV_TIN
		from PROCEDURES p
		Left Outer Join CLAIMLINE c on
			p.EMPI=c.EMPI and
			p.CLAIM_ID=c.CLAIM_ID and
			ISNULL(p.SV_LINE,0)=ISNULL(c.SV_LINE,0) and
			p.ROOT_COMPANIES_ID=c.ROOT_COMPANIES_ID and
			p.PROC_DATA_SRC=c.CL_DATA_SRC
		where 
			p.ROOT_COMPANIES_ID=@rootId and
			ISNULL(c.POS,'0')!='81' and
			ISNULL(PROC_STATUS,'EVN')!='INT' and 
			PROC_START_DATE between @ce_startdt and @ce_enddt and 			
			PROC_CODE IN
			(
				select code from HDS.VALUESET_TO_CODE where Code_System='CPT-CAT-II' and Value_Set_Name in('HbA1c Test Result or Finding','HbA1c Lab Test')
			) 
			and
			ISNULL(MOD_1,'') not in
			(
				select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('CPT CAT II Modifier')
			)
			and
			ISNULL(MOD_2,'') not in
			(
				select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('CPT CAT II Modifier')
			)
			and
			ISNULL(MOD_3,'') not in
			(
				select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('CPT CAT II Modifier')
			)
			and
			ISNULL(MOD_4,'') not in
			(
				select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('CPT CAT II Modifier')
			)
			--and EMPI=199142
		
		Union all

		select 
			p.EMPI
			,PROC_START_DATE as servicedate 
			,p.PROC_CODE as Code
			,c.BILL_PROV
			,c.BILL_PROV_TIN
		from PROCEDURES p
		Left Outer Join CLAIMLINE c on
			p.EMPI=c.EMPI and
			p.CLAIM_ID=c.CLAIM_ID and
			ISNULL(p.SV_LINE,0)=ISNULL(c.SV_LINE,0) and
			p.ROOT_COMPANIES_ID=c.ROOT_COMPANIES_ID and
			p.PROC_DATA_SRC=c.CL_DATA_SRC
		where 
			p.ROOT_COMPANIES_ID=@rootId and
			ISNULL(PROC_STATUS,'EVN')!='INT' and 
			PROC_START_DATE between @ce_startdt and @ce_enddt and 			
			PROC_CODE IN
			(
				select code from HDS.VALUESET_TO_CODE where Code_System!='CPT-CAT-II' and Value_Set_Name in('HbA1c Test Result or Finding','HbA1c Lab Test')
			) 
			

		Union all
	
		select 
			EMPI
			,ResultDate as servicedate
			,ResultCode as Code
			,null
			,null
		from LAB 
		where 
			ROOT_COMPANIES_ID=@rootId and 
			ResultDate between @ce_startdt and @ce_enddt and 
			(
				ResultCode in
				(
					select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('HbA1c Test Result or Finding','HbA1c Lab Test')
				)
				or
				TestCode in
				(
					select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('HbA1c Test Result or Finding','HbA1c Lab Test')
				)
			)
			--and EMPI=199142

		Union all

		select 
			p.EMPI
			,DIAG_START_DATE as servicedate 
			,p.DIAG_CODE as Code
			,null
			,null
		from Diagnosis p
		where 
			p.ROOT_COMPANIES_ID=@rootId and
			DIAG_START_DATE between @ce_startdt and @ce_enddt and 			
			DIAG_CODE IN
			(
				select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('HbA1c Test Result or Finding','HbA1c Lab Test')
			) 
			


	)t1
	
	
	update #cdcdataset set num=1 from #cdcdataset ds join #cdc_hba1c n1 on ds.EMPI=n1.EMPI and ds.meas='CDC1';

	
	
	-- Create CDC1 Numerator Details
	drop table if exists #cdc_numdetails_cdc1;
	CREATE table #cdc_numdetails_cdc1
	(
		EMPI varchar(100),
		servicedate Date,
		Code varchar(20)
				
	)
	Insert into #cdc_numdetails_cdc1
	Select
		EMPI
		,ServiceDate
		,Code
	From
	(
		select 
			*
			,ROW_NUMBER() over(partition by EMPI order by ServiceDate Desc) as rn 
		from #cdc_hba1c
	)t1
	Where 
		rn=1


	-- Numerator 3 - HbA1c Control <8%
	-- Identify visits with HBa1c<8
/*	
-- Identify All HbA1c Visits during the measurement year

drop table if exists #cdc_hba1cvst;
CREATE table #cdc_hba1cvst
(
	EMPI varchar(100),
	servicedate Date
)
Insert into #cdc_hba1cvst
select distinct 
	EMPI
	,servicedate 
from
(
	select 
		p.EMPI
		,PROC_START_DATE as servicedate 
	from PROCEDURES p
	Left outer Join CLAIMLINE c on p.CLAIM_ID=c.CLAIM_ID and
								   p.SV_LINE=c.SV_LINE and
								   p.PROC_DATA_SRC=c.CL_DATA_SRC and
								   p.ROOT_COMPANIES_ID=c.ROOT_COMPANIES_ID

	where 
		p.ROOT_COMPANIES_ID=@rootId and 
		ISNULL(c.POS,'')!='81' and
		PROC_START_DATE between @ce_startdt and @ce_enddt and 
		PROC_CODE in	
		(
			select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('HbA1c Test Result or Finding','HbA1c Lab Test')
		)
		and
		ISNULL(MOD_1,'') not in
		(
			select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('CPT CAT II Modifier')
		)
		and
		ISNULL(MOD_2,'') not in
		(
			select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('CPT CAT II Modifier')
		)
		and
		ISNULL(MOD_3,'') not in
		(
			select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('CPT CAT II Modifier')
		)
		and
		ISNULL(MOD_4,'') not in
		(
			select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('CPT CAT II Modifier')
		)

	union all

	select 
		EMPI
		,ResultDate as servicedate 
	from Lab 
	where 
		ROOT_COMPANIES_ID=@rootId and 
		ResultDate between @ce_startdt and @ce_enddt and 
		(
			ResultCode in
			(
				select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('HbA1c Test Result or Finding','HbA1c Lab Test')
			)
			or
			TestCode in
			(
				select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('HbA1c Test Result or Finding','HbA1c Lab Test')
			)
		)
	
)t1


*/

-- Identify the  most recent HbA1c Visit
drop table if exists #cdc_hba1cmostrecentvisit;
CREATE table #cdc_hba1cmostrecentvisit
(
	EMPI varchar(100),
	Code varchar(20),
	servicedate Date,
	BILL_PROV varchar(20),
	BILL_PROV_TIN varchar(20)
)
insert into #cdc_hba1cmostrecentvisit
Select
	EMPI
	,Code
	,ServiceDate
	,Bill_Prov
	,BILL_PROV_TIN
From
(
	Select
		*
		,ROW_NUMBER() over(partition by EMPI order by serviceDate Desc) as rn
	From #cdc_hba1c
)t1	
Where
	rn=1

	--select * from #cdc_hba1c
	--select * from #cdc_hba1cmostrecentvisit
	-- select * from #cdc_hba1crslt

	


-- Identify the HbA1c Results during measurement year with 1 week buffer
drop table if exists #cdc_hba1crslt;
CREATE table #cdc_hba1crslt
(
	EMPI varchar(100),
	servicedate Date,
	Code varchar(20),
	value varchar(50)
)
Insert into #cdc_hba1crslt
select distinct 
	EMPI
	,servicedate
	,Code
	,value 
from
(
	select 
		EMPI
		,ResultDate as servicedate
		,ResultCode as Code
		,value 
	from LAB 
	where 
		ROOT_COMPANIES_ID=@rootId and 
		ResultDate between @ce_rsltstartdt and @ce_rsltenddt and
		(
			ResultCode in
			(
				select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('HbA1c Test Result or Finding','HbA1c Lab Test')
			)
			or
			TestCode in
			(
				select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('HbA1c Test Result or Finding','HbA1c Lab Test')
			)
		)
		and 
		ResultCode!='27353-2'
		and ISNULL(Value,'')!=''
		--and EMPI=176992

	union all

	select 
		p.EMPI
		,PROC_START_DATE as servicedate
		,PROC_CODE as Code
		,'7.00' as value 
	from PROCEDURES p
	Left Outer Join CLAIMLINE c on p.CLAIM_ID=c.CLAIM_ID and
								   ISNULL(p.SV_LINE,0)=ISNULL(c.SV_LINE,0) and
								   p.PROC_DATA_SRC=c.CL_DATA_SRC and
								   p.ROOT_COMPANIES_ID=c.ROOT_COMPANIES_ID and
								   p.EMPI=c.EMPI
	where 
		p.ROOT_COMPANIES_ID=@rootId and 
		ISNULL(PROC_STATUS,'EVN')!='INT' and
		PROC_START_DATE between @ce_rsltstartdt and @ce_rsltenddt and 
		ISNULL(c.POS,'')!='81' and 
		ISNULL(MOD_1,'') not in
		(
			select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('CPT CAT II Modifier')
		)
		and
		ISNULL(MOD_2,'') not in
		(
			select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('CPT CAT II Modifier')
		)
		and
		ISNULL(MOD_3,'') not in
		(
			select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('CPT CAT II Modifier')
		)
		and
		ISNULL(MOD_4,'') not in
		(
			select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('CPT CAT II Modifier')
		)
		and 
		PROC_CODE in
		(
			select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('HbA1c Level Less Than 7.0','HbA1c Level Greater Than or Equal To 7.0 and Less Than 8.0')
		)

	Union all

	select 
		p.EMPI
		,PROC_START_DATE as servicedate
		,PROC_CODE as Code
		,'9.50' as value 
	from PROCEDURES p
	Left Outer Join CLAIMLINE c on p.CLAIM_ID=c.CLAIM_ID and
								   ISNULL(p.SV_LINE,0)=ISNULL(c.SV_LINE,0) and
								   p.PROC_DATA_SRC=c.CL_DATA_SRC and
								   p.ROOT_COMPANIES_ID=c.ROOT_COMPANIES_ID and
								   p.EMPI=c.EMPi
	where 
		p.ROOT_COMPANIES_ID=@rootId and 
		ISNULL(PROC_STATUS,'EVN')!='INT' and
		PROC_START_DATE between @ce_rsltstartdt and @ce_rsltenddt and 
		ISNULL(c.POS,'')!='81' and 
		ISNULL(MOD_1,'') not in
		(
			select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('CPT CAT II Modifier')
		)
		and
		ISNULL(MOD_2,'') not in
		(
			select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('CPT CAT II Modifier')
		)
		and
		ISNULL(MOD_3,'') not in
		(
			select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('CPT CAT II Modifier')
		)
		and
		ISNULL(MOD_4,'') not in
		(
			select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('CPT CAT II Modifier')
		)
		and 
		PROC_CODE in
		(
			select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('HbA1c Level Greater Than 9.0')
		)

	Union all

	select 
		p.EMPI
		,PROC_START_DATE as servicedate
		,PROC_CODE as Code
		,'8.50' as value
	from PROCEDURES p
	Left Outer Join CLAIMLINE c on p.CLAIM_ID=c.CLAIM_ID and
								   ISNULL(p.SV_LINE,0)=ISNULL(c.SV_LINE,0) and
								   p.PROC_DATA_SRC=c.CL_DATA_SRC and
								   p.ROOT_COMPANIES_ID=c.ROOT_COMPANIES_ID and 
								   p.EMPI=c.EMPI
	where 
		p.ROOT_COMPANIES_ID=@rootId and 
		ISNULL(PROC_STATUS,'EVN')!='INT' and
		PROC_START_DATE between @ce_rsltstartdt and @ce_rsltenddt and 
		ISNULL(c.POS,'0')!='81' and 
		ISNULL(MOD_1,'') not in
		(
			select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('CPT CAT II Modifier')
		)
		and
		ISNULL(MOD_2,'') not in
		(
			select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('CPT CAT II Modifier')
		)
		and
		ISNULL(MOD_3,'') not in
		(
			select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('CPT CAT II Modifier')
		)
		and
		ISNULL(MOD_4,'') not in
		(
			select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('CPT CAT II Modifier')
		)
		and 
		PROC_CODE in
		(
			select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('HbA1c Level Greater Than or Equal To 8.0 and Less Than or Equal To 9.0')
		)

	Union all

	select 
		p.EMPI
		,DIAG_START_DATE as servicedate
		,DIAG_CODE as Code
		,'6.9' as value
	from Diagnosis p
	where 
		p.ROOT_COMPANIES_ID=@rootId and 
		DIAG_START_DATE between @ce_rsltstartdt and @ce_rsltenddt and 
		DIAG_CODE in
		(
			select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('HbA1c Level Less Than 7.0')
		)

	Union all

	select 
		p.EMPI
		,DIAG_START_DATE as servicedate
		,DIAG_CODE as Code
		,'9.1' as value
	from Diagnosis p
	where 
		p.ROOT_COMPANIES_ID=@rootId and 
		DIAG_START_DATE between @ce_rsltstartdt and @ce_rsltenddt and 
		DIAG_CODE in
		(
			select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('HbA1c Level Greater Than 9.0')
		)
		
)t1
	


-- Create Numerator Compliant list

drop table if exists #cdc_num3list;
CREATE table #cdc_num3list
(
	EMPI varchar(100),
	ServiceDate Date,
	Code varchar(20)
						
)	
insert into #cdc_num3list
-- Members who had HbA1c result<=8 with 7 days of HbA1c Test
Select distinct
	EMPI
	,ServiceDate
	,Code
From
(
	select 
		t1.EMPI
		,t1.servicedate
		,t1.Code
		,t1.value
	from
	(
		select 
			rv.EMPI
			,rs.servicedate
			,rs.Code
			,replace(replace(rs.value,'>',''),'<','') as value 
		from #cdc_hba1cmostrecentvisit rv
		join #cdc_hba1crslt rs on rv.EMPI=rs.EMPI
		where 
			DATEDIFF(DAY,rs.servicedate,rv.servicedate )<=7 and 
			DATEDIFF(DAY,rs.servicedate,rv.servicedate )>=-7 and
			ISNUMERIC(replace(replace(rs.value,'>',''),'<',''))=1

	)t1 
	where 
		t1.servicedate = 
		(
			select 
				max(t2.servicedate) 
			from
			(
				select 
					rv.EMPI
					,rs.servicedate
					,rs.Code
					,replace(replace(rs.value,'>',''),'<','') as value 
				from #cdc_hba1cmostrecentvisit rv
				join #cdc_hba1crslt rs on rv.EMPI=rs.EMPI
				where 
					DATEDIFF(DAY,rs.servicedate,rv.servicedate )<=7 and 
					DATEDIFF(DAY,rs.servicedate,rv.servicedate )>=-7 and 
					rs.value!='' and
					ISNUMERIC(replace(replace(rs.value,'>',''),'<',''))=1
			 )t2 
			 where t1.EMPI=t2.EMPI
		)
	)t3
	where 
	value <cast(8 as float) 
	and value!=''
 
 
update #cdcdataset set num=1 from #cdcdataset ds join #cdc_num3list n3 on ds.EMPI=n3.EMPI and ds.meas='CDC10';

	

-- Create Numerator Details for CDC10
drop table if exists #cdc_numdetails_cdc10;
CREATE table #cdc_numdetails_cdc10
(
	EMPI varchar(100),
	ServiceDate Date,
	Code varchar(20)
						
)	
Insert into #cdc_numdetails_cdc10
Select
	EMPI
	,ServiceDate
	,Code
From
(
	select 
		*
		,ROW_NUMBER() over(partition by EMPI order by ServiceDate Desc) as rn 
	from #cdc_num3list
)t1
where
	rn=1



-- CDC 2 HbA1c Poor Control

drop table if exists #cdc_num2list;
CREATE table #cdc_num2list
(
	EMPI varchar(100),
	servicedate date,
	code varchar(20)

	
)	
insert into #cdc_num2list
select distinct 
	EMPI
	,servicedate
	,Code
from
(
-- Identify Members who had Hba1c with a reading of >9 or no reading within 7 days
	select 
		EMPI
		,servicedate
		,Code
		
	from
	(
		select 
			rv.EMPI
			,rs.servicedate
			,rs.Code
			,replace(replace(rs.value,'>',''),'<','') as value 
			,ROW_NUMBER() OVER (PARTITION BY rv.EMPI ORDER BY value DESC) AS Recency 
		from #cdc_hba1cmostrecentvisit rv
		join #cdc_hba1crslt rs on rv.EMPI=rs.EMPI
		where  
			DATEDIFF(DAY,rs.servicedate,rv.servicedate ) between -7 and 7 and
			ISNUMERIC(replace(replace(rs.value,'>',''),'<',''))=1
			--and rv.EMPI=107137
	)t3
	where 
		(
			value>cast(9 as float) 
			or 
			value=''
		) 
		and 
		recency=1 
		--and EMPI=130270

	Union all
	
	-- Identify members who had HbA1c test but had not result within 7 days
	select distinct 
		rv.EMPI
		,rv.servicedate
		,'No HbA1c Result'
	from #cdc_hba1cmostrecentvisit rv
	join #cdc_hba1crslt rs on rv.EMPI=rs.EMPI
	where 
		 DATEDIFF(DAY,rs.servicedate,rv.servicedate ) not between -7 and 7
		 and 
		 rv.EMPI not in 
		 (
			select 
				rv.EMPI 
			from #cdc_hba1cmostrecentvisit rv
			join #cdc_hba1crslt rs on rv.EMPI=rs.EMPI
			where  
				DATEDIFF(DAY,rs.servicedate,rv.servicedate ) between -7 and 7
		 )
		-- and rv.EMPI=107137

	 Union all
	-- Identify members who had no HbA1c test during measurement year
	select 
		m.EMPI
		,'1900-01-01' as servicedate
		,'No HbA1c' as Code
	from #cdc_memlist m 
	where 
		m.EMPI not in
		(
			select EMPI from #cdc_hba1c
		)	
	--and EMPi=130270

	union all
-- Identify Members who had HbA1c test but no result was ever recorded
	select distinct 
		v.EMPI 
		,r.servicedate
		,r.Code
	from #cdc_hba1cmostrecentvisit v
	left outer join #cdc_hba1crslt r on 
		v.EMPI=r.EMPI
	where
		r.EMPI is null
	


	

 )t1




 -- commenting the hedis logic as UHN does not want to apply CE logic
-- update #cdcdataset set num=1 from #cdcdataset ds join #cdc_num2list n2 on ds.EMPI=n2.EMPI and ds.meas='CDC2'  and ds.Event=1 and CE=1 and rexcl=0 and rexcld=0

update #cdcdataset set num=1 from #cdcdataset ds join #cdc_num2list n2 on ds.EMPI=n2.EMPI and ds.meas='CDC2'  and ds.Event=1 and rexcl=0 and rexcld=0



-- Create Num Details for CDC2
drop table if exists #cdc_numdetails_cdc2;
CREATE table #cdc_numdetails_cdc2
(
	EMPI varchar(100),
	servicedate date,
	code varchar(20)

)
Insert into #cdc_numdetails_cdc2
Select
	EMPI
	,ServiceDate
	,Code
From
(
	select 
		*
		,ROW_NUMBER() over(partition by EMPI order by ServiceDate desc) as rn 
	from #cdc_num2list
)t1
Where
	rn=1



	-- Shanawaz 9//24/2021 Adding logic to calculate measure id 5 as a non-inverted measures


-- Members who had HbA1c result<=9 with 7 days of HbA1c Test
Drop table if exists #cdc_5_list
Select
	EMPI,
	ServiceDate,
	Code
	into #cdc_5_list
From
(
	Select 
		EMPI
		,ServiceDate
		,Code
		,ROW_NUMBER() over(Partition by EMPI Order by ServiceDate Desc) as rn
	From
	(
		select 
			t1.EMPI
			,t1.servicedate
			,t1.Code
			,t1.value
		from
		(
			select 
				rv.EMPI
				,rs.servicedate
				,rs.Code
				,replace(replace(rs.value,'>',''),'<','') as value 
			from #cdc_hba1cmostrecentvisit rv
			join #cdc_hba1crslt rs on rv.EMPI=rs.EMPI
			where 
				DATEDIFF(DAY,rs.servicedate,rv.servicedate )<=7 and 
				DATEDIFF(DAY,rs.servicedate,rv.servicedate )>=-7 and
				ISNUMERIC(replace(replace(rs.value,'>',''),'<',''))=1

		)t1 
		where 
			t1.servicedate = 
			(
				select 
					max(t2.servicedate) 
				from
				(
					select 
						rv.EMPI
						,rs.servicedate
						,rs.Code
						,replace(replace(rs.value,'>',''),'<','') as value 
					from #cdc_hba1cmostrecentvisit rv
					join #cdc_hba1crslt rs on rv.EMPI=rs.EMPI
					where 
						DATEDIFF(DAY,rs.servicedate,rv.servicedate )<=7 and 
						DATEDIFF(DAY,rs.servicedate,rv.servicedate )>=-7 and 
						rs.value!='' and
						ISNUMERIC(replace(replace(rs.value,'>',''),'<',''))=1
				 )t2 
				 where t1.EMPI=t2.EMPI
			)
		)t3
		where 
		value <cast(9 as float) 
		and value!=''
)t1
Where
	rn=1



-- Numerator 4 - Eye Exam
-- Diabetic Retinal Screening  During the Measurement Year
	
	drop table if exists #cdc_num_4_1_list;
	CREATE table #cdc_num_4_1_list
	(
		EMPI varchar(100),
		ServiceDate Date,
		Code varchar(20)
							
	)	
	insert into #cdc_num_4_1_list
	Select distinct
		*
	From
	(

		select 
			p.EMPI
			,p.PROC_START_DATE
			,PROC_CODE
		from PROCEDURES p
		Left outer join CLAIMLINE c on 
			p.CLAIM_ID=c.CLAIM_ID and 
			ISNULL(p.SV_LINE,0)=ISNULL(c.SV_LINE,0) and 
			p.PROC_DATA_SRC=c.CL_DATA_SRC and
			p.ROOT_COMPANIES_ID=c.ROOT_COMPANIES_ID and
			p.EMPI=c.EMPI
		Join PROVIDER_FLAGS prv on
				c.ATT_NPI=prv.ProvId and
				p.ROOT_COMPANIES_ID=prv.ROOT_COMPANIES_ID and
				EyeCareProv='Y'
		where 
			p.ROOT_COMPANIES_ID=@rootId and
			ISNULL(PROC_STATUS,'EVN')!='INT' and
			ISNULL(c.POS,'')!='81' and
			p.PROC_START_DATE between @ce_startdt and @ce_enddt and 
			PROC_Code in
			(
				select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Diabetic Retinal Screening')
			)

		Union All

		select 
			p.EMPI
			,p.PROC_START_DATE
			,PROC_CODE
		from PROCEDURES p
		join Visit v on 
			p.EMPI=v.EMPI and 
			p.PROC_DATA_SRC=v.Visit_DATA_SRC and
			p.ROOT_COMPANIES_ID=v.ROOT_COMPANIES_ID and
			p.PROC_START_DATE=v.VisitDate
		Join PROVIDER_FLAGS prv on
				v.VisitProviderNPI=prv.ProvId and
				p.ROOT_COMPANIES_ID=prv.ROOT_COMPANIES_ID and
				EyeCareProv='Y'
		where 
			p.ROOT_COMPANIES_ID=@rootId and
			ISNULL(PROC_STATUS,'EVN')!='INT' and
			p.PROC_START_DATE between @ce_startdt and @ce_enddt and 
			PROC_Code in
			(
				select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Diabetic Retinal Screening')
			)
		
	)t1
	
	
	update #cdcdataset set num=1 from #cdcdataset ds join #cdc_num_4_1_list n4_1 on ds.EMPI=n4_1.EMPI and ds.meas='CDC4';
	
	
	-- Num 4.2 - Diabetic Retinal Screening During the year prior with negative retinopathy
	-- Current no way to identify negative retinopathy
	drop table if exists #cdc_num_4_2_list;
	CREATE table #cdc_num_4_2_list
	(
		EMPI varchar(100),
		ServiceDate Date,
		Code varchar(20)
	)	
	insert into #cdc_num_4_2_list
	Select distinct
		EMPI
		,ResultDate as ServiceDate
		,ResultCode as Code
	From LAB
	Where
		ROOT_COMPANIES_ID=@rootId and
		ResultDate between @ce_startdt1 and @ce_enddt1 and
		(
			TestCode in
			(
				select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Diabetic Retinal Screening')
			)
			or
			ResultCode in
			(
				select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Diabetic Retinal Screening')
			)
		)
		and
		Value='Negative'
	
	

	-- Numerator 4.3 - Diabetic Retinal Screening with Diabetes Mellitus Without Complications prior year
	drop table if exists #cdc_memberswdbmwoc
	CREATE table #cdc_memberswdbmwoc
	(
		EMPI varchar(100),
		servicedate Date,
		DATA_SRC varchar(50)
						
	)
	Insert into #cdc_memberswdbmwoc
	Select
		EMPI
		,DIAG_START_DATE
		,DIAG_DATA_SRC
	From GetDiagnosis(@rootId,@ce_startdt1,@ce_enddt,'Diabetes Mellitus Without Complications')

--		select * from #cdc_memberswdbmwoc where EMPI=167818
--		select * from #cdc_mem_w_retinalscreening where EMPI=167818

	-- Members with Diabetic Retinal Screening prior year
	drop table if exists #cdc_mem_w_retinalscreening;
	CREATE table #cdc_mem_w_retinalscreening
	(
		EMPI varchar(100),
		servicedate Date,
		Code varchar(20),
		DATA_SRC varchar(50)
						
	)	
	insert into #cdc_mem_w_retinalscreening
	Select distinct
		EMPI
		,PROC_START_DATE
		,PROC_CODE
		,PROC_DATA_SRC
	From
	(
		select 
			p.EMPI
			,p.PROC_START_DATE
			,PROC_CODE
			,PROC_DATA_SRC
		from PROCEDURES p
		join CLAIMLINE c on 
			p.CLAIM_ID=c.CLAIM_ID and
			ISNULL(p.SV_LINE,0)=ISNULL(c.SV_LINE,0) and 
			p.PROC_DATA_SRC=c.CL_DATA_SRC and
			p.ROOT_COMPANIES_ID=c.ROOT_COMPANIES_ID and
			p.EMPI=c.EMPI
		Join PROVIDER_FLAGS prv on
			c.ATT_NPI=prv.ProvId and
			p.ROOT_COMPANIES_ID=prv.ROOT_COMPANIES_ID and
			EyeCareProv='Y'

		where 
			p.ROOT_COMPANIES_ID=@rootId and
			ISNULL(PROC_STATUS,'EVN')!='INT' and
			ISNULL(c.POS,'0')!='81' and
			p.PROC_START_DATE between @ce_startdt1 and @ce_enddt1 and 
			PROC_CODE in
			(
				select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Diabetic Retinal Screening')
			)
			and
			p.CLAIM_ID is not null
			
		
		Union all

		select 
			p.EMPI
			,p.PROC_START_DATE
			,PROC_CODE
			,PROC_DATA_SRC
		from PROCEDURES p
		Join Visit v on 
			p.EMPI=v.EMPI and
			p.ROOT_COMPANIES_ID=v.ROOT_COMPANIES_ID and
			p.PROC_START_DATE=v.VisitDate and
			p.PROC_DATA_SRC=v.VISIT_DATA_SRC
		Join PROVIDER_FLAGS prv on
			v.visitProviderNPI=prv.ProvId and
			p.ROOT_COMPANIES_ID=prv.ROOT_COMPANIES_ID and
			EyeCareProv='Y'
		where 
			p.ROOT_COMPANIES_ID=@rootId and
			ISNULL(PROC_STATUS,'EVN')!='INT' and
			p.PROC_START_DATE between @ce_startdt1 and @ce_enddt1 and 
			PROC_CODE in
			(
				select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Diabetic Retinal Screening')
			)
				


	)t1
	
	
	
	
	drop table if exists #cdc_num_4_3_list
	CREATE table #cdc_num_4_3_list
	(
		EMPI varchar(100),
		ServiceDate Date,
		Code varchar(20)
						
	)	
	Insert into #cdc_num_4_3_list
	select distinct 
		t1.EMPI 
		,t1.servicedate
		,t1.Code
	from #cdc_mem_w_retinalscreening t1
	join #cdc_memberswdbmwoc t2 on 
		t1.EMPI=t2.EMPI and 
		t1.servicedate=t2.servicedate and
		t1.DATA_SRC=t2.DATA_SRC
	
	
	
	update #cdcdataset set num=1 from #cdcdataset ds join #cdc_num_4_3_list n4_3 on ds.EMPI=n4_3.EMPI and ds.meas='CDC4';
	
	-- Numerator 4 - Eye Exam With Evidence of Retinopathy Value Set or Eye Exam Without Evidence of Retinopathy Value Set measurement year
	
	drop table if exists #cdc_num_4_4_list;
	CREATE table #cdc_num_4_4_list
	(
		EMPI varchar(100),
		ServiceDate Date,
		Code varchar(20)
	)	
	insert into #cdc_num_4_4_list
	Select distinct
		*
	From
	(
		Select
			EMPI
			,PROC_START_DATE
			,PROC_CODE
		From GetProceduresWithOutMods(@rootId,@ce_startdt,@ce_enddt,'Eye Exam With Evidence of Retinopathy','CPT CAT II Modifier')

		Union all

		Select
			EMPI
			,PROC_START_DATE
			,PROC_CODE
		From GetProceduresWithOutMods(@rootId,@ce_startdt,@ce_enddt,'Eye Exam Without Evidence of Retinopathy','CPT CAT II Modifier')

		Union all

		Select
			EMPI
			,PROC_START_DATE
			,PROC_CODE
		From GetProcedures(@rootId,@ce_startdt,@ce_enddt,'Automated Eye Exam')
	)t1
	
	update #cdcdataset set num=1 from #cdcdataset ds join #cdc_num_4_4_list n4_4 on ds.EMPI=n4_4.EMPI and ds.meas='CDC4';
	
	-- Numerator 4 the Eye Exam Without Evidence of Retinopathy prior year
	drop table if exists #cdc_num_4_5_list;
	CREATE table #cdc_num_4_5_list
	(
		EMPI varchar(100),
		ServiceDate Date,
		Code varchar(20)
	)
	insert into #cdc_num_4_5_list
	Select
		EMPI
		,PROC_START_DATE
		,PROC_CODE
	From GetProceduresWithOutMods(@rootId,@ce_startdt1,@ce_enddt1,'Eye Exam Without Evidence of Retinopathy','CPT CAT II Modifier')
	
	
	update #cdcdataset set num=1 from #cdcdataset ds join #cdc_num_4_5_list n4_5 on ds.EMPI=n4_5.EMPI and ds.meas='CDC4';
	
	-- Numerator 4 Eye Exam "Diabetic Retinal Screening Negative In Prior Year " during the measurement year
	
	drop table if exists #cdc_num_4_6_list;
	CREATE table #cdc_num_4_6_list
	(
		EMPI varchar(100),
		ServiceDate Date,
		Code varchar(20)
	)	
	insert into #cdc_num_4_6_list
	Select
		EMPI
		,PROC_START_DATE
		,PROC_CODE
	From GetProceduresWithOutMods(@rootId,@ce_startdt,@ce_enddt,'Diabetic Retinal Screening Negative In Prior Year','CPT CAT II Modifier')


	
	update #cdcdataset set num=1 from #cdcdataset ds join #cdc_num_4_6_list n4_6 on ds.EMPI=n4_6.EMPI and ds.meas='CDC4';
	
	-- Numerator 4.7 Eye Exam •	Unilateral eye enucleation (Unilateral Eye Enucleation Value Set) with a bilateral modifier (Bilateral Modifier Value Set). 
	
	drop table if exists #cdc_num_4_7_list;
	CREATE table #cdc_num_4_7_list
	(
		EMPI varchar(100),
		ServiceDate Date,
		Code Varchar(20)
	)
	insert into #cdc_num_4_7_list
	Select
		EMPI
		,PROC_START_DATE
		,PROC_CODE
	From GetProceduresWithMods(@rootId,'1900-01-01',@ce_enddt,'Unilateral Eye Enucleation','Bilateral Modifier')

	
	update #cdcdataset set num=1 from #cdcdataset ds join #cdc_num_4_7_list n4_7 on ds.EMPI=n4_7.EMPI and ds.meas='CDC4';
	
	-- Numerator 4.8 - Two unilateral eye enucleations (Unilateral Eye Enucleation Value Set) with service dates 14 days or more apart
drop table if exists #cdc_num_4_8_list;
CREATE table #cdc_num_4_8_list
(
	EMPI varchar(100),
	ServiceDate Date,
	Code varchar(20)

);
with cte as
(
	select 
		EMPI
		,ServiceDate
		,Code
       ,datediff(d,lag(servicedate) over (partition by EMPI order by servicedate),servicedate) as DaysSinceLastOrder 
	from
	(
		Select
			EMPI
			,PROC_START_DATE as servicedate
			,PROC_CODE as Code
		From GetProcedures(@rootId,'1900-01-01',@ce_enddt,'Unilateral Eye Enucleation')
		
	)t1
)
Insert into #cdc_num_4_8_list
select 
	EMPI
	,ServiceDate
	,Code
from cte
where 
	DaysSinceLastOrder >= 14
	
	
	
	update #cdcdataset set num=1 from #cdcdataset ds join #cdc_num_4_8_list n4_8 on ds.EMPI=n4_8.EMPI and ds.meas='CDC4';
	
	-- Numerator 4.9 : •	Left unilateral eye enucleation (Unilateral Eye Enucleation Left Value Set) and right unilateral eye enucleation (Unilateral Eye Enucleation Right Value Set) on the same or different dates of service
	
	drop table if exists #cdc_mem_leftEyeNucleation
	CREATE table #cdc_mem_leftEyeNucleation
	(
		EMPI varchar(100),
		servicedate Date,
		Code varchar(20)

	)	
	Insert into #cdc_mem_leftEyeNucleation
	Select
		EMPI
		,PROC_START_DATE as servicedate
		,ICDPCS_Code as Code
	From GetICDPCS(@rootId,'1900-01-01',@ce_enddt,'Unilateral Eye Enucleation Left')

	
	-- Right eye nucelation
	drop table if exists #cdc_mem_rightEyeNucleation
	CREATE table #cdc_mem_rightEyeNucleation
	(
		EMPI varchar(100),
		servicedate Date,
		Code varchar(20)

	)
	Insert into #cdc_mem_rightEyeNucleation
	Select
		EMPI
		,PROC_START_DATE as servicedate
		,ICDPCS_Code as Code
	From GetICDPCS(@rootId,'1900-01-01',@ce_enddt,'Unilateral Eye Enucleation Right')


	-- Identify members who had left and right enucleation
	drop table if exists #cdc_num_4_9_list;
	CREATE table #cdc_num_4_9_list
	(
		EMPI varchar(100),
		servicedate Date,
		Code varchar(20)

	)
	insert into #cdc_num_4_9_list
	select 
		t1.EMPI
		,t1.servicedate
		,t1.Code
	from #cdc_mem_leftEyeNucleation t1
	join #cdc_mem_rightEyeNucleation t2 on t1.EMPI=t2.EMPI
	
		
	update #cdcdataset set num=1 from #cdcdataset ds join #cdc_num_4_9_list n4_9 on ds.EMPI=n4_9.EMPI and ds.meas='CDC4';
	
	--Numerator 4.10 :•	A unilateral eye enucleation (Unilateral Eye Enucleation Value Set) and a left unilateral eye enucleation (Unilateral Eye Enucleation Left Value Set) with service dates 14 days or more apart
	
	
	drop table if exists #cdc_mem_EyeNucleation
	CREATE table #cdc_mem_EyeNucleation
	(
		EMPI varchar(100),
		servicedate Date,
		Code varchar(20)

	)
	insert into #cdc_mem_EyeNucleation
	Select
		EMPI
		,PROC_START_DATE as servicedate
		,Proc_Code as Code
	From GetProcedures(@rootId,'1900-01-01',@ce_enddt,'Unilateral Eye Enucleation')

	

	
	
	
	drop table if exists #cdc_num_4_10_list;
	CREATE table #cdc_num_4_10_list
	(
		EMPI varchar(100),
		servicedate Date,
		Code varchar(20)
	)	
	insert into #cdc_num_4_10_list
	select 
		t1.EMPI
		,t1.servicedate
		,t1.Code
	from #cdc_mem_EyeNucleation t1
	join #cdc_mem_leftEyeNucleation t2 on t1.EMPI=t2.EMPI
	where Abs(DATEDIFF(day,t1.servicedate,t2.servicedate))>=14
	
	update #cdcdataset set num=1 from #cdcdataset ds join #cdc_num_4_10_list n4_10 on ds.EMPI=n4_10.EMPI and ds.meas='CDC4';
	
	
	-- Numerator 4.11:•	A unilateral eye enucleation (Unilateral Eye Enucleation Value Set) and a right unilateral eye enucleation (Unilateral Eye Enucleation Right Value Set) with service dates 14 days or more apart.
	
	drop table if exists #cdc_num_4_11_list;
	CREATE table #cdc_num_4_11_list
	(
		EMPI varchar(100),
		servicedate Date,
		Code varchar(20)
	)	
	insert into #cdc_num_4_11_list
	select 
		t1.EMPI
		,t1.servicedate
		,t1.Code
	from #cdc_mem_EyeNucleation t1
	join #cdc_mem_rightEyeNucleation t2 on t1.EMPI=t2.EMPI
	where 
		Abs(DATEDIFF(day,t1.servicedate,t2.servicedate))>=14
	

	update #cdcdataset set num=1 from #cdcdataset ds join #cdc_num_4_11_list n4_11 on ds.EMPI=n4_11.EMPI and ds.meas='CDC4';

	-- Create Numerator Details for CDC4
	Drop table if exists #cdc_numdetails_cdc4;
	Create table #cdc_numdetails_cdc4
	(
		EMPI varchar(100),
		ServiceDate Date,
		Code varchar(20)
	)
	Insert into #cdc_numdetails_cdc4
	Select
		EMPI
		,ServiceDate
		,Code
	From
	(
		Select 
			*
			,row_number() over(partition by EMPI order by ServiceDate Desc) as rn
		From
		(
			select EMPI,ServiceDate,Code from #cdc_num_4_1_list

			Union all

			select EMPI,ServiceDate,Code from #cdc_num_4_2_list

			Union all

			select EMPI,ServiceDate,Code from #cdc_num_4_3_list

			Union all

			select EMPI,ServiceDate,Code from #cdc_num_4_4_list

			union all

			select EMPI,ServiceDate,Code from #cdc_num_4_5_list

			union all 

			select EMPI,ServiceDate,Code from #cdc_num_4_6_list

			union all

			select EMPI,ServiceDate,Code from #cdc_num_4_7_list

			union all

			select EMPI,ServiceDate,Code from #cdc_num_4_8_list

			union all

			select EMPI,ServiceDate,Code from #cdc_num_4_9_list

			union all

			select EMPI,ServiceDate,Code from #cdc_num_4_10_list

			union all

			select EMPI,ServiceDate,Code from #cdc_num_4_11_list
		)t1
	)t2
	where
		rn=1

	

	-- Numerator Nephropathy CDC 7
	drop table if exists #cdc_num_7_list;
	CREATE table #cdc_num_7_list
	(
		EMPI varchar(100),
		ServiceDate Date,
		Code varchar(20)

	)	
	insert into #cdc_num_7_list
	select distinct 
		EMPI
		,ServiceDate
		,Code
	from
	(

		select 
			p.EMPI
			,p.PROC_START_DATE as ServiceDate
			,p.PROC_CODE as Code
		from PROCEDURES p
		where 
			p.ROOT_COMPANIES_ID=@rootId and 
			ISNULL(PROC_STATUS,'EVN')!='INT' and 
			PROC_START_DATE between @ce_startdt and @ce_enddt	and
			PROC_CODE in
			(
				select Code from HDS.VALUESET_TO_CODE where code_system!='CPT-CAT-II' and Value_Set_Name='Urine Protein Tests'
			)
	
			

		Union all

		select 
			p.EMPI
			,p.PROC_START_DATE as ServiceDate
			,p.PROC_CODE as Code
		from PROCEDURES p
		where 
			p.ROOT_COMPANIES_ID=@rootId and 
			ISNULL(PROC_STATUS,'EVN')!='INT' and 
			PROC_START_DATE between @ce_startdt and @ce_enddt	and
			PROC_CODE in
			(
				select Code from HDS.VALUESET_TO_CODE where code_system='CPT-CAT-II' and Value_Set_Name='Urine Protein Tests'
			)
			and
			ISNULL(MOD_1,'') not in
			(
				select Code from HDS.VALUESET_TO_CODE where  Value_Set_Name='CPT CAT II Modifier'
			)
			and
			ISNULL(MOD_2,'') not in
			(
				select Code from HDS.VALUESET_TO_CODE where  Value_Set_Name='CPT CAT II Modifier'
			)
			and
			ISNULL(MOD_3,'') not in
			(
				select Code from HDS.VALUESET_TO_CODE where  Value_Set_Name='CPT CAT II Modifier'
			)
			and
			ISNULL(MOD_4,'') not in
			(
				select Code from HDS.VALUESET_TO_CODE where  Value_Set_Name='CPT CAT II Modifier'
			)

		Union All

		Select
			EMPI
			,PROC_START_DATE as ServiceDate
			,PROC_CODE as Code
		From GetProcedureswithoutMods(@rootId,@ce_startdt,@ce_enddt,'Nephropathy Treatment','CPT CAT II Modifier')
	--	Where EMPI=182024

		Union All

		Select
			EMPI
			,DIAG_START_DATE as ServiceDate
			,DIAG_CODE as Code
		From GetDiagnosis(@rootId,@ce_startdt,@ce_enddt,'Nephropathy Treatment')
	--	Where EMPI=182024

		Union All

		Select
			EMPI
			,DIAG_START_DATE as ServiceDate
			,DIAG_CODE as Code
		From GetDiagnosis(@rootId,@ce_startdt,@ce_enddt,'ESRD Diagnosis')
	--	Where EMPI=182024

		Union All

		Select
			EMPI
			,PROC_START_DATE as ServiceDate
			,PROC_CODE as Code
		From GetProcedures(@rootId,@ce_startdt,@ce_enddt,'Dialysis Procedure')
		--Where EMPI=182024

		
		Union All

		Select
			EMPI
			,PROC_START_DATE as ServiceDate
			,ICDPCS_CODE as Code
		From GetICDPCS(@rootId,@ce_startdt,@ce_enddt,'Dialysis Procedure')
	--	Where EMPI=182024

		Union All

		Select
			EMPI
			,PROC_START_DATE as ServiceDate
			,PROC_CODE as Code
		From GetProcedures(@rootId,@ce_startdt,@ce_enddt,'Nephrectomy')
	--	Where EMPI=182024

		
		Union All

		Select
			EMPI
			,PROC_START_DATE as ServiceDate
			,ICDPCS_CODE as Code
		From GetICDPCS(@rootId,@ce_startdt,@ce_enddt,'Nephrectomy')
	--	where EMPi=182024

		Union All

		Select
			EMPI
			,PROC_START_DATE as ServiceDate
			,PROC_CODE as Code
		From GetProcedures(@rootId,@ce_startdt,@ce_enddt,'Kidney Transplant')
		--where EMPI=182024
		
		Union All

		Select
			EMPI
			,PROC_START_DATE as ServiceDate
			,ICDPCS_CODE as Code
		From GetICDPCS(@rootId,@ce_startdt,@ce_enddt,'Kidney Transplant')
		--where EMPI=182024

		Union All

		Select
			EMPI
			,DIAG_START_DATE as ServiceDate
			,DIAG_CODE as Code
		From GetDiagnosis(@rootId,@ce_startdt,@ce_enddt,'CKD Stage 4')
	--	where EMPI=182024

		Union all

		Select
			EMPI
			,ResultDate
			,ResultCode
		From Lab
		Where
			ROOT_COMPANIES_ID=@rootId and
			ResultDate Between @ce_startdt and @ce_enddt and
			ResultCode in
			(
				select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Urine Protein Tests')
			)
		--	and EMPi=182024

		Union all

		Select
			EMPI
			,ResultDate
			,TestCode
		From Lab
		Where
			ROOT_COMPANIES_ID=@rootId and
			ResultDate Between @ce_startdt and @ce_enddt and
			TestCode in
			(
				select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Urine Protein Tests')
			)
			--and EMPi=182024


	

		Union all

	
		Select
			EMPI
			,FILL_DATE as ServiceDate
			,MEDICATION_CODE as Code
		From MEDICATION
		Where
			ROOT_COMPANIES_ID=@rootId and
			FILL_DATE between @ce_startdt and @ce_enddt and
			MEDICATION_CODE in
			(
				select code from HDS.MEDICATION_LIST_TO_CODES where Medication_List_Name='ACE Inhibitor and ARB Medications'
			)
			--and EMPI=182024

		Union All

		Select
			EMPI
			,From_Date
			,'Nephrologist Visit' as Code
		From CLAIMLINE c
		Join Provider_Flags prv on
			c.ATT_NPI=prv.ProvId and
			c.ROOT_COMPANIES_ID=prv.ROOT_COMPANIES_ID and
			Nephrologist='Y'
		where
			c.ROOT_COMPANIES_ID=@rootId and
			ISNULL(POS,'')!='81' and
			FROM_DATE between @ce_startdt and @ce_enddt
		--	and EMPI=182024

		Union all

		Select
			EMPI
			,VisitDate
			,'Nephrologist Visit' as Code
		From Visit c
		Join Provider_Flags prv on
			c.VisitProviderNPI=prv.ProvId and
			c.ROOT_COMPANIES_ID=prv.ROOT_COMPANIES_ID and
			Nephrologist='Y'
		where
			c.ROOT_COMPANIES_ID=@rootId and
			VisitDate between @ce_startdt and @ce_enddt
		--	and EMPI=182024
		
	
)t1
	

	
	update #cdcdataset set num=1 from #cdcdataset ds join #cdc_num_7_list n7 on ds.EMPI=n7.EMPI and ds.meas='CDC7';

	
	--Creating Numdetails for CDC7
	drop table if exists #cdc_numdetails_cdc7
	Create table #cdc_numdetails_cdc7
	(
		EMPI varchar(100),
		ServiceDate Date,
		Code varchar(20)
	)
	Insert into #cdc_numdetails_cdc7
	Select
		EMPI
		,ServiceDate
		,Code
	From
	(
		Select
			*
			,ROW_NUMBER() over (partition by EMPI order by ServiceDate desc) as rn
		from #cdc_num_7_list
	)t1
	where
		rn=1

	

	-- Numerator CDC 9 :BP Control <140/90 mm Hg
	
	-- Identify Vst visit (Outpatient Without UBREV Value Set), telephone visit (Telephone Visits Value Set), e-visit or virtual check-in (Online Assessments Value Set), a nonacute inpatient encounter (Nonacute Inpatient Value Set), or remote monitoring event (Remote Blood Pressure Monitoring Value Set) during the measurement year. 
	
	-- Identify Eligible viit list
	drop table if exists #cdc_9_visitlist;				
	CREATE table #cdc_9_visitlist
	(
		EMPI varchar(100),
		servicedate Date
					
	)	
	Insert into #cdc_9_visitlist
	Select distinct
		*
	From
	(


		Select 
			EMPI
			,PROC_START_DATE 
		From PROCEDURES 
		where
			ROOT_COMPANIES_ID=@rootId and
			ISNULL(PROC_STATUS,'EVN')!='INT' AND
			PROC_START_DATE between @ce_startdt and @ce_enddt and
			PROC_CODE in
			(
				select Code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Telephone Visits','Online Assessments','Nonacute Inpatient','Remote Blood Pressure Monitoring')
			)

		Union all

		Select 
			EMPI
			,VisitDate 
		From VISIT 
		where
			ROOT_COMPANIES_ID=@rootId and
			VisitDate between @ce_startdt and @ce_enddt and
			VisitTypeCode in
			(
				select Code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Telephone Visits','Online Assessments','Nonacute Inpatient','Remote Blood Pressure Monitoring')
			)

		Union all

		Select 
			EMPI
			,DIAG_START_DATE 
		From Diagnosis 
		where
			ROOT_COMPANIES_ID=@rootId and
			DIAG_START_DATE between @ce_startdt and @ce_enddt and
			DIAG_CODE in
			(
				select Code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Telephone Visits','Online Assessments','Nonacute Inpatient','Remote Blood Pressure Monitoring')
			)

		Union all

		Select 
			EMPI
			,FROM_DATE 
		From CLAIMLINE 
		where
			ROOT_COMPANIES_ID=@rootId and
			FROM_DATE between @ce_startdt and @ce_enddt and
			REV_CODE in
			(
				select Code from HDS.VALUESET_TO_CODE where code_system='UBREV' and Value_Set_Name in('Outpatient')
			)


		
	)t1



	-- Identify BP Reading visit
	drop table if exists #cdc_bpreadinglist;				
	CREATE table #cdc_bpreadinglist
	(
		EMPI varchar(100),
		servicedate Date,
		Code varchar(20),
		DATA_SRC varchar(50),
		CLAIM_ID varchar(100)
						
	)
	Insert into #cdc_bpreadinglist
	Select distinct
		EMPI
		,ServiceDate
		,Code
		,PROC_DATA_SRC
		,CLAIM_ID
	From
	(
		select 
			p.EMPI 
			,p.PROC_START_DATE as ServiceDate
			,p.PROC_CODE as Code
			,PROC_DATA_SRC
			,p.CLAIM_ID
		from PROCEDURES p
		Left outer Join CLAIMLINE c on p.CLAIM_ID=c.CLAIM_ID and
									   ISNULL(p.SV_LINE,0)=ISNULL(c.SV_LINE,0) and
									   p.PROC_DATA_SRC=c.CL_DATA_SRC and
									   p.ROOT_COMPANIES_ID=c.ROOT_COMPANIES_ID and
									   p.EMPI=c.EMPI
		where 
			p.ROOT_COMPANIES_ID=@rootId and 
			ISNULL(PROC_STATUS,'EVN')!='INT' and
			PROC_START_DATE between @ce_startdt and @ce_enddt and 
			PROC_CODE IN
			(
				select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Diastolic Blood Pressure','Systolic Blood Pressure')
			)
			and
			ISNULL(MOD_1,'') not in
			(
				select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('CPT CAT II Modifier')
			)
			and
			ISNULL(MOD_2,'') not in
			(
				select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('CPT CAT II Modifier')
			)
			and
			ISNULL(MOD_3,'') not in
			(
				select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('CPT CAT II Modifier')
			)
			and
			ISNULL(MOD_4,'') not in
			(
				select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('CPT CAT II Modifier')
			)

		Union all

		Select
			EMPI
			,ResultDate as ServiceDate
			,ResultCode as Code
			,LAB_DATA_SRC
			,NULL as CLAIM_ID
		From LAB
		Where
			ROOT_COMPANIES_ID=@rootId and
			ResultDate between @ce_startdt and @ce_enddt and
			(
				TestCode in
				(
					select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Diastolic Blood Pressure','Systolic Blood Pressure')
				)
				or
				ResultCode in
				(
					select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Diastolic Blood Pressure','Systolic Blood Pressure')
				)
			)

		Union ALl

		Select
			EMPI
			,Cast(VitalDateTime as Date) as ServiceDate
			,VitalCode as Code
			,VIT_DATA_SRC
			,NULL as CLAIM_ID
		From VITAL
		Where
			ROOT_COMPANIES_ID=@rootId and
			VitalDateTime between @ce_startdt and @ce_enddt and
			VitalCode in
			(
				select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Diastolic Blood Pressure','Systolic Blood Pressure')
			)

		Union ALl

		Select
			EMPI
			,DIAG_START_DATE
			,DIAG_CODE as Code
			,DIAG_DATA_SRC
			,NULL as CLAIM_ID
		From Diagnosis
		Where
			ROOT_COMPANIES_ID=@rootId and
			DIAG_START_DATE between @ce_startdt and @ce_enddt and
			DIAG_CODE in
			(
				select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Diastolic Blood Pressure','Systolic Blood Pressure')
			)
	)t1


			
	
	
	-- Numerator compliant members
	drop table if exists #cdc_numcompliantmemlist;				
	CREATE table #cdc_numcompliantmemlist
	(
		EMPI varchar(100),
		servicedate Date,
		Code varchar(20)
				
	)
	insert into #cdc_numcompliantmemlist
	-- Members with Systolic <140 and diastolic<90 based on results
	-- Numerator compliant members
	select distinct 
		EMPI
		,servicedate 
		,Code
	from
	(
		select distinct 
			s.EMPI
			,s.servicedate 
			,s.Code
		from
		(
			Select 
				EMPI
				,Servicedate
				,Code
			from
			(
				Select
					EMPI
					,Cast(ResultDate as Date) as ServiceDate
					,ResultCode as Code
					,Try_convert(decimal,SUBSTRING(value, PATINDEX('%[0-9]%', value), LEN(value))) as value
				From LAB
				Where
					ROOT_COMPANIES_ID=@rootId and
					ResultDate between @ce_startdt and @ce_enddt and
					(
						TestCode in
						(
							select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Systolic Blood Pressure')
						)
						or
						ResultCode in
						(
							select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Systolic Blood Pressure')
						)
					)
			)t1
			where
				value<140

			Union ALL

			Select
				EMPI
				,Cast(VitalDateTime as Date) as ServiceDate
				,VitalCode as Code
			From VITAL
			Where
				ROOT_COMPANIES_ID=@rootId and
				VitalDateTime between @ce_startdt and @ce_enddt and
				VitalCode in
				(
					select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Systolic Blood Pressure')
				)
				and
				value<cast(140 as float)

	
	)s
	join
	(

		Select distinct
				EMPI
				,Servicedate
				,Code
		from
		(
			Select 
				EMPI
				,Cast(ResultDate as Date) as ServiceDate
				,ResultCode as Code
				,Try_convert(decimal,SUBSTRING(value, PATINDEX('%[0-9]%', value), LEN(value))) as value
			From LAB
			Where
				ROOT_COMPANIES_ID=@rootId and
				ResultDate between @ce_startdt and @ce_enddt and
				(
					TestCode in
					(
						select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Diastolic Blood Pressure')
					)
					or
					ResultCode in
					(
						select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Diastolic Blood Pressure')
					)
				)
			)t1
			where
				value<90

			Union ALL

			Select 
				EMPI
				,Cast(VitalDateTime as Date) as ServiceDate
				,VitalCode as Code
			From VITAL
			Where
				ROOT_COMPANIES_ID=@rootId and
				VitalDateTime between @ce_startdt and @ce_enddt and
				VitalCode in
				(
					select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Diastolic Blood Pressure')
				)
				and
				value<cast(90 as float)
		
	
	)d on 
		d.EMPI=s.EMPI and 
		d.servicedate=s.servicedate

	Union all

-- Members with Systolic <140 and diastolic<90 based on CPT || category codes
	-- Members with Systolic <140 and diastolic<90 based on CPT || category codes
	
	Select
		s.EMPI
		,s.ServiceDate
		,s.Code
	From
	(
		Select distinct
			EMPI
			,Proc_start_date as ServiceDate
			,Proc_Code as Code
		From GetProceduresWithOutMods(@rootId,@ce_startdt,@ce_enddt,'Systolic Less Than 140','CPT CAT II Modifier')
		
		
	)s
	Join
	(
		Select distinct
			EMPI
			,Proc_start_date as ServiceDate
			,Proc_Code as Code
		From GetProceduresWithOutMods(@rootId,@ce_startdt,@ce_enddt,'Diastolic 80-89','CPT CAT II Modifier')

		Union all

		Select distinct
			EMPI
			,Proc_start_date as ServiceDate
			,Proc_Code as Code
		From GetProceduresWithOutMods(@rootId,@ce_startdt,@ce_enddt,'Diastolic Less Than 80','CPT CAT II Modifier')
		
		
	
	)d on 
		s.EMPI=d.EMPI and 
		s.servicedate=d.servicedate
)numcompmemlist


	

	-- Join readings with visit list
	drop table if exists #cdc_bpwithnumvstlist;				
	CREATE table #cdc_bpwithnumvstlist
	(
		EMPI varchar(100),
		servicedate Date,
		Code varchar(20)
						
	)
	insert into #cdc_bpwithnumvstlist
	Select distinct
		*
	From
	(

		select 
			t1.EMPI
			,t1.servicedate
			,t2.Code
		from #cdc_9_visitlist t1
		join #cdc_bpreadinglist t2 on 
			t1.EMPI=t2.EMPI and 
			t1.servicedate=t2.servicedate 
	--	Where t1.EMPI=123925
	)t1



	-- identify most recent reading
	drop table if exists #cdc_mostrecentbpreadinglist;				
	CREATE table #cdc_mostrecentbpreadinglist
	(
		EMPI varchar(100),
		servicedate date,
		Code varchar(20)
						
	)
	Insert into #cdc_mostrecentbpreadinglist
	select 
		t1.EMPI
		,t1.servicedate 
		,t1.Code
	from #cdc_bpwithnumvstlist t1 
	where 
		t1.servicedate=
		(
			select max(t2.servicedate) from #cdc_bpwithnumvstlist t2 where t1.EMPI=t2.EMPI
		)

		
	
    -- Identify bp readings taken during the specified visit types        
	drop table if exists #cdc_num_9_list;						
	CREATE table #cdc_num_9_list
	(
		EMPI varchar(100),
		ServiceDate Date,
		Code varchar(20)
					
	)	
	Insert into #cdc_num_9_list
	select distinct 
		nc.EMPI
		,nc.ServiceDate
		,nc.Code
	from #cdc_numcompliantmemlist nc
	join #cdc_mostrecentbpreadinglist nv on 
		nv.EMPI=nc.EMPI and 
		nv.servicedate=nc.servicedate
	
	
	
	
	

	-- Updating NUM cdcumn
	
	update #cdcdataset set num=1 from #cdcdataset ds	join #cdc_num_9_list n9 on n9.EMPI=ds.EMPI  and ds.meas='CDC9' ;

	
		--Creating Numdetails for CDC9
	drop table if exists #cdc_numdetails_cdc9
	Create table #cdc_numdetails_cdc9
	(
		EMPI varchar(100),
		ServiceDate Date,
		Code varchar(20)
	)
	Insert into #cdc_numdetails_cdc9
	Select
		EMPI
		,ServiceDate
		,Code
	From
	(
		Select
			*
			,ROW_NUMBER() over (partition by EMPI order by ServiceDate desc) as rn
		from #cdc_num_9_list
	)t1
	where
		rn=1


	
	
	-- SES for CDC 4
	--SES Startification
	
	Drop Table if exists #cdc_lisflag
	Create Table #cdc_lisflag
	(
		EMPI varchar(100)
	)
	Insert into #cdc_lisflag
	Select
		EMPI
	From GetLISFlag(@rootId,@ce_startdt,@ce_enddt)


update ds 
	set ds.lis=1 
from #cdcdataset ds 
join #cdc_lisflag l on 
	l.EMPI=ds.EMPI
where 
	ds.payer IN('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP','MR') 
	

-- Item 48
drop table if exists #cdc_orec;
CREATE table #cdc_orec
(
		EMPI varchar(100),
		orec varchar(2)
			
)
Insert into #cdc_orec
Select
	EMPI
	,OREC
From GetOREC(@rootId,@ce_startdt,@ce_enddt)
	


update #cdcdataset set orec=o.orec from #cdcdataset ds
join #cdc_orec o on o.EMPI=ds.EMPI
where ds.payer IN('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP','MR') ;



update #cdcdataset set meas='CDC4NON' WHERE orec=0 AND lis=0 AND event=1 and ce=1 AND rexcl=0 and rexcld=0 AND payer IN ('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP','MR')  and meas='CDC4';


UPDATE #cdcdataset SET meas='CDC4LISDE' WHERE orec=0 AND lis=1 AND event=1 and ce=1 AND rexcl=0 and rexcld=0 AND payer IN ('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP','MR')  and meas='CDC4';

UPDATE #cdcdataset SET meas='CDC4DIS' WHERE orec IN(1,3) AND lis=0 AND event=1 AND ce=1 AND rexcl=0 and rexcld=0 AND payer IN ('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP','MR')  and meas='CDC4';

UPDATE #cdcdataset SET meas='CDC4CMB' WHERE orec IN(1,3) AND lis=1 AND event=1 AND ce=1 AND rexcl=0 and rexcld=0 AND payer IN ('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP','MR')  and meas='CDC4';


UPDATE #cdcdataset SET meas='CDC4OT' WHERE orec IN(2,9) AND event=1 AND ce=1 AND rexcl=0 and rexcld=0 AND payer IN ('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP','MR')  and meas='CDC4';


-- Get ReportId from Report_Details

	exec GetReportDetail @rundate=@rundate,@rootId=@rootId,@startdate=@startdate output,@enddate=@enddate output,@quarter=@quarter output,@reportId=@reportId output


	-- Commenting GapSupportingReport logic as we are not populating this in Quality Gap Reports
	/*
	Delete from KPI_ENGINE_MART.RPT.GapSupportingReport where Root_Companies_id=@rootId and Measure_id='4';
	Insert into KPI_ENGINE_MART.RPT.GapSupportingReport(EMPI,ServiceDate,Code,GapReason,BillingProviderNPI,BillingProviderTIN,BillingProvider,ROOT_COMPANIES_ID,ReportId,Measure_Id,RefreshDate)
	Select
		EMPI,
		ServiceDate,
		Code,
		GapReason,
		BillingProviderNPI,
		BillingProviderTIN,
		BillingProvider,
		@rootId as ROOT_COMPANIES_ID,
		@reportId as ReportId,
		'4' as Measure_Id,
		GetDate() as RefreshDate
	From
	(
		select 
			ds.EMPI,
			rv.servicedate,
			Coalesce(rv.Code,rs.Code) as Code,
			rs.value,
			case
				when rv.servicedate is null then 'No HbA1c'
				when rs.servicedate is null then 'No HbA1c Result'
				when cast(rs.value as float)>=cast(8.0 as float)  then 'Result>8'
				
			end as GapReason,
			rv.BILL_PROV as BillingProviderNPI,
			rv.BILL_PROV_TIN as BillingProviderTIN,
			coalesce(NULLIF(concat(Provider_Last_Name,' ',Provider_First_Name),' '),Provider_organization_name) as BillingProvider,
			row_number() over(Partition by ds.EMPI order by rv.servicedate desc,rs.servicedate desc) as rn
		from #cdcdataset ds
		Left Outer join #cdc_hba1cmostrecentvisit rv on
			ds.EMPI=rv.EMPI 
		Left outer Join #cdc_hba1crslt rs on
			rv.EMPI=rs.EMPI and
			rs.servicedate between Dateadd(day,-7,rv.servicedate) and Dateadd(day,7,rv.servicedate)
		Left outer Join RFT_NPI n on 
			rv.BILL_PROV=n.NPI
		where
			ds.meas='CDC10' and Event=1 and excl=0 and rexcl=0 and rexcld=0 and num=0
			--and ds.EMPI='E58AADA0-2D58-4133-B86A-69CF334ED026'

	)t1
	Where
		rn=1 and GapReason is not null

		*/

	


	Delete from HDS.HEDIS_MEASURE_OUTPUT  where MEASURE_ID=@meas and RUN_ID=@reportId and ROOT_COMPANIES_ID=@rootId

	Insert into HDS.HEDIS_MEASURE_OUTPUT(memid,Meas,payer,CE,Event,Epop,Excl,Num,Rexcl,RExclD,Age,Gender,Measure_ID,Measurement_year,RUN_ID,ROOT_COMPANIES_ID)
	SELECT EMPI AS memid,meas,payer,CE,EVENT,CASE WHEN CE=1 AND EVENT=1 AND rexcl=0 and rexcld=0 THEN 1 ELSE 0 END AS epop,excl,num,rexcl,rexcld,age,gender,@meas,@meas_year,@reportId,@rootId FROM #cdcdataset

	

	Delete from RPT.MEASURE_DETAILED_LINE where MEASURE_ID in('4','5','6','8','CDC1','CDC9') and REPORT_ID=@reportId and ROOT_COMPANIES_ID=@rootId;


	-- Insert data into Measure Detailed Line
	Insert into RPT.MEASURE_DETAILED_LINE(Provider_Id,PCP_NPI,PCP_NAME,Practice_Name,Specialty,Measure_id,Measure_Name,Payer,PayerId,MEM_FNAME,MEM_MName,MEM_LNAME,MEM_DOB,MEM_GENDER,ENROLLMENT_STATUS,Last_visit_date,Product_Type,Num,Den,Excl,Rexcl,CE,Event,Epop,Report_Id,ReportType,Report_Quarter,Period_Start_date,Period_end_Date,Root_Companies_id,EMPI,MEASURE_TYPE,Code,DateofService)
	Select 
		a.AmbulatoryPCPNPI as Provider_Id 
		,a.AmbulatoryPCPNPI
		,a.AmbulatoryPCPName as PCP_NAME
		,a.AmbulatoryPCPPractice as Practice_Name
		,a.AmbulatoryPCPSpecialty as Specialty
		,Case
			when d.meas='CDC10' then '4'
			when d.meas='CDC2' then '5'
			when d.meas='CDC7' then '6'
			when d.meas like 'CDC4%' then '8'
			else d.meas
		end as Measure_id
		,Case
			when d.meas='CDC10' then 'Diabetic: HbA1c Control <8%'
			when d.meas='CDC2' then 'Diabetic: HbA1c Control <=9%'
			when d.meas='CDC7' then 'Diabetic: Nephropathy Screening'
			when d.meas like 'CDC4%' then 'Diabetic: Dilated Eye Exam'
		--	when d.meas='CDC1' then 'Diabetic: HbA1C Testing'
		--	when d.meas='CDC9' then 'Diabetic: Blood Pressure Control <140/90'
		end as Measure_Name
		,a.DATA_SOURCE as Payer
		,a.PayerId
		,a.MemberFirstName as MEM_FNAME
		,a.MemberMiddleName as MEM_MName
		,a.MemberLastName as MEM_LNAME
		,a.MemberDOB
		,a.MEM_GENDER
		,a.EnrollmentStatus
		,a.AmbulatoryPCPRecentVisit as Last_visit_date
		,d.Payer
		,Case
			when d.meas='CDC2' and Num=1 Then 0
			when d.meas='CDC2' and Num=0 Then 1
			else Num
		end as Num
		,1 as Den
		,Excl
		,case
			when Rexcl=1 or rexcld=1 Then 1
			else 0
		end as Rexcl
		,CE
		,Event
		,CASE 
			WHEN  event=1 THEN 1 
			ELSE 0 
		END AS epop
		,@reportId
		,@reporttype
		,@quarter
		,@startDate
		,@enddate
		,@rootId
		,d.EMPI
		,@measuretype
		,Case
			when d.meas='CDC10' then nd10.Code
			when d.meas='CDC2' then nd2.code
			when d.meas='CDC7' then nd7.Code
			when d.meas like 'CDC4%' then nd4.Code
	--		when d.meas='CDC1' then nd1.Code
	--		when d.meas='CDC9' then nd9.Code
		end as Code
		,Case
			when d.meas='CDC10' then nd10.ServiceDate
			when d.meas='CDC2' then nd2.ServiceDate
			when d.meas='CDC7' then nd7.ServiceDate
			when d.meas like 'CDC4%' then nd4.ServiceDate
	--		when d.meas='CDC1' then nd1.ServiceDate
	--		when d.meas='CDC9' then nd9.ServiceDate
		end as ServiceDate
	From #cdcdataset d
	join KPI_ENGINE.RPT.PCP_ATTRIBUTION a on d.EMPI=a.EMPI and a.Reportid=@reportId
	--Left outer join #cdc_numdetails_cdc1 nd1 on d.EMPI=nd1.EMPI and d.meas='CDC1'
	Left outer join #cdc_5_list nd2 on d.EMPI=nd2.EMPI and d.meas='CDC2'
	Left outer join #cdc_numdetails_cdc4 nd4 on d.EMPI=nd4.EMPI and d.meas like 'CDC4%'
	Left outer join #cdc_numdetails_cdc7 nd7 on d.EMPI=nd7.EMPI and d.meas='CDC7'
	--Left outer join #cdc_numdetails_cdc9 nd9 on d.EMPI=nd9.EMPI and d.meas='CDC9'
	Left outer join #cdc_numdetails_cdc10 nd10 on d.EMPI=nd10.EMPI and d.meas='CDC10'
	where 
		d.Event=1 and
		d.meas not in('CDC9','CDC1') 
		/* Shanawaz 9-24-2021 adding logic to report Measure for Commercial Payer and Measure 5 for Medicare*/
		and 1=Case 
		when d.Meas='CDC10' and a.DATA_SOURCE in(Select DATA_SOURCE from DATA_SOURCE where PayerType='Commercial') Then 1
		when d.Meas='CDC2' and a.DATA_SOURCE in(Select DATA_SOURCE from DATA_SOURCE where PayerType='Medicare') Then 1
		when (d.meas='CDC7' or d.meas like 'CDC4%') Then 1
		else 0
	  end 




	Delete from RPT.PROVIDER_SCORECARD  where MEASURE_ID  in('4','5','6','8') and REPORT_ID=@reportId and ROOT_COMPANIES_ID=@rootId;

		-- Insert data into Provider Scorecard
Insert into RPT.PROVIDER_SCORECARD(Provider_Id,PCP_NPI,PCP_NAME,Specialty,Practice_Name,Measure_id,Measure_Name,Measure_Title,MEASURE_SUBTITLE,Measure_Type,NUM_COUNT,DEN_COUNT,Excl_Count,Rexcl_Count,Gaps,Result,Target,Report_Id,ReportType,Report_Quarter,Period_Start_Date,Period_End_Date,Root_Companies_Id,To_Target)
Select
		Provider_Id
		,PCP_NPI
		,PCP_NAME
		,Specialty
		,Practice_Name
		,Measure_id
		,Measure_Name
		,Measure_Title
		,MEASURE_SUBTITLE
		,Measure_Type
		,NUM_COUNT
		,DEN_COUNT
		,Excl_Count
		,Rexcl_Count
		,Gaps
		,Result
		,Target	
		,Report_Id
		,ReportType
		,Report_Quarter
		,Period_Start_Date
		,Period_End_Date
		,Root_Companies_Id
	,Case
		when Measure_id in('4','6','8','5') and Result>=Target Then 0
		when Measure_id in('4','6','8','5') and Result<Target Then CEILING(((Den_excl)*(cast(Target*0.01 as float))) - NUM_COUNT)
		/*
		when Measure_id in('5') and Result > Target then Floor((Den_excl*Target)/100)-NUM_COUNT
		when Measure_id in('5') and Result <= Target then 0
		*/
	end as To_Target
From
(
	Select 
		Provider_Id
		,PCP_NPI
		,PCP_NAME
		,Specialty
		,Practice_Name
		,Measure_id
		,Measure_Name
		,Measure_Title
		,MEASURE_SUBTITLE
		,Measure_Type
		,SUM(Cast(NUM_COUNT as INT)) as NUM_COUNT
		,SUM(Cast(DEN_COUNT as INT)) as DEN_COUNT
		,SUM(Cast(Excl_Count as INT)) as Excl_Count
		,Sum(Cast(Rexcl_count as INT)) as Rexcl_Count
		,Case
			when Measure_id in('4','6','8','5') then sum(Cast(DEN_Excl as INT)) - SUM(Cast(NUM_COUNT as INT)) 
			else SUM(Cast(NUM_COUNT as INT)) 
		end as Gaps
		,Case
			when SUM(Cast(DEN_Excl as Float))>0 Then Round((SUM(cast(NUM_COUNT as Float))/ISNULL(NULLIF(SUM(Cast(DEN_Excl as Float)),0),1))*100,2)
			else 0
		end as Result
		,Target	
		,Report_Id
		,ReportType
		,Report_Quarter
		,Period_Start_Date
		,Period_End_Date
		,Root_Companies_Id
		,sum(Den_excl) as DEN_EXCL
	From
	(
		Select distinct
		m.EMPI
		,a.NPI as Provider_id
		,a.NPI as PCP_NPI
		,a.Prov_Name as PCP_Name
		,a.Specialty
		,a.Practice as Practice_Name
		,m.Measure_id
		,m.Measure_Name
		,l.measure_Title
		,l.Measure_SubTitle
		,'Calculated' as Measure_Type
		,Case
			when NUM=1 and excl=0 and rexcl=0 Then 1
			else 0
		end as NUM_COUNT
		,DEN as DEN_COUNT
		,Case
			When DEN=1 and Excl=0 and Rexcl=0 Then 1
			else 0
		End as Den_excl
		,Excl as Excl_Count
		,Rexcl as Rexcl_count
		,Report_Id
		,l.ReportType
		,Report_Quarter
		,Period_Start_Date
		,Period_End_Date
		,m.Root_Companies_Id
		,l.Target
	From RPT.MEASURE_DETAILED_LINE m
	Join RFT.UHN_measuresList l on
		m.Measure_Id=l.measure_id
	Cross Apply
	(
		Select top 1 
			*
		From RPT.consolidatedattribution_snapshot s
		Where
			s.EMPI=m.EMPI
			and s.EnrollmentStatus='Active'
			and s.Attribution_Type='Ambulatory_PCP'
		Order by
			Attribution_Type
	)a	
	where 
		  a.NPI!='' and
		  m.MEASURE_ID in('4','5','6','8') and
		  REPORT_ID=@reportId 
			  
	)t1
	Group by Provider_Id,PCP_NPI,PCP_NAME,Specialty,Practice_Name,Measure_id,Measure_Name,Measure_Title,MEASURE_SUBTITLE,Report_Id,ReportType,Report_Quarter,Period_Start_Date,Period_End_Date,Root_Companies_Id,Measure_Type,target
)t2


/*Shanawaz - 09-22-2021
adding Logic to add A1c Open Gap Reasons for Gap Reporting*/

Delete from KPI_ENGINE_MART.RPT.QUALITY_GAP_DETAILS where Root_Companies_id=@rootId and Measureid='4';
	Insert into KPI_ENGINE_MART.RPT.QUALITY_GAP_DETAILS(EMPI,ROOT_COMPANIES_ID,MeasureId,Notagap,ImpactDate,Gap_Reason,AddDetails1,AddValue1,AddDetails2,AddValue2,AddDetails3,AddValue3,ReportId)
	Select
		EMPI,
		@rootId as ROOT_COMPANIES_ID,
		'4' as Measure_Id,
		0 as Notagap,
		@ce_enddt as ImpactDate,
		GapReason,
		Case	
			when GapReason='No HbA1c Result' Then 'A1c Test Date'
			when GapReason='Result>8' Then 'A1c Test Date'
		end as AddDetails1,
		Case	
			when GapReason='No HbA1c Result' Then ServiceDate
			when GapReason='Result>8' Then ServiceDate
		end as AddValue1,
		Case
			when GapReason='No HbA1c Result' Then 'Billing Provider'
		end as AddDetails2,
		Case
			when GapReason='No HbA1c Result' Then BillingProvider
		end as AddValue2,
		Case
			when GapReason='No HbA1c Result' Then 'Billing Provider TIN'
		end as AddDetails3,
		Case
			when GapReason='No HbA1c Result' Then BillingProviderTIN
		end as AddValue3,
		@reportId as ReportId
	From
	(
		select 
			ds.EMPI,
			rv.servicedate,
			Coalesce(rv.Code,rs.Code) as Code,
			rs.value,
			case
				when rv.servicedate is null then 'No HbA1c'
				when rs.servicedate is null then 'No HbA1c Result'
				--when cast(rs.value as float)>=cast(8.0 as float)  then 'Result>8'
				when cast(rs.value as float)>=cast(8.0 as float)  then 'Result=>8'
--Change Log: KPISQL-502 | 2021-11-03 | (SG) Changed the description of the gap to 'Result>=8' as requested by Stef
			end as GapReason,
			rv.BILL_PROV as BillingProviderNPI,
			rv.BILL_PROV_TIN as BillingProviderTIN,
			coalesce(NULLIF(concat(Provider_Last_Name,' ',Provider_First_Name),' '),Provider_organization_name) as BillingProvider,
			row_number() over(Partition by ds.EMPI order by rv.servicedate desc,rs.servicedate desc) as rn
		from #cdcdataset ds
		Left Outer join #cdc_hba1cmostrecentvisit rv on
			ds.EMPI=rv.EMPI 
		Left outer Join #cdc_hba1crslt rs on
			rv.EMPI=rs.EMPI and
			rs.servicedate between Dateadd(day,-7,rv.servicedate) and Dateadd(day,7,rv.servicedate)
		Left outer Join RFT_NPI n on 
			rv.BILL_PROV=n.NPI
		where
			ds.meas='CDC10' and Event=1 and excl=0 and rexcl=0 and rexcld=0 and num=0
			--and ds.EMPI='E58AADA0-2D58-4133-B86A-69CF334ED026'

	)t1
	Where
		rn=1 and GapReason is not null


Delete from KPI_ENGINE_MART.RPT.QUALITY_GAP_DETAILS where Root_Companies_id=@rootId and Measureid='5';
	Insert into KPI_ENGINE_MART.RPT.QUALITY_GAP_DETAILS(EMPI,ROOT_COMPANIES_ID,MeasureId,Notagap,ImpactDate,Gap_Reason,AddDetails1,AddValue1,AddDetails2,AddValue2,AddDetails3,AddValue3,ReportId)
	Select
		EMPI,
		@rootId as ROOT_COMPANIES_ID,
		'5' as Measure_Id,
		0 as Notagap,
		@ce_enddt as ImpactDate,
		GapReason,
		Case	
			when GapReason='No HbA1c Result' Then 'A1c Test Date'
			when GapReason='Result>9' Then 'A1c Test Date'
		end as AddDetails1,
		Case	
			when GapReason='No HbA1c Result' Then ServiceDate
			when GapReason='Result>9' Then ServiceDate
		end as AddValue1,
		Case
			when GapReason='No HbA1c Result' Then 'Billing Provider'
		end as AddDetails2,
		Case
			when GapReason='No HbA1c Result' Then BillingProvider
		end as AddValue2,
		Case
			when GapReason='No HbA1c Result' Then 'Billing Provider TIN'
		end as AddDetails3,
		Case
			when GapReason='No HbA1c Result' Then BillingProviderTIN
		end as AddValue3,
		@reportId as ReportId
	From
	(
		select 
			ds.EMPI,
			rv.servicedate,
			Coalesce(rv.Code,rs.Code) as Code,
			rs.value,
			case
				when rv.servicedate is null then 'No HbA1c'
				when rs.servicedate is null then 'No HbA1c Result'
				when cast(rs.value as float)>=cast(8.0 as float)  then 'Result>9'
				
			end as GapReason,
			rv.BILL_PROV as BillingProviderNPI,
			rv.BILL_PROV_TIN as BillingProviderTIN,
			coalesce(NULLIF(concat(Provider_Last_Name,' ',Provider_First_Name),' '),Provider_organization_name) as BillingProvider,
			row_number() over(Partition by ds.EMPI order by rv.servicedate desc,rs.servicedate desc) as rn
		from #cdcdataset ds
		Left Outer join #cdc_hba1cmostrecentvisit rv on
			ds.EMPI=rv.EMPI 
		Left outer Join #cdc_hba1crslt rs on
			rv.EMPI=rs.EMPI and
			rs.servicedate between Dateadd(day,-7,rv.servicedate) and Dateadd(day,7,rv.servicedate)
		Left outer Join RFT_NPI n on 
			rv.BILL_PROV=n.NPI
		where
			ds.meas='CDC2' and Event=1 and excl=0 and rexcl=0 and rexcld=0 and num=1
			--and ds.EMPI='E58AADA0-2D58-4133-B86A-69CF334ED026'

	)t1
	Where
		rn=1 and GapReason is not null



--/****** Added by Sameer Gawde - 2021-09-23
--for populating [KPI_ENGINE_MART].RPT.QUALITY_GAP_DETAILS table used for gap reporting  ******/
--/****** START ******/
--Commented this SP call and added it in the KPI_ENGINE.dbo.KPI_Scheduledjobs table
--EXEC [KPI_ENGINE_MART].RPT.SP_CDC4GapReport
--/****** FINISH ******/





		SET @PERF_ROW = @@ROWCOUNT
		SET @PERF_DURATION = DATEDIFF(MINUTE,@PERF_START,GETDATE());
		EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, 'Enrollment Completed', @PERF_ROW, @DURATION_IN_MIN=@PERF_DURATION, @LOG2ID=@LOGID;
		EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, @LOG2ID=@LOGID, @END_FLAG=1;

END TRY
BEGIN CATCH
		BEGIN
			EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, 'FATAL ERROR', @LOG2ID=@LOGID;
			RAISERROR ('',16,0)
			RETURN 16
        END
	END CATCH
RETURN 0
END


GO
