SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON





CREATE PROCEDURE [HDS].[RUN_HEDIS_IPU]  @meas_year nvarchar(4)
AS
-- Declare Variables

Declare @runid INT=0;
DECLARE @ce_startdt varchar(8);
DECLARE @ce_enddt varchar(8);
DECLARE @ce_startdt1 varchar(8);
DECLARE @ce_enddt1 varchar(8);
DECLARE @meas VARCHAR(10);

SET @meas='IPU';
SET @ce_startdt=concat(@meas_year,'0101');
SET @ce_enddt=concat(@meas_year,'1231');
SET @ce_startdt1=concat(@meas_year-1,'0101');
SET @ce_enddt1=concat(@meas_year-1,'1231');



-- Hospice Intervention

drop table if exists #ipu_hospicemembers;
CREATE table #ipu_hospicemembers
(
	Memid varchar(50)
		
);


Insert into #ipu_hospicemembers
select MemID from HEDIS_VISIT v	where Measure_id=@meas and HCFAPOS!=81 and Date_S!='' and Date_S between @ce_startdt and @ce_enddt
and 
(
	CPT in(select code from VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or
	HCPCS in(select code from VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or 
	REV in(select code from VALUESET_TO_CODE where Code_System='UBREV' and Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
)
	
CREATE CLUSTERED INDEX idx_ipu_hospicemembers ON #ipu_hospicemembers ([memid]);


-- Exclude discharges with a principal diagnosis of mental health or chemical dependency (Mental and Behavioral Disorders Value Set) on the discharge claim.


DROP TABLE IF EXISTS #ipu_MDBstaylist;
	
CREATE table #ipu_MDBstaylist
(
	Memid varchar(50),
	servicedate varchar(8),
	claimid INT
				
);


insert into #ipu_MDBstaylist
select distinct Memid,date_s as servicedate,claimid from HEDIS_VISIT where Measure_id=@meas and suppdata='N' and Date_S!='' and hcfapos!=81
and Diag_I_1 in(select code from VALUESET_TO_CODE where Value_Set_Name='Mental and Behavioral Disorders')


CREATE CLUSTERED INDEX idx_ipu_MDBstaylist ON #ipu_MDBstaylist ([memid],[claimid]);


--Exclude newborn care rendered from birth to discharge home from delivery 

DROP TABLE IF EXISTS #ipu_newbornstaylist;
	
CREATE table #ipu_newbornstaylist
(
	Memid varchar(50),
	servicedate varchar(8),
	claimid INT
				
);


insert into #ipu_newbornstaylist
select distinct Memid,date_s as servicedate,claimid from HEDIS_VISIT where Measure_id=@meas and suppdata='N' and Date_S!='' and hcfapos!=81
and Diag_I_1 in(select code from VALUESET_TO_CODE where Value_Set_Name='Deliveries Infant Record')



CREATE CLUSTERED INDEX idx_ipu_newbornstaylist ON #ipu_newbornstaylist ([memid],[claimid]);


-- Inpatient Stay List

DROP TABLE IF EXISTS #ipu_ipstaylist;
	
CREATE table #ipu_ipstaylist
(
	Memid varchar(50),
	servicedate varchar(8),
	admissiondate varchar(8),
	dischargedate varchar(8),
	claimid INT
				
);


insert into #ipu_ipstaylist
select Memid,date_s as servicedate,Date_Adm as admissiondate,Date_Disch as dischargedate,ClaimID from HEDIS_VISIT where MEASURE_ID=@meas and Date_Disch!='' and Date_Disch between @ce_startdt and @ce_enddt and HCFAPOS!=81 and suppdata='N' and rev in(select code from VALUESET_TO_CODE where Code_System='UBREV' and Value_Set_Name='Inpatient Stay') and ClaimStatus=1


CREATE CLUSTERED INDEX idx_ipu_ipstaylist ON #ipu_ipstaylist ([memid],[servicedate],[claimid]);

-- Non Acute Onpatient Staty List

DROP TABLE IF EXISTS #ipu_naipstaylist;
	
CREATE table #ipu_naipstaylist
(
	Memid varchar(50),
	claimid INT
				
);



insert into #ipu_naipstaylist
select Memid,claimid from HEDIS_VISIT where Measure_id=@meas and suppdata='N' and Date_Disch!='' and Date_Disch between @ce_startdt and @ce_enddt and hcfapos!=81
	and (REV in(select code from VALUESET_TO_CODE where code_system='UBREV' and Value_Set_Name='Nonacute Inpatient Stay') or RIGHT('0000'+CAST(Trim(BillType) AS VARCHAR(4)),4) in(select code from VALUESET_TO_CODE where code_system='UBTOB' and Value_Set_Name='Nonacute Inpatient Stay'))


CREATE CLUSTERED INDEX idx_ipu_naipstaylist ON #ipu_naipstaylist ([memid],[claimid]);


--Get Members with Only medicaid coverage on discharge date

Drop table if exists #ipu_MCDmemlist;
CREATE table #ipu_MCDmemlist
(
	Memid varchar(50),
	servicedate varchar(8),
	admissiondate varchar(8),
	dischargedate varchar(8),
	claimid INT,
	Payer varchar(10)
				
);

Insert into #ipu_MCDmemlist
select Memid,servicedate,admissiondate,dischargedate,claimid,payer from(
select ip.*,e.payer,e.StartDate,e.FinishDate,count(ip.memid) over(partition by ip.memid,ip.servicedate,ip.dischargedate) as payerct from #ipu_ipstaylist ip
join HEDIS_MEMBER_EN e on e.MemID=ip.Memid and e.MEASURE_ID=@meas and ip.dischargedate between e.StartDate and e.FinishDate
--where ip.memid=110437
)t1 where t1.payerct=1 and t1.payer in('MD','MLI','MRB')


CREATE CLUSTERED INDEX idx_ipu_MCDmemlist ON #ipu_MCDmemlist ([memid],[claimid]);

-- Acute Inpatient List with exclusions

DROP TABLE IF EXISTS #ipu_acuteipstaylist;
	
CREATE table #ipu_acuteipstaylist
(
	Memid varchar(50),
	servicedate varchar(8),
	admissiondate varchar(8),
	dischargedate varchar(8),
	claimid INT,
	payer varchar(10)
				
);


insert into #ipu_acuteipstaylist
select t1.Memid,t1.servicedate,t1.admissiondate,t1.dischargedate,t1.claimid,t1.payer from #ipu_MCDmemlist t1
left outer join #ipu_naipstaylist t2 on t1.memid=t2.memid and t1.claimid=t2.claimid
left outer join #ipu_MDBstaylist t3 on t1.memid=t3.Memid and t1.claimid=t3.claimid
left outer join #ipu_newbornstaylist t4 on t1.memid=t4.Memid and t1.claimid=t4.claimid
left outer join #ipu_hospicemembers t5 on t1.Memid=t5.Memid
where t2.memid is null and t3.memid is null and t4.memid is null and t5.Memid is null

CREATE CLUSTERED INDEX idx_ipu_acuteipstaylist ON #ipu_acuteipstaylist ([memid],[servicedate],[claimid]);

-- Total Acute Inpatient score output

DROP TABLE IF EXISTS #ipu_totalscore;
	
CREATE table #ipu_totalscore
(
	Memid varchar(50),
	meas varchar(10),
	payer varchar(8),
	"Proc" INT,
	LOS INT,
	MM INT,	
	age INT,
	gender varchar(4)	
				
);

-- Get data in output format
insert into #ipu_totalscore
select t1.Memid,'IPUT' as meas,t1.Payer,count(t1.memid) over(partition by t1.Memid,t1.dischargedate) as "Proc",ISNULL(NULLIF(datediff(day,t1.admissiondate,t1.dischargedate),0),1) as LOS,0 as MM,CASE WHEN
 convert(varchar,cast(DATEADD(year,DATEDIFF(year, d.dob  ,t1.dischargedate) , d.dob) as date),112)> t1.dischargedate
THEN DATEDIFF(year, d.dob  ,t1.dischargedate) -1
ELSE DATEDIFF(year, d.dob  ,t1.dischargedate)
END as age,d.Gender from #ipu_acuteipstaylist t1
join HEDIS_MEMBER_GM d on d.memid=t1.Memid and d.MEASURE_ID=@meas
order by 1




-- Members with Maternity Acute IP
DROP TABLE IF EXISTS #ipu_maternitylist;
	
CREATE table #ipu_maternitylist
(
	Memid varchar(50),
	claimid INT
				
);

Insert into #ipu_maternitylist
select distinct Memid,claimid from HEDIS_VISIT where Measure_id=@meas and Date_S!='' and suppdata='N' and HCFAPOS!=81
and claimstatus=1 and
(
		Diag_I_1 in(select code from VALUESET_TO_CODE where Value_Set_Name='Maternity Diagnosis')
		or 
		REV in(select code from VALUESET_TO_CODE where Code_System='UBREV' and Value_Set_Name='Maternity')
		or 
		RIGHT('0000'+CAST(Trim(BillType) AS VARCHAR(4)),4) in(select code from VALUESET_TO_CODE where Code_System='UBTOB' and Value_Set_Name='Maternity')
	)

CREATE CLUSTERED INDEX idx_ipu_maternitylist ON #ipu_maternitylist ([memid],[claimid]);



DROP TABLE IF EXISTS #ipu_maternityscore;
	
CREATE table #ipu_maternityscore
(
	Memid varchar(50),
	meas varchar(10),
	payer varchar(8),
	"Proc" INT,
	LOS INT,
	MM INT,	
	age INT,
	gender varchar(4)	
				
);

-- Get data in output format
insert into #ipu_maternityscore
select t1.Memid,'IPUMAT' as meas,t1.Payer,count(t1.memid) over(partition by t1.Memid,t1.dischargedate) as "Proc",ISNULL(NULLIF(datediff(day,t1.admissiondate,t1.dischargedate),0),1) as LOS,0 as MM,CASE WHEN
 convert(varchar,cast(DATEADD(year,DATEDIFF(year, d.dob  ,t1.dischargedate) , d.dob) as date),112)> t1.dischargedate
THEN DATEDIFF(year, d.dob  ,t1.dischargedate) -1
ELSE DATEDIFF(year, d.dob  ,t1.dischargedate)
END as age,d.Gender from #ipu_acuteipstaylist t1
join #ipu_maternitylist m on m.Memid=t1.Memid and m.claimid=t1.claimid
join HEDIS_MEMBER_GM d on d.memid=t1.Memid and d.MEASURE_ID=@meas and d.Gender='F'
order by 1


CREATE CLUSTERED INDEX idx_ipu_maternityscore ON #ipu_maternityscore ([memid],[payer],[meas]);


-- Surgery


-- Create Members with Surgery Acute IP Stay

DROP TABLE IF EXISTS #ipu_surgerylist;
	
CREATE table #ipu_surgerylist
(
	Memid varchar(50),
	servicedate varchar(8),
	claimid INT
				
);

Insert into #ipu_surgerylist
select distinct Memid,date_s as servicedate,claimid from HEDIS_VISIT where Measure_id=@meas and Date_S!='' and suppdata='N' and HCFAPOS!=81
and claimstatus=1 and	REV in(select code from VALUESET_TO_CODE where Code_System='UBREV' and Value_Set_Name='Surgery')
		

CREATE CLUSTERED INDEX idx_ipu_surgerylist ON #ipu_surgerylist ([memid],[servicedate],[claimid]);


DROP TABLE IF EXISTS #ipu_surgeryscore;
-- Get data in output format	
CREATE table #ipu_surgeryscore
(
	Memid varchar(50),
	meas varchar(10),
	payer varchar(8),
	"Proc" INT,
	LOS INT,
	MM INT,	
	age INT,
	gender varchar(4)	
				
);

insert into #ipu_surgeryscore
select t1.Memid,'IPUS' as meas,t1.Payer,count(t1.memid) over(partition by t1.Memid,t1.dischargedate) as "Proc",ISNULL(NULLIF(datediff(day,t1.admissiondate,t1.dischargedate),0),1) as LOS,0 as MM,CASE WHEN
 convert(varchar,cast(DATEADD(year,DATEDIFF(year, d.dob  ,t1.dischargedate) , d.dob) as date),112)> t1.dischargedate
THEN DATEDIFF(year, d.dob  ,t1.dischargedate) -1
ELSE DATEDIFF(year, d.dob  ,t1.dischargedate)
END as age,d.Gender from #ipu_acuteipstaylist t1
join #ipu_surgerylist s on s.Memid=t1.Memid and s.claimid=t1.claimid
join HEDIS_MEMBER_GM d on d.memid=t1.Memid and d.MEASURE_ID=@meas
left outer join #ipu_maternityscore m on t1.Memid=m.Memid 
where m.Memid is null
order by 1


CREATE CLUSTERED INDEX idx_ipu_surgeryscore ON #ipu_surgeryscore ([memid],[payer],[meas]);

-- Medicine

DROP TABLE IF EXISTS #ipu_medicinescore;
	
CREATE table #ipu_medicinescore
(
	Memid varchar(50),
	meas varchar(10),
	payer varchar(8),
	"Proc" INT,
	LOS INT,
	MM INT,	
	age INT,
	gender varchar(4)	
				
);

insert into #ipu_medicinescore
select t1.Memid,'IPUM' as meas,t1.Payer,count(t1.memid) over(partition by t1.Memid,t1.dischargedate) as "Proc",ISNULL(NULLIF(datediff(day,t1.admissiondate,t1.dischargedate),0),1) as LOS,0 as MM,CASE WHEN
 convert(varchar,cast(DATEADD(year,DATEDIFF(year, d.dob  ,t1.dischargedate) , d.dob) as date),112)> t1.dischargedate
THEN DATEDIFF(year, d.dob  ,t1.dischargedate) -1
ELSE DATEDIFF(year, d.dob  ,t1.dischargedate)
END as age,d.Gender from #ipu_acuteipstaylist t1
join HEDIS_MEMBER_GM d on d.memid=t1.Memid and d.MEASURE_ID=@meas
left outer join #ipu_surgerylist s on s.Memid=t1.Memid 
left outer join #ipu_maternityscore m on t1.Memid=m.Memid 
where m.Memid is null and s.memid is null
order by 1


CREATE CLUSTERED INDEX idx_ipu_medicinescore ON #ipu_medicinescore ([memid],[payer],[meas]);



--Generate Output

-- Generate Output
	select @runid=max(RUN_ID) from HEDIS_MEASURE_OUTPUT_UTL where Measure_id=@meas;

	if(@runid>=1)
	Begin
		SET @runid=@runid+1;
	End
	Else
	Begin
		SET @runid=1;
	END
	

Insert into HEDIS_MEASURE_OUTPUT_UTL(Memid,Meas,Payer,"Proc",LOS,MM,Age,Gender,Measure_id,Measurement_year,RUN_ID)
select *,@meas,@meas_year,@runid from(
select * from #ipu_totalscore
Union all
select * from  #ipu_maternityscore
Union all
select * from  #ipu_surgeryscore
Union all
select * from  #ipu_medicinescore
)t


GO
