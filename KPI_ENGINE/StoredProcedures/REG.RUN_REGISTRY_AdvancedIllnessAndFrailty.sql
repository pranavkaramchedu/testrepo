SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
CREATE Proc [REG].[RUN_REGISTRY_AdvancedIllnessAndFrailty]
As
BEGIN
DECLARE @INFO VARCHAR(8000)
					,	@TRUE BIT = 1
					,	@FALSE BIT = 0
					,	@DB_ID INT = DB_ID()
					,	@LOGID INT = 0
					,	@PROC VARCHAR(500)=OBJECT_SCHEMA_NAME(@@PROCID)+'.'+OBJECT_NAME(@@PROCID)
					,	@PERF_START DATETIME
					,	@PERF_DURATION INT
					,	@PERF_ROW INT
					,	@RC	INT
					;

		EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, @INFO=@INFO, @LOG2ID=@LOGID OUT;
		DECLARE @PROCFULLNAME VARCHAR(500) = DB_NAME()+'.'+@PROC;
		----EXEC dbo.SP_MI_UTIL_SEND_MAIL @PROCFULLNAME, 'STARTED', 1, @ECHO=0; 
	DECLARE @MSG VARCHAR(1000)
	DECLARE @PERF_START_PROC DATETIME 
	DECLARE @PERF_RPS NUMERIC(10,2) 
	DECLARE @ROWS INT 
	DECLARE @PERF_ROWS INT 
	SET @PERF_ROWS = 0 
	SET @PERF_START = GETDATE() 
	SET @PERF_START_PROC = GETDATE() 

BEGIN TRY



declare @rundate Date=GetDate();
declare @meas_year varchar(4)=Year(Dateadd(month,-2,@rundate))
declare @rootId INT=159
declare @attributiondate Date=DATEADD(month, DATEDIFF(month, 0, Dateadd(month,-2,@rundate)), 0)

Declare @startDate Date;
Declare @enddate date;
Declare @quarter varchar(20)
Declare @reportId INT;


-- Get ReportId from Report_Details

	exec GetReportDetail @rundate=@rundate,@rootId=@rootId,@startDate=@startDate output,@enddate=@enddate output,@quarter=@quarter output,@reportId=@reportId output



-- Creating Temp table for Members with Advanced Illness visits in last 2 years
Drop table if exists #advancedillness;
Create table #advancedillness
(
	EMPI varchar(100),
	Member_id varchar(100),
	Claim_id varchar(100),
	ServiceDate date,
	DiagnosisCode varchar(20),
	AttendingProviderNPI varchar(20),
	AttendingProviderName varchar(200),
	AttendingProviderSpecialty varchar(100),
	DiagnosisDataSource varchar(20)

)

Insert into #advancedillness
Select 
	EMPI
	,MEMBER_ID
	,CLAIM_ID
	,DIAG_START_DATE
	,DIAG_CODE
	,ATT_NPI
	,AttendingProviderName
	,AttendingProviderSpecialty
	,CL_DATA_SRC
from
(

	Select 
		t1.EMPI
		,t1.MEMBER_ID
		,CLAIM_ID
		,DIAG_START_DATE
		,DIAG_CODE
		,ATT_NPI
		,AttendingProviderName
		,ATT_PROV_SPEC
		,PrimarySpecialtyCode
		,Case  
			when ATT_PROV_SPEC is null then PreferredConceptName 
			else ATT_PROV_SPEC 
		end as AttendingProviderSpecialty
		,CL_DATA_SRC
	from(
	select distinct 
		c.EMPI
		,c.Member_id
		,c.CLAIM_ID
		,d.diag_start_date
		,d.DIAG_CODE
		,c.ATT_NPI
		,case 
			when NPI is not null then CONCAT(n.Provider_Last_Name,',',n.Provider_First_Name,' ',n.Provider_Middle_Name) 
			else c.ATT_PROV_NAME 
		end as AttendingProviderName
		,c.ATT_PROV_SPEC
		,Case 
			when n.Healthcare_provider_primary_Taxonomy_switch_1='Y' then n.Healthcare_provider_Taxonomy_code_1
			when n.Healthcare_provider_primary_Taxonomy_switch_2='Y' then n.Healthcare_provider_Taxonomy_code_2
			when n.Healthcare_provider_primary_Taxonomy_switch_3='Y' then n.Healthcare_provider_Taxonomy_code_3
			when n.Healthcare_provider_primary_Taxonomy_switch_4='Y' then n.Healthcare_provider_Taxonomy_code_4
			when n.Healthcare_provider_primary_Taxonomy_switch_5='Y' then n.Healthcare_provider_Taxonomy_code_5
		end as PrimarySpecialtyCode
		,c.CL_DATA_SRC 
	from KPI_ENGINE.dbo.CLAIMLINE c
	join KPI_ENGINE.dbo.DIAGNOSIS d on c.ROOT_COMPANIES_ID=d.ROOT_COMPANIES_ID and 
										c.CL_DATA_SRC=d.DIAG_DATA_SRC and 
										c.CLAIM_ID=d.CLAIM_ID
	left outer join KPI_ENGINE.dbo.RFT_NPI n on c.ATT_NPI=n.NPI
	where diag_Code in
	(
		select Replace(code,'.','') from KPI_ENGINE.HDS.Valueset_to_code where value_set_name = 'Advanced Illness'
	) 
	and
	ISNULL(c.POS,'')!='81' and
	c.ROOT_COMPANIES_ID=@rootId
	and 
	DIAG_START_DATE between  DATEADD(month, DATEDIFF(month, 0, dateadd(month,-23,@enddate)), 0) and @enddate 
	and 
	(n.Entity_Type_code='1' or n.Entity_Type_code is null)
	)t1
	join KPI_ENGINE.RPT.PCP_ATTRIBUTION a on t1.EMPI=a.EMPI  and 
											 a.AssignedStatus='Assigned' and 
											 a.ReportId=@reportId
	left outer join KPI_ENGINE.dbo.RFT_TAXONOMIES t on t1.PrimarySpecialtyCode=t.ConceptCode
)t2



--Creating Temp table for Members with  Frailty diagnosis  in last 1 year

Drop table if exists #FrailtyMembers;
Create table #FrailtyMembers
(
	EMPI varchar(100),
	Member_id varchar(100),
	Claim_id varchar(100),
	ServiceDate date,
	DiagnosisCode varchar(20),
	AttendingProviderNPI varchar(20),
	AttendingProviderName varchar(200),
	AttendingProviderSpecialty varchar(100),
	DiagnosisDataSource varchar(20)

)
Insert into #FrailtyMembers
Select 
	EMPI
	,MEMBER_ID
	,CLAIM_ID
	,ServiceDate
	,DiagnosisCode
	,AttendingProviderNPI
	,AttendingProviderName
	,AttendingProviderSpecialty
	,DiagnosisDataSource
From
(
	select distinct 
		t1.EMPI
		,t1.MEMBER_ID
		,t1.CLAIM_ID
		,Code as DiagnosisCode
		,ServiceDate
		,AttendingProviderName
		,AttendingProviderNPI
		,Case  
			when AttendingProviderSpecialty is null then PreferredConceptName 
			else AttendingProviderSpecialty 
		end as AttendingProviderSpecialty
		,DiagnosisDataSource
	from
	(
		select 
			c.EMPI
			,c.Member_id
			,c.CLAIM_ID
			,p.PROC_START_DATE as ServiceDate
			,p.PROC_CODE as Code
			,c.ATT_NPI as AttendingProviderNPI
			,case 
				when NPI is not null then CONCAT(n.Provider_Last_Name,',',n.Provider_First_Name,' ',n.Provider_Middle_Name) 
				else c.ATT_PROV_NAME 
			end as AttendingProviderName
			,c.ATT_PROV_SPEC as AttendingProviderSpecialty
			,Case 
				when n.Healthcare_provider_primary_Taxonomy_switch_1='Y' then n.Healthcare_provider_Taxonomy_code_1
				when n.Healthcare_provider_primary_Taxonomy_switch_2='Y' then n.Healthcare_provider_Taxonomy_code_2
				when n.Healthcare_provider_primary_Taxonomy_switch_3='Y' then n.Healthcare_provider_Taxonomy_code_3
				when n.Healthcare_provider_primary_Taxonomy_switch_4='Y' then n.Healthcare_provider_Taxonomy_code_4
				when n.Healthcare_provider_primary_Taxonomy_switch_5='Y' then n.Healthcare_provider_Taxonomy_code_5
			end as PrimarySpecialtyCode
			,c.CL_DATA_SRC as DiagnosisDataSource
		from KPI_ENGINE.dbo.CLAIMLINE c
		Join KPI_ENGINE.dbo.PROCEDURES p on c.CLAIM_ID=p.CLAIM_ID and 
											c.CL_DATA_SRC=p.PROC_DATA_SRC and 
											c.SV_LINE=p.SV_LINE and 
											c.ROOT_COMPANIES_ID=p.ROOT_COMPANIES_ID

		left outer join KPI_ENGINE.dbo.RFT_NPI n on c.ATT_NPI=n.NPI
		where ISNULL(POS,'')!='81'  and c.ROOT_COMPANIES_ID=@rootId and
			  PROC_START_DATE!='' and  
			  PROC_START_DATE  between DATEADD(month, DATEDIFF(month, 0, dateadd(month,-11,@enddate)), 0) and @enddate
			  and 
			  p.PROC_CODE in
			  (
					select code from KPI_ENGINE.HDS.VALUESET_TO_CODE where Value_Set_Name in('Frailty Device','Frailty Encounter')
			  ) 
			  and 
		      (n.Entity_Type_code='1' or n.Entity_Type_code is null)	
					
		Union all

		select  
			c.EMPI
			,c.Member_id
			,c.CLAIM_ID
			,d.diag_start_date as ServiceDate
			,d.DIAG_CODE as Code
			,c.ATT_NPI as AttendingProviderNPI
			,case 
				when NPI is not null then CONCAT(n.Provider_Last_Name,',',n.Provider_First_Name,' ',n.Provider_Middle_Name) 
				else c.ATT_PROV_NAME 
			end as AttendingProviderName
			,c.ATT_PROV_SPEC as AttendingProviderSpecialty
			,Case 
				when n.Healthcare_provider_primary_Taxonomy_switch_1='Y' then n.Healthcare_provider_Taxonomy_code_1
				when n.Healthcare_provider_primary_Taxonomy_switch_2='Y' then n.Healthcare_provider_Taxonomy_code_2
				when n.Healthcare_provider_primary_Taxonomy_switch_3='Y' then n.Healthcare_provider_Taxonomy_code_3
				when n.Healthcare_provider_primary_Taxonomy_switch_4='Y' then n.Healthcare_provider_Taxonomy_code_4
				when n.Healthcare_provider_primary_Taxonomy_switch_5='Y' then n.Healthcare_provider_Taxonomy_code_5
			end as PrimarySpecialtyCode
			,c.CL_DATA_SRC as DiagnosisDataSource
		from KPI_ENGINE.dbo.CLAIMLINE c
		join KPI_ENGINE.dbo.DIAGNOSIS d on c.ROOT_COMPANIES_ID=d.ROOT_COMPANIES_ID and
											c.CL_DATA_SRC=d.DIAG_DATA_SRC and 
											c.CLAIM_ID=d.CLAIM_ID
		left outer join KPI_ENGINE.dbo.RFT_NPI n on c.ATT_NPI=n.NPI
		where diag_start_date!='' and 
			  c.ROOT_COMPANIES_ID=@rootId and
			  diag_start_date between DATEADD(month, DATEDIFF(month, 0, dateadd(month,-11,@enddate)), 0) and @enddate
			  and 
			  diag_Code in
			  (
				select Replace(code,'.','') from KPI_ENGINE.HDS.Valueset_to_code where value_set_name in('Frailty Symptom','Frailty Diagnosis')
			  ) 
			  and 
			  ISNULL(c.POS,'')!='81'  
			  and 
			  (n.Entity_Type_code='1' or n.Entity_Type_code is null)
		)t1
		left outer join KPI_ENGINE.dbo.RFT_TAXONOMIES t on t1.PrimarySpecialtyCode=t.ConceptCode
			
	)t2


-- Identify Members with Inpatient Stay in last 2 years
Drop table if exists #inpatientStays;
Create Table #inpatientStays
(
	EMPI varchar(100),
	Member_id varchar(100),
	ServiceDate Date,
	Claim_id varchar(100),
	CL_DATA_SRC varchar(20)
	
)
Insert into #inpatientStays
select distinct 
	EMPI
	,Member_id
	,FROM_DATE
	,Claim_id
	,CL_DATA_SRC 
from KPI_ENGINE.dbo.CLAIMLINE v 
where ISNULL(v.POS,'')!='81'  and 
	  v.DIS_DATE!='' and 
	  coalesce(v.DIS_DATE,v.TO_DATE) between DATEADD(month, DATEDIFF(month, 0, dateadd(month,-23,@enddate)), 0) and @enddate and 
	  RIGHT('0000'+CAST(Trim(v.REV_CODE) AS VARCHAR(4)),4) in (select code from KPI_ENGINE.HDS.VALUESET_TO_CODE where code_system='UBREV' and Value_Set_Name in('Inpatient Stay')) and 
	  ROOT_COMPANIES_ID=@rootId


-- Identify Non Acute Inpatient Stays in last 2 years
Drop table if exists #nonacutestays
Create Table #nonacutestays
(
	EMPI varchar(100),
	Member_id varchar(100),
	ServiceDate Date,
	Claim_id varchar(100),
	CL_DATA_SRC varchar(20)
	
)
Insert into #nonacutestays
select distinct 
	EMPI
	,Member_id
	,FROM_DATE
	,Claim_id
	,CL_DATA_SRC 
from KPI_ENGINE.dbo.CLAIMLINE 
where ISNULL(POS,'')!='81' and 
	  ROOT_COMPANIES_ID=@rootId and
	  DIS_DATE!='' and 
	  coalesce(DIS_DATE,TO_DATE) between DATEADD(month, DATEDIFF(month, 0, dateadd(month,-23,@enddate)), 0) and @enddate and 
	  (
		REV_CODE in 
		(
			select code from KPI_ENGINE.HDS.VALUESET_TO_CODE where code_system='UBREV'and Value_Set_Name in('Nonacute Inpatient Stay')
		) 
		or 
		RIGHT('0000'+CAST(Trim(UB_BILL_TYPE) AS VARCHAR(4)),4) in 
		(
			select code from KPI_ENGINE.HDS.VALUESET_TO_CODE where code_system='UBTOB' and Value_Set_Name in('Nonacute Inpatient Stay')
		)
	 )

-- Identify Observation,ED,telephone and Online Asseeements in last 2 years
Drop table if exists #ambulatoryvisits
Create Table #ambulatoryvisits
(
	EMPI varchar(100),
	Member_id varchar(100),
	ServiceDate Date,
	Claim_id varchar(100),
	CL_DATA_SRC varchar(20)
	
)
Insert into #ambulatoryvisits
select distinct 
	c.EMPI
	,c.Member_id
	,FROM_DATE
	,c.CLAIM_ID
	,c.CL_DATA_SRC 
from KPI_ENGINE.dbo.CLAIMLINE c
Join KPI_ENGINE.dbo.PROCEDURES p on c.CLAIM_ID=p.CLAIM_ID and 
									c.CL_DATA_SRC=p.PROC_DATA_SRC and 
									c.SV_LINE=p.SV_LINE and
									c.ROOT_COMPANIES_ID=p.ROOT_COMPANIES_ID
where ISNULL(POS,'')!='81' and 
	  c.ROOT_COMPANIES_ID=@rootId and
	  FROM_DATE!='' and  
	  FROM_DATE between DATEADD(month, DATEDIFF(month, 0, dateadd(month,-23,@enddate)), 0) and @enddate
	  and 
	  (
		p.PROC_CODE in
		(
			select code from KPI_ENGINE.HDS.VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments','Nonacute Inpatient')
		)
		or
		c.REV_CODE in
		(
			select code from KPI_ENGINE.HDS.VALUESET_TO_CODE where Code_System='UBREV' and Value_Set_Name in('Outpatient','ED')
		)
	  )


-- Identify Members with Acute Inpatient Visit
Drop table if exists #acuteipvsts
Create Table #acuteipvsts
(
	EMPI varchar(100),
	Member_id varchar(100),
	ServiceDate Date,
	Claim_id varchar(100),
	CL_DATA_SRC varchar(20)
	
)
Insert into #acuteipvsts
select distinct 
	C.EMPI
	,c.Member_id
	,FROM_DATE
	,c.CLAIM_ID
	,c.CL_DATA_SRC 
from KPI_ENGINE.dbo.CLAIMLINE c
Join KPI_ENGINE.dbo.PROCEDURES p on c.CLAIM_ID=p.CLAIM_ID and 
									c.CL_DATA_SRC=p.PROC_DATA_SRC and 
									c.SV_LINE=p.SV_LINE and 
									c.ROOT_COMPANIES_ID=p.ROOT_COMPANIES_ID
where ISNULL(POS,'')!='81'  and 
	  c.ROOT_COMPANIES_ID=@rootId and
	  FROM_DATE!='' and  
	  FROM_DATE between DATEADD(month, DATEDIFF(month, 0, dateadd(month,-23,@enddate)), 0) and @enddate
	  and 
	  p.PROC_CODE in
	  (
		select code from KPI_ENGINE.HDS.VALUESET_TO_CODE where Value_Set_Name='Acute Inpatient'
	  )


-- Identify Advanced Illness Diagnosis in specified settings

Drop table if exists #advancedillnesswithvsts;
Create table #advancedillnesswithvsts
(
	EMPI varchar(100),
	Member_id varchar(100),
	Claim_id varchar(100),
	ServiceDate date,
	DiagnosisCode varchar(20),
	AttendingProviderNPI varchar(20),
	AttendingProviderName varchar(200),
	AttendingProviderSpecialty varchar(100),
	DiagnosisDataSource varchar(20)

)
Insert into #advancedillnesswithvsts
Select distinct * from(

Select * from #advancedillness
where EMPI in(
	Select t3.EMPI from(
	select distinct t2.EMPI,t2.ServiceDate
	from(
		select distinct a.* from(
				select EMPI,Member_id,ServiceDate,Claim_id,CL_DATA_SRC  from #ambulatoryvisits 
				union all
				select na.EMPI,na.Member_id,na.ServiceDate,na.Claim_id,na.CL_DATA_SRC from #nonacutestays na
				join #inpatientStays inp on na.Member_id=inp.Member_id and na.Claim_id=inp.Claim_id and na.CL_DATA_SRC=inp.CL_DATA_SRC
		)t1
		Join #advancedillness a on a.Member_id=t1.Member_id and a.Claim_id=t1.Claim_id and a.DiagnosisDataSource=t1.CL_DATA_SRC
	)t2 
	)t3 group by EMPI having count(*)>1

)

Union all
-- Members with Acute Inpatient Encounters and Advanced Illness


select ai.* from #acuteipvsts a
join #advancedillness ai on a.Member_id=ai.Member_id and a.Claim_id=ai.Claim_id and a.CL_DATA_SRC=ai.DiagnosisDataSource


Union all

-- Members with Acute Inpatient Stays and Advanced Illness
select a.* from(
	select inp.Member_id,inp.ServiceDate,inp.Claim_id,inp.CL_DATA_SRC from #inpatientStays inp
		left outer join #nonacutestays na on inp.Member_id=na.Member_id and inp.Claim_id=na.Claim_id and inp.CL_DATA_SRC=na.CL_DATA_SRC
		where na.Member_id is null
)t1
join #advancedillness a on a.Member_id=t1.Member_id and a.Claim_id=t1.Claim_id and a.DiagnosisDataSource=t1.CL_DATA_SRC

)advill



-- Insert or delete the report if it exists

	
		
		Delete from KPI_ENGINE.REG.REGISTRY_OUTPUT where ReportId=@reportId and RegistryName='Advanced Illness And Frailty' and ROOT_COMPANIES_ID=@rootId;
		Delete from KPI_ENGINE.REG.REGISTRY_OUTPUT where ReportId=@reportId and RegistryName='Advanced Illness Opportunities' and ROOT_COMPANIES_ID=@rootId;;
		Delete from KPI_ENGINE.REG.REGISTRY_OUTPUT where ReportId=@reportId and RegistryName='Frailty Opportunities' and ROOT_COMPANIES_ID=@rootId;;
		Delete from KPI_ENGINE.REG.REGISTRY_OUTPUT where ReportId=@reportId and RegistryName='Advanced Illness Opportunities - Single Diagnosis' and ROOT_COMPANIES_ID=@rootId;;
		Delete from KPI_ENGINE.REG.REGISTRY_OUTPUT_LINE where ReportId=@reportId and RegistryName='Advanced Illness' and ROOT_COMPANIES_ID=@rootId;;
		Delete from KPI_ENGINE.REG.REGISTRY_OUTPUT_LINE where ReportId=@reportId and RegistryName='Frailty' and ROOT_COMPANIES_ID=@rootId;;
		Delete from KPI_ENGINE.REG.REGISTRY_OUTPUT_LINE where ReportId=@reportId and RegistryName='Advanced Illness Opportunities' and ROOT_COMPANIES_ID=@rootId;;
		Delete from KPI_ENGINE.REG.REGISTRY_OUTPUT_LINE where ReportId=@reportId and RegistryName='Frailty Opportunities' and ROOT_COMPANIES_ID=@rootId;;
		Delete from KPI_ENGINE.REG.REGISTRY_OUTPUT_LINE where ReportId=@reportId and RegistryName='Advanced Illness Opportunities - Single Diagnosis' and ROOT_COMPANIES_ID=@rootId;;


		


-- Members with Advanced Illness and Frailty


Insert into KPI_ENGINE.REG.REGISTRY_OUTPUT_LINE(RegistryName,EMPI,Payer,PayerId,MemberLastName,MemberFirstName,MemberMiddleName,MemberDOB,MEM_GENDER,EnrollmentStatus,PracticeName,AttributedProviderName,AttributedProviderSpecialty,AttributedProviderNPI,DiagnosisCode,ServiceDate,AttendingProviderName,AttendingProviderNPI,AttendingProviderSpecialty,DiagnosisDataSource,ROOT_COMPANIES_ID,ReportId)
Select 
	'Advanced Illness' as RegistryName
	,t3.EMPI
	,a.Data_source as Payer
	,a.PayerId
	,a.MemberLastName
	,a.MemberFirstName
	,a.MemberMiddleName
	,a.MemberDOB
	,a.MEM_GENDER
	,a.EnrollmentStatus
	,a.AmbulatoryPCPPractice as PracticeName
	,a.AmbulatoryPCPName as AttributedProviderName
	,a.AmbulatoryPCPSpecialty as AttributedProviderSpecialty
	,a.AmbulatoryPCPNPI as AttributedProviderNPI
	,DiagnosisCode
	,ServiceDate
	,AttendingProviderName
	,AttendingProviderNPI
	,AttendingProviderSpecialty
	,DiagnosisDataSource
	,@rootId as ROOT_COMPANIES_ID
	,@reportId
from(
	select
		EMPI
		,MEMBER_ID
		,ServiceDate
		,DiagnosisCode
		,AttendingProviderNPI
		,AttendingProviderName
		,AttendingProviderSpecialty
		,DiagnosisDataSource 
	from(
		select 
			*
			,DENSE_RANK() over(partition by EMPI order by ServiceDate Desc,AttendingProviderNPI,rn Desc) as rnk 
		from(
			select
				a.EMPI
				,a.Member_id
				,a.ServiceDate
				,a.DiagnosisCode
				,a.AttendingProviderNPI
				,a.AttendingProviderName
				,a.AttendingProviderSpecialty
				,a.DiagnosisDataSource 
				,ROW_NUMBER() over(partition by a.EMPI order by a.ServiceDate Desc) as rn
			from #advancedillnesswithvsts a
			join #FrailtyMembers f on a.EMPI=f.EMPI
		)t1
	)t2
)t3
join KPI_ENGINE.RPT.PCP_ATTRIBUTION a on t3.EMPI=a.EMPI  and 
									a.AssignedStatus='Assigned' and 
									a.ReportId=@reportId


-- Frailty Output in Line

Insert into KPI_ENGINE.REG.REGISTRY_OUTPUT_LINE(RegistryName,EMPI,Payer,PayerId,MemberLastName,MemberFirstName,MemberMiddleName,MemberDOB,MEM_GENDER,EnrollmentStatus,PracticeName,AttributedProviderName,AttributedProviderSpecialty,AttributedProviderNPI,DiagnosisCode,ServiceDate,AttendingProviderName,AttendingProviderNPI,AttendingProviderSpecialty,DiagnosisDataSource,ROOT_COMPANIES_ID,ReportId)
Select 'Frailty' as RegistryName,t3.EMPI,a.Data_source as Payer,a.PayerId,a.MemberLastName,a.MemberFirstName,a.MemberMiddleName,a.MemberDOB,a.MEM_GENDER,a.EnrollmentStatus,a.AmbulatoryPCPPractice as PracticeName,a.AmbulatoryPCPName as AttributedProviderName,a.AmbulatoryPCPSpecialty as AttributedProviderSpecialty,a.AmbulatoryPCPNPI as AttributedProviderNPI,DiagnosisCode,ServiceDate,AttendingProviderName,AttendingProviderNPI,AttendingProviderSpecialty,DiagnosisDataSource,@rootId as ROOT_COMPANIES_ID,@reportId
from(
select EMPI,MEMBER_ID,ServiceDate,DiagnosisCode,AttendingProviderNPI,AttendingProviderName,AttendingProviderSpecialty,DiagnosisDataSource from(
select *,DENSE_RANK() over(partition by EMPI order by ServiceDate Desc,AttendingProviderNPI) as rnk from(
select distinct a.EMPI,a.Member_id,a.ServiceDate,a.DiagnosisCode,a.AttendingProviderNPI,a.AttendingProviderName,a.AttendingProviderSpecialty,a.DiagnosisDataSource from #FrailtyMembers a
join #advancedillnesswithvsts f on a.EMPI=f.EMPI
)t1
)t2
)t3
join KPI_ENGINE.RPT.PCP_ATTRIBUTION a on t3.EMPI=a.EMPI  and a.AssignedStatus='Assigned' and a.ReportId=@reportId


 -- Advanced Illness  and Frailty
Insert into KPI_ENGINE.REG.REGISTRY_OUTPUT(RegistryName,EMPI,Payer,PayerId,MemberLastName,MemberFirstName,MemberMiddleName,MemberDOB,MEM_GENDER,EnrollmentStatus,PracticeName,AttributedProviderName,AttributedProviderSpecialty,AttributedProviderNPI,DiagnosisCode,ServiceDate,AttendingProviderName,AttendingProviderNPI,AttendingProviderSpecialty,DiagnosisDataSource,ROOT_COMPANIES_ID,ReportId)
Select 
	'Advanced Illness And Frailty' as RegistryName
	,t3.EMPI
	,a.Data_source as Payer
	,a.PayerId
	,a.MemberLastName
	,a.MemberFirstName
	,a.MemberMiddleName
	,a.MemberDOB
	,a.MEM_GENDER
	,a.EnrollmentStatus
	,a.AmbulatoryPCPPractice as PracticeName
	,a.AmbulatoryPCPName as AttributedProviderName
	,a.AmbulatoryPCPSpecialty as AttributedProviderSpecialty
	,a.AmbulatoryPCPNPI as AttributedProviderNPI
	,DiagnosisCode
	,ServiceDate
	,AttendingProviderName
	,AttendingProviderNPI
	,AttendingProviderSpecialty
	,DiagnosisDataSource
	,@rootId as ROOT_COMPANIES_ID
	,@reportId
from(
	select
		EMPI
		,MEMBER_ID
		,ServiceDate
		,DiagnosisCode
		,AttendingProviderNPI
		,AttendingProviderName
		,AttendingProviderSpecialty
		,DiagnosisDataSource 
	from(
		select 
			*
			,DENSE_RANK() over(partition by EMPI order by ServiceDate Desc,AttendingProviderNPI desc,rn Desc) as rnk 
		from(
			select
				a.EMPI
				,a.Member_id
				,a.ServiceDate
				,a.DiagnosisCode
				,a.AttendingProviderNPI
				,a.AttendingProviderName
				,a.AttendingProviderSpecialty
				,a.DiagnosisDataSource 
				,ROW_NUMBER() over(partition by a.EMPI order by a.ServiceDate Desc) as rn
			from #advancedillnesswithvsts a
			join #FrailtyMembers f on a.EMPI=f.EMPI
		)t1
	)t2 where rnk=1
)t3
join KPI_ENGINE.RPT.PCP_ATTRIBUTION a on t3.EMPI=a.EMPI  and 
									a.AssignedStatus='Assigned' and 
									a.ReportId=@reportId


-- Members with only Advanced Illness
	-- Inserting data in REGISTRY_OUTPUT_LINE

	Insert into KPI_ENGINE.REG.REGISTRY_OUTPUT_LINE(RegistryName,EMPI,Payer,PayerId,MemberLastName,MemberFirstName,MemberMiddleName,MemberDOB,MEM_GENDER,EnrollmentStatus,PracticeName,AttributedProviderName,AttributedProviderSpecialty,AttributedProviderNPI,DiagnosisCode,ServiceDate,AttendingProviderName,AttendingProviderNPI,AttendingProviderSpecialty,DiagnosisDataSource,ROOT_COMPANIES_ID,ReportId)
	Select 'Advanced Illness Opportunities' as RegistryName,t3.EMPI,a.Data_source as Payer,a.PayerId,a.MemberLastName,a.MemberFirstName,a.MemberMiddleName,a.MemberDOB,a.MEM_GENDER,a.EnrollmentStatus,a.AmbulatoryPCPPractice as PracticeName,a.AmbulatoryPCPName as AttributedProviderName,a.AmbulatoryPCPSpecialty as AttributedProviderSpecialty,a.AmbulatoryPCPNPI as AttributedProviderNPI,DiagnosisCode,ServiceDate,AttendingProviderName,AttendingProviderNPI,AttendingProviderSpecialty,DiagnosisDataSource,@rootId as ROOT_COMPANIES_ID,@reportId
	from(
	select EMPI,MEMBER_ID,ServiceDate,DiagnosisCode,AttendingProviderNPI,AttendingProviderName,AttendingProviderSpecialty,DiagnosisDataSource from(
	select *,DENSE_RANK() over(partition by EMPI order by ServiceDate Desc,AttendingProviderNPI) as rnk from(
	select distinct EMPI,Member_id,ServiceDate,DiagnosisCode,AttendingProviderNPI,AttendingProviderName,AttendingProviderSpecialty,DiagnosisDataSource from #advancedillnesswithvsts where EMPI not in(select EMPI from #FrailtyMembers)
	)t1
	)t2 
	)t3
	join KPI_ENGINE.RPT.PCP_ATTRIBUTION a on t3.EMPI=a.EMPI  and a.AssignedStatus='Assigned' and a.ReportId=@reportId



-- Inserting data into Registry_output
Insert into KPI_ENGINE.REG.REGISTRY_OUTPUT(RegistryName,EMPI,Payer,PayerId,MemberLastName,MemberFirstName,MemberMiddleName,MemberDOB,MEM_GENDER,EnrollmentStatus,PracticeName,AttributedProviderName,AttributedProviderSpecialty,AttributedProviderNPI,DiagnosisCode,ServiceDate,AttendingProviderName,AttendingProviderNPI,AttendingProviderSpecialty,DiagnosisDataSource,ROOT_COMPANIES_ID,ReportId)
Select 
	'Advanced Illness Opportunities' as RegistryName
	,t3.EMPI
	,a.Data_source as Payer
	,a.PayerId
	,a.MemberLastName
	,a.MemberFirstName
	,a.MemberMiddleName
	,a.MemberDOB
	,a.MEM_GENDER
	,a.EnrollmentStatus
	,a.AmbulatoryPCPPractice as PracticeName
	,a.AmbulatoryPCPName as AttributedProviderName
	,a.AmbulatoryPCPSpecialty as AttributedProviderSpecialty
	,a.AmbulatoryPCPNPI as AttributedProviderNPI
	,DiagnosisCode
	,ServiceDate
	,AttendingProviderName
	,AttendingProviderNPI
	,AttendingProviderSpecialty
	,DiagnosisDataSource
	,@rootId as ROOT_COMPANIES_ID
	,@reportId
from(
	select 
		EMPI
		,MEMBER_ID
		,ServiceDate
		,DiagnosisCode
		,AttendingProviderNPI
		,AttendingProviderName
		,AttendingProviderSpecialty
		,DiagnosisDataSource 
	from(
		select 
			*
			,DENSE_RANK() over(partition by EMPI order by ServiceDate Desc,AttendingProviderNPI desc,rn desc) as rnk 
		from(
			select distinct		
				EMPI
				,Member_id
				,ServiceDate
				,DiagnosisCode
				,AttendingProviderNPI
				,AttendingProviderName
				,AttendingProviderSpecialty
				,DiagnosisDataSource 
				,ROW_NUMBER() over(partition by EMPI order by ServiceDate Desc) as rn
			from #advancedillnesswithvsts 
			where EMPI not in(select EMPI from #FrailtyMembers)
		)t1
	)t2 where rnk=1
)t3
join KPI_ENGINE.RPT.PCP_ATTRIBUTION a on t3.EMPI=a.EMPI  and a.AssignedStatus='Assigned' and a.ReportId=@reportId






-- Members with only Frailty
Insert into KPI_ENGINE.REG.REGISTRY_OUTPUT_LINE(RegistryName,EMPI,Payer,PayerId,MemberLastName,MemberFirstName,MemberMiddleName,MemberDOB,MEM_GENDER,EnrollmentStatus,PracticeName,AttributedProviderName,AttributedProviderSpecialty,AttributedProviderNPI,DiagnosisCode,ServiceDate,AttendingProviderName,AttendingProviderNPI,AttendingProviderSpecialty,DiagnosisDataSource,ROOT_COMPANIES_ID,ReportId)
Select 'Frailty Opportunities' as RegistryName,t3.EMPI,a.Data_source as Payer,a.PayerId,a.MemberLastName,a.MemberFirstName,a.MemberMiddleName,a.MemberDOB,a.MEM_GENDER,a.EnrollmentStatus,a.AmbulatoryPCPPractice as PracticeName,a.AmbulatoryPCPName as AttributedProviderName,a.AmbulatoryPCPSpecialty as AttributedProviderSpecialty,a.AmbulatoryPCPNPI as AttributedProviderNPI,DiagnosisCode,ServiceDate,AttendingProviderName,AttendingProviderNPI,AttendingProviderSpecialty,DiagnosisDataSource,@rootId as ROOT_COMPANIES_ID,@reportId
from(
select EMPI,MEMBER_ID,ServiceDate,DiagnosisCode,AttendingProviderNPI,AttendingProviderName,AttendingProviderSpecialty,DiagnosisDataSource from(
select *,DENSE_RANK() over(partition by EMPI order by ServiceDate Desc,AttendingProviderNPI) as rnk from(
select distinct EMPI,Member_id,ServiceDate,DiagnosisCode,AttendingProviderNPI,AttendingProviderName,AttendingProviderSpecialty,DiagnosisDataSource from #FrailtyMembers where EMPI not in(select EMPI from #advancedillnesswithvsts)
)t1
)t2 
)t3
join KPI_ENGINE.RPT.PCP_ATTRIBUTION a on t3.EMPI=a.EMPI   and a.AssignedStatus='Assigned' and a.ReportId=@reportId


-- Insert Frailty Opportunities in Registry Output
Insert into KPI_ENGINE.REG.REGISTRY_OUTPUT(RegistryName,EMPI,Payer,PayerId,MemberLastName,MemberFirstName,MemberMiddleName,MemberDOB,MEM_GENDER,EnrollmentStatus,PracticeName,AttributedProviderName,AttributedProviderSpecialty,AttributedProviderNPI,DiagnosisCode,ServiceDate,AttendingProviderName,AttendingProviderNPI,AttendingProviderSpecialty,DiagnosisDataSource,ROOT_COMPANIES_ID,ReportId)
Select 
	'Frailty Opportunities' as RegistryName
	,t3.EMPI
	,a.Data_source as Payer
	,a.PayerId
	,a.MemberLastName
	,a.MemberFirstName
	,a.MemberMiddleName
	,a.MemberDOB
	,a.MEM_GENDER
	,a.EnrollmentStatus
	,a.AmbulatoryPCPPractice as PracticeName
	,a.AmbulatoryPCPName as AttributedProviderName
	,a.AmbulatoryPCPSpecialty as AttributedProviderSpecialty
	,a.AmbulatoryPCPNPI as AttributedProviderNPI
	,DiagnosisCode
	,ServiceDate
	,AttendingProviderName
	,AttendingProviderNPI
	,AttendingProviderSpecialty
	,DiagnosisDataSource
	,@rootId as ROOT_COMPANIES_ID
	,@reportId
from(
	select	
		EMPI
		,MEMBER_ID
		,ServiceDate
		,DiagnosisCode
		,AttendingProviderNPI
		,AttendingProviderName
		,AttendingProviderSpecialty
		,DiagnosisDataSource 
	from(
		select 
			*
			,DENSE_RANK() over(partition by EMPI order by ServiceDate Desc,AttendingProviderNPI desc,rn desc) as rnk 
		from(
			select distinct		
				EMPI
				,Member_id
				,ServiceDate
				,DiagnosisCode
				,AttendingProviderNPI
				,AttendingProviderName
				,AttendingProviderSpecialty
				,DiagnosisDataSource 
				,ROW_NUMBER() over(partition by EMPI order by ServiceDate Desc) as rn
			from #FrailtyMembers 
			where EMPI not in(select EMPI from #advancedillnesswithvsts)
		)t1
	)t2 where rnk=1
)t3
join KPI_ENGINE.RPT.PCP_ATTRIBUTION a on t3.EMPI=a.EMPI and a.AssignedStatus='Assigned' and a.ReportId=@reportId




-- Members with just one diagnosis of Advanced Illness
Insert into KPI_ENGINE.REG.REGISTRY_OUTPUT_LINE(RegistryName,EMPI,Payer,PayerId,MemberLastName,MemberFirstName,MemberMiddleName,MemberDOB,MEM_GENDER,EnrollmentStatus,PracticeName,AttributedProviderName,AttributedProviderSpecialty,AttributedProviderNPI,DiagnosisCode,ServiceDate,AttendingProviderName,AttendingProviderNPI,AttendingProviderSpecialty,DiagnosisDataSource,ROOT_COMPANIES_ID,ReportId)
Select 'Advanced Illness Opportunities - Single Diagnosis' as RegistryName,t3.EMPI,a.Data_source as Payer,a.PayerId,a.MemberLastName,a.MemberFirstName,a.MemberMiddleName,a.MemberDOB,a.MEM_GENDER,a.EnrollmentStatus,a.AmbulatoryPCPPractice as PracticeName,a.AmbulatoryPCPName as AttributedProviderName,a.AmbulatoryPCPSpecialty as AttributedProviderSpecialty,a.AmbulatoryPCPNPI as AttributedProviderNPI,DiagnosisCode,ServiceDate,AttendingProviderName,AttendingProviderNPI,AttendingProviderSpecialty,DiagnosisDataSource,@rootId as ROOT_COMPANIES_ID,@reportId
from(
select EMPI,MEMBER_ID,ServiceDate,DiagnosisCode,AttendingProviderNPI,AttendingProviderName,AttendingProviderSpecialty,DiagnosisDataSource from(
select *,DENSE_RANK() over(partition by EMPI order by ServiceDate Desc,AttendingProviderNPI) as rnk from(

Select EMPI,MEMBER_ID,ServiceDate,DiagnosisCode,AttendingProviderNPI,AttendingProviderName,AttendingProviderSpecialty,DiagnosisDataSource from #advancedillness
where EMPI in(
	Select t3.EMPI from(
	select distinct t2.EMPI,t2.ServiceDate
	from(
		select distinct a.* from(
				select EMPI,Member_id,ServiceDate,Claim_id,CL_DATA_SRC  from #ambulatoryvisits 
				union all
				select na.EMPI,na.Member_id,na.ServiceDate,na.Claim_id,na.CL_DATA_SRC from #nonacutestays na
				join #inpatientStays inp on na.Member_id=inp.Member_id and na.Claim_id=inp.Claim_id and na.CL_DATA_SRC=inp.CL_DATA_SRC
		)t1
		Join #advancedillness a on a.Member_id=t1.Member_id and a.Claim_id=t1.Claim_id and a.DiagnosisDataSource=t1.CL_DATA_SRC
	)t2 
	)t3 group by EMPI having count(*)=1
	except
	(
		select distinct ai.EMPI from #acuteipvsts a
		join #advancedillness ai on a.Member_id=ai.Member_id and a.Claim_id=ai.Claim_id and a.CL_DATA_SRC=ai.DiagnosisDataSource

		Union all

		select distinct a.EMPI from(
		select inp.Member_id,inp.ServiceDate,inp.Claim_id,inp.CL_DATA_SRC from #inpatientStays inp
		left outer join #nonacutestays na on inp.Member_id=na.Member_id and inp.Claim_id=na.Claim_id and inp.CL_DATA_SRC=na.CL_DATA_SRC
		where na.Member_id is null
		)t1
		join #advancedillness a on a.Member_id=t1.Member_id and a.Claim_id=t1.Claim_id and a.DiagnosisDataSource=t1.CL_DATA_SRC
		
	)
)

)t1
)t2
)t3
join KPI_ENGINE.RPT.PCP_ATTRIBUTION a on t3.EMPI=a.EMPI  and a.AssignedStatus='Assigned' and a.ReportId=@reportId


-- Insert data into Registry Output
Insert into KPI_ENGINE.REG.REGISTRY_OUTPUT(RegistryName,EMPI,Payer,PayerId,MemberLastName,MemberFirstName,MemberMiddleName,MemberDOB,MEM_GENDER,EnrollmentStatus,PracticeName,AttributedProviderName,AttributedProviderSpecialty,AttributedProviderNPI,DiagnosisCode,ServiceDate,AttendingProviderName,AttendingProviderNPI,AttendingProviderSpecialty,DiagnosisDataSource,ROOT_COMPANIES_ID,ReportId)
Select 
	'Advanced Illness Opportunities - Single Diagnosis' as RegistryName
	,t3.EMPI
	,a.Data_source as Payer
	,a.PayerId
	,a.MemberLastName
	,a.MemberFirstName
	,a.MemberMiddleName
	,a.MemberDOB
	,a.MEM_GENDER
	,a.EnrollmentStatus
	,a.AmbulatoryPCPPractice as PracticeName
	,a.AmbulatoryPCPName as AttributedProviderName
	,a.AmbulatoryPCPSpecialty as AttributedProviderSpecialty
	,a.AmbulatoryPCPNPI as AttributedProviderNPI
	,DiagnosisCode
	,ServiceDate
	,AttendingProviderName
	,AttendingProviderNPI
	,AttendingProviderSpecialty
	,DiagnosisDataSource
	,@rootId as ROOT_COMPANIES_ID
	,@reportId
from(
	select 
		EMPI
		,MEMBER_ID
		,ServiceDate
		,DiagnosisCode
		,AttendingProviderNPI
		,AttendingProviderName
		,AttendingProviderSpecialty
		,DiagnosisDataSource 
	from(
		select 
			*
			,DENSE_RANK() over(partition by EMPI order by ServiceDate Desc,AttendingProviderNPI desc,rn desc) as rnk 
		from(
	
			Select 
				EMPI
				,MEMBER_ID
				,ServiceDate
				,DiagnosisCode
				,AttendingProviderNPI
				,AttendingProviderName
				,AttendingProviderSpecialty
				,DiagnosisDataSource
				,ROW_NUMBER() over(partition by EMPI order by ServiceDate Desc) as rn 
			from #advancedillness
			where EMPI in(
							Select 
								t3.EMPI 
							from(
									select distinct 
										t2.EMPI
										,t2.ServiceDate
									from(
											select distinct 
												a.* 
											from(
													select 
														EMPI
														,Member_id
														,ServiceDate
														,Claim_id
														,CL_DATA_SRC  
													from #ambulatoryvisits 
													
													union all
													
													select 
														na.EMPI
														,na.Member_id
														,na.ServiceDate
														,na.Claim_id
														,na.CL_DATA_SRC 
													from #nonacutestays na
													join #inpatientStays inp on na.Member_id=inp.Member_id and				
																				na.Claim_id=inp.Claim_id and 
																				na.CL_DATA_SRC=inp.CL_DATA_SRC
												)t1
												Join #advancedillness a on a.Member_id=t1.Member_id and 
																		   a.Claim_id=t1.Claim_id and
																		   a.DiagnosisDataSource=t1.CL_DATA_SRC
											)t2 
										)t3 group by EMPI having count(*)=1
									
										except
										
										(
											select distinct 
												ai.EMPI 
											from #acuteipvsts a
											join #advancedillness ai on a.Member_id=ai.Member_id and 
																		a.Claim_id=ai.Claim_id and
																		a.CL_DATA_SRC=ai.DiagnosisDataSource

											
											Union all

											select distinct 
												a.EMPI 
											from(
													select 
														inp.Member_id
														,inp.ServiceDate
														,inp.Claim_id
														,inp.CL_DATA_SRC 
													from #inpatientStays inp
													left outer join #nonacutestays na on inp.Member_id=na.Member_id and
																						 inp.Claim_id=na.Claim_id and
																						 inp.CL_DATA_SRC=na.CL_DATA_SRC
													where na.Member_id is null
										)t1
										join #advancedillness a on a.Member_id=t1.Member_id and 
																   a.Claim_id=t1.Claim_id and
																   a.DiagnosisDataSource=t1.CL_DATA_SRC
		
									)
								)

							)t1
						)t2 where rnk=1
					)t3
					join KPI_ENGINE.RPT.PCP_ATTRIBUTION a on t3.EMPI=a.EMPI  and 
															 a.AssignedStatus='Assigned' and 
															 a.ReportId=@reportId


		SET @PERF_ROW = @@ROWCOUNT
		SET @PERF_DURATION = DATEDIFF(MINUTE,@PERF_START,GETDATE());
		EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, 'Enrollment Completed', @PERF_ROW, @DURATION_IN_MIN=@PERF_DURATION, @LOG2ID=@LOGID;
		EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, @LOG2ID=@LOGID, @END_FLAG=1;

END TRY
BEGIN CATCH
		BEGIN
			EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, 'FATAL ERROR', @LOG2ID=@LOGID;
			RAISERROR ('',16,0)
			RETURN 16
        END
	END CATCH
RETURN 0
END

GO
