SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON





CREATE proc [dbo].[KPI_UHN_AllCauseReadmission_40]
AS
BEGIN
DECLARE @INFO VARCHAR(8000)
					,	@TRUE BIT = 1
					,	@FALSE BIT = 0
					,	@DB_ID INT = DB_ID()
					,	@LOGID INT = 0
					,	@PROC VARCHAR(500)=OBJECT_SCHEMA_NAME(@@PROCID)+'.'+OBJECT_NAME(@@PROCID)
					,	@PERF_START DATETIME
					,	@PERF_DURATION INT
					,	@PERF_ROW INT
					,	@RC	INT
					;

		EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, @INFO=@INFO, @LOG2ID=@LOGID OUT;
		DECLARE @PROCFULLNAME VARCHAR(500) = DB_NAME()+'.'+@PROC;
		----EXEC dbo.SP_MI_UTIL_SEND_MAIL @PROCFULLNAME, 'STARTED', 1, @ECHO=0; 
	DECLARE @MSG VARCHAR(1000)
	DECLARE @PERF_START_PROC DATETIME 
	DECLARE @PERF_RPS NUMERIC(10,2) 
	DECLARE @ROWS INT 
	DECLARE @PERF_ROWS INT 
	SET @PERF_ROWS = 0 
	SET @PERF_START = GETDATE() 
	SET @PERF_START_PROC = GETDATE() 

BEGIN TRY


--Declare vaiable
Declare @rundate date=GetDate();
declare @meas_year varchar(4)=Year(Dateadd(month,-2,@rundate))
declare @rootId varchar(10)='159'


Declare @ce_startdt Date
Declare @ce_startdt1 Date
Declare @ce_startdt2 Date
Declare @ce_dischstartdt Date
Declare @ce_enddt Date
Declare @ce_enddt1 Date
Declare @snf_enddt Date
Declare @ce_dischenddt Date
Declare @meas varchar(10);
Declare @runid INT=0;
Declare @reportId INT;

Declare @startDate Date;
Declare @enddate date;
Declare @quarter varchar(20);
Declare @measure_id varchar(10);
Declare @target INT;
Declare @domain varchar(100);
Declare @subdomain varchar(100);
Declare @measuretype varchar(100);
Declare @measurename varchar(100);
Declare @reporttype varchar(100);



Set @meas='PCR';
SET @ce_startdt=concat(@meas_year,'-01-01');
SET @ce_startdt1=concat(@meas_year-1,'-01-01');
SET @ce_startdt2=concat(@meas_year-2,'-01-01');
SET @ce_dischstartdt=concat(@meas_year,'-01-03');
SET @ce_enddt=concat(@meas_year,'-12-31');
Set @snf_enddt=concat(@meas_year,'-12-02');
SET @ce_enddt1=concat(@meas_year-1,'-12-31');
SET @ce_dischenddt=concat(@meas_year,'-12-01');

Set @reporttype='Network'
Set @measurename='30 Day Readmission Rate – All Cause'
--Set @startDate=DATEADD(yy, DATEDIFF(yy, 0,Dateadd(month,-2,GETDATE())), 0) 
--Set @enddate=eomonth(Dateadd(month,-2,GetDate()))
--Set @quarter=Concat(Year(@enddate),' - Q',DATEPART(q, @enddate))
set @target=12
Set @domain='Utilization Management'
Set @subdomain='Readmission Rate'
Set @measuretype='Calculated'
Set @measure_id='40'


-- hospice Exclusion
drop table if exists #m40_hospicemembers;
CREATE table #m40_hospicemembers
(
	EMPI varchar(100)
		
)
Insert into #m40_hospicemembers
select EMPI from hospicemembers(@rootId,@ce_startdt,@ce_enddt)


-- Step 1.1 -- Identify Inpatient and Observation Stay 
drop table if exists #m40_staylist;
CREATE table #m40_staylist
(
	EMPI varchar(100),
	FROM_DATE Date,
	ADM_DATE Date,
	DIS_DATE Date,
	CLAIM_ID varchar(100),
	SV_STAT varchar(20)
				
)
Insert into #m40_staylist
Select distinct
	EMPI
	,FROM_DATE
	,Coalesce(ADM_DATE,FROM_DATE) as ADM_DATE
	,Coalesce(DIS_DATE,TO_DATE) as DIS_DATE
	,CLAIM_ID
	,SV_STAT
From CLAIMLINE
Where
	ROOT_COMPANIES_ID=@rootId and
	ISNULL(POS,'0')!='81' and
	RIGHT('0000'+CAST(Trim(REV_CODE) AS VARCHAR(4)),4) in
	(
		select code from hds.VALUESET_TO_CODE where Code_System='UBREV' and Value_Set_Name in('Inpatient Stay')
	)




-- Step 1.2 -- Identify Nonacute Inpatient Stay
drop table if exists #m40_nonacutestays
CREATE table #m40_nonacutestays
(
	EMPI varchar(100),
	CLAIM_ID varchar(100)
				
)
-- Identify non acute Inpatient Stays
Insert into #m40_nonacutestays
Select distinct
	EMPI
	,CLAIM_ID
From CLAIMLINE
where 
	ROOT_COMPANIES_ID=@rootId and 
	ISNULL(POS,'0')!='81' and 
	(
		RIGHT('0000'+CAST(Trim(REV_CODE) AS VARCHAR(4)),4) in 
		(
			select code from HDS.VALUESET_TO_CODE where code_system='UBREV'and Value_Set_Name in('Nonacute Inpatient Stay')
		) 
		or 
		RIGHT('0000'+CAST(Trim(UB_BILL_TYPE) AS VARCHAR(4)),4) in 
		(
			select code from HDS.VALUESET_TO_CODE where code_system='UBTOB' and Value_Set_Name in('Nonacute Inpatient Stay')
		)
	)



-- Step 1.3 -- Identify only Acute and observation Discharges and remove non acute stays
drop table if exists #m40_acutestays;
CREATE table #m40_acutestays
(
	EMPI varchar(100),
	FROM_DATE Date,
	ADM_DATE Date,
	DIS_DATE Date,
	CLAIM_ID varchar(100),
	SV_STAT varchar(20)
		
)
-- removing non acute inpatient stays from total Inpatient stays
Insert into #m40_acutestays
select 
	t1.EMPI
	,t1.FROM_DATE
	,t1.ADM_DATE
	,t1.DIS_DATE
	,t1.CLAIM_ID
	,t1.SV_STAT
	from #m40_staylist t1
	left outer join #m40_nonacutestays t2 on 
		t1.EMPI=t2.EMPI and 
		t1.CLAIM_ID=t2.CLAIM_ID
	Where 
		t2.EMPI is null


-- Step 2 and 3 - Identify Direct Transfers , Select discharges between 1st Jan and 1st Dec , Exlcude same day stays
drop table if exists #m40_directtransfers;
Create table #m40_directtransfers
(
	EMPI varchar(100),
	ADM_DATE Date,
	DIS_DATE Date,
	AdmissionClaimId varchar(100),
	DischargeClaimId varchar(100),
	grp_claimid nvarchar(max)
)
;with grp_starts as 
(
  select distinct 
	EMPI
	,ADM_DATE
	,DIS_DATE
	,CLAIM_ID
	,case
		when datediff(day,lag(DIS_DATE) over(partition by EMPI order by ADM_DATE, DIS_DATE),ADM_DATE) <=1 	
		or 
		(datediff(day,lag(DIS_DATE,2) over(partition by EMPI order by ADM_DATE, DIS_DATE),ADM_DATE)<=1 )
		then 0 
		else 1
	end grp_start
  from #m40_acutestays
  

)
, grps as 
(
  select 
	EMPI
	,ADM_DATE
	, DIS_DATE
	,CLAIM_ID
	,sum(grp_start) over(partition by EMPI order by ADM_DATE, DIS_DATE) grp
  from grp_starts
)
Insert into #m40_directtransfers
select 
	EMPI
	,AdmissionDate
	,DischargeDate
	,AdmissionClaimId
	,DischargeClaimId
	,grp_claimid 
from	
(
			select 
				EMPI
				,min(ADM_DATE) as AdmissionDate
				,max(DIS_DATE) as DischargeDate
				,min(CLAIM_ID) as AdmissionClaimId
				,max(CLAIM_ID) as DischargeClaimId
				,STRING_AGG(CLAIM_ID,',') as grp_claimid
			from grps 
			group by EMPI,grp
		
)t1



-- Create a normalized transfer table to improve lookup
drop table if exists #m40_directtransfers_normalized;
Create table #m40_directtransfers_normalized
(
	EMPI varchar(100),
	ADM_DATE Date,
	DIS_DATE Date,
	AdmissionClaimId varchar(100),
	DischargeClaimId varchar(100),
	grp_claimid nvarchar(max),
	CLAIM1 varchar(100),
	CLAIM2 varchar(100),
	CLAIM3 varchar(100),
	CLAIM4 varchar(100),
	CLAIM5 varchar(100),
)
Insert into #m40_directtransfers_normalized
select 
	* 
from 
(
	select 
		* 
	from #m40_directtransfers t 
	cross apply 
	(
		select 
			RowN=Row_Number() over (Order by (SELECT NULL))
			,value 
		from string_split(t.grp_claimid, ',') ) d
	) src
	pivot 
		(max(value) for src.RowN in([1],[2],[3],[4],[5])
) p
	



-- Step 4 - Identify Exclusions

-- Step 4.1 - Identify members who were dead on discharge
drop table if exists #m40_deceasedmembers;
CREATE table #m40_deceasedmembers
(
	EMPI varchar(100),
	CLAIM_ID varchar(100)
)
Insert into #m40_deceasedmembers
select distinct
	EMPI
	,CLAIM_ID 
from CLAIMLINE 
where 
	ROOT_COMPANIES_ID=@rootId and 
	ISNULL(POS,'')!='81' and 
	DIS_STAT='20';



-- Step 4.2  Identify Prgnacncy Visits
drop table if exists #m40_pregvstlist;
CREATE table #m40_pregvstlist
(
	EMPI varchar(100),
	FROM_DATE DATE,
	ADM_DATE DATE,
	DIS_DATE DATE,
	CLAIM_ID varchar(100)
);
Insert into #m40_pregvstlist
Select distinct
	d.EMPI
	,d.DIAG_START_DATE		
	,coalesce(c.ADM_DATE,c.FROM_DATE) as ADM_DATE
	,coalesce(c.DIS_DATE,c.TO_DATE) as DIS_DATE
	,d.CLAIM_ID
From Diagnosis d
join CLAIMLINE c on d.CLAIM_ID=c.CLAIM_ID and
  				    d.DIAG_DATA_SRC=c.CL_DATA_SRC and
					d.ROOT_COMPANIES_ID=c.ROOT_COMPANIES_ID and
					d.EMPI=c.EMPI
Join open_empi_master m on d.EMPI=m.EMPI_ID and 
						   m.Gender='F' and
						   d.ROOT_COMPANIES_ID=m.Root_Companies_ID
						   
where
	d.ROOT_COMPANIES_ID=@rootId and
	ISNULL(c.POS,'0')!='81' and
	DIAG_CODE in
	(
		select code_new from HDS.VALUESET_TO_CODE where Value_Set_Name='Pregnancy'
	)
	and
	DIAG_SEQ_NO=1
	

-- Step 4.3 -- Identify Perinatal Conditions
drop table if exists #m40_perinatalvstlist;
CREATE table #m40_perinatalvstlist
(
	EMPI varchar(100),
	FROM_DATE DATE,
	ADM_DATE DATE,
	DIS_DATE DATE,
	CLAIM_ID varchar(100)
);
Insert into #m40_perinatalvstlist
Select distinct
	d.EMPI
	,d.DIAG_START_DATE		
	,coalesce(c.ADM_DATE,c.FROM_DATE) as ADM_DATE
	,coalesce(c.DIS_DATE,c.TO_DATE) as DIS_DATE
	,d.CLAIM_ID
From Diagnosis d
join CLAIMLINE c on d.CLAIM_ID=c.CLAIM_ID and
  				    d.DIAG_DATA_SRC=c.CL_DATA_SRC and
					d.ROOT_COMPANIES_ID=c.ROOT_COMPANIES_ID and
					d.EMPI=c.EMPI
where
	d.ROOT_COMPANIES_ID=@rootId and
	ISNULL(c.POS,'')!='81' and
	DIAG_CODE in
	(
		select code_new from HDS.VALUESET_TO_CODE where Value_Set_Name='Perinatal Conditions'
	)
	and
	DIAG_SEQ_NO=1

	
-- Step 4.4 -- Exclude stays identified in step 4.1,4.2 and 4.3
drop table if exists #m40_directtransferswithexcl;
Create table #m40_directtransferswithexcl
(
	EMPI varchar(100),
	ADM_DATE Date,
	DIS_DATE Date,
	AdmissionClaimId varchar(100),
	DischargeClaimId varchar(100),
	grp_claimid nvarchar(max),
	CLAIM1 varchar(100),
	CLAIM2 varchar(100),
	CLAIM3 varchar(100),
	CLAIM4 varchar(100),
	CLAIM5 varchar(100),
	StayNumber INT,
	Age INT
)
Insert into #m40_directtransferswithexcl
Select distinct
	dt.*
	,RANK() over(partition by dt.EMPI order by dt.DIS_DATE) as StayNumber
	,CASE 
		WHEN dateadd(year, datediff (year, Date_of_Birth,dt.DIS_DATE), Date_of_Birth) >dt.DIS_DATE THEN datediff(year, Date_of_Birth, dt.DIS_DATE) - 1
		ELSE datediff(year, Date_of_Birth, dt.DIS_DATE)
	END as Age
From #m40_directtransfers_normalized dt
Join open_empi_master oem on 
	dt.EMPI=oem.EMPI_ID and
	oem.Root_Companies_ID=@rootId
Left outer join #m40_deceasedmembers dm on 
	dt.EMPI=dm.EMPI and 
	dm.CLAIM_ID in(dt.CLAIM1,dt.CLAIM2,dt.CLAIM3,dt.CLAIM4,dt.CLAIM5)
Left outer join #m40_perinatalvstlist prn on 
	dt.EMPI=prn.EMPI and 
	prn.CLAIM_ID in(dt.CLAIM1,dt.CLAIM2,dt.CLAIM3,dt.CLAIM4,dt.CLAIM5)
Left outer join #m40_pregvstlist prg on 
	dt.EMPI=prg.EMPI and 
	prg.CLAIM_ID in(dt.CLAIM1,dt.CLAIM2,dt.CLAIM3,dt.CLAIM4,dt.CLAIM5)
left outer join #m40_hospicemembers hspexcl on dt.EMPI=hspexcl.EMPI
Where
	dm.EMPI is null and
	prn.EMPI is null and
	prg.EMPI is null and
	hspexcl.EMPI is null and
	dt.DIS_DATE between @ce_startdt and @ce_enddt
	

-- Numerator Logic


-- Step 1.1 -- Identify Inpatient and Observation Stay 
drop table if exists #m40_num_staylist;
CREATE table #m40_num_staylist
(
	EMPI varchar(100),
	FROM_DATE Date,
	ADM_DATE Date,
	DIS_DATE Date,
	CLAIM_ID varchar(100),
	SV_STAT varchar(20)
				
)
Insert into #m40_num_staylist
Select distinct
	EMPI
	,FROM_DATE
	,Coalesce(ADM_DATE,FROM_DATE) as ADM_DATE
	,Coalesce(DIS_DATE,TO_DATE) as DIS_DATE
	,CLAIM_ID
	,SV_STAT
From CLAIMLINE
Where
	ROOT_COMPANIES_ID=@rootId and
	ISNULL(POS,'0')!='81' and
	RIGHT('0000'+CAST(Trim(REV_CODE) AS VARCHAR(4)),4) in
	(
		select code from hds.VALUESET_TO_CODE where Code_System='UBREV' and Value_Set_Name in('Inpatient Stay')
	)
	





-- Step 1.3 -- Identify only Acute and observation Discharges and remove non acute stays
drop table if exists #m40_num_acutestays;
CREATE table #m40_num_acutestays
(
	EMPI varchar(100),
	FROM_DATE Date,
	ADM_DATE Date,
	DIS_DATE Date,
	CLAIM_ID varchar(100),
	SV_STAT varchar(20)
		
)
-- removing non acute inpatient stays from total Inpatient stays
Insert into #m40_num_acutestays
select 
	t1.EMPI
	,t1.FROM_DATE
	,t1.ADM_DATE
	,t1.DIS_DATE
	,t1.CLAIM_ID
	,t1.SV_STAT
from #m40_num_staylist t1
left outer join #m40_nonacutestays t2 on 
	t1.EMPI=t2.EMPI and 
	t1.CLAIM_ID=t2.CLAIM_ID
Where 
	t2.EMPI is null


	
-- Step 2 and 3 - Identify Direct Transfers , Select discharges between 1st Jan and 1st Dec , Exlcude same day stays
drop table if exists #m40_num_directtransfers;
Create table #m40_num_directtransfers
(
	EMPI varchar(100),
	ADM_DATE Date,
	DIS_DATE Date,
	AdmissionClaimId varchar(100),
	DischargeClaimId varchar(100),
	grp_claimid nvarchar(max)
)
;with grp_starts as 
(
  select distinct
	EMPI
	,ADM_DATE
	,DIS_DATE
	,CLAIM_ID
	,case
		when datediff(day,lag(DIS_DATE) over(partition by EMPI order by ADM_DATE, DIS_DATE),ADM_DATE) <=1 	
		or 
		(datediff(day,lag(DIS_DATE,2) over(partition by EMPI order by ADM_DATE, DIS_DATE),ADM_DATE)<=1 )
		then 0 
		else 1
	end grp_start
  from #m40_num_acutestays
  

)
, grps as 
(
  select 
	EMPI
	,ADM_DATE
	, DIS_DATE
	,CLAIM_ID
	,sum(grp_start) over(partition by EMPI order by ADM_DATE, DIS_DATE) grp
  from grp_starts
)
Insert into #m40_num_directtransfers
select 
	EMPI
	,AdmissionDate
	,DischargeDate
	,AdmissionClaimId
	,DischargeClaimId
	,grp_claimid 
from	
(
			select 
				EMPI
				,min(ADM_DATE) as AdmissionDate
				,max(DIS_DATE) as DischargeDate
				,min(CLAIM_ID) as AdmissionClaimId
				,max(CLAIM_ID) as DischargeClaimId
				,STRING_AGG(CLAIM_ID,',') as grp_claimid
			from grps 
			group by EMPI,grp
		
)t1
	



-- Create a normalized transfer table to improve lookup
drop table if exists #m40_num_directtransfers_normalized;
Create table #m40_num_directtransfers_normalized
(
	EMPI varchar(100),
	ADM_DATE Date,
	DIS_DATE Date,
	AdmissionClaimId varchar(100),
	DischargeClaimId varchar(100),
	grp_claimid nvarchar(max),
	CLAIM1 varchar(100),
	CLAIM2 varchar(100),
	CLAIM3 varchar(100),
	CLAIM4 varchar(100),
	CLAIM5 varchar(100),
)
Insert into #m40_num_directtransfers_normalized
select 
	* 
from 
(
	select 
		* 
	from #m40_num_directtransfers t 
	cross apply 
	(
		select 
			RowN=Row_Number() over (Order by (SELECT NULL))
			,value 
		from string_split(t.grp_claimid, ',') ) d
	) src
	pivot 
		(max(value) for src.RowN in([1],[2],[3],[4],[5])
) p
	



--Idntifying Planned Admissions
-- Indetifying Chemotherapy
drop table if exists #m40_chemovstlist;
CREATE table #m40_chemovstlist
(
	EMPI varchar(100),
	CLAIM_ID varchar(100)
	
);
Insert into #m40_chemovstlist
Select distinct
	d.EMPI
	,d.CLAIM_ID
From Diagnosis d
join CLAIMLINE c on d.CLAIM_ID=c.CLAIM_ID and
  				    d.DIAG_DATA_SRC=c.CL_DATA_SRC and
					d.ROOT_COMPANIES_ID=c.ROOT_COMPANIES_ID and
					d.EMPI=c.EMPI
where
	d.ROOT_COMPANIES_ID=@rootId and
	ISNULL(c.POS,'')!='81' and
	COALESCE(c.DIS_DATE,c.TO_DATE) between @ce_startdt1 and @ce_enddt and
	DIAG_CODE in
	(
		select code_new from HDS.VALUESET_TO_CODE where Value_Set_Name='Chemotherapy Encounter'
	)
	and
	DIAG_SEQ_NO=1


-- Rehabilitation
drop table if exists #m40_rehabvstlist;
CREATE table #m40_rehabvstlist
(
	EMPI varchar(100),
	CLAIM_ID varchar(100)
	
);

Insert into #m40_rehabvstlist
Select distinct
	d.EMPI
	,d.CLAIM_ID
From Diagnosis d
join CLAIMLINE c on d.CLAIM_ID=c.CLAIM_ID and
  				    d.DIAG_DATA_SRC=c.CL_DATA_SRC and
					d.ROOT_COMPANIES_ID=c.ROOT_COMPANIES_ID and
					d.EMPI=c.EMPI
where
	d.ROOT_COMPANIES_ID=@rootId and
	ISNULL(c.POS,'')!='81' and
	COALESCE(c.DIS_DATE,c.TO_DATE) between @ce_startdt1 and @ce_enddt and
	DIAG_CODE in
	(
		select code_new from HDS.VALUESET_TO_CODE where Value_Set_Name='Rehabilitation'
	)
	and
	DIAG_SEQ_NO=1



-- Organ Transplant
drop table if exists #m40_organtransplanttlist;
CREATE table #m40_organtransplanttlist
(
	EMPI varchar(100),
	CLAIM_ID varchar(100)
	
);
Insert into #m40_organtransplanttlist
Select distinct
	p.EMPI
	,p.CLAIM_ID
From Procedures p
Join CLAIMLINE c on p.CLAIM_ID=c.CLAIM_ID and
					ISNULL(p.SV_LINE,0)=ISNULL(c.SV_LINE,0) and
					p.PROC_DATA_SRC=c.CL_DATA_SRC and
					p.ROOT_COMPANIES_ID=c.ROOT_COMPANIES_ID and
					ISNULL(p.PROC_STATUS,'EVN')!='INT' and
					p.EMPI=c.EMPI
Where
	p.ROOT_COMPANIES_ID=@rootId and
	ISNULL(c.POS,'')!='81' and
	COALESCE(c.DIS_DATE,c.TO_DATE) between @ce_startdt1 and @ce_enddt and
	(
		PROC_CODE in
		(
			select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Kidney Transplant','Organ Transplant Other Than Kidney')
		)
		or
		ICDPCS_CODE in
		(
			select code_new from HDS.VALUESET_TO_CODE where Value_Set_Name in('Kidney Transplant','Bone Marrow Transplant','Organ Transplant Other Than Kidney','Introduction of Autologous Pancreatic Cells')
		)
	)

	

--Potentially Planned Procedure
-- Identify Planned Procedures
drop table if exists #m40_plannedprocedures
Create Table #m40_plannedprocedures
(
	EMPI varchar(100),
	CLAIM_ID varchar(100)
)
Insert into #m40_plannedprocedures
select distinct
	EMPI
	,CLAIM_ID
From GetICDPCS(@rootId,@ce_startdt1,@ce_enddt,'Potentially Planned Procedures')



-- Identify Acute COndition
drop table if exists #m40_acuteconditions
Create Table #m40_acuteconditions
(
	EMPI varchar(100),
	CLAIM_ID varchar(100)
)
Insert into #m40_acuteconditions
select distinct
	EMPI
	,CLAIM_ID
From GetDiagnosis(@rootId,@ce_startdt1,@ce_enddt,'Acute Condition')
Where
	DIAG_SEQ_NO=1


--Identify Planned procedures with acute conditions
drop table if exists #m40_plannedprocedurewithoutacutecondition
Create Table #m40_plannedprocedurewithoutacutecondition
(
	EMPI varchar(100),
	CLAIM_ID varchar(100)
)
Insert into #m40_plannedprocedurewithoutacutecondition
Select
	p.EMPI
	,p.CLAIM_ID
From #m40_plannedprocedures p
left outer join #m40_acuteconditions a on
	p.EMPI=a.EMPI and
	p.CLAIM_ID=a.CLAIM_ID
Where
	a.EMPI is null
	



-- Identifying Valid Numerator stays after excluding stays as per definition
	drop table if exists #m40_numstaylist;
	create table #m40_numstaylist
	(
		EMPI varchar(100),
		ADM_DATE Date,
		DIS_DATE Date
		
	)
	Insert into #m40_numstaylist
	select distinct
		b.EMPI
		,b.ADM_DATE
		,b.DIS_DATE
	from #m40_num_directtransfers_normalized b
	left outer join #m40_chemovstlist c on 
		b.EMPI=c.EMPI and 
		c.CLAIM_ID in(CLAIM1,CLAIM2,CLAIM3,CLAIM4,CLAIM5)
	left outer join #m40_hospicemembers h on 
		b.EMPI=h.EMPI
	left outer join #m40_rehabvstlist r on 
		b.EMPI=r.EMPI and 
		r.CLAIM_ID in(CLAIM1,CLAIM2,CLAIM3,CLAIM4,CLAIM5)
	left outer join #m40_pregvstlist pg on 
		b.EMPI=pg.EMPI and 
		pg.CLAIM_ID in(CLAIM1,CLAIM2,CLAIM3,CLAIM4,CLAIM5)
	left outer join #m40_organtransplanttlist o on 
		b.EMPI=o.EMPI and 
		o.CLAIM_ID in(CLAIM1,CLAIM2,CLAIM3,CLAIM4,CLAIM5)
	left outer join #m40_plannedprocedurewithoutacutecondition p on 
		b.EMPI=p.EMPI and 
		p.CLAIM_ID in(CLAIM1,CLAIM2,CLAIM3,CLAIM4,CLAIM5)
	left outer join #m40_perinatalvstlist pn on 
		b.EMPI=pn.EMPI and 
		pn.CLAIM_ID in(CLAIM1,CLAIM2,CLAIM3,CLAIM4,CLAIM5)
	where 
		b.ADM_DATE between @ce_startdt and Dateadd(month,3,@ce_enddt) and 
		c.EMPI is null and 
		r.EMPI is null and 
		o.EMPI is null and 
		p.EMPI is null and
		h.EMPI is null and
		pg.EMPI is null and 
		pn.EMPI is null


	drop table if exists #m40_readmissionset;
	Select
		*
	into #m40_readmissionset
	From
	(
		select 
			b.EMPI,
			b.ADM_DATE as AnchorAdmissionDate,
			b.DIS_DATE as AnchorDischargeDate,
			ns.ADM_DATE as ReadmissionDate,
			ns.DIS_DATE as ReadmissionDischargeDate
			,ROW_NUMBER() over(partition by b.EMPI,b.DIS_DATE order by ns.DIS_DATE) as rn1
			,ROW_NUMBER() over(partition by b.EMPI,ns.DIS_DATE order by b.DIS_DATE desc) as rn2
		from #m40_directtransferswithexcl b
		join #m40_numstaylist ns on 
			b.EMPI=ns.EMPI and 
			ns.ADM_DATE between b.DIS_DATE and dateadd(day,30,b.DIS_DATE)
		where
			b.ADM_DATE!=ns.ADM_DATE
	)t1
	Where
		rn1=1 and rn2=1
	order by 
		EMPI,AnchorDischargeDate,ReadmissionDate

		

	
-- Get ReportId from Report_Details

exec GetReportDetail @rundate=@rundate,@rootId=@rootId,@startDate=@startDate output,@enddate=@enddate output,@quarter=@quarter output,@reportId=@reportId output


	

	-- Insert data into Measure Detailed Line

	Delete from RPT.MEASURE_DETAILED_LINE where MEASURE_ID=@measure_id and REPORT_ID=@reportId and ROOT_COMPANIES_ID=@rootId;

	Insert into RPT.MEASURE_DETAILED_LINE(EMPI,Measure_id,Measure_Name,Num,Den,Excl,Rexcl,Report_Id,ReportType,Report_Quarter,Period_Start_date,Period_end_Date,Root_Companies_id,MEASURE_TYPE,Code,DateofService,DischargeDate)
	Select 
		d.EMPI
		,ml.Measure_id
		,ml.Measure_Name
		,Case When re.EMPI is not null Then 1 else 0 end as NUM
		,1 as Den
		,0 as Excl
		,0 as Rexcl
		,@reportId
		,ml.ReportType
		,@quarter
		,@startDate
		,@enddate
		,@rootId
		,@measuretype
		,'Readmission' as Code
		,re.ReadmissionDate as DateofService
		,d.DIS_DATE as DischargeDate
	From #m40_directtransferswithexcl d
	Left outer Join #m40_readmissionset re on
		d.EMPI=re.EMPI and 
		d.DIS_DATE=re.AnchorDischargeDate
	Join RFT.UHN_MeasuresList ml on
		ml.Measure_id=@measure_id
	
	
	
	

	
		-- Insert data into Provider Scorecard
		Delete from RPT.PROVIDER_SCORECARD  where MEASURE_ID=@measure_id and REPORT_ID=@reportId and ROOT_COMPANIES_ID=@rootId;

/*
Insert into RPT.PROVIDER_SCORECARD(Measure_id,Measure_Name,Measure_Title,MEASURE_SUBTITLE,Measure_Type,NUM_COUNT,DEN_COUNT,Excl_Count,Rexcl_Count,Gaps,Result,Target,Report_Id,ReportType,Report_Quarter,Period_Start_Date,Period_End_Date,Root_Companies_Id,To_Target)
Select Distinct
		Measure_id
		,Measure_Name
		,Measure_Title
		,MEASURE_SUBTITLE
		,Measure_Type
		,NUM_COUNT
		,DEN_COUNT
		,Excl_Count
		,Rexcl_Count
		,Gaps
		,Result
		,Target	
		,Report_Id
		,ReportType
		,Report_Quarter
		,Period_Start_Date
		,Period_End_Date
		,Root_Companies_Id
	,Case
		when Result > Target then Floor((Den_excl*Target)/100)-NUM_COUNT
		when Result <= Target then 0
	end as To_Target
From
(
	Select 
		Measure_id
		,Measure_Name
		,@domain as Measure_Title
		,@subdomain as MEASURE_SUBTITLE
		,@measuretype as Measure_Type
		,NUM_COUNT as NUM_COUNT
		,DEN_COUNT as DEN_COUNT
		,EXCL_COUNT as Excl_Count
		,Rexcl_count as Rexcl_Count
		,NUM_COUNT as Gaps
		,Case
			when Den_excl>0 Then Round((cast(NUM_COUNT as decimal)/Den_excl)*100,2)
			else 0
		end as Result
		,@target as Target
		,Report_Id
		,ReportType
		,Report_Quarter
		,Period_Start_Date
		,Period_End_Date
		,Root_Companies_Id
		,DEN_EXCL
	From
	(
		Select 
			EMPI
			,Measure_id
			,Measure_Name
			,count(case when NUM=1 and Rexcl=0 Then 1 end) as NUM_COUNT
			,count(case when Den=1 Then 1 end) as DEN_COUNT
			,count(case when Den=1 and Rexcl=0 Then 1 end) Den_excl
			,count(case when excl=1 Then 1 end) as EXCL_COUNT
			,count(case when rexcl=1 Then 1 end) as Rexcl_count
			,Code
			,DateofService
			,DischargeDate
			,Report_Id
			,ReportType
			,Report_Quarter
			,Period_Start_Date
			,Period_End_Date
			,Root_Companies_Id
		From RPT.MEASURE_DETAILED_LINE
		where Enrollment_Status='Active' and
			  MEASURE_ID=@measure_id and
			  REPORT_ID=@reportId
		Group by Measure_id,Measure_Name,Report_Id,ReportType,Report_Quarter,Period_Start_Date,Period_End_Date,Root_Companies_Id
	)t1
	
)t2


	
*/


Insert into RPT.PROVIDER_SCORECARD(Measure_id,Measure_Name,Measure_Title,MEASURE_SUBTITLE,Measure_Type,NUM_COUNT,DEN_COUNT,Excl_Count,Rexcl_Count,Gaps,Result,Target,To_Target,Report_Id,ReportType,Report_Quarter,Period_Start_Date,Period_End_Date,Root_Companies_Id)

	Select
		Measure_id,
		Measure_Name,
		Measure_Title,
		MEASURE_SUBTITLE,
		Measure_Type,
		NUM_COUNT,
		DEN_COUNT,
		Excl_Count,
		Rexcl_Count,
		Gaps,
		Result,
		Target,
		Case
			when Result > Target then Floor((DEN_COUNT*Target)/100)-NUM_COUNT
			when Result <= Target then 0
		end as To_Target,
		Report_Id,
		ReportType,
		Report_Quarter,
		Period_Start_Date,
		Period_End_Date,
		Root_Companies_Id
	From
	(
		
			Select
				Measure_id,
				Measure_Name,
				Measure_Title,
				MEASURE_SUBTITLE,
				Measure_Type,
				SUM(Cast(NUM_COUNT as INT)) as NUM_COUNT,
				SUM(Cast(Den_excl as INT)) as DEN_COUNT,
				SUM(Cast(Excl_Count as INT)) as Excl_Count,
				Sum(Cast(Rexcl_count as INT)) as Rexcl_Count,
				SUM(Cast(NUM_COUNT as INT))  as Gaps,
				Case
					when SUM(Cast(DEN_Excl as Float))>0 Then Round((SUM(cast(NUM_COUNT as Float))/ISNULL(NULLIF(SUM(Cast(DEN_Excl as Float)),0),1))*100,2)
					else 0
				end as Result,
				Target,
				Report_Id,
				ReportType,
				Report_Quarter,
				Period_Start_Date,
				Period_End_Date,
				Root_Companies_Id
			From
			(
				Select distinct
					m.EMPI
					,a.NPI as Provider_id
					,a.NPI as PCP_NPI
					,a.Prov_Name as PCP_Name
					,a.Specialty
					,a.Practice as Practice_Name
					,m.Measure_id
					,m.Measure_Name
					,l.measure_Title
					,l.Measure_SubTitle
					,m.MEASURE_TYPE
					,Case
						when NUM=1 and excl=0 and rexcl=0 Then 1
						else 0
					end as NUM_COUNT
					,DEN as DEN_COUNT
					,Case
						When DEN=1 and Excl=0 and Rexcl=0 Then 1
						else 0
					End as Den_excl
					,Excl as Excl_Count
					,Rexcl as Rexcl_count
					,Report_Id
					,m.ReportType
					,Report_Quarter
					,Period_Start_Date
					,Period_End_Date
					,m.Root_Companies_Id
					,l.Target
					,m.Code
					,m.DateofService
					,m.DischargeDate
				From RPT.MEASURE_DETAILED_LINE m
				Join RPT.ConsolidatedAttribution_Snapshot a on
					m.EMPI=a.EMPI and
					a.Attribution_Type='Ambulatory_PCP'
				Join RFT.UHN_measuresList l on
					m.Measure_Id=l.measure_id
				where 
					a.EnrollmentStatus='Active' and
					a.NPI!='' and
					m.MEASURE_ID=@measure_id and
					REPORT_ID=@reportId 

			)tbl
			Group By
				Measure_id,Measure_Name,Measure_Title,MEASURE_SUBTITLE,Report_Id,ReportType,Report_Quarter,Period_Start_Date,Period_End_Date,Root_Companies_Id,Measure_Type,target
		
	)tbl2		
			
			



		SET @PERF_ROW = @@ROWCOUNT
		SET @PERF_DURATION = DATEDIFF(MINUTE,@PERF_START,GETDATE());
		EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, 'Enrollment Completed', @PERF_ROW, @DURATION_IN_MIN=@PERF_DURATION, @LOG2ID=@LOGID;
		EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, @LOG2ID=@LOGID, @END_FLAG=1;

END TRY
BEGIN CATCH
		BEGIN
			EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, 'FATAL ERROR', @LOG2ID=@LOGID;
			RAISERROR ('',16,0)
			RETURN 16
        END
	END CATCH
RETURN 0
END

GO
