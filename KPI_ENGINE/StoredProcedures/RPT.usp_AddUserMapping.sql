SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
CREATE PROCEDURE RPT.usp_AddUserMapping (@userEmail varchar(100), @userName varchar(100), @userLevel varchar(10), @providerNPI varchar(20), @practiceName varchar(max))
AS
BEGIN

	DECLARE @practiceNames as Table (SR INT IDENTITY(1,1), PracticeName varchar(200));
	INSERT INTO @practiceNames (PracticeName)
	SELECT DISTINCT *
	FROM STRING_SPLIT(@practiceName,'|');

	IF @userLevel = 'Provider'
	BEGIN

		--Provider
		Insert into RPT.Provider_Scorecard_Mail
		(Practice,Specialty,Provider,Email_Id,UserName,ProviderNPI,midlevel,ROOT_COMPANIES_ID)
		Select Prov_Grp_Name,Prov_Spec_desc,Prov_Name,@userEmail,@userName,PROV_NPI,case when _PROV_UDF_01_='midlevel' then 1 else 0 end,159 
		from Provider 
		where _PROV_UDF_02_='y' and _PROV_UDF_04_='Primary Address' and Prov_NPI=@providerNPI

	END

	IF @userLevel = 'Practice'
	BEGIN
		
		DECLARE 
			@practiceLoops INT = (SELECT COUNT(DISTINCT PracticeName) FROM @practiceNames)
			,@counter int = 1
		;

		SELECT * FROM @practiceNames;
		SELECT @practiceLoops;

		WHILE (@counter<=@practiceLoops)
		BEGIN
			
			DECLARE @singlePracticeName varchar(200) = (SELECT TRIM(PracticeName) FROM @practiceNames WHERE SR = @counter)

			SELECT @counter, @singlePracticeName;

			--Practice
			Insert into RPT.Provider_Scorecard_Mail
			(Practice,Specialty,Provider,Email_Id,UserName,ProviderNPI,midlevel,ROOT_COMPANIES_ID)
			Select Prov_GRP_Name,Prov_Spec_desc2,Prov_Name,@userEmail,@userName,PROV_NPI,case when _PROV_UDF_01_='midlevel' then 1 else 0 end,159 
			from Provider 
			where _PROV_UDF_02_='y' and _PROV_UDF_04_='Primary Address' and PROV_GRP_NAME = @singlePracticeName

			DELETE @practiceNames WHERE SR = @counter;
			SET @counter += 1;

		END

	END

END
GO
