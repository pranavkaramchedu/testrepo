SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
CREATE PROC [dbo].[UpdateNetworkScores]
As

BEGIN
DECLARE @INFO VARCHAR(8000)
					,	@TRUE BIT = 1
					,	@FALSE BIT = 0
					,	@DB_ID INT = DB_ID()
					,	@LOGID INT = 0
					,	@PROC VARCHAR(500)=OBJECT_SCHEMA_NAME(@@PROCID)+'.'+OBJECT_NAME(@@PROCID)
					,	@PERF_START DATETIME
					,	@PERF_DURATION INT
					,	@PERF_ROW INT
					,	@RC	INT
					;

		EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, @INFO=@INFO, @LOG2ID=@LOGID OUT;
		DECLARE @PROCFULLNAME VARCHAR(500) = DB_NAME()+'.'+@PROC;
		----EXEC dbo.SP_MI_UTIL_SEND_MAIL @PROCFULLNAME, 'STARTED', 1, @ECHO=0; 
	DECLARE @MSG VARCHAR(1000)
	DECLARE @PERF_START_PROC DATETIME 
	DECLARE @PERF_RPS NUMERIC(10,2) 
	DECLARE @ROWS INT 
	DECLARE @PERF_ROWS INT 
	SET @PERF_ROWS = 0 
	SET @PERF_START = GETDATE() 
	SET @PERF_START_PROC = GETDATE() 

BEGIN TRY


-- Initialize variables
Declare @rundate date=GetDate();

declare @meas_year varchar(4)=Year(Dateadd(month,-2,@rundate))
declare @rootId varchar(10)='159'
Declare @startDate Date;
Declare @enddate date;
Declare @quarter varchar(20);
Declare @reportId INT;

-- Get The latest Report id
	--exec GetReportDetail @rundate=@rundate,@rootId=@rootId,@startdate=@startdate output,@enddate=@enddate output,@quarter=@quarter output,@reportId=@reportId output
	Set @reportId=(select Top 1 ReportId  from RPT.Report_details where root_companies_id=@rootId Order by ReportEndDate desc)
	Set @startDate=(select Top 1 ReportStartDate from RPT.Report_details where root_companies_id=@rootId Order by ReportEndDate desc)
	Set @enddate=(select Top 1 ReportEndDate  from RPT.Report_details where root_companies_id=@rootId Order by ReportEndDate desc)
	Set @quarter=(select Top 1 ReportQuarter  from RPT.Report_details where root_companies_id=@rootId Order by ReportEndDate desc)
-- Logic to add historical measures from last quarter if new data is not available

EXEC SP_KPI_DROPTABLE '#uhn_measures'
select 
	PROVIDER_ID
	,PCP_NPI
	,PCP_NAME
	,SPECIALTY
	,PRACTICE_NAME
	,MEASURE_ID
	,MEASURE_NAME
	,MEASURE_TITLE
	,Measure_Subtitle
	,Measure_Type
	,Num_Count
	,Den_Count
	,Excl_Count
	,Rexcl_Count
	,Gaps
	,Result
	,Target
	,TO_Target
	,NetworkResult
	,REPORT_ID
	,ReportType
INTO #uhn_measures
from RPT.PROVIDER_SCORECARD ps1
where 
	MEASURE_ID in(select Measure_id from RFT.UHN_MeasuresList where UHNCalculated=1) and
	REPORT_ID=(Select top 1 REPORT_ID from RPT.PROVIDER_SCORECARD ps2 where ps1.ROOT_COMPANIES_ID=@rootId and ps2.MEASURE_ID=ps1.MEASURE_ID and ps2.PERIOD_START_DATE>=@startDate order by REPORT_ID desc)


EXEC SP_KPI_DROPTABLE '#uhn_measures_latest'
select
	*
Into #uhn_measures_latest
From RPT.PROVIDER_SCORECARD s
Where
	MEASURE_ID in(select Measure_id from RFT.UHN_MeasuresList where UHNCalculated=1) and
	s.REPORT_ID=@reportId


Insert into RPT.PROVIDER_SCORECARD(PROVIDER_ID,PCP_NPI,PCP_NAME,SPECIALTY,PRACTICE_NAME,MEASURE_ID,MEASURE_NAME,MEASURE_TITLE,MEASURE_SUBTITLE,MEASURE_TYPE,NUM_COUNT,DEN_COUNT,EXCL_COUNT,REXCL_COUNT,GAPS,RESULT,TARGET,TO_TARGET,REPORT_ID,ReportType,REPORT_RUN_DATE,Report_Quarter,PERIOD_START_DATE,PERIOD_END_DATE,NetworkResult,ROOT_COMPANIES_ID)
Select
	m.PROVIDER_ID
	,m.PCP_NPI
	,m.PCP_NAME
	,m.SPECIALTY
	,m.PRACTICE_NAME
	,m.MEASURE_ID
	,m.MEASURE_NAME
	,m.MEASURE_TITLE
	,m.MEASURE_SUBTITLE
	,m.MEASURE_TYPE
	,m.NUM_COUNT
	,m.DEN_COUNT
	,m.EXCL_COUNT
	,m.REXCL_COUNT
	,m.GAPS
	,m.RESULT
	,m.TARGET
	,m.TO_TARGET
	,@reportId
	,m.ReportType
	,GETDATE()
	,@quarter
	,@startdate
	,@enddate
	,m.NetworkResult
	,@rootId
From #uhn_measures m
Left outer Join #uhn_measures_latest ml on
	m.MEASURE_ID=ml.MEASURE_ID
Where
	ml.MEASURE_ID is null


	-- Calculate the networ result
	Drop table if exists #measures_networkscore
	Create Table #measures_networkscore
	(
		Measure_id varchar(20),
		Report_Quarter varchar(20),
		NetworkResult float
	)
	Insert into #measures_networkscore
	Select 
		* 
	from
	(
			Select
				MEASURE_ID
				,Report_Quarter
				,Case
					when Measure_id!='36' and Calc_DEN_COUNT>0 Then Round(((cast(NUM_COUNT as float)/ISNULL(Cast(Calc_DEN_COUNT as float),1))*100),2)
					when Measure_id!='36' and Calc_DEN_COUNT<=0 Then 0
					When Measure_id in('36') then Round((Cast(EDVisits as float)/ISNULL((select
                    sum(MM_UNITS)
                  from
                    MEMBER_MONTH mm
                    join RPT.ConsolidatedAttribution_Snapshot on RPT.ConsolidatedAttribution_Snapshot.EMPI = convert(varchar(100), mm.EMPI)
                    and Attribution_Type = 'Ambulatory_PCP'
                  Where
                    MEMBER_MONTH_START_DATE between DATEADD(yy, DATEDIFF(yy, 0, GETDATE()), 0)
                    and EOMONTH(Dateadd(mm, -1, Getdate()))
                    and 1 = 1
                    and "RPT"."ConsolidatedAttribution_Snapshot"."EnrollmentStatus" = 'Active'),1))*1000*12,0)
				end as NetworkResult
			From
			(

				select
					t1.MEASURE_ID
					,t1.Report_Quarter
					,SUM(Cast(Calc_NUM as INT)) as NUM_COUNT
					,SUM(Cast(Calc_DEN as INT)) as Calc_DEN_COUNT
					,count(Case When Excl=1 or REXCL=1 Then 1 end) as Exclusions
					,sum(Encounters) as EDVisits
				From
				(

					select distinct 
						MEASURE_ID
						,MEASURE_NAME
						,a.EMPI
						,a.PayerId
						,MEM_FNAME
						,MEM_MNAME
						,MEM_LNAME
						,MEM_DOB
						,a.MEM_GENDER
						,ENROLLMENT_STATUS
						,Den
						,Case
							When Num=1 and Excl=0 and Rexcl=0 Then 1
							Else 0
						end as Calc_NUM
						,Case
							When DEN=1 and Excl=0 and Rexcl=0 Then 1
							Else 0
						end as Calc_DEN
						,Excl
						,Rexcl
						,encounters
						,DischargeDate
						--,Code
						--,DateofService
						,Report_Quarter
					From RPT.Measure_detailed_line a
					 Cross Apply (
                        Select
                          Top 1 RPT.ConsolidatedAttribution_Snapshot.*
                        From
                          RPT.ConsolidatedAttribution_Snapshot
                          join RPT.Provider_Scorecard_Mail on ProviderNPI = NPI
                          Join open_empi_master o on EMPI = EMPI_ID
						  Join RFT.UHN_MeasureSpecialtiesMapping msm on a.Measure_id = msm.measure_id
							  and RPT.ConsolidatedAttribution_Snapshot.Specialty = msm.Specialty
							  and 1 =case
								when accesstomidlevel = 1 then 1
								when accesstomidlevel = 0
								and midlevel = 0 then 1
								else 0
							  end
                        Where
                          RPT.ConsolidatedAttribution_Snapshot.EMPI = a.EMPI
                          and NPI!=''
                          and "RPT"."ConsolidatedAttribution_Snapshot"."EnrollmentStatus" = 'Active'
						  order by Attribution_Type
                      ) ca
					Where
                  		a.root_companies_id=@rootId
						
						and ReportType!='Network'
				)t1
				Group by
					MEASURE_ID,Report_Quarter
			)t2

			Union All

			Select distinct
				s.Measure_id
				,Report_Quarter
				,AVG(result) as NetworkResult
			From RPT.PROVIDER_SCORECARD s
			Cross Apply (
                        Select
                          distinct RPT.ConsolidatedAttribution_Snapshot.Specialty
                        From
                          RPT.ConsolidatedAttribution_Snapshot
                          join RPT.Provider_Scorecard_Mail on Provider = Prov_Name
                          Join open_empi_master o on EMPI = EMPI_ID
                        Where
                          NPI!=''
                          and "RPT"."ConsolidatedAttribution_Snapshot"."EnrollmentStatus" = 'Active'
						  
                      ) ca
			Join RFT.UHN_MeasuresList ml on
				s.MEASURE_ID=ml.Measure_id
			Where
				s.ROOT_COMPANIES_ID=@rootId and
				(
					(
						ml.UHNCalculated=1 and
						ml.ReportType='Provider'
					)
					or
					ml.ReportType in('Network','Hospital')
				)
			Group by 
				s.Measure_id,Report_Quarter

			Union all

			select
			  MEASURE_ID,
			  Report_Quarter,
			  ROUND(Cast(AVG(Result) as Decimal(18, 2)), 2) as NetworkResult
			from
			  (
				Select distinct 
					a.PRACTICE_NAME,
					ml.MEASURE_TITLE,
					ml.MEASURE_SUBTITLE,
					a.MEASURE_ID,
					ml.Measure_name,
					DEN_COUNT,
					NUM_COUNT,
					GAPS,
					RESULT,
					NetworkResult,
					a.TARGET,
					TO_TARGET,
					Report_Quarter
					
				from RPT.PROVIDER_SCORECARD a
				Join RFT.UHN_MeasuresList ml on
					a.MEASURE_ID=ml.Measure_id
				cross apply (
					Select
					  distinct RPT.ConsolidatedAttribution_Snapshot.Practice
					from
					  RPT.ConsolidatedAttribution_Snapshot
					  join RPT.Provider_Scorecard_Mail on Provider = Prov_Name
					where
					  NPI != ''
				  ) CS
				where
				  a.root_companies_id = 159
				  and ml.UHNCalculated=1
				  and ml.ReportType = 'Practice'
				  and a.MEASURE_ID not in ('1','52')
				  
			  ) tbl
			Group By
			  MEASURE_ID,
			  Report_Quarter

			Union all

			select
			  MEASURE_ID,
			  Report_Quarter,
			  ROUND(Cast(AVG(Result) as Decimal(18, 2)), 2) as NetworkResult
			from
			  (
				Select distinct 
					ml.MEASURE_TITLE,
					ml.MEASURE_SUBTITLE,
					a.MEASURE_ID,
					ml.Measure_name,
					DEN_COUNT,
					NUM_COUNT,
					GAPS,
					RESULT,
					PRACTICE_NAME,
					a.TARGET,
					TO_TARGET,
					Report_Quarter,
					PROV_GRP_ID
				from RPT.PROVIDER_SCORECARD a
				Join PROVIDER p on 
					a.PRACTICE_NAME = p.PROV_GRP_NAME
				Join RFT.UHN_MeasuresList ml on
					a.MEASURE_ID=ml.Measure_id
				cross apply (
					Select
					  distinct RPT.ConsolidatedAttribution_Snapshot.Practice
					from
					  RPT.ConsolidatedAttribution_Snapshot
					  join RPT.Provider_Scorecard_Mail on ProviderNPI = NPI
					where
					  NPI != ''
					  and RPT.ConsolidatedAttribution_Snapshot.Practice=a.PRACTICE_NAME
				  ) CS
				where
				  a.root_companies_id = @rootId
				  and ml.UHNCalculated=1
				  and ml.ReportType = 'Practice'
				  and a.MEASURE_ID in ('1','52')
				  
			  ) tbl
			Group By
			  MEASURE_ID,
			  Report_Quarter



		)f1

		

		-- Update Networkscore in provider scorecard

		update s Set 
			s.NetworkResult=ns.NetworkResult
		from Rpt.PROVIDER_SCORECARD s
		join #measures_networkscore ns on
			s.MEASURE_ID=ns.Measure_id and
			s.Report_Quarter=ns.Report_Quarter
		Where
			s.Report_Quarter=@quarter

			
		SET @PERF_ROW = @@ROWCOUNT
		SET @PERF_DURATION = DATEDIFF(MINUTE,@PERF_START,GETDATE());
		EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, 'Enrollment Completed', @PERF_ROW, @DURATION_IN_MIN=@PERF_DURATION, @LOG2ID=@LOGID;
		EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, @LOG2ID=@LOGID, @END_FLAG=1;

END TRY
BEGIN CATCH
		BEGIN
			EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, 'FATAL ERROR', @LOG2ID=@LOGID;
			RAISERROR ('',16,0)
			RETURN 16
        END
	END CATCH
RETURN 0
END

GO
