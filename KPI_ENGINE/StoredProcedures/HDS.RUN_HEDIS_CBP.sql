SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

CREATE proc [HDS].[RUN_HEDIS_CBP] @meas_year nvarchar(4)
AS


-- Declare Variables

Declare @meas varchar(10)='CBP';
Declare @ce_startdt varchar(8);
Declare @ce_startdt1 varchar(8);
Declare @ce_middt varchar(8);
Declare @ce_enddt varchar(8);
DECLARE @pt_ctr Int=0;
DECLARE @i INT=0;
declare @patientid varchar(50);
declare @plan_ct INT;
declare @gender varchar(10);
declare @age INT;
Declare @latest_insenddate varchar(8);
Declare @plan1 varchar(20);
Declare @plan2 varchar(20);
Declare @planid varchar(20);
Declare @runid INT;




-- Set Measure dates
SET @ce_startdt=concat(@meas_year,'0101');
SET @ce_middt=concat(@meas_year,'0630');
SET @ce_enddt=concat(@meas_year,'1231');
SET @ce_startdt1=concat(@meas_year-1,'0101');

-- Clear #cbpdataset

drop table if exists #cbpdataset;

CREATE TABLE #cbpdataset (
  [patient_id] varchar(100) NOT NULL,
  [meas] varchar(20) DEFAULT NULL,
  [payer] varchar(100) DEFAULT NULL,
  [patient_gender] varchar(45) NOT NULL,
  [age] decimal(12,4) DEFAULT NULL,
  [orec] smallint DEFAULT NULL,
  [lis] smallint DEFAULT '0',
  [rexcl] smallint DEFAULT '0',
  [rexcld] smallint DEFAULT '0',
  [event] smallint DEFAULT '0',
  [CE] smallint DEFAULT '0',
  [excl] smallint DEFAULT '0',
  [num] smallint DEFAULT '0'
) ;
CREATE INDEX [idx1] ON #cbpdataset ([payer]);
CREATE INDEX [patient_id_UNIQUE] ON #cbpdataset ([patient_id],[payer]);


-- Create Eligible Patient LIST

drop table if exists #cbp_memlist; 

CREATE TABLE #cbp_memlist (
    memid varchar(100)
    
);

CREATE CLUSTERED INDEX ix_memlist_memid ON #cbp_memlist ([memid]);

insert into #cbp_memlist
SELECT DISTINCT en.MemID FROM HDS.HEDIS_MEMBER_GM gm
join hds.HEDIS_MEMBER_EN en on en.MemID=gm.MemID and en.MEASURE_ID=gm.MEASURE_ID
WHERE en.MEASURE_ID='CBP' and
en.StartDate<=@ce_enddt AND FinishDate>=@ce_startdt
AND YEAR(@ce_enddt)-YEAR(DOB) BETWEEN 18 AND 85 ORDER BY 1;

-- Create Temp Table for enrollment

drop table if exists #cbp_tmpsubscriber;

CREATE TABLE #cbp_tmpsubscriber (
    memid varchar(100),
    dob varchar(8),
	gender varchar(1),
	payer varchar(50),
	StartDate varchar(8),
	EndDate varchar(8),
);

CREATE CLUSTERED INDEX ix_tmpsub_memid ON #cbp_tmpsubscriber ([memid],[StartDate],[EndDate]);

insert into #cbp_tmpsubscriber
SELECT en.MemID,gm.DOB,gm.Gender,en.Payer,en.StartDate,en.FinishDate  FROM HDS.HEDIS_MEMBER_GM gm
join hds.HEDIS_MEMBER_EN en on en.MemID=gm.MemID and en.MEASURE_ID=gm.MEASURE_ID
WHERE en.MEASURE_ID='CBP' and
en.StartDate<=@ce_enddt AND FinishDate>=@ce_startdt
AND YEAR(@ce_enddt)-YEAR(DOB) BETWEEN 18 AND 85 ORDER BY en.MemID,en.StartDate,en.FinishDate;



-- Get Patient COUNT

SELECT @pt_ctr=COUNT(*)  FROM #cbp_memlist;

PRINT 'pt_ctr:' + Cast(@pt_ctr as nvarchar);

-- Loop through enrollment for each patient

While @i<@pt_ctr
	BEGIN
		-- Get Patient to Loop
		SELECT  @patientid=memid FROM #cbp_memlist ORDER BY memid ASC OFFSET  @i ROWS FETCH NEXT 1 ROWS ONLY ;
		
		-- ReadingGender and age
		SELECT Top 1 @gender=gender,@age=YEAR(@ce_enddt)-YEAR(DOB) FROM #cbp_tmpsubscriber WHERE memid=@patientid;
		
		-- Check for dual eligibility by checking if the patient is enrolled in more than one plan at the end of Enrollment
		
		SELECT Top 1 @latest_insenddate=EndDate FROM #cbp_tmpsubscriber WHERE  memid=@patientid  AND StartDate<=@ce_enddt ORDER BY StartDate DESC,EndDate Desc  ;
		
		-- Check for plan count patient is enrolled in during the end of measurement year
		
		SELECT @plan_ct=COUNT(*) FROM #cbp_tmpsubscriber WHERE  memid=@patientid AND @latest_insenddate BETWEEN StartDate AND EndDate;
		
		-- If only one plan, then skip the Payer reporting logic
		
		if(@plan_ct=1)
		BEGIN
			
			SELECT @planid=payer FROM #cbp_tmpsubscriber WHERE  memid=@patientid AND @latest_insenddate BETWEEN StartDate AND EndDate ORDER BY payer;
		
			-- If plan is Dual , Insert MCR & MCD	
			
			If(@planid in('MMP','SN3','MDE'))
			BEGIN
				IF(@planid in('MMP','SN3'))
				Begin
					Insert INTO #cbpdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
				END
				
				SET @planid='MCD';
				Insert INTO #cbpdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
				
				SET @planid='MCR';
				Insert INTO #cbpdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
				
			END
			
			Else if(@planid IN('MD','MLI','MRB'))
			Begin
				SET @planid='MCD';
				Insert INTO #cbpdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
			END
			
			Else If(@planid IN('SN1','SN2'))
			Begin
				Insert INTO #cbpdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
				
				SET @planid='MCR';
				Insert INTO #cbpdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
			END
			ELSE
			BEGIN
				Insert INTO #cbpdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
			END
		END
				
		-- Compare Plans if Plan count is more than one	
		if(@plan_ct>1)
		BEGIN
			
			-- Read First Plan
			SELECT  @plan1=payer FROM #cbp_tmpsubscriber where memid=@patientid and @latest_insenddate between StartDate and EndDate order by 1 OFFSET  0 ROWS FETCH NEXT 1 ROWS ONLY ;
					
			-- Read second plan
			SELECT  @plan2=payer FROM #cbp_tmpsubscriber where memid=@patientid and @latest_insenddate between StartDate and EndDate order by 1 OFFSET  1 ROWS FETCH NEXT 1 ROWS ONLY ;
			
			
			
			If(@plan1 IN('MCR','MP','MC','SN1','SN2','MCS') and @plan2 IN('PPO','POS','HMO','CEP'))
			BEGIN
				if(@plan1 IN('SN1','SN2'))
				BEGIN
					SET @planid=@plan1;
					Insert INTO #cbpdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
					
					SET @planid='MCR';
					Insert INTO #cbpdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
					
				END
				
				if(@plan1 in ('MCR','MP','MC','MCS'))
				Begin
					
					Set @planid=@plan1;
					Insert INTO #cbpdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
				
				END

			END
			
			Else If(@plan2 IN('MCR','MP','MC','SN1','SN2','MCS') AND @plan1 IN('PPO','POS','HMO','CEP'))
			BEGIN
			
				IF(@plan2 IN('SN1','SN2'))
				Begin
					
					Set @planid=@plan2;
					Insert INTO #cbpdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
				
					Set @planid='MCR';
					Insert INTO #cbpdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
					
				END
				
				
				IF(@plan2 IN('MCR','MP','MC','MCS'))
				BEGIN
					
					Set @planid=@plan2;
					
					Insert INTO #cbpdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
					
				END
			END
			
			ELSE IF(@plan1 IN('PPO','POS','HMO','CEP') AND @plan2 IN('MD','MLI','MRB'))
			BEGIN
			
				Set @planid=@plan1;
				Insert INTO #cbpdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
					
			END
			
			ELSE IF(@plan2 IN('PPO','POS','HMO','CEP') AND @plan1 IN('MD','MLI','MRB'))
			BEGIN
			
				Set @planid=@plan2;
				Insert INTO #cbpdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
				
			END
			
			ELSE IF(@plan1 IN('MDE','SN3','MMP') or @plan2 in('MDE','SN3','MMP'))
			BEGIN
			
				if(@plan1 IN('SN3','MMP'))
				Begin
					Set @planid=@plan1;
					Insert INTO #cbpdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
				End
				
				if(@plan2 IN('SN3','MMP'))
				Begin
					Set @planid=@plan2
					Insert INTO #cbpdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
				END
				
					
				Set @planid='MCR';
				Insert INTO #cbpdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
				
				Set @planid='MCD';
				Insert INTO #cbpdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
				
			END
			/*
			ELSE IF(@plan2 IN('SN3','MMP','MDE'))
			BEGIN
			
				if(@plan2 IN('SN3','MMP'))
				Begin
					Set @planid=@plan2
					Insert INTO #cbpdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
				END
				
					
				Set @planid='MCR';
				Insert INTO #cbpdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
				
				Set @planid='MCD';
				Insert INTO #cbpdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
			END
			*/
			ELSE
			BEGIN
			
				Set @planid=@plan1;
				
				Insert INTO #cbpdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
				
				Set @planid=@plan2;
				Insert INTO #cbpdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
			END
			
		END
		
		Set @i=@i+1;
		
	END;
	
-- Continuous Enrollment
				
	DROP TABLE IF EXISTS #cbp_contenroll;
	
	CREATE table #cbp_contenroll
	(
		Memid varchar(50),
		CE INT
	);

	CREATE CLUSTERED INDEX ix_cbp_contenroll ON #cbp_contenroll ([memid]);

	
	Insert into #cbp_contenroll
	SELECT MemID,SUM(ceflag) AS CE FROM
	(
	
		-- CE Check for 1st year
		SELECT MemID,SUM(coverage) AS coverage, 1 AS ceflag FROM
		(
			SELECT MemID,Payer,start_date,end_date,DATEDIFF(day,start_date,end_date)+1 AS coverage FROM
			(

				SELECT s.MemID,s.Payer,s.FinishDate,
				CASE
				WHEN StartDate<@ce_startdt THEN @ce_startdt
				ELSE StartDate
				END AS start_date,
				CASE
				WHEN FinishDate>@ce_enddt THEN @ce_enddt
				ELSE FinishDate
				END AS end_date
				FROM HEDIS_MEMBER_EN s
				JOIN 
				(
					SELECT MemID,SUM(diff) as diff FROM
					(
						SELECT MemID,nextstartdate,FinishDate,
						CASE
						WHEN DATEDIFF(day,FinishDate,nextstartdate)<=1 THEN 0
						ELSE 1
						END AS diff
						  FROM
						  (
							SELECT MemID,StartDate,FinishDate,
							CASE
							WHEN nextstartdate IS NULL THEN FinishDate
							ELSE nextstartdate
							END AS nextstartdate FROM
							(
								SELECT s.MemID,s.StartDate,s.FinishDate,
								(
									SELECT Top 1 s1.StartDate
									FROM HEDIS_MEMBER_EN s1
									WHERE s1.MEASURE_ID=s.MEASURE_ID AND s1.MemID=s.MemID AND s.FinishDate<s1.StartDate AND s1.StartDate<=@ce_enddt AND s1.FinishDate>=@ce_startdt
									ORDER BY s1.MemID,s1.StartDate,s1.FinishDate
								) AS nextstartdate

								 FROM HEDIS_MEMBER_EN s
								WHERE MEASURE_ID='CBP' AND s.StartDate<=@ce_enddt AND s.FinishDate>=@ce_startdt
							)t
						)t1 
					)t2 GROUP BY MemID HAVING SUM(diff)<=1
				)
				s1 ON s.MemID=s1.MemID
				WHERE s.MEASURE_ID='CBP'
				-- SQLINES DEMO *** 100001
				AND s.StartDate<=@ce_enddt AND s.FinishDate>=@ce_startdt
	
			)t2
		)t3 GROUP BY MemID HAVING sum(coverage)>=(DATEPART(dy, @ce_enddt)-45)

		Union All

		-- Anchor Date
	 
		SELECT MemID,SUM(DATEDIFF(day,start_date,end_date)) AS coverage, 1 AS ceflag FROM
			(
			SELECT MemID,FinishDate,
			CASE
				WHEN StartDate<@ce_startdt THEN @ce_startdt
				ELSE StartDate
			END AS start_date,
			CASE
				WHEN FinishDate>@ce_enddt THEN @ce_enddt
				ELSE FinishDate
			END AS end_date
			FROM HEDIS_MEMBER_EN 
			WHERE MEASURE_ID='CBP'
			-- SQLINES DEMO *** 0000          
			AND @ce_enddt BETWEEN StartDate AND FinishDate
		
		)t4  GROUP BY MemID
 
	)cont_enroll GROUP BY MemID  HAVING SUM(ceflag)=2 order by MemID;
	
	update #cbpdataset set CE=1 from #cbpdataset ds join #cbp_contenroll ce on ds.patient_id=ce.MemID;
	

	-- AdvancedIllness

	drop table if exists #cbp_advillness;
	
	CREATE table #cbp_advillness
		(
			Memid varchar(50),
			
			servicedate varchar(8),
			claimid int
		);

	CREATE CLUSTERED INDEX ix_cbp_advillness ON #cbp_advillness ([memid],[servicedate],[claimid]);

	Insert into #cbp_advillness
	select MemID,Date_S,claimid from HEDIS_VISIT v
	where MEASURE_ID='CBP' and HCFAPOS!=81 and suppdata='N' and Date_S between @ce_startdt1 and @ce_enddt
	and (
	Diag_I_1 in(select code from VALUESET_TO_CODE where Value_Set_Name='Advanced Illness')
	or
	Diag_I_2 in(select code from VALUESET_TO_CODE where Value_Set_Name='Advanced Illness')
	or
	Diag_I_3 in(select code from VALUESET_TO_CODE where Value_Set_Name='Advanced Illness')
	or
	Diag_I_4 in(select code from VALUESET_TO_CODE where Value_Set_Name='Advanced Illness')
	or
	Diag_I_5 in(select code from VALUESET_TO_CODE where Value_Set_Name='Advanced Illness')
	or
	Diag_I_6 in(select code from VALUESET_TO_CODE where Value_Set_Name='Advanced Illness')
	or
	Diag_I_7 in(select code from VALUESET_TO_CODE where Value_Set_Name='Advanced Illness')
	or
	Diag_I_8 in(select code from VALUESET_TO_CODE where Value_Set_Name='Advanced Illness')
	or
	Diag_I_9 in(select code from VALUESET_TO_CODE where Value_Set_Name='Advanced Illness')
	or
	Diag_I_10 in(select code from VALUESET_TO_CODE where Value_Set_Name='Advanced Illness')
	or
	CPT in(select code from VALUESET_TO_CODE where Value_Set_Name='Advanced Illness')
	or
	CPT2 in(select code from VALUESET_TO_CODE where Value_Set_Name='Advanced Illness')
	or
	HCPCS in(select code from VALUESET_TO_CODE where Value_Set_Name='Advanced Illness')
	or 
	Proc_I_1 in(select code from VALUESET_TO_CODE where Value_Set_Name='Advanced Illness')
	or 
	Proc_I_2 in(select code from VALUESET_TO_CODE where Value_Set_Name='Advanced Illness')
	or 
	Proc_I_3 in(select code from VALUESET_TO_CODE where Value_Set_Name='Advanced Illness')
	or 
	Proc_I_4 in(select code from VALUESET_TO_CODE where Value_Set_Name='Advanced Illness')
	or 
	Proc_I_5 in(select code from VALUESET_TO_CODE where Value_Set_Name='Advanced Illness')
	)
	
	-- Required Exclusion

	drop table if exists #cbp_reqdexcl
	CREATE table #cbp_reqdexcl
	(
		Memid varchar(50)
			
	);

	CREATE CLUSTERED INDEX idx_cbp_reqdexcl ON #cbp_reqdexcl ([memid]);


	insert into #cbp_reqdexcl
	select distinct memid from(

	Select Memid from HEDIS_VISIT_E where Measure_id='CBP' and sdate between @ce_startdt and @ce_enddt and activity IN(select code from VALUESET_TO_CODE where Value_Set_Name in('Palliative Care Assessment','Palliative Care Intervention','Palliative Care Encounter'))

	Union all

	select Memid from HEDIS_OBS where Measure_id='CBP' and date between @ce_startdt and @ce_enddt and OCode IN(select code from VALUESET_TO_CODE where Value_Set_Name in('Palliative Care Assessment','Palliative Care Intervention','Palliative Care Encounter'))
		
	union all

	select MemID from HEDIS_VISIT v
	where MEASURE_ID='CBP' and HCFAPOS!=81 and Date_S between @ce_startdt and @ce_enddt
	and (
	Diag_I_1 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Palliative Care Assessment','Palliative Care Intervention','Palliative Care Encounter'))
	or
	Diag_I_2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Palliative Care Assessment','Palliative Care Intervention','Palliative Care Encounter'))
	or
	Diag_I_3 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Palliative Care Assessment','Palliative Care Intervention','Palliative Care Encounter'))
	or
	Diag_I_4 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Palliative Care Assessment','Palliative Care Intervention','Palliative Care Encounter'))
	or
	Diag_I_5 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Palliative Care Assessment','Palliative Care Intervention','Palliative Care Encounter'))
	or
	Diag_I_6 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Palliative Care Assessment','Palliative Care Intervention','Palliative Care Encounter'))
	or
	Diag_I_7 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Palliative Care Assessment','Palliative Care Intervention','Palliative Care Encounter'))
	or
	Diag_I_8 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Palliative Care Assessment','Palliative Care Intervention','Palliative Care Encounter'))
	or
	Diag_I_9 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Palliative Care Assessment','Palliative Care Intervention','Palliative Care Encounter'))
	or
	Diag_I_10 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Palliative Care Assessment','Palliative Care Intervention','Palliative Care Encounter'))
	or
	CPT in(select code from VALUESET_TO_CODE where Value_Set_Name in('Palliative Care Assessment','Palliative Care Intervention','Palliative Care Encounter'))
	or
	CPT2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Palliative Care Assessment','Palliative Care Intervention','Palliative Care Encounter'))
	or
	HCPCS in(select code from VALUESET_TO_CODE where Value_Set_Name in('Palliative Care Assessment','Palliative Care Intervention','Palliative Care Encounter'))
	or 
	Proc_I_1 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Palliative Care Assessment','Palliative Care Intervention','Palliative Care Encounter'))
	or 
	Proc_I_2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Palliative Care Assessment','Palliative Care Intervention','Palliative Care Encounter'))
	or 
	Proc_I_3 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Palliative Care Assessment','Palliative Care Intervention','Palliative Care Encounter'))
	or 
	Proc_I_4 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Palliative Care Assessment','Palliative Care Intervention','Palliative Care Encounter'))
	or 
	Proc_I_5 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Palliative Care Assessment','Palliative Care Intervention','Palliative Care Encounter'))
	)
	)t1 
	
	update #cbpdataset set rexcld=1 from #cbpdataset ds join #cbp_reqdexcl re on ds.patient_id=re.memid;
	
	-- Members with Institutinal SNP

	UPDATE #cbpdataset SET #cbpdataset.rexcl=1 FROM #cbpdataset ds JOIN HEDIS_MEMBER_EN s on ds.patient_id=s.MemID and s.MEASURE_ID='CBP' WHERE  ds.age>=66 AND ds.payer IN('MCR','MCS','MP','MC','SN2','SN1') AND s.StartDate<=@ce_enddt AND s.FinishDate>=@ce_startdt AND s.Payer='SN2';


	-- LTI Exclusion
	
	drop table if exists #cbp_LTImembers;
	
	CREATE table #cbp_LTImembers
	(
		Memid varchar(50)
				
	);

	CREATE CLUSTERED INDEX idx_cbp_ltimembers ON #cbp_LTImembers ([memid]);

	Insert into #cbp_LTImembers
	SELECT DISTINCT Beneficiary_ID FROM HEDIS_MMDF WHERE Measure_ID='CBP' AND Run_Date BETWEEN @ce_startdt AND @ce_enddt AND LTI_Flag='Y';



	update #cbpdataset set rexcl=1 from #cbpdataset ds join #cbp_LTImembers re on ds.patient_id=re.Memid where ds.age>=66 AND ds.payer IN('MCR','MCS','MP','MC','MMP','SN1');

	

	-- Hospice Exclusion

	drop table if exists #cbp_hospicemembers;
	CREATE table #cbp_hospicemembers
	(
		Memid varchar(50)
		
	);

	CREATE CLUSTERED INDEX idx_hospicemembers ON #cbp_hospicemembers ([memid]);

	Insert into #cbp_hospicemembers
	select distinct t1.MemID from
	(
	select Memid from HEDIS_OBS where Measure_id='CBP' and date between @ce_startdt and @ce_enddt and OCode IN(select code from VALUESET_TO_CODE where Value_Set_Name in('Hospice Encounter','Hospice Intervention'))
	Union all
	Select Memid from HEDIS_VISIT_E where Measure_id='CBP' and sdate between @ce_startdt and @ce_enddt and activity IN(select code from VALUESET_TO_CODE where Value_Set_Name in('Hospice Encounter','Hospice Intervention'))

	Union all

	select Beneficiary_id as Memid from Hedis_MMDF where measure_id='CBP' and Run_date between @ce_startdt and @ce_enddt and Hospice='Y'

	Union all

	select MemID from HEDIS_VISIT v
	where MEASURE_ID='CBP' and HCFAPOS!=81 and Date_S between @ce_startdt and @ce_enddt
	and (
	Diag_I_1 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or
	Diag_I_2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or
	Diag_I_3 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or
	Diag_I_4 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or
	Diag_I_5 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or
	Diag_I_6 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or
	Diag_I_7 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or
	Diag_I_8 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or
	Diag_I_9 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or
	Diag_I_10 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or
	CPT in(select code from VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or
	CPT2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or
	HCPCS in(select code from VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or 
	Proc_I_1 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or 
	Proc_I_2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or 
	Proc_I_3 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or 
	Proc_I_4 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or 
	Proc_I_5 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or
	Rev in(select code from VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	)
	)t1
	
	update #cbpdataset set rexcl=1 from #cbpdataset ds join #cbp_hospicemembers hos on hos.memid=ds.patient_id;
			
				
	-- Frailty Members LIST
	drop table if exists #cbp_frailtymembers;
	
	CREATE table #cbp_frailtymembers
	(
		
		Memid varchar(50)
			
	);

	CREATE CLUSTERED INDEX idx_cbp_frailtymembers ON #cbp_frailtymembers ([memid]);

	Insert into #cbp_frailtymembers
	select MemID from HEDIS_VISIT v
	where MEASURE_ID='CBP' and HCFAPOS!=81 and suppdata='N' and Date_S between @ce_startdt and @ce_enddt
	and (
	Diag_I_1 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Frailty Diagnosis','Frailty Device','Frailty Encounter','Frailty Symptom'))
	or
	Diag_I_2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Frailty Diagnosis','Frailty Device','Frailty Encounter','Frailty Symptom'))
	or
	Diag_I_3 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Frailty Diagnosis','Frailty Device','Frailty Encounter','Frailty Symptom'))
	or
	Diag_I_4 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Frailty Diagnosis','Frailty Device','Frailty Encounter','Frailty Symptom'))
	or
	Diag_I_5 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Frailty Diagnosis','Frailty Device','Frailty Encounter','Frailty Symptom'))
	or
	Diag_I_6 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Frailty Diagnosis','Frailty Device','Frailty Encounter','Frailty Symptom'))
	or
	Diag_I_7 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Frailty Diagnosis','Frailty Device','Frailty Encounter','Frailty Symptom'))
	or
	Diag_I_8 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Frailty Diagnosis','Frailty Device','Frailty Encounter','Frailty Symptom'))
	or
	Diag_I_9 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Frailty Diagnosis','Frailty Device','Frailty Encounter','Frailty Symptom'))
	or
	Diag_I_10 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Frailty Diagnosis','Frailty Device','Frailty Encounter','Frailty Symptom'))
	or
	CPT in(select code from VALUESET_TO_CODE where Value_Set_Name in('Frailty Diagnosis','Frailty Device','Frailty Encounter','Frailty Symptom'))
	or
	CPT2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Frailty Diagnosis','Frailty Device','Frailty Encounter','Frailty Symptom'))
	or
	HCPCS in(select code from VALUESET_TO_CODE where Value_Set_Name in('Frailty Diagnosis','Frailty Device','Frailty Encounter','Frailty Symptom'))
	or 
	Proc_I_1 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Frailty Diagnosis','Frailty Device','Frailty Encounter','Frailty Symptom'))
	or 
	Proc_I_2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Frailty Diagnosis','Frailty Device','Frailty Encounter','Frailty Symptom'))
	or 
	Proc_I_3 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Frailty Diagnosis','Frailty Device','Frailty Encounter','Frailty Symptom'))
	or 
	Proc_I_4 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Frailty Diagnosis','Frailty Device','Frailty Encounter','Frailty Symptom'))
	or 
	Proc_I_5 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Frailty Diagnosis','Frailty Device','Frailty Encounter','Frailty Symptom'))
	or
	Rev in(select code from VALUESET_TO_CODE where Value_Set_Name in('Frailty Diagnosis','Frailty Device','Frailty Encounter','Frailty Symptom'))
	);


-- Required Exclusion 1

	-- Inpatient Stay List
	drop table if exists #cbp_inpatientstaylist;
	CREATE table #cbp_inpatientstaylist
	(
		Memid varchar(50),
		
		date_s varchar(8),
		claimid int
	);

	CREATE CLUSTERED INDEX ix_cbp_inpatientstaylist ON #cbp_inpatientstaylist ([memid],[date_s]);

	Insert into #cbp_inpatientstaylist
	select distinct MemID,Date_S,claimid from HEDIS_VISIT v where v.Measure_id='CBP' and v.HCFAPOS!=81 and suppdata='N' and v.date_disch between @ce_startdt1 and @ce_enddt and v.REV in (select code from VALUESET_TO_CODE where code_system='UBREV' and Value_Set_Name in('Inpatient Stay'));

	-- Non acute Inpatient stay list
	
	drop table if exists #cbp_noncauteinpatientstaylist;
	CREATE table #cbp_noncauteinpatientstaylist
		(
			Memid varchar(50),
			
			date_s varchar(8),
			claimid int
		);

	CREATE CLUSTERED INDEX ix_cbp_nonacuteinpatientstaylist ON #cbp_noncauteinpatientstaylist ([memid],[date_s]);

	Insert into #cbp_noncauteinpatientstaylist
	select distinct MemID,Date_S,claimid from HEDIS_VISIT v where v.Measure_id='CBP' and v.HCFAPOS!=81 and suppdata='N' and v.date_disch between @ce_startdt1 and @ce_enddt and (v.REV in (select code from VALUESET_TO_CODE where code_system='UBREV'and Value_Set_Name in('Nonacute Inpatient Stay')) or RIGHT('0000'+CAST(Trim(v.BillType) AS VARCHAR(4)),4) in (select code from VALUESET_TO_CODE where code_system='UBTOB' and Value_Set_Name in('Nonacute Inpatient Stay')))
	
	-- Outpatient and other visits
	drop table if exists #cbp_visitlist;
	CREATE table #cbp_visitlist
	(
		Memid varchar(50),
		
		date_s varchar(8),
		claimid int
	);

	CREATE CLUSTERED INDEX ix_cbp_visitlist ON #cbp_visitlist ([memid],[date_s]);

	Insert into #cbp_visitlist
	select distinct MemID,Date_S,claimid from HEDIS_VISIT v
	where MEASURE_ID='CBP' and HCFAPOS!=81 and suppdata='N' and Date_S between @ce_startdt1 and @ce_enddt
	and (
	Diag_I_1 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments','Nonacute Inpatient'))
	or
	Diag_I_2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments','Nonacute Inpatient'))
	or
	Diag_I_3 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments','Nonacute Inpatient'))
	or
	Diag_I_4 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments','Nonacute Inpatient'))
	or
	Diag_I_5 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments','Nonacute Inpatient'))
	or
	Diag_I_6 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments','Nonacute Inpatient'))
	or
	Diag_I_7 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments','Nonacute Inpatient'))
	or
	Diag_I_8 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments','Nonacute Inpatient'))
	or
	Diag_I_9 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments','Nonacute Inpatient'))
	or
	Diag_I_10 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments','Nonacute Inpatient'))
	or
	CPT in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments','Nonacute Inpatient'))
	or
	CPT2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments','Nonacute Inpatient'))
	or
	HCPCS in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments','Nonacute Inpatient'))
	or 
	Proc_I_1 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments','Nonacute Inpatient'))
	or 
	Proc_I_2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments','Nonacute Inpatient'))
	or 
	Proc_I_3 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments','Nonacute Inpatient'))
	or 
	Proc_I_4 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments','Nonacute Inpatient'))
	or 
	Proc_I_5 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments','Nonacute Inpatient'))
	or 
	Rev in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Observation','ED','Telephone Visits','Online Assessments','Nonacute Inpatient'))
	
	)

	-- Required exclusion table
	drop table if exists #cbp_reqdexcl1;
	CREATE table #cbp_reqdexcl1
	(
		Memid varchar(50)
			
	);

	CREATE CLUSTERED INDEX idx_cbp_reqdexcl1 ON #cbp_reqdexcl1 ([memid]);

	Insert into #cbp_reqdexcl1
	select distinct t3.Memid from(
	select t2.MemId from(
	select distinct t1.Memid,t1.date_s from(
	select Memid,date_s,claimid  from #cbp_visitlist 
	union all
	select na.Memid,na.Date_s,na.claimid from #cbp_noncauteinpatientstaylist na
	join #cbp_inpatientstaylist inp on na.Memid=inp.Memid and na.claimid=inp.claimid
	)t1
	Join #cbp_advillness a on a.Memid=t1.Memid and a.claimid=t1.claimid
	)t2 group by t2.Memid having count(t2.Memid)>1
	)t3 
	Join #cbp_frailtymembers f on f.Memid=t3.Memid

	update #cbpdataset set rexcl=1 from #cbpdataset ds
	join #cbp_reqdexcl1 re1 on re1.Memid=ds.patient_id and ds.age BETWEEN 66 AND 80;

-- Required Exclusion 2

	-- Acute Inpatient with Advanced Illness
	drop table if exists #cbp_reqdexcl2;
	CREATE table #cbp_reqdexcl2
	(
		Memid varchar(50)
				
	);

	CREATE CLUSTERED INDEX idx_cbp_reqdexcl2 ON #cbp_reqdexcl2 ([memid]);

	insert into #cbp_reqdexcl2
	select distinct t2.Memid from(
	select t1.MemID from (
	select distinct MemID,Date_S,claimid from HEDIS_VISIT v
	where MEASURE_ID='CBP' and HCFAPOS!=81 and suppdata='N' and Date_S between @ce_startdt1 and @ce_enddt
	and (
	Diag_I_1 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Acute Inpatient'))
	or
	Diag_I_2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Acute Inpatient'))
	or
	Diag_I_3 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Acute Inpatient'))
	or
	Diag_I_4 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Acute Inpatient'))
	or
	Diag_I_5 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Acute Inpatient'))
	or
	Diag_I_6 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Acute Inpatient'))
	or
	Diag_I_7 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Acute Inpatient'))
	or
	Diag_I_8 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Acute Inpatient'))
	or
	Diag_I_9 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Acute Inpatient'))
	or
	Diag_I_10 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Acute Inpatient'))
	or
	CPT in(select code from VALUESET_TO_CODE where Value_Set_Name in('Acute Inpatient'))
	or
	CPT2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Acute Inpatient'))
	or
	HCPCS in(select code from VALUESET_TO_CODE where Value_Set_Name in('Acute Inpatient'))
	or 
	Proc_I_1 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Acute Inpatient'))
	or 
	Proc_I_2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Acute Inpatient'))
	or 
	Proc_I_3 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Acute Inpatient'))
	or 
	Proc_I_4 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Acute Inpatient'))
	or 
	Proc_I_5 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Acute Inpatient'))
	)
	)t1
	Join #cbp_advillness a on a.Memid=t1.MemID and a.claimid=t1.claimid
	)t2
	join #cbp_frailtymembers f on f.Memid=t2.MemID

	update #cbpdataset set rexcl=1 from #cbpdataset ds
	join #cbp_reqdexcl2 re2 on re2.Memid=ds.patient_id and ds.age BETWEEN 66 AND 80;
	
			
	-- Required exclusion 3
	drop table if exists #cbp_reqdexcl3;
	CREATE table #cbp_reqdexcl3
	(
		Memid varchar(50)
			
	);

	CREATE CLUSTERED INDEX idx_cbp_reqdexcl3 ON #cbp_reqdexcl3 ([memid]);

	insert into #cbp_reqdexcl3
	select distinct t2.MemId from(
	select t1.Memid,t1.date_s,t1.claimid from(
	select inp.Memid,inp.date_s,inp.claimid from #cbp_inpatientstaylist inp
	left outer join #cbp_noncauteinpatientstaylist na on inp.Memid=na.Memid and inp.claimid=na.claimid
	where na.Memid is null
	)t1
	join #cbp_advillness a on a.Memid=t1.Memid and a.claimid=t1.claimid
	)t2
	join #cbp_frailtymembers f on f.Memid=t2.Memid

	update #cbpdataset set rexcl=1 from #cbpdataset ds
	join #cbp_reqdexcl3 re3 on re3.Memid=ds.patient_id and ds.age BETWEEN 66 AND 80;

		
-- RequiredExcl 4
	drop table if exists #cbp_reqdexcl4;
	CREATE table #cbp_reqdexcl4
	(
		Memid varchar(50)
				
	);

		CREATE CLUSTERED INDEX idx_cbp_reqdexcl4 ON #cbp_reqdexcl4 ([memid]);

	insert into #cbp_reqdexcl4
	select t1.Memid from(
	select Memid from HEDIS_PHARM where Measure_id='CBP'  and suppdata='N' and PrServDate between @ce_startdt1 and @ce_enddt and NDC in(select code from MEDICATION_LIST_TO_CODES where Medication_List_Name='Dementia Medications')
	)t1
	Join #cbp_frailtymembers f on f.MemId=t1.Memid;

	update #cbpdataset set rexcl=1 from #cbpdataset ds
	join #cbp_reqdexcl4 re4 on re4.Memid=ds.patient_id and ds.age BETWEEN 66 AND 80;
	
-- Required Exclusion 5

	update #cbpdataset set rexcl=1 from #cbpdataset ds
	join #cbp_frailtymembers re4 on re4.Memid=ds.patient_id and ds.age>=81;
	
-- Optional Exclusion 1 and 3
	drop table if exists #cbp_optexcl;
	CREATE table #cbp_optexcl
	(
		Memid varchar(50)
				
	);

		CREATE CLUSTERED INDEX idx_cbp_optexcl ON #cbp_optexcl ([memid]);

	insert into #cbp_optexcl
	select distinct Memid from(
-- Optional Exclusion Kidney 
	select MemId from HEDIS_DIAG where Measure_id='CBP' and sdate <=@ce_enddt and sdate!=''  and dcode IN(select code from VALUESET_TO_CODE where Value_Set_Name in('ESRD Diagnosis','Dialysis Procedure','Nephrectomy','Kidney Transplant','History of Kidney Transplant')) 
	Union all
	select MemID from HEDIS_LAB where Measure_id='CBP' and date_s <= @ce_enddt and date_s!='' and (LOINC in(select code from VALUESET_TO_CODE where Value_Set_Name in('ESRD Diagnosis','Dialysis Procedure','Nephrectomy','Kidney Transplant','History of Kidney Transplant')) or CPT_Code IN(select code from VALUESET_TO_CODE where Value_Set_Name in('ESRD Diagnosis','Dialysis Procedure','Nephrectomy','Kidney Transplant','History of Kidney Transplant')))
	union all
	select MemId from HEDIS_OBS where Measure_id='CBP' and date <=@ce_enddt and OCode IN(select code from VALUESET_TO_CODE where Value_Set_Name in('ESRD Diagnosis','Dialysis Procedure','Nephrectomy','Kidney Transplant','History of Kidney Transplant'))
	Union all
	select MemID from HEDIS_PROC where Measure_id='CBP' and sdate <= @ce_enddt and pstatus='EVN' and  pcode IN(select code from VALUESET_TO_CODE where Value_Set_Name in('ESRD Diagnosis','Dialysis Procedure','Nephrectomy','Kidney Transplant','History of Kidney Transplant'))
	Union all
	Select Memid from HEDIS_VISIT_E where Measure_id='CBP' and sdate <= @ce_enddt and activity IN(select code from VALUESET_TO_CODE where Value_Set_Name in('ESRD Diagnosis','Dialysis Procedure','Nephrectomy','Kidney Transplant','History of Kidney Transplant'))

	Union all
	select MemID from HEDIS_VISIT v
	where MEASURE_ID='CBP' and Date_S <= @ce_enddt and Date_S!='' and hcfapos!=81
	and (
	Diag_I_1 in(select code from VALUESET_TO_CODE where Value_Set_Name in('ESRD Diagnosis','Dialysis Procedure','Nephrectomy','Kidney Transplant','History of Kidney Transplant'))
	or
	Diag_I_2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('ESRD Diagnosis','Dialysis Procedure','Nephrectomy','Kidney Transplant','History of Kidney Transplant'))
	or
	Diag_I_3 in(select code from VALUESET_TO_CODE where Value_Set_Name in('ESRD Diagnosis','Dialysis Procedure','Nephrectomy','Kidney Transplant','History of Kidney Transplant'))
	or
	Diag_I_4 in(select code from VALUESET_TO_CODE where Value_Set_Name in('ESRD Diagnosis','Dialysis Procedure','Nephrectomy','Kidney Transplant','History of Kidney Transplant'))
	or
	Diag_I_5 in(select code from VALUESET_TO_CODE where Value_Set_Name in('ESRD Diagnosis','Dialysis Procedure','Nephrectomy','Kidney Transplant','History of Kidney Transplant'))
	or
	Diag_I_6 in(select code from VALUESET_TO_CODE where Value_Set_Name in('ESRD Diagnosis','Dialysis Procedure','Nephrectomy','Kidney Transplant','History of Kidney Transplant'))
	or
	Diag_I_7 in(select code from VALUESET_TO_CODE where Value_Set_Name in('ESRD Diagnosis','Dialysis Procedure','Nephrectomy','Kidney Transplant','History of Kidney Transplant'))
	or
	Diag_I_8 in(select code from VALUESET_TO_CODE where Value_Set_Name in('ESRD Diagnosis','Dialysis Procedure','Nephrectomy','Kidney Transplant','History of Kidney Transplant'))
	or
	Diag_I_9 in(select code from VALUESET_TO_CODE where Value_Set_Name in('ESRD Diagnosis','Dialysis Procedure','Nephrectomy','Kidney Transplant','History of Kidney Transplant'))
	or
	Diag_I_10 in(select code from VALUESET_TO_CODE where Value_Set_Name in('ESRD Diagnosis','Dialysis Procedure','Nephrectomy','Kidney Transplant','History of Kidney Transplant'))
	or
	CPT in(select code from VALUESET_TO_CODE where Value_Set_Name in('ESRD Diagnosis','Dialysis Procedure','Nephrectomy','Kidney Transplant','History of Kidney Transplant'))
	or
	CPT2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('ESRD Diagnosis','Dialysis Procedure','Nephrectomy','Kidney Transplant','History of Kidney Transplant'))
	or
	HCPCS in(select code from VALUESET_TO_CODE where Value_Set_Name in('ESRD Diagnosis','Dialysis Procedure','Nephrectomy','Kidney Transplant','History of Kidney Transplant'))
	or 
	Proc_I_1 in(select code from VALUESET_TO_CODE where Value_Set_Name in('ESRD Diagnosis','Dialysis Procedure','Nephrectomy','Kidney Transplant','History of Kidney Transplant'))
	or 
	Proc_I_2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('ESRD Diagnosis','Dialysis Procedure','Nephrectomy','Kidney Transplant','History of Kidney Transplant'))
	or 
	Proc_I_3 in(select code from VALUESET_TO_CODE where Value_Set_Name in('ESRD Diagnosis','Dialysis Procedure','Nephrectomy','Kidney Transplant','History of Kidney Transplant'))
	or 
	Proc_I_4 in(select code from VALUESET_TO_CODE where Value_Set_Name in('ESRD Diagnosis','Dialysis Procedure','Nephrectomy','Kidney Transplant','History of Kidney Transplant'))
	or 
	Proc_I_5 in(select code from VALUESET_TO_CODE where Value_Set_Name in('ESRD Diagnosis','Dialysis Procedure','Nephrectomy','Kidney Transplant','History of Kidney Transplant'))
	or
	Rev in(select code from VALUESET_TO_CODE where Value_Set_Name in('ESRD Diagnosis','Dialysis Procedure','Nephrectomy','Kidney Transplant','History of Kidney Transplant'))
	)

	   	  
	Union All

	-- Optional Exclusion 3 - Non Acute Inpatient Stay
	select MemID from HEDIS_VISIT v
		where MEASURE_ID='CBP' and Date_Adm between @ce_startdt and @ce_enddt and Date_Adm!='' and hcfapos!=81
		and rev in(select code from VALUESET_TO_CODE where code_system='UBREV' and value_set_name in('Inpatient Stay'))
		and (rev in(select code from VALUESET_TO_CODE where code_system='UBREV' and value_set_name in('Nonacute Inpatient Stay'))
		or RIGHT('0000'+CAST(Trim(BillType) AS VARCHAR(4)),4) in (select code from VALUESET_TO_CODE where code_system='UBTOB' and value_set_name in('Nonacute Inpatient Stay')))
	)t1
	
	
	update #cbpdataset set excl=1 from #cbpdataset ds
	join #cbp_optexcl oe on oe.Memid=ds.patient_id ;
	
	-- Option Exclusion - Pregnancy
	drop table if exists #cbp_optexcl1;
	CREATE table #cbp_optexcl1
	(
		Memid varchar(50)
				
	);

		CREATE CLUSTERED INDEX idx_cbp_optexcl1 ON #cbp_optexcl1 ([memid]);

	insert into #cbp_optexcl1
	
-- Optional Exclusion - Pregnancy

	select distinct Memid from(

	select MemId from HEDIS_DIAG where Measure_id='CBP' and sdate between @ce_startdt and @ce_enddt and sdate!='' and  dcode IN(select code from VALUESET_TO_CODE where Value_Set_Name in('Pregnancy')) 

	Union all
	select MemID from HEDIS_VISIT v
	where MEASURE_ID='CBP' and Date_S between @ce_startdt and @ce_enddt and Date_S!='' and HCFAPOS!=81
	and (
	Diag_I_1 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Pregnancy'))
	or
	Diag_I_2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Pregnancy'))
	or
	Diag_I_3 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Pregnancy'))
	or
	Diag_I_4 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Pregnancy'))
	or
	Diag_I_5 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Pregnancy'))
	or
	Diag_I_6 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Pregnancy'))
	or
	Diag_I_7 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Pregnancy'))
	or
	Diag_I_8 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Pregnancy'))
	or
	Diag_I_9 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Pregnancy'))
	or
	Diag_I_10 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Pregnancy'))
	or
	CPT in(select code from VALUESET_TO_CODE where Value_Set_Name in('Pregnancy'))
	or
	CPT2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Pregnancy'))
	or
	HCPCS in(select code from VALUESET_TO_CODE where Value_Set_Name in('Pregnancy'))
	or 
	Proc_I_1 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Pregnancy'))
	or 
	Proc_I_2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Pregnancy'))
	or 
	Proc_I_3 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Pregnancy'))
	or 
	Proc_I_4 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Pregnancy'))
	or 
	Proc_I_5 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Pregnancy'))
	or
	Rev in(select code from VALUESET_TO_CODE where Value_Set_Name in('Pregnancy'))
	)

	)t1


	update #cbpdataset set excl=1 from #cbpdataset ds
	join #cbp_optexcl1 oe1 on oe1.Memid=ds.patient_id and ds.patient_gender='F';
	
	
	-- Event

	-- Create list of patients with Hypertension
	
	drop table if exists #cbp_hypertension;
	CREATE table #cbp_hypertension
	(
		Memid varchar(50),
		servicedate varchar(8),
		claimid int
	);

	CREATE CLUSTERED INDEX ix_cbp_cbp_hypertension ON #cbp_hypertension ([memid],[servicedate],[claimid]);

				
	insert into #cbp_hypertension
	select distinct MemID,Date_S,claimid from HEDIS_VISIT v
	where MEASURE_ID='CBP'  and Date_S between @ce_startdt1 and @ce_middt and HCFAPOS!=81 and suppdata='N'
	and (
	Diag_I_1 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Essential Hypertension'))
	or
	Diag_I_2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Essential Hypertension'))
	or
	Diag_I_3 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Essential Hypertension'))
	or
	Diag_I_4 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Essential Hypertension'))
	or
	Diag_I_5 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Essential Hypertension'))
	or
	Diag_I_6 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Essential Hypertension'))
	or
	Diag_I_7 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Essential Hypertension'))
	or
	Diag_I_8 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Essential Hypertension'))
	or
	Diag_I_9 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Essential Hypertension'))
	or
	Diag_I_10 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Essential Hypertension'))
	or
	CPT in(select code from VALUESET_TO_CODE where Value_Set_Name in('Essential Hypertension'))
	or
	CPT2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Essential Hypertension'))
	or
	HCPCS in(select code from VALUESET_TO_CODE where Value_Set_Name in('Essential Hypertension'))
	or 
	Proc_I_1 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Essential Hypertension'))
	or 
	Proc_I_2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Essential Hypertension'))
	or 
	Proc_I_3 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Essential Hypertension'))
	or 
	Proc_I_4 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Essential Hypertension'))
	or 
	Proc_I_5 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Essential Hypertension'))
	)


	-- Identify Patients with Outpatient,Telephone,Online visit
	drop table if exists #cbp_hypertensionvstlist;				
	CREATE table #cbp_hypertensionvstlist
	(
		Memid varchar(50),
		servicedate varchar(8),
		claimid int
	);

	CREATE CLUSTERED INDEX ix_cbp_hypertensionvstlist ON #cbp_hypertensionvstlist ([memid],[servicedate],[claimid]);
	
	insert into #cbp_hypertensionvstlist
	select distinct MemID,Date_S,claimid from HEDIS_VISIT v
	where MEASURE_ID='CBP'  and Date_S between @ce_startdt1 and @ce_middt and HCFAPOS!=81 and suppdata='N'
	and (
	Diag_I_1 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient Without UBREV','Telephone Visits','Online Assessments'))
	or
	Diag_I_2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient Without UBREV','Telephone Visits','Online Assessments'))
	or
	Diag_I_3 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient Without UBREV','Telephone Visits','Online Assessments'))
	or
	Diag_I_4 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient Without UBREV','Telephone Visits','Online Assessments'))
	or
	Diag_I_5 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient Without UBREV','Telephone Visits','Online Assessments'))
	or
	Diag_I_6 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient Without UBREV','Telephone Visits','Online Assessments'))
	or
	Diag_I_7 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient Without UBREV','Telephone Visits','Online Assessments'))
	or
	Diag_I_8 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient Without UBREV','Telephone Visits','Online Assessments'))
	or
	Diag_I_9 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient Without UBREV','Telephone Visits','Online Assessments'))
	or
	Diag_I_10 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient Without UBREV','Telephone Visits','Online Assessments'))
	or
	CPT in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient Without UBREV','Telephone Visits','Online Assessments'))
	or
	CPT2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient Without UBREV','Telephone Visits','Online Assessments'))
	or
	HCPCS in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient Without UBREV','Telephone Visits','Online Assessments'))
	or 
	Proc_I_1 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient Without UBREV','Telephone Visits','Online Assessments'))
	or 
	Proc_I_2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient Without UBREV','Telephone Visits','Online Assessments'))
	or 
	Proc_I_3 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient Without UBREV','Telephone Visits','Online Assessments'))
	or 
	Proc_I_4 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient Without UBREV','Telephone Visits','Online Assessments'))
	or 
	Proc_I_5 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient Without UBREV','Telephone Visits','Online Assessments'))
	)

	-- Event LIST
	
	drop table if exists #cbp_eventlist;
				
	CREATE table #cbp_eventlist
		(
			Memid varchar(50)
		);

	CREATE CLUSTERED INDEX ix_cbp_eventlist ON #cbp_eventlist ([memid]);
	
	insert into #cbp_eventlist
	select t1.Memid from(
	select distinct h.Memid,h.servicedate from #cbp_hypertension h
	join #cbp_hypertensionvstlist v on h.Memid=v.Memid and h.claimid=v.claimid 
	)t1  group by t1.Memid having count(t1.Memid)>1
	
	
	update #cbpdataset set event=1 from #cbpdataset ds
	join #cbp_eventlist e on e.Memid=ds.patient_id ;
	
	
	-- Numerator 
	-- Identify Vst visit (Outpatient Without UBREV Value Set), telephone visit (Telephone Visits Value Set), e-visit or virtual check-in (Online Assessments Value Set), a nonacute inpatient encounter (Nonacute Inpatient Value Set), or remote monitoring event (Remote Blood Pressure Monitoring Value Set) during the measurement year. 
	drop table if exists #numvstlist;				
	CREATE table #numvstlist
	(
		Memid varchar(50),
		
		servicedate varchar(8)
		
				
	);

	CREATE CLUSTERED INDEX idx_cbp_numvstlist ON #numvstlist ([memid],[servicedate]);
	
	Insert into #numvstlist
	select distinct Memid,servicedate from(
	select MemId,sdate as servicedate from HEDIS_DIAG where Measure_id='CBP' and sdate between @ce_startdt and @ce_enddt and  dcode IN(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient Without UBREV','Telephone Visits','Online Assessments','Nonacute Inpatient','Remote Blood Pressure Monitoring')) 
	union all
	select MemID ,date_s as servicedate from HEDIS_LAB where Measure_id='CBP' and date_s between @ce_startdt and @ce_enddt and (LOINC in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient Without UBREV','Telephone Visits','Online Assessments','Nonacute Inpatient','Remote Blood Pressure Monitoring')) or CPT_Code IN(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient Without UBREV','Telephone Visits','Online Assessments','Nonacute Inpatient','Remote Blood Pressure Monitoring')))
	Union all
	select Memid,date as servicedate from HEDIS_OBS where Measure_id='CBP' and date between @ce_startdt and @ce_enddt and OCode IN(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient Without UBREV','Telephone Visits','Online Assessments','Nonacute Inpatient','Remote Blood Pressure Monitoring'))
	Union all

	select MemID,sdate as servicedate from HEDIS_PROC where Measure_id='CBP' and pstatus='EVN' and sdate between @ce_startdt and @ce_enddt and  pcode IN(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient Without UBREV','Telephone Visits','Online Assessments','Nonacute Inpatient','Remote Blood Pressure Monitoring'))
	union all
	Select Memid,sdate as servicedate from HEDIS_VISIT_E where Measure_id='CBP' and sdate between @ce_startdt and @ce_enddt and activity IN(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient Without UBREV','Telephone Visits','Online Assessments','Nonacute Inpatient','Remote Blood Pressure Monitoring'))
	union all
	select MemID,Date_S as servicedate from HEDIS_VISIT v
	where MEASURE_ID='CBP' and Date_S between @ce_startdt and @ce_enddt
	and (
	Diag_I_1 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient Without UBREV','Telephone Visits','Online Assessments','Nonacute Inpatient','Remote Blood Pressure Monitoring'))
	or
	Diag_I_2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient Without UBREV','Telephone Visits','Online Assessments','Nonacute Inpatient','Remote Blood Pressure Monitoring'))
	or
	Diag_I_3 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient Without UBREV','Telephone Visits','Online Assessments','Nonacute Inpatient','Remote Blood Pressure Monitoring'))
	or
	Diag_I_4 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient Without UBREV','Telephone Visits','Online Assessments','Nonacute Inpatient','Remote Blood Pressure Monitoring'))
	or
	Diag_I_5 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient Without UBREV','Telephone Visits','Online Assessments','Nonacute Inpatient','Remote Blood Pressure Monitoring'))
	or
	Diag_I_6 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient Without UBREV','Telephone Visits','Online Assessments','Nonacute Inpatient','Remote Blood Pressure Monitoring'))
	or
	Diag_I_7 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient Without UBREV','Telephone Visits','Online Assessments','Nonacute Inpatient','Remote Blood Pressure Monitoring'))
	or
	Diag_I_8 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient Without UBREV','Telephone Visits','Online Assessments','Nonacute Inpatient','Remote Blood Pressure Monitoring'))
	or
	Diag_I_9 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient Without UBREV','Telephone Visits','Online Assessments','Nonacute Inpatient','Remote Blood Pressure Monitoring'))
	or
	Diag_I_10 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient Without UBREV','Telephone Visits','Online Assessments','Nonacute Inpatient','Remote Blood Pressure Monitoring'))
	or
	CPT in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient Without UBREV','Telephone Visits','Online Assessments','Nonacute Inpatient','Remote Blood Pressure Monitoring'))
	or
	CPT2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient Without UBREV','Telephone Visits','Online Assessments','Nonacute Inpatient','Remote Blood Pressure Monitoring'))
	or
	HCPCS in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient Without UBREV','Telephone Visits','Online Assessments','Nonacute Inpatient','Remote Blood Pressure Monitoring'))
	or 
	Proc_I_1 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient Without UBREV','Telephone Visits','Online Assessments','Nonacute Inpatient','Remote Blood Pressure Monitoring'))
	or 
	Proc_I_2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient Without UBREV','Telephone Visits','Online Assessments','Nonacute Inpatient','Remote Blood Pressure Monitoring'))
	or 
	Proc_I_3 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient Without UBREV','Telephone Visits','Online Assessments','Nonacute Inpatient','Remote Blood Pressure Monitoring'))
	or 
	Proc_I_4 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient Without UBREV','Telephone Visits','Online Assessments','Nonacute Inpatient','Remote Blood Pressure Monitoring'))
	or 
	Proc_I_5 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient Without UBREV','Telephone Visits','Online Assessments','Nonacute Inpatient','Remote Blood Pressure Monitoring'))
	or
	Rev in(select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient Without UBREV','Telephone Visits','Online Assessments','Nonacute Inpatient','Remote Blood Pressure Monitoring'))
	)
	)t1
		
					
	-- Numerator compliant members
	drop table if exists #numcompliantmemlist;				
	CREATE table #numcompliantmemlist
	(
		Memid varchar(50),
		servicedate varchar(8)
				
	);

	CREATE CLUSTERED INDEX idx_cbp_numcompliantmemlist ON #numcompliantmemlist ([memid],[servicedate]);
	
	insert into #numcompliantmemlist
	-- Members with Systolic <140 and diastolic<90 based on results
	select distinct memid,servicedate from(
	select distinct s.Memid,s.servicedate from(
	select Memid,date as servicedate from HEDIS_OBS where Measure_id='CBP' and date between @ce_startdt and @ce_enddt and OCode IN(select code from VALUESET_TO_CODE where Value_Set_Name in('Systolic Blood Pressure')) and value<140
	)s
	join
	(
	select Memid,date as servicedate from HEDIS_OBS where Measure_id='CBP' and date between @ce_startdt and @ce_enddt and OCode IN(select code from VALUESET_TO_CODE where Value_Set_Name in('Diastolic Blood Pressure')) and value<90
	)d on d.MemID=s.MemID and d.servicedate=s.servicedate

	Union all

-- Members with Systolic <140 and diastolic<90 based on CPT || category codes
	select distinct s.MemId,s.servicedate from(
	select MemID,Date_S as servicedate,claimid from HEDIS_VISIT v
	where MEASURE_ID='CBP' and Date_S between @ce_startdt and @ce_enddt
	and (
	CPT in(select code from VALUESET_TO_CODE where Value_Set_Name in('Systolic Less Than 140')) 
	or
	CPT2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Systolic Less Than 140'))
	
	)
	and
	CPTMod_1 not in(select code from VALUESET_TO_CODE where Value_Set_Name in('CPT CAT II Modifier'))
	and
	CPTMod_2 not in(select code from VALUESET_TO_CODE where Value_Set_Name in('CPT CAT II Modifier'))
	and
	CPT2Mod not in(select code from VALUESET_TO_CODE where Value_Set_Name in('CPT CAT II Modifier'))
	
	)s
	Join
	(
	select distinct MemID,Date_S as servicedate,claimid from HEDIS_VISIT v
	where MEASURE_ID='CBP' and Date_S between @ce_startdt and @ce_enddt
	and (
	CPT in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diastolic 80-89','Diastolic Less Than 80')) 
	or
	CPT2 in(select code from VALUESET_TO_CODE where Value_Set_Name in('Diastolic 80-89','Diastolic Less Than 80'))
	
	)
	and
	CPTMod_1 not in(select code from VALUESET_TO_CODE where Value_Set_Name in('CPT CAT II Modifier'))
	and
	CPTMod_2 not in(select code from VALUESET_TO_CODE where Value_Set_Name in('CPT CAT II Modifier'))
	and
	CPT2Mod not in(select code from VALUESET_TO_CODE where Value_Set_Name in('CPT CAT II Modifier'))
	)d on s.MemID=d.MemID and s.servicedate=d.servicedate

	)numcompmemlist
	
				
	
    -- Identify bp readings taken during the specified visit types        
	drop table if exists #bpvisit;						
	CREATE table #bpvisit
	(
		Memid varchar(50),
		servicedate varchar(8)
				
	);

	CREATE CLUSTERED INDEX idx_cbp_bpvisit ON #bpvisit ([memid],[servicedate]);
	
	Insert into #bpvisit
	select nc.Memid,nc.servicedate from #numcompliantmemlist nc
	join #numvstlist nv on nv.memid=nc.memid and nv.servicedate=nc.servicedate
	
	
	-- Get 2nd visit date diagnosis of hypertension

	drop table if exists  #cbp_numbasedate;
	CREATE table #cbp_numbasedate
	(
		Memid varchar(50),
		servicedate varchar(8)
				
	);
	-- Identify most recent bp reading and it should be taken after the 2nd hypertension diagnosis
	CREATE CLUSTERED INDEX idx_cbp_numbasedate ON #cbp_numbasedate ([memid],[servicedate]);

	insert into #cbp_numbasedate
	select memid,servicedate from(
	select memid,servicedate,ROW_NUMBER() over (partition by memid order by memid) as row_num from(
	select distinct h1.memid,h1.servicedate from #cbp_hypertension h1 join #cbp_hypertensionvstlist hv1 on h1.Memid=hv1.Memid and h1.claimid=hv1.claimid  
	)temp 
	)temp1 where row_num>1

	
	
	 
	drop table if exists  #numlist;
	CREATE table #numlist
	(
		Memid varchar(50),
		servicedate varchar(8)
				
	);
	-- Identify most recent bp reading and it should be taken after the 2nd hypertension diagnosis
	CREATE CLUSTERED INDEX idx_cbp_numlist ON #numlist ([memid],[servicedate]);
	
	 Insert into #numlist
	 select distinct t1.memid,t1.servicedate from (
	 select bp1.memid,bp1.servicedate from #bpvisit bp1 where servicedate=(select max(bp2.servicedate) from #bpvisit bp2 where bp1.memid=bp2.Memid) 
	 )t1 
	 join #cbp_numbasedate dt on t1.Memid=dt.memid and t1.servicedate>=dt.servicedate
	
	
	
	-- Updating NUM column
	update #cbpdataset set num=1 from #cbpdataset ds
	join #numlist n on n.Memid=ds.patient_id ;


	select @runid=max(RUN_ID) from HEDIS_MEASURE_OUTPUT where measure_id='CBP';

	if(@runid>=1)
	Begin
		SET @runid=@runid+1;
	End
	Else
	Begin
		SET @runid=1;
	END


	Insert into HEDIS_MEASURE_OUTPUT(memid,Meas,payer,CE,Event,Epop,Excl,Num,Rexcl,RExclD,Age,Gender,Measure_ID,Measurement_year,RUN_ID)
	SELECT patient_id AS memid,meas,payer,CE,EVENT,CASE WHEN CE=1 AND EVENT=1 AND rexcl=0 and rexcld=0 THEN 1 ELSE 0 END AS epop,excl,num,rexcl,rexcld,cast(age as Int) AS age,patient_gender AS gender,'CBP' as Measure_ID,@meas_year,@runid FROM #cbpdataset
	

GO
