SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON



CREATE proc [HDS].[KPI_HEDIS_CBP] 
AS


BEGIN
DECLARE @INFO VARCHAR(8000)
					,	@TRUE BIT = 1
					,	@FALSE BIT = 0
					,	@DB_ID INT = DB_ID()
					,	@LOGID INT = 0
					,	@PROC VARCHAR(500)=OBJECT_SCHEMA_NAME(@@PROCID)+'.'+OBJECT_NAME(@@PROCID)
					,	@PERF_START DATETIME
					,	@PERF_DURATION INT
					,	@PERF_ROW INT
					,	@RC	INT
					;

		EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, @INFO=@INFO, @LOG2ID=@LOGID OUT;
		DECLARE @PROCFULLNAME VARCHAR(500) = DB_NAME()+'.'+@PROC;
		----EXEC dbo.SP_MI_UTIL_SEND_MAIL @PROCFULLNAME, 'STARTED', 1, @ECHO=0; 
	DECLARE @MSG VARCHAR(1000)
	DECLARE @PERF_START_PROC DATETIME 
	DECLARE @PERF_RPS NUMERIC(10,2) 
	DECLARE @ROWS INT 
	DECLARE @PERF_ROWS INT 
	SET @PERF_ROWS = 0 
	SET @PERF_START = GETDATE() 
	SET @PERF_START_PROC = GETDATE() 

BEGIN TRY

-- Declare Variables
declare @rundate Date=GetDate()
declare @meas_year varchar(4)=Year(Dateadd(month,-2,@rundate))
declare @rootId varchar(10)='159'


Declare @meas varchar(10)='CBP';
Declare @ce_startdt date;
Declare @ce_startdt1 Date;
Declare @ce_middt Date;
Declare @ce_enddt Date;
Declare @runid INT;

Declare @startDate Date;
Declare @enddate date;
Declare @quarter varchar(20);
Declare @measure_id varchar(10);
Declare @target INT;
Declare @domain varchar(100);
Declare @subdomain varchar(100);
Declare @measuretype varchar(100);
Declare @measurename varchar(100);
Declare @reporttype varchar(100);
Declare @reportId INT;




-- Set Measure dates
SET @ce_startdt=concat(@meas_year,'-01-01');
SET @ce_middt=concat(@meas_year,'-06-30');
SET @ce_enddt=concat(@meas_year,'-12-31');
SET @ce_startdt1=concat(@meas_year-1,'-01-01');


Set @reporttype='Physician'
Set @measurename='Hypertension: BP Control <140/90 (18-85); GAP Open/Closed by last Documented B/P for CY'
--Set @startDate=DATEADD(yy, DATEDIFF(yy, 0,Dateadd(month,-2,GETDATE())), 0) 
--Set @enddate=eomonth(Dateadd(month,-2,GetDate()))
--Set @quarter=Concat(Year(@enddate),' - Q',DATEPART(q, @enddate))
set @target=82
Set @domain='Clinical Quality / Chronic Condition Management'
Set @subdomain='Hypertension'
Set @measuretype='UHN'
Set @measure_id='12'


-- Create Eligible Patient LIST

drop table if exists #cbp_memlist; 
CREATE TABLE #cbp_memlist (
    EMPI varchar(100)
    
)
insert into #cbp_memlist
SELECT DISTINCT 
	en.EMPI 
FROM open_empi_master gm
join Enrollment en on 
	en.EMPI=gm.EMPI_ID and 
	en.ROOT_COMPANIES_ID=gm.ROOT_COMPANIES_ID
WHERE 
	en.ROOT_COMPANIES_ID=@rootId and
	en.EFF_DATE<=@ce_enddt AND 
	TERM_DATE>=@ce_startdt AND 
	YEAR(@ce_enddt)-YEAR(Date_of_Birth) BETWEEN 18 AND 85 ORDER BY 1;

-- Create Temp Table for enrollment

drop table if exists #cbp_tmpsubscriber;
CREATE TABLE #cbp_tmpsubscriber (
    EMPI varchar(100),
    dob Date,
	gender varchar(1),
	Age INT,
	payer varchar(50),
	StartDate Date,
	EndDate Date
);
insert into #cbp_tmpsubscriber
SELECT distinct
	en.EMPI
	,gm.Date_of_Birth
	,gm.Gender
	,YEAR(@ce_enddt)-YEAR(Date_of_Birth) as age
	,en.PAYER_TYPE
	,en.EFF_DATE
	,en.TERM_DATE  
FROM Open_EMPI_MASTER gm
join ENROLLMENT en on en.EMPI=gm.EMPI_ID and 
					  en.ROOT_COMPANIES_ID=gm.ROOT_COMPANIES_ID
WHERE 
	en.ROOT_COMPANIES_ID=@rootId and
	en.EFF_DATE<=@ce_enddt AND 
	TERM_DATE>=@ce_startdt AND 
	YEAR(@ce_enddt)-YEAR(Date_of_Birth) BETWEEN 18 AND 85 
	ORDER BY 
		en.EMPI
		,en.EFF_DATE
		,en.TERM_DATE;




-- Clear #cbpdataset

drop table if exists #cbpdataset;

CREATE TABLE #cbpdataset (
  EMPI varchar(100) NOT NULL,
  [meas] varchar(20) DEFAULT NULL,
  [payer] varchar(100) DEFAULT NULL,
  [gender] varchar(45) NOT NULL,
  [age] INT,
  [orec] smallint DEFAULT NULL,
  [lis] smallint DEFAULT '0',
  [rexcl] smallint DEFAULT '0',
  [rexcld] smallint DEFAULT '0',
  [event] smallint DEFAULT '0',
  [CE] smallint DEFAULT '0',
  [excl] smallint DEFAULT '0',
  [num] smallint DEFAULT '0'
) ;
Insert into #cbpdataset(EMPI,meas,Payer,Age,gender)
Select distinct
	EMPI
	,'CBP'
	,pm.PayerMapping
	,age
	,gender
From
(
	Select distinct
		EMPI
		,gender
		,age
		,case 
				when (Payer in('HMO','CEP','POS','PPO','EPO') and nxtpayer in('MD','MLI','MRB'))  then TRIM(Payer)
				When (Payer in('HMO','CEP','POS','PPO','EPO') and prvpayer in('MD','MLI','MRB'))  then TRIM(Payer)
				When (Payer in('MD','MLI','MRB') and nxtpayer in('HMO','CEP','POS','PPO','EPO'))  then TRIM(nxtpayer)
				When (Payer in('MD','MLI','MRB') and prvpayer in('HMO','CEP','POS','PPO','EPO'))  then TRIM(prvpayer)
				when (Payer in('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP','MR') and nxtpayer in('HMO','CEP','POS','PPO','EPO'))  then TRIM(Payer)
				When (Payer in('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP','MR') and prvpayer in('HMO','CEP','POS','PPO','EPO'))  then TRIM(Payer)
				When (Payer in('HMO','CEP','POS','PPO','EPO') and nxtpayer in('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP','MR'))  then TRIM(nxtpayer)
				When (Payer in('HMO','CEP','POS','PPO','EPO') and prvpayer in('MCR','MCS','MP','MC','SN1','SN2','SN3','MMP','MR'))  then TRIM(prvpayer)
				else Payer 
			end as newpayer
	From
	(
		select
			*
			,isnull(lead(Payer) over(partition by EMPI order by Payer),Payer) as nxtpayer
			,isnull(lag(Payer) over(partition by EMPI order by Payer),Payer) as prvpayer
		From 
		(
			select
				EMPI
				,payer
				,gender
				,age
			From
			(
				select 
					EMPI
					,StartDate
					,EndDate
					,TRIM(payer) as payer
					,gender
					,age
					,RANK() over(partition by EMPI order by StartDate desc,EndDate Desc) as rn 
				from #cbp_tmpsubscriber 
				where  
					StartDate<=@ce_enddt
				--	and EMPI=95066
			)t1
			where 
				rn=1
		)t2
		
	)t3
)t4
Join HDS.HEDIS_PAYER_MAPPING pm on 
	t4.newpayer=pm.payer and 
	pm.Measure_id='CBP'
Order by 1



-- Continuous Enrollment
				
	DROP TABLE IF EXISTS #cbp_contenroll;
	CREATE table #cbp_contenroll
	(
		EMPI varchar(100),
		
	);
	Insert into #cbp_contenroll
	select 
		EMPI
	from GetContinuousEnrolledEMPI(@rootId,@ce_startdt,@ce_enddt,45,0)

	
	
	
	update #cbpdataset set CE=1 from #cbpdataset ds join #cbp_contenroll ce on ds.EMPI=ce.EMPI;


	-- Required Exclusion

	drop table if exists #cbp_reqdexcl
	CREATE table #cbp_reqdexcl
	(
		EMPI varchar(100)
			
	)
	insert into #cbp_reqdexcl
	Select
		EMPI
	From palliativecare(@rootId,@ce_startdt,@ce_enddt)
	
	update #cbpdataset set rexcld=1 from #cbpdataset ds join #cbp_reqdexcl re on ds.EMPI=re.EMPI;

	
	
		
	-- AdvancedIllness

	drop table if exists #cbp_advillness;
	CREATE table #cbp_advillness
	(
		EMPI varchar(100),
		servicedate Date,
		CLAIM_ID varchar(100)
	);
	Insert into #cbp_advillness
	select
		EMPI
		,DIAG_START_DATE
		,CLAIM_ID 
	from advancedillness(@rootId,@ce_startdt1,@ce_enddt)
	Where
		DIAG_DATA_SRC not in(select DATA_SOURCE from DATA_SOURCE where supplemental=1)


	
	-- Members with Institutinal SNP

	UPDATE #cbpdataset 
	SET #cbpdataset.rexcl=1 
	FROM #cbpdataset ds 
	JOIN ENROLLMENT s on 
		ds.EMPI=s.EMPI and 
		s.ROOT_COMPANIES_ID=@rootId 
	WHERE  
		ds.age>=66 AND 
		ds.payer IN('MCR','MCS','MP','MC','SN2','SN1','MR') AND 
		s.EFF_DATE<=@ce_enddt AND s.TERM_DATE>=@ce_startdt AND 
		s.PAYER_TYPE='SN2';


	-- LTI Exclusion
	
	drop table if exists #cbp_LTImembers;
	CREATE table #cbp_LTImembers
	(
		EMPI varchar(100)
				
	)
	Insert into #cbp_LTImembers
	Select
		EMPI
	From LTImembers(@rootId,@ce_startdt,@ce_enddt)

	update #cbpdataset set rexcl=1 from #cbpdataset ds join #cbp_LTImembers re on ds.EMPI=re.EMPI where ds.age>=66 AND ds.payer IN('MCR','MCS','MP','MC','MMP','SN1','MC','MR');

	

	-- Hospice Exclusion

	drop table if exists #cbp_hospicemembers;
	CREATE table #cbp_hospicemembers
	(
		EMPI varchar(100)
		
	)
	Insert into #cbp_hospicemembers
	Select
		EMPI
	From hospicemembers(@rootId,@ce_startdt,@ce_enddt)
	
	update #cbpdataset set rexcl=1 from #cbpdataset ds join #cbp_hospicemembers hos on hos.EMPI=ds.EMPI;
			
				
	-- Frailty Members LIST
	drop table if exists #cbp_frailtymembers;	
	CREATE table #cbp_frailtymembers
	(
		
		EMPI varchar(100)
			
	)
	Insert into #cbp_frailtymembers
	Select distinct
		EMPI
	From Frailty(@rootId,@ce_startdt,@ce_enddt)
	Where
		DataSource not in(select DATA_SOURCE from DATA_SOURCE where supplemental=1)
	

-- Required Exclusion 1

	-- Inpatient Stay List
	drop table if exists #cbp_inpatientstaylist;
	CREATE table #cbp_inpatientstaylist
	(
		EMPI varchar(100),
		FROM_DATE Date,
		CLAIM_ID varchar(100)
	)
	Insert into #cbp_inpatientstaylist
	select distinct 
		EMPI
		,FROM_DATE
		,CLAIM_ID 
	from Inpatientstays(@rootId,@ce_startdt1,@ce_enddt)
	Where
		CL_DATA_SRC not in(select DATA_SOURCE from DATA_SOURCE where supplemental=1)

	

	-- Non acute Inpatient stay list
	
	drop table if exists #cbp_noncauteinpatientstaylist;
	CREATE table #cbp_noncauteinpatientstaylist
	(
		EMPI varchar(100),
		FROM_DATE Date,
		CLAIM_ID varchar(100)
	)
	Insert into #cbp_noncauteinpatientstaylist
	select distinct 
		EMPI
		,FROM_DATE
		,CLAIM_ID 
	from nonacutestays(@rootId,@ce_startdt1,@ce_enddt)
	Where
		CL_DATA_SRC not in(select DATA_SOURCE from DATA_SOURCE where supplemental=1)

	
	-- Outpatient and other visits
	drop table if exists #cbp_visitlist;
	CREATE table #cbp_visitlist
	(
		EMPI varchar(100),
		FROM_DATE Date,
		CLAIM_ID varchar(100)
	)
	Insert into #cbp_visitlist
	Select distinct
		*
	From
	(
		
		Select
			EMPI
			,PROC_START_DATE
			,CLAIM_ID
		From GetProcedures(@rootId,@ce_startdt1,@ce_enddt,'Outpatient')

		Union all

		Select
			EMPI
			,PROC_START_DATE
			,CLAIM_ID
		From GetProcedures(@rootId,@ce_startdt1,@ce_enddt,'Observation')

		Union all

		Select
			EMPI
			,PROC_START_DATE
			,CLAIM_ID
		From GetProcedures(@rootId,@ce_startdt1,@ce_enddt,'ED')

		Union all

		Select
			EMPI
			,PROC_START_DATE
			,CLAIM_ID
		From GetProcedures(@rootId,@ce_startdt1,@ce_enddt,'Telephone Visits')

		Union all

		Select
			EMPI
			,PROC_START_DATE
			,CLAIM_ID
		From GetProcedures(@rootId,@ce_startdt1,@ce_enddt,'Online Assessments')

		Union all

		Select
			EMPI
			,PROC_START_DATE
			,CLAIM_ID
		From GetProcedures(@rootId,@ce_startdt1,@ce_enddt,'Nonacute Inpatient')

		Union all

		select 
			EMPI
			,FROM_DATE
			,CLAIM_ID 
		from CLAIMLINE 
		where 
			ROOT_COMPANIES_ID=@rootId and 
			ISNULL(POS,'')!='81' and 
			CL_DATA_SRC not in(select DATA_SOURCE from DATA_SOURCE where supplemental=1) and
			FROM_DATE between @ce_startdt1 and @ce_enddt and
			REV_CODE in
			(
				select code from HDS.VALUESET_TO_CODE where code_system='UBREV' and  Value_Set_Name in('Outpatient','ED')
			)
	)t1
	
	
	-- Required exclusion table
	drop table if exists #cbp_reqdexcl1;
	CREATE table #cbp_reqdexcl1
	(
		EMPI varchar(100)
			
	)
	Insert into #cbp_reqdexcl1
	select distinct 
		t3.EMPI 
	from
	(
		select 
			t2.EMPI 
		from
		(
			select distinct 
				t1.EMPI
				,t1.FROM_DATE 
			from
			(
				select 
					EMPI
					,FROM_DATE
					,CLAIM_ID  
				from #cbp_visitlist 
				
				union all
				
				select 
					na.EMPI
					,na.FROM_DATE
					,na.CLAIM_ID 
				from #cbp_noncauteinpatientstaylist na
				join #cbp_inpatientstaylist inp on 
					na.EMPI=inp.EMPI and 
					na.CLAIM_ID=inp.CLAIM_ID
	
			)t1
			Join #cbp_advillness a on
				a.EMPI=t1.EMPI and
				a.CLAIM_ID=t1.CLAIM_ID
		)t2 
		group by 
			t2.EMPI 
		having 
			count(t2.EMPI)>1
	)t3 
	Join #cbp_frailtymembers f on 
		f.EMPI=t3.EMPI

	update #cbpdataset set rexcl=1 from #cbpdataset ds
	join #cbp_reqdexcl1 re1 on re1.EMPI=ds.EMPI and ds.age BETWEEN 66 AND 80;

-- Required Exclusion 2

	-- Acute Inpatient with Advanced Illness
	drop table if exists #cbp_reqdexcl2;
	CREATE table #cbp_reqdexcl2
	(
		EMPI varchar(100)
				
	)
	insert into #cbp_reqdexcl2
	select distinct 
		t2.EMPI 
	from
	(
		select 
			t1.EMPI 
		from 
		(
			Select distinct
				EMPI
				,PROC_START_DATE
				,CLAIM_ID
			From GetProcedures(@rootId,@ce_startdt1,@ce_enddt,'Acute Inpatient')
		)t1
		Join #cbp_advillness a on 
			a.EMPI=t1.EMPI and 
			a.CLAIM_ID=t1.CLAIM_ID
	)t2
	join #cbp_frailtymembers f on 
		f.EMPI=t2.EMPI

	update #cbpdataset set rexcl=1 from #cbpdataset ds
	join #cbp_reqdexcl2 re2 on re2.EMPI=ds.EMPI and ds.age BETWEEN 66 AND 80;
	
			
	-- Required exclusion 3
	drop table if exists #cbp_reqdexcl3;
	CREATE table #cbp_reqdexcl3
	(
		EMPI varchar(100)
			
	)
	insert into #cbp_reqdexcl3
	select distinct 
		t2.EMPI 
	from
	(
		select 
			t1.EMPI
			,t1.FROM_DATE
			,t1.CLAIM_ID 
		from
		(
			select 
				inp.EMPI
				,inp.FROM_DATE
				,inp.CLAIM_ID 
			from #cbp_inpatientstaylist inp
			left outer join #cbp_noncauteinpatientstaylist na on 
				inp.EMPI=na.EMPI and 
				inp.CLAIM_ID=na.CLAIM_ID
			where 
				na.EMPI is null
		)t1
		join #cbp_advillness a on 
			a.EMPI=t1.EMPI and 
			a.CLAIM_ID=t1.CLAIM_ID
	)t2
	join #cbp_frailtymembers f on 
		f.EMPI=t2.EMPI

	update #cbpdataset set rexcl=1 from #cbpdataset ds
	join #cbp_reqdexcl3 re3 on re3.EMPI=ds.EMPI and ds.age BETWEEN 66 AND 80;

		
-- RequiredExcl 4
	drop table if exists #cbp_reqdexcl4;
	CREATE table #cbp_reqdexcl4
	(
		EMPI varchar(100)
				
	)
	insert into #cbp_reqdexcl4
	select 
		t1.EMPI 
	from
	(
		select 
			EMPI 
		from MEDICATION 
		where 
			ROOT_COMPANIES_ID=@rootId  and 
			MED_DATA_SRC in(select DATA_SOURCE from DATA_SOURCE where supplemental=0) and 
			FILL_DATE between @ce_startdt1 and @ce_enddt and 
			MEDICATION_CODE in
			(
				select code from HDS.MEDICATION_LIST_TO_CODES where Medication_List_Name='Dementia Medications'
			)
		
	)t1
	Join #cbp_frailtymembers f on 
		f.EMPI=t1.EMPI;

	update #cbpdataset set rexcl=1 from #cbpdataset ds
	join #cbp_reqdexcl4 re4 on re4.EMPI=ds.EMPI and ds.age BETWEEN 66 AND 80;
	
-- Required Exclusion 5

	update #cbpdataset set rexcl=1 from #cbpdataset ds
	join #cbp_frailtymembers re4 on re4.EMPI=ds.EMPI and ds.age>=81;


	
	
-- Optional Exclusion 1 and 3
	drop table if exists #cbp_optexcl;
	CREATE table #cbp_optexcl
	(
		EMPI varchar(100)
				
	)
	insert into #cbp_optexcl
	select distinct 
		EMPI 
	from
	(
	-- Optional Exclusion Kidney 

		Select 
			EMPI
		From GetDiagnosis(@rootId,'1900-01-01',@ce_enddt,'ESRD Diagnosis')
		

		
		Union All

		Select 
			EMPI
		From GetProcedures(@rootId,'1900-01-01',@ce_enddt,'Dialysis Procedure')
		

		Union all

		Select 
			EMPI
		From GetProcedures(@rootId,'1900-01-01',@ce_enddt,'Nephrectomy')
		

		Union all

		Select 
			EMPI
		From GetProcedures(@rootId,'1900-01-01',@ce_enddt,'Kidney Transplant')
		

		Union all

		Select 
			EMPI
		From GetProcedures(@rootId,'1900-01-01',@ce_enddt,'History of Kidney Transplant')
		

		Union all

		Select 
			EMPI
		From GetDiagnosis(@rootId,'1900-01-01',@ce_enddt,'History of Kidney Transplant')
		

		Union all

		Select
			EMPI
		From GETICDPCS(@rootId,'1900-01-01',@ce_enddt,'History of Kidney Transplant')
		

		Union all

		Select
			EMPI
		From GETICDPCS(@rootId,'1900-01-01',@ce_enddt,'Dialysis Procedure')
		

		Union all

		Select
			EMPI
		From GETICDPCS(@rootId,'1900-01-01',@ce_enddt,'Nephrectomy')
		

		Union all

		Select
			EMPI
		From GETICDPCS(@rootId,'1900-01-01',@ce_enddt,'Kidney Transplant')
		
	)t1

	
	
	update #cbpdataset set excl=1 from #cbpdataset ds
	join #cbp_optexcl oe on oe.EMPI=ds.EMPI ;
	
	-- Option Exclusion - Pregnancy
	drop table if exists #cbp_optexcl1;
	CREATE table #cbp_optexcl1
	(
		EMPI varchar(100)
				
	)
	insert into #cbp_optexcl1
	-- Optional Exclusion - Pregnancy
	Select distinct
		EMPI
	From GetDiagnosis(@rootId,@ce_startdt,@ce_enddt,'Pregnancy')
	
	

	update #cbpdataset set excl=1 from #cbpdataset ds
	join #cbp_optexcl1 oe1 on oe1.EMPI=ds.EMPI and ds.gender='F';


	-- Exclude non acute stays

	-- Identify inpatient Stays
	Drop table if exists #cbp_event_ipstays;
	Create Table #cbp_event_ipstays
	(
		EMPI varchar(100),
		ADM_DATE Date,
		DIS_DATE Date,
		CLAIM_ID varchar(100)
	)
	Insert into #cbp_event_ipstays
	Select
		EMPI
		,Coalesce(ADM_DATE,FROM_DATE) as ADM_DATE
		,Coalesce(DIS_DATE,TO_DATE) as DIS_DATE
		,CLAIM_ID
	From CLAIMLINE
	Where
		ROOT_COMPANIES_ID=@rootId and
		ISNULL(POS,'')!='81' and
		REV_CODE in
		(
			select code from HDS.ValueSet_To_Code where Code_system='UBREV' and Value_Set_Name='Inpatient Stay'
		)


	--Identify Non acute stays
	Drop table if exists #cbp_event_nastays;
	Create Table #cbp_event_nastays
	(
		EMPI varchar(100),
		ADM_DATE Date,
		DIS_DATE Date,
		CLAIM_ID varchar(100)
	)
	Insert into #cbp_event_nastays
	Select
		EMPI
		,Coalesce(ADM_DATE,FROM_DATE) as ADM_DATE
		,Coalesce(DIS_DATE,TO_DATE) as DIS_DATE
		,CLAIM_ID
	From CLAIMLINE
	Where
		ROOT_COMPANIES_ID=@rootId and
		ISNULL(POS,'')!='81' and
		(
				REV_CODE in
				(
					select code from HDS.ValueSet_To_Code where Code_system='UBREV' and Value_Set_Name='Nonacute Inpatient Stay'
				)
				or 
				RIGHT('0000'+CAST(Trim(UB_BILL_TYPE) AS VARCHAR(4)),4) in 
				(
					select code from HDS.VALUESET_TO_CODE where code_system='UBTOB' and Value_Set_Name in('Nonacute Inpatient Stay')
				)
		)

		
	--Identify Non acute stays in 2021
	Drop table if exists #cbp_optexcl3;
	Create Table #cbp_optexcl3
	(
		EMPI varchar(100)
	)
	Insert into #cbp_optexcl3
	Select
		na.EMPI
	From #cbp_event_nastays na
	Join #cbp_event_ipstays ip on
		na.EMPI=ip.EMPI and
		na.CLAIM_ID=ip.CLAIM_ID
	where
		na.ADM_DATE between @ce_startdt and @ce_enddt


	update #cbpdataset set excl=1 from #cbpdataset ds
	join #cbp_optexcl3 oe1 on oe1.EMPI=ds.EMPI ;
	
	
		
	-- Event

	-- Create list of patients with Hypertension
	
	drop table if exists #cbp_hypertension;
	CREATE table #cbp_hypertension
	(
		EMPI varchar(100),
		servicedate Date,
		CLAIM_ID varchar(100)
	)				
	insert into #cbp_hypertension
	Select distinct
		EMPI
		,DIAG_START_DATE
		,CLAIM_ID
	From GetDiagnosis(@rootId,@ce_startdt1,@ce_middt,'Essential Hypertension')
	Where
		DIAG_DATA_SRC not in(select Data_Source from Data_source where Supplemental=1 and ROOT_COMPANIES_ID=@rootId)
	

	-- Identify Patients with Outpatient,Telephone,Online visit
	drop table if exists #cbp_hypertensionvstlist;				
	CREATE table #cbp_hypertensionvstlist
	(
		EMPI varchar(100),
		servicedate Date,
		CLAIM_ID varchar(100)
	)
	insert into #cbp_hypertensionvstlist
	Select distinct 
		*
	From
	(

		Select
			EMPI
			,PROC_START_DATE
			,CLAIM_ID
		From GetProcedures(@rootId,@ce_startdt1,@ce_middt,'Outpatient Without UBREV')
		Where
			PROC_DATA_SRC not in(select Data_Source from Data_source where Supplemental=1 and ROOT_COMPANIES_ID=@rootId)

		Union all

		Select
			EMPI
			,PROC_START_DATE
			,CLAIM_ID
		From GetProcedures(@rootId,@ce_startdt1,@ce_middt,'Online Assessments')
		Where
			PROC_DATA_SRC not in(select Data_Source from Data_source where Supplemental=1 and ROOT_COMPANIES_ID=@rootId)

		Union all

		Select
			EMPI
			,PROC_START_DATE
			,CLAIM_ID
		From GetProcedures(@rootId,@ce_startdt1,@ce_middt,'Telephone Visits')
		Where
			PROC_DATA_SRC not in(select Data_Source from Data_source where Supplemental=1 and ROOT_COMPANIES_ID=@rootId)

	)t1	



	-- Event LIST
	
	drop table if exists #cbp_eventlist;
	CREATE table #cbp_eventlist
	(
		EMPI varchar(100)
	)	
	insert into #cbp_eventlist
	select 
		t1.EMPI 
	from
	(
		select distinct 
			h.EMPI
			,h.servicedate 
		from #cbp_hypertension h
		join #cbp_hypertensionvstlist v on 
			h.EMPI=v.EMPI and 
			h.CLAIM_ID=v.CLAIM_ID 
	)t1  
	group by 
		t1.EMPI 
	having 
		count(t1.EMPI)>1
	
	
	update #cbpdataset set event=1 from #cbpdataset ds
	join #cbp_eventlist e on e.EMPI=ds.EMPI ;

	
	
	
	
	
	-- Numerator 
	-- Identify Vst visit (Outpatient Without UBREV Value Set), telephone visit (Telephone Visits Value Set), e-visit or virtual check-in (Online Assessments Value Set), a nonacute inpatient encounter (Nonacute Inpatient Value Set), or remote monitoring event (Remote Blood Pressure Monitoring Value Set) during the measurement year. 
	drop table if exists #numvstlist;				
	CREATE table #numvstlist
	(
		EMPI varchar(100),
		servicedate Date				
	)	
	Insert into #numvstlist
	Select distinct
		*
	From
	(


		Select 
			EMPI
			,PROC_START_DATE 
		From PROCEDURES 
		where
			ROOT_COMPANIES_ID=@rootId and
			ISNULL(PROC_STATUS,'EVN')!='INT' AND
			PROC_START_DATE between @ce_startdt and @ce_enddt and
			PROC_CODE in
			(
				select Code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Outpatient Without UBREV','Telephone Visits','Online Assessments','Nonacute Inpatient','Remote Blood Pressure Monitoring')
			)

		Union all

		Select 
			EMPI
			,VisitDate 
		From VISIT 
		where
			ROOT_COMPANIES_ID=@rootId and
			VisitDate between @ce_startdt and @ce_enddt and
			VisitTypeCode in
			(
				select Code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Outpatient Without UBREV','Telephone Visits','Online Assessments','Nonacute Inpatient','Remote Blood Pressure Monitoring')
			)

		Union all

		Select 
			EMPI
			,DIAG_START_DATE 
		From Diagnosis 
		where
			ROOT_COMPANIES_ID=@rootId and
			DIAG_START_DATE between @ce_startdt and @ce_enddt and
			DIAG_CODE in
			(
				select Code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Outpatient Without UBREV','Telephone Visits','Online Assessments','Nonacute Inpatient','Remote Blood Pressure Monitoring')
			)

		
	)t1
	
					
	-- Numerator compliant members
	drop table if exists #numcompliantmemlist;				
	CREATE table #numcompliantmemlist
	(
		EMPI varchar(100),
		servicedate Date,
		Code varchar(20)
				
	)	
	insert into #numcompliantmemlist
	-- Members with Systolic <140 and diastolic<90 based on results
	select distinct 
		EMPI
		,servicedate 
		,Code
	from
	(
		select distinct 
			s.EMPI
			,s.servicedate 
			,s.Code
		from
		(
			Select 
				EMPI
				,Servicedate
				,Code
			from
			(
				Select
					EMPI
					,Cast(ResultDate as Date) as ServiceDate
					,ResultCode as Code
					,Try_convert(decimal,SUBSTRING(value, PATINDEX('%[0-9]%', value), LEN(value))) as value
				From LAB
				Where
					ROOT_COMPANIES_ID=@rootId and
					ResultDate between @ce_startdt and @ce_enddt and
					(
						TestCode in
						(
							select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Systolic Blood Pressure')
						)
						or
						ResultCode in
						(
							select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Systolic Blood Pressure')
						)
					)
			)t1
			where
				value<140

			Union ALL

			Select
				EMPI
				,Cast(VitalDateTime as Date) as ServiceDate
				,VitalCode as Code
			From VITAL
			Where
				ROOT_COMPANIES_ID=@rootId and
				VitalDateTime between @ce_startdt and @ce_enddt and
				VitalCode in
				(
					select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Systolic Blood Pressure')
				)
				and
				value<cast(140 as float)

	
	)s
	join
	(

		Select distinct
				EMPI
				,Servicedate
				,Code
		from
		(
			Select 
				EMPI
				,Cast(ResultDate as Date) as ServiceDate
				,ResultCode as Code
				,Try_convert(decimal,SUBSTRING(value, PATINDEX('%[0-9]%', value), LEN(value))) as value
			From LAB
			Where
				ROOT_COMPANIES_ID=@rootId and
				ResultDate between @ce_startdt and @ce_enddt and
				(
					TestCode in
					(
						select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Diastolic Blood Pressure')
					)
					or
					ResultCode in
					(
						select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Diastolic Blood Pressure')
					)
				)
			)t1
			where
				value<90

			Union ALL

			Select 
				EMPI
				,Cast(VitalDateTime as Date) as ServiceDate
				,VitalCode as Code
			From VITAL
			Where
				ROOT_COMPANIES_ID=@rootId and
				VitalDateTime between @ce_startdt and @ce_enddt and
				VitalCode in
				(
					select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Diastolic Blood Pressure')
				)
				and
				value<cast(90 as float)
		
	
	)d on 
		d.EMPI=s.EMPI and 
		d.servicedate=s.servicedate

	Union all

-- Members with Systolic <140 and diastolic<90 based on CPT || category codes
	-- Members with Systolic <140 and diastolic<90 based on CPT || category codes
	
	Select
		s.EMPI
		,s.ServiceDate
		,s.Code
	From
	(
		Select distinct
			EMPI
			,Proc_start_date as ServiceDate
			,Proc_Code as Code
		From GetProceduresWithOutMods(@rootId,@ce_startdt,@ce_enddt,'Systolic Less Than 140','CPT CAT II Modifier')
		
		
	)s
	Join
	(
		Select distinct
			EMPI
			,Proc_start_date as ServiceDate
			,Proc_Code as Code
		From GetProceduresWithOutMods(@rootId,@ce_startdt,@ce_enddt,'Diastolic 80-89','CPT CAT II Modifier')

		Union all

		Select distinct
			EMPI
			,Proc_start_date as ServiceDate
			,Proc_Code as Code
		From GetProceduresWithOutMods(@rootId,@ce_startdt,@ce_enddt,'Diastolic Less Than 80','CPT CAT II Modifier')
		
		
	
	)d on 
		s.EMPI=d.EMPI and 
		s.servicedate=d.servicedate
)numcompmemlist
	
				
	
    -- Identify bp readings taken during the specified visit types        
	drop table if exists #bpvisit;						
	CREATE table #bpvisit
	(
		EMPI varchar(100),
		servicedate Date,
		Code varchar(20)
				
	)	
	Insert into #bpvisit
	select 
		nc.EMPI
		,nc.servicedate
		,nc.Code
	from #numcompliantmemlist nc
	join #numvstlist nv on 
		nv.EMPI=nc.EMPI and 
		nv.servicedate=nc.servicedate
	
	
	-- Get 2nd visit date diagnosis of hypertension

	drop table if exists  #cbp_numbasedate;
	CREATE table #cbp_numbasedate
	(
		EMPI varchar(100),
		servicedate Date,
		
				
	)
	-- Identify most recent bp reading and it should be taken after the 2nd hypertension diagnosis
	insert into #cbp_numbasedate
	select 
		EMPI
		,servicedate 

	from
	(
		select 
			EMPI
			,servicedate
			,DENSE_RANK() over (partition by EMPI order by servicedate asc) as row_num 
		from
		(
			select distinct
				h1.EMPI
				,h1.servicedate 
			from #cbp_hypertension h1 
			join #cbp_hypertensionvstlist hv1 on 
				h1.EMPI=hv1.EMPI and 
				h1.CLAIM_ID=hv1.CLAIM_ID  
		)temp 
	)temp1 
	where 
		row_num>1

	
	
	 
	drop table if exists  #numlist;
	CREATE table #numlist
	(
		EMPI varchar(100),
		servicedate Date,
		Code varchar(20)
				
	);
	-- Identify most recent bp reading and it should be taken after the 2nd hypertension diagnosis
	 Insert into #numlist
	 select distinct 
		t1.EMPI
		,t1.servicedate 
		,t1.Code
	from 
	(
		 select 
			bp1.EMPI
			,bp1.servicedate 
			,bp1.Code
		 from #bpvisit bp1 
		 where 
			servicedate=
			(
				select 
					max(bp2.servicedate) 
				from #bpvisit bp2 
				where 
					bp1.EMPI=bp2.EMPI
			) 
	 )t1 
	 join #cbp_numbasedate dt on 
		t1.EMPI=dt.EMPI and 
		t1.servicedate>=dt.servicedate
	
	
	
	-- Updating NUM column
	update #cbpdataset set num=1 from #cbpdataset ds
	join #numlist n on n.EMPI=ds.EMPI ;

	

-- Get ReportId from Report_Details

	exec GetReportDetail @rundate=@rundate,@rootId=@rootId,@startDate=@startDate output,@enddate=@enddate output,@quarter=@quarter output,@reportId=@reportId output




	Delete from HDS.HEDIS_MEASURE_OUTPUT  where MEASURE_ID=@meas and RUN_ID=@reportId and ROOT_COMPANIES_ID=@rootId
	Insert into HDS.HEDIS_MEASURE_OUTPUT(memid,Meas,payer,CE,Event,Epop,Excl,Num,Rexcl,RExclD,Age,Gender,Measure_ID,Measurement_year,RUN_ID,ROOT_COMPANIES_ID)
	SELECT EMPI,meas,payer,CE,EVENT,CASE WHEN CE=1 AND EVENT=1 AND rexcl=0 and rexcld=0 THEN 1 ELSE 0 END AS epop,excl,num,rexcl,rexcld,cast(age as Int) AS age,gender AS gender,'CBP' as Measure_ID,@meas_year,@reportId,@rootId FROM #cbpdataset


	

	-- Insert data into Measure Detailed Line
	Delete from RPT.MEASURE_DETAILED_LINE where MEASURE_ID=@measure_id and REPORT_ID=@reportId and ROOT_COMPANIES_ID=@rootId;
	Insert into RPT.MEASURE_DETAILED_LINE(Provider_Id,PCP_NPI,PCP_NAME,Practice_Name,Specialty,Measure_id,Measure_Name,Payer,PayerId,MEM_FNAME,MEM_MName,MEM_LNAME,MEM_DOB,MEM_GENDER,ENROLLMENT_STATUS,Last_visit_date,Product_Type,Num,Den,Excl,Rexcl,CE,Event,Epop,Report_Id,ReportType,Report_Quarter,Period_Start_date,Period_end_Date,Root_Companies_id,EMPI,MEASURE_TYPE,Code,DateofService)
	Select 
		a.AmbulatoryPCPNPI as Provider_Id 
		,a.AmbulatoryPCPNPI
		,a.AmbulatoryPCPName as PCP_NAME
		,a.AmbulatoryPCPPractice as Practice_Name
		,a.AmbulatoryPCPSpecialty as Specialty
		,@measure_id as Measure_id
		,@measurename as Measure_Name
		,a.DATA_SOURCE as Payer
		,a.PayerId
		,a.MemberFirstName as MEM_FNAME
		,a.MemberMiddleName as MEM_MName
		,a.MemberLastName as MEM_LNAME
		,a.MemberDOB
		,a.MEM_GENDER
		,a.EnrollmentStatus
		,a.AmbulatoryPCPRecentVisit as Last_visit_date
		,d.Payer
		,Num
		,1 as Den
		,Excl
		,case
			when Rexcl=1 or rexcld=1 then 1
			else 0
		end as Rexcl
		,CE
		,Event
		,CASE 
			WHEN CE=1  AND rexcl=0 and rexcld=0 and Event=1 THEN 1 
			ELSE 0 
		END AS epop
		,@reportId
		,@reporttype
		,@quarter
		,@startDate
		,@enddate
		,@rootId
		,d.EMPI as EMPI
		,@measuretype
		,nd.Code
		,nd.ServiceDate
	From #cbpdataset d
	join KPI_ENGINE.RPT.PCP_ATTRIBUTION a on d.EMPI=a.EMPI and a.Reportid=@reportId
	Left outer join #numlist nd on d.EMPI=nd.EMPI
	Where Event=1
--	where a.AmbulatoryPCPSpecialty in('Family Medicine','General Practice','Gynecology','Internal Medicine','Obstetrics & Gynecology')



	-- Insert data into Provider Scorecard
Delete from RPT.PROVIDER_SCORECARD  where MEASURE_ID=@measure_id and REPORT_ID=@reportId and ROOT_COMPANIES_ID=@rootId;
Insert into RPT.PROVIDER_SCORECARD(Provider_Id,PCP_NPI,PCP_NAME,Specialty,Practice_Name,Measure_id,Measure_Name,Measure_Title,MEASURE_SUBTITLE,Measure_Type,NUM_COUNT,DEN_COUNT,Excl_Count,Rexcl_Count,Gaps,Result,Target,To_Target,Report_Id,ReportType,Report_Quarter,Period_Start_Date,Period_End_Date,Root_Companies_Id)
Select 
	Provider_Id
	,PCP_NPI
	,PCP_NAME
	,Specialty
	,Practice_Name
	,Measure_id
	,Measure_Name
	,Measure_Title
	,MEASURE_SUBTITLE
	,Measure_Type
	,SUM(Cast(NUM_COUNT as INT)) as NUM_COUNT
	,SUM(Cast(DEN_COUNT as INT)) as DEN_COUNT
	,SUM(Cast(Excl_Count as INT)) as Excl_Count
	,Sum(Cast(Rexcl_count as INT)) as Rexcl_Count
	,sum(Cast(DEN_Excl as INT)) - SUM(Cast(NUM_COUNT as INT)) as Gaps
	,Case
		when SUM(Cast(DEN_Excl as Float))>0 Then Round((SUM(cast(NUM_COUNT as Float))/ISNULL(NULLIF(SUM(Cast(DEN_Excl as Float)),0),1))*100,2)
		else 0
	end as Result
	,Target as Target
	,Case
		when ((SUM(Cast(DEN_Excl as INT)))*(cast(Target*0.01 as float))) - SUM(Cast(NUM_COUNT as INT))>0 Then CEILING(((SUM(Cast(DEN_Excl as INT)))*(cast(Target*0.01 as float))) - SUM(Cast(NUM_COUNT as INT)))
		Else 0
	end as To_Target
	,Report_Id
	,ReportType
	,Report_Quarter
	,Period_Start_Date
	,Period_End_Date
	,Root_Companies_Id
From
(
	Select distinct
		m.EMPI
		,a.NPI as Provider_id
		,a.NPI as PCP_NPI
		,a.Prov_Name as PCP_Name
		,a.Specialty
		,a.Practice as Practice_Name
		,m.Measure_id
		,m.Measure_Name
		,l.measure_Title
		,l.Measure_SubTitle
		,'Calculated' as Measure_Type
		,Case
			when NUM=1 and excl=0 and rexcl=0 Then 1
			else 0
		end as NUM_COUNT
		,DEN as DEN_COUNT
		,Case
			When DEN=1 and Excl=0 and Rexcl=0 Then 1
			else 0
		End as Den_excl
		,Excl as Excl_Count
		,Rexcl as Rexcl_count
		,Report_Id
		,l.ReportType
		,Report_Quarter
		,Period_Start_Date
		,Period_End_Date
		,m.Root_Companies_Id
		,l.Target
	From RPT.MEASURE_DETAILED_LINE m
	Join RPT.ConsolidatedAttribution_Snapshot a on
		m.EMPI=a.EMPI
	Join RFT.UHN_measuresList l on
		m.Measure_Id=l.measure_id
	Join RFT.UHN_MeasureSpecialtiesMapping s on
		a.Specialty=s.Specialty and
		m.MEASURE_ID=s.Measure_id
	where Enrollment_Status='Active' and
		  a.NPI!='' and
		  m.MEASURE_ID=@measure_id and
		  REPORT_ID=@reportId 
)t1
Group by Provider_Id,PCP_NPI,PCP_NAME,Specialty,Practice_Name,Measure_id,Measure_Name,Measure_Title,MEASURE_SUBTITLE,Report_Id,ReportType,Report_Quarter,Period_Start_Date,Period_End_Date,Root_Companies_Id,Measure_Type,Target



	
		SET @PERF_ROW = @@ROWCOUNT
		SET @PERF_DURATION = DATEDIFF(MINUTE,@PERF_START,GETDATE());
		EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, 'Enrollment Completed', @PERF_ROW, @DURATION_IN_MIN=@PERF_DURATION, @LOG2ID=@LOGID;
		EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, @LOG2ID=@LOGID, @END_FLAG=1;

END TRY
BEGIN CATCH
		BEGIN
			EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, 'FATAL ERROR', @LOG2ID=@LOGID;
			RAISERROR ('',16,0)
			RETURN 16
        END
	END CATCH
RETURN 0
END

GO
