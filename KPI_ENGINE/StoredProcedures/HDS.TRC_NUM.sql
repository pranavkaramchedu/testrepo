SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
CREATE proc [HDS].[TRC_NUM]
AS

drop table if exists #TRC_memlist; 

CREATE TABLE #TRC_memlist (
    memid varchar(100)
    
);


insert into #TRC_memlist
SELECT DISTINCT en.MemID FROM HDS.HEDIS_MEMBER_GM gm
join hds.HEDIS_MEMBER_EN en on en.MemID=gm.MemID and en.MEASURE_ID=gm.MEASURE_ID
WHERE en.MEASURE_ID='TRC_SAMPLE' and
CONVERT(DATE,en.StartDate)<='2020-12-31' AND CONVERT(DATE,FinishDate)>='2020-01-01'
AND YEAR('2020-12-31')-YEAR(DOB) >=18 ORDER BY 1;


	-- Inpatient Stay List
	drop table if exists #TRC_inpatientstaylist;
	CREATE table #trc_inpatientstaylist
	(
		Memid varchar(50),
		
		date_s varchar(8),
		date_disch varchar(8),
		claimid int
	);

Insert into #TRC_inpatientstaylist
select distinct MemID,Date_S,date_disch,claimid from HDS.HEDIS_VISIT v where v.Measure_id='TRC_SAMPLE' and v.HCFAPOS!=81 and suppdata='N' and v.date_disch!='' and CONVERT(DATE,v.date_disch) between '2020-01-01' and '2020-12-01' and v.REV in (select code from HDS.VALUESET_TO_CODE where code_system='UBREV' and Value_Set_Name in('Inpatient Stay'));
	
CREATE CLUSTERED INDEX ix_cdc_inpatientstaylist ON #TRC_inpatientstaylist ([memid],[date_s]);

----Readmission---------


drop table if exists #Readmission;
	CREATE table #Readmission
	(
		Memid varchar(50)
	);
Insert into #Readmission(Memid)
select distinct MemID from HDS.HEDIS_VISIT v where v.Measure_id='TRC_SAMPLE' and v.HCFAPOS!=81 and suppdata='N' and v.date_disch!='' and CONVERT(DATE,v.date_disch) >'2020-12-01' and v.REV in (select code from HDS.VALUESET_TO_CODE where code_system='UBREV' and Value_Set_Name in('Inpatient Stay'));


drop table if exists #TRC_inpatientstaylist_Final;
	CREATE table #TRC_inpatientstaylist_Final
	(
		Memid varchar(50)
	);
Insert into #TRC_inpatientstaylist_Final(Memid)
select distinct IP.Memid from #TRC_inpatientstaylist Ip
left join #Readmission R on IP.Memid=R.Memid
where  R.Memid is NULL ----AND ip.Memid='148480'





	drop table if exists #Denomerator;
	CREATE table #Denomerator
	(
		Memid varchar(50)
	);

Insert into #Denomerator
Select distinct M.memid from #TRC_memlist M
inner join (select distinct MemId from #TRC_inpatientstaylist_Final) IP on M.memid=Ip.Memid
----where M.memid='146792'
CREATE CLUSTERED INDEX ix_TRC_Denomeratort ON #Denomerator ([memid]);


-- Create Temp Patient Enrollment
drop table if exists #TRC_tmpsubscriber;

CREATE TABLE #TRC_tmpsubscriber (
    memid varchar(100),
    dob varchar(8),
	gender varchar(1),
	payer varchar(50),
	StartDate varchar(8),
	EndDate varchar(8),
	RN int
);
insert into #TRC_tmpsubscriber
SELECT en.MemID,gm.DOB,gm.Gender,en.Payer,en.StartDate,en.FinishDate, ROW_NUMBER() OVER(partition by en.MemID order by en.StartDate,en.FinishDate) as RN
FROM HDS.HEDIS_MEMBER_GM gm
join hds.HEDIS_MEMBER_EN en on en.MemID=gm.MemID and en.MEASURE_ID=gm.MEASURE_ID
Inner join #Denomerator D on gm.MemID=D.Memid
WHERE gm.MEASURE_ID='TRC_SAMPLE' and
CONVERT(DATE,en.StartDate)<='2020-12-31' AND CONVERT(DATE,FinishDate)>='2020-01-01'
AND YEAR('2020-12-31')-YEAR(DOB) >=18 ORDER BY en.MemID,CONVERT(DATE,en.StartDate),CONVERT(DATE,FinishDate);

CREATE CLUSTERED INDEX ix_tmpsub_memid ON #TRC_tmpsubscriber ([memid],[StartDate],[EndDate]);




-- Inpatient Stay List
	drop table if exists #TRC_DISCHARGE_MEMBERS;
	CREATE table #TRC_DISCHARGE_MEMBERS
	(
		Memid varchar(50),
		date_s varchar(8),
		date_disch varchar(8),
		claimid int
	);

Insert into #TRC_DISCHARGE_MEMBERS
select distinct v.MemID,Date_S,date_disch,claimid from HDS.HEDIS_VISIT v 
Inner join #TRC_tmpsubscriber S on v.MemID=S.memid and convert(date,v.Date_Disch) between convert(date, S.StartDate) and convert(date,S.EndDate)
where v.Measure_id='TRC_SAMPLE' and v.HCFAPOS!=81 and suppdata='N' and v.date_disch!='' and CONVERT(DATE,v.date_disch) between '2020-01-01' and '2020-12-01' and v.REV in (select code from HDS.VALUESET_TO_CODE where code_system='UBREV' and Value_Set_Name in('Inpatient Stay'))
AND v.MEMID IN (Select MemID from #Denomerator)

/*
drop table if exists #num;

CREATE table #num
(
	Memid varchar(50)
);

Declare @MemId varchar(50)
Declare @Discharge_Date varchar(10)

DECLARE C CURSOR FOR 
select distinct MemID,date_disch from #TRC_DISCHARGE_MEMBERS

OPEN C  
FETCH NEXT FROM C INTO @MemId,@Discharge_Date
WHILE @@FETCH_STATUS = 0  
BEGIN  

	Insert into #num
	select distinct Memid from(
	select distinct MemID from HDS.HEDIS_VISIT v
	where MEASURE_ID='TRC_SAMPLE' and Date_S!='' and MemId=@MemId AND convert(date,Date_S) between DATEADD(d,1,CONVERT(DATE,@Discharge_Date)) AND DATEADD(d,30,CONVERT(DATE,@Discharge_Date))
	and (
	Diag_I_1 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Telephone Visits'))
	or
	Diag_I_2 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Telephone Visits'))
	or
	Diag_I_3 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Telephone Visits'))
	or
	Diag_I_4 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Telephone Visits'))
	or
	Diag_I_5 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Telephone Visits'))
	or
	Diag_I_6 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Telephone Visits'))
	or
	Diag_I_7 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Telephone Visits'))
	or
	Diag_I_8 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Telephone Visits'))
	or
	Diag_I_9 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Telephone Visits'))
	or
	Diag_I_10 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Telephone Visits'))
	or
	CPT in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in ('Online Assessments','Outpatient','Telephone Visits','Transitional Care Management Services'))
	or
	HCPCS in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in ('Online Assessments','Outpatient'))
	or 
	Proc_I_1 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in ('Outpatient'))
	or 
	Proc_I_2 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in ('Outpatient'))
	or 
	Proc_I_3 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in ('Outpatient'))
	or 
	Proc_I_4 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in ('Outpatient'))
	or 
	Proc_I_5 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in ('Outpatient'))
	or 
	Proc_I_6 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in ('Outpatient'))
	or
	Rev in(select code from HDS.VALUESET_TO_CODE where code_system='UBREV' and Value_Set_Name in('Outpatient'))
	)
)t1
--Union 
---select distinct MemID from HDS.HEDIS_OBS v where Measure_ID='CHL_TEST' AND ocode in (select code from HDS.VALUESET_TO_CODE where Value_Set_Name in ('Chlamydia Tests')) and convert(date,V.date) between '2020-01-01' and '2020-12-31'


      FETCH NEXT FROM C INTO @MemId,@Discharge_Date
END 

CLOSE C  
DEALLOCATE C 
*/






drop table if exists #num;

CREATE table #num
(
	Memid varchar(50)
);
	Insert into #num
	select distinct V.MemID from HDS.HEDIS_VISIT v
inner join
(select distinct MemID,date_disch from #TRC_DISCHARGE_MEMBERS) D
on D.MemID=v.Memid and convert(date,Date_S) between DATEADD(d,1,CONVERT(DATE,D.date_disch)) AND DATEADD(d,30,CONVERT(DATE,D.date_disch))
where MEASURE_ID='TRC_SAMPLE' and Date_S!='' ---and MemId=@MemId AND convert(date,Date_S) between DATEADD(d,1,CONVERT(DATE,@Discharge_Date)) AND DATEADD(d,30,CONVERT(DATE,@Discharge_Date))
	and (
	Diag_I_1 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Telephone Visits'))
	or
	Diag_I_2 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Telephone Visits'))
	or
	Diag_I_3 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Telephone Visits'))
	or
	Diag_I_4 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Telephone Visits'))
	or
	Diag_I_5 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Telephone Visits'))
	or
	Diag_I_6 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Telephone Visits'))
	or
	Diag_I_7 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Telephone Visits'))
	or
	Diag_I_8 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Telephone Visits'))
	or
	Diag_I_9 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Telephone Visits'))
	or
	Diag_I_10 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Telephone Visits'))
	or
	CPT in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in ('Online Assessments','Outpatient','Telephone Visits','Transitional Care Management Services'))
	or
	HCPCS in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in ('Online Assessments','Outpatient'))
	or 
	Proc_I_1 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in ('Outpatient'))
	or 
	Proc_I_2 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in ('Outpatient'))
	or 
	Proc_I_3 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in ('Outpatient'))
	or 
	Proc_I_4 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in ('Outpatient'))
	or 
	Proc_I_5 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in ('Outpatient'))
	or 
	Proc_I_6 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in ('Outpatient'))
	or
	Rev in(select code from HDS.VALUESET_TO_CODE where code_system='UBREV' and Value_Set_Name in('Outpatient'))
	)



---select * into #TRCdataset from hds.TRCdataset
update #TRCdataset set CE=0 
update #TRCdataset set NUM=1 from #TRCdataset ds join #NUM NUM on NUM.memid=ds.patient_id;



select * from #TRCdataset E
left join hds.HEDIS_score S 
on E.patient_id=S.MemID 
where measure_id='TRC_SAMPLE' and E.NUM<>S.NUM AND S.NUM=0


select * from #TRCdataset where patient_id='102839' 
select top 100 * from hds.HEDIS_SCORE E where memid='102839' and MEASURE_ID='TRC_SAMPLE'








/*select distinct Value_Set_Name,Code_System from hds.VALUESET_TO_CODE 
where Value_Set_Name 
in 
(
'Outpatient',
'Telephone Visits',
'Transitional Care Management Services',
'Online Assessments ',
'Medication Reconciliation Encounter',
'Medication Reconciliation Intervention'
) order by 1,2
*/
GO
