SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

CREATE proc hds.LOAD_PROVIDER_TO_HEDIS
AS
BEGIN

INSERT INTO [KPI_ENGINE].[HDS].[HEDIS_PROVIDER]
(
	 [ProvID]
    ,[PCP]
    ,[OBGYN]
    ,[MHProv]
    ,[EyeCProv]
    ,[Dentist]
    ,[Neph]
    ,[Anes]
    ,[NPR]
    ,[PAS]
    ,[ProvPres]
    ,[PHAProv]
    ,[Hosp]
    ,[SNF]
    ,[Surg]
    ,[RN]
    ,[MEASURE_ID]
    ,[LOAD_DATE]
)
SELECT PROV_ID AS [ProvID]
      ,PROV_PCP_FLAG AS [PCP]
      ,PROV_OB_FLAG AS [OBGYN]
      ,PROV_MH_FLAG AS [MHProv]
      ,PROV_EYE_FLAG AS [EyeCProv]
      ,PROV_DEN_FLAG AS [Dentist]
      ,PROV_NEP_FLAG AS [Neph]
      ,PROV_CD_FLAG AS [Anes]
      ,PROV_NP_FLAG AS [NPR]
      ,PROV_PA_FLAG AS [PAS]
      ,PROV_RX_FLAG AS [ProvPres]
      ,PROV_PHARM_FLAG AS [PHAProv]
      ,PROV_HOSP_FLAG AS [Hosp]
      ,PROV_SNF_FLAG AS [SNF]
      ,PROV_SURG_FLAG AS [Surg]
      ,PROV_RN_FLAG AS [RN]
      ,'159' AS [MEASURE_ID]
      ,convert(date,getdate()) AS [LOAD_DATE]
  ---FROM [KPI_ENGINE].[HDS].[HEDIS_PROVIDER]
  FROM KPI_ENGINE.DBO.PROVIDER

END
GO
