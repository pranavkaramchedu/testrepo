SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

CREATE proc [dbo].[KPI_UHN_UnplannedAdmissions_39]
AS

BEGIN
DECLARE @INFO VARCHAR(8000)
					,	@TRUE BIT = 1
					,	@FALSE BIT = 0
					,	@DB_ID INT = DB_ID()
					,	@LOGID INT = 0
					,	@PROC VARCHAR(500)=OBJECT_SCHEMA_NAME(@@PROCID)+'.'+OBJECT_NAME(@@PROCID)
					,	@PERF_START DATETIME
					,	@PERF_DURATION INT
					,	@PERF_ROW INT
					,	@RC	INT
					;

		EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, @INFO=@INFO, @LOG2ID=@LOGID OUT;
		DECLARE @PROCFULLNAME VARCHAR(500) = DB_NAME()+'.'+@PROC;
		----EXEC dbo.SP_MI_UTIL_SEND_MAIL @PROCFULLNAME, 'STARTED', 1, @ECHO=0; 
	DECLARE @MSG VARCHAR(1000)
	DECLARE @PERF_START_PROC DATETIME 
	DECLARE @PERF_RPS NUMERIC(10,2) 
	DECLARE @ROWS INT 
	DECLARE @PERF_ROWS INT 
	SET @PERF_ROWS = 0 
	SET @PERF_START = GETDATE() 
	SET @PERF_START_PROC = GETDATE() 

BEGIN TRY




-- Declare Variables
declare @rundate Date=GetDate();
declare @meas_year varchar(4)=Year(Dateadd(month,-2,@rundate))
declare @rootId INT=159
declare @attributiondate Date=DATEADD(month, DATEDIFF(month, 0, Dateadd(month,-2,@rundate)), 0)

Declare @ce_startdt Date;
Declare @ce_startdt1 Date;
Declare @ce_startdt3 Date;
Declare @ce_startdt2 Date;
Declare @ce_novdt Date;
Declare @ce_enddt Date;
Declare @ce_enddt1 Date;


Declare @startDate Date;
Declare @enddate date;
Declare @quarter varchar(20)
Declare @reportId INT;
Declare @measure_id varchar(10);
Declare @target INT;
Declare @domain varchar(100);
Declare @subdomain varchar(100);
Declare @measuretype varchar(100);
Declare @measurename varchar(100);
Declare @reporttype varchar(100);

-- Set Measure dates
SET @ce_startdt=concat(@meas_year,'-01-01');
SET @ce_novdt=concat(@meas_year,'-11-30');
SET @ce_enddt=concat(@meas_year,'-12-31');
SET @ce_startdt1=concat(@meas_year-1,'-01-01');
SET @ce_enddt1=concat(@meas_year-1,'-12-31');
SET @ce_startdt3=concat(@meas_year-3,'-01-01');
SET @ce_startdt2=concat(@meas_year-2,'-01-01');

Set @reporttype='Network'
Set @measurename='All cause unplanned admissions for patients with multiple chronic conditions'
--Set @startDate=DATEADD(yy, DATEDIFF(yy, 0,Dateadd(month,-2,GETDATE())), 0) 
--Set @enddate=eomonth(Dateadd(month,-2,GetDate()))
--Set @quarter=Concat(Year(@enddate),' - Q',DATEPART(q, @enddate))
set @target=52
Set @domain='Utilization Management'
Set @subdomain='Inpatient Utilization'
Set @measuretype='Calculated'
Set @measure_id=39


-- Identify Deceased Members
Drop table if exists #39_deceasedmembers
Create table #39_deceasedmembers
(
	EMPI varchar(100)
)
Insert into #39_deceasedmembers
select * from deceasedmembers(@rootId,@ce_startdt,@ce_enddt)


--Identifying Members with AMI
Drop table if exists #39_AMI;
Create table #39_AMI
(
	EMPI varchar(100),
	ChronicCondition varchar(50)
)
Insert into #39_AMI
select distinct
	d.EMPI
	,'AMI' as ChronicCondition
from DIAGNOSIS d
join CLAIMLINE c on d.CLAIM_ID=c.CLAIM_ID and 
					d.ROOT_COMPANIES_ID=c.ROOT_COMPANIES_ID and 
					d.DIAG_DATA_SRC=c.CL_DATA_SRC
where 
	d.ROOT_COMPANIES_ID=@rootId and
	DIAG_START_DATE between @ce_startdt and @ce_enddt and
	DIAG_CODE in(select Replace(code,'.','') from REG.REGISTRY_VALUESET where Measure_id=@measure_id and ValueSetName='AMI') and
	ISNULL(c.POS,'')!='81'

	


--Identifying Members with Asthma
Drop table if exists #39_Asthma;
Create table #39_Asthma
(
	EMPI varchar(100),
	ChronicCondition varchar(50)
)
Insert into #39_Asthma
select distinct
	d.EMPI
	,'Asthma' as ChronicCondition
from DIAGNOSIS d
join CLAIMLINE c on d.CLAIM_ID=c.CLAIM_ID and 
					d.ROOT_COMPANIES_ID=c.ROOT_COMPANIES_ID and 
					d.DIAG_DATA_SRC=c.CL_DATA_SRC
where 
	d.ROOT_COMPANIES_ID=@rootId and
	DIAG_START_DATE between @ce_startdt and @ce_enddt and
	DIAG_CODE in(select Replace(code,'.','') from REG.REGISTRY_VALUESET where Measure_id=@measure_id and ValueSetName='Asthma') and
	ISNULL(c.POS,'')!='81'


-- Identify members with Atrial Fibrillation
Drop table if exists #39_AtrialFibrillation;
Create table #39_AtrialFibrillation
(
	EMPI varchar(100),
	ChronicCondition varchar(50)
)
Insert into #39_AtrialFibrillation
select distinct
	d.EMPI
	,'Atrial Fibrillation' as ChronicCondition
from DIAGNOSIS d
join CLAIMLINE c on d.CLAIM_ID=c.CLAIM_ID and 
					d.ROOT_COMPANIES_ID=c.ROOT_COMPANIES_ID and 
					d.DIAG_DATA_SRC=c.CL_DATA_SRC
where 
	d.ROOT_COMPANIES_ID=@rootId and
	DIAG_START_DATE between @ce_startdt and @ce_enddt and
	DIAG_CODE in(select Replace(code,'.','') from REG.REGISTRY_VALUESET where Measure_id=@measure_id and ValueSetName='Atrial Fibrillation') and
	ISNULL(c.POS,'')!='81'


-- Identify members with COPD
Drop table if exists #39_COPD;
Create table #39_COPD
(
	EMPI varchar(100),
	ChronicCondition varchar(50)
)
Insert into #39_COPD
select distinct
	d.EMPI
	,'COPD' as ChronicCondition
from DIAGNOSIS d
join CLAIMLINE c on d.CLAIM_ID=c.CLAIM_ID and 
					d.ROOT_COMPANIES_ID=c.ROOT_COMPANIES_ID and 
					d.DIAG_DATA_SRC=c.CL_DATA_SRC
where 
	d.ROOT_COMPANIES_ID=@rootId and
	DIAG_START_DATE between @ce_startdt and @ce_enddt and
	DIAG_CODE in(select Replace(code,'.','') from REG.REGISTRY_VALUESET where Measure_id=@measure_id and ValueSetName='COPD') and
	ISNULL(c.POS,'')!='81'


-- Identify members with Depression
Drop table if exists #39_Depression;
Create table #39_Depression
(
	EMPI varchar(100),
	ChronicCondition varchar(50)
)
Insert into #39_Depression
select distinct
	d.EMPI
	,'Depression' as ChronicCondition
from DIAGNOSIS d
join CLAIMLINE c on d.CLAIM_ID=c.CLAIM_ID and 
					d.ROOT_COMPANIES_ID=c.ROOT_COMPANIES_ID and 
					d.DIAG_DATA_SRC=c.CL_DATA_SRC
where 
	d.ROOT_COMPANIES_ID=@rootId and
	DIAG_START_DATE between @ce_startdt and @ce_enddt and
	DIAG_CODE in(select Replace(code,'.','') from REG.REGISTRY_VALUESET where Measure_id=@measure_id and ValueSetName='Depression') and
	ISNULL(c.POS,'')!='81'


--Identifying Members with Alzheimers
Drop table if exists #39_Alzheimers;
Create table #39_Alzheimers
(
	EMPI varchar(100),
	ChronicCondition varchar(50)
)
Insert into #39_Alzheimers
select distinct
	d.EMPI
	,'Alzheimers' as ChornicCondition
from DIAGNOSIS d
join CLAIMLINE c on d.CLAIM_ID=c.CLAIM_ID and 
					d.ROOT_COMPANIES_ID=c.ROOT_COMPANIES_ID and 
					d.DIAG_DATA_SRC=c.CL_DATA_SRC
where 
	d.ROOT_COMPANIES_ID=@rootId and
	DIAG_START_DATE between @ce_startdt3 and @ce_enddt and
	DIAG_CODE in(select Replace(code,'.','') from REG.REGISTRY_VALUESET where Measure_id=@measure_id and ValueSetName='Alzheimers diseases') and
	ISNULL(c.POS,'')!='81'


--Identifying Members with CKD
Drop table if exists #39_CKD;
Create table #39_CKD
(
	EMPI varchar(100),
	ChronicCondition varchar(50)
)
Insert into #39_CKD
select distinct
	d.EMPI
	,'CKD' as ChronicCondition
from DIAGNOSIS d
join CLAIMLINE c on d.CLAIM_ID=c.CLAIM_ID and 
					d.ROOT_COMPANIES_ID=c.ROOT_COMPANIES_ID and 
					d.DIAG_DATA_SRC=c.CL_DATA_SRC
where 
	d.ROOT_COMPANIES_ID=@rootId and
	DIAG_START_DATE between @ce_startdt2 and @ce_enddt and
	DIAG_CODE in(select Replace(code,'.','') from REG.REGISTRY_VALUESET where Measure_id=@measure_id and ValueSetName='CKD') and
	ISNULL(c.POS,'')!='81'


--Identifying Members with Heart Failure
Drop table if exists #39_Heartfailure;
Create table #39_Heartfailure
(
	EMPI varchar(100),
	ChronicCondition varchar(50)
)
Insert into #39_Heartfailure
select distinct
	d.EMPI
	,'Heart Failure' as ChronicCondition
from DIAGNOSIS d
join CLAIMLINE c on d.CLAIM_ID=c.CLAIM_ID and 
					d.ROOT_COMPANIES_ID=c.ROOT_COMPANIES_ID and 
					d.DIAG_DATA_SRC=c.CL_DATA_SRC
where 
	d.ROOT_COMPANIES_ID=@rootId and
	DIAG_START_DATE between @ce_startdt2 and @ce_enddt and
	DIAG_CODE in(select Replace(code,'.','') from REG.REGISTRY_VALUESET where Measure_id=@measure_id and ValueSetName='Heart Failure') and
	ISNULL(c.POS,'')!='81'



--Identifying Members with 'Stroke and TIA'
Drop table if exists #39_strokeandTIA;
Create table #39_strokeandTIA
(
	EMPI varchar(100),
	CLAIM_ID varchar(100),
	DATA_SOURCE varchar(50)
)
Insert into #39_strokeandTIA
select distinct
	d.EMPI
	,d.CLAIM_ID
	,d.DIAG_DATA_SRC	
from DIAGNOSIS d
join CLAIMLINE c on d.CLAIM_ID=c.CLAIM_ID and 
					d.ROOT_COMPANIES_ID=c.ROOT_COMPANIES_ID and 
					d.DIAG_DATA_SRC=c.CL_DATA_SRC
where 
	d.ROOT_COMPANIES_ID=@rootId and
	DIAG_START_DATE between @ce_startdt and @ce_enddt and
	DIAG_CODE in(select Replace(code,'.','') from REG.REGISTRY_VALUESET where Measure_id=@measure_id and ValueSetName='Stroke and TIA') and
	ISNULL(c.POS,'')!='81'


--Identifying Members with 'Stroke and TIA' Exclusion
Drop table if exists #39_strokeandTIAexcl;
Create table #39_strokeandTIAexcl
(
	EMPI varchar(100),
	CLAIM_ID varchar(100),
	DATA_SOURCE varchar(50)
)
Insert into #39_strokeandTIAexcl
select distinct
	d.EMPI
	,d.CLAIM_ID
	,d.DIAG_DATA_SRC
from DIAGNOSIS d
join CLAIMLINE c on d.CLAIM_ID=c.CLAIM_ID and 
					d.ROOT_COMPANIES_ID=c.ROOT_COMPANIES_ID and 
					d.DIAG_DATA_SRC=c.CL_DATA_SRC
where 
	d.ROOT_COMPANIES_ID=@rootId and
	DIAG_START_DATE between @ce_startdt and @ce_enddt and
	DIAG_CODE in(select Replace(code,'.','') from REG.REGISTRY_VALUESET where Measure_id=@measure_id and ValueSetName='Stroke and TIA Exclusion') and
	DIAG_SEQ_NO=1 and
	ISNULL(c.POS,'')!='81'



-- Identify members with more than one chronic condition
drop table if exists #39_chronicmembers
Create Table #39_chronicmembers
(
	EMPI varchar(100),
	dcount INT
)
Insert into #39_chronicmembers
select 
	EMPI
	,count(*) as dcount 
from
(

	select distinct 
		t1.EMPI
		,'Stroke and TIA' as ChronicCondition 
	from #39_strokeandTIA t1
	left outer Join #39_strokeandTIAexcl t2 on t1.CLAIM_ID=t2.CLAIM_ID and 
												t1.DATA_SOURCE=t2.DATA_SOURCE
	where t2.CLAIM_ID is null

	Union All

	Select * from #39_Alzheimers

	Union All

	Select * from #39_AMI

	Union all

	Select * from #39_Asthma

	Union all

	Select * from #39_AtrialFibrillation

	Union All

	Select * from #39_CKD

	Union all

	Select * from #39_COPD

	Union all

	select * from #39_Depression

	Union all

	Select * from #39_Heartfailure

)t1
Group by EMPI
Having count(*)>1


-- Identify Denominator Members
drop table if exists #39_denominatorset;
Create table #39_denominatorset
(
	EMPI varchar(100),
	MEMBER_ID varchar(100),
	Gender varchar(10)
)
Insert into #39_denominatorset
select 
	EMPI_ID
	,Org_Patient_Extension_ID as MEMBER_ID
	,Gender
from open_empi
left outer join #39_deceasedmembers d on EMPI_ID=d.EMPI
Join #39_chronicmembers c on c.EMPI=EMPI_ID
where d.EMPI is null




-- Identify Numerator Set
Drop table if exists #39_numerator;
Create table #39_numerator
(
	EMPI varchar(100)
)
Insert into #39_numerator
Select distinct 
	EMPI 
from
(
	Select 
		distinct c.EMPI
		,Case
			when ADM_DATE is null Then From_date
			else ADM_DATE
		end as ADM_DATE
	From KPI_ENGINE.dbo.CLAIMLINE c
	Join #39_denominatorset d on c.EMPI=d.EMPI
	Where
		ROOT_COMPANIES_ID=@rootId and
		REV_CODE in
		(
			select code from HDS.VALUESET_TO_CODE where Code_System='UBREV' and Value_Set_Name='Inpatient Stay'
		) 
		and
		ISNULL(POS,'')!='81'
)t1
where
ADM_DATE between @ce_startdt and @ce_enddt


-- Get Numerator Details
Drop table if exists #39_num_detail
Create table #39_num_detail
(
	EMPI varchar(100),
	ServiceDate Date,
	Code varchar(50)
)
Insert into #39_num_detail
Select
	EMPI
	,ServiceDate
	,Code
	
From
(
	Select
		*
		,DENSE_RANK() over(partition by EMPI order by ServiceDate Desc,rn Desc) as rnk
	From
	(
		Select distinct 
			EMPI
			,Code
			,ServiceDate
			,ROW_NUMBER() over (partition by EMPI order by ServiceDate Desc) as rn
		from(

			Select 
				c.EMPI
				,c.REV_Code as Code
				,Case
					When c.ADM_DATE is null then FROM_DATE
					else ADM_DATE
					end as ServiceDate
			From KPI_ENGINE.dbo.CLAIMLINE c
			Join #39_denominatorset d on c.EMPI=d.EMPI
			Where
				ROOT_COMPANIES_ID=@rootId and
				REV_CODE in(select code from HDS.VALUESET_TO_CODE where Code_System='UBREV' and Value_Set_Name='Inpatient Stay') and
				ISNULL(POS,'')!='81'
		)t1
		where 
		ServiceDate between @ce_startdt and @ce_enddt
	)t2
)t3
Where rnk=1


-- Identify Exclusions 
drop table if exists #39_exclusions;
CREATE table #39_exclusions
(
	EMPI varchar(50)
		
);
Insert into #39_exclusions
select distinct EMPI from hospicemembers(@rootId,@ce_startdt,@ce_enddt)



-- Get ReportId from Report_Details

	exec GetReportDetail @rundate=@rundate,@rootId=@rootId,@startDate=@startDate output,@enddate=@enddate output,@quarter=@quarter output,@reportId=@reportId output


-- Create the output as required
Drop table if exists #39_dataset
Create Table #39_dataset
(
	EMPI varchar(100),
	Provider_Id varchar(20),
	PCP_NPI varchar(20),
	PCP_NAME varchar(200),
	Practice_Name varchar(200),
	Specialty varchar(100),
	Measure_id varchar(20),
	Measure_Name varchar(100),
	Payer varchar(50),
	PayerId varchar(100),
	MEM_FNAME varchar(100),
	MEM_MName varchar(50),
	MEM_LNAME varchar(100),
	MEM_DOB Date,
	MEM_GENDER varchar(20),
	Enrollment_Status varchar(20),
	Last_visit_date Date,
	Product_Type varchar(50),
	Num bit,
	Den bit,
	Excl bit,
	Rexcl bit,
	Report_Id INT,
	ReportType varchar(20),
	Report_Quarter varchar(20),
	Period_Start_Date Date,
	Period_End_Date Date,
	Root_Companies_Id INT

)
Insert into #39_dataset
select distinct
	d.EMPI
	,a.AmbulatoryPCPNPI as Provider_Id 
	,a.AmbulatoryPCPNPI as PCP_NPI
	,a.AmbulatoryPCPName as PCP_NAME
	,a.AmbulatoryPCPPractice as Practice_Name
	,a.AmbulatoryPCPSpecialty as Specialty
	,@measure_id as Measure_id
	,@measurename as Measure_Name
	,a.DATA_SOURCE as Payer
	,a.PayerId
	,a.MemberFirstName as MEM_FNAME
	,a.MemberMiddleName as MEM_MName
	,a.MemberLastName as MEM_LNAME
	,a.MemberDOB
	,m.Gender as MEM_GENDER
	,a.EnrollmentStatus
	,a.AmbulatoryPCPRecentVisit as Last_visit_date
	,mm.PAYER_TYPE as ProductType
	,0 as Num
	,1 as Den
	,0 as Excl
	,0 as Rexcl
	,@reportId
	,@reporttype as ReportType
	,@quarter as Report_quarter
	,@startDate as Period_start_date
	,@enddate as Period_end_date
	,@rootId
from #39_denominatorset d
join RPT.PCP_ATTRIBUTION a on d.EMPI=a.EMPI and a.ReportId=@reportId
Join open_empi_master m on d.EMPI=m.EMPI_ID
left outer join MEMBER_MONTH mm on d.EMPI=mm.EMPI and mm.MEMBER_MONTH_START_DATE=DATEADD(month, DATEDIFF(month, 0, @enddate), 0)
where a.AssignedStatus='Assigned'


update ds Set num=1 from #39_dataset ds join #39_numerator n on ds.EMPI=n.EMPI
update ds Set Rexcl=1 from #39_dataset ds join #39_exclusions n on ds.EMPI=n.EMPI



-- Insert data into Measure Detailed Line
Delete from RPT.MEASURE_DETAILED_LINE where MEASURE_ID=@measure_id and REPORT_ID=@reportId and ROOT_COMPANIES_ID=@rootId;
		
Insert into RPT.MEASURE_DETAILED_LINE(Provider_Id,PCP_NPI,PCP_NAME,Practice_Name,Specialty,Measure_id,Measure_Name,Payer,PayerId,MEM_FNAME,MEM_MName,MEM_LNAME,MEM_DOB,MEM_GENDER,ENROLLMENT_STATUS,Last_visit_date,Product_Type,Num,Den,Excl,Rexcl,Report_Id,ReportType,Report_Quarter,Period_Start_date,Period_end_Date,Root_Companies_id,EMPI,MEASURE_TYPE,Code,DateofService)
Select Provider_Id,PCP_NPI,PCP_NAME,Practice_Name,Specialty,Measure_id,Measure_Name,Payer,PayerId,MEM_FNAME,MEM_MName,MEM_LNAME,MEM_DOB,MEM_GENDER,Enrollment_Status,Last_visit_date,Product_Type,Num,Den,Excl,Rexcl,Report_Id,ReportType,Report_Quarter,Period_Start_date,Period_end_Date,Root_Companies_id,d.EMPI,@measuretype,Code,ServiceDate
From #39_dataset d
Left outer join #39_num_detail nd on d.EMPI=nd.EMPI
--where Specialty in('Cardiac Electrophysiology','Cardiology','Cardiovascular Disease','Family Medicine','Gastroenterology','General Practice','Internal Medicine','Interventional Cardiology','Nephrology','Pulmonary Diseases')




-- Insert data into Provider Scorecard
Delete from RPT.PROVIDER_SCORECARD  where MEASURE_ID=@measure_id and REPORT_ID=@reportId and ROOT_COMPANIES_ID=@rootId;

/*
Insert into RPT.PROVIDER_SCORECARD(Measure_id,Measure_Name,Measure_Title,MEASURE_SUBTITLE,Measure_Type,NUM_COUNT,DEN_COUNT,Excl_Count,Rexcl_Count,Gaps,Result,Target,To_Target,Report_Id,ReportType,Report_Quarter,Period_Start_Date,Period_End_Date,Root_Companies_Id)
Select 
	Measure_id
	,Measure_Name
	,Measure_Title
	,MEASURE_SUBTITLE
	,Measure_Type
	,SUM(Cast(NUM_COUNT as INT)) as NUM_COUNT
	,SUM(Cast(DEN_COUNT as INT)) as DEN_COUNT
	,SUM(Cast(Excl_Count as INT)) as Excl_Count
	,Sum(Cast(Rexcl_count as INT)) as Rexcl_Count
	,SUM(Cast(NUM_COUNT as INT)) as Gaps
	,Case 
		when SUM(Cast(DEN_excl as Float)) >0 then Round((SUM(Cast(NUM_COUNT as Float))/(SUM(Cast(DEN_excl as Float))))*100,2)
		else 0
	end as Result
	,@target as Target
	,Case
		when Round((SUM(Cast(NUM_COUNT as Float))/(SUM(Cast(DEN_excl as Float))))*100,2) > @target then @target-Round((SUM(Cast(NUM_COUNT as Float))/(SUM(Cast(DEN_excl as Float))))*100,2) 
		Else 0
	end as To_Target
	,Report_Id
	,ReportType
	,Report_Quarter
	,Period_Start_Date
	,Period_End_Date
	,Root_Companies_Id
From
(
	Select distinct
		EMPI
		,Measure_id
		,Measure_Name
		,@domain as Measure_Title
		,@subdomain as MEASURE_SUBTITLE
		,@measuretype as Measure_Type
		,Case
			when NUM=1 and Excl=0 and Rexcl=0 then 1
			else 0
		end as NUM_COUNT
		,DEN as DEN_COUNT
		,Case
			when DEN=1 and Excl=0 and Rexcl=0 then 1
			else 0
		end as DEN_excl
		,Excl as Excl_Count
		,Rexcl as Rexcl_count
		,Report_Id
		,ReportType
		,Report_Quarter
		,Period_Start_Date
		,Period_End_Date
		,Root_Companies_Id
	From #39_dataset
	where Enrollment_Status='Active' 
	--and	Specialty in('Cardiac Electrophysiology','Cardiology','Cardiovascular Disease','Family Medicine','Gastroenterology','General Practice','Internal Medicine','Interventional Cardiology','Nephrology','Pulmonary Diseases')
)t1
Group by Measure_id,Measure_Name,Measure_Title,MEASURE_SUBTITLE,Report_Id,ReportType,Report_Quarter,Period_Start_Date,Period_End_Date,Root_Companies_Id,Measure_Type
*/


	Insert into RPT.PROVIDER_SCORECARD(Measure_id,Measure_Name,Measure_Title,MEASURE_SUBTITLE,Measure_Type,NUM_COUNT,DEN_COUNT,Excl_Count,Rexcl_Count,Gaps,Result,Target,To_Target,Report_Id,ReportType,Report_Quarter,Period_Start_Date,Period_End_Date,Root_Companies_Id)

	Select
		Measure_id,
		Measure_Name,
		Measure_Title,
		MEASURE_SUBTITLE,
		Measure_Type,
		NUM_COUNT,
		DEN_COUNT,
		Excl_Count,
		Rexcl_Count,
		Gaps,
		Result,
		Target,
		Case
			when Result > Target then Floor((DEN_COUNT*Target)/100)-NUM_COUNT
			when Result <= Target then 0
		end as To_Target,
		Report_Id,
		ReportType,
		Report_Quarter,
		Period_Start_Date,
		Period_End_Date,
		Root_Companies_Id
	From
	(
		
			Select
				Measure_id,
				Measure_Name,
				Measure_Title,
				MEASURE_SUBTITLE,
				Measure_Type,
				SUM(Cast(NUM_COUNT as INT)) as NUM_COUNT,
				SUM(Cast(Den_excl as INT)) as DEN_COUNT,
				SUM(Cast(Excl_Count as INT)) as Excl_Count,
				Sum(Cast(Rexcl_count as INT)) as Rexcl_Count,
				SUM(Cast(NUM_COUNT as INT))  as Gaps,
				Case
					when SUM(Cast(DEN_Excl as Float))>0 Then Round((SUM(cast(NUM_COUNT as Float))/ISNULL(NULLIF(SUM(Cast(DEN_Excl as Float)),0),1))*100,2)
					else 0
				end as Result,
				Target,
				Report_Id,
				ReportType,
				Report_Quarter,
				Period_Start_Date,
				Period_End_Date,
				Root_Companies_Id
			From
			(
				Select distinct
					m.EMPI
					,a.NPI as Provider_id
					,a.NPI as PCP_NPI
					,a.Prov_Name as PCP_Name
					,a.Specialty
					,a.Practice as Practice_Name
					,m.Measure_id
					,m.Measure_Name
					,l.measure_Title
					,l.Measure_SubTitle
					,'Calculated' as Measure_Type
					,Case
						when NUM=1 and excl=0 and rexcl=0 Then 1
						else 0
					end as NUM_COUNT
					,DEN as DEN_COUNT
					,Case
						When DEN=1 and Excl=0 and Rexcl=0 Then 1
						else 0
					End as Den_excl
					,Excl as Excl_Count
					,Rexcl as Rexcl_count
					,Report_Id
					,l.ReportType
					,Report_Quarter
					,Period_Start_Date
					,Period_End_Date
					,m.Root_Companies_Id
					,l.Target
					,m.Code
					,m.DateofService
					,m.DischargeDate
				From RPT.MEASURE_DETAILED_LINE m
				Join RPT.ConsolidatedAttribution_Snapshot a on
					m.EMPI=a.EMPI and
					a.Attribution_Type='Ambulatory_PCP'
				Join RFT.UHN_measuresList l on
					m.Measure_Id=l.measure_id
				
				where 
					Enrollment_Status='Active' and
					a.NPI!='' and
					m.MEASURE_ID=@measure_id and
					REPORT_ID=@reportId 

			)tbl
			Group By
				Measure_id,Measure_Name,Measure_Title,MEASURE_SUBTITLE,Report_Id,ReportType,Report_Quarter,Period_Start_Date,Period_End_Date,Root_Companies_Id,Measure_Type,target
		
	)tbl2		
			
			

		SET @PERF_ROW = @@ROWCOUNT
		SET @PERF_DURATION = DATEDIFF(MINUTE,@PERF_START,GETDATE());
		EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, 'Enrollment Completed', @PERF_ROW, @DURATION_IN_MIN=@PERF_DURATION, @LOG2ID=@LOGID;
		EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, @LOG2ID=@LOGID, @END_FLAG=1;

END TRY
BEGIN CATCH
		BEGIN
			EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, 'FATAL ERROR', @LOG2ID=@LOGID;
			RAISERROR ('',16,0)
			RETURN 16
        END
	END CATCH
RETURN 0
END

GO
