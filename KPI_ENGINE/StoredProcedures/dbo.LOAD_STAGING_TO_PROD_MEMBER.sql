SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
CREATE PROCEDURE [dbo].[LOAD_STAGING_TO_PROD_MEMBER]

AS

BEGIN

	DECLARE @INFO VARCHAR(8000)
					,	@TRUE BIT = 1
					,	@FALSE BIT = 0
					,	@DB_ID INT = DB_ID()
					,	@LOGID INT = 0
					,	@PROC VARCHAR(500)=OBJECT_SCHEMA_NAME(@@PROCID)+'.'+OBJECT_NAME(@@PROCID)
					,	@PERF_START DATETIME
					,	@PERF_DURATION INT
					,	@PERF_ROW INT
					,	@RC	INT
					;

		EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, @INFO=@INFO, @LOG2ID=@LOGID OUT;
		DECLARE @PROCFULLNAME VARCHAR(500) = DB_NAME()+'.'+@PROC;
		----EXEC dbo.SP_MI_UTIL_SEND_MAIL @PROCFULLNAME, 'STARTED', 1, @ECHO=0; 
	DECLARE @MSG VARCHAR(1000)
	DECLARE @PERF_START_PROC DATETIME 
	DECLARE @PERF_RPS NUMERIC(10,2) 
	DECLARE @ROWS INT 
	DECLARE @PERF_ROWS INT 
	SET @PERF_ROWS = 0 
	SET @PERF_START = GETDATE() 
	SET @PERF_START_PROC = GETDATE() 


BEGIN TRY

Declare @Error_Keys varchar(100);
SET @Error_Keys=NULL;

SET @Error_Keys=(Select Distinct TOP 1 MEM_DATA_SRC FROM [KPI_ENGINE_STAGING].[dbo].[STAGING_MEMBER] M
LEFT JOIN (Select Distinct PAYER  FROM [dbo].[STAGING_TO_PROD_DEDUP_KEY] WHERE PROD_TABLE='MEMBER') DEDUPKEY
ON MEM_DATA_SRC=DEDUPKEY.PAYER 
where PAYER is NULL)

IF @Error_Keys IS NOT NULL
RAISERROR ('CREATE THE DEDUP KEYS FOR MEMBER: Select Distinct TOP 1 MEM_DATA_SRC FROM [KPI_ENGINE_STAGING].[dbo].[STAGING_MEMBER] M
LEFT JOIN (Select Distinct PAYER  FROM [dbo].[STAGING_TO_PROD_DEDUP_KEY] WHERE PROD_TABLE=''MEMBER'') DEDUPKEY
ON MEM_DATA_SRC=DEDUPKEY.PAYER 
where PAYER is NULL',16,1);


DEclare @PROD_TABLE varchar(100)
Declare @PAYER varchar(100)
Declare @ROOT_COMPANIES_ID int
Declare @KEYS varchar(500)
DECLARE @SQL VARCHAR(MAX)
DECLARE C CURSOR FOR 
select
    PROD_TABLE
	,PAYER
	,ROOT_COMPANIES_ID
    ,stuff((
        select ' AND ' + CASE 
	WHEN KEY_COLUMNS_TYPE='VARCHAR' THEN 'ISNULL(M.'+KEY_COLUMNS+','''')' 
	WHEN KEY_COLUMNS_TYPE='INT' THEN 'ISNULL(M.'+KEY_COLUMNS+',''0'')' 
	WHEN KEY_COLUMNS_TYPE='DATE' THEN 'ISNULL(M.'+KEY_COLUMNS+',''1900-01-01'')' 
END+'='+CASE 
	WHEN KEY_COLUMNS_TYPE='VARCHAR' THEN 'ISNULL(PM.'+KEY_COLUMNS+','''')' 
	WHEN KEY_COLUMNS_TYPE='INT' THEN 'ISNULL(PM.'+KEY_COLUMNS+',''0'')' 
	WHEN KEY_COLUMNS_TYPE='DATE' THEN 'ISNULL(PM.'+KEY_COLUMNS+',''1900-01-01'')' 
END
        from [dbo].[STAGING_TO_PROD_DEDUP_KEY] t
        where t.PROD_TABLE = t1.PROD_TABLE AND t.PAYER = t1.PAYER AND t.ROOT_COMPANIES_ID = t1.ROOT_COMPANIES_ID
        order by t.KEY_COLUMNS
        for xml path('')
    ),1,4,'') as name_csv
from [dbo].[STAGING_TO_PROD_DEDUP_KEY] t1
where t1.PROD_TABLE='MEMBER'
group by 
PROD_TABLE
,PAYER
,ROOT_COMPANIES_ID

OPEN C  
FETCH NEXT FROM C INTO @PROD_TABLE,@PAYER,@ROOT_COMPANIES_ID,@KEYS

WHILE @@FETCH_STATUS = 0  
BEGIN  

SET @SQL='
UPDATE [KPI_ENGINE].[dbo].[MEMBER]
SET
  		 [MEM_START_DATE]=LTRIM(RTRIM(M.[MEM_START_DATE]))
		,[MEM_END_DATE]=LTRIM(RTRIM(M.[MEM_END_DATE]))
		,[MEMBER_QUAL]=LTRIM(RTRIM(M.[MEMBER_QUAL]))
		,[MEM_GENDER]=LTRIM(RTRIM(M.[MEM_GENDER]))
		,[MEM_SSN]=LTRIM(RTRIM(M.[MEM_SSN]))
		,[MEM_LNAME]=LTRIM(RTRIM(M.[MEM_LNAME]))
		,[MEM_FNAME]=LTRIM(RTRIM(M.[MEM_FNAME]))
		,[MEM_MNAME]=LTRIM(RTRIM(M.[MEM_MNAME]))
		,[MEM_DOB]=LTRIM(RTRIM(M.[MEM_DOB]))
		,[MEM_DOD]=LTRIM(RTRIM(M.[MEM_DOD]))
		,[MEM_MEDICARE]=LTRIM(RTRIM(M.[MEM_MEDICARE]))
		,[MEM_ADDR1]=LTRIM(RTRIM(M.[MEM_ADDR1]))
		,[MEM_ADDR2]=LTRIM(RTRIM(M.[MEM_ADDR2]))
		,[MEM_CITY]=LTRIM(RTRIM(M.[MEM_CITY]))
		,[MEM_COUNTY]=LTRIM(RTRIM(M.[MEM_COUNTY]))
		,[MEM_STATE]=LTRIM(RTRIM(M.[MEM_STATE]))
		,[MEM_ZIP]=LTRIM(RTRIM(M.[MEM_ZIP]))
		,[MEM_EMAIL]=LTRIM(RTRIM(M.[MEM_EMAIL]))
		,[MEM_PHONE]=LTRIM(RTRIM(M.[MEM_PHONE]))
		,[MEM_RACE]=LTRIM(RTRIM(M.[MEM_RACE]))
		,[MEM_ETHNICITY]=LTRIM(RTRIM(M.[MEM_ETHNICITY]))
		,[MEM_LANGUAGE]=LTRIM(RTRIM(M.[MEM_LANGUAGE]))
		,[_MEM_UDF_01_]=LTRIM(RTRIM(M.[_MEM_UDF_01_]))
		,[_MEM_UDF_02_]=LTRIM(RTRIM(M.[_MEM_UDF_02_]))
		,[_MEM_UDF_03_]=LTRIM(RTRIM(M.[_MEM_UDF_03_]))
		,[_MEM_UDF_04_]=LTRIM(RTRIM(M.[_MEM_UDF_04_]))
		,[_MEM_UDF_05_]=LTRIM(RTRIM(M.[_MEM_UDF_05_]))
		,[_MEM_UDF_06_]=LTRIM(RTRIM(M.[_MEM_UDF_06_]))
		,[_MEM_UDF_07_]=LTRIM(RTRIM(M.[_MEM_UDF_07_]))
		,[_MEM_UDF_08_]=LTRIM(RTRIM(M.[_MEM_UDF_08_]))
		,[_MEM_UDF_09_]=LTRIM(RTRIM(M.[_MEM_UDF_09_]))
		,[_MEM_UDF_10_]=LTRIM(RTRIM(M.[_MEM_UDF_10_]))
		,[_MEM_UDF_11_]=LTRIM(RTRIM(M.[_MEM_UDF_11_]))
		,[_MEM_UDF_12_]=LTRIM(RTRIM(M.[_MEM_UDF_12_]))
		,[_MEM_UDF_13_]=LTRIM(RTRIM(M.[_MEM_UDF_13_]))
		,[_MEM_UDF_14_]=LTRIM(RTRIM(M.[_MEM_UDF_14_]))
		,[_MEM_UDF_15_]=LTRIM(RTRIM(M.[_MEM_UDF_15_]))
		,[_MEM_UDF_16_]=LTRIM(RTRIM(M.[_MEM_UDF_16_]))
		,[_MEM_UDF_17_]=LTRIM(RTRIM(M.[_MEM_UDF_17_]))
		,[_MEM_UDF_18_]=LTRIM(RTRIM(M.[_MEM_UDF_18_]))
		,[_MEM_UDF_19_]=LTRIM(RTRIM(M.[_MEM_UDF_19_]))
		,[_MEM_UDF_20_]=LTRIM(RTRIM(M.[_MEM_UDF_20_]))
		,[ST_MEM_ID]=LTRIM(RTRIM(M.[ST_MEM_ID]))
		,[Filename]=LTRIM(RTRIM(M.[Filename]))
		,[Filedate]=LTRIM(RTRIM(M.[Filedate]))
		,[LoadDateTime]=LTRIM(RTRIM(M.[LoadDateTime]))
FROM KPI_ENGINE_STAGING.dbo.STAGING_MEMBER M
INNER JOIN KPI_ENGINE.dbo.Open_empi EMPI 
on M.MEMBER_ID=EMPI.Org_Patient_Extension_ID AND M.MEM_DATA_SRC=EMPI.[Data_Source] AND M.ROOT_COMPANIES_ID=EMPI.Root_Companies_ID
LEFT JOIN KPI_ENGINE.dbo.MEMBER PM
ON '+@KEYS+
' WHERE PM.MEMBER_ID IS NOT NULL AND M.MEM_DATA_SRC='''+@PAYER+''' AND M.Root_Companies_ID='+CONVERT(VARCHAR(10),@ROOT_COMPANIES_ID)

Declare @updatecount INT;
EXECUTE (@SQL);
SET @updatecount=@@ROWCOUNT;


SET @SQL='
INSERT INTO [KPI_ENGINE].[dbo].[MEMBER]
(
	   [MEM_START_DATE]
      ,[MEM_END_DATE]
      ,[MEMBER_ID]
      ,[MEMBER_QUAL]
      ,[EMPI]
      ,[MEM_GENDER]
      ,[MEM_SSN]
      ,[MEM_LNAME]
      ,[MEM_FNAME]
      ,[MEM_MNAME]
      ,[MEM_DOB]
      ,[MEM_DOD]
      ,[MEM_MEDICARE]
      ,[MEM_ADDR1]
      ,[MEM_ADDR2]
      ,[MEM_CITY]
      ,[MEM_COUNTY]
      ,[MEM_STATE]
      ,[MEM_ZIP]
      ,[MEM_EMAIL]
      ,[MEM_PHONE]
      ,[MEM_RACE]
      ,[MEM_ETHNICITY]
      ,[MEM_LANGUAGE]
      ,[MEM_DATA_SRC]
      ,[_MEM_UDF_01_]
      ,[_MEM_UDF_02_]
      ,[_MEM_UDF_03_]
      ,[_MEM_UDF_04_]
      ,[_MEM_UDF_05_]
      ,[_MEM_UDF_06_]
      ,[_MEM_UDF_07_]
      ,[_MEM_UDF_08_]
      ,[_MEM_UDF_09_]
      ,[_MEM_UDF_10_]
      ,[_MEM_UDF_11_]
      ,[_MEM_UDF_12_]
      ,[_MEM_UDF_13_]
      ,[_MEM_UDF_14_]
      ,[_MEM_UDF_15_]
      ,[_MEM_UDF_16_]
      ,[_MEM_UDF_17_]
      ,[_MEM_UDF_18_]
      ,[_MEM_UDF_19_]
      ,[_MEM_UDF_20_]
      ,[ST_MEM_ID]
      ,[ROOT_COMPANIES_ID]
      ,[Filename]
      ,[Filedate]
      ,[LoadDateTime]
)
Select 
	   LTRIM(RTRIM(M.[MEM_START_DATE]))
      ,LTRIM(RTRIM(M.[MEM_END_DATE]))
      ,LTRIM(RTRIM(M.[MEMBER_ID]))
      ,LTRIM(RTRIM(M.[MEMBER_QUAL]))
      ,LTRIM(RTRIM(EMPI.[EMPI_ID]))
      ,LTRIM(RTRIM(M.[MEM_GENDER]))
      ,LTRIM(RTRIM(M.[MEM_SSN]))
      ,LTRIM(RTRIM(M.[MEM_LNAME]))
      ,LTRIM(RTRIM(M.[MEM_FNAME]))
      ,LTRIM(RTRIM(M.[MEM_MNAME]))
      ,LTRIM(RTRIM(M.[MEM_DOB]))
      ,LTRIM(RTRIM(M.[MEM_DOD]))
      ,LTRIM(RTRIM(M.[MEM_MEDICARE]))
      ,LTRIM(RTRIM(M.[MEM_ADDR1]))
      ,LTRIM(RTRIM(M.[MEM_ADDR2]))
      ,LTRIM(RTRIM(M.[MEM_CITY]))
      ,LTRIM(RTRIM(M.[MEM_COUNTY]))
      ,LTRIM(RTRIM(M.[MEM_STATE]))
      ,LTRIM(RTRIM(M.[MEM_ZIP]))
      ,LTRIM(RTRIM(M.[MEM_EMAIL]))
      ,LTRIM(RTRIM(M.[MEM_PHONE]))
      ,LTRIM(RTRIM(M.[MEM_RACE]))
      ,LTRIM(RTRIM(M.[MEM_ETHNICITY]))
      ,LTRIM(RTRIM(M.[MEM_LANGUAGE]))
      ,LTRIM(RTRIM(M.[MEM_DATA_SRC]))
      ,LTRIM(RTRIM(M.[_MEM_UDF_01_]))
      ,LTRIM(RTRIM(M.[_MEM_UDF_02_]))
      ,LTRIM(RTRIM(M.[_MEM_UDF_03_]))
      ,LTRIM(RTRIM(M.[_MEM_UDF_04_]))
      ,LTRIM(RTRIM(M.[_MEM_UDF_05_]))
      ,LTRIM(RTRIM(M.[_MEM_UDF_06_]))
      ,LTRIM(RTRIM(M.[_MEM_UDF_07_]))
      ,LTRIM(RTRIM(M.[_MEM_UDF_08_]))
      ,LTRIM(RTRIM(M.[_MEM_UDF_09_]))
      ,LTRIM(RTRIM(M.[_MEM_UDF_10_]))
      ,LTRIM(RTRIM(M.[_MEM_UDF_11_]))
      ,LTRIM(RTRIM(M.[_MEM_UDF_12_]))
      ,LTRIM(RTRIM(M.[_MEM_UDF_13_]))
      ,LTRIM(RTRIM(M.[_MEM_UDF_14_]))
      ,LTRIM(RTRIM(M.[_MEM_UDF_15_]))
      ,LTRIM(RTRIM(M.[_MEM_UDF_16_]))
      ,LTRIM(RTRIM(M.[_MEM_UDF_17_]))
      ,LTRIM(RTRIM(M.[_MEM_UDF_18_]))
      ,LTRIM(RTRIM(M.[_MEM_UDF_19_]))
      ,LTRIM(RTRIM(M.[_MEM_UDF_20_]))
      ,LTRIM(RTRIM(M.[ST_MEM_ID]))
      ,LTRIM(RTRIM(M.[ROOT_COMPANIES_ID]))
      ,LTRIM(RTRIM(M.[Filename]))
      ,LTRIM(RTRIM(M.[Filedate]))
      ,LTRIM(RTRIM(M.[LoadDateTime]))
FROM KPI_ENGINE_STAGING.dbo.STAGING_MEMBER M
LEFT JOIN KPI_ENGINE.dbo.Open_empi EMPI 
on M.MEMBER_ID=EMPI.Org_Patient_Extension_ID AND M.MEM_DATA_SRC=EMPI.[Data_Source] AND M.ROOT_COMPANIES_ID=EMPI.Root_Companies_ID
LEFT JOIN KPI_ENGINE.dbo.MEMBER PM
ON '+@KEYS+
' WHERE PM.MEMBER_ID IS NULL AND EMPI.EMPI_ID IS NOT NULL AND M.MEM_DATA_SRC='''+@PAYER+''' AND M.Root_Companies_ID='+CONVERT(VARCHAR(10),@ROOT_COMPANIES_ID)

Declare @insertcount INT;
EXECUTE (@SQL);
SET @insertcount=@@ROWCOUNT;


SET @SQL='
INSERT INTO KPI_ENGINE_STAGING.dbo.STAGING_MEMBER_ERROR
(
	   [MEM_START_DATE]
      ,[MEM_END_DATE]
      ,[MEMBER_ID]
      ,[MEMBER_QUAL]
      ,[MEM_GENDER]
      ,[MEM_SSN]
      ,[MEM_LNAME]
      ,[MEM_FNAME]
      ,[MEM_MNAME]
      ,[MEM_DOB]
      ,[MEM_DOD]
      ,[MEM_MEDICARE]
      ,[MEM_ADDR1]
      ,[MEM_ADDR2]
      ,[MEM_CITY]
      ,[MEM_COUNTY]
      ,[MEM_STATE]
      ,[MEM_ZIP]
      ,[MEM_EMAIL]
      ,[MEM_PHONE]
      ,[MEM_RACE]
      ,[MEM_ETHNICITY]
      ,[MEM_LANGUAGE]
      ,[MEM_DATA_SRC]
      ,[_MEM_UDF_01_]
      ,[_MEM_UDF_02_]
      ,[_MEM_UDF_03_]
      ,[_MEM_UDF_04_]
      ,[_MEM_UDF_05_]
      ,[_MEM_UDF_06_]
      ,[_MEM_UDF_07_]
      ,[_MEM_UDF_08_]
      ,[_MEM_UDF_09_]
      ,[_MEM_UDF_10_]
      ,[_MEM_UDF_11_]
      ,[_MEM_UDF_12_]
      ,[_MEM_UDF_13_]
      ,[_MEM_UDF_14_]
      ,[_MEM_UDF_15_]
      ,[_MEM_UDF_16_]
      ,[_MEM_UDF_17_]
      ,[_MEM_UDF_18_]
      ,[_MEM_UDF_19_]
      ,[_MEM_UDF_20_]
      ,[ST_MEM_ID]
      ,[ROOT_COMPANIES_ID]
      ,[Filename]
      ,[Filedate]
      ,[LoadDateTime]
)
Select 
	   LTRIM(RTRIM(M.[MEM_START_DATE]))
      ,LTRIM(RTRIM(M.[MEM_END_DATE]))
      ,LTRIM(RTRIM(M.[MEMBER_ID]))
      ,LTRIM(RTRIM(M.[MEMBER_QUAL]))
      ,LTRIM(RTRIM(M.[MEM_GENDER]))
      ,LTRIM(RTRIM(M.[MEM_SSN]))
      ,LTRIM(RTRIM(M.[MEM_LNAME]))
      ,LTRIM(RTRIM(M.[MEM_FNAME]))
      ,LTRIM(RTRIM(M.[MEM_MNAME]))
      ,LTRIM(RTRIM(M.[MEM_DOB]))
      ,LTRIM(RTRIM(M.[MEM_DOD]))
      ,LTRIM(RTRIM(M.[MEM_MEDICARE]))
      ,LTRIM(RTRIM(M.[MEM_ADDR1]))
      ,LTRIM(RTRIM(M.[MEM_ADDR2]))
      ,LTRIM(RTRIM(M.[MEM_CITY]))
      ,LTRIM(RTRIM(M.[MEM_COUNTY]))
      ,LTRIM(RTRIM(M.[MEM_STATE]))
      ,LTRIM(RTRIM(M.[MEM_ZIP]))
      ,LTRIM(RTRIM(M.[MEM_EMAIL]))
      ,LTRIM(RTRIM(M.[MEM_PHONE]))
      ,LTRIM(RTRIM(M.[MEM_RACE]))
      ,LTRIM(RTRIM(M.[MEM_ETHNICITY]))
      ,LTRIM(RTRIM(M.[MEM_LANGUAGE]))
      ,LTRIM(RTRIM(M.[MEM_DATA_SRC]))
      ,LTRIM(RTRIM(M.[_MEM_UDF_01_]))
      ,LTRIM(RTRIM(M.[_MEM_UDF_02_]))
      ,LTRIM(RTRIM(M.[_MEM_UDF_03_]))
      ,LTRIM(RTRIM(M.[_MEM_UDF_04_]))
      ,LTRIM(RTRIM(M.[_MEM_UDF_05_]))
      ,LTRIM(RTRIM(M.[_MEM_UDF_06_]))
      ,LTRIM(RTRIM(M.[_MEM_UDF_07_]))
      ,LTRIM(RTRIM(M.[_MEM_UDF_08_]))
      ,LTRIM(RTRIM(M.[_MEM_UDF_09_]))
      ,LTRIM(RTRIM(M.[_MEM_UDF_10_]))
      ,LTRIM(RTRIM(M.[_MEM_UDF_11_]))
      ,LTRIM(RTRIM(M.[_MEM_UDF_12_]))
      ,LTRIM(RTRIM(M.[_MEM_UDF_13_]))
      ,LTRIM(RTRIM(M.[_MEM_UDF_14_]))
      ,LTRIM(RTRIM(M.[_MEM_UDF_15_]))
      ,LTRIM(RTRIM(M.[_MEM_UDF_16_]))
      ,LTRIM(RTRIM(M.[_MEM_UDF_17_]))
      ,LTRIM(RTRIM(M.[_MEM_UDF_18_]))
      ,LTRIM(RTRIM(M.[_MEM_UDF_19_]))
      ,LTRIM(RTRIM(M.[_MEM_UDF_20_]))
      ,LTRIM(RTRIM(M.[ST_MEM_ID]))
      ,LTRIM(RTRIM(M.[ROOT_COMPANIES_ID]))
      ,LTRIM(RTRIM(M.[Filename]))
      ,LTRIM(RTRIM(M.[Filedate]))
      ,LTRIM(RTRIM(M.[LoadDateTime]))
FROM KPI_ENGINE_STAGING.dbo.STAGING_MEMBER M
LEFT JOIN KPI_ENGINE.dbo.Open_empi EMPI 
on M.MEMBER_ID=EMPI.Org_Patient_Extension_ID AND M.MEM_DATA_SRC=EMPI.[Data_Source] AND M.ROOT_COMPANIES_ID=EMPI.Root_Companies_ID
LEFT JOIN KPI_ENGINE_STAGING.dbo.STAGING_MEMBER_ERROR PM
ON '+@KEYS+
' WHERE PM.MEMBER_ID IS NULL AND EMPI.EMPI_ID IS NULL AND M.MEM_DATA_SRC='''+@PAYER+''' AND M.Root_Companies_ID='+CONVERT(VARCHAR(10),@ROOT_COMPANIES_ID)

Declare @errorcount INT;
EXECUTE (@SQL);
Set @errorcount=@@ROWCOUNT

-- Shanawaz 10-21-2021 -- Adding this  for data flow trace
Insert into KPI_ENGINE_RECON.dbo.PROD_INGESTION_STATS(Dimension,Source,UpdateCount,InsertCount,ErrorCount) values(@PROD_TABLE,@PAYER,@updatecount,@insertcount,@errorcount);



FETCH NEXT FROM C INTO @PROD_TABLE,@PAYER,@ROOT_COMPANIES_ID,@KEYS
END 

CLOSE C  
DEALLOCATE C

			SET @PERF_ROW = @@ROWCOUNT
		SET @PERF_DURATION = DATEDIFF(MINUTE,@PERF_START,GETDATE());
		EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, 'Enrollment Completed', @PERF_ROW, @DURATION_IN_MIN=@PERF_DURATION, @LOG2ID=@LOGID;
		EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, @LOG2ID=@LOGID, @END_FLAG=1;


END TRY
BEGIN CATCH  
    DECLARE @ErrorMessage NVARCHAR(4000);  
    DECLARE @ErrorSeverity INT;  
    DECLARE @ErrorState INT;   

	EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, 'FATAL ERROR', @LOG2ID=@LOGID;
  
    SELECT   
        @ErrorMessage = ERROR_MESSAGE(),  
        @ErrorSeverity = ERROR_SEVERITY(),  
        @ErrorState = ERROR_STATE();  
  
    -- Use RAISERROR inside the CATCH block to return error  
    -- information about the original error that caused  
    -- execution to jump to the CATCH block.  
    RAISERROR (@ErrorMessage, -- Message text.  
               @ErrorSeverity, -- Severity.  
               @ErrorState -- State.  
               );  
END CATCH;  

END
GO
