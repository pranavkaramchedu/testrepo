SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON







CREATE PROCEDURE [HDS].[RUN_HEDIS_EDU]  @meas_year nvarchar(4)
AS
-- Declare Variables




DECLARE @pt_ctr INT=0;
Declare @runid INT=0;
DECLARE @i INT= 0;
DECLARE @mem_ptr VARCHAR(10);

DECLARE @gender VARCHAR(20);
DECLARE @age VARCHAR(10);
DECLARE @latest_insdate DATETIME;
DECLARE @latest_insenddate DATETIME;
DECLARE @ce_startdt varchar(8);
DECLARE @ce_startdt1 varchar(8);
DECLARE @ce_enddt varchar(8);
DECLARE @ce_enddt1 varchar(8);
declare @patientid varchar(20);
DECLARE @plan_ct INT=0;
DECLARE @planid VARCHAR(10);
DECLARE @plan1 VARCHAR(10);
DECLARE @plan2 VARCHAR(10);
DECLARE @meas VARCHAR(10);
DECLARE @j INT =0;

 

SET @meas='EDU';
SET @ce_startdt=concat(@meas_year,'0101');
SET @ce_enddt=concat(@meas_year,'1231');
SET @ce_startdt1=concat(@meas_year-1,'0101');
SET @ce_enddt1=concat(@meas_year-1,'1231');





-- Create temp table to store EDU data
drop table if exists #edudataset;

CREATE TABLE #edudataset (
  [Memid] varchar(100) NOT NULL,
  [meas] varchar(20) DEFAULT NULL,
  [payer] varchar(100) DEFAULT NULL,
  [gender] varchar(45) NOT NULL,
  [age] decimal(12,4) DEFAULT NULL,
  [encounters] smallint DEFAULT '0',
  [Epop] smallint DEFAULT '1',
  [Outlier] smallint DEFAULT '0',
  [PPVComorbidWt] float DEFAULT '0',
  [PPVAgeGenWt] float DEFAULT '0',
  [PUCVComorbidWt] float DEFAULT '1',
  [PUCVAgeGenWt] float DEFAULT '0',
    
) ;


-- Continuous enrollment

	
DROP TABLE IF EXISTS #edu_contenroll;
	
CREATE table #edu_contenroll
(
	Memid varchar(50)
		
);


With coverage_CTE1 (Memid,lastcoveragedate,Startdate,Finishdate,nextcoveragedate) as
(

select Memid,isnull(lag(Finishdate,1) over(partition by Memid order by Startdate,FinishDate desc),convert(varchar,cast(@ce_startdt as date),112)) as lastcoveragedate,Case when StartDate<@ce_startdt then convert(varchar,cast(@ce_startdt as date),112) else startdate end as Startdate,case when Finishdate>@ce_enddt then convert(varchar,cast(@ce_enddt as date),112) else finishdate end as Finishdate,isnull(lead(Startdate,1) over(partition by Memid order by Startdate,FinishDate),convert(varchar,cast(@ce_enddt as date),112)) as nextcoveragedate  from hedis_member_en where Measure_id=@meas and Startdate<=@ce_enddt and Finishdate>=@ce_startdt
),
coverage_CTE2 (Memid,lastcoveragedate,Startdate,Finishdate,nextcoveragedate) as
(
select Memid,isnull(lag(Finishdate,1) over(partition by Memid order by Startdate,FinishDate desc),convert(varchar,cast(@ce_startdt1 as date),112)) as lastcoveragedate,Case when StartDate<@ce_startdt1 then convert(varchar,cast(@ce_startdt1 as date),112) else startdate end as Startdate,case when Finishdate>@ce_enddt1 then convert(varchar,cast(@ce_enddt1 as date),112) else finishdate end as Finishdate,isnull(lead(Startdate,1) over(partition by Memid order by Startdate,FinishDate),convert(varchar,cast(@ce_enddt1 as date),112)) as nextcoveragedate  from hedis_member_en where Measure_id=@meas and Startdate<=@ce_enddt1 and Finishdate>=@ce_startdt1
)
Insert into #edu_contenroll

select t3.Memid from(
select t2.Memid from(
select *, case when rn=1 and startdate>@ce_startdt then 1 else 0 end as startgap,case when datediff(day,newFinishdate,nextcoveragedate)>0 then 1  else 0 end as gaps,datediff(day,Startdate,Finishdate)+1 as coveragedays,case when @ce_enddt between Startdate and newfinishdate then 1 else 0 end as anchor from(
Select *,case when datediff(day,Finishdate,nextcoveragedate) <=1 then isnull(lead(Finishdate,1) over(partition by Memid order by Startdate,FinishDate),convert(varchar,cast(@ce_enddt as date),112)) else finishdate end as newfinishdate,ROW_NUMBER() over(partition by memid order by Startdate,Finishdate) as rn from coverage_CTE1
)t1           
)t2  
group by memid having(sum(gaps)+sum(startgap)<=1) and sum(coveragedays)>=(DATEPART(dy, @ce_enddt)-45) and sum(anchor)>0
)t3
Join(

select distinct memid from
(

	select z2.Memid from
	(

		select *, case when rn=1 and startdate>@ce_startdt1 then 1 else 0 end as startgap,case when datediff(day,newFinishdate,nextcoveragedate)>0 then 1  else 0 end as gaps,datediff(day,Startdate,Finishdate)+1 as coveragedays,case when @ce_enddt1 between Startdate and newfinishdate then 1 else 0 end as anchor from
		(
			Select *,case when datediff(day,Finishdate,nextcoveragedate) <=1 then isnull(lead(Finishdate,1) over(partition by Memid order by Startdate,FinishDate),convert(varchar,cast(@ce_enddt1 as date),112)) else finishdate end as newfinishdate,ROW_NUMBER() over(partition by memid order by Startdate,Finishdate) as rn from coverage_CTE2
		)z1           
	)z2 
group by memid having(sum(gaps)+sum(startgap)<=1) and sum(coveragedays)>=(DATEPART(dy, @ce_enddt1)-45)
)z3 
)z4 on z4.memid=t3.memid order by t3.memid

CREATE CLUSTERED INDEX ix_edu_contenroll ON #edu_contenroll ([memid]);

-- hospice Exclusion

drop table if exists #edu_hospicemembers;
CREATE table #edu_hospicemembers
(
	Memid varchar(50)
		
);

	

Insert into #edu_hospicemembers
select distinct t1.MemID from
(
	Select Memid from HEDIS_VISIT_E where Measure_id=@meas and sdate!='' and sdate between @ce_startdt and @ce_enddt and activity IN(select code from VALUESET_TO_CODE where Value_Set_Name in('Hospice Encounter','Hospice Intervention'))

	Union all

	select Beneficiary_id as Memid from Hedis_MMDF where Measure_id=@meas and Run_date!='' and Run_date between @ce_startdt and @ce_enddt and Hospice='Y'

	Union all

	select MemID from HEDIS_VISIT v
	where Measure_id=@meas and HCFAPOS!=81 and Date_S!='' and Date_S between @ce_startdt and @ce_enddt
	and (
	CPT in(select code from VALUESET_TO_CODE where Value_Set_Name ='Hospice Intervention')
	or
	HCPCS in(select code from VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or
	Rev in(select code from VALUESET_TO_CODE where code_system='UBREV' and Value_Set_Name ='Hospice Encounter')
	)
	)t1
	
	CREATE CLUSTERED INDEX idx_hospicemembers ON #edu_hospicemembers ([memid]);
	



-- Creating Denominator Set

drop table if exists #edu_memlist; 

CREATE TABLE #edu_memlist (
    memid varchar(100)
    
);


Insert into #edu_memlist
select distinct en.memid from HEDIS_MEMBER_EN en
join HEDIS_MEMBER_GM gm on en.memid=gm.memid and en.measure_id=gm.measure_id
join #edu_contenroll ce on en.memid=ce.Memid
left outer join #edu_hospicemembers hsx on en.memid=hsx.memid
where en.measure_id=@meas and Year(@ce_enddt) - Year(gm.dob)>=18 and hsx.memid is null 
and @ce_enddt between startdate and finishdate and payer in('MCR','MCS','MP','CEP','HMO','PPO','POS','SN1','SN2','SN3','MMP')
order by 1


CREATE CLUSTERED INDEX idx_edu_memlist ON #edu_memlist ([memid]);
	
	

-- Create Temp Patient Enrollment
drop table if exists #edu_tmpsubscriber;

CREATE TABLE #edu_tmpsubscriber (
    memid varchar(100),
    dob varchar(8),
	gender varchar(1),
	payer varchar(50),
	StartDate varchar(8),
	EndDate varchar(8),
);



insert into #edu_tmpsubscriber
select en.MemID,gm.DOB,gm.Gender,en.Payer,en.StartDate,en.FinishDate from HEDIS_MEMBER_EN en
join HEDIS_MEMBER_GM gm on en.memid=gm.memid and en.measure_id=gm.measure_id
join #edu_contenroll ce on en.memid=ce.Memid
left outer join #edu_hospicemembers hsx on en.memid=hsx.memid
where en.measure_id=@meas and Year(@ce_enddt) - Year(gm.dob)>=18 and hsx.memid is null 
and @ce_enddt between startdate and finishdate and payer in('MCR','MCS','MP','CEP','HMO','PPO','POS','SN1','SN2','SN3','MMP')
ORDER BY en.MemID,en.StartDate,en.FinishDate;


CREATE CLUSTERED INDEX ix_tmpsub_memid ON #edu_tmpsubscriber ([memid],[StartDate],[EndDate]);


-- Get Patient COUNT
SELECT @pt_ctr=COUNT(*)  FROM #edu_memlist;


While @i<@pt_ctr
	BEGIN
		-- Get Patient to Loop
		SELECT  @patientid=memid FROM #edu_memlist ORDER BY memid ASC OFFSET  @i ROWS FETCH NEXT 1 ROWS ONLY ;
		
		-- ReadingGender and age
		SELECT Top 1 @gender=gender,@age=YEAR(@ce_enddt)-YEAR(DOB) FROM #edu_tmpsubscriber WHERE memid=@patientid;
		
		-- Check for dual eligibility by checking if the patient is enrolled in more than one plan at the end of Enrollment
		/*
		SELECT Top 1 @latest_insenddate=EndDate FROM #spd_tmpsubscriber WHERE  memid=@patientid  AND StartDate<=@ce_enddt ORDER BY StartDate DESC,EndDate Desc  ;
		*/
		-- Check for plan count patient is enrolled in during the end of measurement year
		
		SELECT @plan_ct=COUNT(*) FROM #edu_tmpsubscriber WHERE  memid=@patientid AND @ce_enddt BETWEEN StartDate AND EndDate;
		
		-- If only one plan, then skip the Payer reporting logic
		
		if(@plan_ct=1)
		BEGIN
			
			SELECT @planid=payer FROM #edu_tmpsubscriber WHERE  memid=@patientid AND @ce_enddt BETWEEN StartDate AND EndDate ORDER BY payer;
		
			-- If plan is Dual , Insert MCR & MCD	
			
			If(@planid in('MMP','SN3','MDE'))
			BEGIN
				
				SET @planid='MCR';
				Insert INTO #edudataset(Memid,meas,payer,age,gender) VALUES(@patientid,@meas,@planid,@age,@gender);
								
			END
			Else if(@planid IN('MCR','MCS','MP'))
			Begin
				Insert INTO #edudataset(Memid,meas,payer,age,gender) VALUES(@patientid,@meas,@planid,@age,@gender);
				
			END
			
			Else If(@planid IN('SN1','SN2'))
			Begin
				
				SET @planid='MCR';
				Insert INTO #edudataset(Memid,meas,payer,age,gender) VALUES(@patientid,@meas,@planid,@age,@gender);
				
			END
			ELSE if(@planid IN('CEP','HMO','PPO','POS'))
			BEGIN
				Insert INTO #edudataset(Memid,meas,payer,age,gender) VALUES(@patientid,@meas,@planid,@age,@gender);
			END
		END
				
		-- Compare Plans if Plan count is more than one	
		if(@plan_ct>1)
		BEGIN
			
			-- Read First Plan
			SELECT  @plan1=payer FROM #edu_tmpsubscriber where memid=@patientid and @ce_enddt between StartDate and EndDate order by 1 OFFSET  0 ROWS FETCH NEXT 1 ROWS ONLY ;
					
			-- Read second plan
			SELECT  @plan2=payer FROM #edu_tmpsubscriber where memid=@patientid and @ce_enddt between StartDate and EndDate order by 1 OFFSET  1 ROWS FETCH NEXT 1 ROWS ONLY ;
			
			
			
			If(@plan1 IN('MCR','MP','SN1','SN2','MCS','MMP','SN3') and @plan2 IN('PPO','POS','HMO','CEP'))
			BEGIN
				if(@plan1 IN('SN1','SN2','SN3','MMP'))
				BEGIN
					
					SET @planid='MCR';
					Insert INTO #edudataset(Memid,meas,payer,age,gender) VALUES(@patientid,@meas,@planid,@age,@gender);
													
				END
				
				if(@plan1 in ('MCR','MP','MCS'))
				Begin
					
					Set @planid=@plan1;
					Insert INTO #edudataset(Memid,meas,payer,age,gender) VALUES(@patientid,@meas,@planid,@age,@gender);
				END

			END
			
			Else If(@plan2 IN('MCR','MP','SN1','SN2','MCS','SN3','MMP') AND @plan1 IN('PPO','POS','HMO','CEP'))
			BEGIN
			
				IF(@plan2 IN('SN1','SN2','SN3','MMP'))
				Begin
					
					Set @planid='MCR';
					Insert INTO #edudataset(Memid,meas,payer,age,gender) VALUES(@patientid,@meas,@planid,@age,@gender);
								
				END
				
				
				IF(@plan2 IN('MCR','MP','MCS'))
				BEGIN
					
					Set @planid=@plan2;
					
					Insert INTO #edudataset(Memid,meas,payer,age,gender) VALUES(@patientid,@meas,@planid,@age,@gender);
					
				END
			END
			
			ELSE IF(@plan1 IN('PPO','POS','HMO','CEP') AND @plan2 IN('MD','MLI','MRB'))
			BEGIN
			
				Set @planid=@plan1;
				Insert INTO #edudataset(Memid,meas,payer,age,gender) VALUES(@patientid,@meas,@planid,@age,@gender);
									
			END
			
			
			ELSE IF(@plan2 IN('PPO','POS','HMO','CEP') AND @plan1 IN('MD','MLI','MRB'))
			BEGIN
			
				Set @planid=@plan2;
				Insert INTO #edudataset(Memid,meas,payer,age,gender) VALUES(@patientid,@meas,@planid,@age,@gender);
								
			END
			ELSE
			BEGIN

				Set @planid=@plan1;
				Insert INTO #edudataset(Memid,meas,payer,age,gender) VALUES(@patientid,@meas,@planid,@age,@gender);
				
				Set @planid=@plan2;
				Insert INTO #edudataset(Memid,meas,payer,age,gender) VALUES(@patientid,@meas,@planid,@age,@gender);
				

			END						
		END
		
		Set @i=@i+1;
		
	END;
	

-- Create Indices on edudataset
CREATE INDEX [idx1] ON #edudataset ([payer]);
CREATE INDEX [patient_id_UNIQUE] ON #edudataset ([Memid],[payer]);


-- Get ED Visits list

Drop table if exists #edu_edvisits;
Create table #edu_edvisits
(
	Memid varchar(50),
	Date_S varchar(8),
	Date_Adm varchar(8),
	Date_Disch varchar(8),
	Claimid INT
	
)

Insert into #edu_edvisits
select distinct Memid,Date_S,Date_Adm,Date_Disch,ClaimID from(
select Memid,Date_S,Date_Adm,Date_Disch,ClaimID from HEDIS_VISIT where measure_id=@meas and Date_S!='' and Date_S between @ce_startdt and @ce_enddt and SuppData='N' and HCFAPOS!=81 and (
	CPT in (select code from VALUESET_TO_CODE where Value_Set_Name='ED')
	or 
	REV in(select code from VALUESET_TO_CODE where code_system='UBREV' and Value_Set_Name='ED') 
) and claimstatus=1
Union all
select Memid,Date_S,Date_Adm,Date_Disch,ClaimID from HEDIS_VISIT where measure_id=@meas and Date_S!='' and Date_S between @ce_startdt and @ce_enddt and SuppData='N' and HCFAPOS!=81 and CPT in (select code from VALUESET_TO_CODE where Value_Set_Name='ED Procedure Code') and HCFAPOS in(select code from VALUESET_TO_CODE where value_set_name='ED POS') and claimstatus=1
)t1
--where memid=134665  

CREATE CLUSTERED INDEX idx_edu_edvisits ON #edu_edvisits ([memid],[Date_S],[claimid]);







-- Observation Stay and Inpatient Stay

Drop table if exists #edu_stayexclusionlist;
Create table #edu_stayexclusionlist
(
	Memid varchar(50),
	Date_S varchar(8),
	Date_Adm varchar(8),
	Date_Disch varchar(8),
	Claimid INT
)

insert into #edu_stayexclusionlist
select Memid,Date_S,Date_Adm,Date_Disch,ClaimID from HEDIS_VISIT where measure_id=@meas and Date_Disch!='' and Date_Disch between @ce_startdt and @ce_enddt and SuppData='N' and HCFAPOS!=81 and	REV in(select code from VALUESET_TO_CODE where code_system='UBREV' and Value_Set_Name in('Inpatient Stay','Observation Stay'))
--and memid=125240                                 

CREATE CLUSTERED INDEX idx_edu_stayexclusionlist ON #edu_stayexclusionlist ([memid],[Date_S],[claimid]);

--Exclusion Set - Mental and Behavioral Disorders,	Psychiatry ,	Electroconvulsive therapy

Drop table if exists #edu_encounterexclusionlist;
Create table #edu_encounterexclusionlist
(
	Memid varchar(50),
	Date_S varchar(8),
	Date_Adm varchar(8),
	Date_Disch varchar(8),
	Claimid INT
)

Insert into #edu_encounterexclusionlist
select Memid,Date_S,Date_Adm,Date_Disch,ClaimID from HEDIS_VISIT where measure_id=@meas and Date_S!='' and Date_S between @ce_startdt and @ce_enddt and SuppData='N' and HCFAPOS!=81 and	
(
	Diag_I_1 in(select code from VALUESET_TO_CODE where Value_Set_Name='Mental and Behavioral Disorders')
	or
	CPT in(select code from VALUESET_TO_CODE where Value_Set_Name in('Psychiatry','Electroconvulsive Therapy'))
	or
	Proc_I_1 in(select code from VALUESET_TO_CODE where Value_Set_Name='Electroconvulsive Therapy')
	or
	Proc_I_2 in(select code from VALUESET_TO_CODE where Value_Set_Name='Electroconvulsive Therapy')
	or
	Proc_I_3 in(select code from VALUESET_TO_CODE where Value_Set_Name='Electroconvulsive Therapy')
	or
	Proc_I_4 in(select code from VALUESET_TO_CODE where Value_Set_Name='Electroconvulsive Therapy')
	or
	Proc_I_5 in(select code from VALUESET_TO_CODE where Value_Set_Name='Electroconvulsive Therapy')
	or
	Proc_I_6 in(select code from VALUESET_TO_CODE where Value_Set_Name='Electroconvulsive Therapy')
)


CREATE CLUSTERED INDEX idx_edu_encounterexclusionlist ON #edu_encounterexclusionlist ([memid],[Date_S],[claimid]);


-- Ed Visit Count by member

drop table if exists #edu_EDvstcount;
Create table #edu_EDvstcount
(
	Memid varchar(50),
	EDcount INT
)

Insert into #edu_EDvstcount
select Memid,count(*) as EdCount from(
select distinct Memid,Date_S from(
select ed.* from #edu_edvisits ed -- where memid=96248
left outer join #edu_encounterexclusionlist enx on ed.Memid=enx.Memid and ed.Claimid=enx.Claimid
left outer join #edu_stayexclusionlist stx on ed.Memid=stx.Memid and (ed.Claimid=stx.Claimid or ed.date_s between dateadd(day,-1,stx.Date_Adm) and stx.Date_Disch)

where enx.Memid is null and stx.memid is null --and ed.memid=125240
)t1 
)t2 
group by memid 

-- Calculate encounter and outlier fields for output
drop table if exists #edu_encounterlist;
Create table #edu_encounterlist
(
	Memid varchar(50),
	payer varchar(50),
	encounters INT,
	outliers INT
)


Insert into #edu_encounterlist
select ds.Memid,ds.payer,
case 
	when ds.payer in('MCR','MP','MCS') and ds.age between 18 and 64 and ISNULL(ed.EDcount,0)>5 then 0
	when ds.payer in('MCR','MP','MCS') and ds.age>=65 and ISNULL(ed.EDcount,0)>3 then 0
	when ds.payer in('CEP','HMO','POS','PPO') and ds.age>=18 and ISNULL(ed.EDcount,0)>3 then 0
	else ISNULL(ed.edcount,0)
end as encounters,
case 
	when ds.payer in('MCR','MP','MCS') and ds.age between 18 and 64 and ISNULL(ed.EDcount,0)>5 then 1
	when ds.payer in('MCR','MP','MCS') and ds.age>=65 and ISNULL(ed.EDcount,0)>3 then 1
	when ds.payer in('CEP','HMO','POS','PPO') and ds.age>=18 and ISNULL(ed.EDcount,0)>3 then 1
	else 0
end as outliers
from #edudataset ds
left outer join #edu_EDvstcount ed on ds.Memid=ed.memid
--where ds.memid=100428


update ds set ds.encounters=t1.encounters,ds.outlier=t1.outliers from #edudataset ds join #edu_encounterlist t1 on t1.Memid=ds.Memid and t1.payer=ds.payer;


-- Identifying all diagnosis

drop table if exists #edu_diagnosis;
Create table #edu_diagnosis
(
	Memid varchar(50),
	Date_S varchar(8),
	Date_Adm varchar(8),
	Date_Disch varchar(8),
	Claimid INT,
	claimstatus INT,
	diagnosis varchar(50)
	
)

Insert into #edu_diagnosis
select Memid,Date_S,Date_Adm,Date_Disch,claimid,claimstatus,replace(value,'.','') as diagnosis
from HEDIS_VISIT
unpivot
(
  value
  for col in (Diag_i_1, Diag_i_2, Diag_i_3,Diag_i_4,Diag_i_5,Diag_i_6,Diag_i_7,Diag_i_8,Diag_i_9,Diag_i_10)
) un
where measure_id=@meas and value!='' and Date_S!='' and Date_S between @ce_startdt1 and @ce_enddt1 and suppdata='N' and HCFAPOS!=81 and (
	CPT in (select code from VALUESET_TO_CODE where Value_Set_Name in('Outpatient','Telephone Visits','Observation','ED','Nonacute Inpatient','Acute Inpatient'))
	or 
	HCPCS in(select code from VALUESET_TO_CODE where Value_Set_Name='Outpatient') 
	or
	REV in(select code from VALUESET_TO_CODE where code_system='UBREV' and Value_Set_Name in('Outpatient','ED')) 
) --  and memid=99311
Union all
select Memid,Date_S,Date_Adm,Date_Disch,claimid,claimstatus,replace(value,'.','') as diagnosis
from HEDIS_VISIT
unpivot
(
  value
  for col in (Diag_i_1, Diag_i_2, Diag_i_3,Diag_i_4,Diag_i_5,Diag_i_6,Diag_i_7,Diag_i_8,Diag_i_9,Diag_i_10)
) un
where measure_id=@meas and value!='' and Date_Disch!='' and Date_Disch between @ce_startdt1 and @ce_enddt1 and suppdata='N' and HCFAPOS!=81 and (
	CPT in (select code from VALUESET_TO_CODE where Value_Set_Name in('Nonacute Inpatient','Acute Inpatient'))
	or
	REV in(select code from VALUESET_TO_CODE where code_system='UBREV' and Value_Set_Name in('Inpatient Stay')) 
) -- and memid=99311


CREATE CLUSTERED INDEX idx_edu_diagnosis ON #edu_diagnosis ([memid],[Date_S],[claimid],diagnosis);

-- HCC RANK
drop table if exists #edu_hccrank;

Create table #edu_hccrank
(
	Memid varchar(50),
	payer varchar(10),
	genagegroup varchar(20),
	reportingindicator varchar(20),
	productline  varchar(20),
	hcc  varchar(10)
	
)

Insert into #edu_hccrank
select t2.Memid,t2.payer,
case 
	when age between 18 and 44 and gender='F' then 'F_18-44'
	when age between 45 and 54 and gender='F' then 'F_45-54'
	when age between 55 and 64 and gender='F' then 'F_55-64'
	when age between 65 and 74 and gender='F' then 'F_65-74'
	when age between 75 and 84 and gender='F' then 'F_75-84'
	when age >=85 and gender='F' then 'F_85'
	when age between 18 and 44 and gender='M' then 'M_18-44'
	when age between 45 and 54 and gender='M' then 'M_45-54'
	when age between 55 and 64 and gender='M' then 'M_55-64'
	when age between 65 and 74 and gender='M' then 'M_65-74'
	when age between 75 and 84 and gender='M' then 'M_75-84'
	when age >=85 and gender='M' then 'M_85'
	end as genagegroup,
case 
	when payer in('MCR','MP','MCS') and age between 18 and 64 then 'Standard - 18-64'
	when payer in('MCR','MP','MCS') and age>=65 then 'Standard - 65+'
	else 'Standard - 18+'
	end as reportingindicator,
case
	When payer in('CEP','HMO','POS','PPO') then 'Commercial'
	else 'Medicare'
	end as productline,
	hcc
from(
select distinct Memid,HCC,cast(age as INT) as age,gender,payer from (
select d.Memid,cc.Comorbid_CC,hcc.RankingGroup,isnull(hcc.Rank,1) as Rank,ISNULL(hcc.HCC,concat('H',cc.Comorbid_CC)) as HCC,Rank() over(partition by d.Memid,RankingGroup order by d.Memid,RankingGroup,Rank) as calcrank,ds.age,ds.gender,ds.payer from #edu_diagnosis d
left outer join HEDIS_TABLE_CC cc on d.diagnosis=cc.diagnosiscode and cc.hcc_type='Shared'
left outer join HEDIS_HCC_RANK hcc on cc.Comorbid_CC=hcc.CC and hcc.hcc_type='Shared'
join #edudataset ds on ds.memid=d.memid and ds.outlier=0
where Comorbid_CC is not null
)t1 where calcrank=1
)t2

-- HCC RANK COMB
drop table if exists #edu_hccrankcmb;
Create table #edu_hccrankcmb
(
	Memid varchar(50),
	payer varchar(10),
	genagegroup varchar(20),
	reportingindicator varchar(20),
	productline  varchar(20),
	hcc  varchar(10),
	hcccmb  varchar(10)
	
)

Insert into #edu_hccrankcmb
select h.*,cmb.hcccomb from #edu_hccrank h 
left outer join HEDIS_HCC_COMB cmb on cmb.ComorbidHCC1 in(select HCC from #edu_hccrank t1 where t1.memid=h.memid) and cmb.ComorbidHCC2 in(select HCC from #edu_hccrank t1 where t1.memid=h.memid) and cmb.hcc_type='Shared'


-- PVC Model

drop table if exists #edu_ppvagegenwt;
Create table #edu_ppvagegenwt
(
	Memid varchar(50),
	payer varchar(10),
	PPVAgeGenWt float
	
)

Insert into #edu_ppvagegenwt
select distinct t1.Memid,t1.payer,r1.Weight as PPVAgeGenWt from(
select Memid,payer,case 
	when age between 18 and 44 and gender='F' then 'F_18-44'
	when age between 45 and 54 and gender='F' then 'F_45-54'
	when age between 55 and 64 and gender='F' then 'F_55-64'
	when age between 65 and 74 and gender='F' then 'F_65-74'
	when age between 75 and 84 and gender='F' then 'F_75-84'
	when age >=85 and gender='F' then 'F_85'
	when age between 18 and 44 and gender='M' then 'M_18-44'
	when age between 45 and 54 and gender='M' then 'M_45-54'
	when age between 55 and 64 and gender='M' then 'M_55-64'
	when age between 65 and 74 and gender='M' then 'M_65-74'
	when age between 75 and 84 and gender='M' then 'M_75-84'
	when age >=85 and gender='M' then 'M_85'
	end as genagegroup,
case 
	when payer in('MCR','MP','MCS') and age between 18 and 64 then 'Standard - 18-64'
	when payer in('MCR','MP','MCS') and age>=65 then 'Standard - 65+'
	else 'Standard - 18+'
	end as reportingindicator,
case
	When payer in('CEP','HMO','POS','PPO') then 'Commercial'
	else 'Medicare'
	end as productline
 from #edudataset where outlier=0
 )t1
 join HEDIS_RISK_ADJUSTMENT r1 on r1.VariableName=t1.genagegroup and t1.productline=r1.ProductLine and r1.variabletype='DEMO' and r1.Model='PPV' and r1.Measure_id=@meas




drop table if exists #edu_ppvcomorbidwt;
Create table #edu_ppvcomorbidwt
(
	Memid varchar(50),
	payer varchar(10),
	PPVComorbidWt float
	
)

Insert into #edu_ppvcomorbidwt
select Memid,payer,sum(weight) as PPVComorbidWt from(
select distinct h.Memid,h.payer,h.genagegroup,h.reportingindicator,h.hcc as hcc,r1.Weight from #edu_hccrankcmb h
join HEDIS_RISK_ADJUSTMENT r1 on r1.VariableName=h.hcc and h.productline=r1.ProductLine and r1.variabletype='HCC' and r1.Model='PPV' and r1.ReportingIndicator=h.reportingindicator and r1.Measure_ID=@meas

Union all
select distinct h.Memid,h.payer,h.genagegroup,h.reportingindicator,h.hcccmb as hcc,r1.Weight from #edu_hccrankcmb h
join HEDIS_RISK_ADJUSTMENT r1 on r1.VariableName=h.hcccmb and h.productline=r1.ProductLine and r1.variabletype='HCC' and r1.Model='PPV' and r1.ReportingIndicator=h.reportingindicator and r1.Measure_ID=@meas

)t1 group by memid,payer


update #edudataset set PPVAgeGenWt=p.PPVAgeGenWt from #edudataset ds join #edu_ppvagegenwt p on p.Memid=ds.Memid and p.payer=ds.payer;
update #edudataset set PPVComorbidWt=p.PPVComorbidWt from #edudataset ds join #edu_ppvcomorbidwt p on p.Memid=ds.Memid and p.payer=ds.payer;



-- PUCV Model

drop table if exists #edu_pucvagegenwt;
Create table #edu_pucvagegenwt
(
	Memid varchar(50),
	payer varchar(10),
	PUCVAgeGenWt float
	
)

Insert into #edu_pucvagegenwt
select distinct t1.Memid,t1.payer,r1.Weight as PUCVAgeGenWt from(
select Memid,payer,case 
	when age between 18 and 44 and gender='F' then 'F_18-44'
	when age between 45 and 54 and gender='F' then 'F_45-54'
	when age between 55 and 64 and gender='F' then 'F_55-64'
	when age between 65 and 74 and gender='F' then 'F_65-74'
	when age between 75 and 84 and gender='F' then 'F_75-84'
	when age >=85 and gender='F' then 'F_85'
	when age between 18 and 44 and gender='M' then 'M_18-44'
	when age between 45 and 54 and gender='M' then 'M_45-54'
	when age between 55 and 64 and gender='M' then 'M_55-64'
	when age between 65 and 74 and gender='M' then 'M_65-74'
	when age between 75 and 84 and gender='M' then 'M_75-84'
	when age >=85 and gender='M' then 'M_85'
	end as genagegroup,
case 
	when payer in('MCR','MP','MCS') and age between 18 and 64 then 'Standard - 18-64'
	when payer in('MCR','MP','MCS') and age>=65 then 'Standard - 65+'
	else 'Standard - 18+'
	end as reportingindicator,
case
	When payer in('CEP','HMO','POS','PPO') then 'Commercial'
	else 'Medicare'
	end as productline
 from #edudataset where outlier=0
 )t1
 join HEDIS_RISK_ADJUSTMENT r1 on r1.VariableName=t1.genagegroup and t1.productline=r1.ProductLine and r1.variabletype='DEMO' and r1.Model='PUCV'  and r1.Measure_ID=@meas



drop table if exists #edu_pucvcomorbidwt;
Create table #edu_pucvcomorbidwt
(
	Memid varchar(50),
	payer varchar(10),
	PUCVComorbidWt float
	
)

Insert into #edu_pucvcomorbidwt
select Memid,payer,case when decimalct>10 then round(PUCVomorbidWt,10,1) else PUCVomorbidWt end as PUCVomorbidWt from(
select Memid,payer,PUCVomorbidWt,CASE Charindex('.',PUCVomorbidWt ) WHEN 0 THEN 0  ELSE Len (Cast(Cast(Reverse(CONVERT(VARCHAR(50),PUCVomorbidWt, 128)) AS FLOAT) AS BIGINT)) END as decimalct from(
select Memid,payer,EXP(SUM(LOG(weight))) as PUCVomorbidWt from(
select distinct h.Memid,h.payer,h.genagegroup,h.reportingindicator,h.hcc as hcc,r1.Weight as weight from #edu_hccrankcmb h
join HEDIS_RISK_ADJUSTMENT r1 on r1.VariableName=h.hcc and h.productline=r1.ProductLine and r1.variabletype='HCC' and r1.Model='PUCV' and r1.ReportingIndicator=h.reportingindicator  and r1.Measure_ID=@meas
--and memid=97968
Union all
select distinct h.Memid,h.payer,h.genagegroup,h.reportingindicator,h.hcccmb as hcc,r1.Weight from #edu_hccrankcmb h
join HEDIS_RISK_ADJUSTMENT r1 on r1.VariableName=h.hcccmb and h.productline=r1.ProductLine and r1.variabletype='HCC' and r1.Model='PUCV' and r1.ReportingIndicator=h.reportingindicator  and r1.Measure_ID=@meas
--and memid=99311

)t1 group by memid,payer
)t2
)t3



update #edudataset set PUCVAgeGenWt=p.PUCVAgeGenWt from #edudataset ds join #edu_pucvagegenwt p on p.Memid=ds.Memid and p.payer=ds.payer;
update #edudataset set PUCVComorbidWt=p.PUCVComorbidWt from #edudataset ds join #edu_pucvcomorbidwt p on p.Memid=ds.Memid and p.payer=ds.payer;
update #edudataset set PUCVComorbidWt=0 where Outlier=1;


-- Generate Output
	select @runid=max(RUN_ID) from HEDIS_MEASURE_OUTPUT_RISK_UTL where Measure_id=@meas;

	if(@runid>=1)
	Begin
		SET @runid=@runid+1;
	End
	Else
	Begin
		SET @runid=1;
	END
	


Insert into HEDIS_MEASURE_OUTPUT_RISK_UTL(Memid,Meas,payer,Epop,Encounters,Outlier,PPVComorbidWt,PPVAgeGenWt,PUCVComorbidWt,PUCVAgeGenWt,Age,Gender,Measure_ID,Measurement_year,RUN_ID)
	SELECT memid,'EDU' as meas,payer,Epop,Encounters,Outlier,PPVComorbidWt,PPVAgeGenWt,PUCVComorbidWt,PUCVAgeGenWt,cast(age as Int) AS age,gender,@meas,@meas_year,@runid FROM #edudataset


GO
