SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
CREATE PROC [dbo].[EMPI_LINK_UNLINK]

AS


BEGIN
DECLARE @INFO VARCHAR(8000)
					,	@TRUE BIT = 1
					,	@FALSE BIT = 0
					,	@DB_ID INT = DB_ID()
					,	@LOGID INT = 0
					,	@PROC VARCHAR(500)=OBJECT_SCHEMA_NAME(@@PROCID)+'.'+OBJECT_NAME(@@PROCID)
					,	@PERF_START DATETIME
					,	@PERF_DURATION INT
					,	@PERF_ROW INT
					,	@RC	INT
					;

		EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, @INFO=@INFO, @LOG2ID=@LOGID OUT;
		DECLARE @PROCFULLNAME VARCHAR(500) = DB_NAME()+'.'+@PROC;
		----EXEC dbo.SP_MI_UTIL_SEND_MAIL @PROCFULLNAME, 'STARTED', 1, @ECHO=0; 
	DECLARE @MSG VARCHAR(1000)
	DECLARE @PERF_START_PROC DATETIME 
	DECLARE @PERF_RPS NUMERIC(10,2) 
	DECLARE @ROWS INT 
	DECLARE @PERF_ROWS INT 
	SET @PERF_ROWS = 0 
	SET @PERF_START = GETDATE() 
	SET @PERF_START_PROC = GETDATE() 

BEGIN TRY

---select * from [dbo].[open_empi_notifications]
---select * from [dbo].[open_empi_notifications_history]

----UPDATE THE UHN DATA MODEL
	-- Shanawaz 10-05-2021- Updated logic to idnetify EMPI updates
	Drop table if exists #TEMP_LINK_UNLINK;
	select distinct
		MEMBER_ID as Post_Orgid,
		EMPI_ID as Post_oeid
		INTO #TEMP_LINK_UNLINK
	from open_empi
	Join MEMBER on 
		MEMBER_ID=Org_patient_extension_id and
		MEM_DATA_SRC=DATA_SOURCE 
	where
		EMPI!=EMPI_ID

		

	/*Shanawaz 10-05-2021- Commenting this section - implementing logic to identify Member ids for which EMPI has changed by using Member and open_empi table join which is more reliable
	Select
		*
		INTO #TEMP_LINK_UNLINK
	From
	(
		Select
			*
			,ROW_NUMBER() over(partition by Post_Orgid order by ModifiedDate DESC) as rn 
		
		From
		(
			Select 
				n.Post_Orgid
				,n.Post_oeid
				,convert(datetime,n.dateCreatedTime)  as ModifiedDate
			FROM KPI_ENGINE.[dbo].[open_empi_notifications_history] nh
			join open_empi_notifications n on nh.identifierUpdateEventId=n.identifierUpdateEventId
			join open_empi_master o on nh.post_oeid=o.EMPI_ID
			Where
				Status=0

			Union all

			Select 
				cast(PostID as varchar(50)) as PostID,
				cast(postOpenEMPI as varchar(100)) as postOpenEMPI 
				,convert(datetime,dateValue) as ModifiedDate
			FROM KPI_ENGINE.[dbo].open_empi_notifications_update
			join open_empi_master o on cast(postOpenEMPI as varchar(100))=o.EMPI_ID
			Where
				Status=0
				
		)tbl
	)t1
	Where rn=1
	*/

	

EXEC SP_KPI_DROPTABLE '#TEMP_UNQ_LINK_UNLINK'
Select Post_Orgid,Post_oeid
INTO #TEMP_UNQ_LINK_UNLINK
FROM #TEMP_LINK_UNLINK




Update DBO.MEMBER
SET EMPI=L_UNL.Post_oeid
FROM DBO.MEMBER M
INNER JOIN #TEMP_UNQ_LINK_UNLINK L_UNL
ON M.MEMBER_ID=L_UNL.Post_Orgid


Update RPT.MEASURE_DETAILED_LINE
SET EMPI=L_UNL.Post_oeid
FROM RPT.MEASURE_DETAILED_LINE M
INNER JOIN #TEMP_UNQ_LINK_UNLINK L_UNL
ON M.PayerId=L_UNL.Post_Orgid




Update DBO.ENROLLMENT
SET EMPI=L_UNL.Post_oeid
FROM DBO.ENROLLMENT M
INNER JOIN #TEMP_UNQ_LINK_UNLINK L_UNL
ON M.MEMBER_ID=L_UNL.Post_Orgid


Update DBO.CLAIMLINE
SET EMPI=L_UNL.Post_oeid
FROM DBO.CLAIMLINE M
INNER JOIN #TEMP_UNQ_LINK_UNLINK L_UNL
ON M.MEMBER_ID=L_UNL.Post_Orgid

Update DBO.CLAIMHEADER
SET EMPI=L_UNL.Post_oeid
FROM DBO.CLAIMHEADER M
INNER JOIN #TEMP_UNQ_LINK_UNLINK L_UNL
ON M.MEMBER_ID=L_UNL.Post_Orgid


Update DBO.DIAGNOSIS
SET EMPI=L_UNL.Post_oeid
FROM DBO.DIAGNOSIS M
INNER JOIN #TEMP_UNQ_LINK_UNLINK L_UNL
ON M.MEMBER_ID=L_UNL.Post_Orgid


Update DBO.PROCEDURES
SET EMPI=L_UNL.Post_oeid
FROM DBO.PROCEDURES M
INNER JOIN #TEMP_UNQ_LINK_UNLINK L_UNL
ON M.MEMBER_ID=L_UNL.Post_Orgid


Update DBO.MEMBER_MONTH
SET EMPI=L_UNL.Post_oeid
FROM DBO.MEMBER_MONTH M
INNER JOIN #TEMP_UNQ_LINK_UNLINK L_UNL
ON M.MEMBER_ID=L_UNL.Post_Orgid


Update DBO.MemberMonth_snapshot
SET EMPI=L_UNL.Post_oeid
FROM DBO.MemberMonth_snapshot M
INNER JOIN #TEMP_UNQ_LINK_UNLINK L_UNL
ON M.MEMBER_ID=L_UNL.Post_Orgid


Update [dbo].[LAB]
SET EMPI=L_UNL.Post_oeid
FROM DBO.LAB M
INNER JOIN #TEMP_UNQ_LINK_UNLINK L_UNL
ON M.MEMBER_ID=L_UNL.Post_Orgid


Update DBO.MCFIELDS
SET EMPI=L_UNL.Post_oeid
FROM DBO.MCFIELDS M
INNER JOIN #TEMP_UNQ_LINK_UNLINK L_UNL
ON M.MEMBER_ID=L_UNL.Post_Orgid


Update DBO.MEDICATION
SET EMPI=L_UNL.Post_oeid
FROM DBO.MEDICATION M
INNER JOIN #TEMP_UNQ_LINK_UNLINK L_UNL
ON M.MEMBER_ID=L_UNL.Post_Orgid

Update REG.REGISTRY_OUTPUT
SET EMPI=L_UNL.Post_oeid
FROM REG.REGISTRY_OUTPUT M
INNER JOIN #TEMP_UNQ_LINK_UNLINK L_UNL
ON M.PayerId=L_UNL.Post_Orgid

Update REG.REGISTRY_OUTPUT_LINE
SET EMPI=L_UNL.Post_oeid
FROM REG.REGISTRY_OUTPUT_LINE M
INNER JOIN #TEMP_UNQ_LINK_UNLINK L_UNL
ON M.PayerId=L_UNL.Post_Orgid


Update dbo.Vital
SET EMPI=L_UNL.Post_oeid
FROM dbo.Vital M
INNER JOIN #TEMP_UNQ_LINK_UNLINK L_UNL
ON M.MEMBER_ID=L_UNL.Post_Orgid


Update RPT.PCP_Attribution
SET EMPI=L_UNL.Post_oeid
FROM RPT.PCP_Attribution M
INNER JOIN #TEMP_UNQ_LINK_UNLINK L_UNL
ON M.PayerId=L_UNL.Post_Orgid





Update RPT.UHN_UnattributedMembers
SET EMPI=L_UNL.Post_oeid
FROM RPT.UHN_UnattributedMembers M
INNER JOIN #TEMP_UNQ_LINK_UNLINK L_UNL
ON M.PayerId=L_UNL.Post_Orgid


Update RPT.HCC_LIST
SET EMPI=L_UNL.Post_oeid
FROM RPT.HCC_LIST M
INNER JOIN #TEMP_UNQ_LINK_UNLINK L_UNL
ON M.PayerId=L_UNL.Post_Orgid




Update RPT.UtilizationReport
SET EMPI=L_UNL.Post_oeid
FROM RPT.UtilizationReport M
INNER JOIN #TEMP_UNQ_LINK_UNLINK L_UNL
ON M.PayerId=L_UNL.Post_Orgid


Update RPT.ConsolidatedAttribution_Snapshot
SET EMPI=L_UNL.Post_oeid
FROM RPT.ConsolidatedAttribution_Snapshot M
INNER JOIN #TEMP_UNQ_LINK_UNLINK L_UNL
ON M.PayerId=L_UNL.Post_Orgid


Update RPT.Measure_DetailLine_Snapshot
SET EMPI=L_UNL.Post_oeid
FROM RPT.Measure_DetailLine_Snapshot M
INNER JOIN #TEMP_UNQ_LINK_UNLINK L_UNL
ON M.PayerId=L_UNL.Post_Orgid




Update [open_empi_notifications_history] set Status=1 where Status=0;
Update open_empi_notifications_update set Status=1 where Status=0;

----SYNC THE OPEN EMPI NOTIFICATION HISTORY TABLES


		SET @PERF_ROW = @@ROWCOUNT
		SET @PERF_DURATION = DATEDIFF(MINUTE,@PERF_START,GETDATE());
		EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, 'Enrollment Completed', @PERF_ROW, @DURATION_IN_MIN=@PERF_DURATION, @LOG2ID=@LOGID;
		EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, @LOG2ID=@LOGID, @END_FLAG=1;

END TRY
BEGIN CATCH
		BEGIN
			EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, 'FATAL ERROR', @LOG2ID=@LOGID;
			RAISERROR ('',16,0)
			RETURN 16
        END
	END CATCH
RETURN 0
END

GO
