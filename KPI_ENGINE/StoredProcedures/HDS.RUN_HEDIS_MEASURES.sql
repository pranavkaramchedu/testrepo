SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
CREATE Procedure [HDS].[RUN_HEDIS_MEASURES]
AS
BEGIN

---Creating I/P Tables
Exec [HDS].[LOAD_MEMBER_GM]
Exec [HDS].[LOAD_MEMBER_EN]
Exec [HDS].[LOAD_MMDF]
Exec [HDS].[LOAD_LISHIST]
Exec [HDS].[LOAD_PROVIDER]


Declare @MEASURE_ID varchar(50)
Declare @MEASURE_PROC varchar(500)


DECLARE C CURSOR FOR 
select MEASURE_PROC from hds.HEDIS_MEASURES_LIST WHERE MEASURE_ENABLE=1

OPEN C  
FETCH NEXT FROM C INTO @MEASURE_PROC
WHILE @@FETCH_STATUS = 0  
BEGIN  
	
	EXEC @MEASURE_PROC
	  	
	FETCH NEXT FROM C INTO @MEASURE_PROC
END 

CLOSE C  
DEALLOCATE C 

END
GO
