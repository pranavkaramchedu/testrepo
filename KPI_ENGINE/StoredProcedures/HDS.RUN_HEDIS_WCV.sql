SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
--/****** Object:  StoredProcedure [HDS].[WCV]    Script Date: 17-12-2020 00:18:54 ******/
--SET ANSI_NULLS ON
--GO
--SET QUOTED_IDENTIFIER ON
--GO
CREATE proc [HDS].[RUN_HEDIS_WCV]
AS
Drop table if exists #WCVdataset;

CREATE TABLE #WCVdataset (
  [patient_id] varchar(100) NOT NULL,
  [meas] varchar(20) DEFAULT NULL,
  [payer] varchar(100) DEFAULT NULL,
  [patient_gender] varchar(45) NOT NULL,
  [age] decimal(12,4) DEFAULT NULL,
  [orec] smallint DEFAULT NULL,
  [lis] smallint DEFAULT '0',
  [rexcl] smallint DEFAULT '0',
  [rexcld] smallint DEFAULT '0',
  [CE] smallint DEFAULT '0',
  [excl] smallint DEFAULT '0',
  [num] smallint DEFAULT '0',
  [Event] smallint DEFAULT '0',
) ;

-- Eligible Patient List
CREATE INDEX [idx1] ON #WCVdataset ([payer]);
CREATE INDEX [patient_id_UNIQUE] ON #WCVdataset ([patient_id],[payer]);

drop table if exists #WCV_memlist; 

CREATE TABLE #WCV_memlist (
    memid varchar(100)    
);

insert into #WCV_memlist
SELECT DISTINCT en.MemID FROM HDS.HEDIS_MEMBER_GM gm
join hds.HEDIS_MEMBER_EN en on en.MemID=gm.MemID and en.MEASURE_ID=gm.MEASURE_ID
WHERE en.MEASURE_ID='WCV_SAMPLE' 
and convert(date,en.StartDate)<='2020-12-31' AND convert(date,FinishDate)>='2020-01-01'
AND YEAR('2020-12-31')-YEAR(DOB) BETWEEN 3 AND 21

CREATE CLUSTERED INDEX ix_memlist_memid ON #WCV_memlist ([memid]);



-- Create Temp Patient Enrollment
drop table if exists #WCV_tmpsubscriber;

CREATE TABLE #WCV_tmpsubscriber (
    memid varchar(100),
    dob varchar(8),
	gender varchar(1),
	payer varchar(50),
	StartDate varchar(8),
	EndDate varchar(8),
);

CREATE CLUSTERED INDEX ix_tmpsub_memid ON #WCV_tmpsubscriber ([memid],[StartDate],[EndDate]);

insert into #WCV_tmpsubscriber
SELECT en.MemID,gm.DOB,gm.Gender,en.Payer,en.StartDate,en.FinishDate  FROM HDS.HEDIS_MEMBER_GM gm
join hds.HEDIS_MEMBER_EN en on en.MemID=gm.MemID and en.MEASURE_ID=gm.MEASURE_ID
WHERE en.MEASURE_ID='WCV_SAMPLE' and
convert(date,en.StartDate)<='2020-12-31' AND convert(date,FinishDate)>='2020-01-01'
AND YEAR('2020-12-31')-YEAR(DOB) BETWEEN 3 AND 21


Declare @pt_ctr INT;
Declare @i INT = 0;
Declare @patientid varchar(100);
Declare @gender varchar(1);
Declare @age int;
Declare @latest_insenddate varchar(10);
Declare @plan_ct INT=0;
Declare @planid varchar(20);
Declare @meas varchar(10)='WCV';
Declare @plan1 varchar(20);
Declare @plan2 varchar(20);

-- Get Patient COUNT
SELECT @pt_ctr=COUNT(*)  FROM #WCV_memlist;

PRINT 'pt_ctr:' + Cast(@pt_ctr as nvarchar);

-- While LOOP

-- Loop through enrollment for each patient

While @i<@pt_ctr
	BEGIN
		-- Get Patient to Loop
		SELECT  @patientid=memid FROM #WCV_memlist ORDER BY memid ASC OFFSET  @i ROWS FETCH NEXT 1 ROWS ONLY ;
		
		-- ReadingGender and age
		SELECT Top 1 @gender=gender,@age=YEAR('2020-12-31')-YEAR(DOB) FROM #WCV_tmpsubscriber WHERE memid=@patientid;
		
		-- Check for dual eligibility by checking if the patient is enrolled in more than one plan at the end of Enrollment
		
		SELECT Top 1 @latest_insenddate=convert(date,EndDate) FROM #WCV_tmpsubscriber WHERE  memid=@patientid  AND convert(date,StartDate)<='2020-12-31' ORDER BY convert(date,StartDate) DESC,convert(date,EndDate) Desc  ;
		-----SELECT Top 1 convert(date,EndDate) FROM #WCV_tmpsubscriber WHERE  memid='100001'  AND convert(date,StartDate)<='2020-12-31' ORDER BY convert(date,StartDate) DESC,convert(date,EndDate) Desc  ;
		-- Check for plan count patient is enrolled in during the end of measurement year
		
		SELECT @plan_ct=COUNT(*) FROM #WCV_tmpsubscriber WHERE  memid=@patientid AND convert(date,@latest_insenddate) BETWEEN convert(date,StartDate) AND convert(date,EndDate);
		-----SELECT COUNT(*) FROM #WCV_tmpsubscriber WHERE  memid='100001' AND convert(date,'2021-12-31') BETWEEN convert(date,StartDate) AND convert(date,EndDate);
		-- If only one plan, then skip the Payer reporting logic
		
		if(@plan_ct=1)
		BEGIN
			
			SELECT @planid=payer FROM #WCV_tmpsubscriber WHERE  memid=@patientid AND convert(date,@latest_insenddate) BETWEEN convert(date,StartDate) AND convert(date,EndDate) ORDER BY payer;
		
			-- If plan is Dual , Insert MCR & MCD	
			
			If(@planid in('MMP','SN3','MDE'))
			BEGIN
				IF(@planid in('MMP','SN3'))
				Begin
					------Insert INTO #WCVdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);   ----Commented
				Print 'Q'
				END
				
				SET @planid='MCD';
				Insert INTO #WCVdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
				Print 'W'
				SET @planid='MCR';
				------Insert INTO #WCVdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);     ------Commented
				Print 'E'
			END
			
			Else if(@planid IN('MD','MLI','MRB'))
			Begin
				SET @planid='MCD';
				Insert INTO #WCVdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
				Print 'R'
			END
			
			Else If(@planid IN('SN1','SN2'))
			Begin
				---Insert INTO #WCVdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);----------Commented
				Print 'T'
				SET @planid='MCR';
				Insert INTO #WCVdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
				Print 'Y'
			END
			ELSE
			BEGIN
				Insert INTO #WCVdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
				Print 'Z'
			END
		END
				
		-- Compare Plans if Plan count is more than one	
		if(@plan_ct>1)
		BEGIN
			
			-- Read First Plan
			SELECT  @plan1=payer FROM #WCV_tmpsubscriber where memid=@patientid and convert(date,@latest_insenddate) between convert(date,StartDate) AND convert(date,EndDate) order by 1 OFFSET  0 ROWS FETCH NEXT 1 ROWS ONLY ;
					
			-- Read second plan
			SELECT  @plan2=payer FROM #WCV_tmpsubscriber where memid=@patientid and convert(date,@latest_insenddate) between convert(date,StartDate) AND convert(date,EndDate) order by 1 OFFSET  1 ROWS FETCH NEXT 1 ROWS ONLY ;
			
			
			
			If(@plan1 IN('MCR','MP','MC','SN1','SN2','MCS') and @plan2 IN('PPO','POS','HMO','CEP'))
			BEGIN
				if(@plan1 IN('SN1','SN2'))
				BEGIN
					SET @planid=@plan1;
					Insert INTO #WCVdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
					Print 'A'
					SET @planid='MCR';
					Insert INTO #WCVdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
					Print 'B'
				END
				
				if(@plan1 in ('MCR','MP','MC','MCS'))
				Begin
					
					Set @planid=@plan1;
					Insert INTO #WCVdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
				
				END

			END
			
			Else If(@plan2 IN('MCR','MP','MC','SN1','SN2','MCS') AND @plan1 IN('PPO','POS','HMO','CEP'))
			BEGIN
			
				IF(@plan2 IN('SN1','SN2'))
				Begin
					
					Set @planid=@plan2;
					Insert INTO #WCVdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
					Print 'C'
					Set @planid='MCR';
					Insert INTO #WCVdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
					Print 'D'
				END
				
				
				IF(@plan2 IN('MCR','MP','MC','MCS'))
				BEGIN
					
					Set @planid=@plan2;
					
					Insert INTO #WCVdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
					Print 'E'
				END
			END
			
			ELSE IF(@plan1 IN('PPO','POS','HMO','CEP') AND @plan2 IN('MD','MLI','MRB'))
			BEGIN
			
				Set @planid=@plan1;
				Insert INTO #WCVdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
				PRINT 'F'	
			END
			
			ELSE IF(@plan2 IN('PPO','POS','HMO','CEP') AND @plan1 IN('MD','MLI','MRB'))
			BEGIN
			
				Set @planid=@plan2;
				Insert INTO #WCVdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
				PRINT 'G'	
			END
			
			ELSE IF(@plan1 IN('MDE','SN3','MMP') or @plan2 in('MDE','SN3','MMP'))
			BEGIN
			
				if(@plan1 IN('SN3','MMP'))
				Begin
					Set @planid=@plan1;
					Insert INTO #WCVdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
				PRINT 'H'
				End
				
				if(@plan2 IN('SN3','MMP'))
				Begin
					Set @planid=@plan2
					----Insert INTO #WCVdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);  Commented and gm.MemID='138201'
				END
				
					
				Set @planid='MCR';
				---Insert INTO #WCVdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);  -----Commented
				PRINT 'I'
				Set @planid='MCD';
				Insert INTO #WCVdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
				PRINT 'J'
			END
			
			ELSE
			BEGIN
			
				Set @planid=@plan1;
				
				Insert INTO #WCVdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
				Print '1'
				Set @planid=@plan2;
				Insert INTO #WCVdataset(patient_id,meas,payer,age,patient_gender) VALUES(@patientid,@meas,@planid,@age,@gender);
				Print '2'
			END
			
		END
		
		Set @i=@i+1;
		
	END;	





-- Continuous Enrollment
				
	DROP TABLE IF EXISTS #WCV_contenroll;
	
	CREATE table #WCV_contenroll
	(
		Memid varchar(50),
		CE INT
	);

	CREATE CLUSTERED INDEX ix_WCV_contenroll ON #WCV_contenroll ([memid]);

	
	Insert into #WCV_contenroll
	SELECT MemID,SUM(ceflag) AS CE FROM
	(
	
		-- CE Check for 1st year
		SELECT MemID,SUM(coverage) AS coverage, 1 AS ceflag FROM
		(
			SELECT MemID,Payer,start_date,end_date,DATEDIFF(day,start_date,end_date)+1 AS coverage FROM
			(

				SELECT s.MemID,s.Payer,s.FinishDate,
				CASE
				WHEN Convert(date,StartDate)<'2020-01-01' THEN '2020-01-01'
				ELSE Convert(date,StartDate)
				END AS start_date,
				CASE
				WHEN Convert(date,FinishDate)>'2020-12-31' THEN '2020-12-31'
				ELSE Convert(date,FinishDate)
				END AS end_date
				FROM HDS.HEDIS_MEMBER_EN s
				JOIN 
				(
					SELECT MemID,SUM(diff) as diff FROM
					(
						SELECT MemID,nextstartdate,FinishDate,
						CASE
						WHEN DATEDIFF(day,FinishDate,nextstartdate)<=1 THEN 0
						ELSE 1
						END AS diff
						  FROM
						  (
							SELECT MemID,StartDate,FinishDate,
							CASE
							WHEN nextstartdate IS NULL THEN FinishDate
							ELSE nextstartdate
							END AS nextstartdate FROM
							(
								SELECT s.MemID,Convert(date,s.StartDate) as StartDate,Convert(date,s.FinishDate) as FinishDate,
								(
									SELECT Top 1 s1.StartDate
									FROM HDS.HEDIS_MEMBER_EN s1
									WHERE s1.MEASURE_ID=s.MEASURE_ID AND s1.MemID=s.MemID AND s.FinishDate<s1.StartDate AND Convert(date,s1.StartDate)<='2020-12-31' AND Convert(date,s1.FinishDate)>='2020-01-01'
									ORDER BY s1.MemID,s1.StartDate,s1.FinishDate
								) AS nextstartdate

								 FROM HDS.HEDIS_MEMBER_EN s
								WHERE MEASURE_ID='WCV_SAMPLE' AND Convert(date,s.StartDate)<='2020-12-31' AND Convert(date,s.FinishDate)>='2020-01-01'
							)t
						)t1 
					)t2 GROUP BY MemID HAVING SUM(diff)<=1
				)
				s1 ON s.MemID=s1.MemID
				WHERE s.MEASURE_ID='WCV_SAMPLE'
				-- SQLINES DEMO *** 100001
				AND Convert(date,s.StartDate)<='2020-12-31' AND Convert(date,s.FinishDate)>='2020-01-01'
	
			)t2
		)t3 GROUP BY MemID HAVING sum(coverage)>=(DATEPART(dy, '2020-12-31')-45)

		Union All

		-- Anchor Date
	 
		SELECT MemID,SUM(DATEDIFF(day,start_date,end_date)) AS coverage, 1 AS ceflag FROM
			(
			SELECT MemID,FinishDate,
			CASE
				WHEN Convert(date,StartDate)<'2020-01-01' THEN '2020-01-01'
				ELSE Convert(date,StartDate)
			END AS start_date,
			CASE
				WHEN Convert(date,FinishDate)>'2020-12-31' THEN '2020-12-31'
				ELSE Convert(date,FinishDate)
			END AS end_date
			FROM HDS.HEDIS_MEMBER_EN 
			WHERE MEASURE_ID='WCV_SAMPLE'
			-- SQLINES DEMO *** 0000          
			AND '2020-12-31' BETWEEN Convert(date,StartDate) AND Convert(date,FinishDate)
		
		)t4  GROUP BY MemID
 
	)cont_enroll GROUP BY MemID  HAVING SUM(ceflag)=2 order by MemID;
	
	update #WCVdataset set CE=1 from #WCVdataset ds join #WCV_contenroll ce on ds.patient_id=ce.MemID;



--select distinct E.Patient_Id,E.CE,S.CE from #WCVdataset E
--left join hds.HEDIS_score S 
--on E.patient_id=S.MemID 
--where measure_id='WCV_SAMPLE' and E.CE=S.CE and E.CE=1



-- Hospice Exclusion



	drop table if exists #hospicemembers;
	CREATE table #hospicemembers
	(
		Memid varchar(50)
		
	);

	Insert into #hospicemembers
	select distinct t1.MemID from
	(
	select Memid from HDS.HEDIS_OBS where Measure_id='WCV_SAMPLE' and date !='' and convert(date,[date]) between '2020-01-01' and '2020-12-31' and OCode IN(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Hospice Encounter','Hospice Intervention'))
	Union all
	select MemID from HDS.HEDIS_VISIT v
	where MEASURE_ID='WCV_SAMPLE' and HCFAPOS!=81 and Date_S!='' and Convert(date,Date_S) between '2020-01-01' and '2020-12-31'
	and (
	Diag_I_1 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or
	Diag_I_2 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or
	Diag_I_3 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or
	Diag_I_4 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or
	Diag_I_5 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or
	Diag_I_6 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or
	Diag_I_7 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or
	Diag_I_8 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or
	Diag_I_9 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or
	Diag_I_10 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or
	CPT in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or
	CPT2 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or
	HCPCS in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or 
	Proc_I_1 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or 
	Proc_I_2 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or 
	Proc_I_3 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or 
	Proc_I_4 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or 
	Proc_I_5 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or 
	Proc_I_6 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	or
	Rev in(select code from HDS.VALUESET_TO_CODE where code_system='UBREV' and Value_Set_Name in('Hospice Intervention','Hospice Encounter'))
	)
	)t1
	
	CREATE CLUSTERED INDEX idx_hospicemembers ON #hospicemembers ([memid]);
	update #WCVdataset set rexcl=1 from #WCVdataset ds join #hospicemembers hos on hos.memid=ds.patient_id;
	

--select * from #WCVdataset E
--left join hds.HEDIS_score S 
--on E.patient_id=S.MemID 
--where measure_id='WCV_SAMPLE' and E.rexcl<>S.rexcl





drop table if exists #num;
	CREATE table #num
	(
		Memid varchar(50)
	);

	Insert into #num(Memid)
	select distinct Memid from(
	----select distinct memid from HDS.HEDIS_PROC where MEASURE_ID='WCV_SAMPLE' and pstatus='EVN' and convert(date,sdate) between '2020-01-01' and '2020-12-31' and sdate!='' and pcode in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Well-Care'))
	---Union all
	select distinct MemID from HDS.HEDIS_VISIT v
	where MEASURE_ID='WCV_SAMPLE' and Date_S!='' and convert(date,Date_S) between '2020-01-01' AND '2020-12-31' AND HCFAPOS<>'81'
	and ProvID in (select distinct ProvID from hds.HEDIS_PROVIDER where measure_id='WCV_SAMPLE' and (PCP='Y' OR OBGYN='Y')) ----AND SuppData='N' 
	and (
	Diag_I_1 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in ('Well-Care'))
	OR
	Diag_I_2 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in ('Well-Care'))
	OR
	Diag_I_3 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in ('Well-Care'))
	OR
	Diag_I_4 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in ('Well-Care'))
	OR
	Diag_I_5 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Well-Care'))
	OR
	Diag_I_6 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in ('Well-Care'))
	OR
	Diag_I_7 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in ('Well-Care'))
	OR
	Diag_I_8 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in ('Well-Care'))
	OR
	Diag_I_9 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in ('Well-Care'))
	OR
	Diag_I_10 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in ('Well-Care'))
	or
	HCPCS in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in ('Well-Care'))
	OR
	CPT in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in ('Well-Care')) 
	OR
	Proc_I_1 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in ('Well-Care'))
	or 
	Proc_I_2 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in ('Well-Care'))
	or 
	Proc_I_3 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in ('Well-Care'))
	or 
	Proc_I_4 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in ('Well-Care'))
	or 
	Proc_I_5 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in ('Well-Care'))
	or 
	Proc_I_6 in(select code from HDS.VALUESET_TO_CODE where Value_Set_Name in ('Well-Care'))
	)
	UNION
select distinct E.MemID from hds.HEDIS_VISIT_E E
left join hds.HEDIS_DIAG D on E.MemID=D.MemID AND CONVERT(DATE,E.sdate)=CONVERT(DATE,D.sdate) and D.measure_id=E.MEASURE_ID
left join hds.HEDIS_PROC P on E.MemID=P.MemID AND CONVERT(DATE,E.sdate)=CONVERT(DATE,P.sdate) and P.measure_id=E.MEASURE_ID AND P.pstatus='EVN'
where E.measure_id='WCV_SAMPLE' and CONVERT(DATE,E.sdate) BETWEEN '2020-01-01' and '2020-12-31' and 
COALESCE(D.dcode,P.pcode) IN (select code from HDS.VALUESET_TO_CODE where Value_Set_Name in ('Well-Care'))
and e.Providerid IN (select distinct ProvID from hds.HEDIS_PROVIDER where measure_id='WCV_SAMPLE' and (PCP='Y' OR OBGYN='Y'))
)t1

CREATE CLUSTERED INDEX idx_NUM ON #NUM ([memid]);

update #WCVdataset set NUM=1 from #WCVdataset ds join #num NUM on NUM.memid=ds.patient_id;





Declare @runid INT;
Declare @meas_year nvarchar(4)='2020'
select @runid=max(RUN_ID) from HDS.HEDIS_MEASURE_OUTPUT where measure_id='WCV';

if(@runid>=1)
Begin
SET @runid=@runid+1;
End
Else
Begin
		SET @runid=1;
END
	

Insert into HDS.HEDIS_MEASURE_OUTPUT(memid,Meas,payer,CE,Event,Epop,Excl,Num,Rexcl,RExclD,Age,Gender,Measure_ID,Measurement_year,RUN_ID)
SELECT patient_id AS memid,meas,payer,CE,0 as EVENT,CASE WHEN CE='1'  AND EVENT='0' AND rexcl='0' and rexcld='0' AND PAYER not in ('MCS','MC','MCR','MP') THEN 1 ELSE 0 END AS epop,excl,num,rexcl,rexcld,cast(age as Int) AS age,patient_gender AS gender,'WCV' as Measure_ID,@meas_year,@runid FROM #WCVdataset






---select Trim(Memid),Meas,Payer,CE,Event,Epop,Excl,Num,Rexcl,RExcld,Age,Gender 
---from HEDIS_MEASURE_OUTPUT where MEASURE_ID='WCV' and run_id=(select max(run_id) from HEDIS_MEASURE_OUTPUT where measure_id='WCV')
GO
