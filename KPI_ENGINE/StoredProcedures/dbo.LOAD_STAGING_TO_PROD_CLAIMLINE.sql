SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
CREATE PROCEDURE [dbo].[LOAD_STAGING_TO_PROD_CLAIMLINE]

AS

BEGIN

	DECLARE @INFO VARCHAR(8000)
					,	@TRUE BIT = 1
					,	@FALSE BIT = 0
					,	@DB_ID INT = DB_ID()
					,	@LOGID INT = 0
					,	@PROC VARCHAR(500)=OBJECT_SCHEMA_NAME(@@PROCID)+'.'+OBJECT_NAME(@@PROCID)
					,	@PERF_START DATETIME
					,	@PERF_DURATION INT
					,	@PERF_ROW INT
					,	@RC	INT
					;

		EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, @INFO=@INFO, @LOG2ID=@LOGID OUT;
		DECLARE @PROCFULLNAME VARCHAR(500) = DB_NAME()+'.'+@PROC;
		----EXEC dbo.SP_MI_UTIL_SEND_MAIL @PROCFULLNAME, 'STARTED', 1, @ECHO=0; 
	DECLARE @MSG VARCHAR(1000)
	DECLARE @PERF_START_PROC DATETIME 
	DECLARE @PERF_RPS NUMERIC(10,2) 
	DECLARE @ROWS INT 
	DECLARE @PERF_ROWS INT 
	SET @PERF_ROWS = 0 
	SET @PERF_START = GETDATE() 
	SET @PERF_START_PROC = GETDATE() 

BEGIN TRY

Declare @Error_Keys varchar(100);
SET @Error_Keys=NULL;

SET @Error_Keys=(Select Distinct TOP 1 CL_DATA_SRC FROM [KPI_ENGINE_STAGING].[dbo].[STAGING_CLAIMLINE] M
LEFT JOIN (Select Distinct PAYER  FROM [dbo].[STAGING_TO_PROD_DEDUP_KEY] WHERE PROD_TABLE='CLAIMLINE') DEDUPKEY
ON CL_DATA_SRC=DEDUPKEY.PAYER 
where PAYER is NULL)

IF @Error_Keys IS NOT NULL
RAISERROR ('CREATE THE DEDUP KEYS FOR CLAIMLINE: Select Distinct TOP 1 CL_DATA_SRC FROM [KPI_ENGINE_STAGING].[dbo].[STAGING_CLAIMLINE] M
LEFT JOIN (Select Distinct PAYER  FROM [dbo].[STAGING_TO_PROD_DEDUP_KEY] WHERE PROD_TABLE=''CLAIMLINE'') DEDUPKEY
ON CL_DATA_SRC=DEDUPKEY.PAYER 
where PAYER is NULL',16,1);



DEclare @PROD_TABLE varchar(100)
Declare @PAYER varchar(100)
Declare @ROOT_COMPANIES_ID int
Declare @KEYS varchar(5000)
DECLARE @SQL VARCHAR(MAX)
DECLARE C CURSOR FOR 
select
    PROD_TABLE
	,PAYER
	,ROOT_COMPANIES_ID
    ,stuff((
        select ' AND ' + CASE 
	WHEN KEY_COLUMNS_TYPE='VARCHAR' THEN 'ISNULL(CL.'+KEY_COLUMNS+','''')' 
	WHEN KEY_COLUMNS_TYPE='INT' THEN 'ISNULL(CL.'+KEY_COLUMNS+',''0'')' 
	WHEN KEY_COLUMNS_TYPE='DATE' THEN 'ISNULL(CL.'+KEY_COLUMNS+',''1900-01-01'')' 
END+'='+CASE 
	WHEN KEY_COLUMNS_TYPE='VARCHAR' THEN 'ISNULL(PCL.'+KEY_COLUMNS+','''')' 
	WHEN KEY_COLUMNS_TYPE='INT' THEN 'ISNULL(PCL.'+KEY_COLUMNS+',''0'')' 
	WHEN KEY_COLUMNS_TYPE='DATE' THEN 'ISNULL(PCL.'+KEY_COLUMNS+',''1900-01-01'')' 
END
        from [dbo].[STAGING_TO_PROD_DEDUP_KEY] t
        where t.PROD_TABLE = t1.PROD_TABLE AND t.PAYER = t1.PAYER AND t.ROOT_COMPANIES_ID = t1.ROOT_COMPANIES_ID
        order by t.KEY_COLUMNS
        for xml path('')
    ),1,4,'') as name_csv
from [dbo].[STAGING_TO_PROD_DEDUP_KEY] t1
where t1.PROD_TABLE='CLAIMLINE'
group by 
PROD_TABLE
,PAYER
,ROOT_COMPANIES_ID

OPEN C  
FETCH NEXT FROM C INTO @PROD_TABLE,@PAYER,@ROOT_COMPANIES_ID,@KEYS

WHILE @@FETCH_STATUS = 0  
BEGIN  



SET @SQL='
UPDATE KPI_ENGINE.dbo.[CLAIMLINE]
SET
		 [CLAIM_SFX_OR_PARENT]=LTRIM(RTRIM(CL.[CLAIM_SFX_OR_PARENT]))
		,[FORM_TYPE]=LTRIM(RTRIM(CL.[FORM_TYPE]))
		,[ADM_DATE]=LTRIM(RTRIM(CL.[ADM_DATE]))
		,[ADM_TYPE]=LTRIM(RTRIM(CL.[ADM_TYPE]))
		,[ADM_SRC]=LTRIM(RTRIM(CL.[ADM_SRC]))
		,[DIS_DATE]=LTRIM(RTRIM(CL.[DIS_DATE]))
		,[CLIENT_LOS]=LTRIM(RTRIM(CL.[CLIENT_LOS]))
		,[TO_DATE]=LTRIM(RTRIM(CL.[TO_DATE]))
		,[HOSP_TYPE]=LTRIM(RTRIM(CL.[HOSP_TYPE]))
		,[MEMBER_ID]=LTRIM(RTRIM(CL.[MEMBER_ID]))
		,[MEMBER_QUAL]=LTRIM(RTRIM(CL.[MEMBER_QUAL]))
		,[RELATION]=LTRIM(RTRIM(CL.[RELATION]))
		,[GRP_ID]=LTRIM(RTRIM(CL.[GRP_ID]))
		,[CLAIM_REC_DATE]=LTRIM(RTRIM(CL.[CLAIM_REC_DATE]))
		,[CLAIM_ENTRY_DATE]=LTRIM(RTRIM(CL.[CLAIM_ENTRY_DATE]))
		,[PAID_DATE]=LTRIM(RTRIM(CL.[PAID_DATE]))
		,[CHK_NUM]=LTRIM(RTRIM(CL.[CHK_NUM]))
		,[MS_DRG]=LTRIM(RTRIM(CL.[MS_DRG]))
		,[TRI_DRG]=LTRIM(RTRIM(CL.[TRI_DRG]))
		,[AP_DRG]=LTRIM(RTRIM(CL.[AP_DRG]))
		,[APR_DRG]=LTRIM(RTRIM(CL.[APR_DRG]))
		,[APR_DRG_SEV]=LTRIM(RTRIM(CL.[APR_DRG_SEV]))
		,[CONT_STAY_ID]=LTRIM(RTRIM(CL.[CONT_STAY_ID]))
		,[ATT_PROV]=LTRIM(RTRIM(CL.[ATT_PROV]))
		,[ATT_PROV_SPEC]=LTRIM(RTRIM(CL.[ATT_PROV_SPEC]))
		,[ATT_IPA]=LTRIM(RTRIM(CL.[ATT_IPA]))
		,[ATT_ACO]=LTRIM(RTRIM(CL.[ATT_ACO]))
		,[BILL_PROV]=LTRIM(RTRIM(CL.[BILL_PROV]))
		,[REF_PROV]=LTRIM(RTRIM(CL.[REF_PROV]))
		,[ACO_ID]=LTRIM(RTRIM(CL.[ACO_ID]))
		,[MED_HOME_ID]=LTRIM(RTRIM(CL.[MED_HOME_ID]))
		,[CONTRACT]=LTRIM(RTRIM(CL.[CONTRACT]))
		,[BEN_PKG_ID]=LTRIM(RTRIM(CL.[BEN_PKG_ID]))
		,[POS]=LTRIM(RTRIM(CL.[POS]))
		,[UB_BILL_TYPE]=LTRIM(RTRIM(CL.[UB_BILL_TYPE]))
		,[TOS]=LTRIM(RTRIM(CL.[TOS]))
		,[REV_CODE]=LTRIM(RTRIM(CL.[REV_CODE]))
		,[RX_DRUG_COST]=LTRIM(RTRIM(CL.[RX_DRUG_COST]))
		,[RX_INGR_COST]=LTRIM(RTRIM(CL.[RX_INGR_COST]))
		,[RX_DISP_FEE]=LTRIM(RTRIM(CL.[RX_DISP_FEE]))
		,[RX_DISCOUNT]=LTRIM(RTRIM(CL.[RX_DISCOUNT]))
		,[RX_FILL_SRC]=LTRIM(RTRIM(CL.[RX_FILL_SRC]))
		,[RX_REFILLS]=LTRIM(RTRIM(CL.[RX_REFILLS]))
		,[RX_PAR]=LTRIM(RTRIM(CL.[RX_PAR]))
		,[RX_FORM]=LTRIM(RTRIM(CL.[RX_FORM]))
		,[SV_UNITS]=LTRIM(RTRIM(CL.[SV_UNITS]))
		,[AMT_BILLED]=LTRIM(RTRIM(CL.[AMT_BILLED]))		
		,[AMT_ALLOWED_STAT]=LTRIM(RTRIM(CL.[AMT_ALLOWED_STAT]))
		,[AMT_PAID]=LTRIM(RTRIM(CL.[AMT_PAID]))
		,[AMT_HRA]=LTRIM(RTRIM(CL.[AMT_HRA]))
		,[AMT_DEDUCT]=LTRIM(RTRIM(CL.[AMT_DEDUCT]))
		,[AMT_COINS]=LTRIM(RTRIM(CL.[AMT_COINS]))
		,[AMT_COPAY]=LTRIM(RTRIM(CL.[AMT_COPAY]))
		,[AMT_COB]=LTRIM(RTRIM(CL.[AMT_COB]))
		,[AMT_WITHHOLD]=LTRIM(RTRIM(CL.[AMT_WITHHOLD]))
		,[AMT_DISALLOWED]=LTRIM(RTRIM(CL.[AMT_DISALLOWED]))
		,[AMT_MAXIMUM]=LTRIM(RTRIM(CL.[AMT_MAXIMUM]))
		,[AUTH_ID]=LTRIM(RTRIM(CL.[AUTH_ID]))
		,[DIS_STAT]=LTRIM(RTRIM(CL.[DIS_STAT]))
		,[BABY_WGHT]=LTRIM(RTRIM(CL.[BABY_WGHT]))
		,[WEEKS_GEST]=LTRIM(RTRIM(CL.[WEEKS_GEST]))
		,[BABY_CLAIM_ID]=LTRIM(RTRIM(CL.[BABY_CLAIM_ID]))
		,[CLAIM_IN_NETWORK]=LTRIM(RTRIM(CL.[CLAIM_IN_NETWORK]))
		,[DRG_TYPE]=LTRIM(RTRIM(CL.[DRG_TYPE]))
		,[TEST_VALUE]=LTRIM(RTRIM(CL.[TEST_VALUE]))
		,[TEST_INDICATOR]=LTRIM(RTRIM(CL.[TEST_INDICATOR]))
		,[LOINC]=LTRIM(RTRIM(CL.[LOINC]))
		,[_KPI_USER_DIM_01_]=LTRIM(RTRIM(CL.[_KPI_USER_DIM_01_]))
		,[_KPI_USER_DIM_02_]=LTRIM(RTRIM(CL.[_KPI_USER_DIM_02_]))
		,[_KPI_USER_DIM_03_]=LTRIM(RTRIM(CL.[_KPI_USER_DIM_03_]))
		,[_KPI_USER_DIM_04_]=LTRIM(RTRIM(CL.[_KPI_USER_DIM_04_]))
		,[_KPI_USER_DIM_05_]=LTRIM(RTRIM(CL.[_KPI_USER_DIM_05_]))
		,[_KPI_USER_DIM_06_]=LTRIM(RTRIM(CL.[_KPI_USER_DIM_06_]))
		,[_KPI_USER_DIM_07_]=LTRIM(RTRIM(CL.[_KPI_USER_DIM_07_]))
		,[_KPI_USER_DIM_08_]=LTRIM(RTRIM(CL.[_KPI_USER_DIM_08_]))
		,[_KPI_USER_DIM_09_]=LTRIM(RTRIM(CL.[_KPI_USER_DIM_09_]))
		,[_KPI_USER_DIM_10_]=LTRIM(RTRIM(CL.[_KPI_USER_DIM_10_]))
		,[_Claim_UDF_01_]=LTRIM(RTRIM(CL.[_Claim_UDF_01_]))
		,[_Claim_UDF_02_]=LTRIM(RTRIM(CL.[_Claim_UDF_02_]))
		,[_Claim_UDF_03_]=LTRIM(RTRIM(CL.[_Claim_UDF_03_]))
		,[_Claim_UDF_04_]=LTRIM(RTRIM(CL.[_Claim_UDF_04_]))
		,[_Claim_UDF_05_]=LTRIM(RTRIM(CL.[_Claim_UDF_05_]))
		,[_Claim_UDF_06_]=LTRIM(RTRIM(CL.[_Claim_UDF_06_]))
		,[_Claim_UDF_07_]=LTRIM(RTRIM(CL.[_Claim_UDF_07_]))
		,[_Claim_UDF_08_]=LTRIM(RTRIM(CL.[_Claim_UDF_08_]))
		,[_Claim_UDF_09_]=LTRIM(RTRIM(CL.[_Claim_UDF_09_]))
		,[_Claim_UDF_10_]=LTRIM(RTRIM(CL.[_Claim_UDF_10_]))
		,[_Claim_UDF_11_]=LTRIM(RTRIM(CL.[_Claim_UDF_11_]))
		,[_Claim_UDF_12_]=LTRIM(RTRIM(CL.[_Claim_UDF_12_]))
		,[_Claim_UDF_13_]=LTRIM(RTRIM(CL.[_Claim_UDF_13_]))
		,[_Claim_UDF_14_]=LTRIM(RTRIM(CL.[_Claim_UDF_14_]))
		,[_Claim_UDF_15_]=LTRIM(RTRIM(CL.[_Claim_UDF_15_]))
		,[_Claim_UDF_16_]=LTRIM(RTRIM(CL.[_Claim_UDF_16_]))
		,[_Claim_UDF_17_]=LTRIM(RTRIM(CL.[_Claim_UDF_17_]))
		,[_Claim_UDF_18_]=LTRIM(RTRIM(CL.[_Claim_UDF_18_]))
		,[_Claim_UDF_19_]=LTRIM(RTRIM(CL.[_Claim_UDF_19_]))
		,[_Claim_UDF_20_]=LTRIM(RTRIM(CL.[_Claim_UDF_20_]))
		,[_Claim_UDF_21_]=LTRIM(RTRIM(CL.[_Claim_UDF_21_]))
		,[_Claim_UDF_22_]=LTRIM(RTRIM(CL.[_Claim_UDF_22_]))
		,[_Claim_UDF_23_]=LTRIM(RTRIM(CL.[_Claim_UDF_23_]))
		,[_Claim_UDF_24_]=LTRIM(RTRIM(CL.[_Claim_UDF_24_]))
		,[_Claim_UDF_25_]=LTRIM(RTRIM(CL.[_Claim_UDF_25_]))
		,[_Claim_UDF_26_]=LTRIM(RTRIM(CL.[_Claim_UDF_26_]))
		,[_Claim_UDF_27_]=LTRIM(RTRIM(CL.[_Claim_UDF_27_]))
		,[_Claim_UDF_28_]=LTRIM(RTRIM(CL.[_Claim_UDF_28_]))
		,[_Claim_UDF_29_]=LTRIM(RTRIM(CL.[_Claim_UDF_29_]))
		,[_Claim_UDF_30_]=LTRIM(RTRIM(CL.[_Claim_UDF_30_]))
		,[_Claim_UDF_31_]=LTRIM(RTRIM(CL.[_Claim_UDF_31_]))
		,[_Claim_UDF_32_]=LTRIM(RTRIM(CL.[_Claim_UDF_32_]))
		,[_Claim_UDF_33_]=LTRIM(RTRIM(CL.[_Claim_UDF_33_]))
		,[_Claim_UDF_34_]=LTRIM(RTRIM(CL.[_Claim_UDF_34_]))
		,[_Claim_UDF_35_]=LTRIM(RTRIM(CL.[_Claim_UDF_35_]))
		,[_Claim_UDF_36_]=LTRIM(RTRIM(CL.[_Claim_UDF_36_]))
		,[_Claim_UDF_37_]=LTRIM(RTRIM(CL.[_Claim_UDF_37_]))
		,[_Claim_UDF_38_]=LTRIM(RTRIM(CL.[_Claim_UDF_38_]))
		,[_Claim_UDF_39_]=LTRIM(RTRIM(CL.[_Claim_UDF_39_]))
		,[_Claim_UDF_40_]=LTRIM(RTRIM(CL.[_Claim_UDF_40_]))
		,[_Claim_UDF_41_]=LTRIM(RTRIM(CL.[_Claim_UDF_41_]))
		,[_Claim_UDF_42_]=LTRIM(RTRIM(CL.[_Claim_UDF_42_]))
		,[_Claim_UDF_43_]=LTRIM(RTRIM(CL.[_Claim_UDF_43_]))
		,[_Claim_UDF_44_]=LTRIM(RTRIM(CL.[_Claim_UDF_44_]))
		,[_Claim_UDF_45_]=LTRIM(RTRIM(CL.[_Claim_UDF_45_]))
		,[_Claim_UDF_46_]=LTRIM(RTRIM(CL.[_Claim_UDF_46_]))
		,[_Claim_UDF_47_]=LTRIM(RTRIM(CL.[_Claim_UDF_47_]))
		,[_Claim_UDF_48_]=LTRIM(RTRIM(CL.[_Claim_UDF_48_]))
		,[_Claim_UDF_49_]=LTRIM(RTRIM(CL.[_Claim_UDF_49_]))
		,[_Claim_UDF_50_]=LTRIM(RTRIM(CL.[_Claim_UDF_50_]))
		,[ATT_PROV_NAME]=LTRIM(RTRIM(CL.[ATT_PROV_NAME]))
		,[ATT_NPI]=LTRIM(RTRIM(CL.[ATT_NPI]))
		,[BILL_PROV_NAME]=LTRIM(RTRIM(CL.[BILL_PROV_NAME]))
		,[BILL_PROV_TIN]=LTRIM(RTRIM(CL.[BILL_PROV_TIN]))
		,[REF_PROV_NAME]=LTRIM(RTRIM(CL.[REF_PROV_NAME]))		
		,[Filename]=LTRIM(RTRIM(CL.[Filename]))
		,[Filedate]=LTRIM(RTRIM(CL.[Filedate]))
		,[SERV_LOC_NAME]=LTRIM(RTRIM(CL.[SERV_LOC_NAME]))
		,[SERV_LOC_NPI]=LTRIM(RTRIM(CL.[SERV_LOC_NPI]))
FROM [KPI_ENGINE_STAGING].[dbo].[STAGING_CLAIMLINE] CL
INNER JOIN KPI_ENGINE.dbo.Open_empi EMPI
on CL.MEMBER_ID=EMPI.Org_Patient_Extension_ID AND (CL.CL_DATA_SRC=EMPI.[Data_Source] or CL.CL_DATA_SRC in(''FlatFiles-Gaps'',''FlatFiles'')) AND CL.ROOT_COMPANIES_ID=EMPI.Root_Companies_ID  
LEFT JOIN KPI_ENGINE.dbo.[CLAIMLINE] PCL
ON '+@KEYS+
' WHERE PCL.ST_CLAIM_ID IS NOT NULL AND CL.CL_DATA_SRC='''+@PAYER+''' AND CL.Root_Companies_ID='+CONVERT(VARCHAR(10),@ROOT_COMPANIES_ID)


Declare @updatecount INT;
EXECUTE (@SQL)
Set @updatecount=@@ROWCOUNT;

--PRINT (@SQL)
--SELECT (@SQL)




SET @SQL='
INSERT INTO KPI_ENGINE.DBO.CLAIMLINE
(
	   [CLAIM_ID]
      ,[CLAIM_SFX_OR_PARENT]
      ,[SV_LINE]
      ,[FORM_TYPE]
      ,[SV_STAT]
      ,[ADM_DATE]
      ,[ADM_TYPE]
      ,[ADM_SRC]
      ,[DIS_DATE]
      ,[CLIENT_LOS]
      ,[FROM_DATE]
      ,[TO_DATE]
      ,[HOSP_TYPE]
      ,[MEMBER_ID]
      ,[MEMBER_QUAL]
      ,EMPI
	  ,[RELATION]
      ,[GRP_ID]
      ,[CLAIM_REC_DATE]
      ,[CLAIM_ENTRY_DATE]
      ,[PAID_DATE]
      ,[CHK_NUM]
      ,[MS_DRG]
      ,[TRI_DRG]
      ,[AP_DRG]
      ,[APR_DRG]
      ,[APR_DRG_SEV]
      ,[CONT_STAY_ID]
      ,[ATT_PROV]
      ,[ATT_PROV_SPEC]
      ,[ATT_IPA]
      ,[ATT_ACO]
      ,[BILL_PROV]
      ,[REF_PROV]
      ,[ACO_ID]
      ,[MED_HOME_ID]
      ,[CONTRACT]
      ,[BEN_PKG_ID]
      ,[POS]
      ,[UB_BILL_TYPE]
      ,[TOS]  
	  ,[REV_CODE]
      ,[RX_DRUG_COST]
      ,[RX_INGR_COST]
      ,[RX_DISP_FEE]
      ,[RX_DISCOUNT]
      ,[RX_FILL_SRC]
      ,[RX_REFILLS]
      ,[RX_PAR]
      ,[RX_FORM]
      ,[SV_UNITS]
      ,[AMT_BILLED]
      ,[AMT_ALLOWED]
      ,[AMT_ALLOWED_STAT]
      ,[AMT_PAID]
      ,[AMT_HRA]
      ,[AMT_DEDUCT]
      ,[AMT_COINS]
      ,[AMT_COPAY]
      ,[AMT_COB]
      ,[AMT_WITHHOLD]
      ,[AMT_DISALLOWED]
      ,[AMT_MAXIMUM]
      ,[AUTH_ID]
      ,[DIS_STAT]
      ,[BABY_WGHT]
      ,[WEEKS_GEST]
      ,[BABY_CLAIM_ID]
      ,[CL_DATA_SRC]
      ,[CLAIM_IN_NETWORK]
      ,[DRG_TYPE]
      ,[TEST_VALUE]
      ,[TEST_INDICATOR]
      ,[LOINC]
      ,[_KPI_USER_DIM_01_]
      ,[_KPI_USER_DIM_02_]
      ,[_KPI_USER_DIM_03_]
      ,[_KPI_USER_DIM_04_]
      ,[_KPI_USER_DIM_05_]
      ,[_KPI_USER_DIM_06_]
      ,[_KPI_USER_DIM_07_]
      ,[_KPI_USER_DIM_08_]
      ,[_KPI_USER_DIM_09_]
      ,[_KPI_USER_DIM_10_]
      ,[_Claim_UDF_01_]
      ,[_Claim_UDF_02_]
      ,[_Claim_UDF_03_]
      ,[_Claim_UDF_04_]
      ,[_Claim_UDF_05_]
      ,[_Claim_UDF_06_]
      ,[_Claim_UDF_07_]
      ,[_Claim_UDF_08_]
      ,[_Claim_UDF_09_]
      ,[_Claim_UDF_10_]
      ,[_Claim_UDF_11_]
      ,[_Claim_UDF_12_]
      ,[_Claim_UDF_13_]
      ,[_Claim_UDF_14_]
      ,[_Claim_UDF_15_]
      ,[_Claim_UDF_16_]
      ,[_Claim_UDF_17_]
      ,[_Claim_UDF_18_]
      ,[_Claim_UDF_19_]
      ,[_Claim_UDF_20_]
      ,[_Claim_UDF_21_]
      ,[_Claim_UDF_22_]
      ,[_Claim_UDF_23_]
      ,[_Claim_UDF_24_]
      ,[_Claim_UDF_25_]
      ,[_Claim_UDF_26_]
      ,[_Claim_UDF_27_]
      ,[_Claim_UDF_28_]
      ,[_Claim_UDF_29_]
      ,[_Claim_UDF_30_]
      ,[_Claim_UDF_31_]
      ,[_Claim_UDF_32_]
      ,[_Claim_UDF_33_]
      ,[_Claim_UDF_34_]
      ,[_Claim_UDF_35_]
      ,[_Claim_UDF_36_]
      ,[_Claim_UDF_37_]
      ,[_Claim_UDF_38_]
      ,[_Claim_UDF_39_]
      ,[_Claim_UDF_40_]
      ,[_Claim_UDF_41_]
      ,[_Claim_UDF_42_]
      ,[_Claim_UDF_43_]
      ,[_Claim_UDF_44_]
      ,[_Claim_UDF_45_]
      ,[_Claim_UDF_46_]
      ,[_Claim_UDF_47_]
      ,[_Claim_UDF_48_]
      ,[_Claim_UDF_49_]
      ,[_Claim_UDF_50_]
      ,[ATT_PROV_NAME]
      ,[ATT_NPI]
      ,[BILL_PROV_NAME]
      ,[BILL_PROV_TIN]
      ,[REF_PROV_NAME]
      ,[ROOT_COMPANIES_ID]
      ,[Filename]
      ,[Filedate]
      ,[SERV_LOC_NAME]
      ,[SERV_LOC_NPI]
)
SELECT 
	   LTRIM(RTRIM(CL.[CLAIM_ID]))
      ,LTRIM(RTRIM(CL.[CLAIM_SFX_OR_PARENT]))
      ,LTRIM(RTRIM(CL.[SV_LINE]))
      ,LTRIM(RTRIM(CL.[FORM_TYPE]))
      ,LTRIM(RTRIM(CL.[SV_STAT]))
      ,LTRIM(RTRIM(CL.[ADM_DATE]))
      ,LTRIM(RTRIM(CL.[ADM_TYPE]))
      ,LTRIM(RTRIM(CL.[ADM_SRC]))
      ,LTRIM(RTRIM(CL.[DIS_DATE]))
      ,LTRIM(RTRIM(CL.[CLIENT_LOS]))
      ,LTRIM(RTRIM(CL.[FROM_DATE]))
      ,LTRIM(RTRIM(CL.[TO_DATE]))
      ,LTRIM(RTRIM(CL.[HOSP_TYPE]))
      ,LTRIM(RTRIM(CL.[MEMBER_ID]))
      ,LTRIM(RTRIM(CL.[MEMBER_QUAL]))
	  ,LTRIM(RTRIM(EMPI.EMPI_ID))
      ,LTRIM(RTRIM(CL.[RELATION]))
      ,LTRIM(RTRIM(CL.[GRP_ID]))
      ,LTRIM(RTRIM(CL.[CLAIM_REC_DATE]))
      ,LTRIM(RTRIM(CL.[CLAIM_ENTRY_DATE]))
      ,LTRIM(RTRIM(CL.[PAID_DATE]))
      ,LTRIM(RTRIM(CL.[CHK_NUM]))
      ,LTRIM(RTRIM(CL.[MS_DRG]))
      ,LTRIM(RTRIM(CL.[TRI_DRG]))
      ,LTRIM(RTRIM(CL.[AP_DRG]))
      ,LTRIM(RTRIM(CL.[APR_DRG]))
      ,LTRIM(RTRIM(CL.[APR_DRG_SEV]))
      ,LTRIM(RTRIM(CL.[CONT_STAY_ID]))
      ,LTRIM(RTRIM(CL.[ATT_PROV]))
      ,LTRIM(RTRIM(CL.[ATT_PROV_SPEC]))
      ,LTRIM(RTRIM(CL.[ATT_IPA]))
      ,LTRIM(RTRIM(CL.[ATT_ACO]))
      ,LTRIM(RTRIM(CL.[BILL_PROV]))
      ,LTRIM(RTRIM(CL.[REF_PROV]))
      ,LTRIM(RTRIM(CL.[ACO_ID]))
      ,LTRIM(RTRIM(CL.[MED_HOME_ID]))
      ,LTRIM(RTRIM(CL.[CONTRACT]))
      ,LTRIM(RTRIM(CL.[BEN_PKG_ID]))
      ,LTRIM(RTRIM(CL.[POS]))
      ,LTRIM(RTRIM(CL.[UB_BILL_TYPE]))
      ,LTRIM(RTRIM(CL.[TOS]))
      ,LTRIM(RTRIM(CL.[REV_CODE]))
      ,LTRIM(RTRIM(CL.[RX_DRUG_COST]))
      ,LTRIM(RTRIM(CL.[RX_INGR_COST]))
      ,LTRIM(RTRIM(CL.[RX_DISP_FEE]))
      ,LTRIM(RTRIM(CL.[RX_DISCOUNT]))
      ,LTRIM(RTRIM(CL.[RX_FILL_SRC]))
      ,LTRIM(RTRIM(CL.[RX_REFILLS]))
      ,LTRIM(RTRIM(CL.[RX_PAR]))
      ,LTRIM(RTRIM(CL.[RX_FORM]))
      ,LTRIM(RTRIM(CL.[SV_UNITS]))
      ,LTRIM(RTRIM(CL.[AMT_BILLED]))
      ,LTRIM(RTRIM(CL.[AMT_ALLOWED]))
      ,LTRIM(RTRIM(CL.[AMT_ALLOWED_STAT]))
      ,LTRIM(RTRIM(CL.[AMT_PAID]))
      ,LTRIM(RTRIM(CL.[AMT_HRA]))
      ,LTRIM(RTRIM(CL.[AMT_DEDUCT]))
      ,LTRIM(RTRIM(CL.[AMT_COINS]))
      ,LTRIM(RTRIM(CL.[AMT_COPAY]))
      ,LTRIM(RTRIM(CL.[AMT_COB]))
      ,LTRIM(RTRIM(CL.[AMT_WITHHOLD]))
      ,LTRIM(RTRIM(CL.[AMT_DISALLOWED]))
      ,LTRIM(RTRIM(CL.[AMT_MAXIMUM]))
      ,LTRIM(RTRIM(CL.[AUTH_ID]))
      ,LTRIM(RTRIM(CL.[DIS_STAT]))
      ,LTRIM(RTRIM(CL.[BABY_WGHT]))
      ,LTRIM(RTRIM(CL.[WEEKS_GEST]))
      ,LTRIM(RTRIM(CL.[BABY_CLAIM_ID]))
      ,LTRIM(RTRIM(CL.[CL_DATA_SRC]))
      ,LTRIM(RTRIM(CL.[CLAIM_IN_NETWORK]))
      ,LTRIM(RTRIM(CL.[DRG_TYPE]))
      ,LTRIM(RTRIM(CL.[TEST_VALUE]))
      ,LTRIM(RTRIM(CL.[TEST_INDICATOR]))
      ,LTRIM(RTRIM(CL.[LOINC]))
      ,LTRIM(RTRIM(CL.[_KPI_USER_DIM_01_]))
      ,LTRIM(RTRIM(CL.[_KPI_USER_DIM_02_]))
      ,LTRIM(RTRIM(CL.[_KPI_USER_DIM_03_]))
      ,LTRIM(RTRIM(CL.[_KPI_USER_DIM_04_]))
      ,LTRIM(RTRIM(CL.[_KPI_USER_DIM_05_]))
      ,LTRIM(RTRIM(CL.[_KPI_USER_DIM_06_]))
      ,LTRIM(RTRIM(CL.[_KPI_USER_DIM_07_]))
      ,LTRIM(RTRIM(CL.[_KPI_USER_DIM_08_]))
      ,LTRIM(RTRIM(CL.[_KPI_USER_DIM_09_]))
      ,LTRIM(RTRIM(CL.[_KPI_USER_DIM_10_]))
      ,LTRIM(RTRIM(CL.[_Claim_UDF_01_]))
      ,LTRIM(RTRIM(CL.[_Claim_UDF_02_]))
      ,LTRIM(RTRIM(CL.[_Claim_UDF_03_]))
      ,LTRIM(RTRIM(CL.[_Claim_UDF_04_]))
      ,LTRIM(RTRIM(CL.[_Claim_UDF_05_]))
      ,LTRIM(RTRIM(CL.[_Claim_UDF_06_]))
      ,LTRIM(RTRIM(CL.[_Claim_UDF_07_]))
      ,LTRIM(RTRIM(CL.[_Claim_UDF_08_]))
      ,LTRIM(RTRIM(CL.[_Claim_UDF_09_]))
      ,LTRIM(RTRIM(CL.[_Claim_UDF_10_]))
      ,LTRIM(RTRIM(CL.[_Claim_UDF_11_]))
      ,LTRIM(RTRIM(CL.[_Claim_UDF_12_]))
      ,LTRIM(RTRIM(CL.[_Claim_UDF_13_]))
      ,LTRIM(RTRIM(CL.[_Claim_UDF_14_]))
      ,LTRIM(RTRIM(CL.[_Claim_UDF_15_]))
      ,LTRIM(RTRIM(CL.[_Claim_UDF_16_]))
      ,LTRIM(RTRIM(CL.[_Claim_UDF_17_]))
      ,LTRIM(RTRIM(CL.[_Claim_UDF_18_]))
      ,LTRIM(RTRIM(CL.[_Claim_UDF_19_]))
      ,LTRIM(RTRIM(CL.[_Claim_UDF_20_]))
      ,LTRIM(RTRIM(CL.[_Claim_UDF_21_]))
      ,LTRIM(RTRIM(CL.[_Claim_UDF_22_]))
      ,LTRIM(RTRIM(CL.[_Claim_UDF_23_]))
      ,LTRIM(RTRIM(CL.[_Claim_UDF_24_]))
      ,LTRIM(RTRIM(CL.[_Claim_UDF_25_]))
      ,LTRIM(RTRIM(CL.[_Claim_UDF_26_]))
      ,LTRIM(RTRIM(CL.[_Claim_UDF_27_]))
      ,LTRIM(RTRIM(CL.[_Claim_UDF_28_]))
      ,LTRIM(RTRIM(CL.[_Claim_UDF_29_]))
      ,LTRIM(RTRIM(CL.[_Claim_UDF_30_]))
      ,LTRIM(RTRIM(CL.[_Claim_UDF_31_]))
      ,LTRIM(RTRIM(CL.[_Claim_UDF_32_]))
      ,LTRIM(RTRIM(CL.[_Claim_UDF_33_]))
      ,LTRIM(RTRIM(CL.[_Claim_UDF_34_]))
      ,LTRIM(RTRIM(CL.[_Claim_UDF_35_]))
      ,LTRIM(RTRIM(CL.[_Claim_UDF_36_]))
      ,LTRIM(RTRIM(CL.[_Claim_UDF_37_]))
      ,LTRIM(RTRIM(CL.[_Claim_UDF_38_]))
      ,LTRIM(RTRIM(CL.[_Claim_UDF_39_]))
      ,LTRIM(RTRIM(CL.[_Claim_UDF_40_]))
      ,LTRIM(RTRIM(CL.[_Claim_UDF_41_]))
      ,LTRIM(RTRIM(CL.[_Claim_UDF_42_]))
      ,LTRIM(RTRIM(CL.[_Claim_UDF_43_]))
      ,LTRIM(RTRIM(CL.[_Claim_UDF_44_]))
      ,LTRIM(RTRIM(CL.[_Claim_UDF_45_]))
      ,LTRIM(RTRIM(CL.[_Claim_UDF_46_]))
      ,LTRIM(RTRIM(CL.[_Claim_UDF_47_]))
      ,LTRIM(RTRIM(CL.[_Claim_UDF_48_]))
      ,LTRIM(RTRIM(CL.[_Claim_UDF_49_]))
      ,LTRIM(RTRIM(CL.[_Claim_UDF_50_]))
      ,LTRIM(RTRIM(CL.[ATT_PROV_NAME]))
      ,LTRIM(RTRIM(CL.[ATT_NPI]))
      ,LTRIM(RTRIM(CL.[BILL_PROV_NAME]))
      ,LTRIM(RTRIM(CL.[BILL_PROV_TIN]))
      ,LTRIM(RTRIM(CL.[REF_PROV_NAME]))
      ,LTRIM(RTRIM(CL.[ROOT_COMPANIES_ID]))
      ,LTRIM(RTRIM(CL.[Filename]))
      ,LTRIM(RTRIM(CL.[Filedate]))
      ,LTRIM(RTRIM(CL.[SERV_LOC_NAME]))
      ,LTRIM(RTRIM(CL.[SERV_LOC_NPI]))
FROM [KPI_ENGINE_STAGING].[dbo].[STAGING_CLAIMLINE] CL
INNER JOIN KPI_ENGINE.dbo.Open_empi EMPI 
on CL.MEMBER_ID=EMPI.Org_Patient_Extension_ID AND (CL.CL_DATA_SRC=EMPI.[Data_Source] or CL.CL_DATA_SRC in(''FlatFiles-Gaps'',''FlatFiles'')) AND  CL.ROOT_COMPANIES_ID=EMPI.Root_Companies_ID
LEFT JOIN KPI_ENGINE.dbo.[CLAIMLINE] PCL
ON '+@KEYS+
' WHERE PCL.ST_CLAIM_ID IS NULL AND CL.CL_DATA_SRC='''+@PAYER+''' AND CL.Root_Companies_ID='+CONVERT(VARCHAR(10),@ROOT_COMPANIES_ID)


Declare @insertcount INT;
EXECUTE (@SQL);
SET @insertcount=@@ROWCOUNT;
--PRINT (@SQL)
--SELECT (@SQL)


SET @SQL='
INSERT INTO KPI_ENGINE_STAGING.dbo.STAGING_CLAIMLINE_ERROR
(
	   [CLAIM_ID]
      ,[CLAIM_SFX_OR_PARENT]
      ,[SV_LINE]
      ,[FORM_TYPE]
      ,[SV_STAT]
      ,[ADM_DATE]
      ,[ADM_TYPE]
      ,[ADM_SRC]
      ,[DIS_DATE]
      ,[CLIENT_LOS]
      ,[FROM_DATE]
      ,[TO_DATE]
      ,[HOSP_TYPE]
      ,[MEMBER_ID]
      ,[MEMBER_QUAL]
	  ,[RELATION]
      ,[GRP_ID]
      ,[CLAIM_REC_DATE]
      ,[CLAIM_ENTRY_DATE]
      ,[PAID_DATE]
      ,[CHK_NUM]
      ,[MS_DRG]
      ,[TRI_DRG]
      ,[AP_DRG]
      ,[APR_DRG]
      ,[APR_DRG_SEV]
      ,[CONT_STAY_ID]
      ,[ATT_PROV]
      ,[ATT_PROV_SPEC]
      ,[ATT_IPA]
      ,[ATT_ACO]
      ,[BILL_PROV]
      ,[REF_PROV]
      ,[ACO_ID]
      ,[MED_HOME_ID]
      ,[CONTRACT]
      ,[BEN_PKG_ID]
      ,[POS]
      ,[UB_BILL_TYPE]
      ,[TOS]
      ,[REV_CODE]
      ,[RX_DRUG_COST]
      ,[RX_INGR_COST]
      ,[RX_DISP_FEE]
      ,[RX_DISCOUNT]
      ,[RX_FILL_SRC]
      ,[RX_REFILLS]
      ,[RX_PAR]
      ,[RX_FORM]
      ,[SV_UNITS]
      ,[AMT_BILLED]
      ,[AMT_ALLOWED]
      ,[AMT_ALLOWED_STAT]
      ,[AMT_PAID]
      ,[AMT_HRA]
      ,[AMT_DEDUCT]
      ,[AMT_COINS]
      ,[AMT_COPAY]
      ,[AMT_COB]
      ,[AMT_WITHHOLD]
      ,[AMT_DISALLOWED]
      ,[AMT_MAXIMUM]
      ,[AUTH_ID]
      ,[DIS_STAT]
      ,[BABY_WGHT]
      ,[WEEKS_GEST]
      ,[BABY_CLAIM_ID]
      ,[CL_DATA_SRC]
      ,[CLAIM_IN_NETWORK]
      ,[DRG_TYPE]
      ,[TEST_VALUE]
      ,[TEST_INDICATOR]
      ,[LOINC]
      ,[_KPI_USER_DIM_01_]
      ,[_KPI_USER_DIM_02_]
      ,[_KPI_USER_DIM_03_]
      ,[_KPI_USER_DIM_04_]
      ,[_KPI_USER_DIM_05_]
      ,[_KPI_USER_DIM_06_]
      ,[_KPI_USER_DIM_07_]
      ,[_KPI_USER_DIM_08_]
      ,[_KPI_USER_DIM_09_]
      ,[_KPI_USER_DIM_10_]
      ,[_Claim_UDF_01_]
      ,[_Claim_UDF_02_]
      ,[_Claim_UDF_03_]
      ,[_Claim_UDF_04_]
      ,[_Claim_UDF_05_]
      ,[_Claim_UDF_06_]
      ,[_Claim_UDF_07_]
      ,[_Claim_UDF_08_]
      ,[_Claim_UDF_09_]
      ,[_Claim_UDF_10_]
      ,[_Claim_UDF_11_]
      ,[_Claim_UDF_12_]
      ,[_Claim_UDF_13_]
      ,[_Claim_UDF_14_]
      ,[_Claim_UDF_15_]
      ,[_Claim_UDF_16_]
      ,[_Claim_UDF_17_]
      ,[_Claim_UDF_18_]
      ,[_Claim_UDF_19_]
      ,[_Claim_UDF_20_]
      ,[_Claim_UDF_21_]
      ,[_Claim_UDF_22_]
      ,[_Claim_UDF_23_]
      ,[_Claim_UDF_24_]
      ,[_Claim_UDF_25_]
      ,[_Claim_UDF_26_]
      ,[_Claim_UDF_27_]
      ,[_Claim_UDF_28_]
      ,[_Claim_UDF_29_]
      ,[_Claim_UDF_30_]
      ,[_Claim_UDF_31_]
      ,[_Claim_UDF_32_]
      ,[_Claim_UDF_33_]
      ,[_Claim_UDF_34_]
      ,[_Claim_UDF_35_]
      ,[_Claim_UDF_36_]
      ,[_Claim_UDF_37_]
      ,[_Claim_UDF_38_]
      ,[_Claim_UDF_39_]
      ,[_Claim_UDF_40_]
      ,[_Claim_UDF_41_]
      ,[_Claim_UDF_42_]
      ,[_Claim_UDF_43_]
      ,[_Claim_UDF_44_]
      ,[_Claim_UDF_45_]
      ,[_Claim_UDF_46_]
      ,[_Claim_UDF_47_]
      ,[_Claim_UDF_48_]
      ,[_Claim_UDF_49_]
      ,[_Claim_UDF_50_]
      ,[ST_CLAIM_ID]
      ,[ATT_PROV_NAME]
      ,[ATT_NPI]
      ,[BILL_PROV_NAME]
      ,[BILL_PROV_TIN]
      ,[REF_PROV_NAME]
      ,[ROOT_COMPANIES_ID]
      ,[Filename]
      ,[Filedate]
      ,[SERV_LOC_NAME]
      ,[SERV_LOC_NPI]
)
SELECT 
	   LTRIM(RTRIM(CL.[CLAIM_ID]))
      ,LTRIM(RTRIM(CL.[CLAIM_SFX_OR_PARENT]))
      ,LTRIM(RTRIM(CL.[SV_LINE]))
      ,LTRIM(RTRIM(CL.[FORM_TYPE]))
      ,LTRIM(RTRIM(CL.[SV_STAT]))
      ,LTRIM(RTRIM(CL.[ADM_DATE]))
      ,LTRIM(RTRIM(CL.[ADM_TYPE]))
      ,LTRIM(RTRIM(CL.[ADM_SRC]))
      ,LTRIM(RTRIM(CL.[DIS_DATE]))
      ,LTRIM(RTRIM(CL.[CLIENT_LOS]))
      ,LTRIM(RTRIM(CL.[FROM_DATE]))
      ,LTRIM(RTRIM(CL.[TO_DATE]))
      ,LTRIM(RTRIM(CL.[HOSP_TYPE]))
      ,LTRIM(RTRIM(CL.[MEMBER_ID]))
      ,LTRIM(RTRIM(CL.[MEMBER_QUAL]))
      ,LTRIM(RTRIM(CL.[RELATION]))
      ,LTRIM(RTRIM(CL.[GRP_ID]))
      ,LTRIM(RTRIM(CL.[CLAIM_REC_DATE]))
      ,LTRIM(RTRIM(CL.[CLAIM_ENTRY_DATE]))
      ,LTRIM(RTRIM(CL.[PAID_DATE]))
      ,LTRIM(RTRIM(CL.[CHK_NUM]))
      ,LTRIM(RTRIM(CL.[MS_DRG]))
      ,LTRIM(RTRIM(CL.[TRI_DRG]))
      ,LTRIM(RTRIM(CL.[AP_DRG]))
      ,LTRIM(RTRIM(CL.[APR_DRG]))
      ,LTRIM(RTRIM(CL.[APR_DRG_SEV]))
      ,LTRIM(RTRIM(CL.[CONT_STAY_ID]))
      ,LTRIM(RTRIM(CL.[ATT_PROV]))
      ,LTRIM(RTRIM(CL.[ATT_PROV_SPEC]))
      ,LTRIM(RTRIM(CL.[ATT_IPA]))
      ,LTRIM(RTRIM(CL.[ATT_ACO]))
      ,LTRIM(RTRIM(CL.[BILL_PROV]))
      ,LTRIM(RTRIM(CL.[REF_PROV]))
      ,LTRIM(RTRIM(CL.[ACO_ID]))
      ,LTRIM(RTRIM(CL.[MED_HOME_ID]))
      ,LTRIM(RTRIM(CL.[CONTRACT]))
      ,LTRIM(RTRIM(CL.[BEN_PKG_ID]))
      ,LTRIM(RTRIM(CL.[POS]))
      ,LTRIM(RTRIM(CL.[UB_BILL_TYPE]))
      ,LTRIM(RTRIM(CL.[TOS]))
      ,LTRIM(RTRIM(CL.[REV_CODE]))
      ,LTRIM(RTRIM(CL.[RX_DRUG_COST]))
      ,LTRIM(RTRIM(CL.[RX_INGR_COST]))
      ,LTRIM(RTRIM(CL.[RX_DISP_FEE]))
      ,LTRIM(RTRIM(CL.[RX_DISCOUNT]))
      ,LTRIM(RTRIM(CL.[RX_FILL_SRC]))
      ,LTRIM(RTRIM(CL.[RX_REFILLS]))
      ,LTRIM(RTRIM(CL.[RX_PAR]))
      ,LTRIM(RTRIM(CL.[RX_FORM]))
      ,LTRIM(RTRIM(CL.[SV_UNITS]))
      ,LTRIM(RTRIM(CL.[AMT_BILLED]))
      ,LTRIM(RTRIM(CL.[AMT_ALLOWED]))
      ,LTRIM(RTRIM(CL.[AMT_ALLOWED_STAT]))
      ,LTRIM(RTRIM(CL.[AMT_PAID]))
      ,LTRIM(RTRIM(CL.[AMT_HRA]))
      ,LTRIM(RTRIM(CL.[AMT_DEDUCT]))
      ,LTRIM(RTRIM(CL.[AMT_COINS]))
      ,LTRIM(RTRIM(CL.[AMT_COPAY]))
      ,LTRIM(RTRIM(CL.[AMT_COB]))
      ,LTRIM(RTRIM(CL.[AMT_WITHHOLD]))
      ,LTRIM(RTRIM(CL.[AMT_DISALLOWED]))
      ,LTRIM(RTRIM(CL.[AMT_MAXIMUM]))
      ,LTRIM(RTRIM(CL.[AUTH_ID]))
      ,LTRIM(RTRIM(CL.[DIS_STAT]))
      ,LTRIM(RTRIM(CL.[BABY_WGHT]))
      ,LTRIM(RTRIM(CL.[WEEKS_GEST]))
      ,LTRIM(RTRIM(CL.[BABY_CLAIM_ID]))
      ,LTRIM(RTRIM(CL.[CL_DATA_SRC]))
      ,LTRIM(RTRIM(CL.[CLAIM_IN_NETWORK]))
      ,LTRIM(RTRIM(CL.[DRG_TYPE]))
      ,LTRIM(RTRIM(CL.[TEST_VALUE]))
      ,LTRIM(RTRIM(CL.[TEST_INDICATOR]))
      ,LTRIM(RTRIM(CL.[LOINC]))
      ,LTRIM(RTRIM(CL.[_KPI_USER_DIM_01_]))
      ,LTRIM(RTRIM(CL.[_KPI_USER_DIM_02_]))
      ,LTRIM(RTRIM(CL.[_KPI_USER_DIM_03_]))
      ,LTRIM(RTRIM(CL.[_KPI_USER_DIM_04_]))
      ,LTRIM(RTRIM(CL.[_KPI_USER_DIM_05_]))
      ,LTRIM(RTRIM(CL.[_KPI_USER_DIM_06_]))
      ,LTRIM(RTRIM(CL.[_KPI_USER_DIM_07_]))
      ,LTRIM(RTRIM(CL.[_KPI_USER_DIM_08_]))
      ,LTRIM(RTRIM(CL.[_KPI_USER_DIM_09_]))
      ,LTRIM(RTRIM(CL.[_KPI_USER_DIM_10_]))
      ,LTRIM(RTRIM(CL.[_Claim_UDF_01_]))
      ,LTRIM(RTRIM(CL.[_Claim_UDF_02_]))
      ,LTRIM(RTRIM(CL.[_Claim_UDF_03_]))
      ,LTRIM(RTRIM(CL.[_Claim_UDF_04_]))
      ,LTRIM(RTRIM(CL.[_Claim_UDF_05_]))
      ,LTRIM(RTRIM(CL.[_Claim_UDF_06_]))
      ,LTRIM(RTRIM(CL.[_Claim_UDF_07_]))
      ,LTRIM(RTRIM(CL.[_Claim_UDF_08_]))
      ,LTRIM(RTRIM(CL.[_Claim_UDF_09_]))
      ,LTRIM(RTRIM(CL.[_Claim_UDF_10_]))
      ,LTRIM(RTRIM(CL.[_Claim_UDF_11_]))
      ,LTRIM(RTRIM(CL.[_Claim_UDF_12_]))
      ,LTRIM(RTRIM(CL.[_Claim_UDF_13_]))
      ,LTRIM(RTRIM(CL.[_Claim_UDF_14_]))
      ,LTRIM(RTRIM(CL.[_Claim_UDF_15_]))
      ,LTRIM(RTRIM(CL.[_Claim_UDF_16_]))
      ,LTRIM(RTRIM(CL.[_Claim_UDF_17_]))
      ,LTRIM(RTRIM(CL.[_Claim_UDF_18_]))
      ,LTRIM(RTRIM(CL.[_Claim_UDF_19_]))
      ,LTRIM(RTRIM(CL.[_Claim_UDF_20_]))
      ,LTRIM(RTRIM(CL.[_Claim_UDF_21_]))
      ,LTRIM(RTRIM(CL.[_Claim_UDF_22_]))
      ,LTRIM(RTRIM(CL.[_Claim_UDF_23_]))
      ,LTRIM(RTRIM(CL.[_Claim_UDF_24_]))
      ,LTRIM(RTRIM(CL.[_Claim_UDF_25_]))
      ,LTRIM(RTRIM(CL.[_Claim_UDF_26_]))
      ,LTRIM(RTRIM(CL.[_Claim_UDF_27_]))
      ,LTRIM(RTRIM(CL.[_Claim_UDF_28_]))
      ,LTRIM(RTRIM(CL.[_Claim_UDF_29_]))
      ,LTRIM(RTRIM(CL.[_Claim_UDF_30_]))
      ,LTRIM(RTRIM(CL.[_Claim_UDF_31_]))
      ,LTRIM(RTRIM(CL.[_Claim_UDF_32_]))
      ,LTRIM(RTRIM(CL.[_Claim_UDF_33_]))
      ,LTRIM(RTRIM(CL.[_Claim_UDF_34_]))
      ,LTRIM(RTRIM(CL.[_Claim_UDF_35_]))
      ,LTRIM(RTRIM(CL.[_Claim_UDF_36_]))
      ,LTRIM(RTRIM(CL.[_Claim_UDF_37_]))
      ,LTRIM(RTRIM(CL.[_Claim_UDF_38_]))
      ,LTRIM(RTRIM(CL.[_Claim_UDF_39_]))
      ,LTRIM(RTRIM(CL.[_Claim_UDF_40_]))
      ,LTRIM(RTRIM(CL.[_Claim_UDF_41_]))
      ,LTRIM(RTRIM(CL.[_Claim_UDF_42_]))
      ,LTRIM(RTRIM(CL.[_Claim_UDF_43_]))
      ,LTRIM(RTRIM(CL.[_Claim_UDF_44_]))
      ,LTRIM(RTRIM(CL.[_Claim_UDF_45_]))
      ,LTRIM(RTRIM(CL.[_Claim_UDF_46_]))
      ,LTRIM(RTRIM(CL.[_Claim_UDF_47_]))
      ,LTRIM(RTRIM(CL.[_Claim_UDF_48_]))
      ,LTRIM(RTRIM(CL.[_Claim_UDF_49_]))
      ,LTRIM(RTRIM(CL.[_Claim_UDF_50_]))
      ,LTRIM(RTRIM(CL.[ST_CLAIM_ID]))
      ,LTRIM(RTRIM(CL.[ATT_PROV_NAME]))
      ,LTRIM(RTRIM(CL.[ATT_NPI]))
      ,LTRIM(RTRIM(CL.[BILL_PROV_NAME]))
      ,LTRIM(RTRIM(CL.[BILL_PROV_TIN]))
      ,LTRIM(RTRIM(CL.[REF_PROV_NAME]))
      ,LTRIM(RTRIM(CL.[ROOT_COMPANIES_ID]))
      ,LTRIM(RTRIM(CL.[Filename]))
      ,LTRIM(RTRIM(CL.[Filedate]))
      ,LTRIM(RTRIM(CL.[SERV_LOC_NAME]))
      ,LTRIM(RTRIM(CL.[SERV_LOC_NPI]))
FROM [KPI_ENGINE_STAGING].[dbo].[STAGING_CLAIMLINE] CL
LEFT JOIN KPI_ENGINE.dbo.Open_empi EMPI 
on CL.MEMBER_ID=EMPI.Org_Patient_Extension_ID AND (CL.CL_DATA_SRC=EMPI.[Data_Source] or CL.CL_DATA_SRC in(''FlatFiles-Gaps'',''FlatFiles'')) AND  CL.ROOT_COMPANIES_ID=EMPI.Root_Companies_ID
LEFT JOIN KPI_ENGINE_STAGING.dbo.STAGING_CLAIMLINE_ERROR PCL
ON '+@KEYS+
' WHERE PCL.ST_CLAIM_ID IS NULL AND EMPI.EMPI_ID IS NULL AND CL.CL_DATA_SRC='''+@PAYER+''' AND CL.Root_Companies_ID='+CONVERT(VARCHAR(10),@ROOT_COMPANIES_ID)


Declare @errorcount INT;
EXECUTE (@SQL);
SET @errorcount=@@ROWCOUNT;

-- Shanawaz 10-21-2021 -- Adding this  for data flow trace
Insert into KPI_ENGINE_RECON.dbo.PROD_INGESTION_STATS(Dimension,Source,UpdateCount,InsertCount,ErrorCount) values(@PROD_TABLE,@PAYER,@updatecount,@insertcount,@errorcount);


-- Shanawaz - Adding logic to store max dates from Claims
Truncate table KPI_ENGINE_MART.RPT.MAXCLAIMDATES;
Insert into KPI_ENGINE_MART.RPT.MAXCLAIMDATES(Source,MaxClaimDate)
select 
	CL_DATA_SRC as Payer,
	max(FROM_DATE) as [Max Claim Date]
From CLAIMLINE
Group By CL_DATA_SRC


FETCH NEXT FROM C INTO @PROD_TABLE,@PAYER,@ROOT_COMPANIES_ID,@KEYS
END 

CLOSE C  
DEALLOCATE C
	
		SET @PERF_ROW = @@ROWCOUNT
		SET @PERF_DURATION = DATEDIFF(MINUTE,@PERF_START,GETDATE());
		EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, 'Enrollment Completed', @PERF_ROW, @DURATION_IN_MIN=@PERF_DURATION, @LOG2ID=@LOGID;
		EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, @LOG2ID=@LOGID, @END_FLAG=1;

END TRY
BEGIN CATCH  
    DECLARE @ErrorMessage NVARCHAR(4000);  
    DECLARE @ErrorSeverity INT;  
    DECLARE @ErrorState INT;   

	EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, 'FATAL ERROR', @LOG2ID=@LOGID;
  
    SELECT   
        @ErrorMessage = ERROR_MESSAGE(),  
        @ErrorSeverity = ERROR_SEVERITY(),  
        @ErrorState = ERROR_STATE();  
  
    -- Use RAISERROR inside the CATCH block to return error  
    -- information about the original error that caused  
    -- execution to jump to the CATCH block.  
    RAISERROR (@ErrorMessage, -- Message text.  
               @ErrorSeverity, -- Severity.  
               @ErrorState -- State.  
               );  
END CATCH;  

END
GO
