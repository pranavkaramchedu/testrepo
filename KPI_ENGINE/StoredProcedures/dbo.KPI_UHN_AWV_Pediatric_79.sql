SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON



CREATE proc [dbo].[KPI_UHN_AWV_Pediatric_79]
AS

BEGIN
DECLARE @INFO VARCHAR(8000)
					,	@TRUE BIT = 1
					,	@FALSE BIT = 0
					,	@DB_ID INT = DB_ID()
					,	@LOGID INT = 0
					,	@PROC VARCHAR(500)=OBJECT_SCHEMA_NAME(@@PROCID)+'.'+OBJECT_NAME(@@PROCID)
					,	@PERF_START DATETIME
					,	@PERF_DURATION INT
					,	@PERF_ROW INT
					,	@RC	INT
					;

		EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, @INFO=@INFO, @LOG2ID=@LOGID OUT;
		DECLARE @PROCFULLNAME VARCHAR(500) = DB_NAME()+'.'+@PROC;
		----EXEC dbo.SP_MI_UTIL_SEND_MAIL @PROCFULLNAME, 'STARTED', 1, @ECHO=0; 
	DECLARE @MSG VARCHAR(1000)
	DECLARE @PERF_START_PROC DATETIME 
	DECLARE @PERF_RPS NUMERIC(10,2) 
	DECLARE @ROWS INT 
	DECLARE @PERF_ROWS INT 
	SET @PERF_ROWS = 0 
	SET @PERF_START = GETDATE() 
	SET @PERF_START_PROC = GETDATE() 

BEGIN TRY




-- Declare Variables
declare @rundate Date=GetDate();
declare @meas_year varchar(4)=Year(Dateadd(month,-2,@rundate))
declare @rootId INT=159
declare @attributiondate Date=DATEADD(month, DATEDIFF(month, 0, Dateadd(month,-2,@rundate)), 0)

Declare @startDate Date;
Declare @enddate date;
Declare @quarter varchar(20)
Declare @reportId INT;
Declare @measure_id varchar(10);
Declare @target float;
Declare @domain varchar(100);
Declare @subdomain varchar(100);
Declare @measuretype varchar(100);
Declare @measurename varchar(100);
Declare @reporttype varchar(100);

Declare @ce_startdt Date;
Declare @ce_startdt1 Date;
Declare @ce_novdt Date;
Declare @ce_enddt Date;
Declare @ce_enddt1 Date;

-- Set Measure dates
SET @ce_startdt=concat(@meas_year,'-01-01');
SET @ce_novdt=concat(@meas_year,'-11-30');
SET @ce_enddt=concat(@meas_year,'-12-31');
SET @ce_startdt1=concat(@meas_year-1,'-01-01');
SET @ce_enddt1=concat(@meas_year-1,'-12-31');


Set @reporttype='Physician'
Set @measurename='Adolescent Well Child Visit for patients 12 - 21'
--Set @startDate=DATEADD(yy, DATEDIFF(yy, 0,Dateadd(month,-2,GETDATE())), 0) 
--Set @enddate=eomonth(Dateadd(month,-2,GetDate()))
--Set @quarter=Concat(Year(@enddate),' - Q',DATEPART(q, @enddate))
set @target=46.5
Set @domain='Clinical Quality / Wellness and Prevention'
Set @subdomain='Pediatric Wellness and Prevention'
Set @measuretype='UHN'
Set @measure_id=79


-- Identify Dead Members
Drop table if exists #79_deceasedmembers
Create table #79_deceasedmembers
(
	EMPI varchar(100)
)
Insert into #79_deceasedmembers
select * from deceasedmembers(@rootId,@ce_startdt,@ce_enddt)



-- Identify members who are between 12 and 21
drop table if exists #79_denominatorset;
Create table #79_denominatorset
(
	EMPI varchar(100),
	MEMBER_ID varchar(100),
	Gender varchar(10),
	Age INT
)
Insert into #79_denominatorset
Select * from(
select distinct
	o.EMPI_ID
	,Org_Patient_Extension_ID as MEMBER_ID
	,Gender
	,CASE 
		WHEN dateadd(year, datediff (year, Date_of_Birth,eomonth(@ce_enddt)), Date_of_Birth) > eomonth(@ce_enddt) THEN datediff(year, Date_of_Birth, eomonth(@ce_enddt)) - 1
		ELSE datediff(year, Date_of_Birth, eomonth(@ce_enddt))
	END as Age
from open_empi_master o
join CLAIMLINE c on o.EMPI_ID=c.EMPI and 
					c.FROM_DATE between @ce_startdt and @ce_enddt and
					o.Root_Companies_ID=c.ROOT_COMPANIES_ID
left outer join #79_deceasedmembers d on EMPI_ID=d.EMPI
where o.Root_Companies_ID=@rootId and
	  d.EMPI is null
)t1 
where age between 12 and 21

/*
--Identify members with Hospice exclusion
drop table if exists #20_exclusions;
CREATE table #20_exclusions
(
	EMPI varchar(50)
		
);
Insert into #20_exclusions
Select distinct
	EMPI
From
(
	select EMPI from hospicemembers(@rootId,@ce_startdt,@ce_enddt)

	Union all

	Select 
		p.EMPI 
	from KPI_ENGINE.dbo.PROCEDURES p
	left outer join CLAIMLINE c on p.CLAIM_ID=c.CLAIM_ID and 
								   p.SV_LINE=c.SV_LINE and 
								   p.PROC_DATA_SRC=c.CL_DATA_SRC and 
								   p.ROOT_COMPANIES_ID=c.ROOT_COMPANIES_ID 
	where 
		p.ROOT_COMPANIES_ID=@rootId and 
		c.POS!='81' and
		PROC_START_DATE!='' and 
		PROC_START_DATE between @ce_startdt and @ce_enddt and 
		PROC_CODE in('G9904')
)t1

*/

-- Identify Numerator Set
Drop table if exists #79_numeratorset;
Create table #79_numeratorset
(
	EMPI varchar(100),
	ServiceDate Date,
	Code varchar(20)
)
Insert into #79_numeratorset
Select distinct 
	EMPI 
	,ServiceDate
	,Code
from
(

	Select 
		EMPI
		,PROC_START_DATE as ServiceDate
		,PROC_CODE as Code
	from GetProcedures(@rootId,@ce_startdt,@ce_enddt,'Well-Care')

	Union all

	Select 
		EMPI
		,DIAG_START_DATE as ServiceDate
		,DIAG_CODE as Code
	from GetDiagnosis(@rootId,@ce_startdt,@ce_enddt,'Well-Care')

	
	
)t1


-- Get Numerator Details
Drop table if exists #79_num_detail
Create table #79_num_detail
(
	EMPI varchar(100),
	ServiceDate Date,
	Code varchar(50)
)
Insert into #79_num_detail
Select
	EMPI
	,ServiceDate
	,Code
From
(
	Select
		*
		,row_number() over(partition by EMPI order by ServiceDate Desc) as rnk
	From #79_numeratorset
	
)t3
Where rnk=1




-- Get ReportId from Report_Details

	exec GetReportDetail @rundate=@rundate,@rootId=@rootId,@startDate=@startDate output,@enddate=@enddate output,@quarter=@quarter output,@reportId=@reportId output




-- Create the output as required
Drop table if exists #79_dataset
Create Table #79_dataset
(
	EMPI varchar(100),
	Provider_Id varchar(20),
	PCP_NPI varchar(20),
	PCP_NAME varchar(200),
	Practice_Name varchar(200),
	Specialty varchar(100),
	Measure_id varchar(20),
	Measure_Name varchar(100),
	Payer varchar(50),
	PayerId varchar(100),
	MEM_FNAME varchar(100),
	MEM_MName varchar(50),
	MEM_LNAME varchar(100),
	MEM_DOB Date,
	MEM_GENDER varchar(20),
	Enrollment_Status varchar(20),
	Last_visit_date Date,
	Product_Type varchar(50),
	Num bit,
	Den bit,
	Excl bit,
	Rexcl bit,
	Report_Id INT,
	ReportType varchar(20),
	Report_Quarter varchar(20),
	Period_Start_Date Date,
	Period_End_Date Date,
	Root_Companies_Id INT

)
Insert into #79_dataset
select distinct
	d.EMPI
	,a.AmbulatoryPCPNPI as Provider_Id 
	,a.AmbulatoryPCPNPI as PCP_NPI
	,a.AmbulatoryPCPName as PCP_NAME
	,a.AmbulatoryPCPPractice as Practice_Name
	,a.AmbulatoryPCPSpecialty as Specialty
	,@measure_id as Measure_id
	,@measurename as Measure_Name
	,a.DATA_SOURCE as Payer
	,a.PayerId
	,a.MemberFirstName as MEM_FNAME
	,a.MemberMiddleName as MEM_MName
	,a.MemberLastName as MEM_LNAME
	,a.MemberDOB
	,m.Gender as MEM_GENDER
	,a.EnrollmentStatus
	,a.AmbulatoryPCPRecentVisit as Last_visit_date
	,mm.PAYER_TYPE as ProductType
	,0 as Num
	,1 as Den
	,0 as Excl
	,0 as Rexcl
	,@reportId
	,@reporttype as ReportType
	,@quarter as Report_quarter
	,@startDate as Period_start_date
	,@enddate as Period_end_date
	,@rootId
from #79_denominatorset d
join RPT.PCP_ATTRIBUTION a on d.EMPI=a.EMPI and a.ReportId=@reportId
Join open_empi_master m on d.EMPI=m.EMPI_ID
left outer join MEMBER_MONTH mm on d.EMPI=mm.EMPI and mm.MEMBER_MONTH_START_DATE=@attributiondate
where a.AssignedStatus='Assigned' 
--and	  a.AmbulatoryPCPSpecialty in('Family Medicine','Pediatrics')


update ds Set num=1 from #79_dataset ds join #79_numeratorset n on ds.EMPI=n.EMPI
--update ds Set Rexcl=1 from #79_dataset ds join #20_exclusions n on ds.EMPI=n.EMPI



-- Insert data into Measure Detailed Line
Delete from RPT.MEASURE_DETAILED_LINE where MEASURE_ID=@measure_id and REPORT_ID=@reportId and ROOT_COMPANIES_ID=@rootId;

Insert into RPT.MEASURE_DETAILED_LINE(Provider_Id,PCP_NPI,PCP_NAME,Practice_Name,Specialty,Measure_id,Measure_Name,Payer,PayerId,MEM_FNAME,MEM_MName,MEM_LNAME,MEM_DOB,MEM_GENDER,ENROLLMENT_STATUS,Last_visit_date,Product_Type,Num,Den,Excl,Rexcl,Report_Id,ReportType,Report_Quarter,Period_Start_date,Period_end_Date,Root_Companies_id,EMPI,MEASURE_TYPE,Code,DateofService)
Select Provider_Id,PCP_NPI,PCP_NAME,Practice_Name,Specialty,Measure_id,Measure_Name,Payer,PayerId,MEM_FNAME,MEM_MName,MEM_LNAME,MEM_DOB,MEM_GENDER,Enrollment_Status,Last_visit_date,Product_Type,Num,Den,Excl,Rexcl,Report_Id,ReportType,Report_Quarter,Period_Start_date,Period_end_Date,Root_Companies_id,d.EMPI,@measuretype,Code,ServiceDate
From #79_dataset d
Left outer join #79_num_detail nd on d.EMPI=nd.EMPI
--where Specialty in('General Practice','Family Medicine','Hospitalists','Internal Medicine','Pediatrics')




-- Insert data into Provider Scorecard
Delete from RPT.PROVIDER_SCORECARD  where MEASURE_ID=@measure_id and REPORT_ID=@reportId and ROOT_COMPANIES_ID=@rootId;

Insert into RPT.PROVIDER_SCORECARD(Provider_Id,PCP_NPI,PCP_NAME,Specialty,Practice_Name,Measure_id,Measure_Name,Measure_Title,MEASURE_SUBTITLE,Measure_Type,NUM_COUNT,DEN_COUNT,Excl_Count,Rexcl_Count,Gaps,Result,Target,To_Target,Report_Id,ReportType,Report_Quarter,Period_Start_Date,Period_End_Date,Root_Companies_Id)
Select 
	Provider_Id
	,PCP_NPI
	,PCP_NAME
	,Specialty
	,Practice_Name
	,Measure_id
	,Measure_Name
	,Measure_Title
	,MEASURE_SUBTITLE
	,Measure_Type
	,SUM(Cast(NUM_COUNT as INT)) as NUM_COUNT
	,SUM(Cast(DEN_COUNT as INT)) as DEN_COUNT
	,SUM(Cast(Excl_Count as INT)) as Excl_Count
	,Sum(Cast(Rexcl_count as INT)) as Rexcl_Count
	,sum(Cast(DEN_Excl as INT)) - SUM(Cast(NUM_COUNT as INT)) as Gaps
	,Case
		when SUM(Cast(DEN_Excl as Float))>0 Then Round((SUM(cast(NUM_COUNT as Float))/ISNULL(NULLIF(SUM(Cast(DEN_Excl as Float)),0),1))*100,2)
		else 0
	end as Result
	,@target as Target
	,Case	
		when ((SUM(Cast(DEN_Excl as INT)))*(cast(@target*0.01 as float))) - SUM(Cast(NUM_COUNT as INT))>0 Then CEILING(((SUM(Cast(DEN_Excl as INT)))*(cast(@target*0.01 as float))) - SUM(Cast(NUM_COUNT as INT)))
		Else 0
	end as To_Target
	,Report_Id
	,ReportType
	,Report_Quarter
	,Period_Start_Date
	,Period_End_Date
	,Root_Companies_Id
From
(
	Select distinct
		m.EMPI
		,a.NPI as Provider_id
		,a.NPI as PCP_NPI
		,a.Prov_Name as PCP_Name
		,a.Specialty
		,a.Practice as Practice_Name
		,m.Measure_id
		,m.Measure_Name
		,l.measure_Title
		,l.Measure_SubTitle
		,'Calculated' as Measure_Type
		,Case
			when NUM=1 and excl=0 and rexcl=0 Then 1
			else 0
		end as NUM_COUNT
		,DEN as DEN_COUNT
		,Case
			When DEN=1 and Excl=0 and Rexcl=0 Then 1
			else 0
		End as Den_excl
		,Excl as Excl_Count
		,Rexcl as Rexcl_count
		,Report_Id
		,l.ReportType
		,Report_Quarter
		,Period_Start_Date
		,Period_End_Date
		,m.Root_Companies_Id
		,l.Target
	From RPT.MEASURE_DETAILED_LINE m
	Join RPT.ConsolidatedAttribution_Snapshot a on
		m.EMPI=a.EMPI and a.Attribution_type='Ambulatory_PCP'
	Join RFT.UHN_measuresList l on
		m.Measure_Id=l.measure_id
	where Enrollment_Status='Active' and
		  a.NPI!='' and
		  m.MEASURE_ID=@measure_id and
		  REPORT_ID=@reportId 
)t1
Group by Provider_Id,PCP_NPI,PCP_NAME,Specialty,Practice_Name,Measure_id,Measure_Name,Measure_Title,MEASURE_SUBTITLE,Report_Id,ReportType,Report_Quarter,Period_Start_Date,Period_End_Date,Root_Companies_Id,Measure_Type,Target



		SET @PERF_ROW = @@ROWCOUNT
		SET @PERF_DURATION = DATEDIFF(MINUTE,@PERF_START,GETDATE());
		EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, 'Enrollment Completed', @PERF_ROW, @DURATION_IN_MIN=@PERF_DURATION, @LOG2ID=@LOGID;
		EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, @LOG2ID=@LOGID, @END_FLAG=1;

END TRY
BEGIN CATCH
		BEGIN
			EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, 'FATAL ERROR', @LOG2ID=@LOGID;
			RAISERROR ('',16,0)
			RETURN 16
        END
	END CATCH
RETURN 0
END

GO
