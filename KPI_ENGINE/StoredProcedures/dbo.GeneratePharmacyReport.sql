SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
CREATE Proc [dbo].[GeneratePharmacyReport]
As

BEGIN
DECLARE @INFO VARCHAR(8000)
					,	@TRUE BIT = 1
					,	@FALSE BIT = 0
					,	@DB_ID INT = DB_ID()
					,	@LOGID INT = 0
					,	@PROC VARCHAR(500)=OBJECT_SCHEMA_NAME(@@PROCID)+'.'+OBJECT_NAME(@@PROCID)
					,	@PERF_START DATETIME
					,	@PERF_DURATION INT
					,	@PERF_ROW INT
					,	@RC	INT
					;

		EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, @INFO=@INFO, @LOG2ID=@LOGID OUT;
		DECLARE @PROCFULLNAME VARCHAR(500) = DB_NAME()+'.'+@PROC;
		----EXEC dbo.SP_MI_UTIL_SEND_MAIL @PROCFULLNAME, 'STARTED', 1, @ECHO=0; 
	DECLARE @MSG VARCHAR(1000)
	DECLARE @PERF_START_PROC DATETIME 
	DECLARE @PERF_RPS NUMERIC(10,2) 
	DECLARE @ROWS INT 
	DECLARE @PERF_ROWS INT 
	SET @PERF_ROWS = 0 
	SET @PERF_START = GETDATE() 
	SET @PERF_START_PROC = GETDATE() 

BEGIN TRY



Declare @rundate date=GetDate()
declare @meas_year varchar(4)=Year(Dateadd(month,-2,@rundate))
declare @rootId INT=159;
Declare @startDate Date;
Declare @enddate date;
Declare @quarter varchar(20);
Declare @reportId INT;
Declare @medstartdate Date;
Declare @medenddate Date;

Set @medstartdate=concat(@meas_year-2,'-01-01');
Set @medenddate=concat(@meas_year,'-12-31');

-- Update Medication Flag

Update	ndc
	Set ndc.GenericFlag=Case When rx.TTY in('GPCK','SCD') Then 1 When rx.TTY in('BPCK','SBD') Then 2 else 4 end
From RFT.ndccodes ndc
Join RFT.rxnorm_to_ndc rx2ndc on
	ndc.NDC=rx2ndc.ATV
Join RFT.rxnorm rx on
	rx2ndc.RXAUI=rx.RXAUI

	

-- Get ReportId
exec GetReportDetail @rundate=@rundate,@rootId=@rootId,@startDate=@startDate output,@enddate=@enddate output,@quarter=@quarter output,@reportId=@reportId output


-- Insert Part B Drugs
Truncate Table KPI_ENGINE_MART.RPT.PartBDrugs;

Insert into KPI_ENGINE_MART.RPT.PartBDrugs(EMPI,MedicationDate,Code,Description,Strength,AttendingProviderNPI,AttendingProvider,AMT_Billed,Quantity,PrimaryDiagnosis,ROOT_COMPANIES_ID,ReportId)
Select 
	EMPI, 
	PROC_START_DATE,
	PROC_CODE,
	Definition,
	Dosage,
	ATT_NPI,
	AttendingProvider,
	AMT_PAID as AMT_BILLED,
	Quantity,
	PrimaryDiagnosis,
	@rootId,
	@reportId
from(
select 
	p.EMPI,
	p.PROC_START_DATE,
	p.PROC_CODE,
	rv.Definition,
	rv.Dosage,
	c.ATT_NPI,
	UPPER(coalesce(NULLIF(concat(Provider_Last_Name,' ',Provider_First_Name),' '),Provider_Organization_Name,c.ATT_PROV_NAME)) as AttendingProvider,
	rv.Amount,
	c.AMT_PAID,
	case
		when PROC_DATA_SRC='Humana' Then p.SERVICE_UNIT
		when PROC_DATA_SRC='MSSP' Then c.SV_UNITS
	end as Quantity,
	d.DIAG_Code as PrimaryDiagnosis,
	CL_DATA_SRC,
	ROW_NUMBER() over(Partition By p.EMPI,PROC_START_DATE,PROC_CODE order by Provider_Last_Name desc,ST_PROC_ID Desc) as rn
From PROCEDURES p
Join CLAIMLINE c   on
	p.EMPI=c.EMPI and
	p.CLAIM_ID=c.CLAIM_ID and
	ISNULL(p.SV_LINE,0)=ISNULL(c.SV_LINE,0) and
	p.ROOT_COMPANIES_ID=c.ROOT_COMPANIES_ID and
	p.PROC_DATA_SRC=c.CL_DATA_SRC
Join REG.REGISTRY_VALUESET rv on p.PROC_CODE=rv.Code and rv.ValueSetName='Part B Drugs'
Left Outer Join RFT_NPI n on c.ATT_NPI=n.NPI 
Left Outer Join DIAGNOSIS d on
	p.EMPI=d.EMPI and
	p.CLAIM_ID=d.CLAIM_ID and
	p.ROOT_COMPANIES_ID=d.ROOT_COMPANIES_ID and
	p.PROC_DATA_SRC=d.DIAG_DATA_SRC
	and d.DIAG_SEQ_NO=1
Where
	p.ROOT_COMPANIES_ID=@rootId and
	p.PROC_START_DATE between @medstartdate and @medenddate
)t1
Where rn=1


Truncate Table KPI_ENGINE_MART.RPT.PharmacyUtilization;

Insert into KPI_ENGINE_MART.RPT.PharmacyUtilization(EMPI,FILL_DATE,MedicationName,MEDICATION_CODE,DaysOfSupply,GenericFlag,Formulary,PharmacyName,PrescribingProvider,PrescriptionDate,MedicationStrength,MedicationDose,Quantity,PrimaryDiagnosis,TherapeuticClass,AMT_BILLED,ReportId,ROOT_COMPANIES_ID,ReportStartDate,ReportEndDate,ReportQuarter)
Select
	EMPI,FILL_DATE,MedicationName,MEDICATION_CODE,DaysOfSupply,GenericFlag,Formulary,PharmacyName,PrescribingProvider,PrescriptionDate,MedicationStrength,MedicationDose,Quantity,PrimaryDiagnosis,TherapeuticClass,AMT_BILLED,ReportId,ROOT_COMPANIES_ID,ReportStartDate,ReportEndDate,ReportQuarter
From
(
	Select 
		Row_number() over (partition by FILL_DATE,EMPI,MEDICATION_CODE,DaysOfSupply order by EMPI) as rn,
		EMPI,
		FILL_DATE,
		MedicationName,
		MEDICATION_CODE,
		DaysOfSupply,
		GenericFlag,
		Formulary,
		PharmacyName,
		PrescribingProvider,
		PrescriptionDate,
		MedicationStrength,
		MedicationDose,
		ISNULL(Quantity,'') as Quantity,
		PrimaryDiagnosis,
		TherapeuticClass,
		AMT_BILLED
		,@reportId as ReportId
		,@rootId as ROOT_COMPANIES_ID
		,@startDate as ReportStartDate
		,@endDate as ReportEndDate
		,@quarter as ReportQuarter
From
(

	Select
		
		EMPI,
		FILL_DATE,
		MedicationName,
		MEDICATION_CODE,
		sum(DaysOfSupply) as DaysOfSupply,
		GenericFlag,
		Formulary,
		PharmacyName,
		PrescribingProvider,
		PrescriptionDate,
		MedicationStrength,
		MedicationDose,
		sum(Quantity) as Quantity,
		PrimaryDiagnosis,
		TherapeuticClass,
		sum(AMT_BILLED) as AMT_BILLED
	From
	(
		select distinct
			m.CLAIM_ID,
			m.EMPI
			,m.FILL_DATE
			,UPPER(coalesce(ndc.BrandName,ndc.GenericName,m.MEDICATION_NAME)) as MedicationName
			,MEDICATION_CODE
			,Cast(Cast(SUPPLY_DAYS as Numeric) as INT) as DaysOfSupply
			,Case
				When MED_TYPE=1 or ndc.GenericFlag=1 Then 'Generic'
				When MED_TYPE=2 or ndc.GenericFlag=2 Then 'Brand'
				WHen MED_TYPE=3 Then 'Others'
				else 'UNK'
			End as GenericFlag
			,Case
				When MED_COVERAGE=1 Then 'Y'
				When MED_COVERAGE=0 Then 'N'
				else 'UNK'
			end as Formulary
			,coalesce(n.Provider_Organization_Name,PHARM_NAME) as PharmacyName
			,ISNULL(coalesce(NULLIF(concat(prs.Provider_Last_Name,',',prs.Provider_First_Name),','),NULLIF(concat(att.Provider_Last_Name,',',att.Provider_First_Name),','),PRESC_NAME),'') as PrescribingProvider
			,ISNULL(NULLIF(PRESCRIPTION_DATE,'1900-01-01'),'') as PrescriptionDate
			,coalesce(Concat(ndc.ACTIVE_NUMERATOR_STRENGTH,' ',ACTIVE_INGRED_UNIT),m.MED_STRENGTH) as MedicationStrength
			,ISNULL(MED_DOSE,'') as MedicationDose
			,cast(QUANTITY as float) as Quantity
			,ISNULL(d.DIAG_CODE,'') as PrimaryDiagnosis
			,ndc.TherapeuticClass
			,c.AMT_BILLED
			,c.PAID_DATE
			,c.SV_STAT
			,c.SV_LINE
		from MEDICATION m
		join CLAIMLINE c on
			m.CLAIM_ID=c.CLAIM_ID and
			m.MED_DATA_SRC=c.CL_DATA_SRC and 
			m.ROOT_COMPANIES_ID=c.ROOT_COMPANIES_ID
		Left outer join RFT.ndccodes ndc on
			m.Medication_code=ndc.NDC
		Left outer Join RFT_NPI n on
			m.PHARM_ID=n.NPI
		Left outer Join RFT_NPI prs on
			m.PRESC_NPI=prs.NPI and prs.Entity_Type_Code=1
		Left outer Join RFT_NPI att on
			c.ATT_NPI=att.NPI and att.Entity_Type_Code=1
		Left outer Join diagnosis d	on 
			m.CLAIM_ID=d.CLAIM_ID and
			m.MED_DATA_SRC=d.DIAG_DATA_SRC and
			m.ROOT_COMPANIES_ID=d.ROOT_COMPANIES_ID and
			d.DiAG_SEQ_NO=1
		where
			m.ROOT_COMPANIES_ID=@rootId and
			FILL_DATE between @medstartdate and @medenddate
	)tbl1
	Group By EMPI,FILL_DATE,MedicationName,MEDICATION_CODE,GenericFlag,Formulary,PharmacyName,PrescribingProvider,PrescriptionDate,MedicationStrength,MedicationDose,PrimaryDiagnosis,TherapeuticClass
		

)t1
)t2
Where
	rn=1

		SET @PERF_ROW = @@ROWCOUNT
		SET @PERF_DURATION = DATEDIFF(MINUTE,@PERF_START,GETDATE());
		EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, 'Enrollment Completed', @PERF_ROW, @DURATION_IN_MIN=@PERF_DURATION, @LOG2ID=@LOGID;
		EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, @LOG2ID=@LOGID, @END_FLAG=1;

END TRY
BEGIN CATCH
		BEGIN
			EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, 'FATAL ERROR', @LOG2ID=@LOGID;
			RAISERROR ('',16,0)
			RETURN 16
        END
	END CATCH
RETURN 0
END

GO
