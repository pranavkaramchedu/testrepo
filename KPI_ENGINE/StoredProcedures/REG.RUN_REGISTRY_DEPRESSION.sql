SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
CREATE PROC [REG].[RUN_REGISTRY_DEPRESSION]
AS

BEGIN
DECLARE @INFO VARCHAR(8000)
					,	@TRUE BIT = 1
					,	@FALSE BIT = 0
					,	@DB_ID INT = DB_ID()
					,	@LOGID INT = 0
					,	@PROC VARCHAR(500)=OBJECT_SCHEMA_NAME(@@PROCID)+'.'+OBJECT_NAME(@@PROCID)
					,	@PERF_START DATETIME
					,	@PERF_DURATION INT
					,	@PERF_ROW INT
					,	@RC	INT
					;

		EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, @INFO=@INFO, @LOG2ID=@LOGID OUT;
		DECLARE @PROCFULLNAME VARCHAR(500) = DB_NAME()+'.'+@PROC;
		----EXEC dbo.SP_MI_UTIL_SEND_MAIL @PROCFULLNAME, 'STARTED', 1, @ECHO=0; 
	DECLARE @MSG VARCHAR(1000)
	DECLARE @PERF_START_PROC DATETIME 
	DECLARE @PERF_RPS NUMERIC(10,2) 
	DECLARE @ROWS INT 
	DECLARE @PERF_ROWS INT 
	SET @PERF_ROWS = 0 
	SET @PERF_START = GETDATE() 
	SET @PERF_START_PROC = GETDATE() 

BEGIN TRY


declare @rundate Date=GetDate();
declare @meas_year varchar(4)=Year(Dateadd(month,-2,@rundate))
declare @rootId INT=159
declare @attributiondate Date=DATEADD(month, DATEDIFF(month, 0, Dateadd(month,-2,@rundate)), 0)

Declare @startDate Date;
Declare @enddate date;
Declare @quarter varchar(20)

Declare @reportId INT;


-- Get ReportId from Report_Details

	exec GetReportDetail @rundate=@rundate,@rootId=@rootId,@startDate=@startDate output,@enddate=@enddate output,@quarter=@quarter output,@reportId=@reportId output


-- Insert data into Registry Output Line

Drop table if exists #depression;
Create table #depression
(
	RegistryName varchar(50),
	EMPI varchar(100),
	Payer varchar(50),
	PayerId varchar(50),
	MemberLastName varchar(100),
	MemberFirstName varchar(100),
	MemberMiddleName varchar(50),
	MemberDOB Date,
	MEM_GENDER varchar(20),
	EnrollmentStatus varchar(20),
	PracticeName varchar(200),
	AttributedProviderName varchar(200),
	AttributedProviderSpecialty varchar(100),
	AttributedProviderNPI varchar(20),
	DiagnosisCode varchar(20),
	ServiceDate Date,
	AttendingProviderName varchar(200),
	AttendingProviderNPI varchar(20),
	AttendingProviderSpecialty varchar(200),
	DiagnosisDataSource varchar(50),
	ROOT_COMPANIES_ID INT,
	ReportId INT,
	rnk INT
)
Insert into #depression
(RegistryName,EMPI,Payer,PayerId,MemberLastName,MemberFirstName,MemberMiddleName,MemberDOB,MEM_GENDER,EnrollmentStatus,PracticeName,AttributedProviderName,AttributedProviderSpecialty,AttributedProviderNPI,DiagnosisCode,ServiceDate,AttendingProviderName,AttendingProviderNPI,AttendingProviderSpecialty,DiagnosisDataSource,ROOT_COMPANIES_ID,ReportId,rnk)
Select RegistryName,EMPI,Payer,PayerId,MemberLastName,MemberFirstName,MemberMiddleName,MemberDOB,MEM_GENDER,EnrollmentStatus,PracticeName,AttributedProviderName,AttributedProviderSpecialty,AttributedProviderNPI,DiagnosisCode,ServiceDate,ServicingProviderName,ServicingProviderNPI,ServicingProviderSpecialty,DiagnosisDataSource,@rootId as ROOT_COMPANIES_ID,@reportId,rnk
from(
select	
	EMPI
	,Payer
	,PayerId
	,MemberLastName
	,MemberFirstName
	,MemberMiddleName
	,MemberDOB
	,MEM_GENDER
	,EnrollmentStatus
	,PracticeName
	,AttributedProviderName
	,AttributedProviderSpecialty
	,AttributedProviderNPI
	,RegistryName
	,DIAG_CODE as DiagnosisCode
	,ServiceDate
	,ServicingProviderName
	,ServicingProviderNPI
	,Case  
		when ServicingProviderSpecialty is null then PreferredConceptName 
		else ServicingProviderSpecialty 
	end as ServicingProviderSpecialty
	,DataSource as  DiagnosisDataSource
	,DENSE_RANK() over (partition by EMPI order by ServiceDate Desc,ServicingProviderNPI desc,rn desc) as rnk 
from(
	Select 
		a.AmbulatoryPCPPractice as PracticeName
		,a.AmbulatoryPCPName as AttributedProviderName
		,a.AmbulatoryPCPSpecialty as AttributedProviderSpecialty
		,a.AmbulatoryPCPNPI as AttributedProviderNPI
		,a.MemberLastName
		,a.MemberFirstName
		,a.MemberMiddleName
		,a.DATA_SOURCE as Payer
		,a.PayerId
		,a.MemberDOB
		,a.MEM_GENDER
		,a.EnrollmentStatus
		,a.AmbulatoryPCPRecentVisit as RecentVisit
		,'Depression' as RegistryName
		,t1.*
		,t.PreferredConceptName
		,row_number() over(partition by t1.EMPI order by ServiceDate Desc) as rn
	from(
			select distinct 
				c.EMPI
				,c.Member_id
				,d.diag_start_date as ServiceDate
				,d.DIAG_CODE,c.ATT_PROV as ServicingProviderId
				,c.ATT_NPI as ServicingProviderNPI
				,case 
					when NPI is not null then CONCAT(n.Provider_Last_Name,',',n.Provider_First_Name,' ',n.Provider_Middle_Name) 
					else c.ATT_PROV_NAME 
				end as ServicingProviderName
				,c.ATT_PROV_SPEC as ServicingProviderSpecialty
				,Case 
					when n.Healthcare_provider_primary_Taxonomy_switch_1='Y' then n.Healthcare_provider_Taxonomy_code_1
					when n.Healthcare_provider_primary_Taxonomy_switch_2='Y' then n.Healthcare_provider_Taxonomy_code_2
					when n.Healthcare_provider_primary_Taxonomy_switch_3='Y' then n.Healthcare_provider_Taxonomy_code_3
					when n.Healthcare_provider_primary_Taxonomy_switch_4='Y' then n.Healthcare_provider_Taxonomy_code_4
					when n.Healthcare_provider_primary_Taxonomy_switch_5='Y' then n.Healthcare_provider_Taxonomy_code_5
				end as PrimarySpecialtyCode
				,c.CL_DATA_SRC as DataSource 
			from CLAIMLINE c
			join DIAGNOSIS d on	c.CL_DATA_SRC=d.DIAG_DATA_SRC and 
								c.CLAIM_ID=d.CLAIM_ID and
								c.ROOT_COMPANIES_ID=d.ROOT_COMPANIES_ID
			left outer join KPI_ENGINE.dbo.RFT_NPI n on c.ATT_NPI=n.NPI
			where diag_Code in
			(
				select Replace(code,'.','') from KPI_ENGINE.REG.REGISTRY_VALUESET where valuesetname in('Depression','Depression or Other Behavioral Health Condition','Major Depression','Major Depression or Dysthymia')
			) 
			and ISNULL(c.POS,'')!='81' and 
			c.ROOT_COMPANIES_ID=@rootId and
			(n.Entity_Type_code='1' or n.Entity_Type_code is null)
		)t1 
	join KPI_ENGINE.RPT.PCP_ATTRIBUTION a on t1.EMPI=a.EMPI  and 
											 a.AssignedStatus='Assigned' and 
											 a.ReportId=@reportId and
											 a.ROOT_COMPANIES_ID=@rootId
	left outer join KPI_ENGINE.dbo.RFT_TAXONOMIES t on t1.PrimarySpecialtyCode=t.ConceptCode
	--where t1.EMPI='A406C66B-E035-48DC-9933-7D5BE490134D'

	)t2
)t3 



Delete from KPI_ENGINE.REG.REGISTRY_OUTPUT_LINE where ReportId=@reportId and RegistryName='Depression' and ROOT_COMPANIES_ID=@rootId;

Insert into KPI_ENGINE.REG.REGISTRY_OUTPUT_LINE(RegistryName,EMPI,Payer,PayerId,MemberLastName,MemberFirstName,MemberMiddleName,MemberDOB,MEM_GENDER,EnrollmentStatus,PracticeName,AttributedProviderName,AttributedProviderSpecialty,AttributedProviderNPI,DiagnosisCode,ServiceDate,AttendingProviderName,AttendingProviderNPI,AttendingProviderSpecialty,DiagnosisDataSource,ROOT_COMPANIES_ID,ReportId)
Select RegistryName,EMPI,Payer,PayerId,MemberLastName,MemberFirstName,MemberMiddleName,MemberDOB,MEM_GENDER,EnrollmentStatus,PracticeName,AttributedProviderName,AttributedProviderSpecialty,AttributedProviderNPI,DiagnosisCode,ServiceDate,AttendingProviderName,AttendingProviderNPI,AttendingProviderSpecialty,DiagnosisDataSource,ROOT_COMPANIES_ID,ReportId from #depression


Delete from KPI_ENGINE.REG.REGISTRY_OUTPUT where ReportId=@reportId and RegistryName='Depression' and ROOT_COMPANIES_ID=@rootId;

-- Insert data into Registry Output
Insert into KPI_ENGINE.REG.REGISTRY_OUTPUT(RegistryName,EMPI,Payer,PayerId,MemberLastName,MemberFirstName,MemberMiddleName,MemberDOB,MEM_GENDER,EnrollmentStatus,PracticeName,AttributedProviderName,AttributedProviderSpecialty,AttributedProviderNPI,DiagnosisCode,ServiceDate,AttendingProviderName,AttendingProviderNPI,AttendingProviderSpecialty,DiagnosisDataSource,ROOT_COMPANIES_ID,ReportId)
Select RegistryName,EMPI,Payer,PayerId,MemberLastName,MemberFirstName,MemberMiddleName,MemberDOB,MEM_GENDER,EnrollmentStatus,PracticeName,AttributedProviderName,AttributedProviderSpecialty,AttributedProviderNPI,DiagnosisCode,ServiceDate,AttendingProviderName,AttendingProviderNPI,AttendingProviderSpecialty,DiagnosisDataSource,ROOT_COMPANIES_ID,ReportId from #depression where rnk=1


		SET @PERF_ROW = @@ROWCOUNT
		SET @PERF_DURATION = DATEDIFF(MINUTE,@PERF_START,GETDATE());
		EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, 'Enrollment Completed', @PERF_ROW, @DURATION_IN_MIN=@PERF_DURATION, @LOG2ID=@LOGID;
		EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, @LOG2ID=@LOGID, @END_FLAG=1;

END TRY
BEGIN CATCH
		BEGIN
			EXEC dbo.SP_KPI_UTIL_LOG_EVENT @@PROCID, @DB_ID, 'FATAL ERROR', @LOG2ID=@LOGID;
			RAISERROR ('',16,0)
			RETURN 16
        END
	END CATCH
RETURN 0
END

GO
