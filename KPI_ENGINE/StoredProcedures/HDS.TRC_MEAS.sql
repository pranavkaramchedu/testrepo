SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
CREATE proc [HDS].[TRC_MEAS]
AS


drop table if exists #TRC_memlist; 

CREATE TABLE #TRC_memlist (
    memid varchar(100)
    
);


insert into #TRC_memlist
SELECT DISTINCT en.MemID FROM HDS.HEDIS_MEMBER_GM gm
join hds.HEDIS_MEMBER_EN en on en.MemID=gm.MemID and en.MEASURE_ID=gm.MEASURE_ID
WHERE en.MEASURE_ID='TRC_SAMPLE' and
CONVERT(DATE,en.StartDate)<='2020-12-31' AND CONVERT(DATE,FinishDate)>='2020-01-01'
AND YEAR('2020-12-31')-YEAR(DOB) >=18 ORDER BY 1;


	-- Inpatient Stay List
	drop table if exists #TRC_inpatientstaylist;
	CREATE table #trc_inpatientstaylist
	(
		Memid varchar(50),
		
		date_s varchar(8),
		date_disch varchar(8),
		claimid int
	);

Insert into #TRC_inpatientstaylist
select distinct MemID,Date_S,date_disch,claimid from HDS.HEDIS_VISIT v where v.Measure_id='TRC_SAMPLE' and v.HCFAPOS!=81 and suppdata='N' and v.date_disch!='' and CONVERT(DATE,v.date_disch) between '2020-01-01' and '2020-12-01' and v.REV in (select code from HDS.VALUESET_TO_CODE where code_system='UBREV' and Value_Set_Name in('Inpatient Stay'));
	
CREATE CLUSTERED INDEX ix_cdc_inpatientstaylist ON #TRC_inpatientstaylist ([memid],[date_s]);

----Readmission---------


drop table if exists #Readmission;
	CREATE table #Readmission
	(
		Memid varchar(50)
	);
Insert into #Readmission(Memid)
select distinct MemID from HDS.HEDIS_VISIT v where v.Measure_id='TRC_SAMPLE' and v.HCFAPOS!=81 and suppdata='N' and v.date_disch!='' and CONVERT(DATE,v.date_disch) >'2020-12-01' and v.REV in (select code from HDS.VALUESET_TO_CODE where code_system='UBREV' and Value_Set_Name in('Inpatient Stay'));


drop table if exists #TRC_inpatientstaylist_Final;
	CREATE table #TRC_inpatientstaylist_Final
	(
		Memid varchar(50)
	);
Insert into #TRC_inpatientstaylist_Final(Memid)
select distinct IP.Memid from #TRC_inpatientstaylist Ip
left join #Readmission R on IP.Memid=R.Memid
where  R.Memid is NULL ----AND ip.Memid='148480'





	drop table if exists #Denomerator;
	CREATE table #Denomerator
	(
		Memid varchar(50)
	);

Insert into #Denomerator
Select distinct M.memid from #TRC_memlist M
inner join (select distinct MemId from #TRC_inpatientstaylist_Final) IP on M.memid=Ip.Memid
----where M.memid='146792'
CREATE CLUSTERED INDEX ix_TRC_Denomeratort ON #Denomerator ([memid]);


-- Create Temp Patient Enrollment
drop table if exists #TRC_tmpsubscriber;

CREATE TABLE #TRC_tmpsubscriber (
    memid varchar(100),
    dob varchar(8),
	gender varchar(1),
	payer varchar(50),
	StartDate varchar(8),
	EndDate varchar(8),
	RN int
);
insert into #TRC_tmpsubscriber
SELECT en.MemID,gm.DOB,gm.Gender,en.Payer,en.StartDate,en.FinishDate, ROW_NUMBER() OVER(partition by en.MemID order by en.StartDate,en.FinishDate) as RN
FROM HDS.HEDIS_MEMBER_GM gm
join hds.HEDIS_MEMBER_EN en on en.MemID=gm.MemID and en.MEASURE_ID=gm.MEASURE_ID
Inner join #Denomerator D on gm.MemID=D.Memid
WHERE gm.MEASURE_ID='TRC_SAMPLE' and
CONVERT(DATE,en.StartDate)<='2020-12-31' AND CONVERT(DATE,FinishDate)>='2020-01-01'
AND YEAR('2020-12-31')-YEAR(DOB) >=18 ORDER BY en.MemID,CONVERT(DATE,en.StartDate),CONVERT(DATE,FinishDate);

CREATE CLUSTERED INDEX ix_tmpsub_memid ON #TRC_tmpsubscriber ([memid],[StartDate],[EndDate]);




-- Inpatient Stay List
	drop table if exists #TRC_DISCHARGE_MEMBERS;
	CREATE table #TRC_DISCHARGE_MEMBERS
	(
		Memid varchar(50),
		date_s varchar(8),
		date_Adm varchar(8),
		date_disch varchar(8),
		claimid int,
		RN int
	);

Insert into #TRC_DISCHARGE_MEMBERS
select distinct v.MemID,Date_S,Date_Adm,date_disch,claimid,DENSE_RANK() over (partition by v.Memid order by convert(date,date_Adm),convert(date,Date_disch)) as RN
from HDS.HEDIS_VISIT v 
---Inner join #TRC_tmpsubscriber S on v.MemID=S.memid and convert(date,v.Date_Disch) between convert(date, S.StartDate) and convert(date,S.EndDate)
where v.Measure_id='TRC_SAMPLE' and v.HCFAPOS!=81 and suppdata='N' and v.date_disch!='' and CONVERT(DATE,v.date_disch) between '2020-01-01' and '2020-12-01' and v.REV in (select code from HDS.VALUESET_TO_CODE where code_system='UBREV' and Value_Set_Name in('Inpatient Stay'))
AND v.MEMID IN (Select MemID from #Denomerator)



--Medication Reconciliation 
drop table if exists #TRC_Medication_Reconciliation;
CREATE table #TRC_Medication_Reconciliation
(
	Memid varchar(50),
	date_s varchar(8),
);
Insert into #TRC_Medication_Reconciliation
select distinct MemID,Date_S from HDS.HEDIS_VISIT v where convert(date,date_s) between '2020-01-31' and '2021-01-30' and v.HCFAPOS!=81 and suppdata='N' and
Measure_id='TRC_SAMPLE' 
and 
(
CPT2 in (select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Medication Reconciliation Intervention'))
or
DIAG_I_1 in (select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Medication Reconciliation Intervention'))
or
DIAG_I_2 in (select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Medication Reconciliation Intervention'))
or
DIAG_I_3 in (select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Medication Reconciliation Intervention'))
or
DIAG_I_4 in (select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Medication Reconciliation Intervention'))
or
DIAG_I_5 in (select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Medication Reconciliation Intervention'))
or
DIAG_I_6 in (select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Medication Reconciliation Intervention'))
or
DIAG_I_7 in (select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Medication Reconciliation Intervention'))
or
DIAG_I_8 in (select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Medication Reconciliation Intervention'))
or
DIAG_I_9 in (select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Medication Reconciliation Intervention'))
or
DIAG_I_10 in (select code from HDS.VALUESET_TO_CODE where Value_Set_Name in('Medication Reconciliation Intervention'))

)
and ProvID in (select distinct ProvID from hds.HEDIS_PROVIDER where measure_id='TRC_SAMPLE' and (ProvPres='Y' or PHAProv='Y' or RN='Y'))
and v.MEMID IN (Select MemID from #Denomerator)
union
select distinct E.MemID,E.sdate from hds.HEDIS_VISIT_E E
left join hds.HEDIS_PROC P on E.MemID=P.MemID AND CONVERT(DATE,E.sdate)=CONVERT(DATE,P.sdate) and P.measure_id=E.MEASURE_ID AND P.pstatus='EVN'
where E.measure_id='TRC_SAMPLE' and CONVERT(DATE,E.sdate) BETWEEN '2020-01-01' and '2021-01-30' and 
P.pcode IN (select code from HDS.VALUESET_TO_CODE where Value_Set_Name in ('Medication Reconciliation Intervention'))
and e.Providerid IN (select distinct ProvID from hds.HEDIS_PROVIDER where measure_id='WCV_SAMPLE' and (ProvPres='Y' or PHAProv='Y' or RN='Y'))
and E.MEMID IN (Select MemID from #Denomerator)
union 
select distinct E.MemID,E.sdate from hds.HEDIS_VISIT_E E
where E.measure_id='TRC_SAMPLE' and CONVERT(DATE,E.sdate) BETWEEN '2020-01-01' and '2021-01-30' and 
activity IN (select code from HDS.VALUESET_TO_CODE where Value_Set_Name in ('Medication Reconciliation Encounter'))
and e.Providerid IN (select distinct ProvID from hds.HEDIS_PROVIDER where measure_id='WCV_SAMPLE' and (ProvPres='Y' or PHAProv='Y' or RN='Y'))
and E.MEMID IN (Select MemID from #Denomerator)





drop table if exists #TRC_EAEB;
select distinct Memid,'TRCEA' as Meas
into #TRC_EAEB
from #TRC_DISCHARGE_MEMBERS A 
where RN=1
Union
select distinct A.Memid,'TRCEB' as Meas
from #TRC_DISCHARGE_MEMBERS A
inner join #TRC_DISCHARGE_MEMBERS B on A.Memid=B.Memid and A.RN=B.RN+1
where convert(date,a.date_Adm)>DATEADD(d,30,convert(date,b.date_disch))
union
select distinct Memid,'TRCMA' as Meas
from #TRC_DISCHARGE_MEMBERS A 
where RN=1
Union
select distinct A.Memid,'TRCMB' as Meas
from #TRC_DISCHARGE_MEMBERS A
inner join #TRC_DISCHARGE_MEMBERS B on A.Memid=B.Memid and A.RN=B.RN+1
where convert(date,a.date_Adm)>DATEADD(d,30,convert(date,b.date_disch))





/*
drop table if exists #TRC_MAMB;
select distinct A.Memid,'TRCMA' as Meas
into #TRC_MAMB
from #TRC_DISCHARGE_MEMBERS A 
inner join #TRC_Medication_Reconciliation R
on A.Memid=R.Memid and convert(date,R.date_s)  between convert(date,A.date_disch) and dateadd(d,30,convert(date,A.date_disch))
where RN=1
Union
select distinct A.Memid,'TRCMB' as Meas
from #TRC_DISCHARGE_MEMBERS A
inner join #TRC_Medication_Reconciliation R
on A.Memid=R.Memid and convert(date,R.date_s)  between convert(date,A.date_disch) and dateadd(d,30,convert(date,A.date_disch)) 
where A.RN>1
*/



select * from #TRC_EAEB A
left join 
(select distinct Memid,Meas from hds.HEDIS_SCORE where MEASURE_ID='TRC_SAMPLE' and Meas in ('TRCEA','TRCEB','TRCMA','TRCMB')) B
on A.Memid=B.MemID and A.Meas=B.Meas
where B.MemID is NULL
order by A.MemId




select * from (select distinct Memid,Meas from hds.HEDIS_SCORE where MEASURE_ID='TRC_SAMPLE' and Meas in ('TRCEA','TRCEB','TRCMA','TRCMB')) A
left join 
#TRC_EAEB B
on A.Memid=B.MemID and A.Meas=B.Meas
where B.MemID is NULL
order by A.MemId





---select * into #TRCdataset from hds.TRCdataset

select A.Patient_id,B.Meas,Payer,patient_gender,age,lis,rexcl,rexcld,CE,excl,num,Event 
into #TRCdataset_Meas
from #TRCdataset A
left join #TRC_EAEB B
on Patient_id=Memid





select * from (select distinct Memid,Meas from hds.HEDIS_SCORE where MEASURE_ID='TRC_SAMPLE' and Meas in ('TRCEA','TRCEB','TRCMA','TRCMB')) A
left join 
#TRCdataset_Meas B
on A.Memid=B.patient_id and A.Meas=B.Meas
where B.patient_id is NULL
order by A.MemId

select * from #TRCdataset_Meas A
left join 
(select distinct Memid,Meas from hds.HEDIS_SCORE where MEASURE_ID='TRC_SAMPLE' and Meas in ('TRCEA','TRCEB','TRCMA','TRCMB')) B
on B.Memid=A.patient_id and A.Meas=B.Meas
where B.MemID is NULL
order by A.patient_id
GO
